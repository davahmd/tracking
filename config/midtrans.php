<?php

return [
    'client_key' => env('MIDTRANS_CLIENT_KEY'),
    'server_key' => env('MIDTRANS_SERVER_KEY'),
    'is_production' => env('MIDTRANS_IS_PRODUCTION', true),
    'is_sanitized' => env('MIDTRANS_IS_SANITIZED', true),
    'is_3ds' => env('MIDTRANS_IS_3DS', true),
    'available_payment_method' => [
        "akulaku",
        "bca_klikbca",
        "bca_klikpay",
        "bca_va",
        "bni_va",
        "cimb_clicks",
        "bri_epay",
        "credit_card",
        "danamon_online",
        "echannel",
        "gopay",
        "mandiri_clickpay",
        "indomaret",
        "mandiri_ecash",
        "other_va",
        "permata_va"
    ],
];
