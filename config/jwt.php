<?php

return [
    'secret_key' => env('JWT_SECRET_KEY', 'abcd1234'),

    'access_token_ttl' => '60',

    'refresh_token_ttl' => '120',
];