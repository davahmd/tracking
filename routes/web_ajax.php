<?php

Route::get('load_maincategory', 'AjaxController@load_maincategory');
Route::get('load_subcategory', 'AjaxController@load_subcategory');
Route::get('load_secsubcategory', 'AjaxController@load_secsubcategory');

Route::get('load_size', 'AjaxController@load_size');
Route::get('load_color', 'AjaxController@load_color');

Route::get('view_shipment/{id}', 'AjaxController@view_shipment');
Route::get('accept_order/{id}/{status}', 'AjaxController@accept_order');
Route::get('update_shipment/{id}','AjaxController@update_shipment');
Route::post('update_shipment/{order_id}','AjaxController@update_shipment_submit');

//OnlineOrder
Route::get('online/invoice/{id}/{type}', 'AjaxController@online_tax_invoice');
Route::any('online/invoices/{type}', 'AjaxController@online_tax_invoice_batch');
Route::get('online/delivery/{id}/{type}', 'AjaxController@online_delivery_order');
Route::get('online/deliveries/{type}', 'AjaxController@online_delivery_order_batch');
Route::get('transaction/reference/{id}/{type}', 'AjaxController@transaction_reference');
Route::post('transaction/{transaction_id}/payment-cancelled', 'AjaxController@paymentCancelled')->name('payment.cancelled');

Route::get('send_auction_winner/{oa_id}', 'AjaxController@send_auction_winner');
Route::post('send_auction_winner', 'AjaxController@send_auction_winner_submit');

Route::get('load_merchant_store', 'AjaxController@load_merchant_store');
Route::get('load_merchant_store_service', 'AjaxController@load_merchant_store_service');
Route::get('load_city','AjaxController@load_city');
// Route::get('merchant_emailcheck', 'AjaxController@merchant_emailcheck');
// Route::get('merchant_usernamecheck', 'AjaxController@merchant_usernamecheck');

Route::get('offline/reference/{id}/{type}', 'AjaxController@offline_transaction_reference');
Route::get('offline/references/{type}', 'AjaxController@offline_transaction_reference_batch');
Route::get('offline/invoice/{id}/{type}', 'AjaxController@offline_tax_invoice');
Route::get('offline/invoices/{type}', 'AjaxController@offline_tax_invoice_batch');

Route::get('get_merchant_bank_info/{mer_id}', 'AjaxController@get_merchant_bank_info');

Route::get('vcoinlog/{id}', 'AjaxController@vcoinlog');
Route::get('gplog/{id}', 'AjaxController@gplog');

Route::get('load_state','AjaxController@load_state');
Route::get('load_city', 'AjaxController@load_city');
Route::get('load_subdistrict', 'AjaxController@load_subdistrict');

Route::get('edit_product_image/{mer_id}/{id}', 'AjaxController@edit_product_image');
Route::post('/product_image/edit', 'AjaxController@edit_product_image_submit');

Route::get('/get_product_category', 'AjaxController@product_category');
Route::get('/get_product_category/{id}', 'AjaxController@product_category');
Route::get('/get_category_name_by_id/{id}', 'AjaxController@get_category_name_by_id');
Route::get('/product_pricing/edit/{mer_id}/{id}', 'AjaxController@edit_product_pricing');
Route::post('/product_pricing/edit', 'AjaxController@edit_product_pricing_submit');
// Route::any('/pricing_attribute/edit/{mer_id}/{pricing_id}', 'AjaxController@edit_pricing_attribute');
Route::any('/pricing_attribute_quantity/edit/{mer_id}/{pro_id}/{pricing_id}', 'AjaxController@edit_pricing_attribute_quantity');
Route::get('/product_detail/get_attribute_selection/{pro_id}', 'AjaxController@get_attribute_selection');

Route::get('/attribute/check_attribute_exist/{mer_id}/{pro_id}/{attribute_id}','AjaxController@check_attribute_exist');
Route::get('/attribute/remove/{pro_id}','AjaxController@remove_product_attribute');
Route::get('/edit_product_attribute/{attribute_id}/{pro_id}/{mer_id}', 'AjaxController@edit_product_attribute');
Route::get('/attribute/parent_check_attribute_exist/{mer_id}/{pro_id}/{attribute_id}','AjaxController@parent_check_attribute_exist');
Route::post('/update_attribute_parent/{pro_id}', 'AjaxController@update_attribute_parent_submit');
Route::get('/product/attribute_parent/delete/{attribute_id}/{option}', 'AjaxController@parent_delete_product_attribute');
Route::get('/product/attribute/delete/{attribute_id}/{option}', 'AjaxController@delete_product_attribute');
Route::get('/add_attribute_item', 'AjaxController@add_attribute_item_submit');
Route::get('/product/service/delete/{service_id}/{option}', 'AjaxController@delete_service');
Route::get('/edit_service/{service_id}/{pro_id}', 'AjaxController@edit_service');
Route::post('/update_service/{service_id}', 'AjaxController@update_service_submit');

Route::get('/get_offline_category', 'AjaxController@offline_category');
Route::get('/get_offline_category/{id}', 'AjaxController@offline_category');

Route::get('/store_emailcheck', 'AjaxController@store_emailcheck');
Route::get('/store_usernamecheck', 'AjaxController@store_usernamecheck');

Route::get('/member_emailcheck', 'AjaxController@member_emailcheck');
Route::get('/member_usernamecheck', 'AjaxController@member_usernamecheck');
Route::get('/member_phone_check', 'AjaxController@member_phone_check');
Route::get('/sms_verification', 'AjaxController@send_tac');
Route::get('/check_tac', 'AjaxController@check_tac');
Route::get('/check_member_verification/{operation}', 'AjaxController@check_member_verification');

Route::get('/get_code_number_listing/{order_id}/{by}/{type}', 'AjaxController@get_code_number_listing');
Route::post('/description','AjaxController@saveimage');
Route::get('/load_name', 'AjaxController@load_name');
Route::get('/load_store_name', 'AjaxController@load_store_name');
Route::get('/load_merchant_name', 'AjaxController@load_merchant_name');

Route::get('/get_fund_withdraw_statement', 'AjaxController@get_fund_withdraw_statement');
Route::get('/download/ecard', 'AjaxController@download_ecard_template');
Route::get('/generate/country_state', 'AjaxController@buildCountryStateJs');

/* VEHICLE */
Route::get('/get_model/{brand}', 'AjaxController@get_models');
Route::get('/get_variant/{brand}/{model}', 'AjaxController@get_variants');
Route::get('/get_year/{model}/{variant}', 'AjaxController@get_years');

Route::get('/get_recent_products', 'AjaxController@get_recent_products');
Route::get('/get_more_recent_products', 'AjaxController@get_more_recent_products');

Route::get('/load_more_products', 'AjaxController@load_more_products');
Route::get('/get_more_store_products/{type}/{store_slug}', 'AjaxController@get_more_store_products');
Route::get('/get_more_brand_products/{type}/{brand_slug}', 'AjaxController@get_more_brand_products');
Route::get('/get_more_products/{type}', 'AjaxController@get_more_products');

Route::post('/set_server_cookie', 'AjaxController@set_server_cookie');
Route::post('/get_courier_fees', 'AjaxController@get_courier_fees');
Route::post('/update_cart_courier', 'AjaxController@update_cart_courier');
Route::any('/cart_payment', 'AjaxController@cart_payment');

Route::get('/toggle_review_display', 'AjaxController@toggleDisplayReview');
Route::post('/get_multiple_store_products', 'AjaxController@get_multiple_store_products');
Route::post('/get_multiple_store_categories', 'AjaxController@get_multiple_store_categories');

Route::post('/get_promo_code', 'AjaxController@get_promo_code');
Route::get('/get_negotiation_detail/{negotiation_id}', 'AjaxController@get_negotiation_detail');
Route::get('/get_admin_negotiation_detail/{session_id}', 'AjaxController@get_admin_negotiation_detail');


Route::get('/search', 'AjaxController@search');