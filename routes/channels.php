<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\Models\Customer;
use App\Models\Merchant;
use App\Models\Store;
use App\Models\StoreUser;

Broadcast::channel('customer.{customerId}', function ($customer, $customerId) {
    return (int) $customer->cus_id === (int) $customerId;
});

Broadcast::channel('merchant.{merchantId}', function ($merchant, $merchantId) {
    return (int) $merchant->mer_id === (int) $merchantId;
});

Broadcast::channel('store_user.{storeUserId}', function ($store_user, $storeUserId) {
    return (int) $store_user->id === (int) $storeUserId;
});

Broadcast::channel('chat.{customerId}.{storeId}.{productId}', function ($user, $customerId, $storeId, $productId) {
    if ($user instanceof Customer) {
        if ((int) $user->cus_id !== (int) $customerId) {
            return false;
        }

        return [
            'cus_id' => $user->cus_id,
            'cus_name' => $user->cus_name
        ];
    }
    elseif ($user instanceof Merchant) {
        $store_merchant_id = Store::where('stor_id', $storeId)->value('stor_merchant_id');

        if ((int) $user->mer_id !== (int) $store_merchant_id) {
            return false;
        }

        return [
            'mer_id' => $user->cus_id,
            'mer_name' => $user->merchantName()
        ];
    }
    elseif ($user instanceof StoreUser) {
        $store_merchant_id = Store::where('stor_id', $storeId)->value('stor_merchant_id');

        if ((int) $user->mer_id !== (int) $store_merchant_id) {
            return false;
        }

        return [
            'store_user_id' => $user->id,
            'store_user_name' => $user->name
        ];
    }

    return false;
});

