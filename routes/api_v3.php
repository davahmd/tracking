<?php

// API for Mobile APP V3
use App\Models\MemberServiceSchedule;
use App\Models\Order;
use App\Services\Notifier\Notifier;

Route::group(['prefix' => 'v3'], function () {

    Route::get('test', function () {

        $user = \App\Models\Customer::find(19);

        $user->notify(new \App\Notifications\NewMessage(\App\Models\Chat::find(320)));

    });

    // General API
    Route::group(['prefix' => 'general', 'namespace' => 'Api\V3'], function () {
        Route::get('version', 'GeneralController@getVersion');
        Route::get('countries', 'GeneralController@getCountries');
        Route::get('states', 'GeneralController@getStates');
        Route::get('location', 'GeneralController@getLocation');
        Route::get('categories', 'GeneralController@getCategories');
        Route::get('territory', 'GeneralController@getTerritory');
        Route::get('official-brands', 'GeneralController@getOfficialBrands');
        Route::get('car-brands', 'GeneralController@carBrands');
        Route::post('device-token', 'GeneralController@storeDeviceToken');
        Route::get('couriers', 'GeneralController@getCouriers');
        Route::get('complain-subject', 'GeneralController@getComplainSubject');
    });

    Route::get('/home', 'Api\V3\GeneralController@homepage');
    Route::post('/tac','Api\V3\GeneralController@requestTac');

    // Member API
    Route::group(['namespace' => 'Api\V3\Member'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'RegisterController@register');
        Route::post('forgot-password', 'ForgetPasswordController@sendResetLinkEmail');

        Route::post('products', 'ProductController@index');
        Route::get('products/detail', 'ProductController@detail');
        Route::get('products/review', 'ProductController@reviews');

        Route::group(['middleware' => ['auth:api']], function () {
            Route::get('merchants', 'MerchantController@index');
            Route::get('merchants/detail', 'MerchantController@detail');
            Route::get('merchants/info', 'MerchantController@info');
            Route::get('merchants/addresses', 'MerchantController@addresses');
            Route::post('merchants/request-retailer', 'MerchantController@requestRetailer');
        });

        Route::get('stores', 'StoreController@index');
        Route::get('stores/detail', 'StoreController@detail');
        Route::get('stores/info', 'StoreController@info');
        Route::get('stores/top', 'StoreController@topStores');
        Route::get('stores/review', 'StoreController@reviews');

        Route::get('brands', 'OfficialBrandController@index');
        Route::get('search', 'SearchController@index');

        Route::group(['prefix' => 'member', 'middleware' => ['auth:api']], function () {
            Route::post('logout', 'AuthController@logout');

            Route::get('profile', 'ProfileController@details');
            Route::patch('profile', 'ProfileController@update');

            Route::get('cars', 'ProfileController@cars');
            Route::post('cars', 'ProfileController@addCar');
            Route::patch('cars', 'ProfileController@updateCar');
            Route::delete('cars', 'ProfileController@removeCar');

            Route::get('model', 'ProfileController@getModel');

            Route::get('profile-address', 'ProfileController@profileAddress');
            Route::patch('profile-address', 'ProfileController@updateProfileAddress');

            Route::get('addresses', 'ProfileController@addresses');
            Route::post('addresses', 'ProfileController@addAddress');
            Route::patch('addresses', 'ProfileController@updateAddress');
            Route::delete('addresses', 'ProfileController@removeAddress');
            Route::patch('update-password', 'ProfileController@updatePassword');

            Route::get('automobile', 'AutomobileController@view');
            Route::post('automobile', 'AutomobileController@add');
            Route::patch('automobile', 'AutomobileController@update');
            Route::delete('automobile', 'AutomobileController@delete');

            Route::get('orders/details', 'OrderController@orderDetails');
            Route::get('orders/history', 'OrderController@orderHistory');
            Route::post('orders/review', 'OrderController@orderReview');
            Route::post('orders/updateStatus', 'OrderController@update_order_status');

            Route::get('services/history', 'ServiceController@serviceHistory');
            Route::get('services/details', 'ServiceController@serviceDetails');
            Route::post('services/updateStatus', 'ServiceController@update_service_status');
            Route::post('services/scheduleService', 'ServiceController@schedule_service_member');

            Route::group(['prefix' => 'complain'], function () {
                Route::get('/', 'ComplainController@view');
                Route::post('create', 'ComplainController@create');
                Route::post('send-message', 'ComplainController@createMessage');
                Route::post('escalate', 'ComplainController@postEscalate');
            });

            Route::get('carts', 'CartController@index');
            Route::post('carts', 'CartController@addItem');
            Route::delete('carts', 'CartController@removeItem');
            Route::patch('carts/update-quantity', 'CartController@updateQuantity');
            Route::patch('carts/update-courier', 'CartController@updateCourier');
            Route::patch('carts/update-shipping', 'CartController@updateShipping');
            Route::post('carts/checkout', 'CartController@checkout');
            Route::post('carts/promo-code', 'CartController@promoCodeValidation');

            Route::post('negotiation/create', 'NegotiationController@createNegotiation');
            Route::post('negotiation/update', 'NegotiationController@updateNegotiation');
            Route::get('negotiation/listing', 'NegotiationController@negotiationListing');
            Route::get('negotiation/detail', 'NegotiationController@negotiationDetail');

            Route::get('notifications', 'NotificationController@index');
            Route::get('notifications/detail', 'NotificationController@detail');
            Route::get('notifications/unread/count', 'NotificationController@unreadCount');

            Route::post('notifications/test', 'NotificationController@test');

            // chat
            Route::group(['prefix' => 'chat'], function () {
                Route::get('/', 'ChatController@list')->name('api-member-chat-list');
                Route::get('count', 'ChatController@unreadMessagesCount')->name('api-member-chat-count');
                Route::get('{product_id}', 'ChatController@conversation')->name('api-member-chat-conversation');
                Route::post('{store_id}/{product_id}', 'ChatController@postMessage')->name('api-member-chat-post-message');
                Route::post('messages-read/{store_id}/{product_id}', 'ChatController@messagesRead')->name('api-member-chat-messages-read');
            });
        });

    });

    // Merchant API
    Route::group(['prefix' => 'merchant', 'namespace' => 'Api\V3\Merchant'], function () {

        Route::post('login', 'AuthController@login');
        Route::post('forgot-password', 'ForgotPasswordController@sendResetLinkEmail');

        Route::group(['middleware' => ['auth:api', 'merchant']], function () {
            Route::get('home', 'HomeController@index');
            Route::get('revenue', 'HomeController@revenue');
            Route::post('update-password', 'ProfileController@updatePassword');
            Route::post('logout', 'AuthController@logout');

            Route::group(['prefix' => 'stores'], function () {
                Route::get('/', 'StoreController@index');
                Route::get('page', 'StoreController@page');
                Route::get('info', 'StoreController@info');
            });

            Route::group(['prefix' => 'transaction'], function () {
                Route::get('/', 'TransactionController@index');
                Route::get('detail', 'TransactionController@detail');
                Route::post('update-status', 'TransactionController@updateStatus');
            });

            Route::group(['prefix' => 'service'], function () {
                Route::get('/', 'ServiceController@index');
                Route::get('detail', 'ServiceController@detail');
                Route::post('updateStatus', 'ServiceController@update_service_status');
                Route::post('scheduleService', 'ServiceController@schedule_service_merchant');
            });

            // chat
            Route::group(['prefix' => 'chat'], function () {
                Route::get('/', 'ChatController@list')->name('api-merchant-chat-list');
                Route::get('count', 'ChatController@unreadMessagesCount')->name('api-merchant-chat-count');
                Route::get('{customer_id}/{product_id}', 'ChatController@conversation')->name('api-merchant-chat-conversation');
                Route::post('{customer_id}/{product_id}', 'ChatController@postMessage')->name('api-merchant-chat-post-message');
                Route::post('messages-read/{customer_id}/{product_id}', 'ChatController@messagesRead')->name('api-merchant-chat-messages-read');
            });

            Route::group(['prefix' => 'notification'], function () {
                Route::get('/', 'NotificationController@index');
                Route::get('detail', 'NotificationController@detail');
                Route::get('unread/count', 'NotificationController@unreadCount');
            });

            Route::group(['prefix' => 'complain'], function () {
                Route::get('/', 'ComplainController@view');
                Route::get('/list', 'ComplainController@complainList');
                Route::post('send-message', 'ComplainController@createMessage');
                Route::post('escalate', 'ComplainController@postEscalate');
            });

            Route::group(['prefix' => 'negotiation'], function () {
                Route::post('update', 'NegotiationController@updateNegotiation');
                Route::get('listing', 'NegotiationController@negotiationListing');
                Route::get('detail', 'NegotiationController@negotiationDetail');
            });
        });

        Route::group(['middleware' => ['auth:api_merchants']], function () {
            Route::group(['prefix' => 'stores'], function () {
                Route::post('update', 'StoreController@update');
            });
        });
    });
});