<?php

// Merchant route
Route::get('merchant/login', 'Merchant\Auth\AuthController@showLoginForm')->name('merchant.login');
Route::post('merchant/login', 'Merchant\Auth\AuthController@login');

Route::get('merchant/register', 'Merchant\Auth\RegisterController@showRegistrationForm');
Route::post('merchant/register', 'Merchant\Auth\RegisterController@register');

Route::get('merchant/usernamecheck', 'Merchant\Auth\RegisterController@ajaxMerchantUsernameCheck');
Route::get('merchant/emailcheck', 'Merchant\Auth\RegisterController@ajaxMerchantEmailCheck');

Route::get('merchant/password/reset', 'Merchant\Auth\ForgotPasswordController@showLinkRequestForm')->name('merchant.password.request');
Route::post('merchant/password/email', 'Merchant\Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('merchant/password/reset/{token}', 'Merchant\Auth\ResetPasswordController@showResetForm')->name('merchant.password.reset');
Route::post('merchant/password/reset', 'Merchant\Auth\ResetPasswordController@reset');

Route::get('merchant/activation/{token}', 'Merchant\Auth\AuthController@activateUser')->name('merchant.activate');
Route::get('merchant/activation-success', 'Merchant\Auth\AuthController@activationSuccess');
Route::get('merchant/resend/activation/{mer_id}/{mer_email}', 'Merchant\Auth\AuthController@resendActivation');
Route::any('merchant/resend/activation/{mer_id}', ['uses' =>'Merchant\Auth\AuthController@resendActivationForm', 'middleware' => 'merchantThrottle.throttle:5,1']);

Route::group(['middleware' => 'auth:web'], function () {
    Route::group(['prefix' => 'merchant', 'middleware' => 'merchant'], function() {
        Route::get('/', ['uses' => 'Merchant\HomeController@index']);
        Route::get('home', 'Merchant\HomeController@index');
        Route::get('logout', 'Merchant\Auth\AuthController@logout');

        Route::get('merchant-user', ['uses' => 'Merchant\MerchantUserController@index']);
        Route::any('merchant-user/create', ['uses' => 'Merchant\MerchantUserController@create']);
        Route::any('merchant-user/edit/{id}', ['uses' => 'Merchant\MerchantUserController@edit']);

        Route::get('product/add', 'Merchant\ProductController@add_product');
        Route::post('product/add', 'Merchant\ProductController@add_product_submit');
        Route::get('product/edit/{pro_id}', 'Merchant\ProductController@edit_product');
        Route::post('product/edit/{pro_id}', 'Merchant\ProductController@edit_product_submit');
        Route::get('product/description/{pro_id}', 'Merchant\ProductController@product_description');
        Route::post('product/description/{pro_id}', 'Merchant\ProductController@product_description_submit');

        Route::any('product/view/{pro_id}', 'Merchant\ProductController@view_product');
        Route::get('product/manage', 'Merchant\ProductController@manage_product');
        Route::get('product/block/{pro_id}/{type}', 'Merchant\ProductController@change_product_status');

        Route::get('product/sold', 'Merchant\ProductController@sold_product');
        Route::get('product/shipping', 'Merchant\ProductController@manage_product_shipping_details');

        Route::get('/product/review/{product_id}', 'Merchant\ProductController@review')->name('merchant.product.review');

        Route::get('profile', 'Merchant\MerchantUserController@get_merchant_profile');
        // Route::any('profile/edit', 'Merchant\MerchantUserController@edit_merchant_profile');
        Route::any('profile/password', 'Merchant\MerchantUserController@update_password');

        Route::post('/profile/avatar', 'Merchant\MerchantUserController@updateAvatar')->name('merchant.avatar.update');

        Route::group(['prefix' => 'transaction'], function () {
            Route::get('offline', 'Merchant\TransactionController@order_offline');

            Route::group(['prefix' => 'online'], function() {
                Route::get('{type?}', 'Merchant\TransactionController@order_online')->name('merchant.online.transaction');
                Route::get('detail/{parent_id}/{store_id}', 'Merchant\TransactionController@transaction_detail')->name('merchant.online.transaction.detail');
                Route::post('update/status/{parent_id}/{store_id}', 'Merchant\TransactionController@order_status_update')->name('merchant.update.order.status');

                Route::get('{type?}/invoices', 'Merchant\TransactionController@online_invoices')->name('merchant.online.invoices');
                Route::get('{type?}/deliveries', 'Merchant\TransactionController@online_deliveries')->name('merchant.online.deliveries');

                Route::get('{type?}/export/listing', 'Merchant\ExportController@online_orders')->name('merchant.export.online.transaction');
            });

            // Route::get('product/{operation}', 'Merchant\TransactionController@product_transaction');
            // Route::post('product/batch/update/{mer_id}/{operation}', 'Merchant\TransactionController@update_batch_transaction');
            // Route::get('online/complete/{order_id}', 'Merchant\TransactionController@completing_merchant_order');
        });

        Route::get('fund/report', 'Merchant\FundController@fund_report');
        Route::get('fund/withdraw', 'Merchant\FundController@fund_withdraw');
        Route::post('fund/withdraw', 'Merchant\FundController@fund_withdraw_submit');

        Route::get('store/manage', 'Merchant\StoreController@manage');
        Route::get('store/add', 'Merchant\StoreController@add');
        Route::post('store/add', 'Merchant\StoreController@add_submit');
        Route::get('store/edit/{stor_id}/', 'Merchant\StoreController@edit');
        Route::post('store/edit/{stor_id}/', 'Merchant\StoreController@edit_submit');
        Route::get('store/limit/{stor_id}', 'Merchant\StoreController@limit');

        Route::get('/store/review/{product_id}', 'Merchant\StoreController@review')->name('merchant.store.review');

        Route::post('setlocale', 'Merchant\HomeController@setlocale');

        Route::get('credit/log', 'Merchant\MerchantUserController@merchant_credit_log');

        //Product Image
        Route::get('product/image/save_order', 'Merchant\ProductImageController@product_image_saveorder');
        Route::get('product/image/set_main_image/{id}', 'Merchant\ProductImageController@set_main_image');
        Route::get('product/image/toggle_image_status/{id}', 'Merchant\ProductImageController@toggle_image_status');
        Route::get('product/image/view/{pro_id}', 'Merchant\ProductImageController@view_product_image');
        Route::post('product/image/add', 'Merchant\ProductImageController@upload_product_image');
        Route::get('product/image/delete/{id}', 'Merchant\ProductImageController@delete_product_image');

        //Vehicle
        Route::get('/product/vehicle/delete/{pro_id}/{vehicle_id}', 'Merchant\ProductController@vehicle_delete');
        Route::get('/product/vehicle/{pro_id}', 'Merchant\ProductController@product_vehicle');
        Route::post('/product/vehicle', 'Merchant\ProductController@product_vehicle_submit');

        //product e-card
        Route::get('product/code/listing/{pro_id}', 'Merchant\ProductController@ecard_listing');
        Route::post('product/code/upload/{pro_id}', 'Merchant\ProductController@ecard_upload');
        // Route::get('product/code/redeem/{id}/{pro_id}', 'Merchant\ProductController@ecard_redeem');

        //product Pricing
        Route::get('product/pricing/{pro_id}', 'Merchant\ProductController@product_pricing');
        Route::post('product/pricing/add/{pro_id}', 'Merchant\ProductController@product_pricing_submit');
        Route::get('product/toggle_pricing_status/{id}', 'Merchant\ProductController@update_pricing_status');
        Route::get('product/delete_product_pricing/{id}', 'Merchant\ProductController@delete_product_pricing');
        Route::post('product/pricing/status/batch_update/{pro_id}',
            'Merchant\ProductController@update_pricing_status_batch');

        //product quantity log
        Route::get('product/quantity/{id}', 'Merchant\ProductController@view_product_quantity_log');

        //product attribute
        Route::get('product/attribute/{pro_id}', 'Merchant\ProductController@manage_attribute');
        Route::post('product/attribute/{pro_id}', 'Merchant\ProductController@update_attribute');
        Route::post('product/attribute/{pro_id}/add', 'Merchant\ProductController@add_attribute_submit');
        Route::get('product/attribute/{pro_id}/delete/{attribute_id}/{option}',
            'Merchant\ProductController@delete_product_attribute');

        // product category filter
        Route::get('product/filter/{pro_id}', 'Merchant\ProductController@manage_filter');
        Route::post('product/filter/{pro_id}', 'Merchant\ProductController@update_filter');

        // product manage service
        Route::get('/product/manage_service/{mer_id}/{pro_id}', 'Merchant\ProductController@product_manage_service');
        Route::post('/product/service/{pro_id}/create', 'Merchant\ProductController@create_service_store');
        Route::post('/product/service/{store_service_schedule_id}/{pro_id}/add',
            'Merchant\ProductController@add_service_submit');
        Route::post('/product/service/{store_service_schedule_id}/{pro_id}/edit',
            'Merchant\ProductController@edit_service_submit');
        Route::get('/service/manage', 'Merchant\ServiceController@manage_service');
        Route::get('/service/reschedule/{member_service_schedule_id}',
            'Merchant\ServiceController@merchantRescheduleService')->name('merchant-reschedule-service');
        Route::post('/service/reschedule/{member_service_schedule_id}',
            'Merchant\ServiceController@merchantScheduleService_submit')->name('merchant-schedule-service-submit');
        Route::get('/service/confirm/{member_service_schedule_id}/{type}',
            'Merchant\ServiceController@merchantScheduleService_update_status')->name('merchant-schedule-service-update-status');
        Route::get('/service/view/{parent_order_id}',
            'Merchant\ServiceController@viewService')->name('merchant-view-schedule-service-list');

        // Negiotiation
        Route::get('/negotiation/manage', 'Merchant\NegotiationController@manage')->name('merchant.negotiation.manage');
        Route::get('/negotiation/manage/{negotiation}', 'Merchant\NegotiationController@view')->name('merchant.negotiation.view');
        Route::get('/negotiation/accept/{negotiation_id}', 'Merchant\NegotiationController@accept')->name('merchant.negotiation.accept');
        Route::get('/negotiation/decline/{negotiation_id}', 'Merchant\NegotiationController@decline')->name('merchant.negotiation.decline');
        Route::post('/negotiation/offer/{negotiation_id}', 'Merchant\NegotiationController@offer')->name('merchant.negotiation.offer');

        Route::get('/negotiation/{negotiation}/approval/{status}', 'Merchant\NegotiationController@approval')->name('merchant.negotiation.approval');

        Route::get('store/user/manage', 'Merchant\StoreUserController@manage');
        Route::get('store/user/add', 'Merchant\StoreUserController@add');
        Route::post('store/user/check', 'Merchant\StoreUserController@check');
        Route::post('store/user/add', 'Merchant\StoreUserController@add_submit');
        Route::get('store/user/permission/{user_id}', 'Merchant\StoreUserController@store_permission');
        Route::get('store/user/status/{user_id}/{status}', 'Merchant\StoreUserController@toggle_user_status');
        Route::get('store/user/edit/{user_id}', 'Merchant\StoreUserController@edit');
        Route::post('store/user/edit', 'Merchant\StoreUserController@edit_submit');
        Route::post('store/user/reset_password', 'Merchant\StoreUserController@reset_password');

        //export
        // Route::get('export/product_orders/{operation}', 'Merchant\ExportController@product_orders');
        Route::get('export/order_offline', 'Merchant\ExportController@order_offline');

        // chat
        Route::get('chat', 'Merchant\ChatController@list')->name('merchant-chat-list');
        Route::get('chat/{customer_id}/{product_id}',
            'Merchant\ChatController@conversation')->name('merchant-chat-conversation');
        Route::post('chat/{customer_id}/{product_id}',
            'Merchant\ChatController@postMessage')->name('merchant-chat-post-message');
        Route::post('chat/messages-read/{customer_id}/{product_id}',
            'ChatController@messagesRead')->name('merchant-chat-messages-read');

        // promotion
        Route::get('promotion/manage', 'Merchant\PromotionController@manage');
        Route::get('promotion/add', 'Merchant\PromotionController@add');
        Route::post('promotion/add', 'Merchant\PromotionController@add_submit');
        Route::get('promotion/submit/{promotion_id}', 'Merchant\PromotionController@submit');
        Route::get('promotion/edit/{promotion_id}', 'Merchant\PromotionController@edit');
        Route::post('promotion/edit/{promotion_id}', 'Merchant\PromotionController@edit_submit');
        Route::get('promotion/cancel/{promotion_id}', 'Merchant\PromotionController@cancel');
        Route::get('promotion/view/{promotion_id}', 'Merchant\PromotionController@view');

        // Retailer
        Route::get('retailer', 'Merchant\DistributorController@index')->name('retailer-list');
        Route::get('retailer/{status}/{merchantId}', 'Merchant\DistributorController@setStatus')->name('retailer-change-status');

        // calendar
        Route::get('calendar/store/view', 'Merchant\CalendarController@viewStoreCalendar');
        Route::get('calendar/offday', 'Merchant\CalendarController@viewMerchantCalendar');
        Route::post('calendar/offday/add', 'Merchant\CalendarController@addMerchantOffday');
        Route::get('calendar/offday/remove/{id}', 'Merchant\CalendarController@removeMerchantOffday');
        Route::post('calendar/offday/store/add', 'Merchant\CalendarController@addStoreOffday');
        Route::get('calendar/offday/store/remove/{id}', 'Merchant\CalendarController@removeStoreOffday');
        Route::post('calendar/offday/whitelist', 'Merchant\CalendarController@whitelistHoliday');
        Route::get('calendar/offday/whitelist/remove/{id}', 'Merchant\CalendarController@removeWhitelistedHoliday');
        Route::get('calendar/offday/check', 'Merchant\CalendarController@checkHoliday');

        Route::group(['prefix' => 'complains', 'as' => 'merchant.complain::'], function () {
            Route::match(['GET','POST'], '/', 'Merchant\ComplainController@index')->name('index');
            Route::delete('/messages/{complain}', 'Merchant\ComplainController@postDeleteMessages')->name('post-delete-messages');
            Route::get('/messages/{complain}', 'Merchant\ComplainController@messages')->name('messages');
            Route::post('/{order}/messages/{complain}', 'Merchant\ComplainController@postMessages')->name('post-messages');
            Route::post('/{order}/escalate/{complain}', 'Merchant\ComplainController@postEscalate')->name('post-escalate');
        });
    });
});
