<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('track','Tracking\TrackingController@tracking_waybill');

Route::auth();

// BroadcastRoute
Route::post('broadcasting/auth', 'BroadcastController@authenticate');
// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');
$this->post('password/phone', 'Auth\ForgotPasswordController@sendResetLinkPhone');
$this->get('password/phone/reset/{phone}/{token}', 'Auth\ResetPasswordController@showResetPhoneForm');
$this->post('password/phone/reset/{phone}', 'Auth\ResetPasswordController@resetPhone');
$this->get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password_reset_email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password-reset');
Route::post('password_reset_email_submit', 'Auth\ForgotPasswordController@sendResetLinkEmail');

Route::get('resend/activation/{cus_id}/{cus_email}', 'Auth\LoginController@resendActivation');
Route::any('resend/activation/{cus_id}', ['uses' =>'Auth\LoginController@resendActivationForm', 'middleware' => 'memberThrottle.throttle:5,1']);

Route::get('', 'Front\HomeController@index')->name('home');
Route::get('home', 'Front\HomeController@index');
Route::get('about', 'Front\HomeController@about_us')->name('about-us');
Route::any('contact', 'Front\HomeController@contactUs')->name('contact-us');

Route::get('category/{url_slug}', 'Front\ProductController@getCategoryView');
Route::get('product/preview/{url_slug}', 'Front\ProductController@getProductView')->name('product-preview');
Route::get('product/{url_slug}', 'Front\ProductController@getProductView')->name('product');
Route::get('product/wholesale/{url_slug}', 'Front\ProductController@getProductView')->name('product-wholesale');

Route::get('featured-products', 'Front\HomeController@getFeaturedProduct');
Route::get('trending-products', 'Front\HomeController@getTrendingProduct');
Route::get('flashsale-products', 'Front\HomeController@getFlashSaleProduct');
Route::get('featured-stores', 'Front\HomeController@getFeaturedStores');
Route::get('featured-categories', 'Front\HomeController@getFeaturedCategories');
Route::get('official-brands', 'Front\HomeController@getOfficialBrands')->name('official-brands');

Route::group(['prefix' => 'official-brand'], function() {
    Route::get('{brand_slug}', 'Front\OfficialBrandController@view');

    Route::get('{brand_slug}/latest-products', 'Front\OfficialBrandController@latestProducts');
    Route::get('{brand_slug}/most-popular', 'Front\OfficialBrandController@mostPopular');
});

Route::group(['prefix' => 'store'], function() {

    Route::get('profile/{stor_slug}','Front\StoreController@profile')->name('store.profile');

    Route::get('{stor_slug}/latest-products', 'Front\StoreController@latestProducts');
    Route::get('{stor_slug}/most-popular', 'Front\StoreController@mostPopular');
    Route::get('{stor_slug}','Front\StoreController@view')->name('store.product');
});

/* News */
Route::group(['prefix' => 'news', 'namespace' => 'Front'], function() {
    Route::get('load-more', 'NewsController@ajaxLoadMore');
    Route::get('/', 'NewsController@list')->name('news_promotions');
    Route::get('{news}', 'NewsController@detail');
});

Route::group(['prefix' => 'merchants'], function() {
    Route::get('distributors', 'Front\MerchantController@distributors');
    Route::get('distributors/products', 'Front\MerchantController@products');
    Route::post('distributors/{id}/request', 'Front\MerchantController@requestRetailer');
    Route::get('{id}', 'Front\MerchantController@view')->name('merchant.product');
    Route::get('profile/{id}', 'Front\MerchantController@profile')->name('merchant.profile');
});

Route::group(array(), function()
{
    // Route::get('', 'Front\ProfileController@accountInfo')->middleware('auth:web');

    //Route::any('contact-us', 'Front\HomeController@contactUs');
    Route::any('info/{url_slug}', 'Front\HomeController@cms');
    Route::any('search', 'Front\HomeController@search');

    Route::get('email/verification/{token}', 'Front\ProfileController@verifyEmail')->name('email.verify');

    /* Home Product Page */
    // Route::get('products', 'Front\ProductController@all');
    // Route::get('products/{id}', 'Front\ProductController@category');
    // Route::get('products/category/{parent}', 'Front\ProductController@category');
    // Route::get('products/category/{parent}/{child}', 'Front\ProductController@category');
    // Route::get('products/detail/{id}', 'Front\ProductController@detail');

    Route::group(['middleware' => 'auth:web'], function() {
        /* Cart */
        Route::any('cart', 'Front\CartController@cart')->name('cart');
        Route::any('wholesale/cart', 'Front\CartController@cart')->name('wholesale-cart');
        Route::post('cart/add', 'Front\CartController@add');
        Route::any('cart/delete', 'Front\CartController@delete');
        Route::any('cart/update', 'Front\CartController@update');
        Route::any('cart/checkout', 'Front\CartController@checkout')->name('checkout');
        Route::any('wholesale/cart/checkout', 'Front\CartController@checkout')->name('wholesale-checkout');
        Route::get('cart/shippingaddress/list', 'Front\ProfileController@shippingAddress');
        Route::get('cart/payment', 'Front\CartController@payment');

        Route::post('negotiate', 'Front\CartController@nego')->name('new_negotiation');

        /*Account*/
        Route::group(['prefix' => 'account'], function() {

            // General
            Route::get('general/overview', 'Front\AccountController@overview')->name('overview');
            Route::get('general/notification', 'Front\AccountController@notification')->name('notification');
            Route::get('general/chat', 'Front\ChatController@list')->name('chat-list');
            Route::get('general/chat/{product_id}', 'Front\ChatController@conversation')->name('chat-conversation');
            Route::post('general/chat/{store_id}/{product_id}', 'Front\ChatController@postMessage')->name('chat-post-message');
            Route::post('general/chat/messages-read/{store_id}/{product_id}', 'ChatController@messagesRead')->name('chat-messages-read');

            Route::get('general/favourite', 'Front\AccountController@favourite')->name('favourite');

            Route::get('general/automobile', 'Front\AccountController@automobile')->name('automobile');
            Route::post('general/automobile/add', 'Front\AccountController@addAutomobile');
            Route::post('general/automobile/edit', 'Front\AccountController@editAutomobile');
            Route::get('general/automobile/setdefault/{vehicle_id}', 'Front\AccountController@setDefaultAutomobile');
            Route::get('general/automobile/delete/{vehicle_id}', 'Front\AccountController@deleteAutomobile');

            Route::get('general/order/history/{type?}', 'Front\AccountController@orderHistory')->name('order-history');
            Route::get('{order_id}', 'Front\AccountController@getData');
            Route::get('general/order/wholesale/history/{type?}', 'Front\AccountController@orderHistory')->name('order-history-wholesale');
            // Route::get('general/order/history', 'Front\AccountController@orderHistory')->name('order-history');
            Route::get('general/order/detail/{order_id}', 'Front\AccountController@orderDetail')->name('order-detail');
            Route::get('general/review/write/{type}/{order_id}', 'Front\AccountController@createReview')->name('review-create');
            Route::post('general/review/write/{type}/{order_id}', 'Front\AccountController@createReviewSubmit')->name('review-create-submit');

            Route::get('general/schedule/{type}', 'Front\AccountController@schedule');
            Route::get('general/schedule', 'Front\AccountController@schedule')->name('schedule');

            Route::get('general/negotiation/{type?}', 'Front\AccountController@negotiations')->name('nego-history');
            Route::get('general/negotiation/{negotiation}/detail', 'Front\NegotiationController@view')->name('nego-detail');
            Route::get('negotiation/{negotiation}/accept', 'Front\NegotiationController@accept')->name('member-nego-accept');
            Route::get('negotiation/{negotiation}/reject', 'Front\NegotiationController@reject')->name('member-nego-reject');
            Route::post('negotiation/{negotiation}/counter', 'Front\NegotiationController@counterOffer')->name('member-nego-counter');

            // Profile
            Route::get('profile/details', 'Front\AccountController@profileDetails')->name('profile');
            Route::get('profile/delivery', 'Front\AccountController@deliveryAddr')->name('delivery');
            Route::post('profile/delivery', 'Front\AccountController@deliveryAddr');
            Route::get('profile/delivery/setdefault/{ship_id}', 'Front\AccountController@setDefaultAddr');
            Route::get('profile/delivery/delete/{ship_id}', 'Front\AccountController@deleteAddr');
            Route::get('profile/payment', 'Front\AccountController@payment')->name('payment');
            Route::get('profile/details/edit', 'Front\AccountController@profileDetailsEdit')->name('profile_edit');
            Route::post('profile/details/edit', 'Front\AccountController@profileDetailsEdit');
            Route::post('profile/avatar', 'Front\AccountController@avatarUpload');

            // Setting
            Route::any('setting/password', 'Front\AccountController@changePassword')->name('change-password');
            Route::get('setting/notification', 'Front\AccountController@notificationSetting')->name('notification-setting');
            Route::get('setting/chat', 'Front\AccountController@chatSetting')->name('chat-setting');
            Route::get('setting/language', 'Front\AccountController@language')->name('language');

            // Support
            Route::get('support/help', 'Front\AccountController@help')->name('help');
            Route::get('support/faq', 'Front\AccountController@faq')->name('faq');

            Route::get('order_history/{order_id}/update_status', 'Front\OrderController@updateOrderStatus')->name('order-update-status');
            Route::get('account/order_history/order_details/{parent_order_id}', 'Front\OrderController@getOrderDetails')->name('get-order-details');
            Route::get('account/order_history/schedule_service/{parent_order_id}', 'Front\OrderController@memberScheduleService')->name('member-schedule-service');
            Route::post('account/order/schedule_service/{member_service_schedule_id}', 'Front\OrderController@memberScheduleService_submit')->name('member-schedule-service-submit');

            Route::get('account/schedule_history/schedule_service/{member_service_schedule_id}/{type}', 'Front\OrderController@memberScheduleService_update_status')->name('member-schedule-service-update-status');

            // Complain
            Route::group(['prefix' => 'complains', 'as' => 'complain::'], function() {
                Route::get('/{order}/create', 'Front\ComplainController@createComplain')->name('create-complain');
                Route::post('/{order}/create', 'Front\ComplainController@postCreateComplain')->name('post-create-complain');
                Route::get('/{order}/messages/{complain}', 'Front\ComplainController@complainMessage')->name('complain-message');
                Route::post('/{order}/messages/{complain}', 'Front\ComplainController@postComplainMessage')->name('post-complain-message');
                Route::post('/{order}/escalate/{complain}', 'Front\ComplainController@postEscalate')->name('post-escalate');
            });
        });


    });

        //-- Order --//
        // Route::get('account/order_history', 'Front\OrderController@orderHistory')->name('order-history');
        // Route::get('account/order_history/{order_id}/{status_type}/update_status', 'Front\OrderController@updateOrderStatus')->name('order-update-status');
        // Route::get('account/order_history/order_details/{parent_order_id}', 'Front\OrderController@getOrderDetails')->name('get-order-details');
        // Route::get('account/order_history/schedule_service/{parent_order_id}', 'Front\OrderController@memberScheduleService')->name('member-schedule-service');
        // Route::post('account/order/schedule_service/{member_service_schedule_id}', 'Front\OrderController@memberScheduleService_submit')->name('member-schedule-service-submit');
});
Route::get('coba', function () {
    return view('front.coba');
});

/* Set Lang|Country Locale */
Route::any('home/setlocale', 'Front\HomeController@setlocale');
Route::any('home/setcountry', 'Front\HomeController@setcountry');

Route::get('getDate', function() {
    $date = date('Y-m-d H:i:s');
    return json_encode(['dateData'=>$date]);
});

//Route::get('cron/dailycron', 'Cron\OrderProcessController@dailyCron');

Route::any('local-biz/detail/{id}', 'Front\LocalBizController@details');
Route::any('payment/md/notification', 'Front\MDPaymentController@notification');
Route::any('payment/md/status/{parent_order_id}', 'Front\MDPaymentController@transactionStatus');
Route::any('test', function () {
    return view('front.cart.payment', ['token' => '123', 'parent_order_id' => 'abc']);
});