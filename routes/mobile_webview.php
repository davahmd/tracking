<?php

/* News */
Route::group(['prefix' => 'mobile/news', 'namespace' => 'Mobile'], function() {
    Route::get('load-more', 'NewsController@ajaxLoadMore');
    Route::get('/', 'NewsController@list');
    Route::get('{news}', 'NewsController@detail');
});