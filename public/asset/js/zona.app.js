jQuery(document).ready(function($){
	
	var MqL = 992;
	//move nav element position according to window width
	moveNavigation();
	$(window).on('resize', function(){
		(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
	});
	
	var Overlay = document.createElement('div');
	Overlay.className = 'nav-backdrop';
	document.getElementsByTagName('body')[0].appendChild(Overlay);
	
	function checkWindowWidth() {
		//check window width (scrollbar included)
		var e = window, 
            a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if ( e[ a+'Width' ] >= MqL ) {
			return true;
		} else {
			return false;
		}
	}
	
	function moveNavigation(){
		var navigation = $('#navb');
  		var desktop = checkWindowWidth();
        if ( desktop ) {
			navigation.detach();
			navigation.insertAfter('.navbar-toggler');
		} else {
			navigation.detach();
			navigation.insertAfter('#header');
		}
	}
	
	$(".navbar-toggler").click(function(){
		$("#navb").addClass("visible");
		$(".nav-backdrop").addClass("visible");
		$("body").css({ 'overflow': "hidden" });
	});	
	$(".nav-backdrop").click(function(){
		$("#navb").removeClass("visible");
		$(".nav-backdrop").removeClass("visible");
		$("body").css({ 'overflow': "auto" });
	});
	
	$(".search-toggler").click(function(){
		$(".nav-others .form-inline").addClass("visible");
	});	
	$(".close-search").click(function(){
		$(".nav-others .form-inline").removeClass("visible");
	});
	$(document).mouseup(function (close_search){
		var container = $(".nav-others .form-inline");
		if (!container.is(close_search.target) && container.has(close_search.target).length === 0){
			container.removeClass('visible');
		}
	});
	
	$('li.dropdown.category-expand a').click(function(){
		$(this).parent().toggleClass('show');
		$(this).siblings(".dropdown-menu").toggleClass('show');
	});
	$('body').on('click', function (e) {
		if (!$('li.dropdown.category-expand').is(e.target) 
			&& $('li.dropdown.category-expand').has(e.target).length === 0 
			&& $('.show').has(e.target).length === 0
		) {
			$("li.dropdown.category-expand").removeClass('show');
			$("li.dropdown.category-expand .dropdown-menu").removeClass('show');
		}
	});
	$('.menu-back-toggler').click(function(){
		$(this).parent().removeClass('show');
		$("li.dropdown.category-expand").removeClass('show');
	});
	
	$(window).resize(function(){
		if($(window).width() >= 993){
            
		} else {
            $('.wide-menu .box1 > a').click(function(){
                $('.wide-menu .box1').removeClass('show');
                $(this).parent().addClass('show');
                return false;
            });
            $('.sidebar-container .title-box').click(function(){
                $(this).next(".menu-box").slideToggle();
                return false;
            });
        }
	});
	var desktop = checkWindowWidth();
	if ( desktop ) {
		
	} else {
		$('.wide-menu .box1 > a').click(function(){
			$('.wide-menu .box1').removeClass('show');
			$(this).parent().addClass('show');
			return false;
		});
		$('.sidebar-container .title-box').click(function(){
			$(this).next(".menu-box").slideToggle();
			return false;
		});
    }
	
	
	$(document).scroll(function() {
		var y = $(this).scrollTop();
		if (y > 150) {
			$('.MainHeader').addClass("scrolled");
		} else {
			$('.MainHeader').removeClass("scrolled");
		}
	});
	
	 $(document).ready(function(){
		$(".list-general").click(function(){
			$(".collapse-general").collapse('show');
			$(".collapse-profile").collapse('hide');
			$(".collapse-setting").collapse('hide');
			$(".collapse-support").collapse('hide');
		});
		$(".list-profile").click(function(){
			$(".collapse-general").collapse('hide');
			$(".collapse-profile").collapse('show');
			$(".collapse-setting").collapse('hide');
			$(".collapse-support").collapse('hide');
		});
		$(".list-setting").click(function(){
			$(".collapse-general").collapse('hide');
			$(".collapse-profile").collapse('hide');
			$(".collapse-setting").collapse('show');
			$(".collapse-support").collapse('hide');
		});
		$(".list-support").click(function(){
			$(".collapse-general").collapse('hide');
			$(".collapse-profile").collapse('hide');
			$(".collapse-setting").collapse('hide');
			$(".collapse-support").collapse('show');
		});
	});
});

