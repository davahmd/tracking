$(function() {
    var $messageContainer = $('.message-container');
    var hasMorePage = $messageContainer.data('hasMorePage');
    var $btnLoadMore = $('.btn-load-more');
    var nextPageUrl = $messageContainer.data('nextPageUrl');
    var $btnSend = $('.btn-send');
    var $message = $('#message');
    var $feedActivityList = $('.feed-activity-list');
    var $messageChat = $('.message-chat');
    var $scrollable = $('.scrollable');

    $scrollable.animate({ scrollTop:  $messageChat.last().offset().top - 50 }, 'slow');

    hasMorePage ? $btnLoadMore.removeClass('d-none') : $btnLoadMore.addClass('d-none');

    $btnLoadMore.click(function() {
        if (hasMorePage) {
            loadMorePage();
        }
    });

    $btnSend.click(function() {

        var $this = $(this);
        var postUrl = $this.data('url');
        var langSend = $this.data('langSend');

        if ($message.val().trim() === '') {
            alert(window.translations.message_validation_field);
            $message.focus();
            return;
        }

        $.ajax({
            method: 'POST',
            url: postUrl,
            dataType: 'json',
            data: {
                message: $message.val().trim()
            },
            beforeSend: function() {
                $message.prop('disabled', true);
                $this.prop('disabled', true);

                $this.html('<i class="fa fa-spin fa-spinner"></i>');

                $message.val('');
            }
        }).done(function(msg) {
            $feedActivityList.append(msg.view);

            // $('.scrollable').animate({ scrollTop:  $feedActivityList.find('.message-chat').last().offset().top }, 'slow');

        }).always(function() {
            $message.prop('disabled', false);
            $this.prop('disabled', false);

            $this.html(langSend);
        });
    });

    function loadMorePage() {

        $.ajax({
            method: 'GET',
            url: nextPageUrl,
            dataType: 'json',
            beforeSend: function() {
                $btnLoadMore.prop('disabled', true);
            }
        }).done(function(msg) {
            hasMorePage = !!msg.paginator.next_page_url;

            if (hasMorePage) {
                nextPageUrl = msg.paginator.next_page_url;
            } else {
                $btnLoadMore.addClass('d-none');
            }

            if (msg.view) {
                var firstElementChatRender = $messageContainer.find('.message-chat').first();

                $(msg.view).insertBefore(firstElementChatRender);
            }

        }).always(function() {
            $btnLoadMore.prop('disabled', false);
        });
    }
});