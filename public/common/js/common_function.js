$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    var pageInput = document.getElementById("pageno");
    if(pageInput)
    {
        var max = $(pageInput).attr('max');
        if(typeof max !== typeof undefined && max !== false)
        {
            $(pageInput).change(function(){
                var input = $(this).val();
                if(input > max){
                    $(this).val(max);
                }
            });
        }
    }
});

function rpFormat(currency_code, value) {
    return currency_code + ' ' + number_format(Math.ceil(value), 0, '', '.');
}

function number_format(number, decimals, dec_point, thousands_point) {

    if (number == null || !isFinite(number)) {
        throw new TypeError("number is not valid");
    }

    if (!decimals) {
        var len = number.toString().split('.').length;
        decimals = len > 1 ? len : 0;
    }

    if (!dec_point) {
        dec_point = '.';
    }

    if (!thousands_point) {
        thousands_point = ',';
    }

    number = parseFloat(number).toFixed(decimals);

    number = number.replace(".", dec_point);

    var splitNum = number.split(dec_point);
    splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
    number = splitNum.join(dec_point);

    return number;
}

function gotopage($page) {
    $val = $("#pageno").val();

    var href = window.location.href.substring(0, window.location.href.indexOf('?'));
    var qs = window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.length);
    var newParam = $page + '=' + $val;

    if (qs.indexOf($page + '=') == -1) {
        if (qs == '') {
            qs = '?'
        }
        else {
            qs = qs + '&'
        }
        qs = newParam;

    }
    else {
        var start = qs.indexOf($page + "=");
        var end = qs.indexOf("&", start);
        if (end == -1) {
            end = qs.length;
        }
        var curParam = qs.substring(start, end);
        qs = qs.replace(curParam, newParam);
    }
    window.location.replace(href + '?' + qs);
}

const scrollToBottom = (id) => {
    var element = document.getElementById(id);
    element.scrollTop = element.scrollHeight - element.clientHeight;
};

// chat module
const chatModule = {
    buildMessage (side, message) {
        message = message.replace(/[\r\n]/g, '<br>');

        return '<li class="chat-' + side + '"><div class="chat-inner"><p>' + message + '</p><span class="date">' + moment().format('D/M/YY h:mm A') + '</span></div></li>';
    },
    updateChatList (notification) {
        /* inner functions */
        // move message to the top of each respective container inner function
        let moveMessageToTop = (target, container) => {
            target.fadeOut(() => {
                target.detach();
                container.prepend(target);
                target.fadeIn();
            });
        };

        let updateMessageContent = (textTarget, owner, message, datetimeTarget) => {
            textTarget.fadeOut(() => {
                textTarget.html(owner ? owner + ': ' + message : message);
                textTarget.fadeIn();
            });

            datetimeTarget.fadeOut(() => {
                datetimeTarget.text(moment().format('D/M/YY h:mm A'));
                datetimeTarget.fadeIn();
            });
        };
        /* end inner functions */
        
        if (notification.type != "App\\Notifications\\NewMessage") {
            return;
        }

        // initializations
        let chat = notification.data.chat;
        let chatMsg = chat.message.length > 100 ? chat.message.substring(0, 100) + '...' : chat.message;
        chatMsg = chatMsg.replace(/[\r\n]/g, '<br>');
        let target = $('#chat_store_' + chat.store_id);
        let targetCus = $('#chat_store_' + chat.store_id + '_cus_' + chat.cus_id);
        let targetPro = $('#chat_store_' + chat.store_id + '_cus_' + chat.cus_id + '_pro_' + chat.pro_id);
        let targetChatText = target.find('.chat-content .chat-text:first');
        let targetChatDateTime = target.find('.chat-content .info:first');
        let targetProChatText = targetPro.find('.chat-content .chat-text:first');
        let targetProChatDateTime = targetPro.find('.chat-content .info:first');
        let name = chat.sender_type == 'U' ? notification.data.cus_name : notification.data.store_name;

        // update main chat item msg
        updateMessageContent(targetChatText, name, chatMsg, targetChatDateTime);
        // update prodct chat item msg
        updateMessageContent(targetProChatText, name, chatMsg, targetProChatDateTime);

        // move intended chat item to the top
        if (!targetPro.is(':first-child')) {
            let proContainer = $('#chat_store_' + chat.store_id + '_cus_' + chat.cus_id + '_pro');

            moveMessageToTop(targetPro, proContainer);
        }

        if (!targetCus.is(':first-child')) {
            let cusContainer = $('#chat_store_' + chat.store_id + '_cus');

            moveMessageToTop(targetCus, cusContainer);
        }

        if (!target.is(':first-child')) {
            let mainContainer = $('.chat-container');

            moveMessageToTop(target, mainContainer);
        }

        // red dot notifications
        target.find('.notice').eq(0).removeClass('hidden').removeClass('d-none');
        targetCus.find('.notice').eq(0).removeClass('hidden').removeClass('d-none');
        targetPro.find('.notice').eq(0).removeClass('hidden').removeClass('d-none');

    },
    customerOpenChat (e) {
        let element = $(e.currentTarget);
        element.find('.notice').addClass('d-none');
        let proPane = $('#chat_store_' + element.data('store') + '_cus_' + element.data('cus') + 'pro_' + element.data('pro'));
        if (proPane.find('.notice').length == proPane.find('.notice.d-none').length + 1) {
            proPane.find('.notice').addClass('d-none');
        }
        let storePane = $('#chat_store_' + element.data('store'));
        if (storePane.find('.notice').length == storePane.find('.notice.d-none').length + 1) {
            storePane.find('.notice').addClass('d-none');
        }
    },
    merchantOpenChat (e) {
        let element = $(e.currentTarget);
        element.find('.notice').addClass('hidden');
        let cusPane = $('#chat_store_' + element.data('store') + '_cus_' + element.data('cus'));
        if (cusPane.find('.notice').length == cusPane.find('.notice.hidden').length + 1) {
            cusPane.find('.notice').addClass('hidden');
        }
        let storePane = $('#chat_store_' + element.data('store'));
        if (storePane.find('.notice').length == storePane.find('.notice.hidden').length + 1) {
            storePane.find('.notice').addClass('hidden');
        }
    }
};
