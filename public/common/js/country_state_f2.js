/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_library();

function get_countries(country_input, country_id = null, state_input = null, state_id = null, city_input = null, city_id = null, subdistrict_input = null, subdistrict_id = null)
{
    country_id = !isNaN(parseInt(country_id))? parseInt(country_id) : 0;
    state_id = !isNaN(parseInt(state_id))? parseInt(state_id) : 0;

    $(country_input).html('<option value>'+window.translations.selectCountry+'</option>');
    $.each(myJson.country, function (index, value)
    {
        $(country_input).append($('<option>', {
            value: value.id,
            text: value.name,
            // text: (locale === 'cn') ? value.name_cn : value.name_en, //Update for multiple country name translation
        }));

        if(country_id > 0 && country_id == value.id)
        {
            $(country_input).val(value.id).change();
            if(state_id > 0 && state_input && myJson.country[value.id].states != null)
            {
                get_states(state_input, value.id, state_id);
                if(city_id > 0 && city_input && myJson.country[value.id].states[state_id].cities != null)
                {
                    get_cities(city_input, value.id, state_id, city_id);
                    if(subdistrict_id > 0 && subdistrict_input && myJson.country[value.id].states[state_id].cities[city_id].sub_districts != null)
                    {
                        get_subdistricts(subdistrict_input, value.id, state_id, city_id, subdistrict_id);
                    }
                }
            }
        }
    });
}

function get_states(state_input, country_id, state_id = null)
{
    country_id = !isNaN(parseInt(country_id))? parseInt(country_id) : 0;
    state_id = !isNaN(parseInt(state_id))? parseInt(state_id) : 0;

    if(myJson.country[country_id] == null)
    {
        $(state_input).html('<option value>'+window.translations.selectCountryFirst+'</option>');
        return;
    }

    $(state_input).html('<option value>'+window.translations.selectState+'</option>');
    var states = myJson.country[country_id].states;
    $.each(states, function (index, value)
    {
        $(state_input).append($('<option>', {
            value: value.id,
            text: value.name,
        }));

        if(state_input && state_id == value.id)
        {
            $(state_input).val(value.id).change();
        }
    });
}

function get_cities(city_input, country_id, state_id, city_id = null)
{
    country_id = !isNaN(parseInt(country_id))? parseInt(country_id) : 0;
    state_id = !isNaN(parseInt(state_id))? parseInt(state_id) : 0;
    city_id = !isNaN(parseInt(city_id))? parseInt(city_id) : 0;

    if(myJson.country[country_id].states[state_id] == null)
    {
        $(city_input).html('<option value>'+window.translations.selectStateFirst+'</option>');
        return;
    }

    $(city_input).html('<option value>'+window.translations.selectCity+'</option>');
    var cities = myJson.country[country_id].states[state_id].cities;
    $.each(cities, function (index, value)
    {
        $(city_input).append($('<option>', {
            value: value.id,
            text: value.name,
        }));

        if(city_input && city_id == value.id)
        {
            $(city_input).val(value.id).change();
        }
    });
}

function get_subdistricts(subdistrict_input, country_id, state_id, city_id, subdistrict_id = null)
{
    country_id = !isNaN(parseInt(country_id))? parseInt(country_id) : 0;
    state_id = !isNaN(parseInt(state_id))? parseInt(state_id) : 0;
    city_id = !isNaN(parseInt(city_id))? parseInt(city_id) : 0;
    subdistrict_id = !isNaN(parseInt(subdistrict_id))? parseInt(subdistrict_id) : 0;

    if(myJson.country[country_id].states[state_id].cities[city_id] == null)
    {
        $(subdistrict_input).html('<option value>'+window.translations.selectCityFirst+'</option>');
        return;
    }

    $(subdistrict_input).html('<option value>'+window.translations.selectSubdistrict+'</option>');
    var sub_districts = myJson.country[country_id].states[state_id].cities[city_id].sub_districts;
    $.each(sub_districts, function (index, value)
    {
        $(subdistrict_input).append($('<option>', {
            value: value.id,
            text: value.name,
        }));

        if(subdistrict_input && subdistrict_id == value.id)
        {
            $(subdistrict_input).val(value.id).change();
        }
    });
}

function get_phoneAreacode(areacode_input, areacode = null)
{
    areacode = !isNaN(parseInt(areacode))? parseInt(areacode) : 0;

    $.each(myJson.country, function (index, value)
    {
        // var country_name = (locale === 'cn') ? value.name_cn : value.name_en; //Update for multiple country name translation
        $(areacode_input).append($('<option>', {
            value: value.phone_areacode,
            text: '+' + value.phone_areacode + ' - ' + value.name,
            // text: '+' + value.phone_areacode + ' - ' + country_name, //Update for multiple country name translation
        }));

        if(areacode > 0 && areacode == value.phone_areacode)
        {
            $(areacode_input).val(value.phone_areacode).change();
        }
        else
        {
            if(value.phone_areacode == 60) {
                $(areacode_input).val(value.phone_areacode).change();
            }
        }
    });
}

function get_library()
{
    var version = $('[name="country_state_lib_version"]').attr('content');
    var libPath = '/storage/country_state_lib' + version + '.js';
    $.ajax({
        url: libPath,
        type:'GET',
        error: function()
        {
            $.get("/generate/country_state").done(function() {
                location.reload();
            });
        }
    });

    document.write('<script type="text/javascript" src="'+libPath+'"></script>');
}