<?php

namespace App\Helpers;

use DateTime;
use DateTimeZone;

use Carbon\Carbon;

class DateTimeHelper
{
    static $system_timezone;
    static $operation_timezone;

    static function systemTimeZone()
    {
        if (empty(static::$operation_timezone)) {
            static::$operation_timezone = new DateTimeZone('UTC');
        }
        
        return static::$operation_timezone;
    }

    static function operationTimeZone()
    {
        if (empty(static::$operation_timezone)) {
            static::$operation_timezone = new DateTimeZone(config('app.timezone'));
        }

        return static::$operation_timezone;
    }

    static function getOperationDateTimeObj()
    {
        return new DateTime('now', static::operationTimeZone());
    }

    static function systemDateTime($output_format = "Y-m-d H:i:s")
    {
        return (new DateTime('now', static::systemTimeZone()))->format($output_format);
    }

    static function operationDateTime($output_format = "Y-m-d H:i:s")
    {
        return static::getOperationDateTimeObj()->format($output_format);
    }

    static function getSystemStartDateTime($str_input_datetime = null, $input_format = "Y-m-d H:i:s")
    {
        if ($str_input_datetime) {
            $source_datetime = DateTime::createFromFormat($input_format, $str_input_datetime, static::operationTimeZone());
        }
        else {
            // current datetime
            $source_datetime = static::getOperationDateTimeObj();
        }

        $source_datetime = $source_datetime->setTime(0, 0, 0);
        $source_datetime = $source_datetime->setTimeZone(static::systemTimeZone());

        return $source_datetime;
    }
    
    static function getSystemEndDateTime($str_input_datetime = null, $input_format = "Y-m-d H:i:s")
    {
        $source_datetime = static::getSystemStartDateTime($str_input_datetime, $input_format);
        $end_datetime = $source_datetime->modify('+1 day -1 second');

        return $end_datetime;
    }

    public static function systemToOperationDateTime($datetime, $format="Y-m-d H:i:s")
    {
        if ($datetime != false) {
            $system_timezone = 'UTC';
            $operation_timezone = config('app.timezone');
            return static::convertTimezone($datetime, $system_timezone, $operation_timezone, $format);
        }
        else {
            return "-";
        }
    }
    
    public static function operationToSystemDateTime($datetime, $format="Y-m-d H:i:s")
    {
        if ($datetime != false) {
            $system_timezone = 'UTC';
            $operation_timezone = config('app.timezone');
            return static::convertTimezone($datetime, $operation_timezone, $system_timezone, $format);
        }
        else {
            return "-";
        }
    }

    protected static function convertTimezone($datetime, $from_timezone, $to_timezone, $format="Y-m-d H:i:s")
    {
        $from_format = "Y-m-d H:i:s";
        $datetime = date($from_format, strtotime($datetime));
        $date = Carbon::createFromFormat($from_format, $datetime, $from_timezone);
        $date->setTimezone($to_timezone);
        return $date->format($format);
    }
}