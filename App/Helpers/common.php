<?php

use App\Models\Customer;
use App\Models\DeviceToken;
use App\Models\PersonalAccessToken;
use App\Services\Jwt\Token;
use App\Repositories\S3ClientRepo;

function apiResponse($status, $message)
{
    $payload = [
        'status' => $status
    ];

    if (is_array($message)) {
        $payload = array_merge($payload, $message);
    } else {
        $payload['message'] = $message;
    }

    return response()->json($payload);
}

function sanitizeApiRequest($request)
{
    if(isset($request['lang']))
    {
        $lang = strtolower($request['lang']);
        if(in_array($lang, ['en', 'cn']))
        {
            \App::setLocale($lang);
        }
    }

    unset($request['lang']);

    return $request;
}

function productImageLink($image, $merchant_id)
{
    if(!$image)
    {
        return url('common/images/stock.png');
    }

    if (str_contains($image, 'https://') || str_contains($image, 'http://'))
    {
        return $image;
    }

    return config('image.cloud_url') . "/product/$merchant_id/$image";
}

function convert_percentage($rate)
{
    return round($rate / 100, 2);
}

function stdObject($array)
{
    return json_decode(json_encode($array));
}

function rpFormat($value)
{
    return config('app.currency_code') . ' ' . number_format(ceil($value), 0, '', '.');
}

function auth_user_api_member()
{
    if (request()->bearerToken()) {
        $payload = (new Token)->retrievePayload(request()->bearerToken());

        return PersonalAccessToken::where('user_type', Customer::class)
            ->where('user_id', $payload->sub)
            ->find($payload->jti)
            ->user;
    }

    return null;
}

function uploadImage($file, $path, $fileName, $oldFileName = null)
{
    try
    {
        $originalFileName = explode('.', $file->getClientOriginalName());
        $fileExtension = end($originalFileName);
        $newFileName = "$fileName.$fileExtension";

        if(@file_get_contents($file) && !S3ClientRepo::IsExisted($path, $newFileName))
        {
            S3ClientRepo::Upload($path, $file, $newFileName);
        }

        if($oldFileName)
        {
            S3ClientRepo::Delete($path, $oldFileName);
        }

        return stdObject([
            'status' => true,
            'file_name' => $newFileName,
        ]);
    }
    catch (\Exception $e)
    {
        return stdObject([
            'status' => false
        ]);
    }
}