<?php

namespace App\Events;

use App\Models\Chat;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageCreated implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Chat
     */
    public $chat;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel("chat.{$this->chat->cus_id}.{$this->chat->store_id}.{$this->chat->pro_id}");
    }

    // public function broadcastWith()
    // {
    //     $send_chat = [
    //         'cus_id' => $this->chat->cus_id,
    //             'store_id' => $this->chat->store_id,
    //             'pro_id' => $this->chat->pro_id,
    //         'message' => $this->chat->message,
    //         'created_at' => $this->chat->created_at,
    //     ];

    //     return [
    //         // 'chat' => $this->chat
    //         'chat' => $send_chat
    //     ];
    // }
}
