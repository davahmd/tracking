<?php

namespace App\Services\Order;

use App\Services\Midtrans\MidtransService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class BaseProcessTransactionJob
{
    use Dispatchable, Queueable;

    public function handle()
    {
        $this->query()->chunk(200, function ($parentOrders) {
            foreach ($parentOrders as $parentOrder) {
                $midtransTransaction = (new MidtransService)->status($parentOrder->id);
                $orderService = new OrderService;

                try {
                    if (!$midtransTransaction || $this->midtransTransactionIsFailed($midtransTransaction)) {
                        $orderService->cancelByParentOrder($parentOrder, $midtransTransaction);
                    } else {
                        $orderService->ProcessOrder(
                            $parentOrder->id,
                            $orderService->convertMidtransStatus($midtransTransaction)
                        );
                    }
                } catch (Exception $exception) {
                    Log::error($exception);
                }
            }
        });
    }

    protected function midtransTransactionIsFailed($midtransTransaction)
    {
        return in_array($midtransTransaction->transaction_status, [
            'deny',
            'expire',
            'cancel'
        ]);
    }
}