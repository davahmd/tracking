<?php

namespace App\Services\Order;

use App\Models\PriceNegotiation;
use App\Services\Midtrans\MidtransService;
use Veritrans;
use App\Models\PgTransaction;
use App\Models\ParentOrder;
use DB;
use App\Models\StoreServiceExtra;
use App\Models\MemberServiceSchedule;
use App\Services\Notifier\Notifier;
use App\Models\Order;
use App\Models\Promotion;
use App\Models\PromotionProduct;

class OrderService
{
    public function ProcessOrder($parent_order_id, $payment_status)
    {
        $parentOrder = ParentOrder::find($parent_order_id);
        if(!$parentOrder)
        {
           return false;
        }
        try
        {
            DB::beginTransaction();
            $parentOrder->payment_status = $payment_status;
            $orders = $parentOrder->items;
            $order_item_status=0;
            //if payment_status = 1 (captured) update nm_order to 1; add service entry if any; send notification; email
            if($payment_status == 1 || $payment_status == 3) //RUH 19072019 check for settlement status
            {
                foreach($orders as $order)
                {
                    if(!empty($order->service_ids))
                    {
                        $ids = explode("|", $order->service_ids);
                        $order->services = StoreServiceExtra::whereIn('id',$ids)->get();
                        foreach($order->services as $service)
                        {
                            MemberServiceSchedule::create([
                                'cus_id' => $order->order_cus_id,
                                'order_id' => $order->order_id,
                                'stor_id' => $order->product->pro_sh_id,
                                'store_service_extra_id' => $service->id,
                                'service_name_current' => $service->service_name
                            ]);
                        }

                        $parentOrder->items()->where('order_id', $order->order_id)->update(['order_status'=> Order::STATUS_INSTALLATION]);
                    }
                    else {
                        $parentOrder->items()->where('order_id', $order->order_id)->update(['order_status'=> Order::STATUS_PROCESSING]);
                    }
                }

            }
            //if payment_status = 5,6, 7 (deny, expired, cancelled) update nm_order to 5; remove service entry if any; send notification; email
            elseif($payment_status == 5 || $payment_status == 6 || $payment_status == 7)
            {
                $parentOrder->items()->services()->update(['status'=> -1]);
                $parentOrder->items()->update(['order_status' => Order::STATUS_FAIL_OR_CANCELED]);
            }
            $parentOrder->save();
            // $parentOrder->items()->where('service_ids', '')->orWhereNull('service_ids')->update(['order_status'=> $order_item_status]);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return false;
        }
        DB::commit();
        //send notification
        try
        {
            if($payment_status==1 || $payment_status == 3) //RUH 19072019 check for settlement status
            {
                (new Notifier($orders))->send(Notifier::ORDER_PLACED);
            }
            elseif($payment_status==5 || $payment_status==6 || $payment_status==7)
            {
                (new Notifier($orders))->send(Notifier::ORDER_CANCELLED);
            }
        }
        catch(Exception $e)
        {

        }

        return true;

    }

    public function cancelByParentOrder(ParentOrder $parentOrder, $midtrans = false)
    {
        DB::transaction(function () use ($parentOrder, $midtrans) {

            $promo_id = 0;

            foreach ($parentOrder->items as $order) {
                if ($order->order_status != Order::STATUS_FAIL_OR_CANCELED) {
                    $this->returnProductQuantity($order);
                }

                $order->services()->update([
                    'status' => -1
                ]);

                if ($order->negotiation) {
                    $order->negotiation->update([
                        'status' => PriceNegotiation::STATUS['FAILED']
                    ]);
                }

                if ($order->promo_id) {
                    $promo_id = $order->promo_id;
                }

                $promotion_product = PromotionProduct::where('promotion_id', $order->flashsale_id)
                    ->where('product_id', $order->order_pro_id)
                    ->first();
                
                if ($promotion_product) {
                    $promotion_product->count = $promotion_product->count - $order->order_qty;
                    $promotion_product->timestamps = false;
                    $promotion_product->save();
                }
            }

            $promotion = Promotion::where('id', $promo_id)->first();

            if ($promotion) {
                $promotion->promo_code_count = $promotion->promo_code_count - 1;
                $promotion->save();
            }

            $parentOrder->items()->update([
                'order_status' => Order::STATUS_FAIL_OR_CANCELED
            ]);

            $parentOrder->update([
                'status' => 'C',
                'payment_status' => $this->convertMidtransStatus($midtrans)
            ]);
        });

        (new Notifier($parentOrder->items))->send(Notifier::ORDER_CANCELLED);
    }

    public function cancelByOrder(Order $order)
    {
        DB::transaction(function () use ($order) {
            $this->returnProductQuantity($order);
            $order->update(['order_status' => Order::STATUS_FAIL_OR_CANCELED]);
        });
    }

    protected function returnProductQuantity(Order $order)
    {
        $order->product()->increment('pro_qty', $order->order_qty);
        $order->pricing()->increment('quantity', $order->order_qty);

        $order->product->quantityLog()->create([
            'attributes' => $order->order_attributes,
            'credit' => $order->order_qty,
            'current_quantity' => $order->pricing->quantity + $order->order_qty,
            'remarks' => 'Order Canceled, Transaction ID : '.$order->transaction_id
        ]);
    }

    public function convertMidtransStatus($midtrans)
    {
        $status = ParentOrder::PAYMENT_STATUS_NOT_FOUND_OR_CANCELED_BY_SYSTEM;

        if ($midtrans) {
            if ($midtrans->transaction_status == 'capture') {
                if ($midtrans->payment_type == 'credit_card'){
                    if($midtrans->fraud_status == 'challenge'){
                        $status = 2;
                    }
                    else {
                        $status = 1;
                    }
                }
            }
            elseif ($midtrans->transaction_status == 'settlement'){
                $status = 3;
            }
            elseif($midtrans->transaction_status == 'pending'){
                $status = 4;
            }
            else if ($midtrans->transaction_status == 'deny') {
                $status = 5;
            }
            elseif ($midtrans->transaction_status == 'expire') {
                $status = 6;
            }
            elseif ($midtrans->transaction_status == 'cancel') {
                $status = 7;
            }
        }

        return $status;
    }
}