<?php

namespace App\Services\Chat;

use App\Models\Chat;

class ChatService
{
    public function getCustomerUnreadMessagesCount()
    {
        $cus_id = 0;
        try {
            if (auth()->check()) {
                $cus_id = auth()->id();
            }
            elseif (auth('api_members')->check()) {
                $cus_id = auth('api_members')->id();
            }
        }
        catch (\Exception $e) {
            return 0;
        }

        if ($cus_id == 0) return 0;

        return Chat::where([
            'cus_id' => $cus_id,
            'sender_type' => 'S',
            'read_at' => null,
        ])->count();
    }

    public function getMerchantUnreadMessagesCount()
    {
        $arr_store_ids = [];
        $user = auth('api')->user();
        try {
            if (auth('api')->check()) {
                $arr_store_ids = $user->merchant->stores->pluck('stor_id')->toArray();
            }
            elseif (auth('api_storeusers')->check()) {
                $arr_store_ids = $user->storeUser->stores()->select('store_id')->get()->toArray();
            }
        }
        catch (\Exception $e) {
            return 0;
        }

        if (count($arr_store_ids) == 0) return 0;

        return Chat::whereIn('store_id', $arr_store_ids)
            ->where([
                'sender_type' => 'U',
                'read_at' => null,
            ])->count();
    }
}