<?php

namespace App\Services\Midtrans;

use Veritrans;
use App\Models\PgTransaction;
use App\Models\ParentOrder;
use App\Services\Order\OrderService;

class MidtransService
{
    public function __construct()
    {
        $config = new \Veritrans_Config;
        $config::$isProduction = config('midtrans.is_production');
        $config::$serverKey = config('midtrans.server_key');
        $config::$is3ds = config('midtrans.is_3ds');
        $config::$isSanitized = config('midtrans.is_sanitized');
    }

    public function get_snapToken($parentOrder)
    {
        $transaction = array(
            'order_id' => $parentOrder->id,
            'gross_amount' => ceil($parentOrder->charged_amount) // no decimal allowed
        );

        $item_details = array();
        $totalShippingFee = 0;
        $totalServiceFee = 0;
        $totalPlatformFee = 0;

        foreach ($parentOrder->items as $item) {
            $itemdetail = array(
                'id' => $item->order_pricing_id,
                'price' => ceil($item->product_price),
                'quantity' => $item->order_qty,
                'name' => $item->product->pro_title_en
            );
            array_push($item_details, $itemdetail);

            if ($item->total_product_shipping_fees > 0) {
                $totalShippingFee += $item->total_product_shipping_fees;
            }

            if ($item->cus_service_charge_value > 0) {
                $totalServiceFee += $item->cus_service_charge_value;
            }

            if ($item->cus_platform_charge_value > 0) {
                $totalPlatformFee += $item->cus_platform_charge_value;
            }
        }

        if ($totalShippingFee > 0) {
            $shippingDetail = array(
                'id' => $parentOrder->id,
                'price' => ceil($totalShippingFee),
                'quantity' => 1,
                'name' => 'Shipping Fees'
            );
            array_push($item_details, $shippingDetail);
        }

        if ($totalServiceFee > 0) {
            $shippingDetail = array(
                'id' => $parentOrder->id,
                'price' => ceil($totalServiceFee),
                'quantity' => 1,
                'name' => trans('localize.gst')
            );
            array_push($item_details, $shippingDetail);
        }

        if ($totalPlatformFee > 0) {
            $shippingDetail = array(
                'id' => $parentOrder->id,
                'price' => ceil($totalPlatformFee),
                'quantity' => 1,
                'name' => trans('localize.platform_charges')
            );
            array_push($item_details, $shippingDetail);
        }

        $customer_details = array(
            'first_name' => $parentOrder->customer->cus_name,
            'email' => $parentOrder->customer->user->email,
            'phone' => $parentOrder->customer->cus_phone,
        );

        $params = array(
            'transaction_details' => $transaction,
            'item_details' => $item_details,
            'customer_details' => $customer_details,
            'enabled_payments' => config('midtrans.available_payment_method')
        );

        try {
            $payment = new \Veritrans_Snap;
            $token = $payment->getSnapToken($params);

            return $token;

        } catch (Exception $e) {
            return null;
        }
    }

    public function process_payment($data)
    {
        //check parent order existed, if not existed, ignore
        //$parentOrderExists = ParentOrder::where('id', $data->order_id)->exists();
        $parentOrder = ParentOrder::where('id',$data->order_id)->first();
        if(!$parentOrder)
        {
            return response()->json([
                'status' => false,
                'status_code' => 404,
                'message' => "Order does not exist"
            ]);
        }

        //check signature key match, if not match, ignore, SHA512(order_id + status_code + gross_amount + serverkey)
        $hashvalue = hash('sha512', $data->order_id.$data->status_code.$data->gross_amount.config('midtrans.server_key'));
        if($data->signature_key != $hashvalue)
        {
            return response()->json([
                'status' => false,
                'status_code' => 400,
                'message' => "signature key does not match"
            ]);
        }

        //check pg transaction existed with same status code, if not existed, add new entry; if existed, ignore.
        $transactionExists = PgTransaction::where('pg_transaction_id', $data->transaction_id)
        ->where('status_code', $data->status_code)
        ->exists();

        if($transactionExists)
        {
            return response()->json([
                'status' => false,
                'status_code' => 202,
                'message' => "transaction is already processed"
            ]);
        }

        //add new entry of pg transaction
        PgTransaction::create([
            'order_id' => $data->order_id,
            'pg_transaction_id' => $data->transaction_id,
            'gross_amount' => $data->gross_amount,
            'status_code' => trim($data->status_code),
            'status_message' => trim($data->status_message),
            'payment_type' => $data->payment_type,
            'transaction_time' => $data->transaction_time,
            'transaction_status' => trim($data->transaction_status),
            'fraud_status' => !empty($data->fraud_status) ? trim($data->fraud_status) : '-',
            'approval_code' => !empty($data->approval_code) ? trim($data->approval_code) : null,
            'masked_card' => !empty($data->approval_code) && !empty($data->masked_card) ? $data->masked_card : null,
            'bank' => !empty($data->bank) ? $data->bank : null,
            'card_type' => !empty($data->card_type) ? trim($data->card_type) : null,
            'bill_key' => !empty($data->bill_key) ? trim($data->bill_key) : null,
            'biller_code' => !empty($data->biller_code) ? trim($data->biller_code) : null,
        ]);

        //process order
        $parent_order_payment_status = 0;
        $message = '';
        if ($data->transaction_status == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($data->payment_type == 'credit_card'){
                if($data->fraud_status == 'challenge'){
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    $message = "Transaction order_id: " . $data->order_id ." is challenged by FDS";
                    $parent_order_payment_status = 2;
                }
                else {
                    // TODO set payment status in merchant's database to 'Success'
                    $message = "Transaction order_id: " . $data->order_id ." successfully captured using " . $data->payment_type;
                    $parent_order_payment_status = 1;
                }
            }
        }
        else if ($data->transaction_status == 'settlement'){
            // TODO set payment status in merchant's database to 'Settlement'
            $message = "Transaction order_id: " . $data->order_id ." successfully transfered using " . $data->payment_type;
            $parent_order_payment_status = 3;
        }
        else if($data->transaction_status == 'pending'){
            // TODO set payment status in merchant's database to 'Pending'
            $message = "Waiting customer to finish transaction order_id: " . $data->order_id . " using " . $data->payment_type;
            $parent_order_payment_status = 4;
        }
        else if ($data->transaction_status == 'deny') {
            // TODO set payment status in merchant's database to 'Denied'
            $message = "Payment using " . $data->payment_type . " for transaction order_id: " . $data->order_id . " is denied.";
            $parent_order_payment_status = 5;
        }
        else if ($data->transaction_status == 'expire') {
            // TODO set payment status in merchant's database to 'expire'
            $message = "Payment using " . $data->payment_type . " for transaction order_id: " . $data->order_id . " is expired.";
            $parent_order_payment_status = 6;
        }
        else if ($data->transaction_status == 'cancel') {
            // TODO set payment status in merchant's database to 'Denied'
            $message = "Payment using " . $data->payment_type . " for transaction order_id: " . $data->order_id . " is canceled.";
            $parent_order_payment_status = 7;
        }

        $result = (new OrderService)->ProcessOrder($parentOrder->id, $parent_order_payment_status);

        return response()->json([
            'status' => $result,
            'status_code' => 200,
            'message' => $message
        ]);
    }

    /**
     * Retrieve transaction status
     * @param string $id Order ID or transaction ID
     * @return mixed[]
     */
    public function status($id)
    {
        try
        {
            return \Veritrans_ApiRequestor::get(
                \Veritrans_Config::getBaseUrl() . '/' . $id . '/status',
                \Veritrans_Config::$serverKey,
                false
            );
        }
        catch(\Exception  $e)
        {
            return false;
        }
    }

    /**
     * Approve challenge transaction
     * @param string $id Order ID or transaction ID
     * @return string
     */
    public function approve($id)
    {
        return \Veritrans_ApiRequestor::post(
            \Veritrans_Config::getBaseUrl() . '/' . $id . '/approve',
            \Veritrans_Config::$serverKey,
            false
        );
    }

    /**
     * Cancel transaction before it's settled
     * @param string $id Order ID or transaction ID
     * @return string
     */
    public function cancel($id)
    {
        return \Veritrans_ApiRequestor::post(
            \Veritrans_Config::getBaseUrl() . '/' . $id . '/cancel',
            \Veritrans_Config::$serverKey,
            false
        );
    }

    /**
     * Expire transaction before it's setteled
     * @param string $id Order ID or transaction ID
     * @return mixed[]
     */
    public function expire($id)
    {
        return \Veritrans_ApiRequestor::post(
            \Veritrans_Config::getBaseUrl() . '/' . $id . '/expire',
            \Veritrans_Config::$serverKey,
            false
        );
    }
}
