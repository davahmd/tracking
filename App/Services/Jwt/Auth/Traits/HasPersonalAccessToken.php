<?php

namespace App\Services\Jwt\Auth\Traits;

use App\Models\PersonalAccessToken;
use App\Services\Jwt\Token;
use Carbon\Carbon;

trait HasPersonalAccessToken
{
    public function accessTokens()
    {
        return $this->morphMany(PersonalAccessToken::class, 'user');
    }

    public function generateToken()
    {
        $personalAccessToken = $this->accessTokens()->create([
            'expires_in' => Carbon::now()->addYear()->toDateTimeString()
        ]);

        $token = (new Token)->generateAccessToken($personalAccessToken);

        return $token->getAccessToken();
    }

    public function destroyToken()
    {
        $payload = (new Token)->retrievePayload(request()->bearerToken());

        $this->accessTokens()->where('id', $payload->jti)->delete();
    }
}