<?php

namespace App\Services\Jwt\Auth;

use App\Models\PersonalAccessToken;
use App\Services\Jwt\Token;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class JwtGuard implements Guard
{
    use GuardHelpers;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    protected $type = [];

    /**
     * Create a new authentication guard.
     *
     * @param  \Illuminate\Contracts\Auth\UserProvider $provider
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (!is_null($this->user)) {
            return $this->user;
        }

        $user = null;

        $token = $this->request->bearerToken() ?: $this->request->token;

        if (!empty($token)) {
            if ($payload = $this->decodeJwtToken($token)) {
                $user = $this->getUserFromCredentials($payload);
            }
        }

        return $this->user = $user;
    }

    /**
     * Decode Jwt
     *
     * @param $token
     * @return array|null
     */
    protected function decodeJwtToken($token)
    {
        try {
            $payload = (new Token())->retrievePayload($token);
        } catch (Exception $exception) {
            if ($exception instanceof ExpiredException) {
                $this->sendExpiredTokenResponse();
            }

            return null;
        }

        return (array)$payload;
    }

    protected function sendExpiredTokenResponse()
    {
        response()->json([
            'status' => 'fail',
            'message' => 'expired token.'
        ])->throwResponse();
    }

    /**
     * @param $payload
     * @return null|Response
     * @throws Exception
     */
    protected function getUserFromCredentials($payload)
    {
        $query = (new PersonalAccessToken)->newQuery();

        foreach ($payload as $key => $value) {
            if ($key == 'sub') {
                $query->where('user_id', $value);
            }
            if ($key == 'iat') {
                $query->where('created_at', Carbon::createFromTimestamp($value));
            }
            if ($key == 'exp') {
                $query->where('expires_in', Carbon::createFromTimestamp($value));
            }
            if ($key == 'jti') {
                $query->where('id', $value);
            }
            if ($key == 'type') {

                if (!in_array($value, $this->type)) {
                    return null;
                }

                $query->where('user_type', $this->getFullNameSpace($value));
            }
        }

        $accessToken = $query->first();

        if (empty($accessToken)) {
            return null;
        }

        return $accessToken->user;
    }

    /**
     * @param $value
     * @return string
     */
    protected function getFullNameSpace($value)
    {
        return 'App\\Models\\' . ucfirst(camel_case($value));
    }

    /**
     * Validate a user's credentials.
     *
     * @param  array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        $token = $this->request->bearerToken();

        if (!empty($token)) {
            if ($payload = $this->decodeJwtToken($token)) {
                if ($this->getUserFromCredentials($payload)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Set the current request instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }
}
