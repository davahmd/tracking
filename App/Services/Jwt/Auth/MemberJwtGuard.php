<?php

namespace App\Services\Jwt\Auth;

class MemberJwtGuard extends JwtGuard
{
    protected $type = ['customer'];
}