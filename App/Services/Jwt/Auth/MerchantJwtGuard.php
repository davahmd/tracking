<?php

namespace App\Services\Jwt\Auth;

class MerchantJwtGuard extends JwtGuard
{
    protected $type = ['merchant', 'store-user'];
}