<?php

namespace App\Services\Jwt;

use App\Models\PersonalAccessToken;
use App\Models\RefreshToken;
use Carbon\Carbon;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\Model;

class Token
{
    protected $accessToken;
    protected $refreshToken;

    protected function getSecretKey()
    {
        return config('jwt.secret_key');
    }

    public function getAccessTokenTtl()
    {
        return config('jwt.access_token_ttl');
    }

    public function getRefreshTokenTtl()
    {
        return config('jwt.refresh_token_ttl');
    }

    public function generateAccessToken(PersonalAccessToken $personalAccessToken)
    {
        $this->setAccessToken($personalAccessToken);

        return $this;
    }

    public function retrievePayload($token)
    {
        try {
            $payload = JWT::decode($token, $this->getSecretKey(), ['HS256']);
        } catch (Exception $exception) {
            $this->invalidTokenResponse();
        }

        $this->validatePayload($payload);

        return $payload;
    }

    public function generateIdentifier()
    {
        return bin2hex(random_bytes(40));
    }

    protected function getExpireAt()
    {
        return Carbon::now()->addMinutes($this->getAccessTokenTtl())->toDateTimeString();
    }

    protected function getRefreshTokenExpireAt()
    {
        return Carbon::now()->addMinutes($this->getRefreshTokenTtl())->toDateTimeString();
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    protected function setAccessToken(PersonalAccessToken $personalAccessToken)
    {
        $this->accessToken = JWT::encode([
            'sub' => $personalAccessToken->user_id,
            'iat' => $personalAccessToken->created_at->timestamp,
            'nbf' => $personalAccessToken->created_at->timestamp,
            'exp' => $personalAccessToken->expires_in->timestamp,
            'jti' => $personalAccessToken->id,
            'type' => kebab_case(class_basename($personalAccessToken->user_type))
        ], $this->getSecretKey());

        //$this->setRefreshToken($accessToken);
    }

    protected function setRefreshToken($accessToken)
    {
        $refreshToken = RefreshToken::forceCreate([
            'id' => $this->generateIdentifier(),
            'access_token_id' => $accessToken->id,
            'expires_at' => $this->getRefreshTokenExpireAt()
        ]);

        $this->refreshToken = JWT::encode([
            'sub' => $accessToken->id,
            'iat' => $accessToken->created_at->timestamp,
            'nbf' => $accessToken->created_at->timestamp,
            'exp' => $refreshToken->expires_at->timestamp,
            'jti' => $refreshToken->id
        ], $this->getSecretKey());
    }

    protected function validatePayload($payload)
    {
        if (property_exists($payload, 'sub') &&
            property_exists($payload, 'iat') &&
            property_exists($payload, 'nbf') &&
            property_exists($payload, 'exp') &&
            property_exists($payload, 'jti') &&
            property_exists($payload, 'type')) {
            return true;
        }

        $this->invalidTokenResponse();
    }

    public function invalidTokenResponse()
    {
        response()->json('Unauthorized.', 401)->throwResponse();
    }

    public function invalidSignatureResponse()
    {
        response()->json([
            'status' => '422',
            'message' => 'signature verification failed'
        ])->throwResponse();
    }

    public function expiredExceptionResponse()
    {
        response()->json([
            'status' => '401',
            'message' => 'refresh token expired'
        ])->throwResponse();
    }
}
