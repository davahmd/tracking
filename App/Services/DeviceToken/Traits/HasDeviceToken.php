<?php

namespace App\Services\DeviceToken\Traits;

use App\Models\DeviceToken;

trait HasDeviceToken
{
    public function deviceTokens()
    {
        return $this->morphMany(DeviceToken::class, 'user');
    }

    public function distinctDeviceToken()
    {
        return $this->deviceTokens()->distinct();
    }

    public function routeNotificationForFcm()
    {
        return $this->distinctDeviceToken()
            ->pluck('token')
            ->toArray();
    }
}