<?php

namespace App\Services\Notifier;

use App\Models\Customer;
use App\Models\MemberServiceSchedule;
use App\Models\Merchant;
use App\Models\Order;
use App\Models\PriceNegotiation;
use App\Models\Store;
use App\Models\StoreUser;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class Notifier
{
    // Transactions
    const ORDER_CANCELLED = 'OrderCancelled';
    const ORDER_COMPLETED = 'OrderCompleted';
    const ORDER_PLACED = 'OrderPlaced';
    const ORDER_SHIPPED = 'OrderShipped';
    const ORDER_RECEIVED = 'OrderReceived';

    // Services
    const SERVICE_CANCELLED = 'ServiceCancelled';
    const SERVICE_RESCHEDULED = 'ServiceRescheduled';
    const SERVICE_SCHEDULE_RECEIVED = 'ServiceScheduleReceived';
    const SERVICE_SCHEDULE_REMINDER = 'ServiceScheduleReminder';
    const SERVICE_SCHEDULE_CONFIRM = 'ServiceConfirmed';

    // Price Negotiation
    const PRICE_NEGOTIATION_RECEIVED = 'PriceNegotiationReceived';
    const PRICE_NEGOTIATION_COUNTER_OFFER = 'PriceNegotiationCounterOffer';
    const PRICE_NEGOTIATION_ACCEPTED = 'PriceNegotiationAccepted';
    const PRICE_NEGOTIATION_REJECTED = 'PriceNegotiationRejected';
    const PRICE_NEGOTIATION_EXPIRED = 'PriceNegotiationExpired';
    const PRICE_NEGOTIATION_FAILED = 'PriceNegotiationFailed';

    protected $data;

    public function __construct($data)
    {
        $this->validate($data);
    }

    /**
     * @param $data
     * @throws Exception
     */
    protected function validate($data)
    {
        if ($this->isInstanceOfCollection($data)) {
            if ($this->isEmptyCollection($data) || $this->isInstanceOfOrderOrMemberServiceSchedule($data->first())) {
                $this->data = $data;
            } else {
                $exception = new Exception("All data in collection must be instance of Order or MemberServiceSchedule.");
                Log::error($exception);

                throw $exception;
            }
        } elseif ($this->isInstanceOfOrderOrMemberServiceSchedule($data)) {
            $this->data = (new Collection)->push($data);
        } elseif ($this->isInstanceOfPriceNegotiation($data)) {
            $this->data = $data;
        } else {
            $exception = new Exception("Passing data must be either collection or instance of Order or MemberServiceSchedule class ");
            Log::error($exception);

            throw $exception;
        }
    }

    /**
     * @param $data
     * @return bool
     */
    protected function isInstanceOfCollection($data)
    {
        return $data instanceof Collection;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function isEmptyCollection($data)
    {
        return $data->isEmpty();
    }

    /**
     * @param $data
     * @return boolean
     */
    protected function isInstanceOfOrderOrMemberServiceSchedule($data)
    {
        return $data instanceof Order || $data instanceof MemberServiceSchedule;
    }

    /**
     * @param $notificationName
     */
    public function send($notificationName)
    {
        if (str_is('Order*', $notificationName)) {
            return $this->sendOrderNotification($notificationName);
        } elseif (str_is('Service*', $notificationName)) {
            return $this->sendServiceNotification($notificationName);
        } elseif (str_is('PriceNegotiation*', $notificationName)) {
            return $this->sendPriceNegotiationNotification($notificationName);
        }
    }

    /**
     * @param $notificationName
     */
    public function sendOrderNotification($notificationName)
    {
        if ($this->data->isNotEmpty() && $this->data->first() instanceof Order) {

            if ($this->data->count() > 1) {
                $this->data->load('product.merchant');
            }

            $notification = $this->notificationClass($notificationName);

            if ($this->forCustomer($notificationName)) {
                $this->data->groupBy('order_cus_id')
                    ->each(function ($orderItems, $key) use ($notification) {
                        if ($orderItems->isNotEmpty()) {
                            Customer::find($key)->notify(new $notification($orderItems));
                        }
                    });
            }

            if ($this->forMerchant($notificationName)) {
                $this->data->groupBy('product.merchant.mer_id')
                    ->each(function ($orderItems, $key) use ($notification) {
                        if ($orderItems->isNotEmpty()) {
                            Merchant::find($key)->notify(new $notification($orderItems));
                        }
                    });
            }
        }
    }

    /**
     * @param $notificationName
     */
    protected function sendServiceNotification($notificationName)
    {
        if ($this->data->isNotEmpty() && $this->data->first() instanceof MemberServiceSchedule) {

            $this->data->load('store.storeuser');

            $notification = $this->notificationClass($notificationName);

            if ($this->forCustomer($notificationName)) {
                $this->data->groupBy('cus_id')
                    ->each(function ($serviceItems, $key) use ($notification) {
                        if ($serviceItems->isNotEmpty()) {
                            Customer::find($key)->notify(new $notification($serviceItems));
                        }
                    });
            }

            if ($this->forMerchant($notificationName)) {
                $this->data->groupBy('store.stor_id')
                    ->each(function ($serviceItems, $key) use ($notification) {
                        if ($serviceItems->isNotEmpty()) {
                            Notification::send(
                                Store::find($key)->storeUser,
                                new $notification($serviceItems)
                            );
                        }
                    });
            }
        }
    }

    /**
     * @param $notificationName
     * @return string
     */
    protected function notificationClass($notificationName)
    {
        return 'App\\Notifications\\' . $notificationName;
    }

    /**
     * @param $notificationName
     * @return bool
     */
    protected function forCustomer($notificationName)
    {
        return in_array($notificationName, [
            self::ORDER_CANCELLED,
            self::ORDER_PLACED,
            self::ORDER_SHIPPED,
            self::ORDER_RECEIVED,
            self::ORDER_COMPLETED,
            self::SERVICE_CANCELLED,
            self::SERVICE_SCHEDULE_RECEIVED,
            self::SERVICE_RESCHEDULED,
            self::SERVICE_SCHEDULE_REMINDER,
            self::SERVICE_SCHEDULE_CONFIRM,
            self::PRICE_NEGOTIATION_RECEIVED,
            self::PRICE_NEGOTIATION_COUNTER_OFFER,
            self::PRICE_NEGOTIATION_ACCEPTED,
            self::PRICE_NEGOTIATION_REJECTED,
            self::PRICE_NEGOTIATION_EXPIRED
        ]);
    }

    /**
     * @param $notificationName
     * @return bool
     */
    protected function forMerchant($notificationName)
    {
        return in_array($notificationName, [
            self::ORDER_CANCELLED,
            self::ORDER_PLACED,
            self::ORDER_COMPLETED,
            self::SERVICE_CANCELLED,
            self::SERVICE_SCHEDULE_RECEIVED,
            self::SERVICE_RESCHEDULED,
            self::SERVICE_SCHEDULE_REMINDER,
            self::SERVICE_SCHEDULE_CONFIRM,
            self::PRICE_NEGOTIATION_RECEIVED,
            self::PRICE_NEGOTIATION_COUNTER_OFFER,
            self::PRICE_NEGOTIATION_ACCEPTED,
            self::PRICE_NEGOTIATION_REJECTED
        ]);
    }

    /**
     * @param $data
     * @return bool
     */
    protected function isInstanceOfPriceNegotiation($data)
    {
        return $data instanceof PriceNegotiation;
    }

    protected function sendPriceNegotiationNotification($notificationName)
    {
        if (!$this->isInstanceOfPriceNegotiation($this->data)) {
            return null;
        }

        $notification = $this->notificationClass($notificationName);

        if ($this->forCustomer($notificationName)) {
            $this->data->customer->notify(new $notification($this->data));
        }

        if ($this->forMerchant($notificationName)) {
            $this->data->merchant->notify(new $notification($this->data));
        }
    }
}