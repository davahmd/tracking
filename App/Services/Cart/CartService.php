<?php

namespace App\Services\Cart;

use App\Models\Cart;
use App\Models\Order;
use App\Models\ParentOrder;
use App\Models\PriceNegotiation;
use App\Repositories\AttributeRepo;
use App\Repositories\LimitRepo;
use App\Repositories\OrderRepo;
use App\Traits\OrderOnlineLogger;
use DB;
use App\Models\Shipping;
use App\Repositories\CourierRepo;
use App\Repositories\PromotionRepo;

use App\Models\StoreServiceExtra;
use App\Models\Store;
use App\Models\PromotionProduct;

class CartService
{
    public function listing($customer_id = 0, $country_id = null, $wholesale = false)
    {
        if (!$country_id) {
            $country_id = session('countryid');
        }

        $carts = Cart::where('cus_id', $customer_id)
        ->whereWholesale($wholesale)
        ->withAndWhereHas('product', function ($query) {
            $query->where('pro_status', 1)->orderBy('pro_sh_id');
        })
        ->withAndWhereHas('pricing', function ($query) use ($country_id) {
            $query->where('status', 1);
            if ($country_id) {
                $query->where('country_id', $country_id);
            }
        })
        ->withAndWhereHas('product.store', function ($query) {
            $query->where('stor_status', 1);
        })
        ->withAndWhereHas('product.merchant', function ($query) {
            $query->where('mer_staus', 1);
        })
        ->with('negotiation', 'product.images')
        ->get();

        // return $carts;

        $carts = $carts->map(function ($cart) use ($wholesale) {
            $cart['relations'] = array_merge($cart['relations'], $cart->product['relations']);
            $cart->product['relations'] = [];

            $images = $cart->product->images;
            $cart->merchant_id = $cart->product->merchant->mer_id;
            $cart->store_id = $cart->product->store->stor_id;
            $cart->image = $images->isEmpty() ? asset('common/images/stock.png') : $images->first()->imageLink;

            $cart->pricing->attributes = AttributeRepo::get_pricing_attribute_id_json($cart->pricing->id);
            $cart->pricing->attributes_name_view = AttributeRepo::get_pricing_attributes_name($cart->pricing->id);

            $cart->platform_charge_rate = $cart->merchant->mer_platform_charge;
            $cart->service_charge_rate = $cart->merchant->mer_service_charge;

            $cart->discounted = $cart->pricing->is_discounted();
            // $price = $cart->discounted ? round($cart->pricing->discounted_price, 2) : round($cart->pricing->price, 2); // old product pricing discount mechanism
            $price = round($cart->pricing->price, 2);
            // $subtotal = round($price * $cart->quantity, 2);
            $promotion = PromotionRepo::new_pricing_by_promotion($cart->pricing->pro_id);

            if ($wholesale && $cart->wholesale == true) {
                $price = round($cart->pricing->wholesale_price, 2);
            }

            if ($cart->negotiation) {
                $price = $cart->negotiation->merchant_offer_price ?: $cart->negotiation->customer_offer_price;
            }

            else {
                if($promotion) {
                    if($promotion->discount_rate) {

                        $discounted_price = $cart->pricing->price - ($cart->pricing->price * $promotion->discount_rate);
                    }
                    elseif($promotion->discount_value) {

                        $discounted_price = $cart->pricing->price - $promotion->discount_value;
                    }
                }
            }

            // $price = round($cart->pricing->price, 2);
            $subtotal = isset($discounted_price) ? round($discounted_price * $cart->quantity, 2) : round($price * $cart->quantity);
            $platform = round($subtotal * convert_percentage($cart->platform_charge_rate), 2);
            $service = round(($subtotal + $platform) * convert_percentage($cart->service_charge_rate), 2);
            $shipping = 0.00;
            if($cart->pricing->shipping_fees > 0)
            {
                if($cart->pricing->shipping_fees_type == 1)
                {
                    $shipping = round($cart->pricing->shipping_fees * $cart->quantity, 2);
                }
                elseif($cart->pricing->shipping_fees_type == 2)
                {
                    $shipping = round($cart->pricing->shipping_fees, 2);
                }
            }
            if(!empty($cart->service_ids))
            {
                $ids = explode("|", $cart->service_ids);
                $cart->services = StoreServiceExtra::whereIn('id',$ids)->select('service_name')->get();
            }
            

            // $cart->attribute_changed = self::validateAttribute($cart->attributes, $cart->pricing->attributes);
            // $cart->price_changed = (double) $price <> (double) $cart->pricing->price ? true : false;

            $total = round($subtotal + $platform + $service + $shipping, 2);

            $product_id = $cart->product_id;
            $flashsale_id = $cart->flashsale_id;

            $promotion = PromotionProduct::where('promotion_id', $flashsale_id)->where('product_id', $product_id)->first();

            $cart->flashsale = stdObject([
                'count' => isset($promotion) ? $promotion->count : null,
                'limit' => isset($promotion) ? $promotion->limit : null,
            ]);

            $cart->charge = stdObject([
                'amount' => [
                    'value' => isset($discounted_price) ? $discounted_price : $price,
                    'original_price' => isset($discounted_price) ? $price : null,
                    'subtotal' => $subtotal,
                    'total' => $total,
                    'charges' => [
                        'platform' => $platform,
                        'service' => $service,
                        'shipping' => $shipping,
                    ],
                ]
            ]);

            return $cart;
        });

        return $carts;
    }

    public function total($carts)
    {
         return stdObject([
            'amount' => [
                'value' => round($carts->sum(function ($cart) { return $cart->charge->amount->value; }), 2),
                'subtotal' => round($carts->sum(function ($cart) { return $cart->charge->amount->subtotal; }), 2),
                'total' => round($carts->sum(function ($cart) { return $cart->charge->amount->total; }), 2),
                'charges' => [
                    'platform' => round($carts->sum(function ($cart) { return $cart->charge->amount->charges->platform; }), 2),
                    'service' => round($carts->sum(function ($cart) { return $cart->charge->amount->charges->service; }), 2),
                    'shipping' => round($carts->sum(function ($cart) { return $cart->charge->amount->charges->shipping; }), 2),
                ],

            ],
            'totalweight'=>$carts->sum(function ($cart) { return (!$cart->pricing->weight?$cart->product->weight:$cart->pricing->weight); })
        ]);
    }

    public function add($customer_id, $product_id, $pricing_id, $quantity, $service_ids = null, $nego_id = null, $wholesale = false)
    {
        // Check product is under flashsale or not
        $promotion = PromotionRepo::new_pricing_by_promotion($product_id);

        if ($promotion) {
            $flashsale_id = $promotion->id;
        }
        else {
            $flashsale_id = null;
        }

        try {
            # check cart existance
            $cart = Cart::where('cus_id', $customer_id)
            ->where('product_id', $product_id)
            ->where('pricing_id', $pricing_id)
            ->where('service_ids', $service_ids)
            ->where('nego_id', $nego_id)
            ->where('wholesale', $wholesale ? true : false)
            ->first();

            if (!$cart) {
                $cart = Cart::create([
                    'cus_id' => $customer_id,
                    'product_id' => $product_id,
                    'pricing_id' => $pricing_id,
                    'quantity' => $quantity,
                    'service_ids'=>$service_ids,
                    'flashsale_id' => $flashsale_id,
                    'nego_id' => $nego_id,
                    'wholesale' => $wholesale ? true : false
                ]);
                $cart->weight_per_qty = (!$cart->pricing->weight?$cart->product->weight:$cart->pricing->weight);
                $cart->save();
            }
            else {
                $cart->increment('quantity', $quantity);
                $cart->save();
            }

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function updateQuantity($cart_id, $quantity)
    {
        try {
            $cart = Cart::where('id', $cart_id)->update([
                'quantity' => $quantity
            ]);

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function remove($customer_id, $cart_id = null)
    {
        try {
            # if no cart_id will remove all based on customer_id
            if ($cart_id) {
                $cart = Cart::where('id', $cart_id)->where('cus_id', $customer_id)->first();

                if ($cart->negotiation) {
                    $cart->negotiation->update([
                        'status' => PriceNegotiation::STATUS['DECLINED_BY_CUSTOMER']
                    ]);
                }

                $cart->delete();
            }
            else {
                Cart::where('cus_id', $customer_id)->delete();
            }

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function checkout($customer_id, $address, $wholesale = false)
    {
        try {
            $carts = self::listing($customer_id, null, $wholesale);

            $carts_validations = self::validateCartItem($carts, $customer_id);
            foreach ($carts_validations as $item) {
                if ($item->quantity->cart <= 0)
                {
                    self::remove($customer_id, $item->cart_id);
                }

                if(!$item->store_accept_payment->status)
                {
                    return apiResponse(403, $item->store_accept_payment->message);
                }

                if($item->quantity->exceed->status)
                {
                    return apiResponse(403, $item->quantity->exceed->message);
                }

                if($item->item_expired->status)
                {
                    return apiResponse(403, $item->item_expired->message);
                }

                if($item->limit->product)
                {
                    return apiResponse(403, $item->limit->product);
                }

                if($item->limit->store)
                {
                    return apiResponse(403, $item->limit->store);
                }

                if($item->limit->customer)
                {
                    return apiResponse(403, $item->limit->customer);
                }
            }

            try {
                // Update Shipping Fees
                self::updateShippingCharge($customer_id);
            }
            catch(\Exception $e) {
                return apiResponse(403, trans('localize.checkout_fail'));
            }

            do {
                $trans_id = str_random(8);
                $check_trans_id = Order::where('transaction_id', $trans_id)->first();
            } while (!empty($check_trans_id));

            DB::beginTransaction();

            try {
                $parentOrder = ParentOrder::create([
                    'transaction_id' => $trans_id,
                    'customer_id' => $customer_id,
                    'wholesale' => $wholesale
                ]);
            }
            catch(\Exception $e)
            {
                DB::rollback();
                return apiResponse(403, trans('localize.checkout_fail'));
            }

            $parentOrderId = $parentOrder->id;
            $payment_type = 1;

            try {
                $checkouts = DB::select("call cart_checkout(:customer_id, :country_id, :trans_id, :name, :address1, :address2, :city, :country, :postcode, :telephone, :state, :payment_type, :parent_id, :subdistrict, :wholesale)",[
                    ':customer_id' => $customer_id,
                    ':country_id' => $address['country'],
                    ':trans_id' => $trans_id,
                    ':name' => $address['name'],
                    ':address1' => $address['address_1'],
                    ':address2' => $address['address_2'],
                    ':city' => $address['city'],
                    ':country' => $address['country'],
                    ':postcode' => $address['postal_code'],
                    ':telephone' => $address['telephone'],
                    ':state' => $address['state'],
                    ':payment_type' => $payment_type,
                    ':parent_id' => $parentOrderId,
                    ':subdistrict' => $address['sub_district'],
                    ':wholesale' => $wholesale ? 1 : 0
                ]);
            }
            catch(\Exception $e)
            {
                DB::rollback();
                return apiResponse(403, trans('localize.checkout_fail'));
            }

            if (!$checkouts) {
                DB::rollback();
                return apiResponse(403, trans('localize.checkout_fail'));
            }

            DB::commit();

            LimitRepo::update_limit_transaction('online', $trans_id);
            OrderRepo::generateTaxInvoice($parentOrder->id);
            // OrderRepo::generateDeliveryOrder($parentOrder->id);
            OrderOnlineLogger::log($parentOrder->id, null, null, 'customer', 'Purchased order');

            $parentOrder->updateCompletedNegotiationStatus();

            return apiResponse(400, [
                'data' => [
                    'transaction_id' => $trans_id,
                    'orders' => $checkouts
                ]
            ]);
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function validateCartItem($carts, $customer_id)
    {
        $response = [];
        foreach ($carts->groupBy('store_id') as $store_id => $carts)
        {
            $total_purchasing = round($carts->sum(function ($cart) {
                return $cart->charge->amount->subtotal;
            }), 2);

            foreach($carts as $key => $cart)
            {
                $expired = $cart->product->product_type == 3 && !empty($cart->expired_at) && Carbon::now('UTC') >= Carbon::parse($cart->expired_at)? true : false;
                $exceed = $cart->pricing->quantity < $cart->quantity ? true : false;

                $response[$key] = [
                    'cart_id' => $cart->id,
                    'store_accept_payment' => [
                        'status' => $cart->store->accept_payment,
                        'message' => trans('localize.store_not_accept_payment')
                    ],
                    'item_expired' => [
                        'status' => $expired,
                        'message' => trans('localize.checkouts.product.expired', ['name' => $cart->product->title])
                    ],
                    'quantity' => [
                        'cart' => $cart->quantity,
                        'product' => $cart->pricing->quantity,
                        'exceed' => [
                            'status' => $exceed,
                            'message' => trans('localize.checkouts.insufficient.quantity', ['name' => $cart->product->title])
                        ],
                    ],
                    'limit' => [
                        'product' => LimitRepo::check_payment_limitation('productLimit', $cart->quantity, null, $customer_id, $cart->pro_id),
                        'store' => LimitRepo::check_payment_limitation('storeLimit', $total_purchasing, $cart->product->store->stor_id),
                        'customer' => LimitRepo::check_payment_limitation('customerLimit', $total_purchasing, null, $customer_id),
                    ]
                ];
            }
        }

        return json_decode(json_encode($response));
    }

    public function getCourier($items, $shipping)
    {
        $item_with_courier = array();

        $weight = 0;
        foreach ($items as $y => $cart)
        {
            if(empty($cart->service_ids))
                $weight +=  $cart->weight_per_qty * $cart->quantity;
        }
        $store = $cart->product->store;
        $param['origin'] = !$store->stor_subdistrict?$store->stor_city:$store->stor_subdistrict;
        $param['originType'] = !$store->stor_subdistrict?'city':'subdistrict';
        $param['destination'] = !$shipping->ship_subdistrict_id?$shipping->ship_ci_id:$shipping->ship_subdistrict_id;
        $param['destinationType'] = !$shipping->ship_subdistrict_id?'city':'subdistrict';
        $param['weight'] =  $weight;
        $param['courier'] = 'jne:pos:tiki';
        $result = CourierRepo::get_couriers_fee($param);
        $item= array(
            // 'store_id' =>$cart->product->pro_sh_id,
            'weight'=>$param['weight'],
            'options'=>$result
        );
        array_push($item_with_courier, $item);

        return $item_with_courier;
    }

    public function updateCartCourier($customer_id, $store_id, $data)
    {
        try {
            $carts= Cart::where('cus_id', $customer_id)->withAndWhereHas('product', function ($query) use ($store_id) {
                $query->where('pro_sh_id', $store_id);
            })->get();

            $weight = $carts->filter(function ($item) {
                return $item->service_ids == null || $item->service_ids == '';
            })->reduce(function ($carry, $item) {
                return $carry + ($item->quantity * $item->weight_per_qty);
            });

            $feePerGram = ($data['fee'] / $weight);

            foreach($carts as $item)
            {
                $item->ship_code = $data['code'];
                $item->ship_service = $data['service'];
                $item->shipping_note = $data['note'];
                $item->ship_eta = $data['eta'];
                $item->shipping_fee = (!$item->service_ids) ? ($feePerGram * ($item->quantity * $item->weight_per_qty)) : 0;
                $item->Save();
            }

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function updateShippingCharge($customer_id)
    {
        $carts = Cart::where('cus_id', $customer_id)
        ->withAndWhereHas('product', function ($query) {
            $query->where('pro_status', 1)->orderBy('pro_sh_id');
        })
        ->withAndWhereHas('product.store', function ($query) {
            $query->where('stor_status', 1);
        })->get();

        $shippings = Shipping::where('ship_cus_id', $customer_id)->where('ship_order_id', 0)->get();
        $cart_shipping_id = $carts->where('ship_id', '>', '0')->pluck('ship_id')->first();

        if (!$cart_shipping_id) {
            $shipping = $shippings->where('ship_order_id', 0)->sortByDesc('isdefault')->first();
        }
        else {
            $shipping = $shippings->where('ship_id', $cart_shipping_id)->first();
        }

        $item_with_courier = array();
        foreach ($carts->groupBy('product.store.stor_id') as $store_id => $cart)
        {
            $weight = $cart->filter(function ($item) {
                return $item->service_ids == null || $item->service_ids == '';
            })->reduce(function ($carry, $item) {
                return $carry + ($item->quantity * $item->weight_per_qty);
            });

            $ship_code = $cart->filter(function ($item) {
                return $item->service_ids == null || $item->service_ids == '';
            })->pluck('ship_code')->first();

            $ship_service = $cart->filter(function ($item) {
                return $item->service_ids == null || $item->service_ids == '';
            })->pluck('ship_service')->first();

            $store = Store::find($store_id);

            $param['origin'] = !$store->stor_subdistrict?$store->stor_city:$store->stor_subdistrict;
            $param['originType'] = !$store->stor_subdistrict?'city':'subdistrict';
            $param['destination'] = !$shipping->ship_subdistrict_id?$shipping->ship_ci_id:$shipping->ship_subdistrict_id;
            $param['destinationType'] = !$shipping->ship_subdistrict_id?'city':'subdistrict';
            $param['weight'] =  $weight;
            $param['courier'] = $ship_code;
            $result = CourierRepo::get_couriers_fee($param);

            if ($result) {
                $courier = collect(collect($result)->first()->costs)->where('service', $ship_service);
                $fee = collect($courier->first()->cost)->first()->value;
                $feePerGram = ($fee / $weight);

                foreach($cart as $item) {
                    $item->shipping_fee = (!$item->service_ids) ? ($feePerGram * ($item->quantity * $item->weight_per_qty)) : 0;
                    $item->Save();
                }
            }
        }
        return;
    }
}
