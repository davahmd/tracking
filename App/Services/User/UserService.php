<?php

namespace App\Services\User;

use App\Models\User;
use App\Providers\ActivationServiceProvider;
use App\Models\AdminSetting;

class UserService
{
    protected $user = null;

    public function check($email)
    {
        $user = User::where('email', $email)->first();

        if ($user) {
            return [
                'user' => $user,
                'customer' => $user->customer,
                'merchant' => $user->merchant,
                'storeUser' => $user->storeUser
            ];
        }

        return false;
    }

    public function create($data, $type, $created_by = null)
    {
        $check = self::check($data['email']);
        $temp_password = str_random(8);

        if (!$check) {
            $this->user = User::create([
                'email' => $data['email'],
                'password' => bcrypt($temp_password)
            ]);

            if ($type == 'C') {
                $this->createCustomer($data);
            }

            if ($type == 'M') {
                $this->createCustomer([
                    'cus_name' => $data['fname'] . ' ' . $data['lname']
                ]);

                $this->createMerchant($data);
            }

            if($type == 'S') {
                $this->createCustomer([
                    'cus_name' => $data['name']
                ]);

                $this->createStoreUser($data);
            }
        }
        else {
            $this->user = $check['user'];

            if ($type == 'M') {
                if (!$this->user->customer) {
                    $this->createCustomer([
                        'cus_name' => $data['fname'] . ' ' . $data['lname'],
                        'cus_status' => $this->user->status
                    ]);
                }

                $this->createMerchant($data);
            }

            if($type == 'S') {
                if (!$this->user->storeUser && !$this->user->merchant) {
                    if (!$this->user->customer) {
                        $this->createCustomer([
                            'cus_name' => $data['name'],
                            'cus_status' => $this->user->status
                        ]);
                    }

                    $this->createStoreUser($data);
                } else {
                    return false;
                }
            }
        }

        if ($created_by == 'Admin' && $this->user->wasRecentlyCreated) {
            resolve(ActivationServiceProvider::class)->sendActivationMail_byAdmin($this->user, $temp_password);
        }
        else {
            resolve(ActivationServiceProvider::class)->sendActivationMail($this->user);
        }

        return $this->user->fresh();
    }

    public function activate($user_id)
    {
        $user = User::find($user_id);
        $user->activation = true;
        $user->status = true;

        if ($user->customer) {
            $user->customer->update([
                'cus_status' => true
            ]);
        }

        if ($user->merchant) {
            $user->merchant->update([
                'mer_staus' => true
            ]);
        }

        if ($user->storeUser) {
            $user->storeUser->update([
                'status' => true
            ]);
        }

        $user->save();

        return $user;
    }

    protected function createCustomer($data)
    {
        return $this->user ? $this->user->customer()->create([
            'cus_name' => $data['cus_name'],
            'cus_phone' => !empty($data['cus_phone']) ? $data['cus_phone'] : null,
            'phone_area_code' => !empty($data['cus_phone']) ? $data['areacode'] : null,
            'cus_address1' => !empty($data['cus_address1']) ? $data['cus_address1'] : null,
            'cus_address2' => !empty($data['cus_address2']) ? $data['cus_address2'] : null,
            'cus_country' => !empty($data['cus_country']) ? $data['cus_country'] : 0,
            'cus_state' => !empty($data['cus_state']) ? $data['cus_state'] : 0,
            'cus_city' => !empty($data['cus_city']) ? $data['cus_city'] : 0,
            'cus_subdistrict' => !empty($data['cus_subdistrict']) ? $data['cus_subdistrict'] : 0,
            'cus_postalcode' => !empty($data['cus_postalcode']) ? $data['cus_postalcode'] : null,
            'cus_status' => !empty($data['cus_status']) ? $data['cus_status'] : 0
        ]) : null;
    }

    protected function createMerchant($data)
    {
        $setting = AdminSetting::first();

        return $this->user ? $this->user->merchant()->create([
            'slug' => $data['url_slug'],
            'mer_fname' => $data['fname'],
            'mer_lname' => $data['lname'],
            'mer_phone' => $data['tel'],
            'mer_office_number' => $data['office_number'],
            'mer_address1' => $data['address1'],
            'mer_address2' => $data['address2'],
            'mer_co_id' => !empty($data['country'])? $data['country'] : 0,
            'mer_state' => !empty($data['state'])? $data['state'] : 0,
            'mer_ci_id' => !empty($data['city'])? $data['city'] : 0,
            'mer_subdistrict' => !empty($data['subdistrict'])? $data['subdistrict'] : 0,
            'zipcode' => $data['zipcode'],
            'mer_payment' => $data['email'],
            'mer_commission' => 10,
            'bank_acc_name' => $data['bank_acc_name'],
            'bank_acc_no' => $data['bank_acc_no'],
            'bank_name' => $data['bank_name'],
            'bank_country' => $data['bank_country'],
            'bank_address' => $data['bank_address'],
            'bank_swift' => $data['bank_swift'],
            'bank_europe' => $data['bank_europe'],
            'bank_gst' => $data['bank_gst'],
            'mer_platform_charge' => $setting->platform_charge,
            'mer_service_charge' => $setting->service_charge,
            'mer_staus' => 0
        ]) : null;
    }

    protected function createStoreUser($data)
    {
        return $this->user ? $this->user->storeUser()->create([
            'mer_id' => $data['merchant_id'],
            'name' => $data['name'],
            'phone' => $data['phone'],
            'status' => 0
        ]) : null;
    }
}
