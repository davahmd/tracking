<?php

namespace App\Services\Rating;

use App\Models\Order;
use App\Models\Rating;

class RatingService
{
    public function addReview($order_id, $customer_id, $request)
    {
        try
        {
            if(!isset($request['reference']))
            {
                throw new \Exception;
            }

            $reference = $request['reference'];

            switch ($reference)
            {
                case 'product':
                    $reference_type = Rating::REFERENCE_TYPE_PRODUCT;
                    break;

                case 'store':
                    $reference_type = Rating::REFERENCE_TYPE_STORE;
                    break;

                default:
                    return apiResponse(422, 'Invalid ref type.');
                    break;
            }

            $order = Order::withAndWhereHas('customer', function ($query) use ($customer_id)
            {
                $query->where('cus_id', $customer_id);
            })
            ->withAndWhereHas('product.store')
            ->where('order_id', $order_id)
            ->first();

            if(!$order || !$order->product || !$order->product->store)
            {
                return apiResponse(404, 'Order not found.');
            }

            if($reference_type == Rating::REFERENCE_TYPE_PRODUCT && !$order->rateEligibility->product)
            {
                return apiResponse(403, 'Product already rated.');
            }

            if($reference_type == Rating::REFERENCE_TYPE_STORE && !$order->rateEligibility->store)
            {
                return apiResponse(403, 'Store already rated.');
            }

            $rating = $order->ratings()->create([
                'customer_id' => $customer_id,
                'parent_order_id' => $order->parent_order_id,
                'reference_id' => $reference_type == Rating::REFERENCE_TYPE_PRODUCT ? $order->product->pro_id : $order->product->store->stor_id,
                'reference_type' => $reference_type,
                'rating' => $request['rating'],
                'review' => isset($request['review']) ? $request['review'] : null,
                'display' => 1
            ]);

            if(isset($request['images']))
            {
                foreach ($request['images'] as $file)
                {
                    $fileName = str_random(10)."_".date('YmdHis');
                    $path = "$reference/review/images/{$rating->id}";
                    $upload = uploadImage($file, "/$path", $fileName);

                    if($upload->status)
                    {
                        $image = $upload->file_name;

                        $rating->images()->create([
                            'image' => $image,
                            'path' => $path,
                        ]);
                    }
                }
            }

            return apiResponse(200, trans('api.success'));
        }
        catch (\Exception $e)
        {
            return apiResponse(500, trans('api.systemError'));
        }
    }

    public function toggleDisplay($rating_id)
    {
        try
        {
            $rating = Rating::find($rating_id);
            if(!$rating)
            {
                throw new \Exception;
            }

            $rating->toggleDisplay()->save();

            return apiResponse(200, trans('api.success'));
        }
        catch (\Exception $e)
        {
            return apiResponse(500, trans('api.systemError'));
        }
    }
}
