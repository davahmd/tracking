<?php

namespace App\Repositories;
use App\Models\Courier;
use Auth;
use RajaOngkir;
use App\Models\Cart;
use App\Models\Shipping;
use App\Models\Store;

class CourierRepo
{
    public static function get_couriers()
    {
        return Courier::where('status', '=', 1)->get();
    }

    public static function all()
    {
        return Courier::all();
    }

    public static function add($data)
    {
        $courier = Courier::create([
            'name' => $data['name'],
            'link' => $data['link'],
            'status' => $data['status'],
        ]);

        return $courier;
    }

    public static function find($id)
    {
        return Courier::find($id);
    }

    public static function update($id,$data)
    {
        $courier = Courier::find($id);
        $courier->name = $data['name'];
        $courier->link = $data['link'];
        $courier->status = $data['status'];
        $courier->save();

        return $courier;
    }

    public static function delete($id)
    {
        $courier = Courier::find($id);
        $courier->delete();

        return $courier;
    }

    public static function get_couriers_fee($data)
    {
        $origin = $data['origin'];
        $originType = $data['originType'];
        $destination = $data['destination'];
        $destinationType = $data['destinationType'];
        $weight = (int) $data['weight'];
        $courier = $data['courier'];

        try {
            $cost = (array) json_decode(RajaOngkir::cost($origin, $originType, $destination, $destinationType, $weight, $courier));
            $returnCode = $cost['rajaongkir']->status->code;

            if($returnCode != 200) {
                return false;
                /* return response()->json([
                    'data' => null,
                    'message' => $cost['rajaongkir']->status->description,
                    'status' => false,
                ]); */
            }

            $result = [];
            if(!empty($cost)) {
                return $cost['rajaongkir']->results;
                /* $result['data'] = $cost['rajaongkir']->results;
                $result['message'] = 'Data found';
                $result['status'] = true; */
            }

            //return response()->json($result);
        }
        catch (\Exception $e) {
            return false;
            /* return response()->json([
                'data' => null,
                'messages' => $e->getMessage(),
                'status' => false
            ]); */
        }
    }

    public static function calculate_shipping_fee($carts, $customer_id)
    {
        $ship_carts = Cart::where('cus_id', $customer_id)
        ->withAndWhereHas('product', function ($query) {
            $query->where('pro_status', 1)->orderBy('pro_sh_id');
        })
        ->withAndWhereHas('product.store', function ($query) {
            $query->where('stor_status', 1);
        })->get();

        $updated_carts = [];

        $shippings = Shipping::where('ship_cus_id', $customer_id)->where('ship_order_id', 0)->get();
        $cart_shipping_id = $carts->where('ship_id', '>', '0')->pluck('ship_id')->first();

        if (!$cart_shipping_id) {
            $shipping = $shippings->where('ship_order_id', 0)->sortByDesc('isdefault')->first();
        }
        else {
            $shipping = $shippings->where('ship_id', $cart_shipping_id)->first();
        }

        $item_with_courier = array();
        foreach ($ship_carts->groupBy('product.store.stor_id') as $store_id => $cart)
        {
            $weight = $cart->filter(function ($item) {
                return $item->service_ids == null || $item->service_ids == '';
            })->reduce(function ($carry, $item) {
                return $carry + ($item->quantity * $item->weight_per_qty);
            });

            $ship_code = $cart->filter(function ($item) {
                return $item->service_ids == null || $item->service_ids == '';
            })->pluck('ship_code')->first();

            $ship_service = $cart->filter(function ($item) {
                return $item->service_ids == null || $item->service_ids == '';
            })->pluck('ship_service')->first();

            $store = Store::find($store_id);

            $param['origin'] = !$store->stor_subdistrict?$store->stor_city:$store->stor_subdistrict;
            $param['originType'] = !$store->stor_subdistrict?'city':'subdistrict';
            $param['destination'] = !$shipping->ship_subdistrict_id?$shipping->ship_ci_id:$shipping->ship_subdistrict_id;
            $param['destinationType'] = !$shipping->ship_subdistrict_id?'city':'subdistrict';
            $param['weight'] =  $weight;
            $param['courier'] = $ship_code;
            $result = CourierRepo::get_couriers_fee($param);

            if ($result) {
                $courier = collect(collect($result)->first()->costs)->where('service', $ship_service);
                $fee = collect($courier->first()->cost)->first()->value;
                $feePerGram = ($fee / $weight);

                foreach($cart as $item) {
                    $item->shipping_fee = (!$item->service_ids) ? ($feePerGram * ($item->quantity * $item->weight_per_qty)) : 0;
                    $item->save();

                    foreach ($carts as $cart) {

                        if ($cart->product_id == $item->product_id) {
                            $cart->shipping_fee = $item->shipping_fee;
                        }
                    }
                }
            }
        }
    }
}
