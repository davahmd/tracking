<?php

namespace App\Repositories;

use Carbon\Carbon;

use App\Models\Cart;
use App\Models\Customer;
use App\Models\PriceNegotiation;
use App\Models\Product;
use App\Services\Cart\CartService;
use App\Services\Notifier\Notifier;

class NegotiationRepo
{
    public static function all($input)
    {
        $negotiations = PriceNegotiation::query();

        $negotiations->where('status', '<>', PriceNegotiation::STATUS['COUNTER_OFFER']);

        if (!empty($input['customer_id'])) {
            $negotiations->where('customer_id', $input['customer_id']);
        }

        if (!empty($input['customer_name'])) {
            $customer_name = $input['customer_name'];
            $negotiations->whereHas('customer', function($query) use ($customer_name) {
                $query->where('cus_name', 'LIKE', '%'.$customer_name.'%');
            });
        }

        if (!empty($input['merchant_id'])) {
            $negotiations->where('merchant_id', $input['merchant_id']);
        }

        if (!empty($input['merchant_name'])) {
            $merchant_name = $input['merchant_name'];
            $negotiations->whereHas('merchant', function($query) use ($merchant_name) {
                $query->where('merchant_name', 'LIKE', '%'.$merchant_name.'%');
            });
        }

        if (!empty($input['product_id'])) {
            $negotiations->where('product_id', $input['product_id']);
        }

        if (!empty($input['product_name'])) {
            $product_name = $input['product_name'];
            $negotiations->whereHas('product', function($query) use ($product_name) {
                $query->where('pro_title_en', 'LIKE', '%'.$product_name.'%')->orWhere('pro_title_ind', 'LIKE', '%'.$product_name.'%');
            });
        }

        if (!empty($input['status'])) {
            $negotiations->where('status', $input['status']);
        }

        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'customer_name_asc':
                    $negotiations->join('nm_customer', 'price_negotiations.customer_id', '=', 'nm_customer.cus_id')->orderBy('nm_customer.cus_name', 'asc');
                    break;
                case 'customer_name_desc':
                    $negotiations->join('nm_customer', 'price_negotiations.customer_id', '=', 'nm_customer.cus_id')->orderBy('nm_customer.cus_name', 'desc');
                    break;
                case 'merchant_name_asc':
                    $negotiations->join('nm_merchant', 'price_negotiations.merchant_id', '=', 'nm_merchant.mer_id')->orderBy('nm_merchant.username', 'asc');
                    break;
                case 'merchant_name_desc':
                    $negotiations->join('nm_merchant', 'price_negotiations.merchant_id', '=', 'nm_merchant.mer_id')->orderBy('nm_merchant.username', 'desc');
                    break;
                case 'product_name_asc':
                    $negotiations->join('nm_product', 'price_negotiations.product_id', '=', 'nm_product.pro_id')->orderBy('nm_product.pro_title_en', 'asc');
                    break;
                case 'product_name_desc':
                    $negotiations->join('nm_product', 'price_negotiations.product_id', '=', 'nm_product.pro_id')->orderBy('nm_product.pro_title_en', 'desc');
                    break;
                case 'new':
                    $negotiations->orderBy('created_at', 'desc');
                    break;
                case 'old':
                    $negotiations->orderBy('created_at', 'asc');
                    break;
                default:
                    $negotiations->orderBy('id', 'desc');
                    break;
            }
        } else {
            $negotiations->orderBy('created_at', 'desc');
        }

        return $negotiations->paginate();
    }

    public static function openNewNegotiation($cus_id, $nego_qty, $nego_price, $nego_product_id, $nego_pricing_id, $wholesale)
    {
        $product = Product::where('pro_id', $nego_product_id)->first();

        return PriceNegotiation::create([
            'product_id' => $nego_product_id,
            'pricing_id' => $nego_pricing_id,
            'quantity' =>$nego_qty,
            'customer_id' => $cus_id,
            'customer_offer_price' => $nego_price,
            'wholesale' => $wholesale,
            'merchant_id' => $product->pro_mr_id,
            'expired_at' => Carbon::now()->addDays(2),
        ]);
    }

    public function getExistingNegotiation($cus_id, $product_id, $pricing_id)
    {
        return PriceNegotiation::where([
            'customer_id' => $cus_id,
            'product_id' => $product_id,
            'pricing_id' => $pricing_id,
        ])
        ->whereDate('expired_at', '>', date('Y-m-d H:i:s'))
        ->first();
    }

    public function getMerchantNegotiations($mer_id, $order = 'asc')
    {
        return PriceNegotiation::where('merchant_id', $mer_id)
            ->exceptCounterOffer()
            ->orderBy('id', $order)
            ->get();
    }

    public static function merchantAcceptOffer($nego_id)
    {
        $negotiation = PriceNegotiation::find($nego_id);

        $negotiation->status = PriceNegotiation::STATUS['ACCEPTED_BY_MERCHANT'];
        $negotiation->save();

        return $negotiation;
    }

    public static function merchantDeclineOffer($nego_id)
    {
        $negotiation = PriceNegotiation::find($nego_id);

        $negotiation->status = PriceNegotiation::STATUS['DECLINED_BY_MERCHANT'];
        $negotiation->save();

        return $negotiation;
    }

    public static function merchantCounterOffer($nego_id, $data)
    {
        $negotiation = PriceNegotiation::find($nego_id);

        $negotiation->merchant_offer_price = $data['merchant_offer_price'];
        $negotiation->final_price = isset($data['final']) ? isset($data['final']) : ((isset($data['final_price'])) ? $data['final_price'] : false);
        $negotiation->status = PriceNegotiation::STATUS['ACTIVE'];
        $negotiation->save();

        return $negotiation;
    }

    public static function memberUpdateNegotiation($nego_id, $cus_id, $status, $nego_price = null)
    {
        $negotiation = PriceNegotiation::find($nego_id);

        $notifier = new Notifier($negotiation);

        if ($status == PriceNegotiation::STATUS['ACCEPTED_BY_CUSTOMER']) {
            $negotiation->status = 12;
            $negotiation->save();

            $notifier->send(Notifier::PRICE_NEGOTIATION_ACCEPTED);
        }
        elseif ($status == PriceNegotiation::STATUS['DECLINED_BY_CUSTOMER']) {
            $negotiation->status = 32;
            $negotiation->save();

            $notifier->send(Notifier::PRICE_NEGOTIATION_REJECTED);
        }
        elseif ($status == PriceNegotiation::STATUS['COUNTER_OFFER']) {
            $negotiation->status = 60;
            $negotiation->save();

            $new_nego = PriceNegotiation::create([
                'session_id' => $negotiation->id,
                'product_id' => $negotiation->product_id,
                'pricing_id' => $negotiation->pricing_id,
                'quantity' => $negotiation->quantity,
                'customer_id' => $cus_id,
                'customer_offer_price' => $nego_price,
                'wholesale' => $negotiation->wholesale,
                'merchant_id' => $negotiation->merchant_id,
                'status' => PriceNegotiation::STATUS['ACTIVE'],
                'expired_at' => Carbon::now()->addDays(2),
            ]);

            (new Notifier($new_nego))->send(Notifier::PRICE_NEGOTIATION_COUNTER_OFFER);

            return $new_nego;
        }

        return $negotiation;
    }

    public static function merchantUpdateNegotiation($nego_id, $mer_id, $status, $nego_price = null, $final_price = false)
    {
        $negotiation = PriceNegotiation::where('id', $nego_id)->where('merchant_id', $mer_id)->firstOrFail();

        if ($status == PriceNegotiation::STATUS['ACCEPTED_BY_MERCHANT']) {

            $negotiation = self::merchantAcceptOffer($negotiation->id);

            (new CartService)->add(
                $negotiation->customer_id,
                $negotiation->product_id,
                $negotiation->pricing_id,
                $negotiation->quantity,
                null,
                $negotiation->id,
                $negotiation->wholesale
            );

            (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_ACCEPTED);
        }
        elseif ($status == PriceNegotiation::STATUS['DECLINED_BY_MERCHANT']) {

            $negotiation = self::merchantDeclineOffer($negotiation->id);

            (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_REJECTED);
        }
        elseif ($status == PriceNegotiation::STATUS['ACTIVE']) {

            $data['merchant_offer_price'] = $nego_price;
            $data['final_price'] = $final_price;

            $negotiation = self::merchantCounterOffer($negotiation->id, $data);

            (new Notifier(PriceNegotiation::find($nego_id)))->send(Notifier::PRICE_NEGOTIATION_COUNTER_OFFER);
        }

        return $negotiation;
    }

    public static function memberNegotiationListing($cus_id, $per_page, $page, $product_id = null)
    {
        $negotiations = PriceNegotiation::with('product')
            ->where('customer_id', $cus_id)
            ->exceptCounterOffer()
            ->paginate($per_page);

        if ($product_id != null) {
            $negotiations = PriceNegotiation::with('product')
                ->where('customer_id', $cus_id)
                ->where('product_id', $product_id)
                ->active()
                ->paginate($per_page);

            return $negotiations;
        }

        return $negotiations;
    }

    public static function merchantNegotiationListing($mer_id, $per_page, $page, $product_id = null, $sort = 'oldest')
    {
        // $negotiations = PriceNegotiation::with('product')
        //     ->where('merchant_id', $mer_id)
        //     ->exceptCounterOffer()
        //     ->paginate($per_page);
        //
        // if ($product_id != null) {
        //     $negotiations = PriceNegotiation::with('product')
        //         ->where('merchant_id', $mer_id)
        //         ->where('product_id', $product_id)
        //         ->active()
        //         ->paginate($per_page);
        //
        //     return $negotiations;
        // }
        //
        // return $negotiations;

        $negotiations = PriceNegotiation::with('product')
            ->where('merchant_id', $mer_id);

        if ($product_id != null) {
            $negotiations->where('product_id', $product_id)
                ->active();
        }
        else {
            $negotiations->exceptCounterOffer();
        }

        return $negotiations->{$sort}()->paginate($per_page);
    }

    public static function memberNegotiationDetail($cus_id, $negotiation_id)
    {
        $negotiation = PriceNegotiation::where('customer_id', $cus_id)
            ->where('id', $negotiation_id)
            ->first();

        return $negotiation;
    }

    public static function merchantNegotiationDetail($mer_id, $negotiation_id)
    {
        $negotiation = PriceNegotiation::where('merchant_id', $mer_id)
            ->where('id', $negotiation_id)
            ->first();

        return $negotiation;
    }

    public static function memberNegotiationHistory($cus_id, $negotiation_id)
    {
        $negotiation = PriceNegotiation::where('customer_id', $cus_id)
            ->where('id', $negotiation_id)
            ->first();

        $negotiations = PriceNegotiation::where('customer_id', $cus_id)
            ->where('id', $negotiation->session_id)
            ->orWhere('id', $negotiation_id)
            ->get();

        return $negotiations;
    }

    public static function merchantNegotiationHistory($mer_id, $negotiation_id)
    {
        $negotiation = PriceNegotiation::where('merchant_id', $mer_id)
            ->where('id', $negotiation_id)
            ->first();

        $negotiations = PriceNegotiation::where('merchant_id', $mer_id)
            ->where('id', $negotiation->session_id)
            ->orWhere('id', $negotiation_id)
            ->get();

        return $negotiations;
    }

    public static function validateMerchantPriceNegotiation($nego_id, $mer_id, $status)
    {
        $negotiation = PriceNegotiation::where('merchant_id', $mer_id)
            ->where('id', $nego_id)
            ->where('status', 10)
            ->where('merchant_offer_price', null)
            ->firstOrFail();

        return $negotiation;
    }
}