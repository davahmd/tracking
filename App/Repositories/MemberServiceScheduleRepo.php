<?php

namespace App\Repositories;

use App\Models\MemberServiceSchedule;
use DB;

class MemberServiceScheduleRepo
{
    public static function all($input)
    {
        $member_services = MemberServiceSchedule::leftjoin('store_service_extra', 'member_service_schedule.store_service_extra_id', '=', 'store_service_extra.id')
        ->leftJoin('store_service_schedule', 'store_service_extra.store_service_schedule_id', '=', 'store_service_schedule.id')
        ->leftJoin('nm_product', 'store_service_schedule.pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->leftJoin('nm_store', 'store_service_schedule.stor_id', '=', 'nm_store.stor_id')
        ->leftJoin('nm_order', 'member_service_schedule.order_id', '=', 'nm_order.order_id')
        ->selectRaw('member_service_schedule.*, nm_product.*, nm_store.*, store_service_extra.store_service_schedule_id as store_service_schedule_id, store_service_extra.service_name as service_name, store_service_extra.service_price as service_price, 
        store_service_schedule.pro_id as pro_id, store_service_schedule.interval_period as interval_period, store_service_schedule.stor_id as stor_id, store_service_schedule.appoint_start as appoint_start, store_service_schedule.appoint_end as appoint_end, store_service_schedule.schedule_skip_count as schedule_skip_count,
        nm_order.parent_order_id as parent_order_id,  nm_order.order_qty as order_qty, nm_order.product_price as product_price, nm_order.order_date as order_date, nm_order.transaction_id as transaction_id');

        if (!empty($input['order_id']))
            $member_services->where('nm_order.parent_order_id', '=', $input['order_id']);

        if (!empty($input['id']))
            $member_services->where('member_service_schedule.id', '=', $input['id']);

        if (!empty($input['mid']))
            $member_services->where('nm_store.stor_merchant_id', '=', $input['mid']);

        if (!empty($input['name'])) {
            $search = '%'.$input['name'].'%';
            $member_services->where(function($query) use ($search) {
                $query->whereRaw('nm_product.pro_title_en LIKE ? or nm_product.pro_title_cn LIKE ? or nm_product.pro_title_cnt LIKE ? or nm_product.pro_title_my LIKE ? or nm_merchant.mer_fname LIKE ? or nm_store.stor_name LIKE ?', [$search, $search, $search, $search, $search, $search]);
            });
        }

        if (!empty($input['status']) || $input['status'] == '0')
            $member_services->where('member_service_schedule.status', '=', $input['status']);

        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'name_asc':
                    $member_services->orderBy('nm_product.pro_title_en');
                    break;
                case 'name_desc':
                    $member_services->orderBy('nm_product.pro_title_en', 'desc');
                    break;
                case 'id_asc':
                    $member_services->orderBy('member_service_schedule.id');
                    break;
                case 'id_desc':
                    $member_services->orderBy('member_service_schedule.id', 'desc');
                    break;
                case 'new':
                    $member_services->orderBy('nm_order.order_date', 'desc');
                    break;
                case 'old':
                    $member_services->orderBy('nm_order.order_date', 'asc');
                    break;
                case 'merchant_asc':
                    $member_services->orderBy('nm_merchant.mer_fname', 'asc');
                    break;
                case 'merchant_desc':
                    $member_services->orderBy('nm_merchant.mer_fname', 'desc');
                    break;
                case 'store_asc':
                    $member_services->orderBy('nm_store.stor_name', 'asc');
                    break;
                case 'store_desc':
                    $member_services->orderBy('nm_store.stor_name', 'desc');
                    break;
                default:
                    $member_services->orderBy('member_service_schedule.id', 'desc');
                    break;
            }
        } else {
            $member_services->orderBy('member_service_schedule.id', 'desc');
        }

        // if (isset($input['export']) && $input['export'] == 'all')
        // {
        //     $member_services = $member_services->get();
        // }
        // else
        // {
        //     // dd($products->toSql());
        //     $member_services = $member_services->paginate(20);
        // }

        return $member_services->get();
    }

    public static function update_member_service($schedule_datetime, $member_service_schedule_id, $tz)
    {
        return MemberServiceSchedule::where('id', '=', $member_service_schedule_id)->update([
            'schedule_datetime' => $schedule_datetime,
            'status' => 1,
            'reschedule_count_member' => DB::raw('reschedule_count_member + 1'),
            'timezone' => $tz
            ]);
    }

    public static function update_member_service_by_merchant($schedule_datetime, $member_service_schedule_id)
    {
        return MemberServiceSchedule::where('id', '=', $member_service_schedule_id)->update([
            'schedule_datetime' => $schedule_datetime,
            'status' => 2,
            'reschedule_count_merchant' => DB::raw('reschedule_count_merchant + 1')
            ]);
    }

    public static function update_member_service_status_cancelled($member_service_schedule_id, $status)
    {
        return MemberServiceSchedule::where('id', '=', $member_service_schedule_id)->update([
            'status' => $status
            ]);
    }

    public static function update_member_service_status_cancelled_by_order_id($order_id, $status)
    {
        return MemberServiceSchedule::where('order_id', '=', $order_id)->update([
            'status' => $status
            ]);
    }

    public static function get_member_service_details_from_cus_id($cus_id, $status=null)
    {
        $member_service = MemberServiceSchedule::leftjoin('store_service_extra', 'member_service_schedule.store_service_extra_id', '=', 'store_service_extra.id')
        ->leftJoin('store_service_schedule', 'store_service_extra.store_service_schedule_id', '=', 'store_service_schedule.id')
        ->leftJoin('nm_product', 'store_service_schedule.pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_store', 'store_service_schedule.stor_id', '=', 'nm_store.stor_id')
        ->leftJoin('nm_order', 'member_service_schedule.order_id', '=', 'nm_order.order_id')
        ->selectRaw('member_service_schedule.*, nm_product.*, nm_store.*, store_service_extra.store_service_schedule_id as store_service_schedule_id, store_service_extra.service_name as service_name, store_service_extra.service_price as service_price, 
        store_service_schedule.pro_id as pro_id, store_service_schedule.interval_period as interval_period, store_service_schedule.stor_id as stor_id, store_service_schedule.appoint_start as appoint_start, store_service_schedule.appoint_end as appoint_end, store_service_schedule.schedule_skip_count as schedule_skip_count,
        nm_order.parent_order_id as parent_order_id,  nm_order.order_qty as order_qty, nm_order.product_price as product_price, nm_order.order_date as order_date, nm_order.order_status as order_status, nm_order.currency as currency')
        ->where('member_service_schedule.cus_id', '=', $cus_id);

        if (isset($status)) {
            $member_service->where('member_service_schedule.status', '=', $status);
        } 
        // ->whereIn('member_service_schedule.status', $status);
        // ->orderBy('member_service_schedule.schedule_datetime', 'desc')
        // ->orderBy('nm_order.order_date', 'desc');
        // ->get();

        return $member_service->orderBy('member_service_schedule.schedule_datetime', 'desc')->orderBy('nm_order.order_date', 'desc');
    }

    public static function get_service_details_from_cus_id($cus_id, $status=null)
    {
        $member_service = MemberServiceSchedule::leftjoin('store_service_extra', 'member_service_schedule.store_service_extra_id', '=', 'store_service_extra.id')
        ->leftJoin('store_service_schedule', 'store_service_extra.store_service_schedule_id', '=', 'store_service_schedule.id')
        ->leftJoin('nm_product', 'store_service_schedule.pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_store', 'store_service_schedule.stor_id', '=', 'nm_store.stor_id')
        ->leftJoin('nm_order', 'member_service_schedule.order_id', '=', 'nm_order.order_id')
        ->selectRaw('member_service_schedule.*, nm_product.*, nm_store.*, store_service_extra.store_service_schedule_id as store_service_schedule_id, store_service_extra.service_name as service_name, store_service_extra.service_price as service_price, 
        store_service_schedule.pro_id as pro_id, store_service_schedule.interval_period as interval_period, store_service_schedule.stor_id as stor_id, store_service_schedule.appoint_start as appoint_start, store_service_schedule.appoint_end as appoint_end, store_service_schedule.schedule_skip_count as schedule_skip_count,
        nm_order.parent_order_id as parent_order_id,  nm_order.order_qty as order_qty, nm_order.product_price as product_price, nm_order.order_date as order_date, nm_order.order_status as order_status, nm_order.currency as currency, nm_order.transaction_id as transaction_id')
        ->where('member_service_schedule.cus_id', '=', $cus_id)

        // if (isset($status)) {
        //     $member_service->where('member_service_schedule.status', '=', $status);
        // } 
        ->whereIn('member_service_schedule.status', $status)
        ->orderBy('member_service_schedule.schedule_datetime', 'desc')
        ->orderBy('nm_order.order_date', 'desc');
        // ->get();

        return $member_service;
    }

    public static function update_member_schedule_service_status($member_service_schedule_id, $status)
    {
        return $member_service_status = MemberServiceSchedule::where('id', $member_service_schedule_id)
        ->update(array('status' => $status));
    }

    public static function get_details_by_mer_id($mer_id, $status)
    {
        $member_service = MemberServiceSchedule::leftjoin('store_service_extra', 'member_service_schedule.store_service_extra_id', '=', 'store_service_extra.id')
        ->leftJoin('store_service_schedule', 'store_service_extra.store_service_schedule_id', '=', 'store_service_schedule.id')
        ->leftJoin('nm_product', 'store_service_schedule.pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_store', 'store_service_schedule.stor_id', '=', 'nm_store.stor_id')
        ->leftJoin('nm_order', 'member_service_schedule.order_id', '=', 'nm_order.order_id')
        ->selectRaw('member_service_schedule.*, nm_product.*, nm_store.*, store_service_extra.store_service_schedule_id as store_service_schedule_id, store_service_extra.service_name as service_name, store_service_extra.service_price as service_price, 
        store_service_schedule.pro_id as pro_id, store_service_schedule.interval_period as interval_period, store_service_schedule.stor_id as stor_id, store_service_schedule.appoint_start as appoint_start, store_service_schedule.appoint_end as appoint_end, store_service_schedule.schedule_skip_count as schedule_skip_count,
        nm_order.parent_order_id as parent_order_id,  nm_order.order_qty as order_qty, nm_order.product_price as product_price, nm_order.order_date as order_date, nm_order.transaction_id as transaction_id')
        ->where('nm_store.stor_merchant_id', '=', $mer_id);
        // ->where('member_service_schedule.status', '=', $status)
        // ->orderBy('member_service_schedule.schedule_datetime', 'desc')
        // ->orderBy('nm_order.order_date', 'desc')
        // ->get();

        if (isset($status)) {
            $member_service->where('member_service_schedule.status', '=', $status);
        }

        // return $member_service->orderBy('member_service_schedule.schedule_datetime', 'desc')->orderBy('nm_order.order_date', 'desc')->get();
        return $member_service->orderBy('member_service_schedule.schedule_datetime', 'desc')->orderBy('nm_order.order_date', 'desc');
    }

    public static function get_details_by_service_id($member_service_schedule_id)
    {
        $member_service = MemberServiceSchedule::leftjoin('store_service_extra', 'member_service_schedule.store_service_extra_id', '=', 'store_service_extra.id')
        ->leftJoin('store_service_schedule', 'store_service_extra.store_service_schedule_id', '=', 'store_service_schedule.id')
        ->leftJoin('nm_product', 'store_service_schedule.pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_store', 'store_service_schedule.stor_id', '=', 'nm_store.stor_id')
        ->leftJoin('nm_order', 'member_service_schedule.order_id', '=', 'nm_order.order_id')
        ->selectRaw('member_service_schedule.*, nm_product.*, nm_store.*, store_service_extra.store_service_schedule_id as store_service_schedule_id, store_service_extra.service_name as service_name, store_service_extra.service_price as service_price, 
        store_service_schedule.pro_id as pro_id, store_service_schedule.interval_period as interval_period, store_service_schedule.stor_id as stor_id, store_service_schedule.appoint_start as appoint_start, store_service_schedule.appoint_end as appoint_end, store_service_schedule.schedule_skip_count as schedule_skip_count,
        nm_order.parent_order_id as parent_order_id,  nm_order.order_qty as order_qty, nm_order.product_price as product_price, nm_order.order_date as order_date, nm_order.transaction_id as transaction_id')
        ->where('member_service_schedule.id', '=', $member_service_schedule_id)
        ->first();

        return $member_service;     
    }

    public static function getCustomerServiceScheduleByDate($date)
    {
        return MemberServiceSchedule::whereDate('schedule_datetime', $date)->get();
    }
}