<?php

namespace App\Repositories;
use App\Models\Subdistrict;

class SubdistrictRepo
{
	public static function get_subdistricts_by_city_id($city_id)
	{
		$subdistricts = Subdistrict::where('city_id', $city_id)->where('status', 1)->get();
		return $subdistricts;
	}
}