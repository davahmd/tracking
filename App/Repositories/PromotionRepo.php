<?php

namespace App\Repositories;

use DB;
use Helper;

use App\Models\Product;
use App\Models\Promotion;
use App\Models\PromotionProduct;
use App\Models\PromotionLog;
use App\Models\Category;
use App\Models\Cart;

use Carbon\Carbon;

class PromotionRepo
{
	public static function all($input)
	{
		$promotions = Promotion::query();

		if (!empty($input['merchant_id'])) {
			$promotions->where('merchant_id', $input['merchant_id']);
		}

		if (!empty($input['id']))
            $promotions->where('id', '=', $input['id']);

        if (!empty($input['name']))
            $promotions->where('name', 'LIKE', '%'.$input['name'].'%');

		if (!empty($input['status']) || $input['status'] == '0')
            $promotions->where('status', '=', $input['status']);

		if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'name_asc':
                    $promotions->orderBy('name');
                    break;
                case 'name_desc':
                    $promotions->orderBy('name', 'desc');
                    break;
                case 'id_asc':
                    $promotions->orderBy('id');
                    break;
                case 'id_desc':
                    $promotions->orderBy('id', 'desc');
                    break;
                case 'new':
                    $promotions->orderBy('created_at', 'desc');
                    break;
                case 'old':
                    $promotions->orderBy('created_at', 'asc');
                    break;
                default:
                    $promotions->orderBy('id', 'desc');
                    break;
            }
        } else {
            $promotions->orderBy('created_at', 'desc');
        }

        return $promotions->paginate(50);
	}

	public static function get_promotion($merchant_id, $promotion_id)
	{
		$promotion = Promotion::where([
			'id' => $promotion_id,
			'merchant_id' => $merchant_id,
		])->first();

		return $promotion;
	}

	public static function add($data, $merchant_id, $request_approval, $create_initiator, $create_uid)
	{
		if ($data['promo_type'] === 'F') {

			for ($i=0; $i < count($data['stores']); $i++) {
				$stores[] = $data['stores'][$i];
			}
			$stores = implode(',', $stores);

			for ($i=0; $i < count($data['products']); $i++) {
				$products[] = $data['products'][$i];
			}

			$products = implode(',', $products);

			$promotion = Promotion::create([
	            'name' => $data['name'],
	            'merchant_id' => $merchant_id,
	            'stores' => $stores,
	            'products' => $products,
	            'promo_type' => $data['promo_type'],
	            'discount_rate' => ($data['discount_type'] == 1) ? bcdiv($data['value'], '100', 4) : null,
	            'discount_value' => ($data['discount_type'] == 2) ? $data['value'] : null,
	            'started_at' => $data['start'],
	            'ended_at' => $data['end'],
	            'request' => ($merchant_id != 0 && $request_approval == true) ? 'A' : null,
	            'status' => ($merchant_id == 0 && $request_approval == true) ? 1 : 0,
	            'create_initiator' => $create_initiator,
	            'create_uid' => $create_uid,
	        ]);

			for ($i=0; $i < count($data['products']); $i++) { 
				PromotionProduct::insert([
					'promotion_id' => $promotion->id,
					'product_id' => $data['products'][$i],
					'limit' => $data['quantity'][$i],
				]);
			}
		}
		elseif ($data['promo_type'] === 'P') {

			if (in_array('P', $data['applied_to']) && in_array('S', $data['applied_to'])) {
				$applied_to = 2;
			}
			elseif (in_array('S', $data['applied_to'])) {
				$applied_to = 1;
			}
			elseif (in_array('P', $data['applied_to'])) {
				$applied_to = 0;
			}

			$products = null;
			$categories = null;

			for ($i=0; $i < count($data['stores']); $i++) {
				$stores[] = $data['stores'][$i];
			}
			$stores = implode(',', $stores);

			if (isset($data['products']) > 0) {
				for ($i=0; $i < count($data['products']); $i++) {
					$products[] = $data['products'][$i];
				}
				$products = implode(',', $products);
			} 
			elseif (isset($data['categories']) > 0) {
				for ($i=0; $i < count($data['categories']); $i++) {
					$categories[] = $data['categories'][$i];
				}
				$categories = implode(',', $categories);
			}

			$promotion = Promotion::create([
				'name' => $data['name'],
				'merchant_id' => $merchant_id,
				'stores' => $stores,
				'categories' => $categories,
				'products' => $products,
				'promo_type' => $data['promo_type'],
				'promo_code' => $data['promo_code'],
				'promo_code_limit' => $data['promo_code_limit'],
				'promo_min_spend' => $data['promo_min_spend'],
				'discount_rate' => ($data['discount_type'] == 1) ? bcdiv($data['value'], '100', 4) : null,
	            'discount_value' => ($data['discount_type'] == 2) ? $data['value'] : null,
	            'applied_to' => $applied_to,
	            'started_at' => $data['start'],
	            'ended_at' => $data['end'],
	            'request' => ($merchant_id !=0 && $request_approval == true) ? 'A' : null,
	            'status' => ($merchant_id == 0 && $request_approval == true) ? 1 : 0,
	            'create_initiator' => $create_initiator,
	            'create_uid' => $create_uid,
			]);

		}

        if ($merchant_id == 0 && $request_approval == true) {
        	PromotionLog::insert([
	        	'promotion_id' => $promotion->id,
	        	'action' => 'A', // Approve
	        	'initiator_type' => $create_initiator,
	        	'initiator_uid' => $create_uid,
	        	'approver_uid' => $create_uid,
        	]);
        }

        return $promotion;
	}

	public static function update($data, $promotion_id, $request_approval)
	{
		$promotion = Promotion::find($promotion_id);

		$promotion->name = $data['name'];
		$promotion->discount_rate = ($data['discount_type'] == 1) ? bcdiv($data['value'], '100', 4) : null;
		$promotion->discount_value = ($data['discount_type'] == 2) ? $data['value'] : null;
		$promotion->started_at = $data['start'];
		$promotion->ended_at = $data['end'];

		if ($data['promo_type'] === 'F') {

			for ($i=0; $i < count($data['products']); $i++) {
				$products[] = $data['products'][$i];
			}

			PromotionProduct::where('promotion_id', $promotion_id)->delete();

			for ($i=0; $i < count($data['products']); $i++) { 
				PromotionProduct::insert([
					'promotion_id' => $promotion->id,
					'product_id' => $data['products'][$i],
					'limit' => $data['quantity'][$i],
				]);
			}

			$products = implode(',', $products);

			$promotion->products = $products;
		}
		elseif ($data['promo_type'] === 'P') {

			$products = null;
			$categories = null;

			for ($i=0; $i < count($data['stores']); $i++) {
				$stores[] = $data['stores'][$i];
			}
			$stores = implode(',', $stores);

			if (isset($data['products']) > 0) {
				for ($i=0; $i < count($data['products']); $i++) {
					$products[] = $data['products'][$i];
				}
				$products = implode(',', $products);
			} 
			elseif (isset($data['categories']) > 0) {
				for ($i=0; $i < count($data['categories']); $i++) {
					$categories[] = $data['categories'][$i];
				}
				$categories = implode(',', $categories);
			}

			$promotion->stores = $stores;
			$promotion->categories = $categories;
			$promotion->products = $products;
			$promotion->promo_code = $data['promo_code'];
			$promotion->promo_code_limit = $data['promo_code_limit'];
			$promotion->promo_min_spend = $data['promo_min_spend'];

			if (in_array('P', $data['applied_to']) && in_array('S', $data['applied_to'])) {
				$applied_to = 2;
			}
			elseif (in_array('S', $data['applied_to'])) {
				$applied_to = 1;
			}
			elseif (in_array('P', $data['applied_to'])) {
				$applied_to = 0;
			}

			$promotion->applied_to = $applied_to;
		}

		if($request_approval == true) {
			if($promotion->merchant_id == 0) { // awaiting approval is not needed for zona/admin
				$promotion->status = 1;

				$promotion_log = PromotionLog::insert([
					'promotion_id' => $promotion->id,
					'action' => 'A',
					'initiator_type' => $promotion->create_initiator,
					'initiator_uid' => $promotion->create_uid,
					'approver_uid' => $promotion->create_uid,
				]);
			}
			else { // merchant submit approval request
				$promotion->request = 'A';
			}
		}

		$promotion->save();

		return $promotion;
	}

	public static function submit($promotion_id, $create_initiator, $create_uid)
	{
		$promotion = Promotion::find($promotion_id);

		if ($create_initiator === 'A') {
			$promotion->status = 1;

			$promotion_log = PromotionLog::insert([
				'promotion_id' => $promotion->id,
				'action' => 'A',
				'initiator_type' => $promotion->create_initiator,
				'initiator_uid' => $promotion->create_uid,
				'approver_uid' => $promotion->create_uid,
			]);
		}
		else {
			$promotion->request = 'A';
		}

		$promotion->save();

		return $promotion;
	}

	public static function cancel($promotion_id, $initiator_type, $initiator_uid)
	{
		$promotion = Promotion::find($promotion_id);

		if ($promotion->status == 1) {
			$promotion->request = 'C';
		}
		elseif ($promotion->status == 0) {
			$promotion->status = -1;
		}

		$promotion->save();

		return $promotion;
	}

	public static function approve($promotion_id, $admin_id)
	{
		$promotion = Promotion::find($promotion_id);

		$promotion_log = PromotionLog::insert([
			'promotion_id' => $promotion->id,
			'action' => 'A',
			'initiator_type' => $promotion->create_initiator,
			'initiator_uid' => $promotion->create_uid,
			'approver_uid' => $admin_id,
		]);

		$promotion->status = 1;
		$promotion->request = null;

		$promotion->save();

		return $promotion;
	}

	public static function deny($promotion_id, $admin_id, $data)
	{
		$promotion = Promotion::find($promotion_id);

		$promotion_log = PromotionLog::insert([
			'promotion_id' => $promotion->id,
			'action' => 'C',
			'initiator_type' => $promotion->create_initiator,
			'initiator_uid' => $promotion->create_uid,
			'approver_uid' => $admin_id,
			'reason' => $data['reason'],
		]);

		$promotion->status = -1;
		$promotion->save();

		return $promotion;
	}

	public static function new_pricing_by_promotion($pro_id)
	{
		$now = Carbon::now();

		$promotion = Promotion::where('status', 1)
			->whereRaw("FIND_IN_SET(". $pro_id .", products)")
			->where('promo_type', 'F')
			->where('started_at', '<=', $now)
			->where('ended_at', '>=', $now)
			->first();

		if ($promotion) {
			$product = PromotionProduct::where('promotion_id', $promotion->id)
				->where('product_id', $pro_id)
				->whereColumn('count', '<', 'limit')
				->first();

			if ($product) {
				return $promotion;
			}
		}

		return false;
	}

	public static function getFlashProducts($limit = null)
	{
		// $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
		$today_datetime = Carbon::now();

		$promotions = Promotion::where('status', 1)->where('promo_type', 'F')->where('started_at', '<=', $today_datetime)->where('ended_at', '>=', $today_datetime)->get();
		$promotion_items = PromotionProduct::whereColumn('limit', '>', 'count')->whereIn('promotion_id', $promotions->pluck('id'))->whereHas('product_details', function ($q) {
				$q->where('pro_status', 1);
		})->get();

		if ($limit) {
			$promotion_items = PromotionProduct::whereColumn('limit', '>', 'count')->whereIn('promotion_id', $promotions->pluck('id'))->limit($limit)->whereHas('product_details', function ($q) {
					$q->where('pro_status', 1);
			})->get();
		}

		$promotion_items_info = [];

		foreach($promotion_items as $item) {
			$promotion_items_info[] = Product::active()->where('pro_id', $item->product_id)->first();
		}

		return $promotion_items_info;

	}

	public static function checkPromoMinSpend($promotion_id, $promo_products)
	{
		$spending = 0;

		foreach ($promo_products as $product) {
			$spending += $product->charge->amount->subtotal;
		}

		$promotion = Promotion::where('id', $promotion_id)
			->where('promo_min_spend', '<=', $spending)
			->firstOrFail();

		return $promotion;
	}

	public static function getPromoCode($promo_code, $products)
	{
		$now = Carbon::now();

		$promotion = Promotion::where('promo_code', $promo_code)
			->where('promo_type', 'P')
			->where('status', 1)
			->where('started_at', '<=', $now)
			->where('ended_at', '>=', $now)
			->firstOrFail();

		if ($promotion->promo_code_limit) {
			$promotion = Promotion::where('promo_code', $promo_code)
				->where('promo_type', 'P')
				->where('status', 1)
				->where('started_at', '<=', $now)
				->where('ended_at', '>=', $now)
				->whereColumn('promo_code_count', '<', 'promo_code_limit')->firstOrFail();
		}

		if ($promotion) {
			if ($promotion->products) {

				$promo_products = explode(',', $promotion->products);

				$promotion['promo_products'] = array_intersect($promo_products, $products);
				$promotion['promo_products_stores'] = Product::whereIn('pro_id', $promotion['promo_products'])->pluck('pro_sh_id')->unique()->toArray();
			}
			else if ($promotion->categories) {

				$promo_stores = explode(',', $promotion->stores);

				$promo_categories = explode(',', $promotion->categories);

				$categories = Category::whereIn('id', $promo_categories)->get();

				$promo_products = [];

				foreach ($categories as $category) {
					foreach ($category->products as $product) {
						array_push($promo_products, $product->pro_id);
					}
				}

				$promo_products = Product::whereIn('pro_id', $promo_products)->whereIn('pro_sh_id', $promo_stores)->pluck('pro_id')->toArray();

				$promotion['promo_products'] = $promo_products;
				$promotion['promo_products_stores'] = Product::whereIn('pro_id', $promotion['promo_products'])->pluck('pro_sh_id')->unique()->toArray();
			}
			else if ($promotion->stores) {

				$promo_stores = explode(',', $promotion->stores);

				$promotion['promo_products'] = Product::whereIn('pro_sh_id', $promo_stores)->pluck('pro_id')->toArray();
				$promotion['promo_products_stores'] = Product::whereIn('pro_id', $promotion['promo_products'])->pluck('pro_sh_id')->unique()->toArray();
			}
		}

		return $promotion;
	}

	public static function updateCartPromoId($customer_id, $carts, $promotion)
	{
		foreach ($carts as $cart) {
			$promo_product = in_array($cart->product_id, $promotion->promo_products);

			if ($promo_product) {
				Cart::where('cus_id', $customer_id)->where('product_id', $cart->product_id)->update(['promo_id' => $promotion->id]);
			}
		}
	}

	public static function validatePromoItem($carts, $promotion)
	{
		$promo_products = [];

		foreach ($carts as $cart) {
			
			$promo_product = in_array($cart->product_id, $promotion->promo_products);

			if ($promo_product) {
				array_push($promo_products, $cart);
			}
		}

		return $promo_products;
	}

	public static function validatePromoCodeDateRange($promo_code)
	{
		$now = Carbon::now();

		$promotion = Promotion::where('promo_code', $promo_code)
			->where('promo_type', 'P')
			->where('status', 1)
			->where('started_at', '<=', $now)
			->where('ended_at', '>=', $now)
			->firstOrFail();

		return $promotion;
	}

	public static function validatePromoCodeCount($promo_code)
	{
		$now = Carbon::now();

		$promotion = Promotion::where('promo_code', $promo_code)
			->where('promo_type', 'P')
			->where('status', 1)
			->where('started_at', '<=', $now)
			->where('ended_at', '>=', $now)
			->firstOrFail();

		if ($promotion->promo_code_limit) {
			$promotion = Promotion::where('promo_code', $promo_code)
				->where('promo_type', 'P')
				->where('status', 1)
				->where('started_at', '<=', $now)
				->where('ended_at', '>=', $now)
				->whereColumn('promo_code_count', '<', 'promo_code_limit')->firstOrFail();
		}

		return $promotion;
	}

	public static function validatePromoCode($promo_code, $products)
	{
		$promotion = Promotion::where('promo_code', $promo_code)
			->where('promo_type', 'P')
			->where('status', 1)
			->firstOrFail();

		if ($promotion) {
			if ($promotion->products) {

				$promo_products = explode(',', $promotion->products);

				$promotion['promo_products'] = array_intersect($promo_products, $products);
				$promotion['promo_products_stores'] = Product::whereIn('pro_id', $promotion['promo_products'])->pluck('pro_sh_id')->unique()->toArray();
			}
			else if ($promotion->categories) {

				$promo_stores = explode(',', $promotion->stores);

				$promo_categories = explode(',', $promotion->categories);

				$categories = Category::whereIn('id', $promo_categories)->get();

				$promo_products = [];

				foreach ($categories as $category) {
					foreach ($category->products as $product) {
						array_push($promo_products, $product->pro_id);
					}
				}

				$promo_products = Product::whereIn('pro_id', $promo_products)->whereIn('pro_sh_id', $promo_stores)->pluck('pro_id')->toArray();

				$promotion['promo_products'] = $promo_products;
				$promotion['promo_products_stores'] = Product::whereIn('pro_id', $promotion['promo_products'])->pluck('pro_sh_id')->unique()->toArray();
			}
			else if ($promotion->stores) {

				$promo_stores = explode(',', $promotion->stores);

				$promotion['promo_products'] = Product::whereIn('pro_sh_id', $promo_stores)->pluck('pro_id')->toArray();
				$promotion['promo_products_stores'] = Product::whereIn('pro_id', $promotion['promo_products'])->pluck('pro_sh_id')->unique()->toArray();
			}
		}

		return $promotion;
	}

	public static function validateFlashsaleProducts($products, $started_at, $ended_at)
	{
		$flashsale_promotions = Promotion::where('promo_type', 'F')
            ->where('status', 1)
            ->where('started_at', '<=', $ended_at)
            ->where('ended_at', '>=', $started_at)
            ->pluck('id');

        $products = PromotionProduct::whereIn('promotion_id', $flashsale_promotions)
        	->whereIn('product_id', $products)
            ->whereColumn('count', '<', 'limit')
            ->get();

        return $products;
	}
}