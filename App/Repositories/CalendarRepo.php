<?php

namespace App\Repositories;

use Carbon\Carbon;
use DB;

use App\Models\Holiday;
use App\Models\Offday;
use App\Models\Onday;

class CalendarRepo
{

    public function get_holiday_by_id($id)
    {
        return Holiday::find($id);
    }

    public function get_holiday_by_date($date)
    {
        return Holiday::where('start_date','<=', $date)
            ->where('end_date', '>', $date)
            ->first();
    }

    public function get_all_holidays()
    {
        return Holiday::select('id','name AS title','start_date AS start','end_date AS end')
            ->orderBy('start_date', 'asc')
            ->get();
    }

    public function add_holiday($name, $start, $end)
    {
        return Holiday::create([
            'name' => $name,
            'start_date' => $start,
            'end_date' => $end,
        ]);
    }

    public function remove_holiday($id)
    {
        $holiday = $this->get_holiday_by_id($id);

        return $holiday->delete();
    }

    public function get_offday_by_id($id) {
        return Offday::find($id);
    }

    public function remove_offday($id)
    {
        $offday = $this->get_offday_by_id($id);

        return $offday->delete();
    }

    public function get_merchant_offdays_by_id($mer_id, $pagination = false, $chunk = 10)
    {
        if ($pagination) {
            return Offday::where('merchant_id', $mer_id)->paginate($chunk);
        }

        return Offday::where('merchant_id', $mer_id)
            ->select('id','name AS title','start_date AS start','end_date AS end')
            ->get();
    }

    public function get_store_offdays_by_id($stor_id, $pagination = false, $chunk = 10)
    {
        if ($pagination) {
            return Offday::where('store_id', $stor_id)->paginate($chunk);
        }

        return Offday::where('store_id', $stor_id)
            ->select('id','name AS title','start_date AS start','end_date AS end')
            ->get();
    }

    public function merchant_add_offday($mer_id, $name, $start, $end)
    {
        return Offday::create([
            'merchant_id' => $mer_id,
            'name' => $name,
            'start_date' => $start,
            'end_date' => $end,
        ]);
    }

    public function store_add_offday($store_id, $name, $start, $end)
    {
        return Offday::create([
            'store_id' => $store_id,
            'name' => $name,
            'start_date' => $start,
            'end_date' => $end,
        ]);
    }

    public function get_store_offday_by_date($store_id, $date)
    {
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');

        $offday = Offday::where('store_id', $store_id)
            ->where('start_date', $date)
            ->first();

        if ($offday) {
            return $offday;
        }

        $offday = Offday::where('store_id', $store_id)
            ->where('start_date','<=', $date)
            ->where('end_date', '>', $date)
            ->first();

        return $offday?:false;
    }

    public function get_merchant_offday_by_date($merchant_id, $date)
    {
        $offday = Offday::where('merchant_id', $merchant_id)
            ->where('start_date', $date)
            ->first();

        if ($offday) {
            return $offday;
        }

        $offday = Offday::where('merchant_id', $merchant_id)
            ->where('start_date','<=', $date)
            ->where('end_date', '>', $date)
            ->first();

        return $offday?:false;
    }

    public function whitelist_holiday($merchant_id, $start, $end)
    {
        $onday = Onday::create([
            'merchant_id' => $merchant_id,
            'name' => 'Whitelisted rule',
            'start_date' => $start,
            'end_date' => $end,
        ]);

        $onday->update([
            'name' => 'Whitelist rule #' . $onday->id,
        ]);

        return $onday;
    }

    public function whitelist_offday($store_id, $start, $end)
    {
        return Onday::create([
            'store_id' => $store_id,
            'start_date' => $start,
            'end_date' => $end,
        ]);
    }

    public function check_merchant_existing_onday($merchant_id, $date)
    {
        return Onday::where('merchant_id', $merchant_id)
            ->whereDate('start_date','=', $date)
            ->first();
    }

    public function get_all_merchant_whitelisted_holiday($merchant_id) {

        return Onday::where('merchant_id',$merchant_id)
            ->select('id','name AS title','start_date AS start','end_date AS end')
            ->orderBy('start_date', 'asc')
            ->get();
    }

    public function remove_whitelisted_rule($rule_id) {

        $rule = Onday::find($rule_id);

        return $rule->delete();
    }
}
