<?php

namespace App\Repositories;
use App\Models\ProductPricing;
use App\Models\ProductAttribute;
use App\Models\Cart;
use App\Models\PricingAttributeMapping;
use App\Models\Product;
use App\Models\Store;
use App\Models\StoreServiceSchedule;

class StoreServiceScheduleRepo
{
    public static function get_details_by_pro_id($pro_id)
    {
        $service_store_schedule = StoreServiceSchedule::where('pro_id', '=', $pro_id)->first();
        // $details = StoreServiceSchedule::where('pro_id', '=', $pro_id)->first();

        return $service_store_schedule;
    }

    public static function create_service_store_add_product($data, $pro_id)
    {
        $storeserviceschedule = StoreServiceSchedule::create([
            'pro_id' => $pro_id,
            'stor_id' => $data['stor_id_service'],
            'interval_period' =>isset($data['interval_period']) ? $data['interval_period'] : null,
            'appoint_start' => isset($data['appoint_start']) ? $data['appoint_start'] : null,
            'appoint_end' => isset($data['appoint_end']) ? $data['appoint_end'] : null,
        ]);

        return $storeserviceschedule;
    }

    public static function create_service_store($data, $pro_id)
    {
        $storeserviceschedule = StoreServiceSchedule::create([
            'pro_id' => $pro_id,
            'stor_id' => $data['stor_id'],
            'interval_period' =>isset($data['interval_period']) ? $data['interval_period'] : null,
            'appoint_start' => isset($data['appoint_start']) ? $data['appoint_start'] : null,
            'appoint_end' => isset($data['appoint_end']) ? $data['appoint_end'] : null,
        ]);

        return $storeserviceschedule;
    }

    public static function edit_service($service_stores_schedule_id, $pro_id, $data)
    {
        $storeserviceschedule = StoreServiceSchedule::find($service_stores_schedule_id);
        $storeserviceschedule->stor_id = $data['stor_id'];
        $storeserviceschedule->interval_period = $data['interval_period'];
        $storeserviceschedule->appoint_start = $data['appoint_start'];
        $storeserviceschedule->appoint_end = $data['appoint_end'];

        $storeserviceschedule->save();

        return $storeserviceschedule;
    }
}