<?php

namespace App\Repositories;

use App\Models\OfficialBrands;
use App;
use Cache;

class OfficialBrandsRepo
{
    public static function all($input)
    {
        $officialbrands = OfficialBrands::query();

        if (!empty($input['id']))
            $officialbrands->where('brand_id', '=', $input['id']);

        if (!empty($input['name']))
            $officialbrands->where('brand_name', 'LIKE', '%'.$input['name'].'%');

        if (!empty($input['status']) || $input['status'] == '0')
            $officialbrands->where('brand_status', '=', $input['status']);

        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'id_asc':
                    $officialbrands->orderBy('brand_id', 'asc');
                    break;
                case 'id_desc':
                    $officialbrands->orderBy('brand_id', 'desc');
                    break;
                case 'new':
                    $officialbrands->orderBy('created_at', 'desc');
                    break;
                case 'old':
                    $officialbrands->orderBy('created_at', 'asc');
                    break;
                default:
                    $officialbrands->orderBy('brand_id', 'desc');
                    break;
            }
        } else {
            $officialbrands->orderBy('brand_id', 'desc');
        }

        return $officialbrands->paginate(50);
    }

    public static function get_brand_status($brand_id, $type)
    {

        $officialbrands = OfficialBrands::findorfail($brand_id);

        switch ($type) {
            case 'unblocked':
                $officialbrands->brand_status = 1;
                break;

            case 'blocked':
                $officialbrands->brand_status = 0;
                break;

            default:
                break;
        }
        $officialbrands->save();

        return $officialbrands;
    }


    public static function get_official_brands_details($brand_id)
    {
        return OfficialBrands::find($brand_id);
    }

    public static function update_officialbrand_status($brand_id, $status)
    {
        $officialbrand = OfficialBrands::where('brand_id','=',$brand_id)->first();
        $officialbrand->brand_status = $status;
        $officialbrand->save();

        return $officialbrand;
    }

    public static function add_brands($data)
    {
        $officialbrands = OfficialBrands::create([
            'brand_name' => $data['brands_name'],
            'brand_mkeywords' => $data['metakeyword'],
            'brand_mdesc' => $data['metadescription'],
            'brand_slug' => $data['slug'],
            'brand_desc' => $data['brand_desc'],
            'brand_status' => 1,
            'brand_Img' => $data['img']
        ]);

        return $officialbrands;
    }

    public static function edit_officialbrand($data, $brand_id)
    {
        $officialbrand = OfficialBrands::find($brand_id);
        $officialbrand->brand_name = $data['brands_name'];
        $officialbrand->brand_mkeywords = $data['metakeyword'];
        $officialbrand->brand_mdesc = $data['metadescription'];
        $officialbrand->brand_slug = $data['slug'];
        $officialbrand->brand_desc = $data['brand_desc'];
        $officialbrand->save();

        return $officialbrand;
    }

    public static function upload_brand_image($id, $new_file_name)
    {
        $brand = OfficialBrands::find($id);
        $brand->brand_Img = $new_file_name;
        $brand->save();

        return $brand;
    }

    public static function get_active_brands()
    {
        return OfficialBrands::where('brand_status', 1)->get();
    }

    public function get_official_brands($limit = null)
    {
        return OfficialBrands::active()
            ->select('brand_id','brand_name','brand_status','brand_slug','brand_Img')
            ->limit($limit)
            ->cursor();
    }

    public function m_get_official_brands()
    {
        return OfficialBrands::active()
            ->select('brand_id','brand_name','brand_status','brand_Img')
            ->get();
    }

    public static function get_brand_by_slug($brand_slug)
    {
        return OfficialBrands::where('brand_slug', $brand_slug)
            ->active()
            ->first();
    }




}
