<?php

namespace App\Repositories;

use App\Repositories\GeneratedCodeRepo;
use App\Repositories\ProductPricingRepo;
use App\Repositories\LimitRepo;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Order;
use App\Models\Merchant;
use App\Models\Shipping;
use App\Models\ProductImage;
use App\Models\ProductPricing;
use App\Models\Country;
use App\Models\MemberServiceSchedule;
use App\Models\MerchantVTokenLog;
use App\Models\Store;
use App\Models\StoreUserMapping;
use App\Models\ParentOrder;
use App\Models\DeliveryOrder;
use App\Models\TaxInvoice;
use App\Models\OrderMapping;
use App\Services\Notifier\Notifier;
use App\Traits\OrderOnlineLogger;
use Carbon\Carbon;
use Session;
use Mail;
use DB;

class OrderRepo
{
    public static function update_token($token, $uid)
    {
        return Cart::where('cus_id', '=', $uid)->update(['token' => $token]);
    }

    public static function update_user($token, $uid)
    {
        return Cart::where('token', '=', $token)->whereNull('cus_id')->update(['cus_id' => $uid]);
    }

    public static function get_shopping_carts($token, $uid, $cid = null)
    {
        if (!empty($token) || $uid > 0 ) {
            if(!$cid)
                $cid = session('countryid');

            $shoppingcarts = array();
            $carts = new Cart;
            $carts = $carts->select('temp_cart.*','nm_product.*', 'nm_merchant.mer_platform_charge', 'nm_merchant.mer_service_charge','temp_cart.attributes');
            $carts = $carts->leftJoin('nm_product', 'nm_product.pro_id', '=', 'temp_cart.product_id');
            $carts = $carts->where('nm_product.pro_status', '=', 1);
            $carts = $carts->leftJoin('nm_product_pricing','nm_product_pricing.id','=','temp_cart.pricing_id');
            $carts = $carts->where('nm_product_pricing.country_id','=',$cid);
            $carts = $carts->where('nm_product_pricing.status','=', 1);
            $carts = $carts->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id');
            $carts = $carts->where('nm_store.stor_status', '=', 1);
            $carts = $carts->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id');
            $carts = $carts->where('nm_merchant.mer_staus', '=', 1);

            $carts = $carts->where(function($q) use ($token, $uid) {
                if (!empty($token)) {
                    $q->orWhere('token', '=', $token);
                }

                if ($uid != 0) {
                    $q->orWhere('cus_id', '=', $uid);
                }
            });

            $carts = $carts->get();

            foreach ($carts as $key => $cart) {
                $shoppingcarts[$key]['product'] = Product::where('pro_id', '=', $cart->product_id)->first();
                $shoppingcarts[$key]['main_image'] = ProductImage::where('pro_id', '=', $cart->product_id)->orderBy('main', 'desc')->orderBy('order', 'asc')->first();
                $pricing = ProductPricing::where('id', '=', $cart->pricing_id)->where('pro_id','=',$cart->product_id)->first();
                $pricing->attributes = AttributeRepo::get_pricing_attribute_id_json($pricing->id);
                $pricing->attributes_name = AttributeRepo::get_pricing_attributes_name($pricing->id);
                $shoppingcarts[$key]['pricing'] = $pricing;
                $cart->attribute_changes = AttributeRepo::check_attribute_changes_cart_and_pricing($cart->attributes, $pricing->attributes);
                $shoppingcarts[$key]['cart'] = $cart;
                //only triggered when no errors then pricing can be updated
                if(empty(Session::get('errors')))
                    self::update_cart_price($pricing, $cart->id);
            }
            return $shoppingcarts;
        }

        return array();
    }

    public static function check_cart_quantity($token, $uid, $cid)
    {
        $carts = Cart::select('temp_cart.*', 'nm_product.pro_title_en', 'nm_product_pricing.quantity as curr_qty', 'nm_product.end_date as expired_at', 'nm_product.pro_type as product_type', 'nm_product.pro_id', 'nm_store.accept_payment')
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'temp_cart.product_id')
        ->leftJoin('nm_product_pricing','nm_product_pricing.id','=','temp_cart.pricing_id')
        ->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('nm_product.pro_status', '=', 1)
        ->where('nm_product_pricing.country_id','=',$cid)
        ->where('nm_product_pricing.status','=', 1)
        ->where('nm_store.stor_status', '=', 1)
        ->where('nm_merchant.mer_staus', '=', 1)
        ->where(function($q) use ($token, $uid) {
            if (!empty($token)) {
                $q->orWhere('token', '=', $token);
            }

            if ($uid != 0) {
                $q->orWhere('cus_id', '=', $uid);
            }
        })->get();

        $response = array();
        foreach ($carts as $key => $cart) {
            $total_pro_quantity_in_cart = (int)$cart->quantity;
            $total_pro_quantity = (int)$cart->curr_qty;

            $response[$key] = [
                'pro_title' => $cart->pro_title_en,
                'total_quantity_in_cart' => $total_pro_quantity_in_cart,
                'pro_qty' => $total_pro_quantity,
                'status' => ($total_pro_quantity_in_cart<1 || $total_pro_quantity_in_cart > $total_pro_quantity) ? 0 : 1,
                'expired' => ($cart->product_type == 3 && !empty($cart->expired_at) && Carbon::now('UTC') >= Carbon::parse($cart->expired_at))? true : false,
                'pro_id' => $cart->pro_id,
                'accept_payment' => $cart->accept_payment,
                'cart_id' => $cart->id,
                'exceed' => [
                    'productLimit' => $uid? LimitRepo::check_payment_limitation('productLimit', $total_pro_quantity_in_cart, null, $uid, $cart->pro_id) : false
                ],
            ];
        }

        return $response;
    }

    public static function check_exist($token, $uid, $data)
    {
        return Cart::where('product_id', '=', $data['product_id'])
            ->where('pricing_id','=',$data['price_id'])
            ->where('remarks', '=', $data['remarks'])
            ->where(function($q) use ($token, $uid) {
                if (isset($uid))
                    $q->where('cus_id', '=', $uid);
                else
                    $q->where('token', '=', $token);
            })->first();
    }

    public static function get_quantity_of_product($token, $uid, $pro_id)
    {
        $total = Cart::select('pricing_id','quantity')->where('product_id', '=', $pro_id)
        ->where(function($q) use ($token, $uid) {
            if (isset($uid))
                $q->where('cus_id', '=', $uid);
            else
                $q->where('token', '=', $token);
        })
        ->get();

        $total_array = [];
        foreach ($total as $key => $price) {
            $total_array[$price->pricing_id] = $price->quantity;
        }

        return $total_array;
    }

    public static function update($id, $qty)
    {
        return Cart::where('id', '=', $id)->update(['quantity' => $qty]);
    }

    public static function delete($id)
    {
        return Cart::where('id', '=', $id)->delete();
    }

    public static function check_trans_id($id)
    {
        return Order::where('transaction_id', '=', $id)->first();
    }

    public static function get_carts_total($token, $uid, $payment_type, $cid = null)
    {
        if(!$cid)
            $cid = session('countryid');

        $shoppingcarts = array();

        $carts = Cart::select('temp_cart.*', 'nm_product.pro_title_en', 'nm_product.pro_qty', 'nm_merchant.mer_platform_charge', 'nm_merchant.mer_service_charge')
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'temp_cart.product_id')
        ->leftJoin('nm_product_pricing','nm_product_pricing.id','=','temp_cart.pricing_id')
        ->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('nm_product.pro_status', '=', 1)
        ->where('nm_product_pricing.country_id','=',$cid)
        ->where('nm_product_pricing.status','=', 1)
        ->where('nm_store.stor_status', '=', 1)
        ->where('nm_merchant.mer_staus', '=', 1)
        ->where(function($q) use ($token, $uid) {
            if (!empty($token)) {
                $q->orWhere('token', '=', $token);
            }

            if ($uid != 0) {
                $q->orWhere('cus_id', '=', $uid);
            }
        })->get();

        $co_rate = Country::where('co_id','=',$cid)->value('co_rate');
        $platform_charge = round(\Config::get('settings.platform_charge'));
        $service_charge = round(\Config::get('settings.service_charge'));
        $grandtotal_price = 0;
        $grandtotal_credit = 0;
        $total = 0;
        $cart_grandtotal_price = 0;
        $cart_grandtotal_credit = 0;
        $cart_total = 0;

        $total_for_wallet_type = [];

        foreach ($carts as $key => $cart) {
            $pro_price = ProductPricing::where('id', '=', $cart->pricing_id)->where('pro_id','=',$cart->product_id)->first();
            $wallet_id = WalletRepo::get_wallet_id_by_product_id_based_on_product_category($cart->product_id);

            $quantity = $cart->quantity;
            $price = $pro_price->price;

            if ( $pro_price->discounted_price > 0.00) {
                $today = new \DateTime;
                $discounted_from = new \DateTime($pro_price->discounted_from);
                $discounted_to = new \DateTime($pro_price->discounted_to);

                if (($today >= $discounted_from) && ($today <= $discounted_to))
                {
                    $price = $pro_price->discounted_price;
                }
            }

            // shipping fees
            if ($pro_price->shipping_fees_type == 1) {
                $shippingfees = round(($pro_price->shipping_fees * $quantity), 2);
            } elseif ($pro_price->shipping_fees_type == 2) {
                $shippingfees = round(($pro_price->shipping_fees), 2);
            } else {
                $shippingfees = 0.00;
            }
            $shippingfees_credit = round(($shippingfees / $co_rate), 4);

            $purchasing_price = round(($price * $quantity), 2);
            $product_credit = round(($purchasing_price / $co_rate), 4);

            $platform_charge_value = round($product_credit * ($cart->mer_platform_charge/100), 4);
            $service_charge_value = round(($product_credit + $platform_charge_value)* ($cart->mer_service_charge/100), 4);

            $purchasing_credit = round(($product_credit + $platform_charge_value + $service_charge_value + $shippingfees_credit), 4);

            $grandtotal_price += $purchasing_price;
            $grandtotal_credit += $purchasing_credit;

            // cart
            $cart_purchasing_price = round(($cart->product_price * $quantity), 2);
            $cart_product_credit = round(($cart_purchasing_price / $co_rate), 4);

            $cart_platform_charge_value = round($cart_product_credit * ($cart->mer_platform_charge/100), 4);
            $cart_service_charge_value = round(($cart_product_credit + $cart_platform_charge_value) * ($cart->mer_service_charge/100), 4);

            $cart_purchasing_credit = round(($cart_product_credit + $cart_platform_charge_value + $cart_service_charge_value + $shippingfees_credit), 4);

            $cart_grandtotal_price += ($cart_purchasing_price + $shippingfees);
            $cart_grandtotal_credit += $cart_purchasing_credit;

            if(array_key_exists($wallet_id, $total_for_wallet_type)) {
                $total_for_wallet_type[$wallet_id] += $cart_purchasing_credit;
            } else {
                $total_for_wallet_type[$wallet_id] = $cart_purchasing_credit;
            }
        }

        if($payment_type == 'credit') {
            $total = $grandtotal_credit;
            $cart_total = $cart_grandtotal_credit;
        } else if ($payment_type == 'cash') {
            $total = $grandtotal_price;
            $cart_total = $cart_grandtotal_price;
        }

        $response = array();
        $response = [
            'from_pricing' => $total,
            'from_cart' => $cart_total,
            'total_by_wallet' => $total_for_wallet_type,
            'price_credit' => round($cart_grandtotal_price / $co_rate, 2),
        ];

        return $response;
    }

    public static function get_order_by_id($id)
    {
        return $order = Order::leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('nm_order.order_id', '=', $id)->first();
    }

    public static function update_order_status($id, $status)
    {
        return $order = Order::where('order_id', $id)
        ->update(array('order_status' => $status));
    }

    public static function update_order_shipment($data,$id)
    {
        return $order = Order::where('order_id', '=', $id)
        ->update($data);
    }

    public static function update_merchant_order($order_id, $mer_id, $mer_vtoken, $order_vtoken)
    {
        $merchant = Merchant::where('mer_id', '=', $mer_id)->update(['mer_vtoken' => ($mer_vtoken + $order_vtoken)]);

        $merchant_vlog = new MerchantVTokenLog;
        $merchant_vlog->mer_id = $mer_id;
        $merchant_vlog->credit_amount = $order_vtoken;
        $merchant_vlog->debit_amount = 0;
        $merchant_vlog->order_id = $order_id;
        $merchant_vlog->remark = 'Order Delivered';
        $merchant_vlog->created_at = date('Y-m-d H:i:s');
        $merchant_vlog->updated_at = date('Y-m-d H:i:s');
        $merchant_vlog->save();

        return true;
    }

    public static function completing_merchant_order($order_id, $order_tracking_no = null)
    {
        $order = Order::leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('order_id', $order_id)->first();

        if(!is_null($order))
        {
            DB::beginTransaction();

            self::update_order_status($order_id, 4);

            if(!is_null($order_tracking_no))
                self::update_order_tracking_no($order_id, $order_tracking_no);

            $currency = $order->currency;

            /**
             * Disable get curreny_rate from order table first to support indonesia only.
             * $currency_rate = $order->currency_rate;
             */
            $currency_rate = 1;

            // Merchant Transaction
            $total_price = $order->total_product_price; // get total product price * quantity
            $_earning = $total_price - $order->merchant_charge_value; // get merchant earning
            $_shipping_fee = $order->total_product_shipping_fees; // get total shipping fees

            // Calulate based on exchange rate
            $earning = round(($_earning * $currency_rate), 2);
            $shipping_fee = round(($_shipping_fee * $currency_rate), 2);

            try {
                $merchant = Merchant::where('mer_id', $order->pro_mr_id)->first();
                $merchant->earning = $merchant->earning + $earning + $shipping_fee;
                $merchant->save();
            }
            catch (\Exception $e) {
                DB::rollback();
                return false;
            }

            try {
                $merchant_vlog = new MerchantVTokenLog;
                $merchant_vlog->mer_id = $merchant->mer_id;
                $merchant_vlog->credit_amount = $earning;
                $merchant_vlog->debit_amount = 0;
                $merchant_vlog->order_id = $order_id;
                $merchant_vlog->remark = 'Order Delivered';
                $merchant_vlog->currency = $currency;
                $merchant_vlog->currency_rate = $currency_rate;
                $merchant_vlog->amount = $_earning;
                $merchant_vlog->created_at = date('Y-m-d H:i:s');
                $merchant_vlog->updated_at = date('Y-m-d H:i:s');
                $merchant_vlog->save();
            }
            catch (\Exception $e) {
                DB::rollback();
                return false;
            }


            if ($shipping_fee > 0)
            {
                try {
                    $merchant_vlog = new MerchantVTokenLog;
                    $merchant_vlog->mer_id = $merchant->mer_id;
                    $merchant_vlog->credit_amount = $shipping_fee;
                    $merchant_vlog->debit_amount = 0;
                    $merchant_vlog->order_id = $order_id;
                    $merchant_vlog->remark = 'Shipping Fees';
                    $merchant_vlog->currency = $currency;
                    $merchant_vlog->currency_rate = $currency_rate;
                    $merchant_vlog->amount = $_shipping_fee;
                    $merchant_vlog->created_at = date('Y-m-d H:i:s');
                    $merchant_vlog->updated_at = date('Y-m-d H:i:s');
                    $merchant_vlog->save();
                }
                catch (\Exception $e) {
                    DB::rollback();
                    return false;
                }
            }

            // Main Merchant Transaction
            $_merchant_charge = $order->merchant_charge_value;
            $_service_charge = $order->cus_service_charge_rate;
            $_platform_charge = $order->cus_platform_charge_value;
            $_main_merchant_earning = $_merchant_charge + $_service_charge + $_platform_charge;

            // Calulate based on exchange rate
            $merchant_charge = round(($_merchant_charge * $currency_rate), 2);
            $service_charge = round(($_service_charge * $currency_rate), 2);
            $platform_charge = round(($_platform_charge * $currency_rate), 2);
            $main_merchant_earning = round(($_main_merchant_earning * $currency_rate), 2);

            try {
                $main_merchant = Merchant::where('mer_id', config('app.merchant.main'))->first();
                $main_merchant->earning = $main_merchant->earning + $main_merchant_earning;
                $main_merchant->save();
            }
            catch (\Exception $e) {
                DB::rollback();
                return false;
            }

            if ($order->merchant_charge_value > 0) {
                try {
                    $merchant_vlog = new MerchantVTokenLog;
                    $merchant_vlog->mer_id = $main_merchant->mer_id;
                    $merchant_vlog->credit_amount = $merchant_charge;
                    $merchant_vlog->debit_amount = 0;
                    $merchant_vlog->order_id = $order_id;
                    $merchant_vlog->remark = 'Order Commission';
                    $merchant_vlog->currency = $currency;
                    $merchant_vlog->currency_rate = $currency_rate;
                    $merchant_vlog->amount = $_merchant_charge;
                    $merchant_vlog->created_at = date('Y-m-d H:i:s');
                    $merchant_vlog->updated_at = date('Y-m-d H:i:s');
                    $merchant_vlog->save();
                }
                catch (\Exception $e) {
                    DB::rollback();
                    return false;
                }
            }

            if ($order->cus_service_charge_value > 0) {
                try {
                    $merchant_vlog = new MerchantVTokenLog;
                    $merchant_vlog->mer_id = $main_merchant->mer_id;
                    $merchant_vlog->credit_amount = $service_charge;
                    $merchant_vlog->debit_amount = 0;
                    $merchant_vlog->order_id = $order_id;
                    $merchant_vlog->remark = 'Order Service Charge';
                    $merchant_vlog->currency = $currency;
                    $merchant_vlog->currency_rate = $currency_rate;
                    $merchant_vlog->amount = $_service_charge;
                    $merchant_vlog->created_at = date('Y-m-d H:i:s');
                    $merchant_vlog->updated_at = date('Y-m-d H:i:s');
                    $merchant_vlog->save();
                }
                catch (\Exception $e) {
                    DB::rollback();
                    return false;
                }
            }

            if ($order->cus_platform_charge_value > 0) {
                try {
                    $merchant_vlog = new MerchantVTokenLog;
                    $merchant_vlog->mer_id = $main_merchant->mer_id;
                    $merchant_vlog->credit_amount = $platform_charge;
                    $merchant_vlog->debit_amount = 0;
                    $merchant_vlog->order_id = $order_id;
                    $merchant_vlog->remark = 'Order Platform Charge';
                    $merchant_vlog->currency = $currency;
                    $merchant_vlog->currency_rate = $currency_rate;
                    $merchant_vlog->amount = $_platform_charge;
                    $merchant_vlog->created_at = date('Y-m-d H:i:s');
                    $merchant_vlog->updated_at = date('Y-m-d H:i:s');
                    $merchant_vlog->save();
                }
                catch (\Exception $e) {
                    DB::rollback();
                    return false;
                }
            }

            DB::commit();

            return true;
        }
        return false;
    }

    public static function get_orders_by_status($mer_id, $type, $input = null)
    {
        $order = Order::select(
            'nm_order.*', 'nm_order.updated_at as updated_at', 'nm_order.created_at as created_at',
            // 'nm_shipping.*',
            'nm_country.co_name',
            'nm_customer.cus_id', 'nm_customer.cus_name',
            'nm_product.pro_id', 'nm_product.pro_title_en','nm_product.pro_mr_id',
            'nm_merchant.mer_id', 'nm_merchant.mer_fname', 'nm_merchant.mer_lname',
            'nm_store.stor_id', 'nm_store.stor_name'
        )
        ->leftJoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id')
        ->leftJoin('nm_shipping', 'nm_order.order_id', '=', 'nm_shipping.ship_order_id')
        ->leftJoin('nm_country','nm_country.co_id', '=', 'nm_shipping.ship_country')
        ->leftJoin('nm_state','nm_state.id', '=', 'nm_shipping.ship_state_id')
        ->leftJoin('nm_store','nm_store.stor_id', '=', 'nm_product.pro_sh_id')
        ->where('nm_order.order_type', '=', $type);
        if (isset($input['admin_country_id_list'])) {
            $order->whereIn('nm_store.stor_country', $input['admin_country_id_list']);
        }
        if(\Auth::guard('storeusers')->check()) {
            $assigned_stores = StoreUserMapping::where('storeuser_id','=',\Auth::guard('storeusers')->user()->id)->pluck('store_id')->toArray();
            $order->whereIn('nm_store.stor_id', $assigned_stores);
        }
        if($mer_id != 'all')
            $order->where('nm_product.pro_mr_id', '=', $mer_id);
        if (!empty($input['id'])) {
            $order->where(function($query) use ($input) {
                $query->where('nm_order.transaction_id', '=', $input['id'])
                ->orWhere('nm_order.order_id', '=', $input['id'])
                ->orWhere('nm_product.pro_id', '=', $input['id'])
                ->orWhere('nm_customer.cus_id', '=', $input['id']);
            });
        }
        if (!empty($input['tid'])) {
            $order->where('nm_order.transaction_id', 'LIKE', '%'.$input['tid'].'%');
        }
        if (!empty($input['oid'])) {
            $order->Where('nm_order.order_id', '=', $input['oid']);
        }
        if (!empty($input['pid']))
            $order->where('nm_product.pro_id', '=', $input['pid']);
        if (!empty($input['cid']))
            $order->where('nm_customer.cus_id', '=', $input['cid']);
        if (!empty($input['mid']))
            $order->where('nm_merchant.mer_id', '=', $input['mid']);
        if (!empty($input['sid']))
            $order->where('nm_store.stor_id', '=', $input['sid']);
        if(!empty($input['merchant_countries'])) {
            $order->where(function($query) use ($input){
                $query->whereIn('nm_merchant.mer_co_id', $input['merchant_countries']);
            });
        }
        if(!empty($input['customer_countries'])) {
            $order->where(function($query) use ($input){
                $query->whereIn('nm_customer.cus_country', $input['customer_countries']);
            });
        }
        if (!empty($input['name'])) {
            $title = '%'.$input['name'].'%';
            $order->where(function($query) use ($title) {
                $query->whereRaw('nm_product.pro_title_en LIKE ? or nm_product.pro_title_cn LIKE ? or nm_product.pro_title_cnt LIKE ? or nm_product.pro_title_my LIKE ?', [$title, $title, $title, $title]);
            });
        }
        if (!empty($input['start']) && !empty($input['end'])) {
            $input['start'] = Carbon::createFromFormat('d/m/Y', $input['start'])->startOfDay()->toDateTimeString();
            $input['end'] = Carbon::createFromFormat('d/m/Y', $input['end'])->endOfDay()->toDateTimeString();
            $order->where('nm_order.order_date', '>=', \Helper::TZtoUTC($input['start']));
            $order->where('nm_order.order_date', '<=', \Helper::TZtoUTC($input['end']));
        }
        if (!empty($input['status']))
                $order->where('nm_order.order_status', '=', $input['status']);
        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'name_asc':
                    $order->orderBy('nm_product.pro_title_en');
                    break;
                case 'name_desc':
                    $order->orderBy('nm_product.pro_title_en', 'desc');
                    break;
                case 'new':
                    $order->orderBy('nm_order.order_id', 'desc');
                    break;
                case 'old':
                    $order->orderBy('nm_order.order_id', 'asc');
                    break;
            }
        }
        else {
            $order->orderBy('nm_order.order_id', 'desc');
        }
        if (!empty($input['action']) && $input['action'] == 'export')
            return $order->get();
        return $order->paginate(50);
    }

    // public static function get_all_product_orders_by_status($status, $mer_id, $input)
    // {
    //     $orders = Order::orderBy('order_date', 'desc')
    //     ->leftjoin('nm_customer','nm_order.order_cus_id','=','nm_customer.cus_id')
    //     ->leftjoin('nm_product','nm_order.order_pro_id','=','nm_product.pro_id')
    //     ->where('nm_order.order_type','=', 1);

    //     if($mer_id != 'all'){
    //        $orders->where('nm_product.pro_mr_id', '=', $mer_id);
    //     }
    //     if ($status != '') {
    //        $orders->where('nm_order.order_status','=', $status);
    //     }

    //     if (!empty($input)) {
    //        $orders->where(function($q) use ($input) {
    //             $orders->where('nm_product.pro_title_en', 'LIKE', '%'.$input.'%')
    //             ->orWhere('nm_customer.cus_name', 'LIKE', '%'.$input.'%')
    //             ->orWhere('nm_order.transaction_id', 'LIKE', '%'.$input.'%');
    //        });
    //     }

    //     return $orders->paginate(50);
    // }

    public static function get_all_deal_orders_by_status($status, $mer_id, $input)
	{
       $orders = Order::orderBy('order_date', 'desc')
        ->leftjoin('nm_customer','nm_order.order_cus_id','=','nm_customer.cus_id')
		->leftjoin('nm_deals','nm_order.order_pro_id','=','nm_deals.deal_id')
		->where('nm_order.order_type','=',2);

        if($mer_id != 'all'){
           $orders->where('nm_product.pro_mr_id', '=', $mer_id);
        }
        if (!empty($status)) {
           $orders->where('order_status','=', $status);
        }

        if (!empty($input)) {
           $orders->where(function($q) use ($input) {
                $orders->where('nm_product.pro_title_en', 'LIKE', '%'.$input.'%')
                ->orWhere('nm_customer.cus_name', 'LIKE', '%'.$input.'%')
                ->orWhere('nm_order.transaction_id', 'LIKE', '%'.$input.'%');
           });
        }

        return $orders->get();
	}


public static function get_order_details($order_id)
    {
        $order = Order::selectRaw('
            nm_order.*, nm_product.*, nm_customer.*, nm_courier.*,
            nm_order.created_at as order_created_at,
            concat_ws(" ", nm_merchant.mer_fname, nm_merchant.mer_lname) as merchant_name,
            nm_merchant.mer_id,
            nm_merchant.mer_address1,
            nm_merchant.mer_address2,
            nm_merchant.zipcode,
            nm_merchant.mer_city_name as mer_city,
            nm_state.name as mer_state,
            nm_country.co_name as mer_country
        ')
        ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->leftJoin('nm_state', 'nm_state.id', '=', 'nm_merchant.mer_state')
        ->leftJoin('nm_country', 'nm_country.co_id', '=', 'nm_merchant.mer_co_id')
        ->leftJoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftJoin('nm_courier', 'nm_order.order_courier_id', '=', 'nm_courier.id')
        ->where('nm_order.order_id', '=', $order_id);

        if (\Auth::guard('merchants')->check()) {
            $mer_id = \Auth::guard('merchants')->user()->mer_id;
            $order->where('nm_product.pro_mr_id', '=', $mer_id);
        }

        return $order->first();
    }

    public static function find_parent_order($parent_id, $merchant_id = null)
    {
        return ParentOrder::where('id', $parent_id)
        ->whereHas('items', function ($q) use ($merchant_id) {
            if($merchant_id)
            {
                $q->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                ->leftJoin('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id')
                ->where('nm_merchant.mer_id', $merchant_id);
            }
        })
        ->with([
            'items' => function ($q) use ($merchant_id) {
                $q->selectRaw("nm_order.*, nm_merchant.mer_id as merchant_id,
                    ROUND(GREATEST((nm_order.product_original_price * nm_order.order_qty) / nm_order.currency_rate, 0), 4) as credit,
                    ROUND(GREATEST(nm_order.order_vtokens - (nm_order.merchant_charge_vtoken + nm_order.cus_service_charge_value + nm_order.cus_platform_charge_value), 0), 4) as merchant_earn_credit
                ")
                ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                ->leftJoin('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id');

                if($merchant_id)
                {
                    $q->where('nm_merchant.mer_id', $merchant_id);
                }
            },
            'invoices' => function ($q) use ($merchant_id) {
                if($merchant_id)
                {
                    $q->where('merchant_id', $merchant_id);
                }
            },
            'customer', 'shipping_address', 'items.product.merchant'])
        ->first();
    }

    public static function get_shipping_address($order_id)
    {
        $ship = Shipping::where('nm_shipping.ship_order_id', '=', $order_id)
        ->leftJoin('nm_city','nm_city.ci_id', '=', 'nm_shipping.ship_ci_id')
        ->leftJoin('nm_state','nm_state.id', '=', 'nm_shipping.ship_state_id')
        ->leftJoin('nm_country','nm_country.co_id', '=', 'nm_shipping.ship_country');

        return $ship->first();
    }

    public static function get_shipment_detail($order_id)
    {
        $shipment = Order::where('nm_order.order_id', '=', $order_id)
        ->leftjoin('nm_courier', 'nm_order.order_courier_id', '=', 'nm_courier.id');

        return $shipment->first();
    }

    public static function get_product_quantity_sold($pro_id)
    {
        return Order::where('order_pro_id','=',$pro_id)->where('order_status','=',4)->get()->sum('order_qty');
    }

    public static function update_cart_price($pricing,$cart_id)
    {
        try {

            $price = $pricing->price;
            if($pricing->is_discounted())
                $price = $pricing->discounted_price;

            Cart::find($cart_id)->update([
                'purchasing_price' => $price,
                'product_price' => $price,
            ]);

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }

    public static function check_cart_and_pricing_attribute($token, $uid, $cid = null)
    {
        if(!$cid)
            $cid = \Cookie::get('country_locale');

        $shoppingcarts = array();
        $carts = new Cart;
        $carts = $carts->select('temp_cart.id','temp_cart.attributes as cart_attribute_json','nm_product_pricing.id as pricing_id');
        $carts = $carts->leftJoin('nm_product', 'nm_product.pro_id', '=', 'temp_cart.product_id');
        $carts = $carts->where('nm_product.pro_status', '=', 1);
        $carts = $carts->leftJoin('nm_product_pricing','nm_product_pricing.id','=','temp_cart.pricing_id');
        $carts = $carts->where('nm_product_pricing.country_id','=',$cid);
        $carts = $carts->where('nm_product_pricing.status','=', 1);
        $carts = $carts->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id');
        $carts = $carts->where('nm_store.stor_status', '=', 1);
        $carts = $carts->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id');
        $carts = $carts->where('nm_merchant.mer_staus', '=', 1);

        $carts = $carts->where(function($q) use ($token, $uid) {
            if (!empty($token)) {
                $q->orWhere('token', '=', $token);
            }

            if ($uid != 0) {
                $q->orWhere('cus_id', '=', $uid);
            }
        });

        $carts = $carts->get();

        $count = 0;
        foreach ($carts as $key => $cart) {

            $from_pricing = (array)json_decode(AttributeRepo::get_pricing_attribute_id_json($cart->pricing_id),true);
            sort($from_pricing);

            $from_cart = (array)json_decode($cart->cart_attribute_json,true);
            sort($from_cart);

            $check_diff = $from_pricing != $from_cart;
            if(!empty($check_diff))
                $count++;
        }

        return $count;

    }

    public static function get_online_total_transaction($mer_id, $type, $input = null)
    {
        $order = Order::query();

        if ($mer_id != 'all') {
            $order->select(\DB::raw("SUM(order_vtokens - cus_service_charge_value - cus_platform_charge_value) as total_credit, SUM(merchant_charge_vtoken) as merchant_charge, SUM(order_vtokens - merchant_charge_vtoken - cus_service_charge_value - cus_platform_charge_value) as merchant_earned, SUM(total_product_shipping_fees_credit) as shipping_fees"))
            // ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
            ->where('nm_product.pro_mr_id', $mer_id);
        } else {
            $order->select(\DB::raw("SUM(order_vtokens) as total_credit, SUM(cus_platform_charge_value) as transaction_fees, SUM(cus_service_charge_value) as service_fees, SUM(merchant_charge_vtoken) as merchant_charge, SUM(order_vtokens - merchant_charge_vtoken - cus_service_charge_value - cus_platform_charge_value) as merchant_earned, SUM(total_product_shipping_fees_credit) as shipping_fees"));
        }

        $order->leftJoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id')
        ->leftJoin('nm_shipping', 'nm_order.order_id', '=', 'nm_shipping.ship_order_id')
        ->leftJoin('nm_country','nm_country.co_id', '=', 'nm_shipping.ship_country')
        ->leftJoin('nm_state','nm_state.id', '=', 'nm_shipping.ship_state_id')
        ->leftJoin('nm_store','nm_store.stor_id', '=', 'nm_product.pro_sh_id')
        ->where('nm_order.order_type', '=', $type);

        if (isset($input['admin_country_id_list'])) {
            $order->whereIn('nm_store.stor_country', $input['admin_country_id_list']);
        }

        if(\Auth::guard('storeusers')->check()) {
            $assigned_stores = StoreUserMapping::where('storeuser_id','=',\Auth::guard('storeusers')->user()->id)->pluck('store_id')->toArray();
            $order->whereIn('nm_store.stor_id', $assigned_stores);
        }

        if (!empty($input['id'])) {
            $order->where(function($query) use ($input) {
                $query->where('nm_order.transaction_id', '=', $input['id'])
                ->orWhere('nm_order.order_id', '=', $input['id'])
                ->orWhere('nm_product.pro_id', '=', $input['id'])
                ->orWhere('nm_customer.cus_id', '=', $input['id']);
            });
        }

        if (!empty($input['tid'])) {
            $order->where(function($query) use ($input) {
                $query->where('nm_order.transaction_id', $input['tid'])
                ->orWhere('nm_order.order_id', $input['tid']);
            });
        }

        if (!empty($input['pid']))
            $order->where('nm_product.pro_id', '=', $input['pid']);

        if (!empty($input['cid']))
            $order->where('nm_customer.cus_id', '=', $input['cid']);

        if (!empty($input['mid']))
            $order->where('nm_merchant.mer_id', '=', $input['mid']);

        if(!empty($input['merchant_countries'])) {
            $order->where(function($query) use ($input){
                $query->whereIn('nm_merchant.mer_co_id', $input['merchant_countries']);
            });
        }

        if(!empty($input['customer_countries'])) {
            $order->where(function($query) use ($input){
                $query->whereIn('nm_customer.cus_country', $input['customer_countries']);
            });
        }

        if (!empty($input['name'])) {
            $title = '%'.$input['name'].'%';
            $order->where(function($query) use ($title) {
                $query->whereRaw('nm_product.pro_title_en LIKE ? or nm_product.pro_title_cn LIKE ? or nm_product.pro_title_cnt LIKE ? or nm_product.pro_title_my LIKE ?', [$title, $title, $title, $title]);
            });
        }

        if (!empty($input['start']) && !empty($input['end'])) {
            $input['start'] = Carbon::createFromFormat('d/m/Y', $input['start'])->startOfDay()->toDateTimeString();
            $input['end'] = Carbon::createFromFormat('d/m/Y', $input['end'])->endOfDay()->toDateTimeString();

            $order->where('nm_order.order_date', '>=', \Helper::TZtoUTC($input['start']));
            $order->where('nm_order.order_date', '<=', \Helper::TZtoUTC($input['end']));
        }

        if (!empty($input['status']))
            $order->where('nm_order.order_status', '=', $input['status']);

        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'name_asc':
                    $order->orderBy('nm_product.pro_title_en');
                    break;
                case 'name_desc':
                    $order->orderBy('nm_product.pro_title_en', 'desc');
                    break;
                case 'new':
                    $order->orderBy('nm_order.order_date', 'desc');
                    break;
                case 'old':
                    $order->orderBy('nm_order.order_date', 'asc');
                    break;
            }
        }
        else {
            $order->orderBy('nm_order.order_date', 'desc');
        }


        return $order->first();
    }

    public static function update_batch_status_transaction($operation, $order_id, $data = null, $actionBy = 'admin')
    {
        try {

            $orders = Order::query();
            $orders->whereIn('order_id', $order_id);

            switch ($operation)
            {
                case 'accept_order':
                    $orders->where('order_status', 1);
                    $orders->update([
                        'order_status' => 2,
                    ]);

                    OrderOnlineLogger::log(null, $order_id, null, $actionBy, 'Accept order');

                    $notificationData = Order::whereIn('order_id', $order_id)
                        ->whereOrderStatus(2)->get();

                    (new Notifier($notificationData))->send(Notifier::ORDER_RECEIVED);

                    break;

                case 'create_shipment':
                    if(!$data)
                        return false;

                    $response = self::findOrCreateDeliveryOrder($data['invoice_id'], $order_id, $data);
                    if(!$response)
                        return false;

                    $orders->where('order_status', 2);
                    $orders->whereIn('product_shipping_fees_type', [0,1,2]);
                    $orders->update([
                        'order_status' => 3,
                        'order_shipment_date' => Carbon::now('UTC')
                    ]);

                    OrderOnlineLogger::log(null, $order_id, null, $actionBy, 'Create shipment');

                    $notificationData = Order::whereIn('order_id', $order_id)
                        ->whereOrderStatus(3)->get();

                    (new Notifier($notificationData))->send(Notifier::ORDER_SHIPPED);

                    break;

                case 'arrange_pickup':
                    if(!$data)
                        return false;

                    $response = self::findOrCreateDeliveryOrder($data['invoice_id'], $order_id, $data);
                    if(!$response)
                        return false;

                    $orders->where('order_status', 2);
                    $orders->where('product_shipping_fees_type', 3);
                    $orders->update([
                        'order_status' => 3,
                        'order_shipment_date' => Carbon::now('UTC')
                    ]);

                    OrderOnlineLogger::log(null, $order_id, null, $actionBy, 'Arrange pickup');
                    break;

                //only shipping type self pickup able to set to complete
                case 'complete_order':
                    $orders->where('product_shipping_fees_type', 3);
                    $orders->where('order_status', 3);
                    $orders->update([
                        'order_status' => 4,
                    ]);

                    foreach ($order_id as $id) {
                        self::completing_merchant_order($id, $data['remarks']);
                    }

                    self::update_delivery_order($data['delivery_order_id'], $data);

                    OrderOnlineLogger::log(null, $order_id, null, $actionBy, 'Complete order');

                    $notificationData = Order::whereIn('order_id', $order_id)
                        ->whereOrderStatus(4)->get();

                    (new Notifier($notificationData))->send(Notifier::ORDER_COMPLETED);

                    break;

                case 'complete_service':
                    $orders->where('order_status', Order::STATUS_INSTALLATION);
                    $orders->update([
                        'order_status' => 4,
                    ]);

                    foreach ($order_id as $id) {
                        self::completing_merchant_order($id, $data['remarks']);
                    }

                    self::update_delivery_order($data['delivery_order_id'], $data);

                    OrderOnlineLogger::log(null, $order_id, null, $actionBy, 'Complete order');

                    $notificationData = Order::whereIn('order_id', $order_id)
                        ->whereOrderStatus(4)->get();

                    (new Notifier($notificationData))->send(Notifier::ORDER_COMPLETED);

                    break;

                case 'cancel_order':
                case 'refund_order':
                    $action = ($operation == 'cancel_order')? 'cancel' : 'refund';

                    $orderUpdates = self::get_orders_by_id($order_id);
                    foreach ($orderUpdates as $order)
                    {
                        $orderid = $order->order_id;

                        switch ($order->order_status)
                        {
                            case 1:
                                $logDesc = "Cancel order from pending status";
                                break;

                            case 2:
                                $logDesc = "Cancel order from packaging status";
                                break;

                            case 3:
                                $logDesc = "Cancel order from shipment status";
                                break;

                            case 4:
                                $logDesc = "Refund order";
                                break;

                            default:
                                $logDesc = "";
                                break;
                        }

                        $result = \DB::select("CALL refund_online_order($orderid)");
                        if ($result)
                        {
                            ProductPricingRepo::refund_pricing_quantity($orderid, $action);

                            if(in_array($order->order_type, [3,4]))
                                GeneratedCodeRepo::cancel_code_by_order($orderid);

                            OrderOnlineLogger::log(null, $orderid, null, $actionBy, $logDesc);

                            $notificationData = Order::whereIn('order_id', $order_id)
                                ->whereOrderStatus(5)->get();

                            (new Notifier($notificationData))->send(Notifier::ORDER_CANCELLED);
                        }
                        else
                        {
                            OrderOnlineLogger::log(null, $orderid, null, $actionBy, "Failed : $logDesc");
                        }
                    }
                    break;

                default:
                    return false;
                    break;
            }

            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    public static function update_order_tracking_no($order_id, $remarks)
    {
        return $order = Order::where('order_id', $order_id)
        ->update([
            'order_tracking_no' => $remarks
        ]);
    }

    public static function empty_cart($cus_id, $country_id)
    {
        return Cart::leftJoin('nm_product_pricing','nm_product_pricing.id','=','temp_cart.pricing_id')
        ->where('temp_cart.cus_id', $cus_id)
        ->where('nm_product_pricing.country_id', $country_id)
        ->delete();
    }

    public static function get_cart_by_id($id, $cus_id, $country_id)
    {
        return Cart::select('temp_cart.*', 'nm_product_pricing.quantity as pricing_quantity')
        ->leftJoin('nm_product_pricing', 'nm_product_pricing.id', '=', 'temp_cart.pricing_id')
        ->where('temp_cart.id', $id)
        ->where('temp_cart.cus_id', $cus_id)
        ->where('nm_product_pricing.country_id', $country_id)
        ->first();
    }

    public static function add_cart($token, $user_id, $data)
    {
        $pricing = ProductPricing::select('nm_product_pricing.*', 'nm_country.co_curcode', 'nm_country.co_rate',
        \DB::raw("
            CASE WHEN ( (((nm_product_pricing.discounted_price IS NOT NULL )) and (nm_product_pricing.discounted_price > 0 )) and (NOW() >= (CAST(nm_product_pricing.discounted_from AS DATETIME)) and NOW() <= (CAST(nm_product_pricing.discounted_to AS DATETIME))) ) THEN nm_product_pricing.discounted_price ELSE nm_product_pricing.price END AS purchase_price")
        )
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_product_pricing.pro_id')
        ->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->leftJoin('nm_country','nm_country.co_id','=','nm_product_pricing.country_id')
        ->where('nm_product.pro_status', '=', 1)
        ->where('nm_product_pricing.status','=', 1)
        ->where('nm_store.stor_status', '=', 1)
        ->where('nm_merchant.mer_staus', '=', 1)
        ->where('nm_product_pricing.id','=', $data['price_id'])
        ->where('nm_product_pricing.pro_id', $data['product_id'])
        ->first();

        if(!$pricing)
            return false;

        $cart = Cart::create([
            'token' => $token,
            'cus_id' => $user_id,
            'product_id' => $data['product_id'],
            'quantity' => $data['qty'],
            'remarks' => $data['remarks'],
            'pricing_id' => $pricing->id,
            'currency' => $pricing->co_curcode,
            'currency_rate' => $pricing->co_rate,
            'purchasing_price' => $pricing->purchase_price,
            'product_price' => $pricing->purchase_price,
            'attributes' => AttributeRepo::get_pricing_attribute_id_json($pricing->id),
            'attributes_name' => AttributeRepo::get_pricing_attributes_name_json($pricing->id),
        ]);

        return true;
    }

    public static function get_online_orders($data, $type = 'retail')
    {
        $transactions = ParentOrder::select('id', 'customer_id', 'transaction_id', 'date as transaction_date');

        $transactions->where(function ($query) use ($data)
        {
            if(isset($data['transaction_id']) && $data['transaction_id'])
            {
                $search = '%'.$data['transaction_id'].'%';
                $query->whereRaw("transaction_id LIKE ?", [$search]);
            }

            if (isset($data['start_date']) && isset($data['end_date']) && $data['start_date'] && $data['end_date'])
            {
                $start = Carbon::createFromFormat('d/m/Y', $data['start_date'])->startOfDay()->toDateTimeString();
                $end = Carbon::createFromFormat('d/m/Y', $data['end_date'])->endOfDay()->toDateTimeString();

                $query->whereBetween('date', [\Helper::TZtoUTC($start), \Helper::TZtoUTC($end)]);
            }
        });

        $transactions->withAndWhereHas('customer', function ($query) use ($data)
        {
            $query->selectRaw("
                cus_id,
                cus_name as customer_name,
                CONCAT(phone_area_code, cus_phone) as customer_phone,
                email as customer_email
            ");

            if(isset($data['customer_id']) && $data['customer_id'])
            {
                $query->where('cus_id', $data['customer_id']);
            }

            if(isset($data['customer_countries']) && $data['customer_countries'])
            {
                $query->where(function($query) use ($data) {
                    $query->whereIn('cus_country', $data['customer_countries']);
                });
            }

            if(isset($data['customer_name']) && $data['customer_name'])
            {
                $search = '%'.$data['customer_name'].'%';
                $query->whereRaw("cus_name LIKE ?", [$search]);
            }
        });

        $transactions->withAndWhereHas('items.product.merchant', function ($query) use ($data)
        {
            if(isset($data['merchant_id']) && $data['merchant_id'])
            {
                $query->where('mer_id', $data['merchant_id']);
            }

            if(isset($data['merchant_countries']) && $data['merchant_countries'])
            {
                $query->where(function($query) use ($data) {
                    $query->whereIn('mer_co_id', $data['merchant_countries']);
                });
            }

            if(isset($data['merchant_countries']) && $data['merchant_countries'])
            {
                $query->where(function($query) use ($data) {
                    $query->whereIn('mer_co_id', $data['merchant_countries']);
                });
            }
        });

        $transactions->withAndWhereHas('items.country', function ($query) use ($data)
        {
            if(isset($data['admin_countries']))
            {
                $query->where(function($query) use ($data) {
                    $query->whereIn('co_id', $data['admin_countries']);
                });
            }
        });

        $transactions->withAndWhereHas('items', function ($query) use ($data)
        {
            $query->selectRaw("
                nm_order.order_id,
                nm_order.parent_order_id,
                currency as currency_code,
                order_status,
                order_type,
                ROUND(GREATEST(nm_order.order_value, 0) , 2) as order_price,
                ROUND(GREATEST(nm_order.cus_platform_charge_rate, 0), 2) as platform_charge_rate,
                ROUND(GREATEST(nm_order.cus_platform_charge_value, 0), 2) as platform_charge_value,
                ROUND(GREATEST(nm_order.cus_service_charge_rate, 0), 2) as service_charge_rate,
                ROUND(GREATEST(nm_order.cus_service_charge_value, 0), 2) as service_charge_value,
                ROUND(GREATEST(nm_order.merchant_charge_percentage, 0), 2) as merchant_charge_rate,
                ROUND(GREATEST(nm_order.merchant_charge_value, 0), 2) as merchant_charge_value,
                ROUND(GREATEST(COALESCE(nm_order.total_product_shipping_fees, 0), 0), 4) as shipping_fees_value,
                ROUND(GREATEST(nm_order.order_value - (nm_order.merchant_charge_value + nm_order.cus_service_charge_value + nm_order.cus_platform_charge_value), 0), 4) as merchant_earn_value,
                nm_merchant.mer_id as merchant_id,
                CONCAT(nm_merchant.mer_fname, ' ', nm_merchant.mer_lname) as merchant_name,
                nm_store.stor_id as store_id,
                nm_store.stor_name as store_name,
                CONCAT(nm_country.co_code, '-ONC', tax_invoices.tax_number) as tax_number,
                CONCAT(nm_country.co_code, '-ONS', tax_invoices.tax_number) as tax_number_merchant
            ")
            ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
            ->leftJoin('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id')
            ->leftJoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            ->leftJoin('order_mappings', 'order_mappings.order_id', '=', 'nm_order.order_id')
            ->leftJoin('tax_invoices', 'tax_invoices.id', '=', 'order_mappings.tax_invoice_id')
            ->leftJoin('nm_country', 'nm_country.co_curcode', '=', 'nm_order.currency');

            if(isset($data['status']))
            {
                $query->where('nm_order.order_status', $data['status']);
            }

            if(isset($data['merchant_id']) && $data['merchant_id'])
            {
                $query->where('nm_merchant.mer_id', $data['merchant_id']);
            }

            if(isset($data['store_id']) && $data['store_id'])
            {
                $query->where('nm_store.stor_id', $data['store_id']);
            }
        });

        $transactions->where("wholesale", ($type == 'retail') ? false : true);

        if(!isset($data['sort']))
            $data['sort'] = 'new';

        switch ($data['sort'])
        {
            case 'old':
                $transactions->orderBy('date', 'asc');
                break;

            case 'new':
                $transactions->orderBy('date', 'desc');
                break;

            default:
                $transactions->orderBy('date', 'desc');
                break;
        }

        if (isset($data['export']) && $data['export'] == 'all')
        {
            $transactions = $transactions->get();
        }
        else
        {
            $transactions = $transactions->paginate(50);
        }

        if ($type == 'wholesale'){
            $transactions->load('customer.user.merchant');
        }

        return $transactions;
    }

    public static function get_online_order_items($parent_id, $store_id)
    {
        $order = ParentOrder::where('id', $parent_id)
        ->with(['invoices.items.product' => function ($q) use ($store_id) {
            $q->where('pro_sh_id', $store_id);
        }, 'shipping_address', 'invoices.items', 'invoices.customer', 'invoices.merchant', 'invoices.deliveries', 'invoices.deliveries.items', 'logs', 'items.logs'])
        ->first();

        return $order;
    }

    public static function update_transaction_status_order($action, $parent_id, $store_id, $order_id = null)
    {
        try
        {
            $orders = Order::leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
            ->leftJoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
            ->where('nm_order.parent_order_id', $parent_id)
            ->where('nm_store.stor_id', $store_id);

            if($order_id)
            {
                $orders->whereIn('nm_order.order_id', $order_id);
            }

            switch ($action)
            {
                case 'accept_order':
                    $orders->where('nm_order.order_status', 1);
                    break;

                case 'create_shipment':
                    $orders->where('nm_order.order_type', 1);
                    $orders->where('nm_order.order_status', 2);
                    $orders->whereIn('nm_order.product_shipping_fees_type', [0,1,2]);
                    break;

                case 'arrange_pickup':
                    $orders->where('nm_order.order_type', 1);
                    $orders->where('nm_order.order_status', 2);
                    $orders->where('nm_order.product_shipping_fees_type', 3);
                    break;

                //only shipping type self pickup able to set to complete
                case 'complete_order':
                    $orders->where('product_shipping_fees_type', 3);
                    $orders->where('nm_order.order_status', 3);
                    break;

                //only installation-status order
                case 'complete_service':
                    $orders->where('nm_order.order_status', Order::STATUS_INSTALLATION);
                    break;

                //disable refund/cancel electronic card
                case 'cancel_order':
                    $orders->whereIn('nm_order.order_type', [1,3,4]);
                    $orders->whereIn('nm_order.order_status', [1,2,3]);
                    break;


                case 'refund_order':
                    $orders->whereIn('nm_order.order_type', [1,3,4]);
                    $orders->where('nm_order.order_status', 4);
                    break;

                default:
                    return false;
                    break;
            }

            $orders = $orders->orderBy('order_id', 'ASC')->pluck('order_id')->toArray();

            if($order_id)
            {

                sort($order_id);

                if($orders === $order_id)
                {
                    return true;
                }

                return false;
            }

            if(!empty($orders))
                return $orders;

            return false;

        } catch (Exception $e) {
            return false;
        }
    }

    public static function create_delivery_order($invoice_id, $order_id, $data, $email = true)
    {
        try
        {
            $invoice = TaxInvoice::find($invoice_id);
            if(!$invoice)
                return false;

            $date = Carbon::now('UTC');
            $year = $date->format('y');
            $month = $date->format('m');
            $runningNo = 1;

            $do_number = DeliveryOrder::where('do_number', 'LIKE', $year.$month.'%')->orderBy('do_number', 'desc')->value('do_number');
            if($do_number)
            {
                $runningNo = (integer)substr($do_number, 4) + 1;
            }

            $do = new DeliveryOrder;
            $do->type = $invoice->shipment_type;
            $do->tax_invoice_id = $invoice->id;
            $do->do_number = $year.$month.str_pad($runningNo, 4, '0', STR_PAD_LEFT);
            $do->courier_id = $data['courier_id']? $data['courier_id'] : 0;
            $do->tracking_number = $data['tracking_number'];
            $do->appointment_detail = $data['appointment_detail'];
            $do->shipment_date = $date;
            $do->save();
            
            $does = new Order;
            $does->order_tracking_no = $data['tracking_number'];
            $does->save();
            
            OrderMapping::whereIn('order_id', $order_id)->update([
                'delivery_order_id' => $do->id,
            ]);

            if($email)
            {
                self::sendDeliveryEmail($do);
            }

            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    public static function find_delivery_order($order_id = null, $delivery_order_id = null)
    {
        if($order_id) {
            $order = Order::find($order_id);
            if(!$order || !$order->mapping || !$order->mapping->delivery_order)
                return null;

            return $order->mapping->delivery_order;
        }

        return DeliveryOrder::find($delivery_order_id);
    }

    public static function update_delivery_order($delivery_order_id, $data)
    {
        try
        {
            $do = DeliveryOrder::find($delivery_order_id);

            if(isset($data['courier_id']) || isset($data['tracking_number']) || isset($data['appointment_detail']))
            {
                $data['shipment_date'] = Carbon::now('UTC');
            }

            if(isset($data['courier_id']))
            {
                $do->courier_id = $data['courier_id']? $data['courier_id'] : 0;
            }

            if(isset($data['tracking_number']))
            {
                $do->tracking_number = $data['tracking_number'];
            }

            if(isset($data['appointment_detail']))
            {
                $do->appointment_detail = $data['appointment_detail'];
            }

            if(isset($data['shipment_date']))
            {
                $do->shipment_date = $data['shipment_date'];
            }

            if(isset($data['remarks']))
            {
                $do->remarks = $data['remarks'];
            }

            $do->save();

            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    public static function find_orders_invoice($orders_id)
    {
        $invoice_id = OrderMapping::whereIn('order_id', $orders_id)->pluck('tax_invoice_id')->unique();
        if(!$invoice_id ||$invoice_id->count() <> 1)
        {
            return false;
        }

        $invoice = TaxInvoice::find($invoice_id->first());
        if(!$invoice)
            return false;

        return $invoice;
    }

    public static function get_orders_by_id($orders_id)
    {
        return Order::selectRaw("*,
            CASE WHEN cus_platform_charge_value > 0 AND cus_service_charge_value > 0 THEN
            ((product_original_price * order_qty) / currency_rate ) - merchant_charge_vtoken
            ELSE
            order_vtokens - merchant_charge_vtoken
            END AS merchant_earn
        ")->whereIn('order_id', $orders_id)
        ->get();
    }

    public static function generateTaxInvoice($parentOrderId)
    {
        $transaction = ParentOrder::with(['items' => function($q) {
            $q->selectRaw("nm_order.parent_order_id, nm_order.order_id, nm_merchant.mer_id as merchant_id, nm_store.stor_id as store_id,
                CASE WHEN order_type = 1 THEN 1 ELSE 2 END as item_type,
                CASE WHEN product_shipping_fees_type = 3 THEN 2 ELSE 1 END as shipment_type,
                CASE WHEN nm_country.co_id IS NOT NULL THEN nm_country.co_id ELSE 0 END as country_id
            ")
                ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                ->leftJoin('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id')
                ->leftJoin('nm_store', 'nm_product.pro_sh_id', '=', 'nm_store.stor_id')
                ->leftJoin('nm_country', 'nm_country.co_curcode', '=', 'nm_order.currency')
                ->whereIn('order_type', [1,3,4,5]);
        }])->find($parentOrderId);

        $results = [];

        $default = $transaction->items->first();

        //group by merchant
        // $byMerchants = $transaction->items->groupBy('merchant_id');
        $byStores = $transaction->items->groupBy('store_id');

        foreach ($byStores as $store_id => $byStore)
        {
            $merchant = Store::find($store_id);

            //group by item type
            $byItems = $byStore->groupBy('item_type');
            foreach($byItems as $item_type => $byItem)
            {
                $orders_id = [];
                if($item_type == 1)
                {
                    //group by shipment_type
                    $byShippings = $byItem->groupBy('shipment_type');
                    foreach ($byShippings as $shipment_type => $byShipping) {
                        $orders_id = $byShipping->pluck('order_id')->toArray();
                        $results[] = [
                            'parent_order_id' => $transaction->id,
                            'date' => $transaction->date,
                            'customer_id' => $transaction->customer_id,
                            'merchant_id' => $merchant->stor_merchant_id,
                            'item_type' => $item_type,
                            'shipment_type' => $shipment_type,
                            'order_id' => $orders_id,
                            'country_id' => $default->country_id,
                        ];
                    }

                } else {
                    $orders_id = $byItem->pluck('order_id')->toArray();
                    $results[] = [
                        'parent_order_id' => $transaction->id,
                        'date' => $transaction->date,
                        'customer_id' => $transaction->customer_id,
                        'merchant_id' => $merchant->stor_merchant_id,
                        'item_type' => $item_type,
                        'shipment_type' => 0,
                        'order_id' => $orders_id,
                        'country_id' => $default->country_id,
                    ];
                }
            }
        }

        $date = Carbon::now('UTC');
        $year = $date->format('y');
        $month = $date->format('m');
        $runningNo = 1;

        foreach ($results as $data)
        {
            $taxInvoicesNumber = TaxInvoice::where('country_id', $data['country_id'])->where('tax_number', 'LIKE', $year.$month.'%')->orderBy('tax_number', 'desc')->value('tax_number');

            if($taxInvoicesNumber)
            {
                $runningNo = (integer)substr($taxInvoicesNumber, 4) + 1;
            }

            $invoice = new TaxInvoice;
            $invoice->tax_number = $year.$month.str_pad($runningNo, 4, '0', STR_PAD_LEFT);
            $invoice->parent_order_id = $data['parent_order_id'];
            $invoice->customer_id = $data['customer_id'];
            $invoice->merchant_id = $data['merchant_id'];
            $invoice->item_type = $data['item_type'];
            $invoice->shipment_type = $data['shipment_type'];
            $invoice->country_id = $data['country_id'];
            $invoice->save();

            $timestamp = Carbon::now('utc');
            $items = [];
            foreach ($data['order_id'] as $order_id) {
                $items[] = [
                    'tax_invoice_id' => $invoice->id,
                    'order_id' => $order_id,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp,
                ];
            }

            OrderMapping::insert($items);

        }
    }

    public static function get_tax_invoices($parent_order_id = null, $invoice_id = [])
    {
        if($parent_order_id)
            return TaxInvoice::where('parent_order_id',$parent_order_id)->get();

        if($invoice_id)
            return TaxInvoice::whereIn('id', $invoice_id)->get();

        return null;
    }

    public static function get_tax_invoices_details($id)
    {
        return TaxInvoice::find($id);
    }

    public static function get_deliveries_order_details($id)
    {
        return DeliveryOrder::find($id);
    }

    public static function get_delivery_orders($delivery_ids)
    {
        return DeliveryOrder::whereIn('id', $delivery_ids)->get();
    }

    public static function findOrCreateDeliveryOrder($invoice_id, $order_id, $data)
    {
        try {

            $mappings = OrderMapping::whereIn('order_id', $order_id)->get();

            //check if these orders already has delivery order and need to update it
            $check = $mappings->pluck('delivery_order_id')->toArray();
            if(count(array_unique($check)) === 1 && end($check) != 0)
            {
                $do_id = end($check);
                $response = self::update_delivery_order($do_id, $data);
                if(!$response)
                    return false;

                $do = DeliveryOrder::find($do_id);

                self::sendDeliveryEmail($do);
            }
            else
            {
                //this order does not has any delivery order and need to create it, only grab order without do
                $order_id = $mappings->where('delivery_order_id', 0)->pluck('order_id')->toArray();
                $response = self::create_delivery_order($invoice_id, $order_id, $data);
                if(!$response)
                    return false;
            }

            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    public static function generateDeliveryOrder($parentOrderId)
    {
        $transaction = ParentOrder::with(['invoices.items'])->find($parentOrderId);
        foreach ($transaction->invoices as $invoice)
        {
            $orders_id = $invoice->items->pluck('order_id')->toArray();

            $data = [
                'type' => $invoice->shipment_type,
                'courier_id' => 0,
                'tracking_number' => null,
                'appointment_detail' => null,
            ];

            self::create_delivery_order($invoice->id, $orders_id, $data, false);
        }

        return true;
    }

    public static function get_order_by_transaction_id($transaction_id)
    {
        return Order::where('transaction_id', $transaction_id)->get();
    }

    public static function sendDeliveryEmail(DeliveryOrder $do)
    {
        //later need to update with latest email queue
        if($do->invoice && $do->invoice->customer && $do->invoice->customer->email_verified)
        {
            $customer = $do->invoice->customer;
            $shipping = $do->invoice->parent_order && $do->invoice->parent_order->shipping_address? $do->invoice->parent_order->shipping_address : null;
            Mail::send('front.emails.order_shipment', ['delivery' => $do, 'shipping' => $shipping], function ($m) use ($customer) {
                $m->to($customer->email, $customer->cus_name)->subject('Order is Shipped!');
            });
        }
    }

    public static function online_histories($input, $type = 'retail')
    {
        $orders = Order::selectRaw("
            nm_order.*,
            ROUND(GREATEST(nm_order.order_value, 0) , 2) as order_price,
            ROUND(GREATEST(nm_order.cus_platform_charge_rate, 0), 4) as platform_charge_rate,
            ROUND(GREATEST(nm_order.cus_platform_charge_value, 0), 4) as platform_charge_value,
            ROUND(GREATEST(nm_order.cus_service_charge_rate, 0), 4) as service_charge_rate,
            ROUND(GREATEST(nm_order.cus_service_charge_value, 0), 4) as service_charge_value,
            ROUND(GREATEST(nm_order.merchant_charge_percentage, 0), 4) as merchant_charge_rate,
            ROUND(GREATEST(nm_order.merchant_charge_value, 0), 4) as merchant_charge_value,
            ROUND(GREATEST(COALESCE(nm_order.total_product_shipping_fees, 0), 0), 4) as shipping_fees_value,
            ROUND(GREATEST(nm_order.order_value - (nm_order.merchant_charge_value + nm_order.cus_service_charge_value + nm_order.cus_platform_charge_value), 0), 4) as merchant_earn_value
        ");

        $orders->where("wholesale", ($type == 'retail') ? false : true);

        $orders->where(function($query) use ($input)
        {
            if(isset($input['order_id']))
            {
                $query->where('order_id', $input['order_id']);
            }

            if(isset($input['status']))
            {
                $query->where('order_status', $input['status']);
            }

            if(isset($input['type']))
            {
                $query->where('order_type', $input['type']);
            }

            if(isset($input['transaction_id']))
            {
                $query->where('transaction_id', $input['transaction_id']);
            }

            if(isset($input['sku_code']))
            {
                $search = '%'.$input['sku_code'].'%';
                $query->where(function($query) use ($search) {
                    $query->whereRaw('sku LIKE ?', [$search]);
                });
            }

            if(isset($input['start_date']) && isset($input['end_date']) && $input['start_date'] && $input['end_date'])
            {
                $start = Carbon::createFromFormat('d/m/Y', $input['start_date'])->startOfDay()->toDateTimeString();
                $end = Carbon::createFromFormat('d/m/Y', $input['end_date'])->endOfDay()->toDateTimeString();

                $query->where(function($query) use ($start, $end) {
                    $query->whereBetween('created_at', [\Helper::TZtoUTC($start), \Helper::TZtoUTC($end)]);
                });
            }
        });

        $orders->withAndWhereHas('product', function($query) use ($input)
        {
            if(isset($input['product_id']))
            {
                $query->where(function($query) use ($input) {
                    $query->where('pro_id', $input['product_id']);
                });
            }

            if(isset($input['product_name']))
            {
                $search = '%'.$input['product_name'].'%';
                $query->where(function($query) use ($search) {
                    $query->whereRaw('pro_title_en LIKE ? or pro_title_cn LIKE ? or pro_title_cnt LIKE ? or pro_title_my LIKE ?', [$search, $search, $search, $search]);
                });
            }
        });

        $orders->withAndWhereHas('customer', function($query) use ($input)
        {
            if(isset($input['customer_id']))
            {
                $query->where(function($query) use ($input) {
                    $query->where('cus_id', $input['customer_id']);
                });
            }

            if(isset($input['customer_countries']) && $input['customer_countries'])
            {
                $query->where(function($query) use ($input) {
                    $query->whereIn('cus_country', $input['customer_countries']);
                });
            }
        });

        $orders->withAndWhereHas('product.merchant', function($query) use ($input)
        {
            if(isset($input['merchant_id']))
            {
                $query->where(function($query) use ($input) {
                    $query->where('mer_id', $input['merchant_id']);
                });
            }

            if(isset($input['merchant_countries']) && $input['merchant_countries'])
            {
                $query->where(function($query) use ($input) {
                    $query->whereIn('mer_co_id', $input['merchant_countries']);
                });
            }
        });

        $orders->withAndWhereHas('product.store', function($query) use ($input)
        {
            if(isset($input['store_id']))
            {
                $query->where('stor_id', $input['store_id']);
            }
        });

        $orders->withAndWhereHas('country', function($query) use ($input)
        {
            if(isset($input['admin_countries']) && $input['admin_countries'])
            {
                $query->where(function($query) use ($input) {
                    $query->whereIn('co_id', $input['admin_countries']);
                });
            }
        });

        $orders->withAndWhereHas('mapping.invoice', function($query) use ($input)
        {
            if(isset($input['do_number']) && $input['do_number'])
            {
                $search = '%'.$input['do_number'].'%';
                $query->select('tax_invoices.*')
                ->leftJoin('nm_country', 'nm_country.co_id', '=', 'tax_invoices.country_id')
                ->whereRaw("CASE WHEN nm_country.co_id IS NOT NULL THEN CONCAT(nm_country.co_code, '-', 'ON', tax_number) ELSE tax_number END LIKE ?", [$search]);
            }
        });

        if(isset($input['sort'])) {
            switch ($input['sort'])
            {
                case 'new':
                    $orders->orderBy('order_id', 'desc');
                    break;

                case 'old':
                    $orders->orderBy('order_id', 'asc');
                    break;
            }
        }
        else
        {
            $orders->orderBy('order_id', 'desc');
        }

        if (isset($input['export']) && $input['export'] == 'all')
        {
            $orders = $orders->get();
        }
        else
        {
            $orders = $orders->paginate(50);
        }

        if ($type == 'wholesale') {
            $orders->load('customer.user.merchant');
        }

        return $orders;
    }

    public static function get_order_details_by_cus_id($cus_id, $wholesale = false)
    {
        return $order = Order::leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('nm_order.wholesale', $wholesale)
        ->where('nm_order.order_cus_id', '=', $cus_id);
    }

    public static function get_parent_order_by_cus_id($cus_id, $itemStatus=null, $wholesale = false)
    {
        return $order = Order::leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftJoin('member_service_schedule', 'member_service_schedule.order_id', '=', 'nm_order.order_id')
        ->selectRaw('nm_order.*, nm_product.*, member_service_schedule.order_id as mss_order_id, member_service_schedule.status as mss_status, member_service_schedule.id as mss_id, member_service_schedule.service_name_current as service_name_current')
        ->where('nm_order.order_cus_id', '=', $cus_id)
        ->where('nm_order.order_status', '=', $itemStatus)
        ->where('nm_order.wholesale', '=', $wholesale)
        ->groupBy('nm_order.order_id')
        ->orderBy('nm_order.order_id', 'desc');
        // ->get();

    }

    public static function get_order_details_by_parent_order_id($par_id)
    {
        return $order = Order::leftjoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftJoin('nm_courier', 'nm_courier.id', '=', 'nm_order.order_courier_id')
        ->where('nm_order.parent_order_id', '=', $par_id)->get();
    }

    public static function get_shipping_address_by_parent_order_id($par_id)
    {
        $ship = Shipping::where('nm_shipping.parent_order_id', '=', $par_id)
        ->leftJoin('nm_city','nm_city.ci_id', '=', 'nm_shipping.ship_ci_id')
        ->leftJoin('nm_state','nm_state.id', '=', 'nm_shipping.ship_state_id')
        ->leftJoin('nm_country','nm_country.co_id', '=', 'nm_shipping.ship_country');

        return $ship->first();
    }

    public static function get_payment_info_by_parent_order_id($par_id)
    {
        $payment_details = Order::selectRaw('sum(total_product_price) as sub_total, sum(total_product_shipping_fees) as shipping_fees, sum(order_qty) as total_items')
        ->where('nm_order.parent_order_id', '=', $par_id)
        ->first();

        return $payment_details;
    }

    public static function get_service_stores_details_by_pro_id($pro_id)
    {
        $store_details = Store::leftjoin('store_service_schedule', 'store_service_schedule.stor_id', '=', 'nm_store.stor_id')
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'store_service_schedule.pro_id')
        ->where('store_service_schedule.pro_id', '=', $pro_id)
        ->get();

        return $store_details;
    }

    public static function get_member_service_schedule_by_order_id($order_id)
    {
        $member_service = MemberServiceSchedule::leftjoin('store_service_extra', 'member_service_schedule.store_service_extra_id', '=', 'store_service_extra.id')
        ->leftJoin('store_service_schedule', 'store_service_extra.store_service_schedule_id', '=', 'store_service_schedule.id')
        ->leftJoin('nm_product', 'store_service_schedule.pro_id', '=', 'nm_product.pro_id')
        ->leftJoin('nm_store', 'store_service_schedule.stor_id', '=', 'nm_store.stor_id')
        ->leftJoin('nm_customer', 'member_service_schedule.cus_id', '=', 'nm_customer.cus_id')
        ->selectRaw('member_service_schedule.*, nm_product.*, nm_store.*, store_service_extra.store_service_schedule_id as store_service_schedule_id, store_service_extra.service_name as service_name, store_service_extra.service_price as service_price,
        store_service_schedule.pro_id as pro_id, store_service_schedule.interval_period as interval_period, store_service_schedule.stor_id as stor_id, store_service_schedule.appoint_start as appoint_start, store_service_schedule.appoint_end as appoint_end, store_service_schedule.schedule_skip_count as schedule_skip_count,
        nm_customer.cus_name as cus_name')
        ->where('member_service_schedule.order_id', '=', $order_id)
        ->get();

        return $member_service;
    }

}
