<?php

namespace App\Repositories;
use App\Models\Vehicle;
use Auth;
use DB;
use Image;
use Exception;
use Storage;

class VehicleRepo
{
	public static function getBrand($brand)
	{
		$brands = Vehicle::query();

		$brands->where('brand', $brand)->select('brand')->distinct()->orderBy('brand', 'desc');

		return $brands->value('brand');
	}

    public static function getModel($brand)
    {
        $models = Vehicle::query();

        $models->where('brand', $brand)->select('model')->distinct()->orderBy('model');

        return $models->pluck('model');
    }

	public static function getVariant($brand, $model)
	{
		$variants = Vehicle::query();

		$variants->where([
			'brand' => $brand,
			'model' => $model,
		])->select('variant')->distinct()->orderBy('variant');

        return $variants->pluck('variant');
    }

    public static function getYear($model, $variant)
    {
        $years = Vehicle::query();

        $years->where([
            'model' => $model,
            'variant' => $variant,
        ])->distinct('year')->orderBy('year', 'desc');

        return $years->select('id', 'year')->get(); 
    }

    public static function all($input)
    {
        $vehicles = Vehicle::query();

        if (!empty($input['brand']))
            $vehicles->where('brand', 'LIKE', '%'.$input['brand'].'%');
        
        if (!empty($input['model']))
            $vehicles->where('model', 'LIKE', '%'.$input['model'].'%');

        if (!empty($input['variant']))
            $vehicles->where('variant', 'LIKE', '%'.$input['variant'].'%');

        if (!empty($input['year']))
            $vehicles->where('year', $input['year']);

        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'brand_asc':
                    $vehicles->orderBy('brand');
                    break;
                case 'brand_desc':
                    $vehicles->orderBy('brand', 'desc');
                    break;
                case 'model_asc':
                    $vehicles->orderBy('model');
                    break;
                case 'model_desc':
                    $vehicles->orderBy('model', 'desc');
                    break;
                case 'variant_asc':
                    $vehicles->orderBy('variant');
                    break;
                case 'variant_desc':
                    $vehicles->orderBy('variant', 'desc');
                    break;
                case 'year_asc':
                    $vehicles->orderBy('year');
                    break;
                case 'year_desc':
                    $vehicles->orderBy('year', 'desc');
                    break;
                default:
                    $vehicles->orderBy('id', 'desc');
                    break;
            }
        } else {
            $vehicles->orderBy('id', 'desc');
        }

        return $vehicles->paginate(50);
    }

    public static function get_brands()
    {
        return Vehicle::select('brand')->distinct()->orderBy('brand')->pluck('brand');
    }

    public static function get_models()
    {
        return Vehicle::select('model')->distinct()->orderBy('model')->pluck('model');
    }

    public static function get_variants()
    {
        return Vehicle::select('variant')->distinct()->orderBy('variant')->pluck('variant');
    }

    public static function get_years()
    {
        return Vehicle::select('year')->distinct()->orderBy('year', 'desc')->pluck('year');
    }

    public static function add($data, $filename)
    {
        $brand = str_replace(' ', '-', strtolower($data['brand']));
        $model = str_replace(' ', '-', strtolower($data['model']));

        $filename = $brand.'/'.$model.'/'.$filename.'.jpg';
        $filepath = 'gallery/vehicle/'.$filename;
        $exist = Storage::disk('s3')->exists($filepath);

        if (!$exist)
            $filename = '';

        $vehicle = Vehicle::create([
            'brand' => $data['brand'],
            'model' => $data['model'],
            'variant' => $data['variant'],
            'year' => $data['year'],
            'vehicle_image' => $filename,
        ]);

        return $vehicle;
    }

    public static function find($id)
    {
        return Vehicle::find($id);
    }

    public static function update($id, $data, $filename)
    {
        $vehicle = Vehicle::find($id);
        $vehicle->brand = $data['brand'];
        $vehicle->model = $data['model'];
        $vehicle->variant = $data['variant'];
        $vehicle->year = $data['year'];

        $brand = str_replace(' ', '-', strtolower($vehicle->brand));
        $model = str_replace(' ', '-', strtolower($vehicle->model));

        $filename = $brand.'/'.$model.'/'.$filename.'.jpg';
        $filepath = 'gallery/vehicle/'.$filename;
        $exist = Storage::disk('s3')->exists($filepath);

        if ($exist)
            $vehicle->vehicle_image = $filename;

        $vehicle->save();

        return $vehicle;
    }

    public static function delete($id)
    {
        $vehicle = Vehicle::find($id);
        $vehicle->delete();

        return $vehicle;
    }

    public static function uploadLogo($file, $brand)
    {
        try {
            $extension = $file->getClientOriginalExtension();
            $formatted_brand = str_replace(' ', '-', strtolower($brand));
            $image_name = $formatted_brand . '.png';

            $compressed_image = $file;

            $info = getimagesize($file);

            switch ($info['mime']) {
                case 'image/jpeg':
                    $image = (string) Image::make($file)->encode("jpg", 75);
                    break;

                case 'image/png':
                    $image = (string) Image::make($file)->encode("png", 75);
                    break;
            }

            $path = 'gallery/vehicle/'. $formatted_brand;
            $filename = $image_name;

            if (@file_get_contents($file)) {
                try {
                    $s3 = Storage::disk('s3');
                    $filePath = $path.'/' . $filename;
                    $s3->put($filePath, $image, "public");
                    @unlink($compressed_image); //remove compressed file

                    return $s3->url($filePath);
                } catch (Exception $e) {
                    return $e;
                }
            }

        } catch (Exception $e) {
            return $e;
        }
    }

    public static function uploadVehicleImage($file, $brand, $model, $filename)
    {
        try {
            $image_name = $filename.'.jpg';

            $compressed_image = $file;

            $info = getimagesize($file);

            switch ($info['mime']) {
                case 'image/jpeg':
                    $image = (string) Image::make($file)->encode("jpg", 75);
                    break;

                case 'image/png':
                    $image = (string) Image::make($file)->encode("png", 75);
                    break;
            }

            $brand = str_replace(' ', '-', strtolower($brand));
            $model = str_replace(' ', '-', strtolower($model));

            $path = 'gallery/vehicle/' . $brand . '/' . $model;

            if (@file_get_contents($file)) {
                try {
                    $s3 = Storage::disk('s3');
                    $filePath = $path.'/' . $image_name;
                    $s3->put($filePath, $image, "public");
                    @unlink($compressed_image); //remove compressed file

                    return $s3->url($filePath);
                } catch (Exception $e) {
                    return $e;
                }
            }

        } catch (Exception $e) {
            return $e;
        }
    }

    public function getMajorVehicleBrands()
    {
        return Vehicle::select('brand', DB::raw('COUNT(brand) as variants'))
            ->groupBy('brand')
            ->orderBy('variants', 'desc')
            ->limit(8)
            ->get();
    }
}