<?php

namespace App\Repositories;
use App\Models\Query;

class InquiriesRepo
{
    public static function all($input)
    {
        $inquiries = Query::orderBy('created_at', 'desc');

        if (!empty($input['search'])) {
            $inquiries->where('name', 'LIKE', '%'.$input['search'].'%')
            ->orWhere('email', 'LIKE', '%'.$input['search'].'%');
        }

        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'new':
                    $inquiries->orderBy('created_at', 'desc');
                    break;
                case 'old':
                    $inquiries->orderBy('created_at', 'asc');
                    break;
            }
        }

        return $inquiries->paginate(20);
    }

    public static function getQueryById($id)
    {
        return Query::find($id);
    }
}
