<?php

namespace App\Repositories;
use App\Models\OrderOffline;
use App\Models\StoreUserMapping;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Wallet;
use App\Models\Country;

class OrderOfflineRepo
{

    public static function get_orders_offline($mer_id, $input)
    {
        $orders = OrderOffline::select('order_offline.*', 'nm_customer.cus_id', 'nm_customer.cus_name', 'nm_merchant.mer_fname', 'nm_merchant.mer_id', 'nm_store.stor_name', 'wallets.name_en as wallet_name');

        if(\Auth::guard('storeusers')->check()) {
            $assigned_stores = StoreUserMapping::where('storeuser_id','=',\Auth::guard('storeusers')->user()->id)->pluck('store_id')->toArray();
            $orders->whereIn('store_id', $assigned_stores);
        }

        if($mer_id != 'all')
            $orders->where('order_offline.mer_id', '=', $mer_id);

        if (!empty($input['mid']))
            $orders->where('order_offline.mer_id', '=', $input['mid']);

        $orders->leftJoin('nm_customer', 'nm_customer.cus_id', '=', 'order_offline.cust_id');
        $orders->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'order_offline.mer_id');
        $orders->leftJoin('nm_store', 'nm_store.stor_id', '=','order_offline.store_id');
        $orders->leftJoin('wallets', 'wallets.id', '=','order_offline.wallet_id');
        $orders->groupBy('order_offline.id');

        if (!empty($input['sid']))
            $orders->where('nm_store.stor_id', '=', $input['sid']);


        if(!empty($input['merchant_countries'])) {
            $orders->where(function($query) use ($input){
                $query->whereIn('nm_merchant.mer_co_id', $input['merchant_countries']);
            });
        }

        if(!empty($input['customer_countries'])) {
            $orders->where(function($query) use ($input){
                $query->whereIn('nm_customer.cus_country', $input['customer_countries']);
            });
        }

        if(isset($input['admin_countries']) && $input['admin_countries'])
        {
            $orders->withAndWhereHas('country', function($query) use ($input)
            {
                $query->where(function($query) use ($input) {
                    $query->whereIn('co_id', $input['admin_countries']);
                });
            });
        }

        if (!empty($input['id'])) {
            $orders->where('order_offline.inv_no', 'LIKE', '%'.$input['id'].'%');
        }

        if (!empty($input['store']))
            $orders->where('order_offline.store_id', '=', $input['store']);

        if (!empty($input['tax_inv_no']))
        {
            $search = '%'.$input['tax_inv_no'].'%';
            $orders->leftJoin('nm_country', 'nm_country.co_curcode', '=', 'order_offline.currency');
            $orders->where(function ($query) use ($search) {
                $query->where('tax_inv_no', '!=', '');
                $query->whereRaw("CASE WHEN nm_country.co_id IS NOT NULL THEN CONCAT(nm_country.co_code, '-', 'OFA', tax_inv_no) ELSE tax_inv_no END LIKE ?", [$search]);
            });
        }

        if (!empty($input['cid']))
            $orders->where('nm_customer.cus_id', '=', $input['cid']);

        if (!empty($input['search'])) {
            $search = '%'.$input['search'].'%';
            $orders->where(function($q) use ($search) {
                $q->whereRaw('nm_merchant.mer_fname LIKE ? or nm_customer.cus_name LIKE ? or nm_store.stor_name LIKE ?', [$search, $search, $search]);
            });
        }

        if (!empty($input['start']) && !empty($input['end'])) {
            $range_type = ($input['type'] != '') ? $input['type'] : 'created_at';
            $input['start'] = Carbon::createFromFormat('d/m/Y', $input['start'])->startOfDay()->toDateTimeString();
            $input['end'] = Carbon::createFromFormat('d/m/Y', $input['end'])->endOfDay()->toDateTimeString();

            $orders->where('order_offline.'.$range_type, '>=', \Helper::TZtoUTC($input['start']));
            $orders->where('order_offline.'.$range_type, '<=', \Helper::TZtoUTC($input['end']));
        }

        if (!empty($input['status']) || strlen($input['status']))
            $orders->where('order_offline.status', '=', $input['status']);

        if (!empty($input['sort'])) {
            switch ($input['sort']) {
                case 'new':
                    $orders->orderBy('order_offline.id', 'desc');
                    break;
                case 'old':
                    $orders->orderBy('order_offline.id', 'asc');
                    break;
                default:
                    $orders->orderBy('order_offline.id', 'desc');
                    break;
            }
        } else {
            $orders->orderBy('order_offline.id', 'desc');
        }

        if (!empty($input['action']) && $input['action'] == 'export')
            return $orders->get();

        return $orders->paginate(50);
    }

    public static function get_order_offline_details($id)
    {
        $order = OrderOffline::select('order_offline.*', 'nm_customer.cus_id', 'nm_customer.cus_name', 'nm_customer.email', 'nm_customer.cus_phone', 'nm_merchant.mer_fname', 'nm_merchant.mer_lname')
        ->addSelect(DB::raw('concat_ws(" ", nm_merchant.mer_fname, nm_merchant.mer_lname) as merchant_name, nm_merchant.mer_address1, nm_merchant.mer_address2, nm_merchant.zipcode, nm_merchant.mer_city_name as mer_city, nm_state.name as mer_state, nm_country.co_name as mer_country, nm_merchant.bank_gst as mer_bank_gst, nm_merchant.mer_phone as mer_phone'))
        ->where('order_offline.id', '=', $id)
        ->leftJoin('nm_customer', 'nm_customer.cus_id', '=', 'order_offline.cust_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'order_offline.mer_id')
        ->leftJoin('nm_state', 'nm_state.id', '=', 'nm_merchant.mer_state')
        ->leftJoin('nm_country', 'nm_country.co_id', '=', 'nm_merchant.mer_co_id');

        if (\Auth::guard('merchants')->check()) {
            $mer_id = \Auth::guard('merchants')->user()->mer_id;
            $order->where('order_offline.mer_id', '=', $mer_id);
        }

        return $order->first();
    }

    public static function get_grand_total($mer_id = null, $input = null)
    {
        $total =  OrderOffline::select(DB::raw("sum(order_offline.merchant_charge_token) as merchant_charge_token"));

        if ($mer_id == 'all') {
            $total->addSelect(DB::raw("sum(order_offline.merchant_platform_charge_token) as merchant_platform_charge_token, sum(order_offline.customer_charge_token) as customer_charge_token, sum(order_offline.order_total_token) as v_credit"));
        } else {
            $total->addSelect(DB::raw("sum(order_offline.v_token) as v_credit"));
            $total->where('order_offline.mer_id', '=', $mer_id);
        }

        if(\Auth::guard('storeusers')->check()) {
            $assigned_stores = StoreUserMapping::where('storeuser_id','=',\Auth::guard('storeusers')->user()->id)->pluck('store_id')->toArray();
            $total->whereIn('order_offline.store_id', $assigned_stores);
        }

        if (!empty($input['mid']))
            $total->where('order_offline.mer_id', '=', $input['mid']);

        $total->leftJoin('nm_customer', 'nm_customer.cus_id', '=', 'order_offline.cust_id');
        $total->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'order_offline.mer_id');
        $total->leftJoin('nm_store', 'nm_store.stor_id', '=','order_offline.store_id');

        if(isset($input['admin_countries']) && $input['admin_countries'])
        {
            $total->withAndWhereHas('country', function($query) use ($input)
            {
                $query->where(function($query) use ($input) {
                    $query->whereIn('co_id', $input['admin_countries']);
                });
            });
        }

        if(!empty($input['merchant_countries'])) {
            $total->where(function($query) use ($input){
                $query->whereIn('nm_merchant.mer_co_id', $input['merchant_countries']);
            });
        }

        if(!empty($input['customer_countries'])) {
            $total->where(function($query) use ($input){
                $query->whereIn('nm_customer.cus_country', $input['customer_countries']);
            });
        }

        if (!empty($input['id'])) {
            $total->where(function($query) use ($input) {
                $query->where('order_offline.inv_no', '=', $input['id'])
                ->orwhere('order_offline.id', '=', $input['id']);
            });
        }

        if (!empty($input['store']))
            $total->where('order_offline.store_id', '=', $input['store']);

        if (!empty($input['tax_inv_no']))
        {
            $number = preg_replace('/\D/', '', $input['tax_inv_no']);
            $total->where('order_offline.tax_inv_no', 'LIKE', '%'.$number.'%');
        }

        if (!empty($input['cid']))
            $total->where('nm_customer.cus_id', '=', $input['cid']);

        if (!empty($input['search'])) {
            $search = '%'.$input['search'].'%';
            $total->where(function($q) use ($search) {
                $q->whereRaw('nm_merchant.mer_fname LIKE ? or nm_customer.cus_name LIKE ? or nm_store.stor_name LIKE ?', [$search, $search, $search]);
            });
        }

        if (!empty($input['start']) && !empty($input['end'])) {
            $range_type = ($input['type'] != '') ? $input['type'] : 'created_at';
            $input['start'] = Carbon::createFromFormat('d/m/Y', $input['start'])->startOfDay()->toDateTimeString();
            $input['end'] = Carbon::createFromFormat('d/m/Y', $input['end'])->endOfDay()->toDateTimeString();

            $total->where('order_offline.'.$range_type, '>=', \Helper::TZtoUTC($input['start']));
            $total->where('order_offline.'.$range_type, '<=', \Helper::TZtoUTC($input['end']));
        }

        if (!empty($input['status']) || strlen($input['status']))
            $total->where('order_offline.status', '=', $input['status']);

        return $total->first();
    }

    public static function get_order_offline_by_id($id)
    {
        return $order = OrderOffline::leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'order_offline.mer_id')
        ->where('order_offline.id', $id)
        ->first();
    }

    public static function generate_tax_inv($order_id)
    {
        $order = OrderOffline::find($order_id);
        if(!$order)
        {
            return null;
        }

        $country = Country::where('co_curcode', $order->currency)->first();
        if(!$country)
        {
            return null;
        }

        $date = Carbon::now($country->timezone);
        $year = $date->format('y');
        $month = $date->format('m');
        $runningNo = 1;

        $taxInvoicesNumber = OrderOffline::where('currency', $order->currency)->where('tax_inv_no', 'LIKE', $year.$month.'%')->orderBy('tax_inv_no', 'desc')->value('tax_inv_no');

        if($taxInvoicesNumber)
        {
            $runningNo = (integer)substr($taxInvoicesNumber, 4) + 1;
        }

        return $year.$month.str_pad($runningNo, 4, '0', STR_PAD_LEFT);
    }

    public static function check_inv($inv_tax_no)
    {

        $order_tax_inv_exist = OrderOffline::where('tax_inv_no','=',$inv_tax_no)->exists();

        if($order_tax_inv_exist == true){

            self::generate_tax_inv();
        }
        else{
            return $inv_tax_no;
        }
    }

    public static function find_order_offline($id)
    {
        return OrderOffline::with('merchant', 'customer')->where('id', $id)->first();
    }

    public static function get_order_offline($id, $merchant_id = null)
    {
        $orders = OrderOffline::with(['merchant' => function ($q) use ($merchant_id) {
            if($merchant_id)
            {
                $q->where('mer_id', $merchant_id);
            }
        }, 'customer']);

        if($merchant_id)
        {
            $orders->whereHas('merchant', function ($q) use ($merchant_id) {
                $q->where('mer_id', $merchant_id);
            });
        }

        if(is_array($id))
        {
            $orders->whereIn('id', $id);
        }
        else
        {
            $orders->where('id', $id);
        }

        $orders = $orders->orderBy('id', 'desc')->get();

        return $orders;
    }

    public static function should_generate_invoice($wallet_id)
    {
        $byWallet = false;
        $onlyWallet = Wallet::where('generate_offline_invoice', 1)->pluck('id');

        //include transaction without wallet and special wallet
        $onlyWallet->push(0);
        $onlyWallet->push(99);

        if($onlyWallet->contains($wallet_id))
        {
            $byWallet = true;
        }

        if($byWallet)
        {
            return true;
        }

        return false;
    }
}