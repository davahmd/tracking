<?php

namespace App\Repositories;
use App\Models\ProductPricing;
use App\Models\ProductAttribute;
use App\Models\Cart;
use App\Models\PricingAttributeMapping;
use App\Models\Product;
use App\Models\Store;
use App\Models\StoreService;
use App\Models\StoreServiceExtra;

class StoreServiceExtraRepo
{
    public static function get_service_list_by_pro_id($pro_id)
    {
        return StoreServiceExtra::where('pro_id', $pro_id)
        ->orderBy('id','asc')
        ->get();
    }

    public static function add_extra_service($store_service_schedule_id, $pro_id, $data)
    {
        $exists = StoreServiceExtra::where('pro_id', $pro_id)
        ->where('service_name', trim($data['service_name']))
        ->exists();

        if(!$exists) {
            StoreServiceExtra::create([
                'pro_id' => $pro_id,
                'store_service_schedule_id' => $store_service_schedule_id,
                'service_name' => trim($data['service_name']),
                'service_price' => trim($data['service_price']),
            ]);

            return 'success';
        } else {
            return 'existed';
        }

        return 'failed';
    }

    public static function delete_service($service_id)
    {
        StoreServiceExtra::find($service_id)->delete();
        return true;
    }

    public static function get_service_by($service_id)
    {
        $items = StoreServiceExtra::where('id', $service_id)->get();

        if ($items) {
            foreach ($items as $key => $item) {
                $results['service_name'] = $item->service_name;
                $results['service_price'] = $item->service_price;
                $results['items'][] = $item;
            }
        }

        return $results;
    }

    public static function update_service($data, $service_id)
    {
        $service = StoreServiceExtra::where('id', $service_id)->where('service_name','!=', $data['old_service_name'])->where('service_name','=', $data['service_name'])->count();

        if($service > 0)
            return false;

        $update = StoreServiceExtra::where('id', $service_id)->where('service_name', $data['old_service_name'])->update([
            'service_name' => $data['service_name'],
            'service_price' => $data['service_price'],
        ]);

        return true;
    }
}