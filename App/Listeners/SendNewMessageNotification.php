<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

use App\Events\MessageCreated;
use App\Notifications\NewMessage;

class SendNewMessageNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageCreated  $event
     * @return void
     */
    public function handle(MessageCreated $event)
    {
        $chat = $event->chat;

        $chat->store()->first(['stor_merchant_id'])->merchant()->first(['mer_id'])->notify(new NewMessage($chat));
        $chat->customer()->first(['cus_id'])->notify(new NewMessage($chat));
    }
}
