<?php

namespace App\Http\ViewComposers;

use Cache;
use Illuminate\View\View;
use App\Repositories\CategoryRepo;
use App\Repositories\CacheRepo;

class CategoryComposer
{
    public $categories = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(CategoryRepo $categoryrepo)
    {
        // $this->categories = Cache::rememberForever('featured_categories', function() {
        //     return CacheRepo::nav_parent_category();
        // });

        $this->category = $categoryrepo;
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = $this->category->all();

        $view
        ->with('categories', $categories);
    }
}
