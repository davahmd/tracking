<?php

namespace App\Http\ViewComposers;

use Cookie;
use Auth;
use Illuminate\View\View;
use App\Repositories\OrderRepo;
use App\Models\Country;
use App\Services\Cart\CartService;

class CartComposer
{
    public function compose(View $view)
    {
        // $user_id = (Auth::user()) ? Auth::user()->cus_id : 0;
        $user_id = auth()->check() ? auth()->user()->customer->cus_id : 0;

        $cartService = new CartService;

        $carts = $cartService->listing($user_id);
        $wholesaleCarts = $cartService->listing($user_id, null, true);
        $total = $cartService->total($carts);
        $wholesaleTotal = $cartService->total($wholesaleCarts);
        $view->with(compact('carts', 'wholesaleCarts', 'total', 'wholesaleTotal'));
    }
}