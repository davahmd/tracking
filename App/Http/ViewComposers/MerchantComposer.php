<?php

namespace App\Http\ViewComposers;

use Auth;
use Illuminate\View\View;
use App\Repositories\MerchantRepo;
use App\Repositories\StoreRepo;

class MerchantComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(MerchantRepo $merchantrepo, StoreRepo $storerepo)
    {
        $this->merchant = $merchantrepo;
        $this->store = $storerepo;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(auth()->user()->merchant)
        {
            // $mer_id = (\Auth::guard('merchants'))? \Auth::guard('merchants')->user()->mer_id : 0;
            $mer_id = auth()->user()->merchant->mer_id;
            $merchant = auth()->user()->merchant;
            $store_count = count($this->store->get_stores_by_merchant($mer_id));
            $type = 'merchants';
            $route = 'merchant';
            $view->with('merchant', $merchant)->with('store_count', $store_count)->with('logintype', $type)->with('route',$route);

        }

        if(auth()->user()->storeUser) {
            $mer_id = auth()->user()->storeUser->mer_id;
            $merchant = $this->merchant->get_merchant($mer_id);
            $type = 'storeusers';
            $name = auth()->user()->storeUser->name;
            $route = 'store';
            $view->with('merchant', $merchant)->with('logintype', $type)->with('name',$name)->with('route',$route);
        }
    }
}
