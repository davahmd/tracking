<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Broadcast;

class BroadcastController extends Controller
{
    /**
     * Authenticate the request for channel access.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        if ($request->hasHeader('X-GUARD')) {
            $guard = $request->header('X-GUARD');

            if ($request->user() && $guard == 'merchants') {
                $authUser = $request->user()->authMerchantUser();
            }
            elseif ($request->user('api')) {
                if ($guard == 'api_members') {
                    $authUser = $request->user('api')->customer;
                }
                elseif ($guard == 'api_merchants') {
                    $authUser = $request->user('api')->authMerchantUser();
                }
            }
        }
        elseif ($request->user()) {
            $authUser = $request->user()->customer;
        }

        $request->setUserResolver(function () use ($authUser) {
            return $authUser;
        });

//        \Log::info($request->user());

        return Broadcast::auth($request);
    }
}