<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\EmailQueue;
use App\Models\PaymentUser;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\Store;
use App\Models\OrderOffline;
use App\Models\ParentOrder;
use App\Models\Customer;

class EmailController extends Controller {

    protected $mailer;

    public function __construct(Mailer $mailer) {
        $this->mailer = $mailer;
    }

    public function orderMailMerchant() {

        $transactions = ParentOrder::whereHas('items', function ($q) {
            $q->where('order_type', '=', 1)
            ->where('notify_mer', '=', 0);
        })
        ->with(
            [
                'items' => function ($q) {
                    $q->select('order_id', 'parent_order_id', 'order_pro_id', 'order_qty', 'order_vtokens', 'nm_merchant.mer_id', 'nm_merchant.mer_fname', 'nm_merchant.email')
                    ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
                    ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
                    ->where('order_type', '=', 1)
                    ->where('notify_mer', '=', 0);
                }
            ], 'items.product', 'shipping_address', 'shipping_address.country', 'shipping_address.state'
        )
        ->get();

        $success = [];
        foreach ($transactions as $transaction)
        {
            $shipping = $transaction->shipping_address;

            foreach ($transaction->items->groupBy('mer_id') as $items)
            {
                $merchant = json_decode(json_encode([
                    'name' => $items->first()->mer_fname,
                    'email' => $items->first()->email,
                ]));

                $orders = [];
                foreach ($items as $order) {
                    $orders[] = json_decode(json_encode([
                        'product' => $order->product? $order->product->pro_title_en : "",
                        'options' => !empty($order->order_attributes)? json_decode($order->order_attributes) : null,
                        'quantity' => $order->order_qty,
                        'credit' => round($order->total()['price'] / $order->order_qty, 4),
                        'total_credit' => $order->total()['price'],
                    ]));
                }

                if($merchant->email)
                {
                    $this->mailer->send('front.emails.checkout_merchant', ['transaction' => $transaction, 'shipping' => $shipping, 'orders' => $orders], function (Message $m) use ($merchant) {
                        $m->to($merchant->email, $merchant->name)->subject('New Order!');
                    });

                    $success = $items->pluck('order_id')->toArray();
                    Order::whereIn('order_id', $success)->update(['notify_mer' => 1]);
                }
            }
        }
    }

    public function orderMailCustomer() {

        $transactions = ParentOrder::whereHas('items', function ($q) {
            $q->where('order_type', '=', 1)
            ->where('notify_cus', '=', 0);
        })
        ->whereHas('customer', function($q) {
            $q->where('email_verified', 1);
        })
        ->with(
            [
                'items' => function ($q) {
                    $q->select('order_id', 'parent_order_id', 'order_pro_id', 'order_qty', 'order_vtokens')
                    ->where('order_type', '=', 1)
                    ->where('notify_cus', '=', 0);
                }
            ],
            [
                'customer' => function ($q) {
                    $q->where('email_verified', 1);
                }
            ], 'items.product', 'shipping_address', 'shipping_address.country', 'shipping_address.state'
        )
        ->get();

        $success = [];
        foreach ($transactions as $transaction)
        {
            $orders = [];
            foreach ($transaction->items as $order)
            {
                $orders[] = json_decode(json_encode([
                    'product' => $order->product? $order->product->pro_title_en : "",
                    'options' => !empty($order->order_attributes)? json_decode($order->order_attributes) : null,
                    'quantity' => $order->order_qty,
                    'credit' => round($order->total()['price'] / $order->order_qty, 4),
                    'total_credit' => $order->total()['price'],
                ]));
            }

            $customer = $transaction->customer;
            $shipping = $transaction->shipping_address;

            if($customer)
            {
                $this->mailer->send('front.emails.checkout_customer', ['transaction' => $transaction, 'shipping' => $shipping, 'orders' => $orders], function (Message $m) use ($customer) {
                    $m->to($customer->email, $customer->cus_name)->subject('Order Success!');
                });

                $success = $transaction->items->pluck('order_id')->toArray();
                Order::whereIn('order_id', $success)->update(['notify_cus' => 1]);
            }
        }
    }

    public function emailQueue()
    {
        $emails = EmailQueue::where('send', 0)->get();
        $success = collect([]);

        foreach ($emails as $email) {

            switch ($email->jobs) {
                case 'LimitAlert':
                    $result = $this->limitAlertEmailProcess($email);
                    if($result)
                        $success->push($email->id);
                    break;

                default:
                    $this->emailError('Invalid email jobs', $email);
                    break;
            }
        }

        EmailQueue::whereIn('id', $success->toArray())->delete();
    }

    protected function limitAlertEmailProcess($email) {

        $subject = $this->getEmailSubject($email->type);
        switch ($email->notifiable_type) {
            case 'Customer':
                $customer = Customer::find($email->notifiable_id);
                if(!$customer || !$customer->limit || $customer->limit->alerts->isEmpty())
                    $this->emailError('Either customer, limit or alerts not found', $email);

                $response = json_decode($email->data);
                $data = [
                    'user_type' => 'customer',
                    'customer' => $customer,
                    'currency' => $customer->country? $customer->country->co_curcode : null,
                    'detail' => [
                        'limit_type' => strtolower($response->limit_type),
                        'block_type' => strtolower($response->block_type), // amount | number
                        'current' => $response->current,
                        'alert_when' => $response->alert_when
                    ],
                ];

                $this->mailer->send('front.emails.limit.email', $data, function (Message $m) use ($customer, $subject) {
                    $m->to($customer->email, $customer->cus_name)
                    ->cc(config('mail.operation.address'), config('mail.operation.name'))
                    ->subject($subject);
                });

                return true;
                break;

            case 'Store':
                $store = Store::find($email->notifiable_id);
                if(!$store || !$store->limit || $store->limit->alerts->isEmpty())
                    $this->emailError('Either store, limit or alerts not found', $email);

                $merchant = $store->merchant;
                if(!$merchant)
                    $this->emailError('Store merchant not found', $email);

                $response = json_decode($email->data);
                $data = [
                    'user_type' => 'merchant',
                    'merchant' => $merchant,
                    'store' => $store,
                    'currency' => $store->country? $store->country->co_curcode : null,
                    'detail' => [
                        'limit_type' => strtolower($response->limit_type),
                        'block_type' => strtolower($response->block_type), // amount | number
                        'current' => $response->current,
                        'alert_when' => $response->alert_when
                    ],
                ];

                $this->mailer->send('front.emails.limit.email', $data, function (Message $m) use ($merchant, $subject) {
                    $m->to($merchant->email, $merchant->merchantName())
                    ->cc(config('mail.operation.address'), config('mail.operation.name'))
                    ->subject($subject);
                });

                return true;
                break;

            default:
                $this->emailError('Invalid email notifiable type', $email);
                break;
        }

        return false;
    }

    protected function getEmailSubject($type)
    {
        switch ($type) {
            case 'SingleLimitNotify':
                return 'Single Transaction Limit Alert Notification';
                break;

            case 'DailyLimitNotify':
            case 'DailyLimitNumberNotify':
                return 'Daily Transaction Limit Alert Notification';
                break;

            case 'WeeklyLimitNotify':
            case 'WeeklyLimitNumberNotify':
                return 'Weekly Transaction Limit Alert Notification';
                break;

            case 'MonthlyLimitNotify':
            case 'MonthlyLimitNumberNotify':
                return 'Monthly Transaction Limit Alert Notification';
                break;

            case 'YearlyLimitNotify':
            case 'YearlyLimitNumberNotify':
                return 'Yearly Transaction Limit Alert Notification';
                break;

            default:
                return '';
                break;
        }
    }

    protected function emailError($remarks, $email)
    {
        $email->send = 2;
        $email->remarks = $remarks;
        $email->save();
    }
}