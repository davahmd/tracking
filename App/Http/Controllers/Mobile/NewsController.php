<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Mobile\Controller;

use App\Models\News;

class NewsController extends Controller
{
    public function list()
    {
        $news = News::loadMore();

        return view('mobile.news.list', compact('news'));
    }

    public function detail(News $news)
    {
        return view('mobile.news.detail', compact('news'));
    }

    public function ajaxLoadMore()
    {
        $count = request('count');

        return News::loadMore($count)->toJson();
    }
}