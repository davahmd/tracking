<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;

use App\Models\Merchant;
use Cookie;
use Validator;
use Helper;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Session;

use App\Repositories\CategoryRepo;
use App\Repositories\CountryRepo;
use App\Repositories\HomeRepo;
use App\Repositories\ProductRepo;
use App\Repositories\ProductImageRepo;
use App\Repositories\OfficialBrandsRepo;
use App\Repositories\StoreRepo;
use App\Repositories\VehicleRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\PromotionRepo;

use App\Models\Promotion;
use App\Models\PromotionProduct;

use Carbon\Carbon;

class HomeController extends Controller
{
    public function __construct(ProductRepo $productrepo, Mailer $mailer, HomeRepo $homerepo, ProductImageRepo $productimagerepo, CategoryRepo $categoryrepo, OfficialBrandsRepo $officialbrandsrepo, StoreRepo $storerepo, PromotionRepo $promotionrepo)
    {
        $this->product = $productrepo;
        $this->mailer = $mailer;
        $this->home = $homerepo;
        $this->image = $productimagerepo;
        $this->category = $categoryrepo;
        $this->brand = $officialbrandsrepo;
        $this->store = $storerepo;
        $this->promotion = $promotionrepo;
    }

    public function about_us()
    {
        return view('front.home.about_us');
    }

    public function index()
    {
        $featured_products = ProductRepo::getProducts('featured', 6);
        $trending_products = ProductRepo::getProducts('trending', 6);
        $latest_products = ProductRepo::getProducts('latest', 24);
        $flash_products = PromotionRepo::getFlashProducts(6);

        $promo_featured_products = [];
        $promo_trending_products = [];
        $promo_latest_products = [];
        $promo_item_info = [];
        $flash_product_info = [];
        $flash_promotion_info = [];
        
        // $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();
        $today_datetime = Carbon::now()->toDateTimeString();

        foreach ($featured_products as $key => $featured_product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($featured_product->pro_id);
            if ($promo_item) {
                $promo_featured_products[$featured_product->pro_id] = $promo_item;
                $promo_item_info[$featured_product->pro_id] = PromotionProduct::where('promotion_id', $promo_featured_products[$featured_product->pro_id]->id)->where('product_id', $featured_product->pro_id)->first();
            }
        }

        foreach ($trending_products as $key => $trending_product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($trending_product->pro_id);
            if ($promo_item) {
                $promo_trending_products[$trending_product->pro_id] = $promo_item;
                $promo_item_info[$trending_product->pro_id] = PromotionProduct::where('promotion_id', $promo_trending_products[$trending_product->pro_id]->id)->where('product_id', $trending_product->pro_id)->first();
            }
        }

        foreach ($latest_products as $key => $latest_product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($latest_product->pro_id);
            if ($promo_item) {
                $promo_latest_products[$latest_product->pro_id] = $promo_item;
                $promo_item_info[$latest_product->pro_id] = PromotionProduct::where('promotion_id', $promo_latest_products[$latest_product->pro_id]->id)->where('product_id', $latest_product->pro_id)->first();
            }
        }

        foreach ($flash_products as $flash_product) {
            $flash_promotion_info[$flash_product->pro_id] = $this->promotion->new_pricing_by_promotion($flash_product->pro_id);
            $flash_product_info[$flash_product->pro_id] = PromotionProduct::where('promotion_id', $flash_promotion_info[$flash_product->pro_id]->id)->where('product_id', $flash_product->pro_id)->first();
        }

        $featured_categories = $this->category->featured_category(8);
        $official_brands = $this->brand->get_official_brands(8);
        $featured_stores = $this->store->get_featured_stores(8);
        $distributors = Merchant::active()->others()->whereIsDistributor(true)->limit(8)->get();

        return view('front.home.index', compact('featured_products','featured_categories','latest_products','trending_products','official_brands','featured_stores', 'promo_featured_products', 'promo_trending_products', 'promo_latest_products', 'promo_item_info', 'today_datetime', 'flash_products', 'flash_promotion_info', 'flash_product_info', 'distributors'));
    }

    public function setlocale()
    {
        $data = \Request::all();
        Session::forget('lang');
        Session::put('lang', $data['lang']);
        
        return back();
    }

    public function setcountry()
    {
        $data = \Request::all();
        $country = CountryRepo::get_country_by_id($data['id']);

        session(['countryid' => $country->co_id]);
        session(['timezone' => $country->timezone]);
        Cookie::queue(Cookie::forever('country_locale', $country->co_id));
        Cookie::queue(Cookie::forever('timezone', $country->timezone));
        Cookie::queue(Cookie::forever('countryid', $data['id']));
        return 'success';
    }

    public function contactUs()
    {
    	if(\Request::isMethod('post'))
        {
            $data = \Request::all();

            $v = Validator::make($data, [
                'name' => 'required',
                'email' => 'required|email',
                'subject' => 'required',
                'content' => 'required',
	        ]);

            if ($v->fails())
               return back()->withInput()->withErrors($v);

            $save = $this->home->insert_contactUs($data);
            if ($save == true)
            {
                $this->mailer->send('front.emails.contact_us', array('details'=>$data), function (Message $m) use ($data) {
                    $m->to($data['email'])->subject(config('app.name').' Enquiry');
                });

                return view('front.home.contact_us', ['success'=>trans('front.contact.success')]);
            }
            return view('front.home.contact_us');
        }
        return view('front.home.contact_us');
    }

    public function cms($url_slug)
    {
        $cms = $this->home->find_cms($url_slug);
        if ($cms) {
            return view('front.home.cms', ['cms'=>$cms]);
        }

        return view('errors.404');
    }

    public function search()
    {
        // $category = (\Request::get('category')) ? \Request::get('category') : 'all';
        // $search = (\Request::get('search')) ? \Request::get('search') : '';
        // $itemParams = (\Request::get('items')) ? \Request::get('items') : '15';
        // $sortParams = (\Request::get('sort')) ? \Request::get('sort') : 'new';
        // $results = $this->home->search_product($category, $search, $itemParams, $sortParams);

        // foreach ($results as $key => $result) {
        //     $result->main_image = $this->image->get_product_main_image($result->pro_id);

        //     $result->price = number_format($result->price,2);
        //     $result->is_discounted = 0;
        //     if ( $result->discounted_price > 0.00) {
        //         $today = new \DateTime;
        //         $discounted_from = new \DateTime($result->discounted_from);
        //         $discounted_to = new \DateTime($result->discounted_to);
        //         if (($today >= $discounted_from) && ($today <= $discounted_to))
        //         {
        //             $result->is_discounted = 1;
        //             $result->discounted_price = number_format($result->discounted_price,2);
        //         }
        //     }
        // }

        // return view('front.home.search', compact('results', 'category', 'search', 'sortParams', 'itemParams'));

        $search = (\Request::get('search')) ? \Request::get('search') : '';

        $type = 'search';
        $products = ProductRepo::getProductByFilter(0, null, null, null, null, null, null, null, null, null, 0, 12, $type, $search);

        $vehicles = [];

        if (Auth::check()) {
            $cus_id = Auth::user()->customer->cus_id;
            $vehicles = CustomerRepo::get_vehicles_by_customer_id($cus_id);
        }

        $car_brands = [];
        $product_brands = [];
        foreach ($products as $product) {
            foreach ($product->vehicles as $vehicle) {
                $car_brands[] = VehicleRepo::getBrand($vehicle->brand);
            }

            if ($product->brand_id != null) {
                $product_brands[] = OfficialBrandsRepo::get_official_brands_details($product->brand_id);
            }

            $product['pro_slug'] = Helper::slug_maker($product->pro_title_en, $product->pro_id);
        }

        $car_brands = array_unique($car_brands);
        $product_brands = array_unique($product_brands);        

        $category = null;

        return view('front.home.listing', compact('category', 'vehicles', 'product_brands', 'car_brands', 'products', 'type', 'data', 'search'));
    }

    public function getFeaturedProduct()
    {
        $type='featured';
        $products = ProductRepo::getProductByFilter(0, null, null, null, null, null, null, null, null, null, 0, 12, $type);
        // $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();
        $today_datetime = Carbon::now()->toDateTimeString();

        $vehicles = [];
        $promo_products = [];
        $promo_product_info = [];

        if (Auth::check()) {
            $cus_id = Auth::user()->customer->cus_id;
            $vehicles = CustomerRepo::get_vehicles_by_customer_id($cus_id);
        }

        $car_brands = [];
        $product_brands = [];
        foreach ($products as $product) {
            foreach ($product->vehicles as $vehicle) {
                $car_brands[] = VehicleRepo::getBrand($vehicle->brand);
            }

            if ($product->brand_id != null) {
                $product_brands[] = OfficialBrandsRepo::get_official_brands_details($product->brand_id);
            }

            $product['pro_slug'] = Helper::slug_maker($product->pro_title_en, $product->pro_id);
        }

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_product_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }

        $car_brands = array_unique($car_brands);
        $product_brands = array_unique($product_brands);        

        $category = null;

        return view('front.home.listing', compact('category', 'vehicles', 'products_brand', 'car_brands', 'products', 'type', 'data', 'promo_products', 'promo_product_info', 'today_datetime'));
    }

    public function getTrendingProduct()
    {
        $type='trending';
        $products = ProductRepo::getProductByFilter(0, 'popularity', null, null, null, null, null, null, null, null, 0, 12, $type);
        // $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();
        $today_datetime = Carbon::now()->toDateTimeString();

        $vehicles = [];
        $promo_products = [];
        $promo_product_info = [];

        if (Auth::check()) {
            $cus_id = Auth::user()->cus_id;
            $vehicles = CustomerRepo::get_vehicles_by_customer_id($cus_id);
        }

        $car_brands = [];
        $product_brands = [];
        foreach ($products as $product) {
            foreach ($product->vehicles as $vehicle) {
                $car_brands[] = VehicleRepo::getBrand($vehicle->brand);
            }

            if ($product->brand_id != null) {
                $product_brands[] = OfficialBrandsRepo::get_official_brands_details($product->brand_id);
            }

            $product['pro_slug'] = Helper::slug_maker($product->pro_title_en, $product->pro_id);
        }

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_product_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }

        $car_brands = array_unique($car_brands);
        $product_brands = array_unique($product_brands);

        $category = null;

        return view('front.home.listing', compact('category', 'vehicles', 'product_brands', 'car_brands', 'products', 'type', 'data', 'promo_products', 'promo_product_info', 'today_datetime'));
    }

    public function getFlashSaleProduct()
    {
        $type='flashsale'; 
        $products = ProductRepo::getProductByFilter(0, 'popularity', null, null, null, null, null, null, null, null, 0, 12, $type);
        // $products = PromotionRepo::getFlashProducts();
        // $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();
        $today_datetime = Carbon::now()->toDateTimeString();
        
        $vehicles = [];
        $promo_products = [];
        $promo_product_info = [];

        if (Auth::check()) {
            $cus_id = Auth::user()->customer->cus_id;
            $vehicles = CustomerRepo::get_vehicles_by_customer_id($cus_id);
        }

        $car_brands = [];
        $product_brands = [];
        foreach ($products as $product) {
            foreach ($product->vehicles as $vehicle) {
                $car_brands[] = VehicleRepo::getBrand($vehicle->brand);
            }

            if ($product->brand_id != null) {
                $product_brands[] = OfficialBrandsRepo::get_official_brands_details($product->brand_id);
            }

            $product['pro_slug'] = Helper::slug_maker($product->pro_title_en, $product->pro_id);
        }

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_product_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }

        $car_brands = array_unique($car_brands);
        $product_brands = array_unique($product_brands);

        $category = null;

        return view('front.home.listing', compact('category', 'vehicles', 'product_brands', 'car_brands', 'products', 'type', 'data', 'promo_products', 'promo_product_info', 'today_datetime'));  
    }

    public function getOfficialBrands()
    {
        $official_brands = $this->brand->get_official_brands();

        return view('front.home.brands', compact('official_brands','type'));
    }

    public function getFeaturedStores()
    {
        $featured_stores = $this->store->get_featured_stores();
        return view('front.home.featured_stores', compact('featured_stores','type'));
    }

}
