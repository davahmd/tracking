<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;

use App\Repositories\MerchantRepo;
use App\Repositories\ProductRepo;
use App\Repositories\StoreRepo;
use App\Repositories\PromotionRepo;

use App\Models\Promotion;
use App\Models\PromotionProduct;

use Carbon\Carbon;
use Helper;

class StoreController extends Controller
{
    public function __construct(MerchantRepo $merchantrepo, ProductRepo $productrepo, StoreRepo $storerepo, PromotionRepo $promotionrepo)
    {
        $this->merchant = $merchantrepo;
        $this->product = $productrepo;
        $this->store = $storerepo;
        $this->promotion = $promotionrepo;
    }

    public function view($stor_slug)
    {
        if ($stor_slug == 'login') {
            return view('merchant.auth.store_login');
        }

        $store = $this->store->get_store_by_slug($stor_slug);

        $latest_products = ProductRepo::getProducts('latest', 6, $store->stor_id);
        $trending_products = ProductRepo::getProducts('trending', 6, $store->stor_id);

        $promo_latest_products = [];
        $promo_trending_products = [];
        $promo_item_info = [];

        $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();

        foreach ($latest_products as $key => $latest_product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($latest_product->pro_id);
            if ($promo_item) {
                $promo_latest_products[$latest_product->pro_id] = $promo_item;
                $promo_item_info[$latest_product->pro_id] = PromotionProduct::where('promotion_id', $promo_latest_products[$latest_product->pro_id]->id)->where('product_id', $latest_product->pro_id)->first();
            }
        }

        foreach ($trending_products as $key => $trending_product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($trending_product->pro_id);
            if ($promo_item) {
                $promo_trending_products[$trending_product->pro_id] = $promo_item;
                $promo_item_info[$trending_product->pro_id] = PromotionProduct::where('promotion_id', $promo_trending_products[$trending_product->pro_id]->id)->where('product_id', $trending_product->pro_id)->first();
            }
        }

        return view('front.store.view', compact('store', 'latest_products','trending_products', 'promo_latest_products', 'promo_trending_products', 'promo_item_info', 'today_datetime'));
    }

    public function profile($stor_slug)
    {
        $store = $this->store->get_store_by_slug($stor_slug)->load('ratings');
        $compiledRatings = $store->compiledRatings();

        $ratings = $store->ratings()->available()->sortBy('latest')->paginate(5);

        return view('front.store.profile', compact('store', 'compiledRatings', 'ratings'));
    }

    public function latestProducts($stor_slug)
    {
        $store = $this->store->get_store_by_slug($stor_slug);
        $products = ProductRepo::getProducts('latest', 24, $store->stor_id);
        $type='latest';

        $promo_products = [];
        $promo_item_info = [];
        $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_item_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }


        return view('front.store.listing', compact('products','type','stor_slug', 'promo_products', 'promo_item_info', 'today_datetime'));
    }

    public function mostPopular($stor_slug)
    {
        $store = $this->store->get_store_by_slug($stor_slug);
        $products = ProductRepo::getProducts('trending', 24, $store->stor_id);
        $type='trending';

        $promo_products = [];
        $promo_item_info = [];
        $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_item_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }

        return view('front.store.listing', compact('products','type', 'stor_slug', 'promo_products', 'promo_item_info', 'today_datetime'));
    }
}
