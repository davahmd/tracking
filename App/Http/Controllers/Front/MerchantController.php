<?php

namespace App\Http\Controllers\Front;

use App\Models\Merchant;
use App\Models\Product;
use App\Notifications\RetailerRequested;
use App\Notifications\RetailerRequestSent;

class MerchantController extends Controller
{
    public function distributors()
    {
        $paginate = Merchant::active()
            ->others()
            ->with(['approvedRetailers' => function ($query) {
                return $query->where('mer_id', auth()->user()->merchant->mer_id);
            }])
            ->whereIsDistributor(true)
            ->paginate();

        if (request()->expectsJson()) {
            return response()->json([
                'html' => view('front.merchant.partials.merchant-list', ['merchants' => $paginate])->render(),
                'next_url' => $paginate->nextPageUrl()
            ]);
        }

        return view('front.merchant.distributors', [
            'merchants' => $paginate
        ]);
    }

    public function products()
    {
        $paginate = Product::hasWholeSalePrice()
            ->whereHas('merchant', function ($query) {
                return $query->others();
            })
            ->available()
            ->latest()
            ->paginate();

        if (request()->expectsJson()) {
            return response()->json([
                'html' => view('front.merchant.partials.product-list', ['products' => $paginate])->render(),
                'next_url' => $paginate->nextPageUrl()
            ]);
        }

        return view('front.merchant.products', [
            'products' => $paginate
        ]);
    }

    public function view($id)
    {
        $merchant = Merchant::active()->findOrFail($id);

        $distributorRelationship = null;

        if (auth()->user() && auth()->user()->merchant) {
            $distributorRelationship = $merchant->retailers()
                ->wherePivot('merchant_id', auth()->user()->merchant->mer_id)
                ->first();
        }

        $products = $merchant->wholeSaleProducts()
            ->available()
            ->latest()
            ->paginate();

        return view('front.merchant.view', [
            'merchant' => $merchant,
            'distributorRelationship' => $distributorRelationship,
            'products' => $products
        ]);
    }

    public function requestRetailer($id)
    {
        $distributor = Merchant::active()->findOrFail($id);
        $merchant = auth()->user()->merchant;

        if ($distributor->retailers()->where('mer_id', $merchant->mer_id)->exists()) {
            return back()->with('error', trans('localize.error'));
        }

        $distributor->retailers()->attach($merchant->mer_id);

        $distributor->notify(new RetailerRequested($merchant));
        $merchant->notify(new RetailerRequestSent($distributor));

        return redirect()->back()->with('success', trans('Request sent.'));
    }

    public function profile($id)
    {
        $merchant = Merchant::active()->findOrFail($id);

        return view('front.merchant.profile', compact('merchant'));
    }
}