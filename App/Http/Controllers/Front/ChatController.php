<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;
use Auth;
use DateTime;
use DB;
use Storage;
use App\Events\MessageCreated;
use App\Helpers\Helper;
use App\Models\Chat;
use App\Models\Order;
use App\Models\Product;

class ChatController extends Controller
{
    public function list()
    {
        $cus_id = Auth::user()->customer->cus_id;

        $store_groups = Chat::whereIn('id', function($query) use ($cus_id) {
            $query->from(with(new Chat)->getTable())
                    ->where('cus_id', $cus_id)
                    ->groupBy('store_id', 'pro_id')
                    ->select(DB::raw('max(id)'));
        })
        ->orderBy('created_at', 'desc')
        ->get()
        ->groupBy('store_id');

        return view('front.account.general.chat_list', compact('store_groups'));
    }

    public function scheduleListByStoreId($store_id)
    {
        $cus_id = Auth::user()->customer->cus_id;

        $arrId = Chat::where([
                'cus_id' => $cus_id,
                'store_id' => $store_id,
            ])
            ->groupBy('schedule_id')
            ->select(DB::raw('max(id) id'))
            ->pluck('id')
            ->toArray();

        $chats = Chat::whereIn('id', $arrId)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json(compact('chats'));
    }

    public function conversation($product_id)
    {
        $cus_id = Auth::user()->customer->cus_id;;

        try {
            // product
            $product = Product::where('pro_id', $product_id)
                ->select('pro_id', 'pro_sh_id')
                ->firstOrFail();

            // store
            $store = $product->store(['stor_id', 'stor_name', 'stor_img'])->firstOrFail();
            $store_id = $store->stor_id;
            $store_name = $store->stor_name ?? '';
            $store_img = asset('asset/images/logo/logo_merchant.png');
            if (Storage::exists('store/'.$store->stor_id.'/'.$store->stor_img)) {
                $store_img = Storage::url('store/'.$store->stor_id.'/'.$store->stor_img);
            }

            // order
            $order = Order::where([
                'order_cus_id' => $cus_id,
                'order_pro_id' => $product_id,
                ['order_status', '<=', 4],
            ])->first();

            // services
            $services = [];
            if ($order) {
                $services = $order->services()->whereNotNull('schedule_datetime')->get();
            }

            // chat history
            $chat_query = $product->chats(['cus_id' => $cus_id]);
            $chats = $product->chats;

            $latest_timestamp = '';
            $latest_chat = $chats->sortByDesc('created_at')->first();
            if ($latest_chat) {
                $latest_timestamp = Helper::UTCtoTZ($latest_chat->created_at, 'j/m/y g:i A');
            }

            // update these chats with customer read datetime
            $chat_query->where('sender_type', 'S')
                ->whereNull('read_at')
                ->update([
                    'read_at' => new DateTime
                ]);
        }
        catch (\Exception $e) {
            return back();
        }

        return view('front.account.general.chat', compact('product_id', 'services', 'store_id', 'store_name', 'store_img', 'latest_timestamp', 'chats'));
    }

    public function postMessage($store_id, $product_id)
    {
        $status = false;

        try {
            $customer_id = Auth::user()->customer->cus_id;

            $order = Order::where([
                'order_cus_id' => $customer_id,
                'order_pro_id' => $product_id,
            ])
            ->select(['order_id'])
            ->first();

            $order_id = null;
            if ($order) {
                $order_id = $order->order_id;
            }

            $message = request('msg');
            $message = htmlspecialchars($message);
            $message = substr($message, 0, 1000);

            $chat = Chat::create([
                'cus_id' => $customer_id,
                'store_id' => (int)$store_id,
                'pro_id' => (int)$product_id,
                'order_id' => $order_id,
                'sender_type' => 'U',
                'message' => $message
            ]);

            // update these chats with customer read datetime
            Chat::where([
                'cus_id' => $customer_id,
                'store_id' => (int)$store_id,
                'pro_id' => (int)$product_id
            ])
            ->where('sender_type', 'S')
            ->whereNull('read_at')
            ->update([
                'read_at' => new DateTime
            ]);

            broadcast(new MessageCreated($chat))->toOthers();
        }
        catch (\Exception $e) {
            return response()->json(compact('status'));
        }

        $status = true;

        return response()->json(compact('status'));
    }

    public function messagesRead($store_id, $product_id)
    {
        $status = false;

        try {
            // update these chats with customer read datetime
            Chat::where([
                'cus_id' => auth('api_members')->id(),
                'store_id' => (int)$store_id,
                'pro_id' => (int)$product_id
            ])
            ->where('sender_type', 'S')
            ->whereNull('read_at')
            ->update([
                'read_at' => new DateTime
            ]);
        }
        catch (\Exception $e) {
            return response()->json(compact('status'));
        }

        $status = true;

        return response()->json(compact('status'));
    }
}
