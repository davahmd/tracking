<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;
use Steevenz\Rajaongkir;

use App\Models\PriceNegotiation;
use Auth;
use DateTime;
use File;
use Hash;
use Validator;
use Redirect;
use App\Models\Customer;
use App\Models\MemberServiceSchedule;
use App\Models\Order;
use App\Models\DeliveryOrder;

use App\Models\ParentOrder;
use App\Models\Shipping;
use App\Repositories\ActivationRepository;
use App\Repositories\CountryRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\LimitRepo;
use App\Repositories\MemberServiceScheduleRepo;
use App\Repositories\OrderRepo;
use App\Repositories\S3ClientRepo;
use App\Repositories\SecurityQuestionRepo;
use App\Repositories\SmsRepo;
use App\Repositories\VehicleRepo;
use App\Services\Chat\ChatService;
use App\Traits\OrderOnlineLogger;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

use Carbon\Carbon;
use App\Models\Rating;
use App\Services\Rating\RatingService;

class AccountController extends Controller
{
    public function __construct(CustomerRepo $customerRepo, OrderRepo $orderRepo, MemberServiceScheduleRepo $memberServiceScheduleRepo, ChatService $chatService)
    {
        $this->customer = $customerRepo;
        $this->order = $orderRepo;
        $this->memberServiceSchedule = $memberServiceScheduleRepo;
        $this->chatService = $chatService;
    }

    public function overview()
    {
        $chat_unread = $this->chatService->getCustomerUnreadMessagesCount();
        $notification_unread = auth()->user()->unreadNotifications()->count();

        return view('front.account.general.overview', compact('chat_unread', 'notification_unread'));
    }

    public function notification()
    {
        auth()->user()->notifications()->update([
            'read_at' => Carbon::now()
        ]);

        $input = \Request::only('name');

        $notifications = auth()->user()->customer
            ->notifications()
            ->latest()
            ->paginate();

        if ($input['name']) {
            $notifications = auth()->user()
            ->notifications()
            ->where('data', 'like', '%' . $input['name'] . '%')
            ->latest()
            ->paginate();
        }

        return view('front.account.general.notification', compact('notifications', 'input'));
    }

    public function chat()
    {
        return view('front.account.general.chat');
    }

    public function favourite()
    {
        return view('front.account.general.favourites');
    }

    public function automobile()
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;

        $brands = VehicleRepo::get_brands();
        $models = VehicleRepo::get_models();
        $variants = VehicleRepo::get_variants();

        $vehicles = CustomerRepo::get_vehicles_by_customer_id($customer_id);

        return view('front.account.general.automobile', compact('brands', 'models', 'variants', 'vehicles'));
    }

    public function addAutomobile()
    {
        $customer_id = Auth::user()->customer->cus_id;

        $data = \Request::all();
        $v = Validator::make($data, [
            'vehicle' => 'required',
            'name' => 'required',
            'insurance' => 'required',
            'insurance_expired_date' => 'required',
            'road_tax_expired_date' => 'required',
            'mileage' => 'numeric',
        ]);
        if ($v->fails())
            return back()->withInput()->withErrors($v);

        try {

            $data['insurance_expired_date'] = DateTime::createFromFormat('d/m/Y', $data['insurance_expired_date'])->format('Y-m-d');
            $data['road_tax_expired_date'] = DateTime::createFromFormat('d/m/Y', $data['road_tax_expired_date'])->format('Y-m-d');

            $customer = CustomerRepo::add_vehicle($customer_id, $data);
        } catch (\Exception $e) {
            return back()->withErrors(trans('localize.Unable_to_add_automobile'));
        }
        return back()->with('success', trans('localize.Successfully_added_automobile'));
    }

    public function editAutomobile()
    {
        $customer_id = Auth::user()->customer->cus_id;

        $data = \Request::all();
        $v = Validator::make($data, [
            'vehicle' => 'required',
            'name' => 'required',
            'insurance' => 'required',
            'insurance_expired_date' => 'required',
            'road_tax_expired_date' => 'required',
            'mileage' => 'numeric',
        ]);
        if ($v->fails())
            return back()->withInput()->withErrors($v);

        try {

            $data['insurance_expired_date'] = DateTime::createFromFormat('d/m/Y', $data['insurance_expired_date'])->format('Y-m-d');
            $data['road_tax_expired_date'] = DateTime::createFromFormat('d/m/Y', $data['road_tax_expired_date'])->format('Y-m-d');

            $customer = CustomerRepo::edit_vehicle($customer_id, $data);

        } catch (\Exception $e) {
            return back()->withErrors(trans('localize.unable_to_edit_automobile'));
        }
        return back()->with('success', trans('localize.successfully_edited_automobile'));
    }

    public function setDefaultAutomobile($automobile_id)
    {
        $customer_id = Auth::user()->customer->cus_id;

        try {
            CustomerRepo::set_automobile_default($customer_id, $automobile_id);
        } catch (\Exception $e) {
            return back()->withErrors(trans('localize.unable_to_set_default_automobile'));
        }

        return back()->with('success', trans('localize.successfully_set_default_automobile'));
    }

    public function deleteAutomobile($automobile_id)
    {
        $customer_id = Auth::user()->customer->cus_id;

        $status = CustomerRepo::delete_vehicle($customer_id, $automobile_id);

        if($status)
            return back()->with('success', trans('localize.successfully_deleted_vehicle'));

        return back()->with('success', trans('localize.failed_to_delete_vehicle'));
    }

    /*public function orderHistory($type='tab-processing')
    {
        $cus_id = Auth::user()->customer->cus_id ? Auth::user()->customer->cus_id : 0;
        $cus_details = $this->customer->get_customer_by_id($cus_id);

        // $orders = Order::where('order_cus_id', $cus_id)->get();
        // $orders_initial = Order::where('order_cus_id', $cus_id);

        $customer = auth()->user()->customer;
        $orders = $customer->orders;

        $parent_orders['all'] = $this->order->get_order_details_by_cus_id($cus_id)->paginate(10);
        $parent_orders['unpaid'] = $this->order->get_parent_order_by_cus_id($cus_id, 0)->paginate(5);
        $parent_orders['processing'] = $this->order->get_parent_order_by_cus_id($cus_id, 1)->paginate(5);
        $parent_orders['packaging'] = $this->order->get_parent_order_by_cus_id($cus_id, 2)->paginate(5);
        $parent_orders['shipped'] = $this->order->get_parent_order_by_cus_id($cus_id, 3)->paginate(5);
        $parent_orders['completed'] = $this->order->get_parent_order_by_cus_id($cus_id, 4)->paginate(5);
        $parent_orders['cancelled'] = $this->order->get_parent_order_by_cus_id($cus_id, 5)->paginate(5);
        $parent_orders['installation'] = $this->order->get_parent_order_by_cus_id($cus_id, 7)->paginate(5);

        return view('front.account.general.order_history', compact('cus_details', 'orders', 'customer', 'parent_orders', 'type'));
    }*/

    public function orderHistory($type='tab-all')
    {
        $cus_id = Auth::user()->customer->cus_id ? Auth::user()->customer->cus_id : 0;
        $cus_details = $this->customer->get_customer_by_id($cus_id);

        // $orders = Order::where('order_cus_id', $cus_id)->get();
        // $orders_initial = Order::where('order_cus_id', $cus_id);

        $customer = auth()->user()->customer;
        // $orders = $customer->orders;

        switch ($type) {
            case 'tab-all':
                $status = null;
                break;
            case 'tab-unpaid':
                $status = 0;
                break;
            case 'tab-installation':
                $status = 7;
                break;
            case 'tab-processing':
                $status = 1;
                break;
            case 'tab-packaging':
                $status = 2;
                break;
            case 'tab-shipped':
                $status = 3;
                break;
            case 'tab-completed':
                $status = 4;
                break;
            case 'tab-cancelled':
                $status = 5;
                break;
            default:
                $status = 1;
                break;
        }

        $index = explode('-', $type);

        if (count($index) > 1) {
            $index = $index[1];
        } else {
            $index = 'processing';
        }

        if ($index === 'all') {
            $parent_orders[$index] = $this->order->get_order_details_by_cus_id($cus_id, request()->routeIs('order-history-wholesale'))->paginate(10);
        } else {
            $parent_orders[$index] = $this->order->get_parent_order_by_cus_id($cus_id, $status, request()->routeIs('order-history-wholesale'))->paginate(5);
        }
        // dd($parent_orders);
        return view('front.account.general.order_history', compact('cus_details', 'orders', 'customer', 'parent_orders', 'type', 'index'));
    }

    public function schedule($type='tab-unscheduled')
    {
        $cus_id = auth()->user()->customer ? auth()->user()->customer->cus_id : 0;
        $cus_details = $this->customer->get_customer_by_id($cus_id);

        $schedules['unscheduled'] = $this->memberServiceSchedule->get_service_details_from_cus_id($cus_id, array(0))->paginate(5);
        $schedules['scheduled'] = $this->memberServiceSchedule->get_service_details_from_cus_id($cus_id, array(1,2))->paginate(5);
        $schedules['confirmed'] = $this->memberServiceSchedule->get_service_details_from_cus_id($cus_id, array(3))->paginate(5);
        $schedules['completed'] = $this->memberServiceSchedule->get_service_details_from_cus_id($cus_id, array(4))->paginate(5);
        $schedules['cancelled'] = $this->memberServiceSchedule->get_service_details_from_cus_id($cus_id, array(-1))->paginate(5);

        // foreach($schedules as $s => $values) {
        //     dump($values->groupBy('parent_order_id'));
        // }

        return view('front.account.general.schedule_history', compact('cus_details', 'schedules', 'type'));
    }

    public function profileDetails()
    {
        // $customer_id = Auth::guard('web')->user()->customer->cus_id;
        // $customer = CustomerRepo::get_customer_by_id($customer_id);
        $customer = auth()->user()->customer;

        return view('front.account.profile.profile', compact('customer'));
    }

    public function profileDetailsEdit()
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;
        $customer = CustomerRepo::get_customer_by_id($customer_id);

        $countries = CountryRepo::get_all_countries();

        if (\Request::isMethod('post')) {
            $data = \Request::all();
            $date = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
            $data['license_expired_date'] = DateTime::createFromFormat('Y-m-d', $date)->format('Y-m-d');
            Validator::make($data, [
                'name' => 'required',
                'address1' => 'required',
                'address2' => '',
                'country' => 'required',
                'state' => 'required',
                'city' => 'required',
                'subdistrict' => 'required',
                'zipcode' => 'required|numeric',
                'day' => 'required|numeric',
                'month' => 'required|numeric',
                'year' => 'required|numeric',
                'license_expired_date' => 'required|date|date_format:Y-m-d',
            ])->validate();

            try {
                $customer = CustomerRepo::update_customer_accountInfo($customer_id, $data);
            } catch (\Exception $e) {
                return back()->withErrors(trans('localize.unable_to_edit_profile'));
            }
            return view('front.account.profile.profile', compact('customer', 'countries'))->with('success', trans('localize.successfully_updated_profile'));
        }
        return view('front.account.profile.profile_edit', compact('customer', 'countries'));
    }

    public function avatarUpload()
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;
        $customer = CustomerRepo::get_customer_by_id($customer_id);

        $countries = CountryRepo::get_all_countries();

        $filename = date('Ymd').'_'.str_random(4);

        $data = \Request::all();
        Validator::make($data, [
            'image' => 'mimes:jpg,jpeg,png|max:2000|required',
        ])->validate();

        try {
            CustomerRepo::upload_avatar($data['image'], $filename);
            $customer = CustomerRepo::update_avatar_path($customer_id, $data, $filename);
        } catch (\Exception $e) {
            return back()->withErrors(trans('localize.unable_to_edit_profile'));
        }
        return back()->with('success', trans('localize.successfully_updated_profile'));
    }

    public function deliveryAddr()
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;
        // $customer = CustomerRepo::get_customer_by_id($customer_id);
        $customer = auth()->user()->customer;

        $countries = CountryRepo::get_all_countries();
        $shippings = CustomerRepo::get_customer_shipping_detail($customer_id);

        if (\Request::isMethod('post')) {
            $data = \Request::all();

            if(array_key_exists("isdefault", $data)){
                $data['isdefault'] = "1";
            }else{
                $data['isdefault'] = "0";
            }

            Validator::make($data, [
                'ship_name' => 'required|max:255',
                'areacode' => 'required',
                'phone' => 'required|numeric',
                'address1' => 'required',
                'address2' => '',
                'country' => 'required',
                'state' => 'required',
                'city' => 'required',
                'subdistrict' => 'required',
                'zipcode' => 'required|numeric',
            ])->validate();

            try {
                CustomerRepo::create_shipping_address($customer_id, $data);
            } catch (\Exception $e) {
                return back()->withErrors(trans('localize.unable_to_add_delivery_address'));
            }
            return back()->with('success', trans('localize.successfully_added_delivery_address'));
        }
        return view('front.account.profile.delivery', compact('customer', 'shippings', 'countries'));
    }

    public function setDefaultAddr($ship_id)
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;

        try {
            CustomerRepo::set_shipping_default($ship_id, $customer_id);
        } catch (\Exception $e) {
            return back()->withErrors(trans('localize.unable_to_set_default_delivery_address'));
        }
        return back()->with('success', trans('localize.successfully_set_default_delivery_address'));
    }

    public function deleteAddr($ship_id)
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;

        try {

            CustomerRepo::delete_shipping_address($customer_id, $ship_id);
            CustomerRepo::auto_set_shipping_default($customer_id);

        } catch (\Exception $e) {
            return back()->withErrors(trans('localize.unable_to_delete_delivery_address'));
        }
        return back()->with('success', trans('localize.succesfully_deleted_delivery_address'));
    }

    public function payment()
    {
        return view('front.account.profile.payment');
    }

    public function changePassword()
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;
        $customer = CustomerRepo::get_customer_by_id($customer_id);

        if(\Request::isMethod('post'))
        {
            $data = \Request::all();

            \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
              return Hash::check($value, current($parameters));
            });

            $message = [
                'old_password.old_password' => trans('localize.old_password_validation'),
                'old_password.required' => trans('localize.oldpasswordInput'),
                'password.required' => trans('localize.password_error'),
                'old_password.min' => trans('localize.minpassword'),
                'password.confirmed' => trans('localize.matchpassword'),
            ];

            $rules = [
                'password' => 'required|min:6|confirmed',
                'old_password' => 'required|min:6|old_password:' . auth()->user()->password
            ];

            $validator = Validator::make($data, $rules, $message);

            if ($validator->fails())
                return back()->withInput()->withErrors($validator);

            if ($data['password'] == $data['old_password'])
                return back()->withInput()->withErrors(trans('localize.unique_password'));

            $new_password = bcrypt($data['password']);
            // $customer->password= $new_password;
            // $customer->save();

            auth()->user()->update([
                'password' => $new_password
            ]);

            return view('front.account.setting.password')->with('success',trans('localize.passwordupdated'));
        }

        return view('front.account.setting.password');
    }

    public function notificationSetting()
    {
        return view('front.account.setting.notification');
    }

    public function chatSetting()
    {
        return view('front.account.setting.chat');
    }

    public function language()
    {
        return view('front.account.setting.language');
    }

    public function help()
    {
        return view('front.account.support.help');
    }

    public function faq()
    {
        return view('front.account.support.faq');
    }

    public function orderDetail($transaction_id)
    {
        $customer_id = Auth::guard('web')->user()->customer->cus_id;
        $parent_order = ParentOrder::where('transaction_id', $transaction_id)->where('customer_id', $customer_id)->firstOrFail();
        
        $orderWithShipping = $parent_order->items->filter(function ($item) {
            return $item->service_ids == null || $item->service_ids == '';
        })->pluck('order_id')->first();
        
        $shipping = null;
        
        if($orderWithShipping) {
            $shipping = Shipping::where('ship_order_id', $orderWithShipping)->first();
        }

        $config['api_key'] = 'efb8a3e10dfc7b83268b4ff16824ff3c';
        $config['account_type'] = 'pro';
        $waybill = new Rajaongkir($config);
        $deliveryOrder = OrderRepo::find_delivery_order($orderWithShipping);
        $courier = DeliveryOrder::find($deliveryOrder->id)->courier->code;
        $rajaongkir = $waybill->getWaybill($deliveryOrder->tracking_number, $courier);
        $details = $rajaongkir['details'];
        $manifest = $rajaongkir['manifest'];
        // var_dump($rajaongkir['details']);die();
      
        return view('front.account.order.parent_order_detail', compact('parent_order', 'shipping','manifest', 'details'));
        
    }

    public function createReview($type, $order_id)
    {
        if(!in_array((string) $type, ['product', 'store']))
        {
            abort(404);
        }

        $order = Order::find($order_id);

        if(!$order || !$order->customer || $order->customer->cus_id <> auth()->user()->customer->cus_id || !$order->product || !$order->product->store)
        {
            abort(404);
        }

        if(($type == 'product' && !$order->rateEligibility->product) || ($type == 'store' && !$order->rateEligibility->store))
        {
            abort(404);
        }

        return view('front.account.order.rating.index', compact('order', 'type'));
    }

    public function createReviewSubmit($type, $order_id)
    {
        if(!in_array((string) $type, ['product', 'store']))
        {
            abort(404);
        }

        $order = Order::find($order_id);

        if(!$order)
        {
            abort(404);
        }

        $this->validate(request(), [
            'rating' => 'required|integer|between:1,5',
            'review' => 'max:255',
            'images.*' => 'mimes:jpg,jpeg,png|max:2000',
        ]);

        $request = request()->all();
        $request['reference'] = $type;

        $response = (new RatingService)->addReview($order_id, auth()->user()->customer->cus_id, $request)->getData();

        if($response->status == 200)
        {
            return redirect()->route('order-detail', [$order->transaction_id])->with('success', 'success');
        }

        return back()->withInput()->with('error', $response->message);
    }

    public function negotiations($type = 'active')
    {
        $query = auth()->user()->customer->negotiation()->exceptCounterOffer()
            ->latest('updated_at');

        switch ($type) {
            case 'active':
                $query->active();
                break;
            case 'declined':
                $query->declined();
                break;
            case 'completed':
                $query->completed();
                break;
            case 'expired':
                $query->expired();
                break;
        }

        $negotiations = $query->paginate();

        return view('front.account.general.negotiation.index', compact('type', 'negotiations'));
    }

}