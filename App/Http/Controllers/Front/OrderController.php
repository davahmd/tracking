<?php namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;

use App\Repositories\CalendarRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\OrderRepo;
use App\Repositories\MemberServiceScheduleRepo;
use App\Repositories\StoreRepo;

use App\Models\MemberServiceSchedule;
use App\Models\Product;
use App\Models\ParentOrder;
use App\Models\Order;
use App\Models\Store;
use App\Models\StoreServiceSchedule;
use App\Models\StoreServiceExtra;

use App\Services\Notifier\Notifier;
use App\Traits\OrderOnlineLogger;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Carbon\Carbon;
use Auth;
use Helper;
use Validator;
use DateTime;
use DateTimeZone;

class OrderController extends Controller
{
    public function __construct(Mailer $mailer, CustomerRepo $customerRepo, OrderRepo $orderRepo, MemberServiceScheduleRepo $memberServiceScheduleRepo, CalendarRepo $calendarrepo, StoreRepo $storerepo)
    {
        $this->mailer = $mailer;
        $this->calendar = $calendarrepo;
        $this->customer = $customerRepo;
        $this->order = $orderRepo;
        $this->memberServiceSchedule = $memberServiceScheduleRepo;
        $this->store = $storerepo;

    }

    // public function orderHistory()
    // {
    //     $cus_id = (Auth::user()) ? Auth::user()->cus_id : 0;
    //     $cus_details = $this->customer->get_customer_by_id($cus_id);

    //     $parent_orders['processing'] = $this->order->get_parent_order_by_cus_id($cus_id, 1);
    //     $parent_orders['packaging'] = $this->order->get_parent_order_by_cus_id($cus_id, 2);
    //     $parent_orders['shipped'] = $this->order->get_parent_order_by_cus_id($cus_id, 3);
    //     $parent_orders['completed'] = $this->order->get_parent_order_by_cus_id($cus_id, 4);
    //     $parent_orders['cancelled'] = $this->order->get_parent_order_by_cus_id($cus_id, 5);

    //     foreach($parent_orders as $p => $values) {
    //         dump($p);
    //     }
    //     return view('front.account.general.order_history', compact('cus_details', 'parent_orders'));
    // }

    public function updateOrderStatus($order_id)
    {
        $cus_id = Auth::user()->customer->cus_id;
        $order = OrderRepo::get_order_by_id($order_id);

        if (empty($order) || $order->order_status != 3 || $order->order_cus_id != $cus_id)
            return back()->with('error', trans('front.order.error.shipment'));

        $remarks = null;
        if($order->product_shipping_fees_type == 3)
            $remarks = 'Self pickup updated by customer';

        if ($order->order_status == Order::STATUS_INSTALLATION) {

            $service_uncompleted_status = MemberServiceSchedule::where('order_id', '=', $order_id)->whereIn('status', array(0,1,2,3,-1))->exists();
            if ($service_uncompleted_status)
                return back()->with('error', trans('front.order.error.double_confirm_service_completed'));
        }
        
        $result = OrderRepo::completing_merchant_order($order_id, $remarks);

        if(!$result) {
            return back()->with('error', trans('localize.unable_update_status'));
        }

        $total_price = $order->total_product_price; // get total product price * quantity
        $earning = $total_price - $order->merchant_charge_value; // get merchant earning

        $data = array(
            // 'type'=>'gamepoint',
            'merchant_firstname' => $order->mer_fname,
            'merchant_lastname' => $order->mer_lname,
            'product_title' => $order->pro_title_en,
            'order_qty' => $order->order_qty,
            'order_value' => $order->order_value,
            'transaction_id' => $order->transaction_id,
            'order_commission' => $order->merchant_charge_percentage,
            'merchant_earning' => number_format($earning, 2),
            'currency' => $order->currency
        );

//        $this->mailer->send('front.emails.merchant_order_completed', $data, function($message) use ($order)
//        {
//            $message->to($order->email, $order->mer_fname)->subject('Order is Completed!');
//        });

        (new Notifier($order))->send(Notifier::ORDER_COMPLETED);

        OrderOnlineLogger::log(null, $order->order_id, null, 'customer', 'Complete order');

        return back()->with('success', trans('localize.update_status_successfully'));
    }

    public function getOrderDetails($parent_order_id)
    {
        $cus_id = (Auth::user()) ? Auth::user()->customer->cus_id : 0;
        $cus_details = $this->customer->get_customer_by_id($cus_id);

        $order_details[] = $this->order->get_order_details_by_parent_order_id($parent_order_id);
        $shipping_details = $this->order->get_shipping_address_by_parent_order_id($parent_order_id);
        $shipping_address = Helper::getShippingAddress($parent_order_id);

        $payment_method = Order::select('payment_method')->where('parent_order_id', $parent_order_id)->first()->payment_method;
        $payment_details = $this->order->get_payment_info_by_parent_order_id($parent_order_id);

        return view('front.account.order_details', compact('order_details', 'shipping_details', 'payment_method', 'payment_details', 'shipping_address'));
    }

    // public function memberScheduleService($parent_order_id)
    public function memberScheduleService($member_service_schedule_id)
    {
        // $shipping_details = $this->order->get_shipping_address_by_parent_order_id($parent_order_id);
        // $shipping_address = Helper::getShippingAddress($parent_order_id);
        // $payment_method = Order::select('payment_method')->where('parent_order_id', $parent_order_id)->first()->payment_method;
        // $payment_details = $this->order->get_payment_info_by_parent_order_id($parent_order_id);

        // $order_info = ParentOrder::find($parent_order_id);
        // foreach($order_info->items as $o) {
        //     $service_stores[$o->order_id] = $this->order->get_service_stores_details_by_pro_id($o->order_pro_id);
        //     $member_services[$o->order_id] = $this->order->get_member_service_schedule_by_order_id($o->order_id);
        // }

        $service_details = $this->memberServiceSchedule->get_details_by_service_id($member_service_schedule_id);
        $product_url_slug = Helper::slug_maker($service_details->pro_title_en, $service_details->pro_id);

        return view('front.account.order_schedule_service', compact('service_details', 'product_url_slug'));
    }

    public function memberScheduleService_submit($member_service_schedule_id)
    {
        if (\Request::isMethod('post')) {
            $data = \Request::all();

            Validator::make($data, [
                'schedule' => 'required',
                'timeslot' => 'required',
	        ], [], [
                "schedule" => '[' . $data['service_name']. "] schedule's date",
                "timeslot" => '[' . $data['service_name']. "] schedule's time-slot",
            ])->validate();

            $member_service_info = MemberServiceSchedule::find($member_service_schedule_id);
            $utc_datetime = Carbon::createFromFormat('d/m/Y h:i A', $data['schedule-date-time'])->format('Y-m-d H:i:s');
            $date = Carbon::createFromFormat('d/m/Y h:i A', $data['schedule-date-time'])->format('Y-m-d');
  
            if ($member_service_info->schedule_datetime == $utc_datetime) {
                return back()->with('warning', trans('front.order.warning.schedule_datetime_cannot_be_same')); 
            }

            // Checking offday
            $holiday = $this->calendar->get_holiday_by_date($utc_datetime);

            $store = $this->store->get_store_by_id($member_service_info->stor_id);

            // Check is either the holiday whitelisted
            if ($holiday) {
                $whitelist = $this->calendar->check_merchant_existing_onday($store->stor_merchant_id, $date);

                if ($whitelist) {
                    $holiday = null;
                }
                else {
                    return back()->with('error', trans('front.order.error.store_offday', ['date' => $utc_datetime, 'day' => $holiday->name]));
                }
            }
            // End of holiday checking

            // Check is either the offday of merchant
            $merchant_offday = $this->calendar->get_merchant_offday_by_date($store->stor_merchant_id, $date);

            if ($merchant_offday) {
                return back()->with('error', trans('front.order.error.store_offday', ['date' => $utc_datetime, 'day' => $merchant_offday->name]));
            }
            // End of merchant offday checking

            $offday = $this->calendar->get_store_offday_by_date($member_service_info->stor_id, $utc_datetime);

            if ($offday || $holiday || $merchant_offday) {
                return back()->with('error', trans('front.order.error.store_offday', ['date' => $utc_datetime, 'day' => $offday->name]));
            }
            // End of offday

            if (\Cookie::get('timezone') != null)
                $tz = \Cookie::get('timezone');
            elseif (\Session::has('timezone'))
                $tz = \Session::get('timezone');

            $dtz = new DateTimeZone($tz);
            $tztime = new DateTime('now', $dtz);
            $offset = $dtz->getOffset( $tztime ) / 3600;
            $timezone = $tz . ' ' . "UTC" . ($offset < 0 ? $offset : "+".$offset);

            $member_service_update = MemberServiceScheduleRepo::update_member_service($utc_datetime, $member_service_schedule_id, $timezone);
            // $member_service_info = MemberServiceSchedule::find($member_service_schedule_id);
            if ($member_service_info->reschedule_count_member > 2) {

                $cancel_service_status = MemberServiceScheduleRepo::update_member_service_status_cancelled($member_service_schedule_id, -1);
                $active_status_exist = MemberServiceSchedule::where('order_id', '=', $member_service_info->order_id)->whereIn('status', array(0,1,2,3))->exists();

                if (!$active_status_exist) {
                    $cancel_order_status = OrderRepo::update_order_status($member_service_info->order_id, 5);
                }

                (new Notifier($member_service_info))->send(Notifier::SERVICE_CANCELLED);

                if ($cancel_service_status && isset($cancel_order_status)) {
                    return back()->with('warning', trans('front.order.warning.schedule_order_cancel', ['schedule' => $data['service_name']])); 
                } elseif ($cancel_service_status) {
                    return back()->with('warning', trans('front.order.warning.schedule_cancel', ['schedule' => $data['service_name']]));
                }

            }

            (new Notifier($member_service_info))->send(Notifier::SERVICE_RESCHEDULED);

            if(!$member_service_update)
                return back()->with('errors', trans('front.order.error.schedule'));

            return back()->with('success', trans('front.order.success.schedule', ['schedule' => $data['service_name']]));
        }
    }

    public function memberScheduleService_update_status($member_service_schedule_id, $status_type)
    {
        $member_service_schedule_status = $this->memberServiceSchedule->update_member_schedule_service_status($member_service_schedule_id, $status_type);
        $member_service_info = MemberServiceSchedule::find($member_service_schedule_id);
        
        $service_not_completed = MemberServiceSchedule::where('order_id', '=', $member_service_info->order_id)->whereIn('status', array(0,1,2,3,-1))->exists();

        if (!$service_not_completed) {
            $complete_order_status = OrderRepo::update_order_status($member_service_info->order_id, 4);
        }

        if (isset($complete_order_status)) {
            return back()->with('success', trans('front.order.success.schedule_order_completed')); 
        }

        if(!$member_service_schedule_status)
            return back()->with('error', trans('localize.unable_update_status'));

        return back()->with('success', trans('localize.update_status_successfully'));
    }
}