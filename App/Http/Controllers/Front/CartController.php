<?php 

namespace App\Http\Controllers\Front;

use Auth;
use Carbon\Carbon;
use Cookie;
use Request;
use Validator;

use App\Http\Controllers\Front\Controller;

use App\Helpers\Helper;

use App\Models\Cart;
use App\Models\City;
use App\Models\MemberServiceSchedule;
use App\Models\Order;
use App\Models\ParentOrder;
use App\Models\Shipping;
use App\Models\StoreServiceExtra;

use App\Repositories\CountryRepo;
use App\Repositories\CourierRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\NegotiationRepo;
use App\Repositories\ProductRepo;

use App\Services\Cart\CartService;
use App\Services\Midtrans\MidtransService;
use App\Services\Notifier\Notifier;
use App\Models\PriceNegotiation;
use App\Repositories\PromotionRepo;

class CartController extends Controller
{
    public function cart()
    {
        $customer = auth()->user()->customer;

        // $wholesaleCarts = (new CartService)->listing($customer->cus_id, null, true);
        // $carts = (new CartService)->listing($customer->cus_id);

        return view('front.cart.cart');
    }

    public function add()
    {
        $data = \Request::all();
        $customer_id = (Auth::user()) ? Auth::user()->customer->cus_id : null;
        $service_id = array();

        if(isset($data['service_request']))
        {
            if (is_array($data['service_request']))
            {
                foreach($data['service_request'] as $value)
                {
                    array_push($service_id, $value);
                }
            }
        }

        if (!isset($data['wholesale'])) {
            $data['wholesale'] = false;
        }

       $service_id=join('|', $service_id);

        // Check product store accept payment or not
        $product = ProductRepo::get_product_store($data['product_id']);
        if ($product->accept_payment != 1) {
            return back()->with('error', trans('localize.store_not_accept_payment'));
        }

        if ($data['qty'] <= 0) {
            return back()->with('error', trans('localize.product_quantity_required'));
        }

        if ((new CartService)->add($customer_id, $data['product_id'], $data['price_id'], $data['qty'], $service_id, false, $data['wholesale'])) {
            // $pro_slug = Helper::slug_maker($product->pro_title_en, $product->pro_id);
            // return redirect('product/' . $pro_slug)->with('success', trans('front.cart.success.add'));
            return back()->with('success', trans('front.cart.success.add'));
        }

        return back()->with('error', trans('common.failed'));
    }

    public function update()
    {
        $data = \Request::all();

        if ((new CartService)->updateQuantity($data['id'], $data['qty'])) {
            return 'success';
        }

        return 'error';
    }

    public function delete()
    {
        $data = \Request::all();
        $customer_id = (Auth::user()) ? Auth::user()->customer->cus_id : null;

        if ((new CartService)->remove($customer_id, $data['id'])) {
            return 'success';
        }

        return 'error';
    }

    public function checkout()
    {
        $cus_id = (Auth::user()) ? Auth::user()->customer->cus_id : null;

        $country = CountryRepo::get_country_by_code('ID');
        $selectedship_id=null;

        if(\Request::isMethod('post'))
        {
            $input = \Request::all();

            if(\Request::has('shipping-address-radio'))
            {
                $selectedship_id = $input['shipping-address-radio'];

            }
            elseif(\Request::has('ship_name'))
            {
                if(array_key_exists("isdefault", $input)){
                    $input['isdefault'] = "1";
                }else{
                    $input['isdefault'] = "0";
                }

                $niceNames = array(
                    'phone' => 'phone number',
                    'address1' => 'Address 1',
                    'address2' => 'Address 2',
                    'zipcode' => 'Zip Code',
                    'subdistrict' => 'Sub District',
                    'ship_name' => 'Recipent Name',
                    );

                $v = Validator::make($input, [
                    'phone' => 'required|numeric',
                    'address1' => 'required|max:255',
                    'zipcode' => 'required|numeric',
                    'country' => 'required',
                    'state' => 'required',
                    'city' => 'required',
                    'subdistrict' => 'required',
                ], [
                    'numeric' => 'Incorrect :attribute',
                ])->setAttributeNames($niceNames)->validate();

                $newshipping = CustomerRepo::create_shipping_address($cus_id, $input);
                if (!$newshipping)
                {
                    return back()->withInput()->with('status', trans('localize.internal_server_error.title'));
                }
                $selectedship_id = $newshipping->ship_id;
            }

            if (\Request::has('ship_id'))
            {
                $input = \Request::only('fail','ship_id','shipping_courier');

                if($input['fail'])
                    return view('front.cart.checkout_fail');

                if (\Request::has('shipping_courier'))
                {
                    foreach (\Request::get('shipping_courier') as $shipping_courier) {
                        if($shipping_courier=='0')
                            return back()->withInput()->with('errors', 'Please Select Courier');
                    }

                }

                $shipping = Shipping::where('ship_id',  $input['ship_id'])->first();
                // $this->updateShippingCharge($cus_id); // disable checking now in cartservice

                $data['name']=$shipping->ship_name;
                $data['address_1']=$shipping->ship_address1;
                $data['address_2']=$shipping->ship_address2;
                $data['city']=$shipping->ship_ci_id;
                $data['country']=$shipping->ship_country;
                $data['state']=$shipping->ship_state_id;
                $data['postal_code']=$shipping->ship_postalcode;
                $data['telephone']=$shipping->ship_phone;
                $data['sub_district']=$shipping->ship_subdistrict_id;

                $address=$data;
                $wholesale = request('wholesale') ? true : false;

                $checkout = (new CartService)->checkout($cus_id, $address, $wholesale)->getData();
                // $checkout=$this->getFakeSuccess()->getData();;

                if ($checkout) {
                    if (isset($checkout->status) && $checkout->status != 400) {
                        return view('front.cart.checkout_fail');
                    }

                    $parentOrder = ParentOrder::where('transaction_id', $checkout->data->transaction_id)->first();

                    $snaptoken = (new MidtransService)->get_snapToken($parentOrder);
                    if($snaptoken)
                    {
                        return view('front.cart.payment', ['token' => $snaptoken, 'transaction_id'=>$parentOrder->transaction_id]);
                    }
                    else{
                        return view('front.cart.checkout_fail');
                    }

                    // return view('front.cart.checkout_success', ['name' => $data['name'], 'transaction_id' => $checkout->data->transaction_id, 'orders' => $orders,'shipping'=>$shipping, 'parent_order'=>$parentOrder]);
                }

                return view('front.cart.checkout_fail');
            }
        }



        $shippings = Shipping::where('ship_cus_id', '=' , $cus_id)->where('ship_order_id', '=', 0)->orderBy('isdefault','desc')->orderBy('created_at','desc')->get();

        if (count($shippings) < 1) {
            return back()->withInput()->with('errors', 'Please Add In Address In Profile First');
        }

        if(empty($selectedship_id))
        {
            $selectedship_id = $shippings[0]->ship_id;
            if (Cookie::has('cart_ship_id')) {
                $selectedship_id = Cookie::get('cart_ship_id');

                // If cookie ship_id does not match user shipping addresses
                if ($shippings->where('ship_id', $selectedship_id)->isEmpty()) {
                    $selectedship_id = $shippings[0]->ship_id;
                }
            }
        }

        Cookie::queue('cart_ship_id', $selectedship_id);

        if (request()->routeIs('wholesale-checkout')) {
            $carts = (new CartService)->listing($cus_id, null, true);
        } else {
            $carts = (new CartService)->listing($cus_id);
        }

        $courier_options = $this->getCouriersByStoreCartItem($carts, $shippings->where('ship_id',$selectedship_id )->first());

        return view('front.cart.checkout', compact('selectedship_id','shippings','country','courier_options'));
    }

    public function getCouriersByStoreCartItem($carts, $shipping)
    {
        $item_with_courier = array();
        foreach ($carts->groupBy('product.pro_sh_id') as $store => $items)
        {
            $weight = 0;
            foreach ($items as $key => $cart)
            {
                if(empty($cart->service_ids))
                    $weight+=  $cart->weight_per_qty * $cart->quantity;
            }
            $store = $cart->product->store;
            $param['origin'] = !$store->stor_subdistrict?$store->stor_city:$store->stor_subdistrict;
            $param['originType'] = !$store->stor_subdistrict?'city':'subdistrict';
            $param['destination'] = !$shipping->ship_subdistrict_id?$shipping->ship_ci_id:$shipping->ship_subdistrict_id;
            $param['destinationType'] = !$shipping->ship_subdistrict_id?'city':'subdistrict';
            $param['weight'] =  $weight;
            $param['courier'] = 'jne:pos:tiki';
            $result = CourierRepo::get_couriers_fee($param);
            $item= array(
                'store_id' =>$cart->product->pro_sh_id,
                'weight'=>$param['weight'],
                'options'=>$result
            );
            array_push($item_with_courier, $item);

        }

        return $item_with_courier;
    }

    public function updateShippingCharge($cus_id)
    {
         $carts = Cart::where('cus_id', $cus_id)
        ->withAndWhereHas('product', function ($query) {
            $query->where('pro_status', 1)->orderBy('pro_sh_id');
        })
        ->withAndWhereHas('product.store', function ($query) {
            $query->where('stor_status', 1);
        })->get();
        //$carts = (new CartService)->listing($cus_id);

        $item_with_courier = array();
        foreach ($carts as $cart)
        {
            if(!empty($cart->service_ids))
                continue;
            $store = $cart->product->store;
            $shipping = Shipping::where('ship_id', $cart->ship_id)->first();
            $param['origin'] = !$store->stor_subdistrict?$store->stor_city:$store->stor_subdistrict;
            $param['originType'] = !$store->stor_subdistrict?'city':'subdistrict';
            $param['destination'] = !$shipping->ship_subdistrict_id?$shipping->ship_ci_id:$shipping->ship_subdistrict_id;
            $param['destinationType'] = !$shipping->ship_subdistrict_id?'city':'subdistrict';
            $param['weight'] =  $cart->weight_per_qty;
            $param['courier'] = $cart->ship_code;
            $result = CourierRepo::get_couriers_fee($param);

            foreach($result[0]->costs as $cost)
            {
                if($cart->ship_service==$cost->service)
                    $cart->shipping_fee = $cost->cost[0]->value;
            }
            $cart->Save();
        }

        return;
    }

    public function getFakeSuccess()
    {
        //take out later;
        $orders = array();
        $order["product_type"]= "1";
        $order["product_name"]= "OMP Velotica 350MM Black Leather Steering Wheel";
        $order["quantity"]= "1";
        $order["order_vc"]= "0.0450";
        $order["payment_method"]= "1";
        $order["order_amt"]= "600.00";
        $order["currency_symbol"]= "Rp";
        $order["json_attributes"]= '{"Color":"DS Diamond & Silk"}';
        $order["serial_number"]= null;
        $order["ship_name"]= "kean yeoh";
        $order["ship_address1"]= "27-2 jalan 1/137b";
        $order["ship_address2"]= "resource industrial";
        $order["ship_city"]= "17";
        $order["ship_country"]= "Indonesia";
        $order["ship_state"]= "Bali";
        $order["ship_postalcode"]= "58000";
        $order["ship_phone"]= "129910101";
        $order["ship_subdistrict"]= "Abiansemal";
        array_push($orders, $order);
        return apiResponse(400, [
            'data' => [
                'transaction_id' => 'akSa9GVt',
                'orders' => $orders
            ]
        ]);
    }

    public function nego()
    {
        $data = Request::only('nego_qty','nego_price','nego_product_id','nego_pricing_id', 'wholesale');

        $rules = [
            'nego_qty' => 'required|integer|min:1',
            'nego_price' => 'required|integer',
            'nego_product_id' => 'required|exists:nm_product,pro_id',
            'nego_pricing_id' => 'required|exists:nm_product_pricing,id',
        ];

        $messages = [];

        $fields = [
            'nego_qty' => trans('localize.nego_qty'),
            'nego_price' => trans('localize.nego_price'),
        ];

        $v = Validator::make($data, $rules, $messages, $fields);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        $this->negotiation = new NegotiationRepo();
        $this->product = new ProductRepo();

        $customer = Auth::user()->customer;
        extract($data);

        $new_nego = $this->negotiation->openNewNegotiation($customer->cus_id, $nego_qty, $nego_price, $nego_product_id, $nego_pricing_id, $wholesale ? $wholesale : 0);

        $new_nego->update([
            'status' => PriceNegotiation::STATUS['ACTIVE'],
        ]);

        (new Notifier($new_nego))->send(Notifier::PRICE_NEGOTIATION_RECEIVED);

        return back()->with('success', trans('localize.nego_msg.success.initiated', ['quantity' => $nego_qty, 'price' => $nego_price]));
    }
}
