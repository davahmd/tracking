<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;

use App\Models\News;

class NewsController extends Controller
{
    public function list()
    {
        $hide_load_more_button = false;
        $news = News::loadMore();

        if ($news->count() < 10) {
            $hide_load_more_button = true;
        }

        return view('front.news.list', compact('news', 'hide_load_more_button'));
    }

    public function detail(News $news)
    {
        return view('front.news.detail', compact('news'));
    }

    public function ajaxLoadMore()
    {
        $count = request('count');

        return News::loadMore($count)->toJson();
    }
}