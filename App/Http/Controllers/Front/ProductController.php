<?php
namespace App\Http\Controllers\Front;

use App\Models\PriceNegotiation;
use Illuminate\Support\Facades\Input as Input;

use App\Http\Controllers\Front\Controller;

use App\Repositories\ProductRepo;
use App\Repositories\OfficialBrandsRepo;
use App\Repositories\OrderRepo;
use App\Repositories\AttributeRepo;
use App\Repositories\FilterRepo;
use App\Repositories\CategoryRepo;
use App\Repositories\StoreRepo;
use App\Repositories\VehicleRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\PromotionRepo;
use App\Repositories\NegotiationRepo;
use App\Models\Country;
use App\Models\StoreServiceExtra;
use App\Models\Promotion;
use App\Models\PromotionProduct;

use Auth;
use Cookie;
use Request;
use Hashids\Hashids;
use Helper;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function  __construct(CategoryRepo $categoryrepo, OfficialBrandsRepo $officialbrandsrepo, ProductRepo $productrepo, StoreRepo $storerepo, VehicleRepo $vehiclerepo, PromotionRepo $promotionrepo, NegotiationRepo $negotiationrepo)
    {
        $this->category = $categoryrepo;
        $this->brand = $officialbrandsrepo;
        $this->product = $productrepo;
        $this->store = $storerepo;
        $this->vehicle = $vehiclerepo;
        $this->promotion = $promotionrepo;
        $this->nego = $negotiationrepo;
    }

    public function all()
    {
        $filterParams = (Request::get('options')) ? explode('-', base64_decode(Request::get('options'))) : '';
        $rangeParams = (Request::get('range')) ? explode('-', Request::get('range')) : '';
        $itemParams = (Request::get('items')) ? Request::get('items') : '15';
        $sortParams = (Request::get('sort')) ? Request::get('sort') : 'new';

        $products = ProductRepo::all_active($filterParams, $rangeParams, $itemParams, $sortParams, null);
        $filters_array = FilterRepo::get_category_filter(0);
        $products->appends(Input::except('page'));
        $category = trans('localize.all_products');
        $co_cursymbol = Country::where('co_id','=',\Cookie::get('country_locale'))->value('co_cursymbol');

        return view('front.product.index', compact('products','filterParams','rangeParams', 'itemParams', 'sortParams','co_cursymbol','filters_array', 'category'));
    }

    // Original Version of searching by category ID instead of url_slug.
    /*public function category($id)
    {
        $filterParams = (\Request::get('options')) ? explode('-', base64_decode(\Request::get('options'))) : '';
        $rangeParams = (\Request::get('range')) ? explode('-', \Request::get('range')) : '';
        $itemParams = (\Request::get('items')) ? \Request::get('items') : '15';
        $sortParams = (\Request::get('sort')) ? \Request::get('sort') : 'new';

        $catid = base64_decode($id);

        if(is_numeric($catid)){
            $category = CategoryRepo::get_category_by_id($catid)->name;
        }else{
            $category = CategoryRepo::get_category_by_slug($id)->name;
        }

        $products = ProductRepo::all_by_category($catid, $filterParams, $rangeParams, $itemParams, $sortParams, null);
        $products->appends(Input::except('page'));
        $filters_array = FilterRepo::get_category_filter($catid);
        $co_cursymbol = Country::where('co_id','=',\Cookie::get('country_locale'))->value('co_cursymbol');
        return view('front.product.list', compact('products', 'filterParams', 'rangeParams', 'itemParams', 'sortParams','co_cursymbol','filters_array', 'category'));
    }*/

    public function category($parent, $child = null)
    {
        $filterParams = (Request::get('options')) ? explode('-', base64_decode(Request::get('options'))) : '';
        $rangeParams = (Request::get('range')) ? explode('-', Request::get('range')) : '';
        $itemParams = (Request::get('items')) ? Request::get('items') : '15';
        $sortParams = (Request::get('sort')) ? Request::get('sort') : 'new';

        // Purpose of decoding due to foreseen the situation where url_slug has not been set,
        // it will take the encoded ID from the original version of searching which is
        // decoding the encoded ID from frontend.

        $parent_id = base64_decode($parent);

        if(is_numeric($parent_id)){
            $category = CategoryRepo::get_category_by_id($parent_id)->name;
        }else{
            $parent_id = CategoryRepo::get_category_by_slug($parent)->id;
            $category = CategoryRepo::get_category_by_slug($parent)->name;
        }

        if($child){
            $child_id = base64_decode($child);

            if(is_numeric($child_id)){
                $category = CategoryRepo::get_category_by_id($child_id)->name;
            }else{
                $child_id = CategoryRepo::get_category_by_slug($child)->id;
                $category = CategoryRepo::get_category_by_slug($child)->name;
            }
        }

        $products = ProductRepo::all_by_category(($child)?$child_id:$parent_id, $filterParams, $rangeParams, $itemParams, $sortParams, null);
        $products->appends(Input::except('page'));
        $filters_array = FilterRepo::get_category_filter(($child)?$child_id:$parent_id);
        $co_cursymbol = Country::where('co_id','=',\Cookie::get('country_locale'))->value('co_cursymbol');
        return view('front.product.list', compact('products', 'filterParams', 'rangeParams', 'itemParams', 'sortParams','co_cursymbol','filters_array', 'category'));
    }

    public function detail($id)
    {
        $platform_fees = round(\Config::get('settings.platform_charge'));
        $product = ProductRepo::detail($id);

        $countryid = \Cookie::get('country_locale')==null?\Session::get('countryid'):\Cookie::get('country_locale');
        $attribute_listing = AttributeRepo::get_product_attribute_listing_by_pro_id($id, $countryid);

        if (!$product || !$product['pricing'])
            return redirect('/products');

        $product['pricing']->price = round($product['pricing']->price, 2);

        if (Cookie::get('cart_token') == null) {
            $cart_token = md5(uniqid(microtime()));
            Cookie::queue(Cookie::forever('cart_token', $cart_token));
        } else {
            $cart_token = Cookie::get('cart_token');
        }

        $user_id = (Auth::user()) ? Auth::user()->customer->cus_id : null;
        $tot_in_cart = OrderRepo::get_quantity_of_product($cart_token, $user_id, $id);

        return view('front.product.detail', ['product' => $product,'total_in_cart' => $tot_in_cart, 'attribute_listing' => $attribute_listing]);
    }

    public function getCategoryView($url_slug)
    {
        $data = Request::only('category','product', 'sort');

        $category = CategoryRepo::get_category_by_slug($url_slug);

        $vehicles = [];

        if (Auth::check()) {
            $cus_id = Auth::user()->customer->cus_id;
            $vehicles = CustomerRepo::get_vehicles_by_customer_id($cus_id);
        }

        $official_brands = $this->brand->get_official_brands();
        $all_car_brands = VehicleRepo::get_brands();

        $products = ProductRepo::getProductByFilter($category->id, $data['sort'], null, null, null, null, null, null, null, null, 0, 12, null);

        // $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();
        $today_datetime = Carbon::now()->toDateTimeString();
        $car_brands = [];
        $product_brands = [];
        $promo_products = [];
        $promo_product_info = [];

        foreach ($products as $product) {
            foreach ($product->vehicles as $vehicle) {
                $car_brands[] = VehicleRepo::getBrand($vehicle->brand);
            }

            if ($product->brand_id != null) {
                $product_brands[] = OfficialBrandsRepo::get_official_brands_details($product->brand_id);
            }

            $product['pro_slug'] = Helper::slug_maker($product->pro_title_en, $product->pro_id);
        }

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_product_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }

        $car_brands = array_unique($car_brands);
        $product_brands = array_unique($product_brands);

        return view('front.product.listing', compact('category', 'vehicles', 'product_brands', 'car_brands', 'all_car_brands', 'products', 'data', 'promo_products', 'promo_product_info', 'today_datetime'));
    }

    public function getProductView($url_slug)
    {
        $product_id = Helper::slug_dicipherer($url_slug);

        if (!$product_id) {
            return view('errors.404');
        }

        if (\Request::is('product/preview/*')) {
            $product = $this->product->detail($product_id, null, true);
        }
        else {
            $product = $this->product->detail($product_id);
        }

        $countryid = \Cookie::get('country_locale')==null?\Session::get('countryid'):\Cookie::get('country_locale');

        $attribute_listing = AttributeRepo::get_product_attribute_listing_by_pro_id($product_id, $countryid);
        if (!$product || !$product['pricing'])
            return redirect('/products');

        $product['pricing']->price = round($product['pricing']->price, 2);

        if (Cookie::get('cart_token') == null) {
            $cart_token = md5(uniqid(microtime()));
            Cookie::queue(Cookie::forever('cart_token', $cart_token));
        } else {
            $cart_token = Cookie::get('cart_token');
        }

        $user_id = (Auth::user()) ? Auth::user()->customer->cus_id : null;
        $total_in_cart = OrderRepo::get_quantity_of_product($cart_token, $user_id, $product_id);

        $attributes = $this->product->attributes($product_id);

        $store = $this->store->get_store_by_id($product['details']['pro_sh_id']);
        $store_service_extra = StoreServiceExtra::where('pro_id', $product_id)->get();

        // dump($product['details']->pro_id);
        // $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();
        $today_datetime = Carbon::now()->toDatetimeString();
        $promo_product = null;
        $promo_product_info = null;
        $time_remaining = null;
        $end = null;

        $promo_item = $this->promotion->new_pricing_by_promotion($product_id[0]);
        if ($promo_item) {
            $promo_product = $promo_item;
            $promo_product_info = PromotionProduct::where('promotion_id', $promo_product->id)->where('product_id', $product_id)->first();
            $ended_at = $promo_product->ended_at;
            // $start  = new Carbon(Carbon::parse(Helper::UTCtoTZ(Carbon::now())));
            $start  = new Carbon(Carbon::now());
            $end    = new Carbon($ended_at);
            $time_remaining = $start->diffInHours($end) . ' hours : ' . $start->diff($end)->format('%I minutes : %S seconds');
        }

        // Checking existing negotiation
        $existing_nego = null;

        if (Auth::check()) {
            $existing_nego = $this->nego->getExistingNegotiation($user_id, $product_id, $product['pricing']->id);
        }

        if (\Request::is('product/preview/*')) {
            return view('front.product.preview', compact('product', 'total_in_cart', 'attribute_listing', 'store', 'store_service_extra', 'promo_product', 'promo_product_info', 'today_datetime', 'time_remaining'));
        }

        $product['details']->load(['negotiations' => function ($query) {
            $query->where('customer_id', Auth::check() ? auth()->user()->customer->cus_id : null)
                ->where('expired_at', '>', Carbon::now()->toDateTimeString())
                ->where('status', PriceNegotiation::STATUS['ACTIVE']);
        }]);

        // dd($product);

        $ratings = $product['details']->ratings()->available()->sortBy('latest')->paginate(5);

        $active_nego = null;

        if (Auth::check()) {
            $active_nego = auth()->user()->customer->negotiation()
            ->where('product_id', $product['details']->pro_id)
            ->where('pricing_id', $product['pricing']->id)
            ->active()
            ->latest()
            ->first();
        }

        return view('front.product.details', compact('product', 'total_in_cart', 'attribute_listing', 'store','store_service_extra', 'promo_product', 'promo_product_info', 'today_datetime', 'time_remaining', 'ratings', 'end','existing_nego', 'active_nego'));
    }
}
