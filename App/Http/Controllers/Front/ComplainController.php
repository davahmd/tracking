<?php

namespace App\Http\Controllers\Front;

use App\Models\Order;
use App\Models\Complain;
use Illuminate\Http\Request;
use App\Models\ComplainMessage;
use App\Models\ComplainSubject;

class ComplainController extends Controller
{
    private $complain = null;
    private $complainSubject = null;
    private $complainMessage = null;

    /**
     * ComplainController constructor. Dependency Injection.
     *
     * @param Complain $complain
     * @param ComplainSubject $complainSubject
     * @param ComplainMessage $complainMessage
     */
    public function __construct(Complain $complain, ComplainSubject $complainSubject, ComplainMessage $complainMessage)
    {
        $this->complain = $complain;
        $this->complainSubject = $complainSubject;
        $this->complainMessage = $complainMessage;
    }

    /**
     * Show complain form.
     *
     * @param Request $request
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createComplain(Request $request, Order $order)
    {
        try {
            if ($this->complain->isComplainExist($order)) {
                $complain = $order->getFirstComplain()->first();

                if ($complain) {
                    return redirect()->route('complain::complain-message', [$order, $complain]);
                }
            }

            $orders = $order->items;
            $subjects = $this->complainSubject->all();

        } catch (\Throwable $e) {
            return redirect()->back()->with('error', trans('localize.internal_server_error.msg'));
        }

        return view('front.account.order.complain.create_complain', compact('order', 'subjects'));
    }

    /**
     * Create complain model.
     *
     * @param Request $request
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreateComplain(Request $request, Order $order)
    {
        $this->validate($request, [
            'subject_id' => 'required|exists:complain_subjects,id',
            'message' => 'required',
//            'nm_order_id' => 'nullable|exists:nm_order,order_id',
            'attachments.*' => 'nullable|mimes:jpg,jpeg,png|max:2000',
        ]);

        if (request()->hasFile('attachments')) {
            $attachments = request()->file('attachments');

            if (count($attachments) > Complain::MAX_UPLOAD_LIMIT) {
                $request->flash();

//                return apiResponse(422, trans('localize.complain.validation.exceed_limit_attachments', [
//                    'limit' => Complain::MAX_UPLOAD_LIMIT
//                ]));
                return back()->with('error', trans('localize.complain.validation.exceed_limit_attachments', [
                    'limit' => Complain::MAX_UPLOAD_LIMIT
                ]));
            }
        }

        try {
            $complainant = auth()->user()->customer;

            $complain = $this->complain->addNewComplainByComplainant(
                $complainant,
                $order,
                $request->except('_token')
            );

            if (request()->hasFile('attachments')) {
                $complain->uploadAttachments($complain, $attachments);
            }

        } catch (\Throwable $e) {
            return redirect()->back()->with('error', trans('localize.internal_server_error.msg'));
        }

        return redirect()->route('complain::complain-message', [$order, $complain])->with('success', trans('localize.complain.create.new_complain_success'));
    }

    /**
     * Show complain conversations.
     *
     * @param Request $request
     * @param Order $order
     * @param Complain $complain
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function complainMessage(Request $request, Order $order, Complain $complain)
    {
        $messages = $complain->getLatestMessages($complain, 10);

        if ($request->ajax()) {
            $view = view('shared.complain.chats', compact('order', 'complain', 'messages'))->render();

            return json_encode([
                'paginator' => $messages,
                'view' => $view
            ]);
        }

        $isAdmin = false;

        return view('front.account.order.complain.complain_message', compact('order', 'complain', 'messages', 'isAdmin'));
    }

    /**
     * Post create new complain message.
     *
     * @param Request $request
     * @param Order $order
     * @param Complain $complain
     * @return false|string
     * @throws \Throwable
     */
    public function postComplainMessage(Request $request, Order $order, Complain $complain)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        $customer = auth()->user()->customer;

        $attributes = $request->all();

        $attributes['is_creator'] = true;

        try {
            $complainMessage = null;

            \DB::transaction(function() use ($complain, $customer, $attributes, &$complainMessage) {
                $complainMessage = $complain->addNewComplainMessage($complain, $customer, $attributes);
            });

        } catch (\Throwable $e) {
            return json_encode([
                'status' => 'failed',
                'message' => trans('localize.internal_server_error.msg')
            ]);
        }

        $view = view('shared.complain.chat', [
            'message' => $complainMessage
        ])->render();

        return json_encode([
            'view' => $view
        ]);
    }

    /**
     * Escalated to admin.
     *
     * @param Order $order
     * @param Complain $complain
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function postEscalate(Order $order, Complain $complain)
    {
        if ($complain->is_escalated) {
            return back()->with('error', trans('localize.complain.validation.already_escalated'));
        }

        try {
            $complain->toggleEscalate();

        } catch (\Exception $e) {
            return back()->with('error', trans('localize.internal_server_error.msg'));
        }

        return redirect()->route('complain::complain-message', [$order, $complain])->with('success', trans('localize.complain.global.success_escalated'));
    }
}