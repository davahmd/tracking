<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;

use App\Repositories\ProductRepo;
use App\Repositories\OfficialBrandsRepo;
use App\Repositories\PromotionRepo;

use App\Models\Promotion;
use App\Models\PromotionProduct;

use Carbon\Carbon;

use Helper;

class OfficialBrandController extends Controller
{
    public function __construct(ProductRepo $productrepo, OfficialBrandsRepo $officialbrandsrepo, PromotionRepo $promotionrepo)
    {
        $this->product = $productrepo;
        $this->officialbrands = $officialbrandsrepo;
        $this->promotion = $promotionrepo;
    }

    public function view($brand_slug)
    {
        $brand = OfficialBrandsRepo::get_brand_by_slug($brand_slug);

        $latest_products = ProductRepo::getProducts('latest', 6, false, $brand->brand_id);
        $trending_products = ProductRepo::getProducts('trending', 6, false, $brand->brand_id);

        $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();
        $promo_latest_products = [];
        $promo_trending_products = [];
        $promo_item_info = [];

        foreach ($latest_products as $key => $latest_product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($latest_product->pro_id);
            if ($promo_item) {
                $promo_latest_products[$latest_product->pro_id] = $promo_item;
                $promo_item_info[$latest_product->pro_id] = PromotionProduct::where('promotion_id', $promo_latest_products[$latest_product->pro_id]->id)->where('product_id', $latest_product->pro_id)->first();
            }
        }

        foreach ($trending_products as $key => $trending_product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($trending_product->pro_id);
            if ($promo_item) {
                $promo_trending_products[$trending_product->pro_id] = $promo_item;
                $promo_item_info[$trending_product->pro_id] = PromotionProduct::where('promotion_id', $promo_trending_products[$trending_product->pro_id]->id)->where('product_id', $trending_product->pro_id)->first();
            }
        }

        return view('front.officialbrand.view', compact('brand', 'latest_products','trending_products', 'promo_latest_products', 'promo_trending_products', 'promo_item_info', 'today_datetime'));
    }

    public function latestProducts($brand_slug)
    {
    	$brand = OfficialBrandsRepo::get_brand_by_slug($brand_slug);
        $products = ProductRepo::getProducts('latest', 6, false, $brand->brand_id);
        $type='latest';

        $promo_products = [];
        $promo_item_info = [];
        $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_item_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }

    	return view('front.officialbrand.listing', compact('products','type','brand_slug', 'promo_products', 'promo_item_info', 'today_datetime'));
    }

    public function MostPopular($brand_slug)
    {
    	$brand = OfficialBrandsRepo::get_brand_by_slug($brand_slug);
        $products = ProductRepo::getProducts('trending', 24, false, $brand->brand_id);
        $type='trending';

        $promo_products = [];
        $promo_item_info = [];
        $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();
        // $today_datetime = Carbon::now()->addHours(8)->toDateTimeString();

        foreach ($products as $key => $product) {
            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            if ($promo_item) {
                $promo_products[$product->pro_id] = $promo_item;
                $promo_item_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
            }
        }

    	return view('front.officialbrand.listing', compact('products','type','brand_slug', 'promo_products', 'promo_item_info', 'today_datetime'));
    }
}
