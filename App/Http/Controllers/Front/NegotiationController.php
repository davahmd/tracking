<?php

namespace App\Http\Controllers\Front;

use App\Models\PriceNegotiation;
use App\Services\Cart\CartService;
use App\Services\Notifier\Notifier;
use Carbon\Carbon;

class NegotiationController extends Controller
{
    public function view(PriceNegotiation $negotiation)
    {
        return view('front.account.general.negotiation.view', compact('negotiation'));
    }

    public function accept(PriceNegotiation $negotiation)
    {
        $negotiation->acceptByCustomer();

        (new CartService)->add(
            $negotiation->customer_id,
            $negotiation->product_id,
            $negotiation->pricing_id,
            $negotiation->quantity,
            null,
            $negotiation->id,
            $negotiation->wholesale
        );

        (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_ACCEPTED);

        return back();
    }

    public function reject(PriceNegotiation $negotiation)
    {
        $negotiation->rejectByCustomer();

        (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_REJECTED);

        return back();
    }

    public function counterOffer(PriceNegotiation $negotiation)
    {
        $this->validate(request(), [
            'price' => 'required|integer'
        ]);
        $newNegotiation = $negotiation->replicate([
            'merchant_offer_price'
        ]);

        $newNegotiation->session_id = $negotiation->id;
        $newNegotiation->customer_offer_price = request('price');
        $newNegotiation->updated_at = Carbon::now();
        $newNegotiation->created_at = Carbon::now();
        $newNegotiation->save();

        $negotiation->update(['status' => PriceNegotiation::STATUS['COUNTER_OFFER']]);

        (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_COUNTER_OFFER);

        return redirect()->route('nego-detail', $newNegotiation);
    }
}