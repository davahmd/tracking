<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;
use Illuminate\Support\Facades\Session;
use Veritrans;
use App\Services\Midtrans\MidtransService;
use Illuminate\Http\Request;

class MDPaymentController extends Controller
{
    public function notification(Request $request)
    {
        $hashvalue = hash('sha512', $request->order_id.$request->status_code.$request->gross_amount.config('midtrans.server_key'));
        if($request->signature_key != $hashvalue)
        {
            return response()->json(false, 500);
        }
        $response = $this->transactionStatus($request->order_id);

        return response()->json(json_decode($response->getContent())->message, json_decode($response->getContent())->status_code);

        // if(!json_decode($response->getContent())->status)
        // {
        //     return response()->json(false, 500);
        // }
        // else
        // {
        //     return response()->json(true, 200);
        // }

    }

    public function transactionStatus($parent_order_id)
    {
        $data =  (new MidtransService)->status($parent_order_id);
        if(!$data)
        {
            return response()->json([
                'status' => false,
                'status_code' => 404,
                'message' => "Midtrans can't find order"
            ]);
        }

        $result = (new MidtransService)->process_payment($data);

        return $result;

    }
}