<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\PriceNegotiation;
use App\Repositories\NegotiationRepo;

class NegotiationController extends MobileApiController
{
	public function createNegotiation()
	{
		$this->validate(request(), [
			'nego_qty' => 'required|integer',
			'nego_price' => 'required|integer',
            'product_id' => 'required|integer',
            'pricing_id' => 'required|integer',
            'wholesale' => 'required|integer',
        ]);

        $customer = auth('api')->user()->customer;

        try {
			$negotiation = NegotiationRepo::openNewNegotiation($customer->cus_id, request('nego_qty'), request('nego_price'), request('product_id'), request('pricing_id'), request('wholesale'));
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.Failed'),
			]);
		}

		$negotiation->update([
            'status' => PriceNegotiation::STATUS['ACTIVE'],
        ]);

		return apiResponse(200, [
			'message' => trans('localize.Success'),
		]);
	}

	public function updateNegotiation()
	{
		$this->validate(request(), [
			'nego_id' => 'required|integer',
			'nego_price' => 'integer',
			'status' => 'required|integer',
		]);

		$customer = auth('api')->user()->customer;

		try {
			$negotiation = NegotiationRepo::memberUpdateNegotiation(request('nego_id'), $customer->cus_id, request('status'), request('nego_price'));
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.Failed'),
			]);
		}

		return apiResponse(200, [
			'message' => trans('localize.Success'),
			'data' => [
				'negotiation_id' => $negotiation->id,
			]
		]);
	}

	public function negotiationListing()
	{
		$this->validate(request(), [
            'page' => 'nullable|integer|required',
            'per_page' => 'nullable|integer|min:1|required',
            'product_id' => 'nullable|integer',
        ]);

		$customer = auth('api')->user()->customer;

		try {
			$negotiations = NegotiationRepo::memberNegotiationListing($customer->cus_id, request('per_page'), request('page'), request('product_id'));
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.Failed'),
			]);
		}

		return apiResponse(200, [
			'count' => $negotiations->total(),
			'total_page' => $negotiations->lastPage(),
            'data' => $negotiations->apiNegotiationListingData(),
        ]);
	}

	public function negotiationDetail()
	{
		$this->validate(request(), [
			'negotiation_id' => 'integer|required',
		]);

		$customer = auth('api')->user()->customer;

		try {
			$negotiation = NegotiationRepo::memberNegotiationDetail($customer->cus_id, request('negotiation_id'));
			$history = NegotiationRepo::memberNegotiationHistory($customer->cus_id, request('negotiation_id'));
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.Failed'),
			]);
		}

		return apiResponse(200, [
			'data' => [
				'id' => $negotiation->id,
				'session_id' => $negotiation->session_id,
				'product_name' => $negotiation->product->title,
				'quantity' => $negotiation->quantity,
				'image' => $negotiation->product->mainImageUrl(),
				'pricing' => [
					'id' => $negotiation->pricing->id,
					'currency_code' => config('app.currency_code'),
					'price' => $negotiation->pricing->getPurchasePrice(),
					'attributes' => $negotiation->pricing->convertProductAttributes(),
				],
				'status' => $negotiation->status,
				'negotiation' => $history->apiNegotiationHistoryData(),
			]
		]);
	}
}