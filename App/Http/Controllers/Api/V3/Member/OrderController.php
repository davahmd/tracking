<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;

use App\Models\Order;
use App\Models\ParentOrder;
use App\Models\MemberServiceSchedule;

use App\Services\Notifier\Notifier;
use App\Traits\OrderOnlineLogger;
use App\Repositories\OrderRepo;
use App\Services\Rating\RatingService;

class OrderController extends MobileApiController
{
    public function __construct(OrderRepo $orderrepo)
    {
        $this->order = $orderrepo;
    }

    public function orderHistory()
    {
        $this->validate(request(),[
            'transaction_id' => 'exists:nm_order,transaction_id',
            'wholesale' => 'nullable|in:0,1'
        ]);

        $cusId = auth('api')->user()->customer->cus_id;
        $orders = ParentOrder::where('customer_id', $cusId)->orderBy('date', 'desc')->get();

        if (request('transaction_id')) {
            $orders = ParentOrder::with('items', 'complainables')->where('customer_id', $cusId)->whereHas('items', function($q){
                $q->where('transaction_id', request('transaction_id'));
            })->get();
        }

        return apiResponse(200, [
            'data' => $orders->apiOrderHistoryData(),
        ]);
    }

    public function orderDetails()
    {
        $this->validate(request(),[
            'order_id' => 'required|exists:nm_order,order_id',
        ]);

        $order = Order::where('order_id', request('order_id'))->get();

        return apiResponse(200, [
            'data' => $order->apiData(),
        ]);
    }

    public function orderReview()
    {
        $this->validate(request(),[
            'order_id' => 'required|exists:nm_order,order_id',
            'reference' => 'required|string|in:product,store',
            'rating' => 'required|integer|between:1,5',
            'review' => 'max:255',
            'images' => 'nullable|array',
            'images.*' => 'mimes:jpg,jpeg,png|max:2000',
        ]);

        return (new RatingService)->addReview(request('order_id'), auth('api')->user()->customer->cus_id, request()->all());
    }

    public function update_order_status()
    {
        $this->validate(request(),[
            'order_id' => 'required',
            'status' => 'required|integer|in:1,2,3,4,5,6,7'
        ]);

        $cus_id = auth('api')->user()->customer->cus_id;
        $order = OrderRepo::get_order_by_id(request('order_id'));

        if (empty($order) || $order->order_status != 3 || $order->order_cus_id != $cus_id)
            return apiResponse(404, [
                'message' => ["Order Id not found"],
            ]);

        $remarks = null;
        if($order->product_shipping_fees_type == 3)
            $remarks = 'Self pickup updated by customer';

        if ($order->order_status == Order::STATUS_INSTALLATION) {

            $service_uncompleted_status = MemberServiceSchedule::where('order_id', '=', $order_id)->whereIn('status', array(0,1,2,3,-1))->exists();
            if ($service_uncompleted_status)
                return apiResponse(404, [
                    'message' => [trans('front.order.error.double_confirm_service_completed')],
                ]);
        }
        
        $result = OrderRepo::completing_merchant_order(request('order_id'), $remarks);

        if(!$result) {
            return apiResponse(500, [
                'message' => [trans('localize.unable_update_status')],
            ]);
        }

        $total_price = $order->total_product_price; // get total product price * quantity
        $earning = $total_price - $order->merchant_charge_value; // get merchant earning

        $data = array(
            // 'type'=>'gamepoint',
            'merchant_firstname' => $order->mer_fname,
            'merchant_lastname' => $order->mer_lname,
            'product_title' => $order->pro_title_en,
            'order_qty' => $order->order_qty,
            'order_value' => $order->order_value,
            'transaction_id' => $order->transaction_id,
            'order_commission' => $order->merchant_charge_percentage,
            'merchant_earning' => number_format($earning, 2),
            'currency' => $order->currency
        );

        (new Notifier($order))->send(Notifier::ORDER_COMPLETED);

        OrderOnlineLogger::log(null, $order->order_id, null, 'customer', 'Complete order');
        
        return apiResponse(200, [
            'message' => [trans('localize.update_status_successfully')],
        ]);
    }
}