<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Category;
use App\Models\Merchant;
use App\Models\OfficialBrands;
use App\Models\Product;
use App\Models\ProductPricing;
use App\Models\Promotion;
use App\Models\PromotionProduct;
use App\Models\Vehicle;
use App\Repositories\AttributeRepo;
use App\Repositories\PromotionRepo;

use Illuminate\Pagination\LengthAwarePaginator;

use Helper;
use Carbon\Carbon;

class ProductController extends MobileApiController
{
    public function index()
    {
        $rules = [
            'category_id' => 'nullable|integer',
            'price_from' => 'nullable|numeric|min:0',
            'price_to' => 'nullable|numeric|min:' . request('price_from'),
            'name' => 'nullable',
            'merchant_id' => 'nullable|integer',
            'store_id' => 'nullable|integer',
            'condition' => 'nullable|integer|in:1,2,3',
            'brand_id' => 'nullable|integer',
            'delivery_from' => 'nullable',
            'featured' => 'nullable|integer|in:0,1',
            'flashsale' => 'nullable|integer|in:0,1',
            'car_id' => 'nullable|integer',
            'car_brand' => 'nullable',
            'wholesale' => 'nullable|in:0,1',
            'per_page' => 'nullable|integer|min:1',
            'sort_by' => 'nullable|in:latest,price,popular',
            'sort_order' => 'nullable',
        ];

        $this->validate(request(), $rules);

        $productQuery = Product::with('pricing')
            ->hasCategoryId(request('category_id'))
            ->has('pricing')
            ->hasPriceBetween(request('price_from'), request('price_to'), ProductPricing::STATUS_ACTIVE)
            ->nameContain(request('name'))
            ->merchantId(request('merchant_id'))
            ->storeId(request('store_id'))
            ->condition(request('condition'))
            ->brandId(request('brand_id'))
            ->deliveryFrom(request('delivery_from'))
            ->featured(request('featured'))
            ->hasCar(request('car_id'), request('car_brand'))
            ->provideInstallation(request('installation'))
            ->available()
            ->sortBy(request('sort_by'), request('sort_order'));

        if (request('wholesale') == 1) {
            $productQuery->hasWholeSalePrice();
        }

        $data = request()->all();

        if (request('flashsale') == '1') {

            $today_datetime = Carbon::now();

            $promotions = Promotion::where('status', 1)->where('promo_type', 'F')->where('started_at', '<=', $today_datetime)->where('ended_at', '>=', $today_datetime)->get();
            $promotion_items = PromotionProduct::whereColumn('limit', '>', 'count')->whereIn('promotion_id', $promotions->pluck('id'))->get();

            $promotion_items_info = [];
            $flash_products_info = [];

            foreach($promotion_items as $item) {

                $query = Product::where('pro_id', $item->product_id);

                if (isset($data['category_id']) && $data['category_id'] != 0) {

                    $query->whereHas('categories', function ($query) use ($data) {
                        $category =Category::find($data['category_id']);
                        $ids[] = $data['category_id'];

                        if ($category && $category->child_list) {
                            $ids = array_merge($ids, explode(',', $category->child_list));
                        }

                        $query->whereIn('category_id', $ids);
                    });
                }

                if (isset($data['price_from']) || isset($data['price_to'])) {
                    $query->whereHas('pricing', function($query) use ($data) {
                        if ($priceStart) {
                            $query->where('price', '>=', $priceStart);
                        }

                        if ($priceEnd) {
                            $query->where('price', '<=', $priceEnd);
                        }

                        if ($status) {
                            $query->where('status', ProductPricing::STATUS_ACTIVE);
                        }

                        $query->orderBy('price');
                    });
                }

                if (isset($data['name'])) {
                    $query->where('pro_title_en', 'LIKE', '%'.request('name').'%');
                }

                if (isset($data['merchant_id']) && $data['merchant_id'] != 0) {
                    $query->where('pro_mr_id', $data['merchant_id']);
                }

                if (isset($data['store_id']) && $data['store_id'] != 0) {
                    $query->where('pro_sh_id', $data['store_id']);
                }

                if ((isset($data['car_brand']) && $data['car_brand'] != 0) || (isset($data['car_id']) && $data['car_id'] != 0)) {
                    $query->whereHas('vehicles', function($query) use ($data) {
                        if ($data['car_id']) {
                            $query->where('vehicles.id', $data['car_id']);
                        }

                        if ($data['car_brand']) {
                            $query->where('vehicles.brand', $data['car_brand']);
                        }
                    });
                }

                if (isset($data['brand_id']) && $data['brand_id'] != 0) {
                    $query->where('brand_id', $data['brand_id']);
                }

                if (isset($data['delivery_from'])) {
                    $query->whereHas('store', function ($query) use ($data) {
                        $query->where('stor_city_name', $data['delivery_from']);
                    });
                }

                if (isset($data['condition']) && $data['condition'] != null) {
                    $query->whereIn('condition', $data['condition']);
                }

                if (isset($data['featured']) && $data['featured'] == '1') {
                    $query->where('featured', true);
                }

                if (isset($data['installation']) && $data['installation'] == '1') {
                    $query->where('installation', 1);
                }

                if (isset($data['sort_by']) && isset($data['sort_order'])) {

                    switch ($data['sort_by']) {
                        case 'latest':
                            $query->orderBy('created_at', $data['sort_order'] ?: 'desc');
                            break;
                        case 'popular':
                            $query->leftJoin('nm_order', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                                ->select('nm_product.*')
                                ->addSelect(DB::raw('count(nm_product.pro_id) as sort'))
                                ->orderBy('sort', $data['sort_order'] ?: 'desc')
                                ->groupBy('nm_product.pro_id');
                            break;
                        case 'price':
                            $query->leftJoin('nm_product_pricing', 'nm_product.pro_id', '=', 'nm_product_pricing.pro_id')
                                ->select('nm_product.*')
                                ->addSelect(DB::raw('min( case when now() between nm_product_pricing.discounted_from and nm_product_pricing.discounted_to then nm_product_pricing.discounted_price else price end) as sort'))
                                ->orderBy('sort', $data['sort_order'] ?: 'asc')
                                ->groupBy('nm_product.pro_id');
                            break;
                        default:
                    }
                }

                $product = $query->first();

                if ($product) {

                    $promo_item = PromotionRepo::new_pricing_by_promotion($product->pro_id);
                    $productPrice = $product->pricing->first();
                    $promo_product = $promo_item;
                    $promo_product_info = PromotionProduct::where('promotion_id', $promo_product->id)->where('product_id', $product->pro_id)->first();
                    $ended_at = $promo_product->ended_at;
                    $start  = new Carbon(Carbon::now());
                    $end    = new Carbon($ended_at);
                    // $time_remaining = $start->diffInHours($end) . ' hours : ' . $start->diff($end)->format('%I minutes : %S seconds');
                    $rating = stdObject($product->compiledRatings(true));

                    $time_remaining = $end->diffInSeconds($start);

                    $flash_product = [
                                    'id' => $product->pro_id,
                                    'name' => $product->title,
                                    'image' => $product->mainImageUrl(),
                                    'currency_symbol' => $productPrice->country->co_cursymbol,
                                    'display_price' => $promo_product->discount_rate ? number_format($product->pricing->first()->price *(1-$promo_product->discount_rate), 2, '.', '') : number_format($product->pricing->first()->price - $promo_product->discount_value, 2, '.', ''),
                                    'original_price' => $productPrice->price,
                                    'discount_rate' => $promo_product->discount_rate ? $promo_product->discount_rate*100 ."%" : rpFormat($promo_product->discount_value),
                                    'remaining_item' => $productPrice->quantity,
                                    'remaining_flash_item' => $promo_product_info->limit - $promo_product_info->count,
                                    'remaining_time_flash' => $time_remaining,
                                    'attributes' => json_decode($productPrice->attributes_name),
                                    'rating' => [
                                        'rate' => $rating->rate,
                                        'max_rate' => 5,
                                        'total' => $rating->total,
                                        'percentage' => $rating->percentage,
                                        'cumulative' => $rating->cumulative,
                                    ]
                    ];

                    array_push($flash_products_info, $flash_product);
                }
            }

            $productIds = null;

            if (!empty(request()->except(['sort_by', 'sort_order', 'page', 'per_page']))) {
                $productIds = collect($flash_products_info)->pluck('pro_id')->toArray();
            }

            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $itemCollection = collect($flash_products_info);
            $perPage = 20;
            $page = 1;

            if (request('per_page')) {
                $perPage = request('per_page');
            }

            if (request('page')) {
                $page = request('page');
            }

            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            $paginatedItems= new LengthAwarePaginator(array_values($currentPageItems) , count($itemCollection), $perPage, $page);

            if ($productIds) {
                $carBrands = Vehicle::hasProductId($productIds)->getBrandList();
                $officialBrands = OfficialBrands::hasProductId($productIds)->get();
                $categories = Category::hasProductId($productIds)
                    ->orderBy('name_en')
                    ->get();
            }
            else {
                $carBrands = Vehicle::hasActiveProducts()->getBrandList();
                $officialBrands = OfficialBrands::hasActiveProducts()->get();
                $categories = Category::hasActiveProducts()
                    ->orderBy('name_en')
                    ->get();
            }

            $addNew = true;

            while ($addNew) {
                $addNew = false;
                foreach ($categories as $cat) {
                    if ($cat->parent_list) {
                        $parentArray = explode(',', $cat->parent_list);

                        foreach ($parentArray as $id) {
                            if ($categories->where('id', $id)->isEmpty()) {
                                $categories->add(Category::find($id));
                                $addNew = true;
                            }
                        }
                    }
                }
            }

            return apiResponse(200, [
                'count' => $paginatedItems->total(),
                'total_page' => $paginatedItems->lastPage(),
                'data' => $paginatedItems->items(),
                'filters' => [
                    'car_brands' => $carBrands,
                    'product_brands' => $officialBrands->map(function ($officialBrand) {
                        return [
                            'id' => $officialBrand->brand_id,
                            'name' => $officialBrand->brand_name
                        ];
                    }),
                    'categories' => $categories->map(function ($category) {
                        return [
                            'id' => $category->id,
                            'name' => $category->name_en,
                        ];
                    })
                ]
            ]);
        }

        $productIds = null;

        if (!empty(request()->except(['sort_by', 'sort_order', 'page', 'per_page']))) {
            $productIds = $productQuery->pluck('pro_id')->toArray();
        }

        $products = $productQuery->paginate(request('per_page'));

        if ($productIds) {
            $carBrands = Vehicle::hasProductId($productIds)->getBrandList();
            $officialBrands = OfficialBrands::hasProductId($productIds)->get();
            $categories = Category::hasProductId($productIds)
                ->orderBy('name_en')
                ->get();
        }
        else {
            $carBrands = Vehicle::hasActiveProducts()->getBrandList();
            $officialBrands = OfficialBrands::hasActiveProducts()->get();
            $categories = Category::hasActiveProducts()
                ->orderBy('name_en')
                ->get();
        }

        $addNew = true;

        while ($addNew) {
            $addNew = false;
            foreach ($categories as $cat) {
                if ($cat->parent_list) {
                    $parentArray = explode(',', $cat->parent_list);

                    foreach ($parentArray as $id) {
                        if ($categories->where('id', $id)->isEmpty()) {
                            $categories->add(Category::find($id));
                            $addNew = true;
                        }
                    }
                }
            }
        }

        return apiResponse(200, [
            'count' => $products->total(),
            'total_page' => $products->lastPage(),
            'data' => $products->apiData(),
            'filters' => [
//                'car_brands' => Vehicle::has('products')->getBrandList(),
                'car_brands' => $carBrands,
                'product_brands' => $officialBrands->map(function ($officialBrand) {
                    return [
                        'id' => $officialBrand->brand_id,
                        'name' => $officialBrand->brand_name
                    ];
                }),
                'categories' => $categories->map(function ($category) {
                    return [
                        'id' => $category->id,
                        'name' => $category->name_en,
//                        'children' => $category->children
                    ];
                })
            ]
        ]);
    }

    public function detail()
    {

        $this->validate(request(), [
            'id' => 'required|integer',
            'pricing_id' => 'integer'
        ]);

        $product = $this->productValidation(request('id'), request('pricing_id'));
        $rating = stdObject($product->compiledRatings(true));

        $promo_item = PromotionRepo::new_pricing_by_promotion($product->pro_id);

        if ($promo_item) {

            $productPrice = $product->pricing->first();
            $promo_product = $promo_item;
            $promo_product_info = PromotionProduct::where('promotion_id', $promo_product->id)->where('product_id', $product->pro_id)->first();
            $ended_at = $promo_product->ended_at;
            $start  = new Carbon(Carbon::now());
            $end    = new Carbon($ended_at);
            $time_remaining = $start->diffInHours($end) . ' hours : ' . $start->diff($end)->format('%I minutes : %S seconds');

            // $option = [
            //     'id' => $productPrice->id,
            //     'currency_symbol' => $productPrice->country->co_cursymbol,
            //     'display_price' => $promo_product->discount_rate ? rpFormat($product->pricing->first()->price *(1-$promo_product->discount_rate)) : rpFormat($product->pricing->first()->price - $promo_product->discount_value),
            //     'original_price' => rpFormat($productPrice->price),
            //     'discount_rate' => $promo_product->discount_rate ? $promo_product->discount_rate*100 ."%" : rpFormat($promo_product->discount_value),
            //     'remaining_item' => $productPrice->quantity,
            //     'remaining_flash_item' => $promo_product_info->limit - $promo_product_info->count,
            //     'remaining_time_flash' => $time_remaining,
            //     'attributes' => json_decode($productPrice->attributes_name)
            // ];

            return apiResponse(200, [
                'data' => [
                    'product_id' => $product->pro_id,
                    'name' => $product->title,
                    'description' => $product->desc,
                    'images' => $product->images->sortBy('order')->map(function ($image) {
                        return $image->imagePath;
                    })->flatten(),
                    'options' => $product->pricing->apiData(),
                    'available_service' => $product->storeServiceExtra->map(function ($service) {
                        return [
                            'id' => $service->id,
                            'service_name' => ucfirst($service->service_name),
                            'price' => $service->service_price
                        ];
                    }),
                    'quantity_sold' => $product->soldOrders()->sum('order_qty'),
                    'share_link' => route('product', Helper::slug_maker($product->pro_title_en, $product->pro_id)),
                    'dsiplay_rating' => false,
                    'rating' => [
                        'rate' => $rating->rate,
                        'max_rate' => 5,
                        'total' => $rating->total,
                        'percentage' => $rating->percentage,
                        'cumulative' => $rating->cumulative,
                    ]
                ]
            ]);
        }

        return apiResponse(200, [
            'data' => [
                'product_id' => $product->pro_id,
                'name' => $product->title,
                'description' => $product->desc,
                'images' => $product->images->sortBy('order')->map(function ($image) {
                    return $image->imagePath;
                })->flatten(),
                'options' => $product->pricing->apiData(),
                'available_service' => $product->storeServiceExtra->map(function ($service) {
                    return [
                        'id' => $service->id,
                        'service_name' => ucfirst($service->service_name),
                        'price' => $service->service_price
                    ];
                }),
                'quantity_sold' => $product->soldOrders()->sum('order_qty'),
                'share_link' => route('product', Helper::slug_maker($product->pro_title_en, $product->pro_id)),
                'dsiplay_rating' => true,
                'rating' => [
                    'rate' => $rating->rate,
                    'max_rate' => 5,
                    'total' => $rating->total,
                    'percentage' => $rating->percentage,
                    'cumulative' => $rating->cumulative,
                ]
            ]
        ]);
    }

    public function productValidation($productId, $pricingId = null)
    {
        $product = Product::active()
            ->withAvailablePricing($pricingId)
            ->find($productId);

        if (!$product) {
            apiResponse(404, 'Product not found.')->throwResponse();
        }

        if ($product->pricing->isEmpty()) {
            apiResponse(404, 'Product pricing not found.')->throwResponse();
        }

        return $product;
    }

    public function reviews()
    {
        $this->validate(request(), [
            'id' => 'required|integer|exists:nm_product,pro_id',
            'rating' => 'nullable|integer|between:1,5',
            'per_page' => 'nullable|integer|min:1',
            'sort_by' => 'nullable|in:latest,oldest,rating_asc,rating_desc',
            'page' => 'nullable|integer|min:1'
        ]);

        $product = $this->productValidation(request('id'));

        $ratings = $product
                    ->ratings()
                    ->available()
                    ->rating(request('rating'))
                    ->sortBy(request('sort_by'))
                    ->paginate(request('per_page', 30));

        return apiResponse(200, [
            'count' => $ratings->total(),
            'total_page' => $ratings->lastPage(),
            'current_page' => $ratings->currentPage(),
            'data' => $ratings->apiData(),
        ]);
    }
}