<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;

use App\Models\Customer;
use App\Models\Order;
use App\Models\ParentOrder;
use App\Models\Product;
use App\Models\MemberServiceSchedule;

use App\Repositories\MemberServiceScheduleRepo;
use App\Repositories\OrderRepo;

use App\Services\Notifier\Notifier;
use Carbon\Carbon;
use Helper;
use DateTime;
use DateTimeZone;

class ServiceController extends MobileApiController
{
    public function __construct(OrderRepo $orderrepo, MemberServiceScheduleRepo $memberServiceScheduleRepo)
    {
        $this->order = $orderrepo;
        $this->memberServiceSchedule = $memberServiceScheduleRepo;
    }
    
    public function serviceHistory()
    {
        $this->validate(request(),[
            'service_status' => 'integer',
        ]);

        $customer = auth('api')->user()->customer;

        $schedules[] = $this->memberServiceSchedule
        // ->get_member_service_details_from_cus_id(auth('api_members')->user()->cus_id, array(0,1,2,3,-1))->paginate(10);
        ->get_member_service_details_from_cus_id($customer->cus_id, request('service_status'))->paginate(10, request('service_status'));

        $service_arr = [];
        foreach ($schedules as $schedule => $keys) {
            // foreach ($values->groupBy('parent_order_id') as $value => $keys) {
                foreach ($keys as $key) {
                    // dump($key->id);
                    $service_info = [
                        'parent_order_id' => $key->parent_order_id,
                        'order_id' => $key->order_id,
                        'product' => $key->pro_title_en,
                        'pro_id' => $key->pro_id,
                        'product_image_link' => Product::find($key->pro_id)->mainImageUrl(),
                        'total_pro_price' => number_format($key->product_price * $key->order_qty, 2),
                        'stor_id' => $key->stor_id,
                        'stor_name' => $key->stor_name,
                        'order_qty' => $key->order_qty,
                        'order_status' => $key->order_status,
                        'order_date' => $key->order_date,
                        'service_id' => $key->id,
                        'service_status' => $key->status,
                        'service_name' => $key->service_name_current,
                        'service_price' => $key->service_price,
                        'currency' => $key->currency,
                        'service_stor_address' => Helper::getStoreAddress($key->stor_id),
                        'service_stor_image_link' => $key->store->getImageUrl(),
                        'service_schedule_time' => Carbon::parse($key->schedule_datetime)->toDateTimeString(),
                        'service_timezone' => $key->timezone,
                        'reschedule_count_member' => $key->reschedule_count_member,
                        'reschedule_count_merchant' => $key->reschedule_count_merchant,
                    ];

                    array_push($service_arr, $service_info);
                }
            // }
            $total_page = $keys->lastPage();
            $current_page = $keys->currentPage();
        }
        return apiResponse(200, [
            'total_page' => $total_page,
            'current_page' => $current_page,
            'data' => $service_arr,
        ]);
    }

    public function serviceDetails()
    {
        $this->validate(request(),[
            'service_id' => 'required',
        ]);

        $service = $this->memberServiceSchedule->get_details_by_service_id(request('service_id'));

        if(!$service)
            return apiResponse(404, [
                'message' => ['service details not found!'],
            ]); 

        $slots = Carbon::parse($service->appoint_end)->diffInMinutes(Carbon::parse($service->appoint_start)) / $service->interval_period;

        $timeschedule_array = [Carbon::parse($service->appoint_start)->format('g:i A')];

        for($i=0; $i<$slots-1; $i++) {

            $timeschedule_array[$i+1] = Carbon::parse($timeschedule_array[$i])->addMinutes($service->interval_period)->format('g:i A');

        }

        $start_date = Carbon::now()->addDays($service->schedule_skip_count+1)->format('Y-m-d');

        $service_details = [
            'service_id' => $service->id,
            'transaction_id' => $service->transaction_id,
            'order_id' => $service->order_id,
            'pro_id' => $service->pro_id,
            'product' => $service->pro_title_en,
            'stor_id' => $service->stor_id,
            'stor_name' => $service->stor_name,
            'service_stor_address' => Helper::getStoreAddress($service->stor_id),
            'service_stor_phone' => $service->stor_phone,
            'service_stor_office_number' => $service->stor_office_number,
            'service_name' => $service->service_name_current,
            'service_interval_time' => $service->interval_period,
            'service_appoint_start' => $service->appoint_start,
            'service_appoint_end' => $service->appoint_end,
            'service_time_slot' => $timeschedule_array,
            'service_schedule_time' => Carbon::parse($service->schedule_datetime)->toDateTimeString(),
            'service_timezone' => $service->timezone,
            'schedule_skip_count' => $service->schedule_skip_count,
            'schedule_start_date' => $start_date,
            'reschedule_count_member' => $service->reschedule_count_member,
            'reschedule_count_merchant' => $service->reschedule_count_merchant,
            'service_status' => $service->status,
        ];

        return apiResponse(200, [
            'data' => $service_details,
        ]); 
    }

    public function update_service_status()
    {
        $this->validate(request(),[
            'service_id' => 'required',
            'status' => 'required|integer|in:0,1,2,3,4,-1'
        ]);

        $service = MemberServiceSchedule::find(request('service_id'));
        if(!$service)
            return apiResponse(404, [
                'message' => ["Service Id not found"],
            ]);

        $update_status = $this->memberServiceSchedule->update_member_schedule_service_status(request('service_id'), request('status'));
        $service_not_completed = MemberServiceSchedule::where('order_id', '=', $service->order_id)->whereIn('status', array(0,1,2,3,-1))->exists();

        if (!$service_not_completed) {
            $complete_order_status = OrderRepo::update_order_status($service->order_id, 4);
        }

        if (isset($complete_order_status)) {
            return apiResponse(200, [
                'message' => ['Order is completed.'],
            ]); 
        }

        if(!$update_status)
            return apiResponse(500, [
                'message' => ['fail to update service status.'],
            ]);

        $notifier = new Notifier($service);

        if (request('status') == MemberServiceSchedule::STATUS_CONFIRMED) {
            $notifier->send(Notifier::SERVICE_SCHEDULE_CONFIRM);
        }
        elseif (request('status') == MemberServiceSchedule::STATUS_CANCELLED) {
            $notifier->send(Notifier::SERVICE_CANCELLED);
        }
        
        return apiResponse(200, [
            'message' => ['update status sucessfully.'],
        ]);
    }

    public function schedule_service_member()
    {
        $this->validate(request(),[
            'service_id' => 'required',
            'schedule_datetime' => 'required|date_format:Y-m-d g:i A'
        ]);
        
        $service = MemberServiceSchedule::find(request('service_id'));
        if (!$service)
            return apiResponse(404, [
                'message' => ["Service Id not found"],
            ]);

        if ($service->status == '-1')
            return apiResponse(404, [
                'message' => ["Service status is cancelled and can't be scheduled "],
            ]);

        $member_service_info = MemberServiceSchedule::find(request('service_id'));
        $utc_datetime = Carbon::parse(request('schedule_datetime'))->format('Y-m-d H:i:s');

        if ($member_service_info->schedule_datetime == $utc_datetime) {
            return apiResponse(404, [
                'message' => ["This timeslot is same with the record, please pick another timeslot."],
            ]);
        }

        $tz = 'Asia/Jakarta';

        if (auth('api')->user()->customer->country) {
            $tz = auth('api')->user()->customer->country->timezone;
        }

        // $tz = auth('api')->user()->customer->country->timezone;
        $dtz = new DateTimeZone($tz);
        $tztime = new DateTime('now', $dtz);
        $offset = $dtz->getOffset( $tztime ) / 3600;
        $timezone = $tz . ' ' . "UTC" . ($offset < 0 ? $offset : "+".$offset);

        $schedule_service = $this->memberServiceSchedule->update_member_service($utc_datetime, request('service_id'), $timezone);

        if ($member_service_info->reschedule_count_member > 2) {

            // MemberServiceScheduleRepo::update_member_service_status_cancelled(request('service_id'), -1);

            $cancel_service_status = MemberServiceScheduleRepo::update_member_service_status_cancelled(request('service_id'), -1);

            $active_status_exist = MemberServiceSchedule::where('order_id', '=', $member_service_info->order_id)->whereIn('status', array(0,1,2,3))->exists();

            if (!$active_status_exist) {

                $cancel_order_status = OrderRepo::update_order_status($member_service_info->order_id, 5);

            }

            if ($cancel_service_status && isset($cancel_order_status)) {

                return apiResponse(200, [
                    'message' => [trans('front.order.warning.schedule_order_cancel', ['schedule' => $member_service_info->service_name_current])],
                ]);

            } elseif ($cancel_service_status) {
                return apiResponse(200, [
                    'message' => [trans('front.order.warning.schedule_cancel', ['schedule' => $member_service_info->service_name_current])],
                ]);

            }
        }

        if(!$schedule_service)
            return apiResponse(500, [
                'message' => ['fail to update service.'],
            ]);

        (new Notifier($member_service_info))->send(Notifier::SERVICE_RESCHEDULED);

        return apiResponse(200, [
            'message' => ['successfully scheduled service.'],
        ]);
    }
}
