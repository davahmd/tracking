<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;

use DateTime;
use DB;
use Storage;
use App\Events\MessageCreated;
use App\Models\Chat;
use App\Models\Order;
use App\Models\MemberServiceSchedule;
use App\Models\Product;
use App\Models\Store;
use App\Services\Chat\ChatService;

class ChatController extends MobileApiController
{
    public function __construct(ChatService $chatService)
    {
        $this->chatService = $chatService;
    }

    public function list()
    {
        $cus_id = auth('api')->user()->customer->cus_id;

        $store_chats = Chat::whereIn('id', function($query) use ($cus_id) {
            $query->from(with(new Chat)->getTable())
                    ->where('cus_id', $cus_id)
                    ->groupBy('store_id', 'pro_id')
                    ->select(DB::raw('max(id)'));
        })
        ->orderBy('created_at', 'desc')
        ->get()
        ->groupBy('store_id');

        $arr_stores = ['stores' => []];
        foreach ($store_chats as $store_id => $product_chats) {
            $store = Store::where('stor_id', $store_id)->select(['stor_name', 'stor_img'])->first();
            $store_name = $store->stor_name;
            $store_img = asset('asset/images/logo/logo_merchant.png');
            if (Storage::exists('store/'.$store_id.'/'.$store->stor_img)) {
                $store_img = Storage::url('store/'.$store_id.'/'.$store->stor_img);
            }
            $store = [
                'id' => (int)$store_id,
                'name' => $store_name,
                'image' => $store_img,
            ];

            // sort chats by desc before going into loop
            $product_chats = $product_chats->sortByDesc('id');

            $arr_products = [];
            foreach ($product_chats as $product_chat) {
                $tmp_product_id = (int)$product_chat->pro_id;
                $tmp_product = Product::where('pro_id', $tmp_product_id)->first();
                if (!$tmp_product) {
                    continue;
                }

                $product_img = asset('asset/images/product/default.jpg');
                if (Storage::exists('product/' . $tmp_product->pro_mr_id . '/' . $tmp_product->mainImage->image)) {
                    $product_img = Storage::url('product/'.$tmp_product->pro_mr_id.'/'. $tmp_product->mainImage->image);
                }

                $product = [
                    // 'chat_id' => $product_chat->id,
                    'id' => $tmp_product_id,
                    'name' => $tmp_product->title,
                    'image' => $product_img,
                    'msg' => $product_chat->message,
                    'timestamp' => $product_chat->created_at->format('Y-m-d H:i:s'),
                    'sender_type' => $product_chat->sender_type,
                    'is_read' => $product_chat->sender_type == 'U' || ($product_chat->sender_type == 'S' && $product_chat->read_at) ? true : false,
                ];
                
                array_push($arr_products, $product);
            }
            $store['products'] = $arr_products;
            array_push($arr_stores['stores'], $store);
        }

        return apiResponse(200, $arr_stores);
    }

    public function conversation($product_id)
    {
        $cus_id = auth('api')->user()->customer->cus_id;

        // product
        $tmp_product = Product::where('pro_id', $product_id)
            ->select('pro_id', 'pro_sh_id', 'pro_title_en', 'pro_title_'.app()->getLocale())
            ->firstOrFail();

        $product = [
            'id' => $tmp_product->pro_id,
            'name' => $tmp_product->title,
        ];

        // store
        $tmp_store = $tmp_product->store(['stor_id', 'stor_name', 'stor_img'])->firstOrFail();
        $store_id = $tmp_store->stor_id;
        $store_name = $tmp_store->stor_name ?? '';
        $store_img = asset('asset/images/logo/logo_merchant.png');
        if (Storage::exists('store/'.$tmp_store->stor_id.'/'.$tmp_store->stor_img)) {
            $store_img = Storage::url('store/'.$tmp_store->stor_id.'/'.$tmp_store->stor_img);
        }

        $store = [
            'id' => $store_id,
            'name' => $store_name,
            'image' => $store_img,
        ];

        // order
        $order = Order::where([
            'order_cus_id' => $cus_id,
            'order_pro_id' => $product_id,
            ['order_status', '<=', 4],
        ])->first();

        // services
        $services = [];
        if ($order) {
            $services = $order->services()->whereNotNull('schedule_datetime')
                ->select('id', DB::raw('service_name_current name'), 'schedule_datetime', 'status')
                ->get();
                
            foreach($services as $service) {
                $service->schedule = date('j/m/y g:i A', strtotime($service->schedule_datetime));
                $service->status = MemberServiceSchedule::status()[$service->status];
                unset($service->schedule_datetime);
            }
        }

        // chat history
        $chat_query = $tmp_product->chats(['cus_id' => $cus_id]);
        $chats = $tmp_product->chats()->select('id', DB::raw('sender_type type'), DB::raw('message msg'), 'created_at')->get();

        $latest_timestamp = '';
        if ($chats->isEmpty() == false) {
            $latest_chat = $chats->sortByDesc('created_at')->first();
            $latest_timestamp = $latest_chat->created_at->format('Y-m-d H:i:s');
        }

        foreach ($chats as $chat) {
            $chat->timestamp = $chat->created_at->format('Y-m-d H:i:s');
            unset($chat->created_at);
        }

        // update these chats with customer read datetime
        $chat_query->where('sender_type', 'S')
            ->whereNull('read_at')
            ->update([
                'read_at' => new DateTime
            ]);

        return apiResponse(200, compact('store', 'product', 'services', 'latest_timestamp', 'chats'));
    }

    public function postMessage($store_id, $product_id)
    {
        if (strlen(request('msg')) <= 0) {
            return;
        }

        $customer_id = auth('api')->user()->customer->cus_id;

        $order = Order::where([
            'order_cus_id' => $customer_id,
            'order_pro_id' => $product_id,
        ])
        ->select(['order_id'])
        ->first();

        $order_id = null;
        if ($order) {
            $order_id = $order->order_id;
        }

        $message = request('msg');
        $message = htmlspecialchars($message);
        $message = substr($message, 0, 1000);

        $chat = Chat::create([
            'cus_id' => $customer_id,
            'store_id' => (int)$store_id,
            'pro_id' => (int)$product_id,
            'order_id' => $order_id,
            'sender_type' => 'U',
            'message' => $message
        ]);

        // update these chats with customer read datetime
        Chat::where([
            'cus_id' => $customer_id,
            'store_id' => (int)$store_id,
            'pro_id' => (int)$product_id
        ])
        ->where('sender_type', 'S')
        ->whereNull('read_at')
        ->update([
            'read_at' => new DateTime
        ]);

        broadcast(new MessageCreated($chat))->toOthers();
     

        return apiResponse(200, 'success');
    }

    public function unreadMessagesCount()
    {
        $unread_count = $this->chatService->getCustomerUnreadMessagesCount();

        return apiResponse(200, compact('unread_count'));
    }

    public function messagesRead($store_id, $product_id)
    {
        // update these chats with customer read datetime
        Chat::where([
            'cus_id' => auth('api')->user()->customer->cus_id,
            'store_id' => (int)$store_id,
            'pro_id' => (int)$product_id
        ])
        ->where('sender_type', 'S')
        ->whereNull('read_at')
        ->update([
            'read_at' => new DateTime
        ]);

        return apiResponse(200, 'success');
    }
}
