<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Merchant;
use App\Models\OfficialBrands;
use App\Models\Product;
use App\Models\Store;

class SearchController extends MobileApiController
{
    public function index()
    {
        $this->validate(request(), [
            'name' => 'nullable|max:255'
        ]);

        return apiResponse(200, [
            'limit' => 5,
            'data' => [
                'stores' => $this->dataCollection(Store::class)->apiData(),
                'official_brands' => $this->dataCollection(OfficialBrands::class)->apiData(),
                'merchants' => $this->dataCollection(Merchant::class)->apiData(),
                'products' => $this->dataCollection(Product::class)->apiData()
            ]
        ]);
    }

    protected function dataCollection($model)
    {
        return $model::nameContain(request('name'))
            ->active()
            ->limit(5)
            ->get();
    }
}