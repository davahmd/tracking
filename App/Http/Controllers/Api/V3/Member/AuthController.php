<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;

use App\Models\Customer;

use Carbon\Carbon;
use DB;
use Exception;
use Hash;

class AuthController extends MobileApiController
{

    public function login()
    {
        $this->validate(request(), [
            'email' => 'required_without:area_code,phone|email',
            'area_code' => 'required_without:email',
            'phone' => 'required_with:area_code',
            'password' => 'required'
        ]);

        $credential = [];

        if (request()->has('email')) {
            $credential['email'] = request('email');
        }
        else {
            $customer = Customer::where('phone_area_code', request('area_code'))
                ->where('cus_phone', request('phone'))
                ->first();

            if (!$customer || !$customer->user) {
                return apiResponse(403, 'Invalid credential.');
            }

            $credential['email'] = $customer->user->email;
        }

        $credential['password'] = request('password');

        $authUser = $this->getAuthUser($credential);
        $customer = $authUser->customer;

        $customer->app_session = substr(str_shuffle(md5(time())), 0, 20);
        $customer->app_session_date = Carbon::now()->toDateTimeString();
        $customer->save();

        return apiResponse(200, [
            'data' => [
                'id' => $authUser->customer->cus_id,
                'name' => $authUser->customer->cus_name,
                'role' => $authUser->authMerchantUser() ? 'merchant' : 'customer',
                'avatar' => ($authUser->customer->cus_pic) ? env('IMAGE_DIR') . '/avatar/' . $authUser->cus_id . '/' . $authUser->cus_pic : '',
                'app_session' => $authUser->customer->app_session,
                'api_token' => $authUser->createToken('Zona Member App')->accessToken
            ]
        ]);
    }

    protected function getAuthUser($credential)
    {
        if (!auth()->once($credential)) {
            apiResponse(403, 'Invalid credential.')->throwResponse();
        }

        return auth()->user();
    }

    public function logout()
    {
        auth('api')->user()->token()->delete();

        return apiResponse(200, 'Logout success.');
    }

    protected function usernameIsEmail($username)
    {
        return filter_var($username, FILTER_VALIDATE_EMAIL);
    }
}
