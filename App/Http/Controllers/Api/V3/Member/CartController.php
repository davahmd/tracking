<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Cart;
use App\Models\MemberServiceSchedule;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductPricing;
use App\Models\StoreServiceExtra;
use App\Services\Cart\CartService;
use App\Models\Shipping;
use App\Services\Notifier\Notifier;
use App\Models\ParentOrder;
use App\Services\Midtrans\MidtransService;
use App\Repositories\PromotionRepo;
use App\Repositories\CourierRepo;

class CartController extends MobileApiController
{
    public function index()
    {
        $this->validate(request(), [
            'wholesale' => 'nullable|in:0,1'
        ]);

        $carts = (new CartService)->listing(auth('api')->user()->customer->cus_id, null, request('wholesale') != null ? request('wholesale') : false);
        $total = (new CartService)->total($carts);
        $shippings = auth('api')->user()->customer->shipping;

        $cart_shipping_id = $carts->where('ship_id', '>', '0')->pluck('ship_id')->first();

        if (!$cart_shipping_id) {
            $shipping = $shippings->where('ship_order_id', 0)->sortByDesc('isdefault')->first();
        }
        else {
            $shipping = $shippings->where('ship_id', $cart_shipping_id)->first();
        }

        return apiResponse(200, [
            'data' => $carts->groupBy('product.store.stor_name')->values()->map(function ($items, $store_id) use ($shipping, $total) {
                $store = $items->first()->product->store;
                return [
                    'store' => [
                        'id' => $store->stor_id,
                        'name' => $store->stor_name
                    ],
                    'items' => $items->map(function ($item) {
                        return [
                            'id' => $item->id,
                            'product' => [
                                'id' => $item->product->pro_id,
                                'name' => $item->product->title,
                                'image' => $item->product->mainImageUrl(),
                            ],
                            'pricing' => [
                                'id' => $item->pricing->id,
                                'purchase_price' => $item->pricing->getPurchasePrice(),
                                'attributes' => $item->pricing->convertProductAttributes(),
                                // 'weight' => $item->pricing->weight,
                            ],
                            'service' => ($item->service_ids) ? $item->services : [],
                            'quantity' => $item->quantity,
                            'weight' => ($item->weight_per_qty * $item->quantity),
                            'subtotal' => $item->charge->amount->subtotal,
                            'currency_symbol' => config('app.currency_code')
                        ];
                    }),
                    'cart_summary' => [
                        'subtotal' => $total->amount->subtotal,
                        'currency_symbol' => config('app.currency_code'),
                    ],
                    'courier' => ($shipping) ? $this->courierMapping($items, $shipping) : []
                ];
            }),
            'proceed_checkout' => ($shipping) ? true : false,
        ]);
    }

    protected function courierMapping($items, $shipping)
    {
        $couriers = (new CartService)->getCourier($items, $shipping);

        $courier_options = [];

        if ($couriers) {
            foreach ($couriers as $x => $courier) {
                if ($courier['options']) {
                    foreach ($courier['options'] as $y => $option) {
                        $courier_options[$y] = [
                            'code' => $option->code,
                            'name' => $option->name
                        ];
                        if ($option->costs) {
                            foreach ($option->costs as $z => $cost) {
                                if ($cost->cost) {
                                    foreach ($cost->cost as $a => $data) {
                                        $courier_options[$y]['costs'][] = [
                                            'service' => $cost->service,
                                            'description' => $cost->description,
                                            'value' => $data->value,
                                            'etd' => $data->etd,
                                            'note' => $data->note
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $courier_options;
    }

    public function addItem()
    {
        $this->validate(request(), [
            'product_id' => 'required|integer',
            'pricing_id' => 'required|integer',
            'quantity' => 'required|integer|min:1',
            'wholesale' => 'nullable|in:0,1'
        ]);

        $product = Product::available()->find(request('product_id'));

        if (!$product) {
            return apiResponse(404, 'Product not found.');
        }

        if ($product->pricing->where('id', request('pricing_id'))->isEmpty()) {
            return apiResponse(404, 'Pricing for this product not found.');
        }

        // filtering non-related service id for requested product
        if (request()->has('service_id') && $product->storeServiceExtra) {
            $service_id = $product->storeServiceExtra
                ->whereIn('id', explode(',', request('service_id')))
                ->pluck('id')
                ->toArray();
        }

        $added = (new CartService)->add(
            auth('api')->user()->customer->cus_id,
            request('product_id'),
            request('pricing_id'),
            request('quantity'),
            isset($service_id) && !empty($service_id) ? implode('|', $service_id) : null,
            null,
            request('wholesale') != null ? request('wholesale') : false
        );

        if ($added) {
            return apiResponse(200, 'success');
        }

        return apiResponse(500, 'Failed.');
    }

    public function removeItem()
    {
        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $this->cartItemValidation(request('id'))->delete();

        return apiResponse(200, 'success');
    }

    public function updateQuantity()
    {
        $this->validate(request(), [
            'id' => 'required|integer',
            'increment' => 'required|integer|in:-1,1'
        ]);

        $cartItem = $this->cartItemValidation(request('id'), auth('api')->user()->customer->cus_id);

        if ($cartItem->wholesale) {
            $newQuantity = $cartItem->quantity + request('increment');

            if ($newQuantity < $cartItem->pricing->min_quantity) {
                return apiResponse(422, [
                    'message' => ['Minimum quantity is ' . $cartItem->pricing->min_quantity]
                ]);
            }
        }

        if (request('increment') > 0 && $cartItem->quantity == $cartItem->pricing->quantity) {
            return apiResponse(422, [
                'message' => ['Already hit maximum quantity for this product.']
            ]);
        }

        $cartItem->increment('quantity', request('increment'));

        return apiResponse(200, 'success');
    }

    protected function cartItemValidation($cartItemId, $customerId = null)
    {
        $cartItem = Cart::find($cartItemId);

        if (!$cartItem) {
            apiResponse(200, 'Cart item not found.')->throwResponse();
        }

        if ($customerId && $cartItem->cus_id != $customerId) {
            apiResponse(403, 'Cart item is not belongs to this customer.')->throwResponse();
        }

        return $cartItem;
    }

    public function updateCourier()
    {
        $this->validate(request(), [
            'store_id' => 'required|integer',
            'courier_code' => 'required',
            'courier_service' => 'required',
            'courier_description' => 'required',
            'courier_etd' => 'required',
            'courier_value' => 'required'
        ]);

        try {
            $cart = (new CartService)->updateCartCourier(auth('api')->user()->customer->cus_id, request('store_id'), [
                'code' => request('courier_code'),
                'service' => request('courier_service'),
                'eta' => request('courier_etd'),
                'fee' => request('courier_value'),
                'note' => request('courier_code') . ' - ' . request('courier_description'),
            ]);

            return apiResponse(200, 'success');
        }
        catch (\Exception $e) {
            return apiResponse(500, 'Failed.');
        }
    }

    public function updateShipping()
    {
        $this->validate(request(), [
            'shipping_id' => 'required|integer'
        ]);

        $customer = auth('api')->user()->customer;

        // Check shipping_id
        $shipping = Shipping::where('ship_id', request('shipping_id'))->where('ship_cus_id', $customer->cus_id)->first();

        if ($shipping) {
            try {
                $cart = Cart::where('cus_id', $customer->cus_id)->update([
                    'ship_id' => request('shipping_id')
                ]);

                return apiResponse(200, 'success');
            }
            catch (\Exception $e) {
                return apiResponse(500, 'Failed.');
            }
        }

        apiResponse(403, 'Shipping Address does not belongs to this customer.')->throwResponse();
    }

    public function checkout()
    {
        $this->validate(request(), [
            'shipping_id' => 'required|integer',
            'wholesale' => 'nullable|in:0,1'
        ]);

        $customer = auth('api')->user()->customer;
        $shipping = Shipping::find(request('shipping_id'));

        if (!$customer->carts()->where('wholesale', request('wholesale') ?? 0)->exists()) {
            return apiResponse(400, 'cart is empty.');
        }

        if (!$shipping && $customer->carts()->hasServices()->exists()) {
            return apiResponse(404, 'shipping address not found.');
        }

        if (Cart::courierInfoNotUpdatedWhereCustomer($customer, request('wholesale'))) {
            return apiResponse(400, 'please select courier.');
        }

        // Cart::updateShippingChargesForCustomer($customer); // disabled checking now in cartservice

        $address['name'] = $shipping->ship_name;
        $address['address_1'] = $shipping->ship_address1;
        $address['address_2'] = $shipping->ship_address2;
        $address['city'] = $shipping->ship_ci_id;
        $address['country'] = $shipping->ship_country;
        $address['state'] = $shipping->ship_state_id;
        $address['postal_code'] = $shipping->ship_postalcode;
        $address['telephone'] = $shipping->ship_phone;
        $address['sub_district'] = $shipping->ship_subdistrict_id;

        $checkout = (new CartService)->checkout($customer->cus_id, $address, request('wholesale'))->getData();

        if ($checkout) {
            if (isset($checkout->status) && $checkout->status != 400) {
                return apiResponse(400, 'checkout failed.');
            }

            $parentOrder = ParentOrder::where('transaction_id', $checkout->data->transaction_id)->first();

            $snaptoken = (new MidtransService)->get_snapToken($parentOrder);
            if($snaptoken)
            {
                return apiResponse(200, [
                    'message' => 'checkout success',
                    'data' => [
                        'snaptoken' => $snaptoken,
                        'parent_order_id' => $parentOrder->id,
                        'transaction_id' => $parentOrder->transaction_id,
                    ]
                ]);
            }

            // return apiResponse(200, 'checkout success');
        }

        return apiResponse(400, 'checkout failed.');
    }

    public function promoCodeValidation()
    {
        $this->validate(request(), [
            'promo_code' => 'required',
        ]);

        $customer_id = auth('api')->user()->customer->cus_id;
        $carts = (new CartService)->listing($customer_id);

        $promo_code = request('promo_code');
        $products = $carts->pluck('product_id')->toArray();

        try {
            $promotion = PromotionRepo::validatePromoCode($promo_code, $products);
        }
        catch (\Exception $e) {
            return apiResponse(400, trans('localize.invalid_promo_code'));
        }

        $promo_products = PromotionRepo::validatePromoItem($carts, $promotion);

        if (!$promo_products) {
            return apiResponse(400, trans('localize.invalid_cart_item'));
        }

        try {
            PromotionRepo::validatePromoCodeDateRange($promo_code);
        }
        catch (\Exception $e) {
            return apiResponse(400, trans('localize.promo_code_expired'));
        }

        try {
            $validate_min_spend = PromotionRepo::checkPromoMinSpend($promotion->id, $promo_products);
        }
        catch (\Exception $e) {
            return apiResponse(400, trans('localize.min_spend_error'));
        }

        try {
            PromotionRepo::validatePromoCodeCount($promo_code);
        }
        catch (\Exception $e) {
            return apiResponse(400, trans('localize.promo_code_fully_redeemed'));
        }

        try {
            $promotion = PromotionRepo::getPromoCode($promo_code, $products);
        }
        catch (\Exception $e) {
            return apiResponse(400, trans('localize.invalid_promo_code'));
        }

        try {
            PromotionRepo::updateCartPromoId($customer_id, $carts, $promotion);
        }
        catch (\Exception $e) {
            return apiResponse(400, trans('localize.error_occurred'));
        }

        $products = [];

        foreach ($promotion->promo_products as $product) {
            array_push($products, $product);
        }

        $deducted_value = 0;

        if ($promotion->applied_to != 1) { // 0: product only, 1: shipping only, 2: both

            // Calculate discount value
            if ($promotion->discount_rate) {
                foreach ($promo_products as $product) {

                    $deducted_value += $product->charge->amount->subtotal * $promotion->discount_rate;
                }
            }
            else if ($promotion->discount_value) {
                foreach ($promo_products as $product) {

                    $deducted_value += ($product->quantity * $promotion->discount_value);
                }
            }
        }

        try {
            CourierRepo::calculate_shipping_fee($carts, $customer_id);
        }
        catch (\Exception $e) {
            return apiResponse(400, trans('localize.error_occurred'));
        }

        $total_shipping_fee = 0;

        $carts = (new CartService)->listing($customer_id);

        foreach ($carts as $cart) {

            $total_shipping_fee += $cart->shipping_fee;
        }

        $deducted_shipping = 0;

        if ($promotion->applied_to != 0) { // 0: product only, 1: shipping only, 2: both

            foreach ($promo_products as $product) {

                if ($promotion->discount_rate) {
                    $deducted_shipping +=  $product->shipping_fee * $promotion->discount_rate;
                }
                elseif ($promotion->discount_value) {
                    $deducted_shipping +=  $promotion->discount_value;
                }
            }
        }

        $total_discount = $deducted_value + $deducted_shipping;

        return apiResponse(200, [
            'message' => trans('localize.promo_code_successfully_applied'),
            'data' => [
                'discount_rate' => $promotion->discount_rate,
                'discount_value' => $promotion->discount_value,
                'total_discount_value' => $total_discount,
                'products' => $products,
                'applied_to' => $promotion->applied_to,
            ]
        ]);
    }
}