<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\OfficialBrands;

class OfficialBrandController extends MobileApiController
{
    public function index()
    {
        $this->validate(request(), [
            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer|min:1'
        ]);

        $brands = OfficialBrands::active()
            ->nameContain(request('name'))
            ->paginate(request('per_page'));

        return apiResponse(200, [
            'count' => $brands->total(),
            'total_page' => $brands->lastPage(),
            'data' => $brands->apiData()
        ]);
    }
}