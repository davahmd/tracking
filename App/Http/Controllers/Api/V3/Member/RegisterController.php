<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;

use App\Models\Country;
use App\Models\Customer;

use App\Models\User;
use App\Repositories\SmsRepo;

use DB;
use Hash;
use Request;
use Validator;

class RegisterController extends MobileApiController
{
    public function __construct(SmsRepo $smsrepo)
    {
        $this->sms = $smsrepo;
    }

    public function register()
    {
        $data = Request::only('fullname','nid','tac','email','country_id','phone_area_code','phone_no','password','password_confirmation');

        $v = Validator::make($data, [
            'fullname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:' . (new Customer)->getTable(),
            'country_id' => 'required|exists:' . (new Country)->getTable() . ',co_id',
            'phone_area_code' => 'required',
            'phone_no' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required_with:password'
        ]);

        $existing_tac = $this->sms->check_tac_exist($data['phone_area_code'] . $data['phone_no'], 'REGISTER_MEMBER', null, null);

        $v->after(function($v) use ($data, $existing_tac){

            if (!$existing_tac) {
                $v->errors()->add('tac', trans('tac.error.not_exist'));
            }
            elseif ($existing_tac->tac != $data['tac']) {
                $v->errors()->add('tac', trans('tac.error.not_exist'));
            }

        });

        if ($v->fails()) { 
            return apiResponse(422, [
                'message' => $v->messages()->all(),
            ]);
        }

        try {

            DB::beginTransaction();

            $existing_tac->update([
                'is_verified' => true,
            ]);

            $user = User::create([
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'status' => true,
                'activation' => true
            ]);

            $user->customer()->create([
                'cus_name' => $data['fullname'],
                'identity_card' => $data['nid'],
                'cus_country' => $data['country_id'],
                'phone_area_code' => $data['phone_area_code'],
                'cus_phone' => $data['phone_no'],
            ]);

            DB::commit();

        }
        catch (Exception $e) {
            DB::rollback();

            return apiResponse(500, 'system error');
        }

        return apiResponse(200, 'success');
    }
}