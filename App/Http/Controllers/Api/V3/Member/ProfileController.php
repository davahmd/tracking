<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\City;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerVehicleMapping;
use App\Models\Shipping;
use App\Models\State;
use App\Models\Subdistrict;
use App\Models\Vehicle;
use App\Repositories\CustomerRepo;
use Hash;
use Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProfileController extends MobileApiController
{
    public function details()
    {
        sanitizeApiRequest(request()->all());

        $customer = auth('api')->user()->customer;

        return apiResponse(200, [
            'data' => [
                'id' => $customer->cus_id,
                'details' => [
                    'name' => $customer->cus_name,
                    'email' => auth('api')->user()->email,
                    'areacode' => $customer->phone_area_code,
                    'phone' => $customer->cus_phone,
                    'avatar' => $customer->getAvatarUrl()
                ],
                'role' => auth('api')->user()->authMerchantUser() ? 'merchant' : 'customer',
                'addresses' => $customer->shipping->map(function ($address) {
                    return [
                        'id' => $address->ship_id,
                        'name' => $address->ship_name,
                        'areacode' => $address->areacode,
                        'phone' => $address->ship_phone,
                        'address_line_1' => $address->ship_address1,
                        'address_line_2' => $address->ship_address2,
                        'city' => $address->ship_city_name,
                        'postcode' => $address->ship_postalcode,
                        'state' => $address->state ? [
                            'id' => $address->state->id,
                            'name' => $address->state->name
                        ] : null,
                        'country' => $address->country ? [
                            'id' => $address->country->co_id,
                            'name' => $address->country->co_name
                        ] : null,
                        'default_address' => $address->isdefault
                    ];
                })
            ]
        ]);
    }

    public function addresses()
    {
        sanitizeApiRequest(request()->all());

        $this->validate(request(), [
            'address_id' => [
                'integer',
                Rule::exists('nm_shipping', 'ship_id')->where(function ($query) {
                    $query->where('ship_cus_id', auth('api')->user()->customer->id);
                })
            ]
        ]);

        return apiResponse(200, [
            'data' => [
                'addresses' => auth('api')->user()->customer
                    ->shipping()
                    ->when(request()->has('address_id'), function ($query) {
                        return $query->whereShipId(request('address_id'));
                    })
                    ->where('ship_order_id', 0)
                    ->get()
                    ->map(function ($address) {
                        return [
                            'id' => $address->ship_id,
                            'name' => $address->ship_name,
                            'areacode' => $address->areacode,
                            'phone' => $address->ship_phone,
                            'address_line_1' => $address->ship_address1,
                            'address_line_2' => $address->ship_address2,
                            'city' => $address->city ? [
                                'id' => $address->city->id,
                                'name' => $address->city->name
                            ] : null,
                            'district' => $address->subdistrict ? [
                                'id' => $address->subdistrict->id,
                                'name' => $address->subdistrict->name
                            ] : null,
                            'postcode' => $address->ship_postalcode,
                            'state' => $address->state ? [
                                'id' => $address->state->id,
                                'name' => $address->state->name
                            ] : null,
                            'country' => $address->country ? [
                                'id' => $address->country->co_id,
                                'name' => $address->country->co_name
                            ] : null,
                            'default_address' => $address->isdefault
                        ];
                    })
            ]
        ]);
    }

    public function update()
    {
        $this->validate(request(), [
            'email' => 'required|email',
            'full_name' => 'required'
        ]);

        DB::transaction(function () {
            auth('api')->user()->update([
                'email' => request('email')
            ]);

            auth('api')->user()->customer->update([
                'cus_name' => request('full_name')
            ]);
        });

        return apiResponse(200, 'success');
    }

    public function updateAddress()
    {
        $country = new Country;

        $this->validate(request(), [
            'id' => 'required|integer',
            'name' => 'required|max:150',
            'address_1' => 'required|max:200',
            'address_2' => 'max:200',
            'city_id' => 'required|integer|exists:' . (new City)->getTable() . ',id',
            'district_id' => 'required|integer|exists:' . (new Subdistrict)->getTable() . ',id',
            'country_id' => 'required|integer|exists:' . $country->getTable() . ',co_id',
            'state_id' => 'required|integer|exists:' . (new State)->getTable() . ',id',
            'areacode' => 'required',
            'phone' => 'required|numeric',
            'postal_code' => 'required|max:20',
            'default_address' => 'nullable|in:0,1'
        ]);

        $customer = auth('api')->user()->customer;
        $address = $this->addressValidation(request('id'), $customer->cus_id);

        DB::transaction(function () use ($customer, $address) {
            $customer->shipping()->where('isdefault', 1)->update(['isdefault' => 0]);

            $address->update([
                'ship_name' => request('name'),
                'areacode' => request('areacode'),
                'ship_phone' => request('phone'),
                'ship_address1' => request('address_1'),
                'ship_address2' => request('address_2'),
                'ship_subdistrict_id' => request('district_id'),
                'ship_ci_id' => request('city_id'),
                'ship_state_id' => request('state_id'),
                'ship_country' => request('country_id'),
                'ship_postalcode' => request('postal_code'),
                'isdefault' => request('default_address'),
            ]);
        });

        return apiResponse(200, 'success');
    }

    public function addressValidation($addressId, $customerId)
    {
        $address = Shipping::find($addressId);

        if (!$address) {
            apiResponse(404, 'address not found')->throwResponse();
        }


        if ($address->ship_cus_id != $customerId) {
            apiResponse(403, 'address is not belongs to customer')->throwResponse();
        }

        return $address;
    }

    public function addAddress()
    {
        $country = new Country;

        $this->validate(request(), [
            'name' => 'required|max:150',
            'address_1' => 'required|max:200',
            'address_2' => 'max:200',
            'city_id' => 'required|integer|exists:' . (new City)->getTable() . ',id',
            'district_id' => 'required|integer|exists:' . (new Subdistrict)->getTable() . ',id',
            'country_id' => 'required|integer|exists:' . $country->getTable() . ',co_id',
            'state_id' => 'required|integer|exists:' . (new State)->getTable() . ',id',
            'areacode' => 'required',
            'phone' => 'required|numeric',
            'postal_code' => 'required|max:20',
            'default_address' => 'nullable|in:0,1'
        ]);

        $address = auth('api')
            ->user()
            ->customer
            ->addShippingAddress(new Shipping([
                'ship_name' => request('name'),
                'areacode' => request('areacode'),
                'ship_phone' => request('phone'),
                'ship_address1' => request('address_1'),
                'ship_address2' => request('address_2'),
                'ship_subdistrict_id' => request('district_id'),
                'ship_ci_id' => request('city_id'),
                'ship_state_id' => request('state_id'),
                'ship_country' => request('country_id'),
                'ship_postalcode' => request('postal_code'),
                'isdefault' => request('default_address'),
            ]));

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'id' => $address->ship_id,
                'name' => $address->ship_name,
                'areacode' => $address->areacode,
                'phone' => $address->ship_phone,
                'address_line_1' => $address->ship_address1,
                'address_line_2' => $address->ship_address2,
                'city' => $address->city ? [
                    'id' => $address->city->id,
                    'name' => $address->city->name
                ] : null,
                'district' => $address->subdistrict ? [
                    'id' => $address->subdistrict->id,
                    'name' => $address->subdistrict->name
                ] : null,
                'postcode' => $address->ship_postalcode,
                'state' => $address->state ? [
                    'id' => $address->state->id,
                    'name' => $address->state->name
                ] : null,
                'country' => $address->country ? [
                    'id' => $address->country->co_id,
                    'name' => $address->country->co_name
                ] : null,
                'default_address' => $address->isdefault
            ]
        ]);
    }

    public function removeAddress()
    {
        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $customer = auth('api')->user()->customer;

        $address = $this->addressValidation(request('id'), $customer->cus_id);

        if ($address->isdefault == true) {
            $customer
                ->shipping()
                ->notDefault()
                ->first()
                ->update([
                    'isdefault' => 1
                ]);
        }

        $address->delete();

        return apiResponse(200, 'success');
    }

    public function updatePassword()
    {
        $this->validate(request(), [
            'old_password' => 'required',
            'new_password' => 'required|confirmed',
            'new_password_confirmation' => 'required'
        ]);

        $user = auth('api')->user();

        if (!Hash::check(request('old_password'), $user->password)) {
            return apiResponse(200, 'Old password verification failed.');
        }

        $user->update([
            'password' => bcrypt(request('new_password'))
        ]);

        return apiResponse(200, 'success');
    }

    public function cars()
    {
        sanitizeApiRequest(request()->all());

        $customer = auth('api')->user()->customer;

        $this->validate(request(), [
            'car_id' => [
                'integer',
                Rule::exists('customer_vehicle_mappings', 'id')->where(function ($query) use ($customer) {
                    $query->where('customer_id', $customer->cus_id);
                })
            ]
        ]);

        try {
            $cars = $customer->vehicles()
                ->orderBy('isdefault', 'DESC')
                ->get();

            for ($i = 0; $i < count($cars); $i++) {
                if ($cars[$i]->vehicle_image != '') {
                    $cars[$i]['vehicle_image'] = Storage::url('gallery/vehicle/' . $cars[$i]->vehicle_image);
                }
            }

            return apiResponse(200, [
                'data' => [
                    'cars' => $cars
                ]
            ]);
        } catch (\Exception $e) {
            return apiResponse(500, trans('api.systemError'));
        }
    }

    public function updateCar()
    {
        $this->validate(request(), [
            'id' => 'required|integer',
            'vehicle_id' => 'required|integer',
            'default_car' => 'required|in:0,1',
            'name' => 'required',
            'insurance' => 'required',
            'insurance_expired_date' => 'required|date',
            'road_tax_expired_date' => 'required|date',
            'mileage' => 'required|integer',
        ]);

        $customer = auth('api')->user()->customer;
        $car = $this->carValidation(request('id'), $customer->cus_id);

        $car->update([
            'vehicle_id' => request('vehicle_id'),
            'isdefault' => request('default_car'),
            'name' => request('name'),
            'insurance' => request('insurance'),
            'insurance_expired_date' => request('insurance_expired_date'),
            'road_tax_expired_date' => request('road_tax_expired_date'),
            'mileage' => request('mileage'),
        ]);

        if (request('default_car') == 1) {
            try {
                CustomerRepo::set_automobile_default($customer->cus_id, request('id'));
            } catch (\Exception $e) {
                return apiResponse(500, 'failed');
            }
        }

        return apiResponse(200, 'success');
    }

    public function carValidation($carId, $customerId)
    {
        $car = CustomerVehicleMapping::find($carId);

        if (!$car) {
            apiResponse(404, 'car not found')->throwResponse();
        }

        if ($car->customer_id != $customerId) {
            apiResponse(403, 'car is not belongs to customer')->throwResponse();
        }

        return $car;
    }

    public function addCar()
    {
        $this->validate(request(), [
            'vehicle_id' => 'required|integer',
            'default_car' => 'nullable|in:0,1',
            'name' => 'required',
            'insurance' => 'required',
            'insurance_expired_date' => 'required|date',
            'road_tax_expired_date' => 'required|date',
            'mileage' => 'required|integer',
        ]);

        $customer = auth('api')->user()->customer;

        auth('api')->user()->customer->vehicles()->attach($customer->cus_id,
            [
                'isdefault' => request('default_car'),
                'vehicle_id' => request('vehicle_id'),
                'name' => request('name'),
                'insurance' => request('insurance'),
                'insurance_expired_date' => request('insurance_expired_date'),
                'road_tax_expired_date' => request('road_tax_expired_date'),
                'mileage' => request('mileage'),
            ]);


        if (request('default_car') === '1') {
            DB::table('customer_vehicle_mappings')
                ->where([
                    ['vehicle_id', '<>', request('vehicle_id')],
                    ['customer_id', '=', $customer->cus_id],
                ])
                ->update(['isdefault' => 0]);
        }
        return apiResponse(200, 'success');
    }

    public function removeCar()
    {
        //request vehicle_id
        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $customer = auth('api')->user()->customer;
        $defaultFlag = false;

        $removeDefault = DB::table('customer_vehicle_mappings')
            ->where([
                ['vehicle_id', '=', request('id')],
                ['customer_id', '=', $customer->cus_id],
            ])->get(['isdefault']);
        if ($removeDefault->first()->isdefault === '1'
        ) {
            $defaultFlag = true;
        };
        $customer->vehicles()->detach(request('id'));

        if ($defaultFlag === true) {
            DB::table('customer_vehicle_mappings')
                ->where([
                    ['customer_id', '=', $customer->cus_id],
                ])
                ->limit(1)
                ->update(['isdefault' => 1]);
        }

        return apiResponse(200, 'success');
    }

    public function getModel()
    {
        $this->validate(request(), [
            'id' => 'nullable',
            'brand' => 'nullable',
            'model' => 'nullable',
            'variant' => 'nullable',
            'year' => 'nullable|integer'
        ]);


        if (request('id')) {
            $data = Vehicle::where([
                ['id', '=', request('id')]
            ])
                ->get()
                ->map(function ($model) {
                    return [
                        'id' => $model->id,
                        'brand' => $model->brand,
                        'model' => $model->model,
                        'variant' => $model->variant,
                        'year' => $model->year
                    ];
                });
        } elseif (request('brand') && request('model') && request('variant') && request('year')) {
            $data = Vehicle::where([
                ['brand', '=', request('brand')],
                ['model', '=', request('model')],
                ['variant', '=', request('variant')],
                ['year', '=', request('year')]
            ])
                ->get()
                ->map(function ($model) {
                    return [
                        'id' => $model->id
                    ];
                });
        } elseif (request('brand') && request('model') && request('variant')) {
            $data = Vehicle::where([
                ['brand', '=', request('brand')],
                ['model', '=', request('model')],
                ['variant', '=', request('variant')]
            ])
                ->get()
                ->map(function ($model) {
                    return [
                        'id' => $model->id,
                        'isSelected' => false,
                        'label' => $model->year
                    ];
                });
        } elseif (request('brand') && request('model')) {
            $data = Vehicle::where([
                ['brand', '=', request('brand')],
                ['model', '=', request('model')]
            ])
                ->groupBy('variant')
                ->get()
                ->map(function ($model) {
                    return [
                        'id' => $model->id,
                        'isSelected' => false,
                        'label' => $model->variant
                    ];
                });
        } elseif (request('brand')) {
            $data = Vehicle::where('brand', '=', request('brand'))
                ->groupBy('model')
                ->get()
                ->map(function ($model) {
                    return [
                        'id' => $model->id,
                        'isSelected' => false,
                        'label' => $model->model
                    ];
                });
        } else {
            $data = Vehicle::groupBy('brand')
                ->get()
                ->map(function ($model) {
                    return [
                        'id' => $model->id,
                        'isSelected' => false,
                        'label' => $model->brand
                    ];
                });
        }

        return apiResponse(200, [
            'data' => $data
        ]);
    }

    public function profileAddress()
    {
        $customer = auth('api')->user()->customer;

        return apiResponse(200, [
            'data' => [
                'address_1' => $customer->cus_address1,
                'address_2' => $customer->cus_address2,
                'postal_code' => $customer->cus_postalcode,
                'country' => $customer->country ? [
                    'id' => $customer->country->co_id,
                    'name' => $customer->country->co_name
                ] : null,
                'state' => $customer->state ? [
                    'id' => $customer->state->id,
                    'name' => $customer->state->name
                ] : null,
                'city' => $customer->city ? [
                    'id' => $customer->city->id,
                    'name' => $customer->city->name
                ] : null,
                'subdistrict' => $customer->subdistrict ? [
                    'id' => $customer->subdistrict->id,
                    'name' => $customer->subdistrict->name
                ] : null,
            ]
        ]);
    }

    public function updateProfileAddress()
    {
        $this->validate(request(), [
            'address_1' => 'required',
            'address_2' => 'nullable',
            'postal_code' => 'required',
            'country_id' => 'required|exists:' . (new Country)->getTable() . ',co_id',
            'state_id' => 'required|exists:' . (new State)->getTable() . ',id',
            'city_id' => 'required|exists:' . (new City)->getTable() . ',id',
            'subdistrict_id' => 'required|exists:' . (new Subdistrict)->getTable() . ',id'
        ]);

        auth('api')->user()->customer->update([
            'cus_address1' => request('address_1'),
            'cus_address2' => request('address_2'),
            'cus_postalcode' => request('postal_code'),
            'cus_country' => request('country_id'),
            'cus_state' => request('state_id'),
            'cus_city' => request('city_id'),
            'cus_subdistrict' => request('subdistrict_id')
        ]);

        return apiResponse(200, 'success');
    }
}
