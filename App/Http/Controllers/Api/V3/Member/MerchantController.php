<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Merchant;
use App\Notifications\RetailerRequested;
use App\Notifications\RetailerRequestSent;
use Carbon\Carbon;

class MerchantController extends MobileApiController
{
    public function index()
    {
        if (!auth('api')->user()->merchant) {
            apiResponse(403, 'You are not a merchant.')->throwResponse();
        }

        $this->validate(request(), [
            'distributor' => 'nullable',
            'name' => 'nullable',
            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer|min:1'
        ]);

        $merchants = Merchant::nameContain(request('name'))
            ->isDistributor(request('distributor'))
            ->active()
            ->paginate(request('per_page'));

        return apiResponse(200, [
            'count' => $merchants->total(),
            'total_page' => $merchants->lastPage(),
            'data' => $merchants->apiData()
        ]);
    }

    public function detail()
    {
        if (!auth('api')->user()->merchant) {
            apiResponse(403, 'You are not a merchant.')->throwResponse();
        }

        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $merchant = $this->merchantValidation(request('id'));

        $retailerStatus = null;

        if (auth('api')->user() && auth('api')->user()->merchant) {
            $distributor = $merchant->retailers()->where('mer_id', auth('api')->user()->merchant->mer_id)->first();

            if ($distributor) {
                $retailerStatus = $distributor->pivot->status;
            }
        }

        return apiResponse(200, [
            'data' => [
                'merchant_id' => $merchant->mer_id,
                'name' => $merchant->merchantName(),
                'image' => null,
                'total_favourite' => 0,
                'product_sold' => $merchant->completedOrders()->count(),
                'image' => $merchant->image_url,
                'retailer_status' => $retailerStatus
            ]
        ]);
    }

    public function info()
    {
        if (!auth('api')->user()->merchant) {
            apiResponse(403, 'You are not a merchant.')->throwResponse();
        }

        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $merchant = $this->merchantValidation(request('id'));

        $retailerStatus = null;

        if (auth('api')->user() && auth('api')->user()->authMerchantUser()) {
            $distributor = $merchant->retailers()->where('mer_id', auth('api')->user()->merchant->mer_id)->first();

            if ($distributor) {
                $retailerStatus = $distributor->pivot->status;
            }
        }

        return apiResponse(200, [
            'data' => [
                'merchant_id' => $merchant->mer_id,
                'name' => $merchant->merchantName(),
                'image' => $merchant->image_url,
                'last_seen' => Carbon::now()->toDateTimeString(),
                'open_since' => $merchant->created_at->toDateTimeString(),
                'operational_hour' => '',
                'delivery_from' => !is_null($merchant->mer_city_name) ? $merchant->mer_city_name : '',
                'address' => $merchant->mer_address1 . ' ' . $merchant->mer_address2 . ' ' . $merchant->zipcode,
                'phone' => $merchant->mer_phone,
                'email' => $merchant->email,
                'retailer_status' => $retailerStatus
            ]
        ]);
    }

    public function addresses()
    {
        if (!auth('api')->user()->merchant) {
            apiResponse(403, 'You are not a merchant.')->throwResponse();
        }

        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $merchant = $this->merchantValidation(request('id'));

        return apiResponse(200, [
            'data' => [
                'merchant_id' => $merchant->mer_id,
                'name' => $merchant->merchantName(),
                'image' => null,
                'main_address' => [
                    'name' => $merchant->merchantName(),
                    'address' => $merchant->fullAddress(),
                    'phone' => $merchant->mer_phone,
                    'email' => $merchant->email
                ],
                'other_address' => $merchant->activeStores->map(function ($store) use ($merchant) {
                    return [
                        'store_id' => $store->stor_id,
                        'name' => $store->stor_name,
                        'address' => $store->getFullAddress(),
                        'phone' => $store->stor_phone,
                        'email' => $store->email ? $store->email : $merchant->email
                    ];
                })
            ]
        ]);
    }

    public function requestRetailer()
    {
        if (!auth('api')->user()->merchant) {
            apiResponse(403, 'You are not a merchant.')->throwResponse();
        }

        $this->validate(request(), [
            'merchant_id' => 'required|integer'
        ]);

        $distributor = $this->merchantValidation(request('merchant_id'));
        $merchant = auth('api')->user()->merchant;

        if ($distributor->is_distributor == false) {
            return apiResponse(403, 'This merchant id is not a distributor.');
        }

        if (! $distributor->retailers()->where('mer_id', $merchant->mer_id)->exists()) {
            $distributor->retailers()->attach($merchant->mer_id);
            $distributor->notify(new RetailerRequested($merchant));
            $merchant->notify(new RetailerRequestSent($distributor));
        }

        return apiResponse(200, trans('Request sent.'));
    }
}