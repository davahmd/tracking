<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;

use App\Models\Customer;
use App\Models\CustomerVehicleMapping;
use App\Models\Vehicle;

use App\Repositories\CustomerRepo;

class AutomobileController extends MobileApiController
{
    public function __construct(CustomerRepo $customerrepo)
    {
        $this->cusId = auth('api')->user() ? auth('api')->user()->customer->cus_id : null;
        $this->customer = $customerrepo;
    }

    public function view()
    {
        $vehicles = $this->customer->get_vehicles_by_customer_id($this->cusId);

        return apiResponse(200, [
            'data' => $vehicles->apiCustomerVehicleData(),
        ]);
    }

    public function add()
    {
        $this->validate(request(),[
            'vehicle_id' => 'required|exists:vehicles,id',
        ]);

        $this->customer->add_vehicle($this->cusId, request('vehicle_id'));

        return $this->view();
    }

    public function delete()
    {
        $this->validate(request(),[
            'vehicle_id' => 'required|exists:vehicles,id',
        ]);

        $this->customer->delete_vehicle($this->cusId, request('vehicle_id'));

        return $this->view();
    }

    public function update()
    {
        $this->validate(request(),[
            'vehicle_id' => 'required|exists:vehicles,id',
        ]);

        $this->customer->set_automobile_default($this->cusId, request('vehicle_id'));

        return $this->view();
    }
}