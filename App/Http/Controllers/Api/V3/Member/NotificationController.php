<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\MemberServiceSchedule;
use App\Models\Order;
use App\Notifications\OrderPlaced;
use App\Notifications\ServiceCancelled;
use App\Notifications\ServiceRescheduled;
use Carbon\Carbon;
use Illuminate\Notifications\Notification;

class NotificationController extends MobileApiController
{
    public function index()
    {
        $customer = auth('api')->user()->customer;

        $customer->notifications()
            ->whereNull('read_at')
            ->update([
                'read_at' => Carbon::now()
            ]);

        $notifications = $customer->notifications()
            ->latest()
            ->paginate(50);

        return apiResponse(200, [
            'count' => $notifications->total(),
            'total_page' => $notifications->lastPage(),
            'data' => $notifications->map(function ($notification) {
                $array = array_merge([
                    'id' => $notification->id,
                    'title' => eval("return " . $notification->data['trans_title'] . ";"),
                    'text' => eval("return " . $notification->data['trans_body'] . ";"),
                    'date_time' => $notification->created_at->toDateTimeString()
                ], $notification->data);

                return array_except($array, [
                    'trans_title',
                    'trans_body',
                    'event',
                    'image',
                    'link'
                ]);
            })
        ]);
    }

    public function detail()
    {
        $this->validate(request(), [
            'notification_id' => 'required|exists:notifications,id'
        ]);

        $notification = auth('api')->user()->customer->notifications()->find(request('notification_id'));

        $data = $array = array_merge([
            'id' => $notification->id,
            'title' => eval("return " . $notification->data['trans_title'] . ";"),
            'text' => eval("return " . $notification->data['trans_body'] . ";"),
            'date_time' => $notification->created_at->toDateTimeString()
        ], $notification->data);

        return apiResponse(200, [
            'message' => 'success',
            'data' => array_except($data, [
                'trans_title',
                'trans_body',
                'event',
                'image',
                'link'
            ])
        ]);
    }

    public function unreadCount()
    {
        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'unread_count' => auth('api')
                    ->user()
                    ->customer
                    ->unreadNotifications()
                    ->count()
            ]
        ]);
    }

    public function test()
    {
        $this->validate(request(), [
            'type' => 'required|in:transaction,service'
        ]);

        if (request('type') == 'service') {
            $service = MemberServiceSchedule::inRandomOrder()->limit(1)->get();

            auth('api')->user()->customer->notify(new ServiceCancelled($service));
        }
        elseif (request('type') == 'transaction') {
            $order = Order::inRandomOrder()->limit(1)->get();

            auth('api')->user()->customer->notify(new OrderPlaced($order));
        }

        return apiResponse(200, 'notification send.');
    }
}