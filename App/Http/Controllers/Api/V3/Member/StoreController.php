<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Rating;
use App\Models\Store;
use Carbon\Carbon;

class StoreController extends MobileApiController
{
    public function index()
    {
        $this->validate(request(), [
            'name' => 'nullable',
            'featured' => 'nullable|integer|in:0,1',
            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer|min:1'
        ]);

        $stores = Store::active()
            ->nameContain(request('name'))
            ->featured(request('featured'))
            ->paginate(request('per_page'));

        $stores->load('state');

        return apiResponse(200, [
            'count' => $stores->total(),
            'total_page' => $stores->lastPage(),
            'data' => $stores->apiData()
        ]);
    }

    public function detail()
    {
        $this->validate(request(), [
            'id' => 'required'
        ]);

        $store = $this->storeValidation(request('id'));
        $rating = stdObject($store->compiledRatings(true));

        return apiResponse(200, [
            'data' => [
                'store_id' => $store->stor_id,
                'name' => $store->stor_name,
                'image' => $store->getImageUrl(),
                'short_description' => $store->short_description,
                'long_description' => $store->long_description,
                'total_favourite' => 0,
                'product_sold' => $store->soldItems->count(),
                'rating' => [
                    'rate' => $rating->rate,
                    'max_rate' => 5,
                    'total' => $rating->total,
                    'percentage' => $rating->percentage,
                    'cumulative' => $rating->cumulative,
                ]
            ]
        ]);
    }

    public function info()
    {
        $this->validate(request(), [
            'id' => 'required'
        ]);

        $store = $this->storeValidation(request('id'));

        return apiResponse(200, [
            'data' => [
                'store_id' => $store->stor_id,
                'name' => $store->stor_name,
                'short_description' => $store->short_description,
                'image' => $store->getImageUrl(),
                'last_seen' => Carbon::now()->toDateTimeString(),
                'open_since' => $store->created_at->toDateTimeString(),
                'operational_hour' => $store->office_hour,
                'delivery_from' => !is_null($store->stor_city_name) ? $store->stor_city_name : '',
                'address' => $store->getFullAddress(),
                'phone' => $store->stor_office_number
            ]
        ]);
    }

    public function topStores()
    {
        $this->validate(request(), [
            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer|min:1',
        ]);

        $stores = Store::active()
            ->whereHas('ratings', function ($query) {
                return $query->groupBy('rating')->havingRaw('AVG(rating) >= ' . 4);
            })
            ->paginate(request('per_page'));

        return apiResponse(200, [
            'count' => $stores->total(),
            'total_page' => $stores->lastPage(),
            'data' => $stores->apiData(),
        ]);
    }

    public function reviews()
    {
        $this->validate(request(), [
            'id' => 'required|integer|exists:nm_store,stor_id',
            'rating' => 'nullable|integer|between:1,5',
            'per_page' => 'nullable|integer|min:1',
            'sort_by' => 'nullable|in:latest,oldest,rating_asc,rating_desc',
            'page' => 'nullable|integer|min:1'
        ]);

        $store = $this->storeValidation(request('id'));

        $ratings = $store
                    ->ratings()
                    ->available()
                    ->rating(request('rating'))
                    ->sortBy(request('sort_by'))
                    ->paginate(request('per_page', 30));

        return apiResponse(200, [
            'count' => $ratings->total(),
            'total_page' => $ratings->lastPage(),
            'current_page' => $ratings->currentPage(),
            'data' => $ratings->apiData(),
        ]);
    }
}