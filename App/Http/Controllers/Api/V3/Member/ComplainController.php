<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Models\Order;
use App\Models\Complain;
use App\Models\Customer;
use App\Models\ComplainSubject;
use App\Models\ComplainMessage;
use App\Http\Controllers\Api\V3\MobileApiController;

class ComplainController extends MobileApiController
{
    /**
     * Create complain.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create()
    {
        $order = new Order;
        $subject = new ComplainSubject;

        $this->validate(request(), [
            'order_id' => 'required|integer|exists:' . sprintf('%s,%s', $order->getTable(), $order->getKeyName()),
            'subject_id' => 'required|integer|exists:' . sprintf('%s,%s', $subject->getTable(), $subject->getKeyName()),
            'message' => 'required',
            'attachments.*' => 'nullable|mimes:jpg,jpeg,png|max:2000'
        ]);

        if (request()->hasFile('attachments')) {
            $attachments = request()->file('attachments');

            if (count($attachments) > Complain::MAX_UPLOAD_LIMIT) {
                return apiResponse(422, trans('localize.complain.validation.exceed_limit_attachments', [
                    'limit' => Complain::MAX_UPLOAD_LIMIT
                ]));
            }
        }

        $order = Order::find(request('order_id'));

        $customer = auth('api')->user()->customer;

        if (!$this->isMyOrder($order, $customer)) {
            return apiResponse(403, trans('localize.complain.validation.order_not_match'));
        }

        if ($order->complainables()->exists()) {
            $complain = $order->getFirstComplain()->first();
        }
        else {
            $complain = (new Complain)->addNewComplainByComplainant(
                $customer,
                $order,
                [
                    'subject_id' => request('subject_id'),
                    'message' => request('message'),
                    'is_creator' => true
                ]
            );

            if (request()->hasFile('attachments')) {
                $complain->uploadAttachments($complain, $attachments);
            }
        }

        return apiResponse(200, [
            'data' => $this->constructComplainResponse($complain)
        ]);
    }

    /**
     * Create new complain message.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createMessage()
    {
        $this->validate(request(), [
            'complain_id' => 'required|exists:' . (new Complain)->getTable() . ',id',
            'message' => 'required'
        ]);

        $complain = Complain::find(request('complain_id'));

        $customer = auth('api')->user()->customer;

        if (!$complain->isMyComplain($customer)) {
            return apiResponse(403, trans('localize.complain.validation.complain_not_match'));
        }

        $complainMessage = $complain->addNewComplainMessage($complain, $customer, [
            'message' => request('message'),
            'is_creator' => true
        ]);

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'complain_message' => $this->constructComplainMessageResponse($complainMessage),
            ]
        ]);
    }

    /**
     * Get complain details.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function view()
    {
        $this->validate(request(), [
            'complain_id' => 'required|exists:' . (new Complain)->getTable() . ',id'
        ]);

        $customer = auth('api')->user()->customer;

        $complain = Complain::find(request('complain_id'));

        if (!$complain->isMyComplain($customer)) {
            return apiResponse(403, trans('localize.complain.validation.complain_not_match'));
        }

        return apiResponse(200, [
            'data' => $this->constructComplainResponse($complain)
        ]);
    }

    /**
     * Escalated to admin.
     *
     * @param Order $order
     * @param Complain $complain
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function postEscalate()
    {
        $this->validate(request(), [
            'complain_id' => 'required|exists:' . (new Complain)->getTable() . ',id',
        ]);

        $complain = Complain::find(request('complain_id'));

        if ($complain->is_escalated) {
            return apiResponse(403, trans('localize.complain.validation.already_escalated'));
        }

        try {
            $complain->toggleEscalate();

        } catch (\Exception $e) {
            return apiResponse(500, trans('localize.internal_server_error.msg'));
        }

        return apiResponse(200, trans('localize.complain.global.success_escalated'));
    }

    /**
     * @param Order $order
     * @param Customer $customer
     * @return bool
     */
    public function isMyOrder(Order $order, Customer $customer)
    {
        if ($order->order_cus_id != $customer->getKey()) {
            return false;
        }

        return true;
    }

    public function constructComplainResponse(Complain $complain)
    {
        return [
            'id' => $complain->id,
            'is_escalated' => $complain->is_escalated ? true : false,
            'status' => $complain->status,
            'created_at' => $complain->created_at->toDateTimeString(),
            'subject' => [
                'id' => $complain->subject->id,
                'title' => $complain->subject->title,
            ],
            'messages' => $this->constructCollectionOfComplainMessagesResponse($complain),
            'attachments' => $complain->complain_images->map(function($item) {
                return $item->image_link;
            })
        ];
    }

    public function constructCollectionOfComplainMessagesResponse(Complain $complain)
    {
        return $complain->complain_messages()
            ->latest()
            ->get()
            ->map(function ($message) {
                return $this->constructComplainMessageResponse($message);
            });
    }

    public function constructComplainMessageResponse(ComplainMessage $complainMessage)
    {
        return [
            'id' => $complainMessage->id,
            'message' => $complainMessage->message,
            'is_creator' => $complainMessage->is_creator ? true : false,
            'created_at' => $complainMessage->created_at->toDateTimeString(),
            'responder' => [
                'type' => class_basename($complainMessage->responder),
                'id' => $complainMessage->responder_id,
                'name' => $complainMessage->responder_name
            ],
        ];
    }
}