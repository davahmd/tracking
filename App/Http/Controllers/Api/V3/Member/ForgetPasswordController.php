<?php

namespace App\Http\Controllers\Api\V3\Member;

use App\Http\Controllers\Api\V3\MobileApiController;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgetPasswordController extends MobileApiController
{
    use SendsPasswordResetEmails;

    protected function sendResetLinkResponse($response)
    {
        return apiResponse(200, trans('passwords.sent'));
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return apiResponse(500, trans($response));
    }
}