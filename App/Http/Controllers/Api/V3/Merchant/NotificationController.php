<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;
use Carbon\Carbon;

class NotificationController extends MobileApiController
{
    public function index()
    {
        $user = auth('api')->user()->authMerchantUser();

        $user->notifications()
            ->whereNull('read_at')
            ->update([
                'read_at' => Carbon::now()
            ]);

        $notifications = $user->notifications()
            ->latest()
            ->paginate(50);

        return apiResponse(200, [
            'count' => $notifications->total(),
            'total_page' => $notifications->lastPage(),
            'data' => $notifications->map(function ($notification) {
                $array = array_merge([
                    'id' => $notification->id,
                    'title' => eval("return " . $notification->data['trans_title'] . ";"),
                    'text' => eval("return " . $notification->data['trans_body'] . ";"),
                    'date_time' => $notification->created_at->toDateTimeString()
                ], $notification->data);

                return array_except($array, [
                    'trans_title',
                    'trans_body',
                    'event',
                    'image',
                    'link'
                ]);
            })
        ]);
    }

    public function detail()
    {
        $this->validate(request(), [
            'notification_id' => 'required|exists:notifications,id'
        ]);

        $notification = auth('api')
            ->user()
            ->authMerchantUser()
            ->notifications()
            ->find(request('notification_id'));

        $data = $array = array_merge([
            'id' => $notification->id,
            'title' => eval("return " . $notification->data['trans_title'] . ";"),
            'text' => eval("return " . $notification->data['trans_body'] . ";"),
            'date_time' => $notification->created_at->toDateTimeString()
        ], $notification->data);

        return apiResponse(200, [
            'message' => 'success',
            'data' => array_except($data, [
                'trans_title',
                'trans_body',
                'event',
                'image',
                'link'
            ])
        ]);
    }

    public function unreadCount()
    {
        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'unread_count' => auth('api')
                    ->user()
                    ->authMerchantUser()
                    ->unreadNotifications()
                    ->count()
            ]
        ]);
    }
}