<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Models\Order;
use App\Models\Complain;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\ComplainMessage;
use App\Models\ComplainSubject;
use App\Http\Controllers\Api\V3\MobileApiController;

class ComplainController extends MobileApiController
{
    /**
     * Get all complain list.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function complainList(Request $request)
    {
        $onlyMerchant = true;

        $complains = (new Complain())->showAll($onlyMerchant);

        return apiResponse(200, [
            'message' => 'success',
            'data' => $complains->isNotEmpty() ? $complains->map(function($complain) {
                return $this->constructComplainResponse($complain);
            }) : [],
            'count' => $complains->count(),
            'total_page' => $complains->currentPage(),
            'next_page' => $complains->nextPageUrl(),
            'prev_page' => $complains->previousPageUrl(),
            'current_page' => $complains->currentPage(),
            'last_page' => $complains->lastPage(),
        ]);
    }
    /**
     * Create new complain message.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createMessage()
    {
        $this->validate(request(), [
            'complain_id' => 'required|exists:' . (new Complain)->getTable() . ',id',
            'message' => 'required'
        ]);

        $complain = Complain::find(request('complain_id'));

        $merchant = auth('api')->user()->authMerchantUser();

        if (!$complain->isComplainRaiseUpForMe($merchant)) {
            return apiResponse(403, trans('localize.complain.validation.complain_not_match'));
        }

        $complainMessage = $complain->addNewComplainMessage($complain, $merchant, [
            'message' => request('message'),
            'is_creator' => false
        ]);

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'complain_message' => $this->constructComplainMessageResponse($complainMessage),
            ]
        ]);
    }

    /**
     * Get complain details.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function view()
    {
        $this->validate(request(), [
            'complain_id' => 'required|exists:' . (new Complain)->getTable() . ',id'
        ]);

        $complain = Complain::find(request('complain_id'));

        $merchant = auth('api')->user()->authMerchantUser();

        if (!$complain->isComplainRaiseUpForMe($merchant)) {
            return apiResponse(403, trans('localize.complain.validation.complain_not_match'));
        }

        return apiResponse(200, [
            'data' => $this->constructComplainResponse($complain)
        ]);
    }

    /**
     * Escalated to admin.
     *
     * @param Order $order
     * @param Complain $complain
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function postEscalate()
    {
        $this->validate(request(), [
            'complain_id' => 'required|exists:' . (new Complain)->getTable() . ',id',
        ]);

        $complain = Complain::find(request('complain_id'));

        $merchant = auth('api')->user()->authMerchantUser();

        if (!$complain->isComplainRaiseUpForMe($merchant)) {
            return apiResponse(403, trans('localize.complain.validation.complain_not_match'));
        }

        if ($complain->is_escalated) {
            return apiResponse(403, trans('localize.complain.validation.already_escalated'));
        }

        try {
            $complain->toggleEscalate();

        } catch (\Exception $e) {
            return apiResponse(500, trans('localize.internal_server_error.msg'));
        }

        return apiResponse(200, trans('localize.complain.global.success_escalated'));
    }

    public function constructComplainResponse(Complain $complain)
    {
        return [
            'id' => $complain->id,
            'is_escalated' => $complain->is_escalated ? true : false,
            'transaction_id' => $complain->complainable->transaction_id,
            'status' => $complain->status,
            'created_at' => $complain->created_at->toDateTimeString(),
            'complainant' => [
                'name' => $complain->complainant_name,
                'email' => $complain->complainant_email,
            ],
            'subject' => [
                'id' => $complain->subject->id,
                'title' => $complain->subject->title,
            ],
            'messages' => $this->constructCollectionOfComplainMessagesResponse($complain),
            'attachments' => $complain->complain_images->map(function($item) {
                return $item->image_link;
            })
        ];
    }

    public function constructCollectionOfComplainMessagesResponse(Complain $complain)
    {
        return $complain->complain_messages()
            ->latest()
            ->get()
            ->map(function ($message) {
                return $this->constructComplainMessageResponse($message);
            });
    }

    public function constructComplainMessageResponse(ComplainMessage $complainMessage)
    {
        return [
            'id' => $complainMessage->id,
            'message' => $complainMessage->message,
            'is_creator' => $complainMessage->is_creator ? true : false,
            'created_at' => $complainMessage->created_at->toDateTimeString(),
            'responder' => [
                'type' => class_basename($complainMessage->responder),
                'id' => $complainMessage->responder_id,
                'name' => $complainMessage->responder_name
            ],
        ];
    }
}