<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Merchant;
use App\Models\StoreUser;
use App\Models\User;
use Carbon\Carbon;

class AuthController extends MobileApiController
{
    public function login()
    {
        $this->validate(request(), [
            'email' => 'required|email',
            'password' => 'required',
            // 'type' => 'required|integer|in:1,2'
        ]);

        $modelProvider = $this->getAuthUser();

        if ($modelProvider instanceof Merchant) {
            $merchant = $modelProvider;
            $stores = $merchant->stores()->get();
        } elseif ($modelProvider instanceof StoreUser) {
            $merchant = $modelProvider->merchant;
            $stores = $modelProvider->stores;
        } else {
            return apiResponse(500, 'Login failed.');
        }

        $stores->load('country');

        $modelProvider->app_session = substr(str_shuffle(md5(time())), 0, 20);
        $modelProvider->app_session_date = Carbon::now()->toDateTimeString();
        $modelProvider->save();

        return apiResponse(200, [
            'status' => 200,
            'merchant_id' => $merchant->mer_id,
            'merchant_name' => $this->getMerchantFullName($merchant),
            'merchant_email' => $merchant->email,
            'merchant_currency' => $merchant->country->co_curcode,
//            'merchant_credit_balance' => $merchant->mer_vtoken,
            'stores' => $stores->map(function ($store) {
                return [
                    'id' => $store->stor_id,
                    'name' => $store->stor_name,
                    'currency' => $store->country->co_curcode,
                    'address' => $store->stor_address1 . ', ' . $store->stor_address2 . ', ' . $store->stor_zipcode . ' ' . $store->stor_city_name . ', ' . $store->name . ', ' . $store->country->co_name,
                    'phone' => $store->stor_phone,
                ];
            }),
            'app_session' => $merchant->app_session,
            'api_token' => auth()->user()->createToken('Zona Merchant App')->accessToken
        ]);
    }

    public function logout()
    {
        auth('api')->user()->token()->delete();

        return apiResponse(200, 'Logout success.');
    }

    protected function getAuthUser()
    {
        // $guard = request('type') == 1 ? 'merchants' : 'storeusers';
        //
        // if (!auth($guard)->once(request()->only('username', 'password'))) {
        //     apiResponse(403, 'Invalid credential.')->throwResponse();
        // }

        // return auth($guard)->user();

        if (!auth()->once(request()->only('email', 'password'))) {
            apiResponse(403, 'Invalid credential.')->throwResponse();
        }

        if (auth()->user()->merchant) {
            return auth()->user()->merchant;
        }
        elseif (auth()->user()->storeUser) {
            return auth()->user()->storeUser;
        }

        return null;
    }

    protected function getMerchantFullName($merchant)
    {
        $merchantName = null;
        if (isset($merchant->mer_fname)) {
            $merchantName = trim($merchant->mer_fname);
        }
        if (isset($merchant->mer_lname)) {
            $merchantName .= ' ' . trim($merchant->mer_lname);
        }

        return $merchantName;
    }
}