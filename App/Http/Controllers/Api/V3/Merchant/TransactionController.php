<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Courier;
use App\Models\Order;
use App\Services\Notifier\Notifier;
use App\Services\Order\OrderService;
use Carbon\Carbon;

class TransactionController extends MobileApiController
{
    public function index()
    {
        $this->validate(request(), [
            'status' => 'required',
            'wholesale' => 'nullable|in:0,1'
        ]);

        $authUser = auth('api')->user()->authMerchantUser();

        $orders = Order::with('product', 'product.mainImage')
            ->withInvoicesForMerchantId($authUser->mer_id)
            ->whereHasProductForStores($authUser->activeStores()->get())
            ->orderStatus(request('status'))
            ->isWholesale(request('wholesale'))
            ->latest('updated_at')
            ->paginate(50);

        return apiResponse(200, [
            'message' => 'success',
            'count' => $orders->count(),
            'total_page' => $orders->currentPage(),
            'data' => $orders->map(function ($order) {
                return [
                    'order_id' => $order->order_id,
                    'quantity' => $order->order_qty,
                    'ordered_at' => $order->created_at->toDateTimeString(),
                    'status' => $order->status(),
                    'wholesale' => $order->wholesale,
                    'invoice_no' => $order->invoices->first() ? $order->invoices->first()->tax_number : null,
                    'transaction_id' => $order->transaction_id,
                    'product' => [
                        'title' => $order->product->pro_title_en,
                        'image' => $order->product->mainImageUrl(),
                        'attributes' => $order->convertOrderAttributes()
                    ]
                ];
            })
        ]);
    }

    public function detail()
    {
        $this->validate(request(), [
            'id' => 'required'
        ]);

        $authUser = auth('api')->user()->authMerchantUser();

        $order = Order::withInvoicesForMerchantId($authUser->mer_id)
            ->whereHasProductForStores($authUser->activeStores()->get())
            ->find(request('id'));

        if (!$order) {
            return apiResponse(404, 'Transaction not found.');
        }

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'product' => [
                    'title' => $order->product->pro_title_en,
                    'image' => $order->product->mainImageUrl(),
                    'store_name' => $order->product->store->stor_name,
                    'product_price' => $order->product_price,
                    'attributes' => $order->convertOrderAttributes()
                ],
                'order_information' => [
                    'order_id' => $order->order_id,
                    'transaction_id' => $order->transaction_id,
                    'invoice_no' => $order->invoices->first() ? $order->invoices->first()->tax_number : null,
                    'quantity' => $order->order_qty,
                    'ordered_at' => $order->created_at->toDateTimeString(),
                    'shipped_at' => $order->order_shipment_date,
                    'tracking_no' => $order->order_tracking_no,
                    'status' => $order->status()
                ],
                'shipping' => $order->parentOrder->shipping_address ? [
                    'name' => ucwords($order->parentOrder->shipping_address->ship_name),
                    'address' => ucwords($order->parentOrder->shipping_address->getFullAddress()),
                    'phone' => $order->parentOrder->shipping_address->phone()
                ] : null,
                'payment_information' => [
                    'merchandise_subtotal' => $order->total_product_price,
                    'shipping_fee' => $order->total_product_shipping_fees,
                    'promo_discount' => $order->getTotalDiscount(),
                    'payment_method' => 'Credit Card - CIMB Niaga',
                    'grand_total' => $order->getGrandTotal()
                ]
            ]
        ]);
    }

    public function updateStatus()
    {
        $this->validate(request(), [
            'id' => 'required',
            'action' => 'in:2,3,5',
            'courier_id' => 'required_if:action,3|exists:' . (new Courier)->getTable() . ',id',
            'tracking_no' => 'required_if:action,3'
        ]);

        $authUser = auth('api')->user()->authMerchantUser();

        $order = Order::whereHasProductForStores($authUser->activeStores()->get())->find(request('id'));

        if (!$order) {
            return apiResponse(404, 'Transaction not found.');
        }

        if ($order->order_status >= request('action') || $order->order_status + 1 > request('action')) {
            return apiResponse(400, 'Bad request.');
        }

        // $order->updateStatus(request('action'));

        switch (request('action')) {
            case Order::STATUS_PACKAGING:
                if (empty($order->service_ids)) {
                    $order->update([
                        'order_status' => Order::STATUS_PACKAGING
                    ]);

                    (new Notifier($order))->send(Notifier::ORDER_RECEIVED);
                }
                else {
                    $order->update([
                        'order_status' => Order::STATUS_INSTALLATION
                    ]);
                }

                break;
            case Order::STATUS_SHIPPED:
                $order->update([
                    'order_status' => Order::STATUS_SHIPPED,
                    'order_courier_id' => request('courier_id'),
                    'order_tracking_no' => request('tracking_no'),
                    'order_shipment_date' => Carbon::now()
                ]);

                // notify customer via email & push notification
                (new Notifier($order))->send(Notifier::ORDER_SHIPPED);

                break;
            case Order::STATUS_FAIL_OR_CANCELED;
                $order->update([
                    'order_status' => Order::STATUS_FAIL_OR_CANCELED
                ]);

                (new OrderService)->cancelByOrder($order);

                // notify customer via email & push notification
                (new Notifier($order))->send(Notifier::ORDER_CANCELLED);
        }

        $order->update(['order_status' => request('action')]);

        return apiResponse(200, 'success');
    }

    protected function sendNotification(Order $order, $name)
    {
        (new Notifier($order))->send($name);
    }
}