<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;

class ProfileController extends MobileApiController
{
    public function updatePassword()
    {
        $this->validate(request(), [
            // 'old_password' => 'required',
            'new_password' => 'required|confirmed',
            'new_password_confirmation' => 'required'
        ]);

        $authUser = auth('api')->user();

        $authUser->update([
            'password' => bcrypt(request('new_password'))
        ]);

        return apiResponse('200', 'success');
    }
}