<?php

namespace App\Http\Controllers\Api\v3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\NegotiationRepo;

class NegotiationController extends MobileApiController
{
    public function updateNegotiation()
    {
    	$this->validate(request(), [
			'nego_id' => 'required|integer',
			'nego_price' => 'required_if:status,10|integer',
			'status' => 'required|integer',
			'final_price' => 'required|boolean',
		]);

		$merchant = auth('api')->user()->merchant;

		try {
			$negotiation = NegotiationRepo::validateMerchantPriceNegotiation(request('nego_id'), $merchant->mer_id, request('status'));
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.nego_msg.error.price_already_updated'),
			]);
		}

		try {
			$negotiation = NegotiationRepo::merchantUpdateNegotiation(request('nego_id'), $merchant->mer_id, request('status'), request('nego_price'), request('final_price'));
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.Failed'),
			]);
		}

		return apiResponse(200, [
			'message' => trans('localize.Success'),
			'data' => [
				'negotiation_id' => $negotiation->id,
			]
		]);
    }

    public function negotiationListing()
    {
    	$this->validate(request(), [
            'page' => 'nullable|integer|required',
            'per_page' => 'nullable|integer|min:1|required',
            'product_id' => 'nullable|integer',
            'sort' => 'nullable|in:latest,oldest'
        ]);

		$merchant = auth('api')->user()->merchant;

		try {
			$negotiations = NegotiationRepo::merchantNegotiationListing($merchant->mer_id, request('per_page'), request('page'), request('product_id'), request('sort') ? request('sort') : 'oldest');
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.Failed'),
			]);
		}

		return apiResponse(200, [
			'count' => $negotiations->total(),
			'total_page' => $negotiations->lastPage(),
            'data' => $negotiations->apiNegotiationListingData(),
        ]);
    }

    public function negotiationDetail()
    {
    	$this->validate(request(), [
			'negotiation_id' => 'integer|required',
		]);

		$merchant = auth('api')->user()->merchant;

		try {
			$negotiation = NegotiationRepo::merchantNegotiationDetail($merchant->mer_id, request('negotiation_id'));
			$history = NegotiationRepo::merchantNegotiationHistory($merchant->mer_id, request('negotiation_id'));
		}
		catch (\Exception $e) {
			return apiResponse(400, [
				'message' => trans('localize.Failed'),
			]);
		}

		return apiResponse(200, [
			'data' => [
				'id' => $negotiation->id,
				'session_id' => $negotiation->session_id,
				'product_name' => $negotiation->product->title,
				'quantity' => $negotiation->quantity,
				'image' => $negotiation->product->mainImageUrl(),
				'pricing' => [
					'id' => $negotiation->pricing->id,
					'currency_code' => config('app.currency_code'),
					'price' => $negotiation->pricing->getPurchasePrice(),
					'attributes' => $negotiation->pricing->convertProductAttributes(),
				],
				'status' => $negotiation->status,
				'negotiation' => $history->apiNegotiationHistoryData(),
			]
		]);
    }
}
