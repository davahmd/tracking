<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Country;
use App\Models\State;
use Carbon\Carbon;

class StoreController extends MobileApiController
{
    public function index()
    {
        $authUser = auth('api')->user()->authMerchantUser();

        $stores = $authUser->activeStores()->paginate(50);

        return apiResponse(200, [
            'message' => 'success',
            'count' => $stores->count(),
            'total_page' => $stores->currentPage(),
            'data' => $stores->map(function ($store) {
                return [
                    'id' => $store->stor_id,
                    'name' => $store->stor_name,
                    'image' => $store->getImageUrl()
                ];
            })
        ]);
    }

    public function page()
    {
        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $authUser = auth('api')->user()->authMerchantUser();
        $store = $this->storeValidation(request('id'), $authUser->mer_id);
        $rating = stdObject($store->compiledRatings(true));

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'store_id' => $store->stor_id,
                'name' => $store->stor_name,
                'short_description' => $store->short_description,
                'image' => $store->getImageUrl(),
                'total_favourite' => 23000,
                'total_product_sold' => $store->soldItems()->sum('order_qty'),
                'rating' => [
                    'rate' => $rating->rate,
                    'max_rate' => 5,
                    'total' => $rating->total,
                    'percentage' => $rating->percentage,
                    'cumulative' => $rating->cumulative,
                ]
            ],
        ]);
    }

    public function info()
    {
        $this->validate(request(), [
            'id' => 'required|integer'
        ]);

        $authUser = auth('api')->user()->authMerchantUser();
        $store = $this->storeValidation(request('id'), $authUser->mer_id);
        $rating = stdObject($store->compiledRatings(true));

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'store_id' => $store->stor_id,
                'name' => $store->stor_name,
                'short_description' => $store->short_description,
                'image' => $store->getImageUrl(),
                'last_seen' => Carbon::now()->toDateTimeString(),
                'open_since' => $store->created_at->toDateTimeString(),
                'operational_hour' => $store->office_hour,
                'delivery_from' => !is_null($store->stor_city_name) ? $store->stor_city_name : '',
                'address' => $store->getFullAddress(),
                'phone' => $store->stor_office_number,
                'product_sold' => $store->soldItems()->sum('order_qty'),
                'rating' => [
                    'rate' => $rating->rate,
                    'max_rate' => 5,
                    'total' => $rating->total,
                    'percentage' => $rating->percentage,
                    'cumulative' => $rating->cumulative,
                ]
            ]
        ]);
    }

    public function update()
    {
        $authUser = auth('api')->user()->authMerchantUser();

        $store = $this->storeValidation(request('id'), $authUser->mer_id);

        $this->validate(request(), [
            'id' => 'required|integer',
            'address_1' => 'required|max:255',
            'postal_code' => 'required',
            'country_id' => 'required|in:' . implode(',', Country::active()->pluck('co_id')->toArray()),
            'state_id' => 'required|in:' . implode(',', State::whereCountryId(request('country_id'))->pluck('id')->toArray()),
            'city' => 'required',
            'phone_no' => 'required',
            'office_phone_no' => 'required',
            'website' => 'nullable|url',
            'image' => 'nullable|mimes:jpeg,jpg,png|max:1000'
        ]);

        // update detail
        $store->update([
            'stor_address1' => request('address_1'),
            'stor_address2' => request('address_2'),
            'stor_zipcode' => request('postal_code'),
            'stor_country' => request('country_id'),
            'stor_state' => request('state_id'),
            'stor_city_name' => request('city'),
            'stor_phone' => request('phone_no'),
            'stor_office_number' => request('office_phone_no'),
            'stor_website' => request('website'),
            'stor_latitude' => request('latitude'),
            'stor_longitude' => request('longitude'),
            'stor_metakeywords' => request('meta_keywords'),
            'stor_metadesc' => request('meta_description'),
            'short_description' => request('short_description'),
            'long_description' => request('long_description'),
            'office_hour' => request('office_hour'),
        ]);

        // update image
        if (request()->hasFile('image')) {
            $store->updateImage(request()->file('image'));
        }

        // update cover image
        if (request()->hasFile('cover_image')) {
            $store->updateCoverImage(request()->file('cover_image'));
        }

        return apiResponse(200, 'success');
    }
}