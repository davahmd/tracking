<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Merchant;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends MobileApiController
{
    use SendsPasswordResetEmails;

    // public function broker()
    // {
    //     return Password::broker('merchants');
    // }

    protected function sendResetLinkResponse($response)
    {
        return apiResponse(200, trans($response));
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return apiResponse(400, trans($response));
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|exists:' . (new User)->getTable()
        ]);

        $user = User::where('email', $request->email)->first();

        $response = $this->broker()->sendResetLink([
            'email' => $user->email
        ]);

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }
}