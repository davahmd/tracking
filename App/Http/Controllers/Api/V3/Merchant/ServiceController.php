<?php

namespace App\Http\Controllers\Api\V3\Merchant;
use App\Helpers\Helper;
use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\MemberServiceSchedule;
use App\Models\Product;
use App\Repositories\MemberServiceScheduleRepo;
use App\Repositories\OrderRepo;
use App\Services\Notifier\Notifier;
use Carbon\Carbon;

class ServiceController extends MobileApiController
{
    public function __construct(OrderRepo $orderrepo, MemberServiceScheduleRepo $memberServiceScheduleRepo)
    {
        $this->order = $orderrepo;
        $this->memberServiceSchedule = $memberServiceScheduleRepo;
    }

    public function index()
    {
        $authUser = auth('api')->user()->authMerchantUser();

        $this->validate(request(),[
            'service_status' => 'integer',
        ]);

        $services = $this->memberServiceSchedule
        ->get_details_by_mer_id($authUser->mer_id, request('service_status'))
        ->paginate(10,  request('service_status'));

        $service_arr = [];

        foreach ($services as $service) {

            $service_listing = [
                'service_id' => $service->id,
                'transaction_id' => $service->transaction_id,
                'service_name' => $service->service_name_current,
                'parent_order_id' => $service->parent_order_id,
                'order_id' => $service->order_id,
                'pro_id' => $service->pro_id,
                'product' => $service->pro_title_en,
                'product_image_link' => Product::find($service->pro_id)->mainImageUrl(),
                'product_attribute' => $service->order->convertOrderAttributes(),
                'stor_id' => $service->stor_id,
                'stor_name' => $service->stor_name,
                'service_stor_address' => Helper::getStoreAddress($service->stor_id),
                'service_stor_phone' => $service->stor_phone,
                'service_stor_office_number' => $service->stor_office_number,
                'order_qty' => $service->order_qty,
                'order_date' => $service->order_date,
                'service_schedule_time' => Carbon::parse($service->schedule_datetime)->toDateTimeString(),
                'service_timezone' => $service->timezone,
                'reschedule_count_member' => $service->reschedule_count_member,
                'reschedule_count_merchant' => $service->reschedule_count_merchant,
                'service_status' => $service->status,
            ];

            array_push($service_arr, $service_listing);

        }

        return apiResponse(200, [
            'total_page' => $services->lastPage(),
            'current_page' => $services->currentPage(),
            'data' => $service_arr,
        ]);
    }

    public function detail()
    {
        $this->validate(request(),[
            'service_id' => 'required',
        ]);

        $service = MemberServiceSchedule::find(request('service_id'));
        if(!$service)
            return apiResponse(404, [
                'message' => ["Service Id not found"],
            ]);

        $service = $this->memberServiceSchedule->get_details_by_service_id(request('service_id'));

        $slots = Carbon::parse($service->appoint_end)->diffInMinutes(Carbon::parse($service->appoint_start)) / $service->interval_period;

        $timeschedule_array = [Carbon::parse($service->appoint_start)->format('g:i A')];

        for($i=0; $i<$slots-1; $i++) {

            $timeschedule_array[$i+1] = Carbon::parse($timeschedule_array[$i])->addMinutes($service->interval_period)->format('g:i A');

        }

        $start_date = Carbon::now()->addDays($service->schedule_skip_count+1)->format('Y-m-d');

        $service_details = [
            'service_id' => $service->id,
            'transaction_id' => $service->transaction_id,
            'order_id' => $service->order_id,
            'pro_id' => $service->pro_id,
            'product' => $service->pro_title_en,
            'product_image' => $service->order->product->mainImageUrl(),
            'product_attribute' => $service->order->convertOrderAttributes(),
            'product_price' => $service->order->product_price,
            'stor_id' => $service->stor_id,
            'stor_name' => $service->stor_name,
            'service_stor_address' => Helper::getStoreAddress($service->stor_id),
            'service_stor_phone' => $service->stor_phone,
            'service_stor_office_number' => $service->stor_office_number,
            'service_name' => $service->service_name_current,
            'service_interval_time' => $service->interval_period,
            'service_appoint_start' => $service->appoint_start,
            'service_appoint_end' => $service->appoint_end,
            'service_time_slot' => $timeschedule_array,
            'service_schedule_time' => Carbon::parse($service->schedule_datetime)->toDateTimeString(),
            'service_timezone' => $service->timezone,
            'order_date' => $service->order_date,
            'schedule_skip_count' => $service->schedule_skip_count,
            'schedule_start_date' => $start_date,
            'reschedule_count_member' => $service->reschedule_count_member,
            'reschedule_count_merchant' => $service->reschedule_count_merchant,
            'service_status' => $service->status,
        ];

        return apiResponse(200, [
            'data' => $service_details,
        ]);
    }

    public function update_service_status()
    {
        $this->validate(request(),[
            'service_id' => 'required',
            'status' => 'required|integer'
        ]);

        $service = MemberServiceSchedule::find(request('service_id'));
        if(!$service)
            return apiResponse(404, [
                'message' => ["Service Id not found"],
            ]);

        $update_status = $this->memberServiceSchedule->update_member_schedule_service_status(request('service_id'), request('status'));

        if(!$update_status)
            return apiResponse(500, [
                'message' => ['fail to update service status.'],
            ]);

        $notifier = new Notifier($service);

        if (request('status') == MemberServiceSchedule::STATUS_CONFIRMED) {
            $notifier->send(Notifier::SERVICE_SCHEDULE_CONFIRM);
        }
        elseif (request('status') == MemberServiceSchedule::STATUS_CANCELLED) {
            $notifier->send(Notifier::SERVICE_CANCELLED);
        }

        return apiResponse(200, [
            'message' => [']update status sucessfully.'],
        ]);
    }

    public function schedule_service_merchant()
    {
        $this->validate(request(),[
            'service_id' => 'required',
            'schedule_datetime' => 'required|date_format:Y-m-d g:i A'
        ]);

        $service = MemberServiceSchedule::find(request('service_id'));

        if(!$service)
            return apiResponse(404, [
                'message' => ["Service Id not found"],
            ]);

        if ($service->status == '-1')
            return apiResponse(404, [
                'message' => ["Service status is cancelled and can't be scheduled "],
            ]);

        $member_service_info = MemberServiceSchedule::find(request('service_id'));
        $utc_datetime = Carbon::parse(request('schedule_datetime'))->format('Y-m-d H:i:s');

        if ($member_service_info->schedule_datetime == $utc_datetime) {
            return apiResponse(404, [
                'message' => ["This timeslot is same with the record, please pick another timeslot."],
            ]);
        }

        $schedule_service = $this->memberServiceSchedule->update_member_service_by_merchant($utc_datetime, request('service_id'));

        if ($member_service_info->reschedule_count_merchant > 2) {

            // MemberServiceScheduleRepo::update_member_service_status_cancelled(request('service_id'), -1);

            $cancel_service_status = MemberServiceScheduleRepo::update_member_service_status_cancelled(request('service_id'), -1);

            $active_status_exist = MemberServiceSchedule::where('order_id', '=', $member_service_info->order_id)->whereIn('status', array(0,1,2,3))->exists();

            if (!$active_status_exist) {

                $cancel_order_status = OrderRepo::update_order_status($member_service_info->order_id, 5);

            }

            if ($cancel_service_status && isset($cancel_order_status)) {

                return apiResponse(200, [
                    'message' => [trans('front.order.warning.schedule_order_cancel', ['schedule' => $member_service_info->service_name_current])],
                ]);

            } elseif ($cancel_service_status) {

                (new Notifier($member_service_info))->send(Notifier::SERVICE_CANCELLED);

                return apiResponse(200, [
                    'message' => [trans('front.order.warning.schedule_cancel', ['schedule' => $member_service_info->service_name_current])],
                ]);

            }
        }

        if(!$schedule_service)
            return apiResponse(500, [
                'message' => ['fail to update service.'],
            ]);

        (new Notifier($member_service_info))->send(Notifier::SERVICE_RESCHEDULED);

        return apiResponse(200, [
            'message' => ['successfully scheduled service.'],
        ]);
    }
}