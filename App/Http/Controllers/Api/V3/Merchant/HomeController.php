<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;
use App\Models\Order;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends MobileApiController
{
    public function index()
    {
        $authUser = auth('api')->user()->authMerchantUser();

        $salesRevenue = $authUser->stores()->get()->map(function ($store) {
            return [
                'revenue' => $store->soldItems()
                    ->whereMonth((new Order)->getTable() . '.created_at', Carbon::now()->month)
                    ->sum(DB::raw('nm_order.order_value - (nm_order.merchant_charge_value + nm_order.cus_service_charge_value + nm_order.cus_platform_charge_value)'))
            ];
        })->sum('revenue');

        $stores = $authUser->activeStores()->withCount(['newTransactions', 'newServices'])->get();

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'sales_revenue' => round($salesRevenue),
                'new_transactions' => $stores->sum('new_transactions_count'),
                'new_schedules' => $stores->sum('new_services_count'),
                'stores' => $stores->map(function ($store) {
                    return [
                        'id' => $store->stor_id,
                        'name' => $store->stor_name,
                        'image' => $store->getImageUrl()
                    ];
                })
            ]
        ]);
    }

    public function revenue()
    {
        $this->validate(request(), [
            'from_date' => 'required|date',
            'to_date' => 'required|date|after_or_equal:from_date'
        ]);

        $authUser = auth('api')->user()->authMerchantUser();

        $orders = Order::with('product')
            ->withInvoicesForMerchantId($authUser->mer_id)
            ->whereHasProductForStores($authUser->stores()->get())
            ->whereBetween('created_at', [
                Carbon::parse(request('from_date'))->subSecond(),
                Carbon::parse(request('to_date'))->addDay()
            ])
            ->completed()
            ->latest()
            ->paginate(50);

        return apiResponse(200, [
            'message' => 'success',
            'data' => [
                'total_revenue' => $orders->sum(function ($order) {
                    return round($order->order_value - ($order->merchant_charge_value + $order->cus_service_charge_value + $order->cus_platform_charge_value));
                }),
                'transactions' => $orders->map(function ($order) {
                    return [
                        'date' => $order->created_at->toDateTimeString(),
                        'invoice_no' => $order->invoices->first()->tax_number,
                        'transaction_id' => $order->transaction_id,
                        'product_name' => $order->product->pro_title_en,
                        'quantity' => $order->order_qty,
                        'revenue' => round($order->order_value - ($order->merchant_charge_value + $order->cus_service_charge_value + $order->cus_platform_charge_value))
                    ];
                })
            ]
        ]);
    }
}