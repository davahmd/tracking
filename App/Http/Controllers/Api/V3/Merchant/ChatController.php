<?php

namespace App\Http\Controllers\Api\V3\Merchant;

use App\Http\Controllers\Api\V3\MobileApiController;

use DateTime;
use DB;
use Storage;
use App\Events\MessageCreated;
use App\Models\Chat;
use App\Models\Customer;
use App\Models\MemberServiceSchedule;
use App\Models\Order;
use App\Models\Product;
use App\Models\Store;
use App\Services\Chat\ChatService;

class ChatController extends MobileApiController
{
    public function __construct(ChatService $chatService)
    {
        $this->chatService = $chatService;
    }

    public function list()
    {
        $merchant = auth('api')->user()->authMerchantUser();
        
        $arr_store_ids = $merchant->stores->pluck('stor_id')->toArray();

        $store_chats = Chat::whereIn('id', function($query) use ($arr_store_ids) {
            $query->from(with(new Chat)->getTable())
                    ->whereIn('store_id', $arr_store_ids)
                    ->groupBy('store_id', 'cus_id', 'pro_id')
                    ->select(DB::raw('max(id)'));
        })
        ->orderBy('created_at', 'desc')
        ->get()
        ->groupBy('store_id');

        $arr_stores = ['stores' => []];
        foreach ($store_chats as $store_id => $customer_chats) {
            // sort chats by desc before going into loop
            $customer_chats = $customer_chats->sortByDesc('id');

            $store = Store::where('stor_id', $store_id)->select(['stor_name', 'stor_img'])->first();
            $store_name = $store->stor_name;
            $store_img = asset('asset/images/logo/logo_merchant.png');
            if (Storage::exists('store/'.$store_id.'/'.$store->stor_img)) {
                $store_img = Storage::url('store/'.$store_id.'/'.$store->stor_img);
            }
            $store = [
                'id' => (int)$store_id,
                'name' => $store_name,
                'image' => $store_img,
            ];

            // group by cus_id first
            $customer_chats = $customer_chats->groupBy('cus_id')->all();

            $arr_customers = [];
            foreach ($customer_chats as $cus_id => $product_chats) {

                $customer = Customer::where('cus_id', $cus_id)->select(['cus_name', 'cus_pic'])->first();
                $customer_name = $customer->cus_name;
                $customer_img = asset('asset/images/icon/icon_account2.png');
                if (Storage::exists('gallery/customer/'.$customer->cus_pic)) {
                    $customer_img = Storage::url('gallery/customer/'.$customer->cus_pic);
                }
                $customer = [
                    'id' => (int)$cus_id,
                    'name' => $customer_name,
                    'image' => $customer_img,
                ];

                // sort chats by desc before going into loop
                $product_chats = $product_chats->sortByDesc('id');

                $arr_products = [];
                foreach ($product_chats as $product_chat) {
                    $tmp_product_id = (int)$product_chat->pro_id;
                    $tmp_product = Product::where('pro_id', $tmp_product_id)->first();
                    if (!$tmp_product) {
                        continue;
                    }

                    $product_img = asset('asset/images/product/default.jpg');
                    if (Storage::exists('product/' . $tmp_product->pro_mr_id . '/' . $tmp_product->mainImage->image)) {
                        $product_img = Storage::url('product/'.$tmp_product->pro_mr_id.'/'. $tmp_product->mainImage->image);
                    }

                    $product = [
                        // 'chat_id' => $product_chat->id,
                        'id' => $tmp_product_id,
                        'name' => $tmp_product->title,
                        'image' => $product_img,
                        'msg' => $product_chat->message,
                        'timestamp' => $product_chat->created_at->format('Y-m-d H:i:s'),
                        'sender_type' => $product_chat->sender_type,
                        'is_read' => $product_chat->sender_type == 'S' || ($product_chat->sender_type == 'U' && $product_chat->read_at) ? true : false,
                    ];
                    
                    array_push($arr_products, $product);
                    // array_push($arr_customers, $customer);
                }
                $customer['products'] = $arr_products;
                array_push($arr_customers, $customer);
            }
            $store['customers'] = $arr_customers;
            array_push($arr_stores['stores'], $store);
            }

        return apiResponse(200, $arr_stores);
    }

    public function conversation($customer_id, $product_id)
    {
        $merchant = auth('api')->user()->authMerchantUser();

        $arr_store_ids = $merchant->stores->pluck('stor_id')->toArray();

        // customer
        $tmp_customer = Customer::where('cus_id', $customer_id)
            ->select('cus_name', 'cus_pic')
            ->firstOrFail();
        $customer_img = asset('asset/images/icon/icon_account2.png');
        if (Storage::exists('gallery/customer/'.$tmp_customer->cus_pic)) {
            $customer_img = Storage::url('gallery/customer/'.$tmp_customer->cus_pic);
        }

        $customer = [
            'id' => $customer_id,
            'name' => $tmp_customer->cus_name,
            'image' => $customer_img,
        ];

        // product
        $tmp_product = Product::where('pro_id', $product_id)
            ->whereIn('pro_sh_id', $arr_store_ids)
            ->select('pro_id', 'pro_sh_id', 'pro_title_en', 'pro_title_'.app()->getLocale())
            ->firstOrFail();

        $product_img = asset('asset/images/product/default.jpg');
        if (Storage::exists('product/' . $tmp_product->pro_mr_id . '/' . $tmp_product->mainImage->image)) {
            $product_img = Storage::url('product/'.$tmp_product->pro_mr_id.'/'. $tmp_product->mainImage->image);
        }

        $product = [
            'id' => $tmp_product->pro_id,
            'name' => $tmp_product->title,
            'image' => $product_img,
        ];

        // order
        $order = Order::where([
            'order_cus_id' => $customer_id,
            'order_pro_id' => $product_id,
            ['order_status', '<=', 4],
        ])->first();

        // services
        $services = [];
        if ($order) {
            $services = $order->services()->whereNotNull('schedule_datetime')
                ->select('id', DB::raw('service_name_current name'), 'schedule_datetime', 'status')
                ->get();
                
            foreach($services as $service) {
                $service->schedule = date('j/m/y g:i A', strtotime($service->schedule_datetime));
                $service->status = MemberServiceSchedule::status()[$service->status];
                unset($service->schedule_datetime);
            }
        }

        // chat history
        $chat_query = $tmp_product->chats(['cus_id' => $customer_id]);
        $chats = $tmp_product->chats()->select('id', DB::raw('sender_type type'), DB::raw('message msg'), 'created_at')->get();

        $latest_timestamp = '';
        if ($chats->isEmpty() == false) {
            $latest_chat = $chats->sortByDesc('created_at')->first();
            $latest_timestamp = $latest_chat->created_at->format('Y-m-d H:i:s');
        }

        foreach ($chats as $chat) {
            $chat->timestamp = $chat->created_at->format('Y-m-d H:i:s');
            unset($chat->created_at);
        }

        // update these chats with store read datetime
        $chat_query->where('sender_type', 'U')
            ->whereNull('read_at')
            ->update([
                'read_at' => new DateTime
            ]);

        return apiResponse(200, compact('customer', 'product', 'services','latest_timestamp', 'chats'));
    }

    public function postMessage($customer_id, $product_id)
    {
        if (strlen(request('msg')) <= 0) {
            return;
        }

        $merchant = auth('api')->user()->authMerchantUser();

        $arr_store_ids = $merchant->stores->pluck('stor_id')->toArray();

        $product = Product::where([
            'pro_id' => $product_id,
        ])
        ->whereIn('pro_sh_id', $arr_store_ids)
        ->select(['pro_sh_id'])
        ->firstOrFail();

        $store_id = $product->pro_sh_id;

        $order = Order::where([
            'order_cus_id' => $customer_id,
            'order_pro_id' => $product_id,
        ])
        ->select(['order_id'])
        ->first();

        $order_id = null;
        if ($order) {
            $order_id = $order->order_id;
        }

        $message = request('msg');
        $message = htmlspecialchars($message);
        $message = substr($message, 0, 1000);
        
        $chat = Chat::create([
            'cus_id' => $customer_id,
            'store_id' => $store_id,
            'pro_id' => $product_id,
            'order_id' => $order_id,
            'sender_type' => 'S',
            'message' => $message
        ]);

        // update these chats with store read datetime
        Chat::where([
            'cus_id' => $customer_id,
            'pro_id' => $product_id
        ])
        ->where('sender_type', 'U')
        ->whereNull('read_at')
        ->update([
            'read_at' => new DateTime
        ]);

        broadcast(new MessageCreated($chat))->toOthers();

        return apiResponse(200, 'success');
    }

    public function unreadMessagesCount()
    {
        $unread_count = $this->chatService->getMerchantUnreadMessagesCount();

        return apiResponse(200, compact('unread_count'));
    }

    public function messagesRead($customer_id, $product_id)
    {
        // update these chats with store read datetime
        Chat::where([
            'cus_id' => $customer_id,
            'pro_id' => $product_id
        ])
        ->where('sender_type', 'U')
        ->whereNull('read_at')
        ->update([
            'read_at' => new DateTime
        ]);

        return apiResponse(200, 'success');
    }
}
