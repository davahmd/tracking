<?php

namespace App\Http\Controllers\Api\V3;

use App\Models\City;
use App\Models\ComplainSubject;
use App\Models\Courier;
use App\Models\DeviceToken;
use App\Models\PersonalAccessToken;
use App\Models\Subdistrict;
use App\Models\Vehicle;
use App\Services\Jwt\Token;

use Helper;
use Request;
use Response;
use Validator;
use DateTime;
use DateTimeZone;

use App\Models\ApiUser;
use App\Models\Category;
use App\Models\Country;
use App\Models\OfficialBrands;
use App\Models\PromotionProduct;
use App\Models\State;
use App\Models\TacVerificationLog;

use App\Repositories\BannerRepo;
use App\Repositories\CategoryRepo;
use App\Repositories\OfficialBrandsRepo;
use App\Repositories\ProductRepo;
use App\Repositories\PromotionRepo;
use App\Repositories\StoreRepo;
use App\Repositories\SmsRepo;

use App\Sms\SmsService;
use Carbon\Carbon;

class GeneralController extends MobileApiController
{
    public function __construct(BannerRepo $bannerrepo, CategoryRepo $categoryrepo, OfficialBrandsRepo $brandrepo, ProductRepo $productrepo, SmsRepo $smsrepo, StoreRepo $storerepo, PromotionRepo $promotionrepo)
    {
        $this->banner = $bannerrepo;
        $this->category = $categoryrepo;
        $this->brand = $brandrepo;
        $this->product = $productrepo;
        $this->sms = $smsrepo;
        $this->store = $storerepo;
        $this->promotion = $promotionrepo;
    }

    public function getVersion()
    {
        sanitizeApiRequest(request()->all());

        $this->validate(request(), [
            'os' => 'required|alpha|max:10|in:android,ios',
            'app' => 'required|alpha|max:10|in:member,merchant'
        ]);

        try {
            $retObj = [];

            if (request('os') == 'android' && request('app') == 'merchant') {
                $app = ApiUser::where('id', 1)->first(['android_merchant_version', 'android_merchant_build_no']);
                $retObj['data'] = [
                    'version' => $app->android_merchant_version,
                    'build_no' => $app->android_merchant_build_no
                ];
            }
            elseif (request('os') == 'android' && request('app') == 'member') {
                $app = ApiUser::where('id', 1)->first(['android_member_version', 'android_member_build_no']);
                $retObj['data'] = [
                    'version' => $app->android_member_version,
                    'build_no' => $app->android_member_build_no
                ];
            }
            elseif (request('os') == 'ios' && request('app') == 'merchant') {
                $app = ApiUser::where('id', 1)->first(['ios_merchant_version', 'ios_merchant_build_no']);
                $retObj['data'] = [
                    'version' => $app->ios_merchant_version,
                    'build_no' => $app->ios_merchant_build_no
                ];
            }
            elseif (request('os') == 'ios' && request('app') == 'member') {
                $app = ApiUser::where('id', 1)->first(['ios_member_version', 'ios_member_build_no']);
                $retObj['data'] = [
                    'version' => $app->ios_member_version,
                    'build_no' => $app->ios_member_build_no
                ];
            }
            else {
                return abort(422);
            }

            return apiResponse(200, $retObj);

        }
        catch (\Exception $e) {
            return apiResponse(500, trans('api.systemError'));
        }
    }

    public function getCountries()
    {
        sanitizeApiRequest(request()->all());

        try {
            $countries = Country::where('co_status', 1)->get();

            return apiResponse(200, [
                'data' => $countries->map(function ($country) {
                    return [
                        'id' => $country->co_id,
                        'name' => $country->co_name,
                        'code' => $country->co_code,
                        'flag' => url('web/lib/flag-icon/flags/4x3/') . '/' . strtolower($country->co_code) . '.png',
                        'phone_area_code' => $country->phone_country_code,
                    ];
                })
            ]);
        }
        catch (\Exception $e) {
            return apiResponse(500, trans('api.systemError'));
        }
    }

    public function getStates()
    {
        sanitizeApiRequest(request()->all());

        $this->validate(request(), [
            'country_id' => 'required|integer|exists:nm_country,co_id'
        ]);

        try {
            $country = Country::find(request('country_id'));

            return apiResponse(200, [
                'data' => $country->states->where('status', 1)->map(function ($state) {
                    return [
                        'id' => $state->id,
                        'name' => $state->name,
                    ];
                })
            ]);
        }
        catch (\Exception $e) {
            return apiResponse(500, trans('api.systemError'));
        }
    }

    public function getLocation()
    {
        sanitizeApiRequest(request()->all());

        try {
            if (request()->has('latitude') && request()->has('longtitude')) {
                $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" . request('latitude') . "," . request('longtitude') . "&key=" . config('app.map.google') . "&result_type=country";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = json_decode(curl_exec($ch));
                curl_close($ch);

                $countrycode = 'MY';
                if (isset($result->results[0]->address_components[0]->short_name)) {
                    $countrycode = $result->results[0]->address_components[0]->short_name;
                }

                $country = Country::where('co_status', 1)->where('co_code', $countrycode)->first();
                if (!$country) {
                    $country = Country::where('co_status', 1)->where('co_code', 'MY')->first();
                }
            }
            else {
                $ip = isset($_SERVER['HTTP_CF_CONNECTING_IP']) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : $_SERVER['REMOTE_ADDR'];
                $iplong = ip2long($ip);
                $sql = 'SELECT  c.iso_code_2 as countrycode, responded_country_id  FROM  ip2nationcountries c, ip2nation i  WHERE  i.ip < INET_ATON("' . $ip . '")  AND  c.code = i.country ORDER BY i.ip DESC LIMIT 0,1';
                $ipcountry = \DB::select($sql);

                $countryid = 1;
                if (!empty($ipcountry))
                {
                    if ($ipcountry[0]->responded_country_id != 0) {
                        $countryid = $ipcountry[0]->responded_country_id;
                    }
                }

                $country = Country::where('co_status', 1)->where('co_id', $countryid)->first();
                if (!$country) {
                    $country = Country::where('co_status', 1)->where('co_id', 5)->first();
                }
            }

            return apiResponse(200, [
                'data' => [
                    'id' => $country->co_id,
                    'name' => $country->co_name,
                    'code' => $country->co_code,
                    'flag' => url('web/lib/flag-icon/flags/4x3/') . '/' . strtolower($country->co_code) . '.png',
                    'phone_area_code' => $country->phone_country_code
                ]
            ]);
        }
        catch (\Exception $e) {
            return apiResponse(500, trans('api.systemError'));
        }
    }

    public function getCategories()
    {
        sanitizeApiRequest(request()->all());

        try {
            $cats = Category::where('status', 1)
                ->hasActiveProducts()
                ->get();

            $category = array(
                'items' => array(),
                'parents' => array()
            );

            $addNew = true;

            while ($addNew) {
                $addNew = false;
                foreach ($cats as $cat) {
                    if ($cat->parent_list) {
                        $parentArray = explode(',', $cat->parent_list);

                        foreach ($parentArray as $id) {
                            if ($cats->where('id', $id)->isEmpty()) {
                                $cats->add(Category::find($id));
                                $addNew = true;
                            }
                        }
                    }
                }
            }

            foreach ($cats as $cat) {
                $category['items'][$cat->id] = $cat;
                $category['parents'][$cat->parent_id][] = $cat->id;
            }

            return apiResponse(200, [
                'data' => $this->build_category_menu(0, $category)
            ]);
        }
        catch (\Exception $e) {
            return apiResponse(500, trans('api.systemError'));
        }
    }

    private function build_category_menu($parent, $menu)
    {
        $category = [];
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $key => $itemId) {
                    $category[$key]['id'] = $menu['items'][$itemId]->id;
                    $category[$key]['name'] = $menu['items'][$itemId]->name;
                    $category[$key]['childs'] = (isset($menu['parents'][$itemId])) ? $this->build_category_menu($itemId, $menu) : [];
                    $category[$key]['image'] = isset($menu['items'][$itemId]->image) ?\Storage::url('category/image/' . $menu['items'][$itemId]->image):null;
                    $category[$key]['banner'] = isset($menu['items'][$itemId]->banner) ?\Storage::url('category/banner/' . $menu['items'][$itemId]->banner):null;
                    $category[$key]['icon'] = isset($menu['items'][$itemId]->icon) ?\Storage::url('category/icon/' . $menu['items'][$itemId]->icon):null; //$menu['items'][$itemId]->icon;
            }
        }

        return $category;
    }

    public function getTerritory()
    {
        $this->validate(request(), [
            'country_id' => 'nullable|integer',
            'state_id' => 'nullable|integer',
            'district_id' => 'nullable|integer',
            'city_id' => 'nullable|integer',
            'sub_district_id' => 'nullable|integer'
        ]);

        if (request('country_id')) {
            $data = State::whereCountryId(request('country_id'))
                ->active()
                ->get()
                ->map(function ($state) {
                    return [
                        'id' => $state->id,
                        'name' => $state->name
                    ];
                });
        }
        elseif (request('state_id')) {
            $data = City::whereStateId(request('state_id'))
                ->active()
                ->get()
                ->map(function ($state) {
                    return [
                        'id' => $state->id,
                        'name' => $state->name
                    ];
                });
        }
        elseif (request('city_id')) {
            $data = Subdistrict::whereCityId(request('city_id'))
                ->active()
                ->get()
                ->map(function ($state) {
                    return [
                        'id' => $state->id,
                        'name' => $state->name
                    ];
                });
        }
        else {
            $data = Country::active()
//                ->with('states.cities.subdistricts')
                ->get()
                ->map(function ($country) {
                    return [
                        'id' => $country->co_id,
                        'name' => $country->co_name,
//                        'states' => $country->states->map(function ($state) {
//                            return [
//                                'id' => $state->id,
//                                'name' => $state->name,
//                                'cities' => $state->cities->map(function ($city) {
//                                    return [
//                                        'id' => $city->id,
//                                        'name' => $city->name,
//                                        'sub_districts' => $city->subdistricts->map(function ($subDistrict) {
//                                            return [
//                                                'id' => $subDistrict->id,
//                                                'name' => $subDistrict->name
//                                            ];
//                                        })
//                                    ];
//                                })
//                            ];
//                        })
                    ];
                });
        }

        return apiResponse(200, [
            'data' => $data
        ]);
    }

    public function getOfficialBrands()
    {
        $officialBrands = OfficialBrands::active()->get();

        return apiResponse(200, [
            'data' => $officialBrands->apiData()
        ]);
    }

    public function homepage()
    {
        // $data = Request::only('featured_pros_lim','trending_pros_lim','latest_pros_lim');

        // $v = Validator::make($data,[
        //     'feat_pros_lim' => 'nullable|numeric',
        //     'tren_pros_lim' => 'nullable|numeric',
        // ]);

        // if ($v->fails()) {

        //     return apiResponse(422, [
        //         'message' => $v->messages()->all(),
        //     ]);
        // }

        $banners = $this->banner->get_mobile_banners(10);
        $featured_categories = $this->category->featured_category();
        $official_brands = $this->brand->m_get_official_brands();
        $featured_stores = $this->store->get_featured_stores(10);
        $featured_products = ProductRepo::getProducts('featured', 10);
        // $trending_products = ProductRepo::getProducts('trending', 10);
        // $latest_products = ProductRepo::getProducts('latest', 10);
        $flash_products = collect(PromotionRepo::getFlashProducts(6));

        $promo_products = [];
        $promo_product_info = [];
        $flash_products_info = [];

        foreach ($flash_products as $product) {

            $promo_item = $this->promotion->new_pricing_by_promotion($product->pro_id);
            $productPrice = $product->pricing->first();

            if ($promo_item) {

                $promo_products[$product->pro_id] = $promo_item;
                $promo_product_info[$product->pro_id] = PromotionProduct::where('promotion_id', $promo_products[$product->pro_id]->id)->where('product_id', $product->pro_id)->first();
                $ended_at = $promo_products[$product->pro_id]->ended_at;
                $start  = new Carbon(Carbon::now());
                $end    = new Carbon($ended_at);
                // $time_remaining = $start->diffInHours($end) . ' hours : ' . $start->diff($end)->format('%I minutes : %S seconds');
                $time_remaining = $end->diffInSeconds($start);
                $rating = stdObject($product->compiledRatings(true));

                $flash_product = [
                    'id' => $product->pro_id,
                    'name' => $product->title,
                    'image' => $product->mainImageUrl(),
                    'currency_symbol' => $productPrice->country->co_cursymbol,
                    'display_price' => $promo_products[$product->pro_id]->discount_rate ? number_format($product->pricing->first()->price *(1-$promo_products[$product->pro_id]->discount_rate), 2, '.', '') : number_format($product->pricing->first()->price - $promo_products[$product->pro_id]->discount_value, 2, '.', ''),
                    'original_price' => $productPrice->price,
                    'discount_rate' => $promo_products[$product->pro_id]->discount_rate ? $promo_products[$product->pro_id]->discount_rate*100 ."%" : rpFormat($promo_products[$product->pro_id]->discount_value),
                    // 'remaining_item' => $productPrice->quantity,
                    'remaining_item' => $product->pro_qty,
                    'remaining_flash_item' => $promo_product_info[$product->pro_id]->limit - $promo_product_info[$product->pro_id]->count,
                    'remaining_time_flash' => $time_remaining,
                    'attributes' => json_decode($productPrice->attributes_name),
                    'rating' => [
                        'rate' => $rating->rate,
                        'max_rate' => 5,
                        'total' => $rating->total,
                        'percentage' => $rating->percentage,
                        'cumulative' => $rating->cumulative,
                    ]
                ];
            }

            array_push($flash_products_info, $flash_product);
        }

        return apiResponse(200, [
            'data' => [
                'banners' => $banners->apiData(),
                'featured_pros' => $featured_products->apiData(),
                // 'trending_pros' => $trending_products->apiData(),
                // 'latest_pros' => $latest_products->apiData(),
                'brands' => $official_brands,
                'featured_cats' => $featured_categories->apiData(),
                'featured_stores' => $featured_stores->apiData(),
                'flash_products' => $flash_products ? $flash_products_info : null
            ],
        ]);
    }

    public function requestTac()
    {
        $data = Request::only('cus_id','phone_code','phone_number','action');

        $v = Validator::make($data,[
            'cus_id' => 'required_unless:action,REGISTER_MEMBER',
            'phone_code' => 'required',
            'phone_number' => 'required',
            'action' => 'required|in:' . implode(',', $this->sms->valid_action_for_tac()),
        ]);

        if ($v->fails()) {
            return apiResponse(422, [
                'message' => $v->errors()->all(),
            ]);
        }

        $data['phone'] = $data['phone_code'] . $data['phone_number'];

        $existing_tac = $this->sms->check_tac_exist($data['phone'], $data['action'], null, null);

        if ($existing_tac) {
            $tac = $existing_tac->tac;

            $tac_log = $existing_tac;
        }
        else {
            $tac = mt_rand(100000,999999);

            $now = \Carbon\Carbon::now();
            $expired = $now->addMinutes(60)->format('Y-m-d H:i:s');

            $tac_log = TacVerificationLog::create([
                'customer_id' => $data['cus_id'],
                'tac' => $tac,
                'phone' => $data['phone'],
                'action' => $data['action'],
                'expired_at' => $expired,
                'sms_content' => trans('localize.tacsms', [
                    'tac' => $tac,
                    'action' => trans('tac.'.$data['action']),
                    'date' => date('Y-m-d H:i:s'),
                ]),
            ]);
        }

        $send_tac = SmsService::send($tac_log->phone, $tac_log->sms_content, $tac_log->tac);

        if (!$send_tac) {
            $update_tac_log = TacVerificationLog::where('id', $tac_log->id)
            ->update([
                'sms_status' => 2,
            ]);

            return [
                'status' => 0,
                'message' => trans('localize.tacfail')
            ];
        }
        else {
            $tac_log->update([
                'sms_status' => 1,
            ]);
        }

        return apiResponse(200, [
            'data' => [
                'tac' => $tac,
            ],
        ]);
    }

    public function carBrands()
    {
        return apiResponse(200, [
            'data' => Vehicle::getBrandList()
        ]);
    }

    public function storeDeviceToken()
    {
        $this->validate(request(), [
            'type' => 'required|in:member,merchant',
            'token' => 'nullable'
        ]);

        if (is_null(request('token'))) {
            return apiResponse(200, request('token'));
        }

        if (auth('api')->check()) {
            $merchant = auth('api')->user()->merchant;
            $customer = auth('api')->user()->customer;

            if (request('type') == 'merchant' && $merchant) {
                $merchant->deviceTokens()->firstOrCreate([
                    'token' => request('token')
                ]);
            } elseif (request('type') == 'member' && $customer) {
                $customer->deviceTokens()->firstOrCreate([
                    'token' => request('token')
                ]);
            }
        } else {
            DeviceToken::firstOrCreate([
                'user_id' => null,
                'user_type' => null,
                'token' => request('token')
            ]);
        }

        return apiResponse(200, 'success');
    }

    public function getCouriers()
    {
        return apiResponse(200, [
            'data' => Courier::active()
                ->orderBy('name')
                ->get()
                ->map(function ($courier) {
                return [
                    'id' => $courier->id,
                    'name' => $courier->name,
                ];
            })
        ]);
    }

    public function getComplainSubject()
    {
        return apiResponse(200, [
            'data' => ComplainSubject::orderBy('title')->get(['id', 'title'])
        ]);
    }
}