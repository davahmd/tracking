<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Controller;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\Store;
use App\Models\TaxInvoice;
use Illuminate\Http\Request;

class MobileApiController extends Controller
{
    protected function throwValidationException(Request $request, $validator)
    {
        apiResponse(422, [
            'message' => $validator->errors()->all()
        ])->throwResponse();
    }

    protected function merchantValidation($merchantId)
    {
        $merchant = $merchantId;

        if (!$merchantId instanceof Merchant) {
            $merchant = Merchant::active()->find($merchantId);
        }

        if (!$merchant) {
            apiResponse(404, 'Merchant not found.')->throwResponse();
        }

        if ($merchant->isBlocked()) {
            apiResponse(400, 'Merchant has been blocked.')->throwResponse();
        }

        return $merchant;
    }

    protected function storeValidation($storeId, $merchantId = null)
    {
        $store = $storeId;

        if (!$storeId instanceof Store) {
            $store = Store::active()->find($storeId);
        }

        if (!$store) {
            apiResponse(404, 'Store not found.')->throwResponse();
        }

        if ($merchantId && $store->stor_merchant_id != $merchantId) {
            apiResponse(400, "Store doesn't belong to this merchant")->throwResponse();
        }

        return $store;
    }

    protected function productValidation($productId)
    {
        $product = Product::find($productId);

        if (!$product) {
            apiResponse(404, 'Product not found.')->throwResponse();
        }

        if ($product->pro_status != 1) {
            apiResponse(404, 'Product is incomplete/block.')->throwResponse();
        }
    }
}