<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Repositories\CountryRepo;
use App\Repositories\ProductRepo;
use App\Models\ProductPricing;
use App\Models\Store;
use App\Models\Category;
use App\Models\Product;
use Validator;
use Request;
use Response;

class ProductController extends Controller
{
    protected $cashVoucerCategoryID;
    protected $entityCardCategoryID;

    public function __construct()
    {
        $this->cashVoucerCategoryID = env('CASHVOUCHER_CATEGORY_ID');
        $this->entityCardCategoryID = env('ENTITYCARD_CATEGORY_ID');

        $this->countries = 'my,cn,hk,sq,id,tw';
        $this->country_code = 'my';
        $this->country = null;
        $this->lang = "en";

        $this->attributes_name = [
            'country_code' => trans('api.country_code'),
            'size' => trans('api.size'),
            'sort' => trans('api.sort'),
            'search' => trans('api.search'),
            'product_id' => trans('api.productId'),
            'pricing_id' => trans('api.pricingId'),
            'product_type' => trans('api.product.type'),
        ];
    }

    public function listing()
    {
        $data = Request::only('lang', 'country_code', 'range', 'page', 'size', 'sort', 'search','product_type');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $v = Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
            'size' => 'nullable|integer',
            'page' => 'nullable|integer',
            'sort' => 'nullable|in:new,name_asc,name_desc,price_asc,price_desc',
            'search' => 'nullable',
            'product_type' => 'required|in:cash_voucher,entity_card,electronic_card',
        ]);

        if ($v->fails()) {
            return Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ], 422);
        }

        // get country by country code
        $data = $this->set_country($data);

        $product_type = null;
        $category_id = null;

        switch ($data['product_type']) {
            case 'cash_voucher':
                $category_id = $this->get_product_category($this->cashVoucerCategoryID);
                break;

            case 'entity_card':
                $category_id = $this->get_product_category($this->entityCardCategoryID);
                break;

            case 'electronic_card':
                $product_type = 4;
                break;

            default:
                return \Response::json([
                    'status' => 422,
                    'message' => trans('validation.in', ['attribute' => trans('api.product.type')])
                ]);
                break;
        }

        // set default parameter
        $size = $data['size']? $data['size'] : '15';
        $sort = $data['sort']? $data['sort'] : 'new';
        $search = $data['search']? $data['search'] : null;

        try {

            $listings = ProductPricing::product_in_grid($this->country->co_id, $category_id, null, null, $size, null, null, $sort, $search, $product_type, true);

            return Response::json([
                'status' => 200,
                'message' => trans('api.successRetrieve'),
                'total' => $listings->total(),
                'current_page' => $listings->currentPage(),
                'total_pages' => $listings->lastPage(),
                'data' => $this->render_product_listing($listings),
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.failRetrieve')
            ]);
        }
    }

    public function detail()
    {
        $data = Request::only('lang', 'country_code', 'product_id');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $v = Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
            'product_id' => 'required|integer',
        ]);

        if ($v->fails()) {
            return Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ], 422);
        }

        try {

            // get country by country code
            $data = $this->set_country($data);

            $validate = ProductRepo::validate_product($data['product_id'], $this->country->co_id);
            $product = Product::find($data['product_id']);
            $productPricings = ProductPricing::where('pro_id', $data['product_id'])->where('country_id', $this->country->co_id)->where('status', 1)->get();
            if(!$validate || !$product || $productPricings->isEmpty()) {
                return Response::json([
                    'status' => 404,
                    'message' => trans('localize.product_not_found')
                ]);
            }
            $product_type = null;
            switch ($product->pro_type) {
                case 1:
                    $product_type = trans('localize.normal_product');
                    break;

                case 2:
                    $product_type = trans('localize.coupon');
                    break;

                case 3:
                    $product_type = trans('localize.ticket');
                    break;

                case 4:
                    $product_type = trans('localize.e-card.name');
                    break;
            }

            $pricings = [];
            foreach($productPricings as $pricing) {

                $discountedRate = null;
                if($pricing->is_discounted()) {
                    $discountedRate = round(100 - ($pricing->discounted_price/$pricing->price)*100, 2);
                }

                $pricings[] = [
                    'pricing_id' => $pricing->id,
                    'quantity' => $pricing->quantity,
                    'currency_symbol' => $this->country->co_curcode,
                    'purchasing_price' => $pricing->is_discounted()? $pricing->discounted_price : $pricing->price,
                    'product_price' => $pricing->price,
                    'discounted' => $pricing->is_discounted(),
                    'discounted_rate' => $discountedRate,
                    'shipping_fees' => [
                        'type' => $this->get_shippingFees($pricing->shipping_fees_type),
                        'value' => ($pricing->shipping_fees_type > 0)? $pricing->shipping_fees : 0,
                    ],
                    'attribute' => json_decode($pricing->attributes_name, true),
                ];
            }

            return Response::json([
                'status' => 200,
                'message' => trans('api.successRetrieve'),
                'data' => [
                    'product_id' => $product->pro_id,
                    'name' => $product->title,
                    'type' => $product_type,
                    'images' => $product->images->pluck('image_path'),
                    'description' => $product->desc,
                    'category' => ($product->categories && $product->categories->isEmpty())? $product->categories->first()->name : null,
                    'options' => $pricings,
                ],
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.failRetrieve')
            ]);
        }
    }

    public function featured()
    {
        $data = Request::only('lang', 'country_code');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $v = Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
        ]);

        if ($v->fails()) {
            return Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ], 422);
        }

        try {

            $data = $this->set_country($data);

            $cashVouchers = ProductPricing::product_in_grid($this->country->co_id, $this->get_product_category($this->cashVoucerCategoryID), 1, null, null, null, null, null, null, null, true)->first();
            $entityCards = ProductPricing::product_in_grid($this->country->co_id, $this->get_product_category($this->entityCardCategoryID), 1, null, null, null, null, null, null, null, true)->first();
            $eCards = ProductPricing::product_in_grid($this->country->co_id, null, 1, null, null, null, null, null, null, 4, true)->first();

            return Response::json([
                'status' => 200,
                'message' => trans('api.successRetrieve'),
                'data' => [
                    [
                        'type' => 'electronic_card',
                        'name' => trans('localize.e-card.name'),
                        'icon' => url('/assets/images/icon/icon_digital_card.png'),
                        'image' => $eCards? $this->get_product_image_path($eCards->pro_mr_id, $eCards->main_image) : null,
                    ],[
                        'type' => 'entity_card',
                        'name' => Category::find($this->entityCardCategoryID)->name,
                        'icon' => url('/assets/images/icon/icon_entity_card.png'),
                        'image' => $entityCards? $this->get_product_image_path($entityCards->pro_mr_id, $entityCards->main_image) : null,
                    ],[
                        'type' => 'cash_voucher',
                        'name' => Category::find($this->cashVoucerCategoryID)->name,
                        'icon' => url('/assets/images/icon/icon_shopping_voucher.png'),
                        'image' => $cashVouchers? $this->get_product_image_path($cashVouchers->pro_mr_id, $cashVouchers->main_image) : null,
                    ],
                ]
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.failRetrieve')
            ]);
        }
    }

    protected function set_lang($request) {
        if (isset($request['lang'])) {
            $this->lang = $request['lang'];
            \App::setLocale($request['lang']);
        }
        unset($request['lang']);

        return $request;
    }

    // if country code not give will get default country code : my
    protected function set_country($request) {

        if (isset($request['country_code'])) {
            $this->country_code = strtolower($request['country_code']);
            unset($request['country_code']);
        }

        $country = CountryRepo::get_country_by_code($this->country_code);
        if($country) {
            $this->country = $country;
        }

        return $request;
    }

    protected function get_product_category($category_id = null, $product_id = null) {

        if($category_id) {
            $category = Category::where('id','=', $category_id)->first();

            $child_id = [];
            if ($category) {
                $child_id = explode(',',$category->child_list);
                array_push($child_id, $category_id);
            }

            return $child_id;
        }

        if($product_id) {
            $product = Product::find($product_id);
            return ($product->categories)? $product->categories->first() : null;
        }

    }

    protected function render_product_listing($listings)
    {
        $products = [];
        foreach ($listings as $product) {

            $discountedRate = null;
            $category = $this->get_product_category(null, $product->pro_id);
            if($product->purchase_price < $product->price) {
                $discountedRate = round(100 - ($product->purchase_price/$product->price)*100, 2);
            }

            $products[] = [
                'product_id' => $product->pro_id,
                'pricing_id' => $product->pricing_id,
                'product_name' => $product->title,
                'product_image' => $this->get_product_image_path($product->pro_mr_id, $product->main_image),
                'category' => $category? $category->name : null,
                'currency_symbol' => $this->country->co_curcode,
                'purchasing_price' => $product->purchase_price,
                'product_price' => $product->price,
                'discounted' => ($product->purchase_price < $product->price)? true : false,
                'discounted_rate' => $discountedRate,
                'shipping_fees' => [
                    'type' => $this->get_shippingFees($product->shipping_fees_type),
                    'value' => ($product->shipping_fees_type > 0)? $product->shipping_fees : 0,
                ],
            ];
        }

        return $products;
    }

    protected function get_product_image_path($merchant_id, $image)
    {
        if(!$merchant_id || !$image)
            return null;

        return env('IMAGE_DIR') . '/product/' .$merchant_id .'/'. $image;
    }

    protected function sanitize_data($data)
    {
        if(isset($data['country_code'])) {
            $data['country_code'] = strtolower($data['country_code']);
        }

        if(isset($data['lang'])) {
            $data['lang'] = strtolower($data['lang']);
            if(!in_array($data['lang'], ['en', 'cn', 'my']))
                unset($data['lang']);
        }

        return $data;
    }

    protected function get_shippingFees($type)
    {
        switch ($type) {
            case 0:
                return trans('localize.shipping_fees_free');
                break;

            case 1:
                return trans('localize.shipping_fees_product');
                break;

            case 2:
                return trans('localize.shipping_fees_transaction');
                break;

            case 3:
                return trans('localize.self_pickup');
                break;

            default:
                return null;
                break;
        }
    }
}