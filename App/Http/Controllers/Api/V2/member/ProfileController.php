<?php

namespace App\Http\Controllers\Api\V2\member;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Shipping;
use App\Repositories\CustomerRepo;
use App\Repositories\S3ClientRepo;
use Validator;

class ProfileController extends Controller
{
    protected $cus_id;

    public function __construct()
    {
        $this->lang = "en";

        if (\Auth::guard('api_members')->check()) {
            $this->cus_id = \Auth::guard('api_members')->user()->cus_id;
        }
    }

    public function update_name()
    {
        $data = \Request::only('name', 'lang');

        if (isset($data['lang'])) {
            $this->lang = $data['lang'];
            \App::setLocale($data['lang']);
        }
        unset($data['lang']);

        $validator = \Validator::make($data, [
            'name' => 'required|max:255'
        ])->setAttributeNames([
            'name' => trans('api.memberName')
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {
            Customer::where('cus_id', $this->cus_id)->update(['cus_name' => $data['name']]);
        } catch (\Exception $e) {
            return \Response::json([
                'status' => 403,
                'message' => trans('api.memberName') . trans('api.failUpdate'),
            ]);
        }

        return \Response::json([
            'status' => 200,
            'message' => trans('api.memberName') . trans('api.updated')
        ]);
    }

    public function update_address()
    {
        $data = $this->sanitize_data(\Request::only('address_id', 'name', 'address_1', 'address_2', 'city', 'state_id', 'country_id', 'postal_code', 'areacode', 'phone', 'default_address', 'lang'));

        $v = \Validator::make($data, [
            'address_id' => 'required|integer',
            'name' => 'required|max:150',
            'address_1' => 'required|max:200',
            'address_2' => 'max:200',
            'city' => 'required|max:100',
            'country_id' => 'required|integer|exists:nm_country,co_id',
            'state_id' => 'required|integer|exists:nm_state,id',
            'phone' => 'required|numeric',
            'postal_code' => 'required|max:20',
            'areacode' => 'required|exists:nm_country,phone_country_code',
            'default_address' => 'nullable|in:1,yes,on,true'
        ]);

        if ($v->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ]);
        }

        try {

            $address = Shipping::where('ship_cus_id', $this->cus_id)->where('ship_id', $data['address_id'])->where('ship_order_id', 0)->first();
            if(!$address)
            {
                return \Response::json([
                    'status' => 404,
                    'message' => trans('api.NotFound', ['type' => trans('localize.address')]),
                ]);
            }

            $isDefault = !is_null($data['default_address'])? 1 : 0;

            $address->ship_name = $data['name'];
            $address->areacode = $data['areacode'];
            $address->ship_phone = $data['phone'];
            $address->ship_address1 = $data['address_1'];
            $address->ship_address2 = $data['address_2'];
            $address->ship_state_id = $data['state_id'];
            $address->ship_country = $data['country_id'];
            $address->ship_city_name = $data['city'];
            $address->ship_postalcode = $data['postal_code'];
            $address->isdefault = $isDefault;
            $address->save();

            if($isDefault)
            {
                Shipping::where('ship_cus_id', $this->cus_id)
                ->where('ship_id', '!=', $address->ship_id)
                ->update([
                    'isdefault' => 0
                ]);
            }

            return \Response::json([
                'status' => 200,
                'message' => trans('api.successUpdate', ['type' => trans('localize.address')]),
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    public function update_avatar()
    {
        $data = \Request::only('file', 'name', 'lang');

        if (isset($data['lang'])) {
            $this->lang = $data['lang'];
            \App::setLocale($data['lang']);
        }
        unset($data['lang']);

        $validator = \Validator::make($data, [
            'file' => 'required|image|mimes:jpeg,jpg,png|max:2000'
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        $member = CustomerRepo::get_customer_by_id($this->cus_id);

        if (!$member) {
            return \Response::json([
                'status' => 403,
                'message' => trans('api.member') . trans('api.notFound'),
            ]);
        }

        $old_file_name = $member->cus_pic;
        $cus_id = $member->cus_id;

        try {
            $file = $data['file'];
            $file_name = $file->getClientOriginalName();
            $file_details = explode('.', $file_name);
            $new_file_name = $cus_id.'_'.date('YmdHis').'.'.$file_details[1];
            $path = 'avatar/'.$cus_id;
            if(@file_get_contents($file) && !S3ClientRepo::IsExisted($path, $new_file_name))
                S3ClientRepo::Upload($path, $file, $new_file_name);

            S3ClientRepo::Delete($path, $old_file_name);

            $member->cus_pic = $new_file_name;
            $member->save();
        } catch (\Exception $e) {
            return \Response::json([
                'status' => 403,
                'message' => trans('api.memberAvatar') . trans('api.failUpdate'),
            ]);
        }

        return \Response::json([
            'status' => 200,
            'message' => trans('api.memberAvatar') . trans('api.updated')
        ]);
    }

    public function update_password()
    {
        $data = \Request::only('old_password', 'password', 'password_confirmation', 'lang');

        if (isset($data['lang'])) {
            $this->lang = $data['lang'];
            \App::setLocale($data['lang']);
        }
        unset($data['lang']);

        $member = CustomerRepo::get_customer_by_id($this->cus_id);

        if (!$member) {
            return \Response::json([
                'status' => 403,
                'message' => trans('api.member') . trans('api.notFound'),
            ]);
        }

        // validate password
        \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return \Hash::check($value, current($parameters));
        });

        $validator = \Validator::make($data, [
            'password' => 'required|min:6|confirmed',
            'old_password' => 'required|min:6|old_password:'.$member->password
        ], [
            'old_password.old_password' => trans('localize.old_password_validation'),
            'old_password.required' => trans('localize.oldpasswordInput'),
            'password.required' => trans('localize.newpasswordInput'),
            'old_password.min' => trans('localize.minpassword'),
            'password.confirmed' => trans('localize.matchpassword'),
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {
            $member->password= bcrypt($data['password']);
            $member->save();
        } catch (\Exception $e) {
            return \Response::json([
                'status' => 403,
                'message' => trans('api.password') . trans('api.failUpdate'),
            ]);
        }

        return \Response::json([
            'status' => 200,
            'message' => trans('api.password') . trans('api.updated'),
        ]);
    }

    public function update_securecode()
    {
        $data = \Request::only('old_securecode', 'securecode', 'securecode_confirmation', 'lang');

        if (isset($data['lang'])) {
            $this->lang = $data['lang'];
            \App::setLocale($data['lang']);
        }
        unset($data['lang']);

        $member = CustomerRepo::get_customer_by_id($this->cus_id);

        if (!$member) {
            return \Response::json([
                'status' => 403,
                'message' => trans('api.member') . trans('api.notFound'),
            ]);
        }

        // validate secure code
        $validator = \Validator::make($data, [
            'securecode' => 'required|integer|digits:6|confirmed',
            'old_securecode' => 'required|integer|digits:6|valid_hash:'.$member->payment_secure_code
        ])->setAttributeNames([
            'securecode' => trans('api.secureCode'),
            'old_securecode' => trans('api.secureCodeOld'),
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {
            $member->payment_secure_code = \Hash::make($data['securecode']);
            $member->save();
        } catch (\Exception $e) {
            return \Response::json([
                'status' => 403,
                'message' => trans('api.secureCode') . trans('api.failUpdate'),
            ]);
        }

        return \Response::json([
            'status' => 200,
            'message' => trans('api.secureCode') . trans('api.updated'),
        ]);
    }

    public function autogenerate_securecode()
    {
        $data = \Request::only('email', 'lang');

        if (isset($data['lang'])) {
            $this->lang = $data['lang'];
            \App::setLocale($data['lang']);
        }
        unset($data['lang']);

        $validator = \Validator::make($data, [
            'email' => 'required|email|max:150',
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        $member = Customer::where('cus_id', $this->cus_id)->where('email','=',$data['email'])->first();

        if ($member) {
            $rand_securecode = mt_rand(100000, 999999);

            try {
                $member->payment_secure_code = \Hash::make($rand_securecode);
                $member->save();

                // email to member new generated secure code
                if ($member->email_verified) {
                    \Mail::send('front.emails.reset_secure_code', ['securecode' => $rand_securecode, 'email' => $member->email], function (Message $m) use ($member) {
                        $m->to($member->email, $member->cus_name)->subject('Payment Secure Code');
                    });
                }
            } catch (\Exception $e) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('api.secureCode') . trans('api.failGenerate'),
                ]);
            }

            return \Response::json([
                'status' => 200,
                'message' => trans('api.secureCode') . trans('api.generated'),
            ]);
        }

        return \Response::json([
            'status' => 403,
            'message' => trans('api.member') . trans('api.notFound'),
        ]);
    }

    public function memberInfo()
    {
        $input = \Request::only(['lang']);

        if (isset($input['lang'])) {
            App::setLocale($input['lang']);
        }
        unset($input['lang']);

        // validate member
        $member = Customer::leftJoin('nm_country', 'nm_country.co_id', '=', 'nm_customer.cus_country')->where('cus_id', trim($this->cus_id))->first();
        if (empty($member)) {
            return \Response::json([
                'status' => 404,
                'message' => trans('api.memberId') . trans('api.notFound')
            ]);
        }

        $cus_wallets = CustomerRepo::get_customer_wallet($member->cus_id);
        $wallets = [];
        foreach ($cus_wallets as $cw) {
            $wallets[$cw->wallet->name] = $cw->credit;
        }

        return \Response::json([
            'status' => 200,
            'member_id' => $member->cus_id,
            'member_name' => $member->cus_name,
            'member_avatar' => ($member->cus_pic) ? env('IMAGE_DIR') . '/avatar/' . $member->cus_id . '/' . $member->cus_pic : null,
            'member_credit_balance' => $member->v_token,
            'member_special_wallet' => $member->special_wallet,
            'member_wallet' => $wallets
        ]);
    }

    public function get_address()
    {
        $data = $this->sanitize_data(\Request::only(['lang']));

        try {

            $addresses = $this->render_shipping_address(CustomerRepo::get_customer_shipping_detail($this->cus_id), true);

            return \Response::json([
                'status' => 200,
                'message' => trans('api.successRetrieve'),
                'data' => $addresses,
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    public function get_address_detail()
    {
        $data = $this->sanitize_data(\Request::only(['lang', 'address_id']));

        $v = Validator::make($data, [
            'address_id' => 'required|integer',
        ]);

        if ($v->fails())
        {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ]);
        }

        try {

            $addresses = $this->render_shipping_address(CustomerRepo::get_customer_shipping_detail($this->cus_id, $data['address_id']));
            if(!$addresses) {
                return \Response::json([
                    'status' => 404,
                    'message' => trans('api.NotFound', ['type' => trans('localize.address')]),
                ]);
            }

            return \Response::json([
                'status' => 200,
                'message' => trans('api.successRetrieve'),
                'data' => $addresses,
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    public function add_address()
    {
        $data = $this->sanitize_data(\Request::only('name', 'address_1', 'address_2', 'city', 'state_id', 'country_id', 'postal_code', 'areacode', 'phone', 'default_address', 'lang'));

        $validator = \Validator::make($data, [
            'name' => 'required|max:150',
            'address_1' => 'required|max:200',
            'address_2' => 'max:200',
            'city' => 'required|max:100',
            'country_id' => 'required|integer|exists:nm_country,co_id',
            'state_id' => 'required|integer|exists:nm_state,id',
            'phone' => 'required|numeric',
            'postal_code' => 'required|max:20',
            'areacode' => 'required|exists:nm_country,phone_country_code',
            'default_address' => 'nullable|in:1,yes,on,true'
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {

            $isDefault = !is_null($data['default_address'])? 1 : 0;

            $address = Shipping::create([
                'ship_name' => $data['name'],
                'ship_cus_id' => $this->cus_id,
                'areacode' => $data['areacode'],
                'ship_phone' => $data['phone'],
                'ship_address1' => $data['address_1'],
                'ship_address2' => $data['address_2'],
                'ship_city_name' => $data['city'],
                'ship_state_id' => $data['state_id'],
                'ship_country' => $data['country_id'],
                'ship_postalcode' => $data['postal_code'],
                'isdefault' => $isDefault,
            ]);

            if($isDefault)
            {
                Shipping::where('ship_cus_id', $this->cus_id)
                ->where('ship_id', '!=', $address->ship_id)
                ->update([
                    'isdefault' => 0
                ]);
            }

            return \Response::json([
                'status' => 200,
                'message' => trans('api.successCreate', ['type' => trans('localize.address')]),
                'data' => $this->render_shipping_address($address)
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    public function delete_address()
    {
        $data = $this->sanitize_data(\Request::only(['lang', 'address_id']));

        $v = Validator::make($data, [
            'address_id' => 'required|integer',
        ]);

        if ($v->fails())
        {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ]);
        }

        try {

            $addresses = $this->render_shipping_address(CustomerRepo::get_customer_shipping_detail($this->cus_id, $data['address_id']));
            if(!$addresses) {
                return \Response::json([
                    'status' => 404,
                    'message' => trans('api.NotFound', ['type' => trans('localize.address')]),
                ]);
            }

            Shipping::where('ship_id', $data['address_id'])->delete();

            return \Response::json([
                'status' => 200,
                'message' => trans('api.successDelete', ['type' => trans('localize.address')]),
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    protected function render_shipping_address($addresses, $multi = false)
    {
        $results = [];
        if(!$addresses || empty($addresses))
            return $results;

        if($multi)
        {
            foreach ($addresses as $address)
            {
                $results[] = $this->render_shipping_address($address);
            }

            return $results;
        }

        $address = $addresses;
        return [
            'address_id' => $address->ship_id,
            'state_id' => $address->ship_state_id,
            'country_id' => $address->ship_country,
            'name' => $address->ship_name,
            'areacode' =>  $address->areacode,
            'phone' => $address->ship_phone,
            'telephone' => $address->areacode.$address->ship_phone,
            'address_1' => $address->ship_address1,
            'address_2' => $address->ship_address2,
            'city' => $address->ship_city_name,
            'postal_code' => $address->ship_postalcode,
            'state' => $address->state? $address->state->name : '',
            'country' => $address->country? $address->country->co_name : '',
            'default_address' => $address->isdefault? true : false,
        ];
    }

    protected function sanitize_data($request)
    {
        if(isset($request['lang'])) {
            $request['lang'] = strtolower($request['lang']);
            if(!in_array($request['lang'], ['en', 'cn', 'my'])) {
                $this->lang = $request['lang'];
                \App::setLocale($request['lang']);
            }
            unset($request['lang']);
        }

        return $request;
    }
}