<?php

namespace App\Http\Controllers\Api\V2\member;

use App\Http\Controllers\Controller;
use App\Repositories\CustomerRepo;
use App\Traits\HasTaxInvoiceDeliveryOrder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\Merchant;
use App\Models\Store;
use App\Models\OrderOffline;
use App\Models\DeliveryOrder;
use App\Models\TaxInvoice;
use Carbon\Carbon;
use Validator;

class TransactionController extends Controller
{
    use HasTaxInvoiceDeliveryOrder;

    protected $cus_id;
    protected $cashVoucerCategoryID;
    protected $entityCardCategoryID;

    public function __construct()
    {
        $this->cashVoucerCategoryID = env('CASHVOUCHER_CATEGORY_ID');
        $this->entityCardCategoryID = env('ENTITYCARD_CATEGORY_ID');
        $this->lang = "en";

        if (\Auth::guard('api_members')->check()) {
            $this->cus_id = \Auth::guard('api_members')->user()->cus_id;
        }
    }

    public function online_order_history()
    {
        $data = $this->sanitize_data(\Request::only('order_status', 'size', 'order_type', 'code_status', 'page', 'sort', 'lang', 'transaction_id'));

        $validator = \Validator::make($data, [
            'order_status' => 'nullable|array',
            'order_status.*' => 'integer|in:1,2,3,4,5,6',
            'page' => 'nullable|integer',
            'size' => 'nullable|integer',
            'sort' => 'nullable|integer|in:1,2',
            'order_type' => 'nullable|in:all,normal_product,cash_voucher,entity_card,electronic_card',
            'code_status' => 'nullable|in:all,open,expired,redeemed,cancelled,not_expired',
            'transaction_id' => 'nullable|max:15'
        ]);

        $data['page'] = (isset($data['page'])) ? $data['page'] : 1;
        $data['size'] = (isset($data['size'])) ? $data['size'] : 25;
        $data['sort'] = (isset($data['sort'])) ? $data['sort'] : 1;
        $data['order_type'] = $data['order_type'] && $data['order_type'] != 'all' ? $data['order_type'] : null;
        $data['code_status'] = !empty($data['code_status']) && $data['code_status'] != 'all'? $data['code_status'] : null;

        if($data['code_status'])
        {
            $data['order_type'] = 'electronic_card';
        }

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        $member = CustomerRepo::get_customer_by_id($this->cus_id);

        $query = (new \App\Models\ParentOrder())->newQuery();

        $query->with(['items' => function ($query) use ($data) {

            if (request()->has('order_status')) {
                $query->whereIn('order_status', request('order_status'));
            }

            if (!empty($data['order_type'])) {

                $categories = [];
                $cashVoucerCategoryID = $this->get_product_category($this->cashVoucerCategoryID);
                $entityCardCategoryID = $this->get_product_category($this->entityCardCategoryID);

                switch ($data['order_type']) {
                    case 'normal_product':

                        array_push($categories, $cashVoucerCategoryID, $entityCardCategoryID);
                        $categories = array_flatten($categories);
                        $query->where('order_type', 1);

                        $query->whereHas('product.categories', function($q) use ($categories) {
                            return $q->whereNotIn('category_id', $categories);
                        });
                        break;

                    case 'cash_voucher':

                        array_push($categories, $cashVoucerCategoryID);
                        $categories = array_flatten($categories);

                        $query->where('order_type', 1);
                        $query->whereHas('product.categories', function($q) use ($categories) {
                            $q->whereIn('category_id', $categories);
                        });
                        break;

                    case 'entity_card':

                        array_push($categories, $entityCardCategoryID);
                        $categories = array_flatten($categories);

                        $query->where('order_type', 1);
                        $query->whereHas('product.categories', function($q) use ($categories) {
                            $q->whereIn('category_id', $categories);
                        });
                        break;

                    case 'electronic_card':
                        $query->where('nm_order.order_type', 5);
                        break;
                }

            }

            //only check only if order type is electronic card
            if(!empty($data['code_status']) && $data['order_type'] && in_array($data['order_type'], ['electronic_card']))
            {
                switch ($data['code_status'])
                {
                    case 'expired':
                        $query->whereHas('generatedCode', function ($q) {
                            $q->where('status', 1)->whereNotNull('valid_to')->where('valid_to', '<=', Carbon::now('UTC'));
                        });
                        break;

                    case 'not_expired':
                        $query->whereHas('generatedCode', function ($q) {
                            $q->whereIn('status', [1,2,3])
                                ->whereRaw("CASE WHEN (generated_codes.status = 1 AND generated_codes.valid_to IS NULL) THEN 1 WHEN (generated_codes.status = 1 AND generated_codes.valid_to <= ?) THEN 0 ELSE 1 END = 1", [Carbon::now('UTC')]);
                        });
                        break;

                    case 'open':
                        $query->whereHas('generatedCode', function ($q) {
                            $q->where('status', 1)
                                ->where(function($sq) {
                                    $sq->where('valid_to', '>', Carbon::now('UTC'))
                                        ->orWhereNull('valid_to');
                                });
                        });
                        break;

                    case 'redeemed':
                        $query->whereHas('generatedCode', function ($q) {
                            $q->where('status', 2);
                        });
                        break;

                    case 'cancelled':
                        $query->whereHas('generatedCode', function ($q) {
                            $q->where('status', 3);
                        });
                        break;
                }
            }

        }, 'items.product'])
        ->where('customer_id', '=', $member->getKey())
        ->select(['id', 'transaction_id', 'date']);

        if (request()->has('transaction_id') && request('transaction_id') !== '') {
            $query->where('transaction_id', '=', request('transaction_id'));
        }

        switch (trim($data['sort'])) {
            case 2:
                $query->orderBy('id', 'ASC');
                break;

            default:
                $query->orderBy('id', 'DESC');
        }

        $orders = $query->get();

        $newCollection = collect([]);

        $orders->each(function($item) use ($newCollection) {
            if (! $item->items->isEmpty()) {
                $newCollection->push($item);
            }
        });

        $offset = ($data['page'] * $data['size']) - $data['size'];

        // Set custom pagination to result set
        $ordersPaginate = new LengthAwarePaginator(
            $newCollection->slice($offset, $data['size']),
            $newCollection->count(),
            $data['size'],
            $data['page'],
            ['path' => request()->url(), 'query' => request()->query()]
        );

        $details = $this->renderOnlineOrders($ordersPaginate, true, $data['code_status']);

        if (empty($orders)) {
            return \Response::json([
                'status' => 403,
                'message' => 'Online order history not found',
            ]);
        }

        return \Response::json([
            'status' => 200,
            'message' => trans('api.successRetrieve'),
            'count' => $ordersPaginate->total(),
            'total_pages' => $ordersPaginate->lastPage(),
            'data' => $details,
            'next_url' => $ordersPaginate->nextPageUrl(),
            'prev_url' => $ordersPaginate->previousPageUrl(),
            'current_page' => $ordersPaginate->currentPage()
        ]);
    }

    public function offline_order_history()
    {
        $data = $this->sanitize_data(\Request::only('page', 'size', 'sort', 'lang'));

        // data validation
        $niceNames = array(
            'page' => trans('api.page'),
            'size' => trans('api.size'),
        );

        $v = Validator::make($data, [
            'page' => 'required|integer',
            'size' => 'required|integer',
            'sort' => 'nullable|integer',
        ]);
        $v->setAttributeNames($niceNames);

        if ($v->fails())
        {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ]);
        }

        try {
            // Grab online order history
            $orders = OrderOffline::with('wallet')
            ->where('order_offline.cust_id', $this->cus_id)
            ->where('order_offline.status', 1);

            switch (trim($data['sort'])) {
                // Sort : Latest
                case '1':
                    $orders->orderBy('order_offline.created_at', 'desc');
                    break;
                // Sort : Highest Amount
                case '2':
                    $orders->orderBy('order_offline.order_total_token', 'desc');
                    break;
                // Sort : Lowest Amount
                case '3':
                    $orders->orderBy('order_offline.order_total_token', 'asc');
                    break;

                default:
                    $orders->orderBy('order_offline.id', 'desc');
                    break;
            }

            $orders = $orders->paginate(trim($data['size']));
        } catch (\Exception $e) {
            return \Response::json([
                'status' => 400,
                'message' => trans('api.failRetrieve')
            ]);
        }

        $details = [];
        foreach ($orders as $order) {
            $merchant = Merchant::find($order->mer_id);
            $merchantName = null;
            if (!empty($merchant->mer_fname))
                $merchantName = trim($merchant->mer_fname);
            if (!empty($merchant->mer_lname))
                $merchantName .= ' ' . trim($merchant->mer_lname);

            $wallet_type = '-';

            if ($order->wallet_id == 99) {
                $wallet_type = 'Hemma';
            } else {
                $wallet_type = ($order->wallet) ? $order->wallet->name : '-';
            }

            $details[] = [
                'order_id' => $order->id,
                'invoice_no' => $order->inv_no,
                'merchant_id' => $order->mer_id,
                'merchant_name' => $merchantName,
                'store_id' => $order->store_id,
                'store_name' => $this->getStoreNameById($order->store_id),
                'currency' => $order->currency,
                'amount' => $order->amount,
                'credit' => $order->v_token,
                'merchant_platform_charge_percentage' => $order->merchant_platform_charge_percentage,
                'merchant_platform_charge_credit' => $order->merchant_platform_charge_token,
                'customer_charge_percentage' => $order->customer_charge_percentage,
                'customer_charge_credit' => $order->customer_charge_token,
                'total' => $order->order_total_token,
                'type' => $order->type,
                'status' => $order->status,
                'remark' => $order->remark,
                'paid_date' => $order->paid_date,
                'created_date' => $order->created_at,
                'wallet_type' => $wallet_type,
            ];
        }


        if (empty($details))
            return \Response::json([
                'status' => 404,
                'message' => 'Offline order history not found',
            ]);

        return \Response::json([
            'status' => 200,
            'message' => 'Offline order history found',
            'count' => $orders->total(),
            'total_pages' => $orders->lastPage(),
            'data' => $details
        ]);
    }

    protected function sanitize_data($request)
    {
        if(isset($request['lang'])) {
            $request['lang'] = strtolower($request['lang']);
            if(!in_array($request['lang'], ['en', 'cn', 'my'])) {
                $this->lang = $request['lang'];
                \App::setLocale($request['lang']);
            }
            unset($request['lang']);
        }

        if(isset($request['expired'])) {
            $request['expired'] = strtolower($request['expired']);
        }

        if(isset($request['order_type'])) {
            $request['order_type'] = strtolower($request['order_type']);
        }

        return $request;
    }

    protected function renderOnlineOrders($orders, $multi = false, $codeStatus = null)
    {
        if ($multi) {

            $details = [];
            foreach ($orders as $parentOrder) {
                if (!$parentOrder->items->isEmpty()) {
                    $details[] = $this->renderOnlineOrders($parentOrder, false, $codeStatus);
                }
            }

            return $details;
        }

        $order = $orders;
        $invoice_ids = $orders->items->map(function ($item) {
            $item->invoice_id = $item->order_mappings->tax_invoice_id;
            return $item;
        })->pluck('invoice_id')->toArray();

        $invoices = $order->invoices->whereIn('id', $invoice_ids)->values();

        $response = [
            'transaction_id' => $order->transaction_id,
            'order_date' => $order->date,
            'invoices' => $invoices->map(function ($item) {
                return [
                    'invoice_id' => $item->id,
                    'tax_number' => $item->tax_number(),
                    'item_type' => $item->item_type(),
                    'shipment_type' => $item->shipment_type(),
                ];
            }),
            'deliveries' => $this->get_invoice_delivery_orders($invoices)->map(function($item) {
                return [
                    'do_id' => $item->id,
                    'do_number' => $item->invoice->tax_number('ON'),
                    'shipment_type' => $item->shipment_type(),
                    'detail' => $item->tracking_number? $item->tracking_number : $item->appointment_detail,
                    'company' => $item->courier ? $item->courier->name : null,
                    'link' => $item->courier ? $item->courier->link : null,
                    'shipment_date' => $item->shipment_date,
                ];
            }),
            'orders' => $order->items->map(function ($item) use ($codeStatus) {

                $serialNumber = null;

                if(in_array($item->order_type, [5])) {
                    $serialNumber = $this->render_serialNumber($item, $codeStatus);
                }

                return [
                    'total_credit' => $item->order_vtokens,
                    'status' => [
                        'code' => $item->order_status,
                        'text' => $item->status()
                    ],
                    'type' => $item->product ? $this->getProductType($item->product->pro_id) : null,
                    'remark' => $item->remarks,
                    'product' => [
                        'id' => $item->product ? $item->product->pro_id : null,
                        'name' => $item->product ? $item->product->title : null,
                        'image' => ($item->product && $item->product->images && !$item->product->images->isEmpty())? $item->product->images->first()->imagePath : null,
                    ],
                    'serial_number' => $serialNumber
                ];
            })
        ];

        return $response;
    }

    protected function getProductType($product_id)
    {
        $product = Product::find($product_id);
        if(!$product)
            return null;

        switch ($product->pro_type)
        {
            case 1:
                $product_type = trans('localize.normal_product');
                if(!$product->categories->isEmpty()) {

                    $cashVoucerCategoryID = $this->get_product_category($this->cashVoucerCategoryID);
                    $entityCardCategoryID = $this->get_product_category($this->entityCardCategoryID);

                    $category_id = $product->categories->first()->id;
                    if(in_array($category_id, $cashVoucerCategoryID))
                    {
                        $product_type = trans('localize.cash_voucher');
                    }
                    else if (in_array($category_id, $entityCardCategoryID))
                    {
                        $product_type = trans('localize.entity_card');
                    }
                }
                break;

            case 2:
                $product_type = trans('localize.coupon');
                break;

            case 3:
                $product_type = trans('localize.ticket');
                break;

            case 4:
                $product_type = trans('localize.e-card.name');
                break;

            default:
                $product_type = null;
                break;
        }

        return $product_type;
    }

    protected function get_order_type($type)
    {
        if(!$type || is_null($type))
            return null;

        switch ($type) {
            case 1:
                $order_type = trans('localize.normal_product');
                break;

            case 3:
                $order_type = trans('localize.coupon');
                break;

            case 4:
                $order_type = trans('localize.ticket');
                break;

            case 5:
                $order_type = trans('localize.e-card.name');
                break;

            default:
                $order_type = null;
                break;
        }

        return $order_type;
    }

    protected function getOrderStatus($order)
    {
        if(!$order)
            return null;

        switch ($order->order_status)
        {
            case 4:
                return trans('localize.completed');
                break;

            case 5:
                return trans('localize.cancelled');
                break;

            case 6:
                return trans('localize.refunded');
                break;
        }

        if($order->order_type == 1)
        {
            switch ($order->order_status)
            {
                case 1:
                    return trans('localize.processing');
                    break;

                case 2:
                    return trans('localize.packaging');
                    break;

                case 3:
                    if($order->product_shipping_fees_type < 3)
                        return trans('localize.shipped');

                    return trans('localize.arranged');
                    break;
            }
        }

        if(in_array($order->order_type, [3,4,5]))
        {
            switch ($order->order_status)
            {
                case 2:
                    return trans('localize.pending');
                    break;
            }
        }

        return null;
    }

    protected function render_serialNumber($order, $codeExpired = null)
    {
        $results = [];
        if(in_array($order->order_type, [4,5]))
        {
            $codes = $order->generatedCode;

            foreach ($codes as $code)
            {
                $getData = false;
                if(!$codeExpired || $codeExpired == 'all') {
                    $getData = true;
                } else {

                    if($codeExpired == 'expired' && $code->status == 1 && $code->isExpired()) {
                        $getData = true;
                    }

                    if($codeExpired == 'not_expired' && in_array($code->status, [1,2,3])) {
                        if($code->status == 1) {
                            if(!$code->isExpired())
                                $getData = true;
                        } else {
                            $getData = true;
                        }
                    }

                    if($codeExpired == 'open' && $code->status == 1 && !$code->isExpired()) {
                        $getData = true;
                    }

                    if($codeExpired == 'redeemed' && $code->status == 2) {
                        $getData = true;
                    }

                    if($codeExpired == 'cancelled' && $code->status == 3) {
                        $getData = true;
                    }
                }

                if($getData) {
                    $results[] = [
                        'serial_number' => $code->serial_number,
                        'status' => $this->getCodeStatus($code, $order->order_status),
                        'valid_from' => $code->valid_from,
                        'valid_to' => $code->valid_to,
                    ];
                }
            }
        }

        return $results;
    }

    protected function getCodeStatus($code, $order_status)
    {
        if(!$code)
            return null;

        switch ($code->status) {
            case 1:
                $order_status = trans('localize.open');
                if($code->isExpired())
                    $order_status = trans('localize.expired');

                break;

            case 2:
                $order_status = trans('localize.redeemed');
                break;

            case 3:
                $order_status = trans('localize.cancelled');
                break;

            default:
                $order_status = null;
                break;
        }

        return $order_status;
    }

    protected function get_product_category($category_id = null, $product_id = null) {

        if($category_id) {
            $category = Category::where('id','=', $category_id)->first();

            $child_id = [];
            if ($category && $category->child_list) {
                $child_id = explode(',',$category->child_list);
            }
            array_push($child_id, $category_id);

            return $child_id;
        }

        if($product_id) {
            $product = Product::find($product_id);
            return ($product->categories)? $product->categories->first() : null;
        }
    }

    private function getStoreNameById($id) {
        $store = Store::find($id);

        return ($store) ? $store->stor_name : '';
    }

    protected function get_invoice_delivery_orders($invoices)
    {
        $deliveries = collect([]);
        foreach ($invoices as $invoice) {
            if($invoice->deliveries) {
                foreach ($invoice->deliveries as $delivery) {
                    $deliveries->push($delivery);
                }
            }
        }

        return $deliveries;
    }

    public function getTaxInvoiceHTML(TaxInvoice $taxInvoice)
    {
        $userType = 'user';
        $operation = 'api';

        return $this->sendTaxInvoiceResponse(
            $taxInvoice,
            $userType,
            $operation,
            false
        );
    }

    public function getDeliveryOrderHTML(DeliveryOrder $deliveryOrder)
    {
        $userType = 'user';
        $operation = 'api';

        return $this->sendOrderDeliveriesResponse(
            $deliveryOrder,
            $userType,
            $operation
        );
    }
}