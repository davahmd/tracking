<?php

namespace App\Http\Controllers\Api\V2\member;

use App\Http\Controllers\Controller;
use App\Repositories\ProductPricingRepo;
use App\Repositories\AttributeRepo;
use App\Repositories\OrderRepo;
use App\Repositories\CountryRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\ProductRepo;
use App\Traits\OrderOnlineLogger;
use App\Models\ProductPricing;
use App\Models\Cart;
use App\Models\Wallet;
use App\Models\ParentOrder;
use Carbon\Carbon;
use Validator;
use Request;
use Response;

class CartController extends Controller
{
    protected $cus_id;
    protected $secure_code;
    protected $attributes_name;

    public function __construct()
    {
        $this->countries = 'my,cn,hk,sq,id,tw';
        $this->country_code = 'my';
        $this->country = null;
        $this->lang = "en";

        $this->attributes_name = [
            'cart_id' => trans('api.cartId'),
            'product_id' => trans('api.productId'),
            'pricing_id' => trans('api.pricingId'),
            'quantity' => trans('api.quantity'),
            'name' => trans('localize.name'),
            'address_1' => trans('localize.address1'),
            'address_2' => trans('localize.address2'),
            'city' => trans('localize.city'),
            'country_id' => trans('localize.country'),
            'state_id' => trans('localize.state'),
            'telephone' => trans('localize.phone'),
            'postal_code' => trans('localize.zipcode'),
            'country_code' => trans('api.country_code')
        ];

        if (\Auth::guard('api_members')->check()) {
            $this->cus_id = \Auth::guard('api_members')->user()->cus_id;
            $this->secure_code = \Auth::guard('api_members')->user()->payment_secure_code;
        }
    }

    //retrieve cart
    public function cart()
    {
        $data = \Request::only('lang', 'country_code');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $v = Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code'
        ]);

        if ($v->fails()) {
            return Response::json([
                'status' => 422,
                'message' => implode("\n", $v->errors()->all())
            ], 422);
        }

        try {

            $data = $this->set_country($data);

            $result = $this->get_shopping_cart();

            return \Response::json([
                'status' => 200,
                'message' => 'get carts success',
                'carts' => $result['cart_details'],
                'charges' => $result['cart_total']
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    //add product to cart
    public function add()
    {
        $data = \Request::only('product_id', 'pricing_id', 'quantity', 'country_code', 'lang');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $validator = \Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
            'product_id' => 'required|integer',
            'pricing_id' => 'required|integer',
            'quantity' => 'required|integer|min:1',
        ])->setAttributeNames($this->attributes_name);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {

            $data = $this->set_country($data);

            $cart_data = [
                'product_id' => $data['product_id'],
                'price_id' => $data['pricing_id'],
                'qty' => $data['quantity'],
                'remarks' => ''
            ];

            // Check product store accept payment or not
            $product = ProductRepo::get_product_store($data['product_id']);
            $pricing = ProductPricingRepo::get_product_pricing_by_id($data['pricing_id']);
            if(!$product || !$pricing || $pricing->pro_id != $product->pro_id) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('localize.product_not_found'),
                ]);
            }

            if ($product->accept_payment != 1) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('localize.store_not_accept_payment'),
                ]);
            }

            if ($data['quantity'] <= 0) {
                return \Response::json([
                    'status' => 422,
                    'message' => trans('validation.required', ['attribute' => trans('api.quantity')]),
                ]);
            }

            $qty = $data['quantity'];

            // get cart if already exist in cart
            $check_exist = OrderRepo::check_exist('', $this->cus_id, $cart_data);
            if($check_exist) {
                $qty += $check_exist->quantity;
            }

            // check maximum available quantity
            if($qty > $pricing->quantity) {
                return Response::json([
                    'status' => 422,
                    'message' => trans('api.cart_limit_quantity')
                ]);
            }

            $result = true;
            if ($check_exist) {
                $result = OrderRepo::update($check_exist->id, $qty);
            } else {
                $cart_token = md5(uniqid(microtime()));
                $result = OrderRepo::add_cart($cart_token, $this->cus_id, $cart_data);
            }

            if(!$result) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('api.addCart') . trans('api.fail'),
                ]);
            }

            return \Response::json([
                'status' => 200,
                'message' => trans('api.addCart') . trans('api.success'),
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    public function synchronize()
    {
        $data = \Request::only('country_code', 'lang');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $validator = \Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
        ])->setAttributeNames($this->attributes_name);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {

            $data = $this->set_country($data);

            $carts = Cart::select('temp_cart.*')
            ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'temp_cart.product_id')
            ->leftJoin('nm_product_pricing','nm_product_pricing.id','=','temp_cart.pricing_id')
            ->leftJoin('nm_store', 'nm_store.stor_id', '=', 'nm_product.pro_sh_id')
            ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
            ->where('nm_product_pricing.country_id', $this->country->co_id)
            ->where('nm_product.pro_status', 1)
            ->where('nm_product_pricing.status', 1)
            ->where('nm_store.stor_status', 1)
            ->where('nm_merchant.mer_staus', 1)
            ->where('temp_cart.cus_id', $this->cus_id)->get();

            //update cart price and attribute
            foreach ($carts as $cart) {
                $pricing = ProductPricing::find($cart->pricing_id);
                if($pricing) {
                    OrderRepo::update_cart_price($pricing, $cart->id);
                    AttributeRepo::update_cart_attribute($cart->id, $pricing->id);

                    if($cart->quantity > $pricing->quantity) {
                        OrderRepo::update($cart->id, $pricing->quantity);
                    }

                    if($pricing->quantity <= 0) {
                        OrderRepo::delete($cart->id);
                    }
                }
            }

            $result = $this->get_shopping_cart();

            return \Response::json([
                'status' => 200,
                'message' => trans('localize.cart') . trans('api.updated'),
                'carts' => $result['cart_details'],
                'charges' => $result['cart_total']
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    //update cart item
    public function update($operation)
    {
        $operations = ['quantity', 'price', 'attribute'];
        if(!in_array($operation, $operations)) {
            return \Response::json([
                'status' => 403,
                'message' => trans('localize.invalid_operation')
            ]);
        }

        $data = \Request::only('cart_id', 'quantity', 'country_code', 'lang');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $validator = \Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
            'cart_id' => 'required|integer',
            'quantity' => $operation == 'quantity'? 'required|integer|min:1' : '',
        ])->setAttributeNames($this->attributes_name);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {

            $data = $this->set_country($data);

            // Check product exist
            $cart = OrderRepo::get_cart_by_id($data['cart_id'], $this->cus_id, $this->country->co_id);
            if (!$cart) {
                return \Response::json([
                    'status' => 404,
                    'message' => trans('localize.cart') . trans('api.notFound')
                ]);
            }

            $update = false;
            switch ($operation) {
                case 'quantity':
                    // check maximum available quantity
                    if($data['quantity'] > $cart->pricing_quantity) {
                        return Response::json([
                            'status' => 422,
                            'message' => trans('validation.max.numeric', ['attribute' => strtolower(trans('localize.quantity')), 'max' => $cart->pricing_quantity])
                        ]);
                    }

                    // Update cart quantity
                    $update = OrderRepo::update($data['cart_id'], $data['quantity']);
                    break;

                case 'price':
                    $pricing = ProductPricing::find($cart->pricing_id);
                    if(!$pricing) {
                        return \Response::json([
                            'status' => 404,
                            'message' => trans('localize.pricing') . trans('api.notFound')
                        ]);
                    }

                    $update = OrderRepo::update_cart_price($pricing, $cart->id);
                    break;

                case 'attribute':
                    //update cart attribute
                    $update = AttributeRepo::update_cart_attribute($cart->id, $cart->pricing_id);
                    break;
            }

            if (!$update) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('localize.cart') . trans('api.failUpdate')
                ]);
            }

            return \Response::json([
                'status' => 200,
                'message' => trans('localize.cart') . trans('api.updated')
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    //remove product from cart
    public function delete()
    {
        $data = \Request::only('cart_id', 'country_code', 'lang');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        $validator = \Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
            'cart_id' => 'required|integer',
        ])->setAttributeNames($this->attributes_name);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {

            $data = $this->set_country($data);

            // Check cart exist
            $cart = OrderRepo::get_cart_by_id($data['cart_id'], $this->cus_id, $this->country->co_id);
            if (!$cart) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('localize.cart') . trans('api.notFound')
                ]);
            }

            // delete cart
            $result = OrderRepo::delete($data['cart_id']);
            if (!$result) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('localize.cart') . trans('api.failDelete')
                ]);
            }

            return \Response::json([
                'status' => 200,
                'message' => trans('localize.cart') . trans('api.deleted')
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    public function empty_cart()
    {
        $data = \Request::only('country_code', 'lang');
        $data = $this->sanitize_data($data);
        $data = $this->set_lang($data);

        try {

            $data = $this->set_country($data);

            // empty cart
            OrderRepo::empty_cart($this->cus_id, $this->country->co_id);
            return \Response::json([
                'status' => 200,
                'message' => trans('api.emptied')
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    public function checkout()
    {
        $data = \Request::only('address_id', 'country_code', 'lang', 'securecode');
        $data = $this->set_lang($data);

        $validator = \Validator::make($data, [
            'country_code' => 'nullable|in:'.$this->countries.'|exists:nm_country,co_code',
            'address_id' => 'required|integer',
            'securecode' => 'required|numeric|digits:6|valid_hash:'.$this->secure_code,
        ])->setAttributeNames($this->attributes_name);

        if ($validator->fails()) {
            return \Response::json([
                'status' => 422,
                'message' => implode("\n", $validator->errors()->all())
            ]);
        }

        try {

            $data = $this->set_country($data);

            //check shipping address is exist
            $address = CustomerRepo::get_customer_shipping_detail($this->cus_id, $data['address_id']);
            if(!$address) {
                return \Response::json([
                    'status' => 404,
                    'message' => trans('api.NotFound', ['type' => trans('localize.address')]),
                ]);
            }

            //check cart attribute is same with pricing attribute
            $attributes = OrderRepo::check_cart_and_pricing_attribute('', $this->cus_id, $this->country->co_id);
            if($attributes > 0) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('localize.checkouts.mismatch_attribute')
                ]);
            }

            // check credit
            $wallets = CustomerRepo::get_customer_wallet_array($this->cus_id);
            $carts_total = OrderRepo::get_carts_total('', $this->cus_id, 'credit', $this->country->co_id);

            if($carts_total && ($carts_total['from_pricing'] <= 0 || $carts_total['from_cart'] <= 0)) {
                return \Response::json([
                    'status' => 404,
                    'message' => trans('api.cartEmpty')
                ]);
            }

            foreach ($carts_total['total_by_wallet'] as $key => $total) {
                if(array_key_exists($key, $wallets)) {
                    if(round($total, 4) > round($wallets[$key]['credit'], 4)) {
                        $wallet = trans('localize.' . $wallets[$key]['name']);
                        return \Response::json([
                            'status' => 403,
                            'message' => trans('localize.insufficient_wallet_for_checkout', ['wallet_name' => $wallet])
                        ]);
                    }
                } else {
                    return \Response::json([
                        'status' => 403,
                        'message' => trans('localize.checkouts.wallet_missing')
                    ]);
                }
            }

            // check total price based on current pricing and current total price based on temp cart
            if (round($carts_total['from_pricing'], 2) > round($carts_total['from_cart'], 2)) {
                return \Response::json([
                    'status' => 403,
                    'message' => trans('localize.checkouts.price_changed')
                ]);
            }

            // Check availability of item quantity
            $items = OrderRepo::check_cart_quantity('', $this->cus_id, $this->country->co_id);
            foreach ($items as $key => $item) {
                if ($item['total_quantity_in_cart'] <= 0) {
                    $delete_item = OrderRepo::delete($item['cart_id']);
                }

                // Check product store accept payment or not
                if ($item['accept_payment'] != 1) {
                    return \Response::json([
                        'status' => 403,
                        'message' => trans('localize.store_not_accept_payment') . '(' . $item['pro_title'] . ')',
                    ]);
                }

                if($item['status'] == 0) {
                    return \Response::json([
                        'status' => 403,
                        'message' => trans('localize.checkouts.insufficient.quantity', ['name' => $item['pro_title']])
                    ]);
                }

                if($item['expired']) {
                    return \Response::json([
                        'status' => 403,
                        'message' => trans('localize.checkouts.product.expired', ['name' => $item['pro_title']])
                    ]);
                }

                if($item['exceed']['productLimit'])
                {
                    return \Response::json([
                        'status' => 403,
                        'message' => $item['exceed']['productLimit']
                    ]);
                }
            }

            do {
                $trans_id = str_random(8);
                $check_trans_id = OrderRepo::check_trans_id($trans_id);
            } while (!empty($check_trans_id));

            $country_id = $this->country->co_id;
            $customer_id = $this->cus_id;
            $payment_type = 1;

            $parentOrder = ParentOrder::create([
                'transaction_id' => $trans_id,
                'customer_id' => $customer_id,
                'date' => Carbon::now('UTC')
            ]);

            $parentOrderId = $parentOrder->id;

            $addressData = [
                'name' => $address->ship_name,
                'address_1' => $address->ship_address1,
                'address_2' => $address->ship_address2,
                'city_name' => $address->ship_city_name,
                'country_id' => $address->ship_country,
                'state_id' => $address->ship_state_id,
                'postal_code' => $address->ship_postalcode,
                'telephone' => $address->areacode.$address->ship_phone,
            ];

            $pdo = \DB::connection()->getPdo();
            $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
            $stmt = $pdo->prepare('CALL cart_checkout(?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->bindParam(1, $customer_id);
            $stmt->bindParam(2, $country_id);
            $stmt->bindParam(3, $trans_id);
            $stmt->bindParam(4, $addressData['name']);
            $stmt->bindParam(5, $addressData['address_1']);
            $stmt->bindParam(6, $addressData['address_2']);
            $stmt->bindParam(7, $addressData['city_name']);
            $stmt->bindParam(8, $addressData['country_id']);
            $stmt->bindParam(9, $addressData['postal_code']);
            $stmt->bindParam(10, $addressData['telephone']);
            $stmt->bindParam(11, $addressData['state_id']);
            $stmt->bindParam(12, $payment_type);
            $stmt->bindParam(13, $parentOrderId);
            $stmt->execute();
            $checkouts = $stmt->fetchAll(\PDO::FETCH_CLASS);
            $stmt->closeCursor();

            if ($checkouts) {

                OrderRepo::generateTaxInvoice($parentOrder->id);
                OrderRepo::generateDeliveryOrder($parentOrder->id);
                OrderOnlineLogger::log($parentOrder->id, null, $customer_id, 'customer', 'Purchased order');

                $orders = OrderRepo::get_order_by_transaction_id($trans_id);
                $total_customerPaid = round($orders->sum('order_vtokens'), 4);
                $total_platformCharge = round($orders->sum('cus_platform_charge_value'), 4);
                $total_serviceCharge = round($orders->sum('cus_service_charge_value'), 4);
                $total_shippingFees = round($orders->sum('total_product_shipping_fees_credit'), 4);
                $total_credit = round($total_customerPaid - $total_platformCharge - $total_serviceCharge - $total_shippingFees, 4);

                $wallets = CustomerRepo::get_customer_wallet_array($this->cus_id);
                $charged_wallet = [];
                foreach ($carts_total['total_by_wallet'] as $key => $total) {
                    $charged_wallet[] = [
                        'name' => $wallets[$key]['name'],
                        'charge' => round($total, 4),
                        'balance' => round($wallets[$key]['credit'], 4),
                    ];
                }

                return \Response::json([
                    'status' => 200,
                    'message' => 'Checkout Success',
                    'data' => [
                        'transaction_id' => $trans_id,
                        'total_credit' => $total_credit,
                        'total_admin_fees' => $total_platformCharge,
                        'total_gst' => $total_serviceCharge,
                        'total_shipping_fees' => $total_shippingFees,
                        'total_charge' => $total_customerPaid,
                        'member_wallet' => $charged_wallet,
                    ]
                ]);
            }

            OrderRepo::cart_log('', $customer_id, $this->country->co_id);
            return \Response::json([
                'status' => 403,
                'message' => 'Checkout Fail'
            ]);

        } catch (\Exception $e) {
            return \Response::json([
                'status' => 500,
                'message' => trans('api.systemError')
            ]);
        }
    }

    protected function set_lang($request) {
        if (isset($request['lang'])) {
            $this->lang = $request['lang'];
            \App::setLocale($request['lang']);
        }
        unset($request['lang']);

        return $request;
    }

    // if country code not give will get default country code : my
    protected function set_country($request) {

        if (isset($request['country_code'])) {
            $this->country_code = strtolower($request['country_code']);
            unset($request['country_code']);
        }

        $country = CountryRepo::get_country_by_code($this->country_code);
        if($country) {
            $this->country = $country;
        }

        return $request;
    }

    protected function sanitize_data($data)
    {
        if(isset($data['country_code'])) {
            $data['country_code'] = strtolower($data['country_code']);
        }

        if(isset($data['lang'])) {
            $data['lang'] = strtolower($data['lang']);
            if(!in_array($data['lang'], ['en', 'cn', 'my']))
                unset($data['lang']);
        }

        return $data;
    }

    protected function get_shopping_cart()
    {
        //retrieve cart by user id
        $carts = OrderRepo::get_shopping_carts(null, $this->cus_id, $this->country->co_id);

        $cart_details = [];
        $cart_total = [
            'currency_symbol' => $this->country->co_curcode,
            'total_purchasing_price' => 0.00,
            'total_credit' => 0.0000,
            'total_admin_fees' => 0.0000,
            'total_gst' => 0.0000,
            'total_shipping_fees' => [
                'price' => 0.0,
                'credit' => 0.0
            ],
            'total_charge' => 0.0000,
        ];

        if (!empty($carts)) {

            foreach ($carts as $key => $cart) {

                if($cart['pricing']) {

                    $product_quantity = $cart['cart']->quantity;

                    $price = $cart['pricing']->price;
                    if ($cart['pricing']->is_discounted())
                        $price = $cart['pricing']->discounted_price;

                    // shipping fees
                    $shippingfees = 0.00;
                    if ($cart['pricing']->shipping_fees_type == 1) {
                        $shippingfees = round(($cart['pricing']->shipping_fees * $product_quantity), 2);
                    } elseif ($cart['pricing']->shipping_fees_type == 2) {
                        $shippingfees = round(($cart['pricing']->shipping_fees), 2);
                    }
                    $shippingfees_credit = round(($shippingfees / $this->country->co_rate), 4);

                    $purchasing_price = round(($price * $product_quantity), 2);
                    $product_credit = round(($purchasing_price / $this->country->co_rate), 4);

                    $platform_charge_value = round(($product_credit * ($cart['cart']->mer_platform_charge/100)), 4);
                    $service_charge_value = round((($product_credit + $platform_charge_value) * ($cart['cart']->mer_service_charge/100)), 4);
                    $purchasing_credit = round(($product_credit + $platform_charge_value + $service_charge_value + $shippingfees_credit), 4);

                    $cart_total['total_purchasing_price'] += round($purchasing_price + $shippingfees, 2);
                    $cart_total['total_credit'] += round($product_credit, 4);
                    $cart_total['total_admin_fees'] += round($platform_charge_value, 4);
                    $cart_total['total_gst'] += round($service_charge_value, 4);
                    $cart_total['total_shipping_fees']['price'] += round($shippingfees, 2);
                    $cart_total['total_shipping_fees']['credit'] += round($shippingfees_credit, 4);
                    $cart_total['total_charge'] += round($product_credit + $platform_charge_value + $service_charge_value + $shippingfees_credit, 4);

                    $cart_details[] = [
                        'cart_id' => $cart['cart']->id,
                        'product_id' => $cart['product']->pro_id,
                        'pricing_id' => $cart['pricing']->id,
                        'product_name' => $cart['product']->pro_title_en,
                        'product_image' => env('IMAGE_DIR') . '/product/' .$cart['product']->pro_mr_id .'/'. $cart['main_image']->image,
                        'product_attribute' => json_decode($cart['cart']->attributes_name),
                        'changes' => [
                            'price' => ($cart['cart']->purchasing_price != $price)? true : false,
                            'attribute' => (json_decode($cart['pricing']->attributes) != json_decode($cart['cart']->attributes))? true : false
                        ],
                        'currency_symbol' => $this->country->co_curcode,
                        'product_price' => round($price, 2),
                        // 'product_credit' => $product_credit,
                        'quantity' => $cart['cart']->quantity,
                        'available_quantity' => $cart['pricing']->quantity,
                        'purchasing_price' => $purchasing_price,
                        // 'purchasing_credit' => $purchasing_credit,
                        // 'admin_fees' => $platform_charge_value,
                        // 'gst' => $service_charge_value,
                        'shipping_fees' => [
                            'type' => $this->get_shippingFees($cart['pricing']->shipping_fees_type),
                            'price' => round($cart['pricing']->shipping_fees, 2),
                            'total' => $shippingfees
                        ],
                        'subtotal' => round($purchasing_price + $shippingfees, 2)
                    ];
                }
            }
        }

        return $result = [
            'cart_details' => $cart_details,
            'cart_total' => $cart_total,
        ];
    }

    protected function get_shippingFees($type)
    {
        switch ($type) {
            case 0:
                return trans('localize.shipping_fees_free');
                break;

            case 1:
                return trans('localize.shipping_fees_product');
                break;

            case 2:
                return trans('localize.shipping_fees_transaction');
                break;

            case 3:
                return trans('localize.self_pickup');
                break;

            default:
                return null;
                break;
        }
    }

    private function getWalletName($id) {
        $wallet = Wallet::find($id);

        return ($wallet) ? $wallet->name : '';
    }
}