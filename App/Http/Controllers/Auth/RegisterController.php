<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Providers\ActivationServiceProvider;
use App\Models\Customer;
use App\Models\User;
use App\Repositories\CountryRepo;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActivationServiceProvider $activationService)
    {
        $this->middleware('guest');
        $this->activationService = $activationService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @param  array  $niceNames
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, array $niceNames, array $customMessages)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:6|confirmed|password',
            'password_confirmation' => 'required',
            'name' => 'required|max:255'
        ], $customMessages, $niceNames);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        try {
            $user = User::create([
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);

            $customer = $user->customer()->create([
                'cus_name' => $data['name'],
                'cus_status' => 1
            ]);

            return $user;
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function register()
    {
        $data = request()->all();

        $niceNames = [
            'email' => trans('localize.email'),
            'password' => trans('localize.password'),
            'password_confirmation' => trans('localize.confirmPassword'),
            'name' => trans('localize.name')
        ];

        $customMessages = [];

        $validator = $this->validator($data, $niceNames, $customMessages);

        // Check Validation
        if ($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }

        $user = $this->create($data);

        if ($user) {
            if ($data['email']) {
                $this->activationService->sendActivationMail($user);
            }

            return back()->with('status', trans('localize.member.registration_success'));
        }

        return back()->with('error', trans('localize.member.registration_fail'));
    }

    public function showRegistrationForm()
    {
        $countries = CountryRepo::get_all_countries();

        return view('auth.register', compact('countries'));
    }
}
