<?php

namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Merchant\Controller;

use DateTime;
use DB;
use Storage;
use App\Events\MessageCreated;
use App\Helpers\Helper;
use App\Models\Chat;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\MemberServiceSchedule;

class ChatController extends Controller
{
    public function list()
    {
        $merchant = auth()->user()->merchant;
        
        $arr_store_ids = $merchant->stores->pluck('stor_id')->toArray();

        $store_groups = Chat::whereIn('id', function($query) use ($arr_store_ids) {
            $query->from(with(new Chat)->getTable())
                    ->whereIn('store_id', $arr_store_ids)
                    ->groupBy('store_id', 'cus_id', 'pro_id')
                    ->select(DB::raw('max(id)'));
        })
        ->orderBy('created_at', 'desc')
        ->get()
        ->groupBy('store_id');

        return view('merchant.chat.chat_list', compact('store_groups'));
    }

    public function conversation($customer_id, $product_id)
    {
        $merchant = auth()->user()->merchant;

        $arr_store_ids = $merchant->stores->pluck('stor_id')->toArray();

        $product = Product::where([
            'pro_id' => $product_id,
        ])
        ->whereIn('pro_sh_id', $arr_store_ids)
        ->first();

        $chat_query = Chat::where([
            'cus_id' => $customer_id,
            'pro_id' => $product_id
        ]);
        $chats = $chat_query->get();
        
        // store & product
        $store_id = $product->pro_sh_id;
        $product_id = $product->pro_id;
        $product_title = $product->pro_title_en;
        $product_name = $product->title;
        $product_img = asset('asset/images/product/default.jpg');
        if (Storage::exists('product/' . $product->pro_mr_id . '/' . $product->mainImage->image)) {
            $product_img = Storage::url('product/'.$product->pro_mr_id.'/'. $product->mainImage->image);
        }

        // customer
        $customer = Customer::where('cus_id', $customer_id)->select('cus_name', 'cus_pic')->firstOrFail();
        $customer_name = $customer->cus_name;
        $customer_img = asset('asset/images/icon/icon_account2.png');
        if (Storage::exists('gallery/customer/'.$customer->cus_pic)) {
            $customer_img = Storage::url('gallery/customer/'.$customer->cus_pic);
        }
        
        // order
        $order = Order::where([
            'order_cus_id' => $customer_id,
            'order_pro_id' => $product_id,
            ['order_status', '<=', 4],
        ])->first();

        // services
        $services = [];
        if ($order) {
            $services = $order->services()->whereNotNull('schedule_datetime')->get();
        }

        $latest_timestamp = '';
        $latest_chat = $chats->sortByDesc('created_at')->first();
        if ($latest_chat) {
            $latest_timestamp = Helper::UTCtoTZ($latest_chat->created_at, 'j/m/y g:i A');
        }

        // update these chats with store read datetime
        $chat_query->where('sender_type', 'U')
            ->whereNull('read_at')
            ->update([
                'read_at' => new DateTime
            ]);

        return view('merchant.chat.chat', compact('customer_id', 'customer_name', 'customer_img', 'store_id', 'product_id', 'product_title', 'product_name', 'product_img', 'latest_timestamp', 'services', 'chats'));
    }

    public function postMessage($customer_id, $product_id)
    {
        $status = false;

        try {
            $merchant = auth()->user()->merchant;

            $arr_store_ids = $merchant->stores->pluck('stor_id')->toArray();

            $product = Product::where([
                'pro_id' => $product_id,
            ])
            ->whereIn('pro_sh_id', $arr_store_ids)
            ->select(['pro_sh_id'])
            ->firstOrFail();

            $store_id = $product->pro_sh_id;

            $order = Order::where([
                'order_cus_id' => $customer_id,
                'order_pro_id' => $product_id,
            ])
            ->select(['order_id'])
            ->first();

            $order_id = null;
            if ($order) {
                $order_id = $order->order_id;
            }

            $message = request('msg');
            $message = htmlspecialchars($message);
            $message = substr($message, 0, 1000);
            
            $chat = Chat::create([
                'cus_id' => $customer_id,
                'store_id' => $store_id,
                'pro_id' => $product_id,
                'order_id' => $order_id,
                'sender_type' => 'S',
                'message' => $message
            ]);

            // update these chats with store read datetime
            Chat::where([
                'cus_id' => $customer_id,
                'pro_id' => $product_id
            ])
            ->where('sender_type', 'U')
            ->whereNull('read_at')
            ->update([
                'read_at' => new DateTime
            ]);

            broadcast(new MessageCreated($chat))->toOthers();
        }
        catch (\Exception $e) {
            return response()->json(compact('status'));
        }

        $status = true;

        return response()->json(compact('status'));
    }

    public function messagesRead($customer_id, $product_id)
    {
        $status = false;

        try {
            // update these chats with store read datetime
            Chat::where([
                'cus_id' => $customer_id,
                'pro_id' => $product_id
            ])
            ->where('sender_type', 'U')
            ->whereNull('read_at')
            ->update([
                'read_at' => new DateTime
            ]);
        }
        catch (\Exception $e) {
            return response()->json(compact('status'));
        }

        $status = true;

        return response()->json(compact('status'));
    }
}
