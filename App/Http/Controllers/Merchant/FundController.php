<?php

namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Merchant\Controller;
use App\Repositories\FundRepo;
use App\Repositories\MerchantRepo;
use App\Repositories\S3ClientRepo;
use Validator;

class FundController extends Controller
{
    public function __construct(FundRepo $fundrepo, MerchantRepo $merchantrepo)
    {
        $this->fund = $fundrepo;
        $this->merchant = $merchantrepo;
    }

    public function fund_report()
	{
        $mer_id = auth()->user()->merchant->mer_id;
        $input = \Request::only('id', 'start', 'end', 'type', 'status', 'sort');
        $status_list = array(
            ''  => trans('localize.all'),
            '0' => trans('localize.pending'),
            '1' => trans('localize.approved'),
            '2' => trans('localize.declined'),
            '3' => trans('localize.paid'),
        );

        $funds = $this->fund->get_fund_transaction_details($mer_id, $input);

        return view('merchant.fund.report', compact('funds', 'status_list', 'input'));
	}

    public function fund_withdraw()
    {
        $mer_id = auth()->user()->merchant->mer_id;

        $requested = $this->fund->check_withdraw_request($mer_id);
        $merchant = $this->merchant->get_merchant_profile_details($mer_id);

        if (!$merchant->bank_acc_no || !$merchant->bank_acc_name || !$merchant->bank_name || !$merchant->bank_country) {
            return redirect('merchant/profile/edit')->with('error', trans('localize.bankInfoCompulsary'));
        }

        return view('merchant.fund.withdraw', compact('requested', 'merchant'));
	}

    public function fund_withdraw_submit()
    {
        $merchant = auth()->user()->merchant;
        $mer_id = $merchant->mer_id;

        $requested = $this->fund->check_withdraw_request($mer_id);
        if ($requested)
            return back()->withErrors(trans('localize.cantprocessfund'));

        if (\Request::isMethod('post')) {
            $data = \Request::all();

            Validator::make($data, [
                'withdraw' => 'required|numeric|between:0.0001,'.max($merchant->earning, 0),
                'file' => 'required|mimes:jpeg,jpg,png,pdf|max:2000',
            ])->validate();

            $file_name = null;
            if(!empty($data['file'])) {
                $upload_file = $data['file']->getClientOriginalName();
                $file_detail = explode('.', $upload_file);
                $file_name = time().'_'.str_random(4).'.'.$file_detail[1];
                $path = "fund/statement/$merchant->mer_id";

                if(@file_get_contents($data['file']) && !S3ClientRepo::IsExisted($path, $file_name))
                    $upload = S3ClientRepo::Upload($path, $data['file'], $file_name);
            }

            $merchant = $this->merchant->get_merchant_profile_details($mer_id);
            $balance = ($merchant->earning - ($data['withdraw']));

            $input = [
                'wd_mer_id' => $mer_id,
                'wd_total_wd_amt' => $merchant->earning,
                'wd_submited_wd_amt' => $data['withdraw'],
                'wd_balance_after' => $balance,
                'wd_admin_comm_amt' => 0, // No commission for now.
                // 'wd_currency' => $merchant->co_curcode,
                // 'wd_rate' => ($merchant->mer_type == 1) ? $merchant->co_offline_rate : $merchant->co_rate,
                'wd_statement' => $file_name,
            ];

            try {
                $this->fund->save_withdraw($input);
            } catch (\Exception $e) {
                return back()->withErrors(trans('localize.fundfailed'));
            }

            return redirect('merchant/fund/report')->with('success', trans('localize.fundsuccess'));
        }
    }
}
