<?php
namespace App\Http\Controllers\Merchant;

use App\Models\PriceNegotiation;
use App\Services\Cart\CartService;
use App\Services\Notifier\Notifier;
use Auth;

use App\Http\Controllers\Merchant\Controller;

use App\Repositories\NegotiationRepo;
use Illuminate\Support\Facades\DB;

class NegotiationController extends Controller
{
    private $mer_id;
    private $route;

    public function __construct(NegotiationRepo $negoRepo)
    {
        $this->middleware(function ($request, $next) {
            if(\Auth::guard()->user()->merchant) {
                $this->mer_id = \Auth::user()->merchant->mer_id;
                $this->logintype = 'merchants';
                $this->route = 'merchant';
            }

            if(\Auth::guard()->user()->storeUser) {
                $this->mer_id = \Auth::user()->storeUser->mer_id;
                $this->logintype = 'storeusers';
                $this->route = 'store';
            }            

            return $next($request);
        });

        $this->negoRepo = $negoRepo;
    }

    public function manage()
    {
        $merchant = Auth::guard()->user()->merchant;

        $input = \Request::only('customer_id', 'customer_name', 'product_id', 'product_name', 'status', 'sort');
        $input['merchant_id'] = $merchant->mer_id;

        $negotiations = NegotiationRepo::all($input);

        $status_list = [
            // 0 => trans('localize.nego_status.initiated'),
            10 => trans('localize.nego_status.active'),
            11 => trans('localize.nego_status.mer_accepted'),
            12 => trans('localize.nego_status.cus_accepted'),
            20 => trans('localize.nego_status.completed_customer'),
            21 => trans('localize.nego_status.completed_merchant'),
            30 => trans('localize.nego_status.declined'),
            31 => trans('localize.nego_status.mer_declined'),
            32 => trans('localize.nego_status.cus_declined'),
            40 => trans('localize.nego_status.expired'),
            50 => trans('localize.nego_status.failed'),
            60 => trans('localize.nego_status.counter_offer'),
        ];

        return view('merchant.negotiation.manage', compact('input', 'status_list', 'negotiations'));
    }

    public function view(PriceNegotiation $negotiation)
    {
        return view('merchant.negotiation.view', compact('negotiation'));
    }

    public function accept($nego_id)
    {
        $negotiation = NegotiationRepo::merchantAcceptOffer($nego_id);

        (new CartService)->add(
            $negotiation->customer_id,
            $negotiation->product_id,
            $negotiation->pricing_id,
            $negotiation->quantity,
            null,
            $negotiation->id,
            $negotiation->wholesale
        );

        (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_ACCEPTED);

        return \redirect('merchant/negotiation/manage')->with('success', trans('localize.nego_msg.success.accepted'));
    }

    public function decline($nego_id)
    {
        $negotiation = NegotiationRepo::merchantDeclineOffer($nego_id);

        (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_REJECTED);

        return \redirect('merchant/negotiation/manage')->with('success', trans('localize.nego_msg.success.declined'));
    }

    public function offer($nego_id)
    {
        $data = \Request::all();

        $v = \Validator::make($data, [
            'merchant_offer_price' => 'required|numeric',
            'final_price' => 'boolean',
        ]);

        if ($v->fails()) {
           return back()->withInput()->withErrors($v);
        }

        try {
            NegotiationRepo::merchantCounterOffer($nego_id, $data);
        }
        catch (\Exception $e) {
            return \redirect('merchant/negotiation/manage')->with('error', trans('localize.nego_msg.error.failed'));
        }

        (new Notifier(PriceNegotiation::find($nego_id)))->send(Notifier::PRICE_NEGOTIATION_COUNTER_OFFER);

        return \redirect('merchant/negotiation/manage')->with('success', trans('localize.nego_msg.success.offered'));
    }

    public function approval(PriceNegotiation $negotiation, $status)
    {
        abort_if(!in_array($status, $negotiation::STATUS), 404);

        if ($status == PriceNegotiation::STATUS['ACCEPTED_BY_MERCHANT']) {
            $negotiation->accept($status);

            (new CartService)->add(
                $negotiation->customer_id,
                $negotiation->product_id,
                $negotiation->pricing_id,
                $negotiation->quantity,
                null,
                $negotiation->id,
                $negotiation->wholesale
            );

            (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_ACCEPTED);
        }
        elseif ($status == PriceNegotiation::STATUS['DECLINED_BY_MERCHANT']) {
            $negotiation->decline($status);

            (new Notifier($negotiation))->send(Notifier::PRICE_NEGOTIATION_REJECTED);
        }
        else {
            abort(404);
        }

        return back()->with(['success', trans('common.success')]);
    }

}
