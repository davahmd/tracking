<?php namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Merchant\Controller;
use App\Repositories\StoreUserRepo;

class HomeController extends Controller
{
    public function index()
    {
        if(auth()->user()->merchant) {
            return view('merchant.dashboard');
        }

        if(auth()->user()->storeUser) {
            $storeuser = auth()->user()->storeUser;
            $stores = StoreUserRepo::get_assigned_store_user($storeuser->id);
            // dd($stores);

            return view('merchant.dashboard',compact('storeuser','stores'));
        }
    }

    public function setlocale()
    {
        $data = \Request::all();
        Session::forget('lang');
        Session::put('lang', $data['lang']);
        return back();
    }
}
