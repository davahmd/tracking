<?php
namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Merchant\Controller;
use App\Repositories\OrderRepo;
use App\Repositories\OrderOfflineRepo;
use App\Repositories\StoreRepo;
use App\Repositories\CourierRepo;
use App\Repositories\MerchantRepo;
use App\Repositories\InvoiceRepo;
use App\Models\Order;

class TransactionController extends Controller
{
    private $mer_id;
    private $route;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(auth()->user()->merchant) {
                $this->mer_id = auth()->user()->merchant->mer_id;
                $this->route = 'merchant';
            }

            if(auth()->user()->storeUser) {
                $this->mer_id = auth()->user()->storeUser->mer_id;
                $this->route = 'store';
            }

            return $next($request);
        });

    }

    public function product_transaction($operation)
    {
        $mer_id = $this->mer_id;
        $route = $this->route;

        $input = \Request::only('id', 'name', 'status', 'sort','start','end');
        $status = $input['status'];

        $status_list = [
            ''  => trans('localize.all'),
            '2' => trans('localize.pending'),
            '4' => trans('localize.completed'),
            '5' => trans('localize.cancelled'),
            '6' => trans('localize.refunded'),
        ];

        $type_list = [
            1 => ['name' => 'orders', 'lang' => trans('localize.Online_Orders')],
            3 => ['name' => 'coupons', 'lang' => trans('localize.coupon_orders')],
            4 => ['name' => 'tickets', 'lang' => trans('localize.ticket_orders')],
            5 => ['name' => 'ecards', 'lang' => trans('localize.e-card.orders')],
        ];

        switch ($operation) {
            case 'orders':
                $status_list = [
                    ''  => trans('localize.all'),
                    '1' => trans('localize.processing'),
                    '2' => trans('localize.packaging'),
                    '3' => trans('localize.shipped'),
                    '4' => trans('localize.completed'),
                    '5' => trans('localize.cancelled'),
                    '6' => trans('localize.refunded'),
                    '7' => trans('localize.installation'),
                ];
                $type = 1;
                break;

            // case 'coupons':
            //     $type = 3;
            //     break;

            // case 'tickets':
            //     $type = 4;
            //     break;

            case 'ecards':
                $type = 5;
                break;

            default:
                return back()->with('error', 'Invalid Transaction');
                break;
        }

        $orders = OrderRepo::get_orders_by_status($mer_id, $type, $input);
        $charges = OrderRepo::get_online_total_transaction($mer_id, $type, $input);
        //dd($orders);
        return view('merchant.transaction.product', compact('orders', 'status_list', 'input', 'mer_id','route', 'charges','status', 'type', 'type_list'));
    }

    public function order_offline()
    {
        $mer_id = $this->mer_id;
        $route = $this->route;

        $input = \Request::only('id', 'start', 'end', 'type', 'status', 'sort', 'store','tax_inv_no');
        $status = $input['status'];
        $status_list = array(
            ''  => trans('localize.all'),
            '0' => trans('localize.unpaid'),
            '1' => trans('localize.paid'),
            '2' => trans('localize.cancel_by_member'),
            '3' => trans('localize.cancel_by_merchant'),
        );

        $store = StoreRepo::get_offline_store_by_merchant_id($mer_id);
        $store_list = array('' => trans('localize.all_store'));

        foreach($store as $key => $stor){
            $store_list[$stor->stor_id] = $stor->stor_name;
        }


        $orders = OrderOfflineRepo::get_orders_offline($mer_id, $input);
        $total = OrderOfflineRepo::get_grand_total($mer_id, $input);

        return view('merchant.transaction.order_offline', compact('orders', 'status', 'status_list', 'input', 'mer_id', 'store_list', 'route', 'total'));
    }

    public function update_batch_transaction($mer_id, $operation)
    {
        if(\Request::isMethod('post'))
        {
            $data = \Request::all();

            if(!isset($data['order_id']))
                return back()->withError('Nothing to be updated');

            OrderRepo::update_batch_status_transaction($operation, $data['order_id'], null, 'merchant');

            return back()->withSuccess(trans('localize.recordupdated'));
        }
    }

    //completing self pickup item only
    public function completing_merchant_order($order_id)
    {
        $mer_id = $this->mer_id;
        $order = OrderRepo::get_order_by_id($order_id);
        if(!$order || $order->mer_id != $mer_id || $order->order_status != 3 || $order->product_shipping_fees_type != 3) {
            return back()->with('error', trans('localize.invalid_request'));
        }

        if ($order->order_status == Order::STATUS_INSTALLATION) {
            $service_uncompleted_status = MemberServiceSchedule::where('order_id', '=', $order_id)->whereIn('status', array(0,1,2,3,-1))->exists();

            if ($service_uncompleted_status) {
                return back()->with('error', trans('localize.double_confirm_service_completed'));
            }
        }

        $update = OrderRepo::completing_merchant_order($order_id);
        if($update)
            return back()->with('success', trans('localize.recordupdated'));

        return back()->with('error', trans('localize.internal_server_error.title'));
    }

    public function transaction_detail($parent_id, $store_id)
    {
        $merchant_id = $this->mer_id;

        $transaction = OrderRepo::get_online_order_items($parent_id, $store_id);

        if(!$transaction || $transaction->invoices->isEmpty())
        {
            return redirect('/merchant')->with('error', 'Invalid transaction');
        }

        foreach ($transaction->invoices as $invoice) {
            foreach ($invoice->items as $key => $item) {
                if (is_null($item->product)) {
                    unset($invoice->items[$key]);
                }
            }
        }

        $couriers = CourierRepo::get_couriers();

        $invoices = $transaction->invoices->map(function ($item) {
            $item->tax_number_full = $item->tax_number('ONS');
            return $item;
        });
        $deliveries = $this->get_invoice_delivery_orders($invoices);
        $invoice = $invoices->first();

        $shipping = $transaction->shipping_address? $transaction->shipping_address : null;
        $customer = $invoice->customer? $invoice->customer : null;
        $merchant = $invoice->merchant? $invoice->merchant : null;

        $render = json_decode(json_encode($this->render_order_items($invoices)));
        $orders = $render->items;
        $actions = $render->actions;
        $displayInvoice = $render->displayInvoice;

        return view('merchant.transaction.online.detail.view', compact('customer', 'shipping', 'merchant', 'store_id','transaction', 'orders', 'couriers', 'actions', 'invoices', 'deliveries', 'displayInvoice', 'permissions'));
    }

    protected function render_order_items($invoices)
    {
        $items = collect([]);
        $actions = collect([]);
        $displayInvoice = true;

        if(!$invoices)
            return $new;

        foreach ($invoices as $invoice) {

            foreach ($invoice->items as $item) {

                $product = null;
                if($item->product)
                {
                    $product = [
                        'id' => $item->product->pro_id,
                        'name' => $item->product->title,
                    ];
                }

                $items->push([
                    'order_type' => $item->order_type,
                    'order_id' => $item->order_id,
                    'currency_code' => $item->currency,
                    'currency_rate' => $item->currency_rate,
                    'quantity' => $item->order_qty,
                    'status' => [
                        'code' => $item->order_status,
                        'text' => $item->status(),
                    ],
                    'grouping' => $invoice->item_type == 2? 0 : $invoice->shipment_type,
                    'shipping_type' => [
                        'code' => $item->product_shipping_fees_type,
                        'text' => $item->shipping_type(),
                    ],
                    'courier_info' => [
                        'courier' => $item->courier ? $item->courier->name : null,
                        'shipping_day' => $item->ship_eta,
                        'shipping_note' => $item->shipping_note,
                    ],
                    'total' => $item->total(),
                    'product' => $product,
                    'options' => !empty($item->order_attributes)? json_decode($item->order_attributes) : null,
                ]);

                switch ($item->order_status) {

                    case 1:
                        $displayInvoice = false;
                        if(!$actions->has('accept_order'))
                        {
                            $actions->put('accept_order', ['text' => trans('localize.Order.accept'), 'button' => 'btn-primary', 'order' => 1]);
                        }
                        break;

                    case 2:
                        // only create delivery order for physical product
                        if($invoice->item_type == 1)
                        {
                            if(!$actions->has('create_shipment') && in_array($invoice->shipment_type, [1]))
                            {
                                $actions->put('create_shipment', ['text' => trans('localize.Order.arrange.shipment'), 'button' => 'btn-info', 'order' => 2]);
                            }

                            if(!$actions->has('arrange_pickup') && in_array($invoice->shipment_type, [2]))
                            {
                                $actions->put('arrange_pickup', ['text' => trans('localize.Order.arrange.self_pickup'), 'button' => 'btn-info', 'order' => 3]);
                            }
                        }
                        break;

                    case 3:
                        if($invoice->shipment_type == 2 && !$actions->has('complete_order'))
                        {
                            $actions->put('complete_order', ['text' => trans('localize.Order.complete'), 'button' => 'btn-default', 'order' => 4]);
                        }
                        break;

                    case 4:
                        // if(!$actions->has('refund_order'))
                        // {
                        //     $actions->put('refund_order', ['text' => trans('localize.Order.refund'), 'button' => 'btn-warning', 'order' => 5]);
                        // }
                        break;
                }

                // if(in_array($item->order_status, [1,2,3]) && in_array($item->order_type, [1,2]) && !$actions->has('cancel_order'))
                // {
                //     $actions->put('cancel_order', ['text' => trans('localize.Order.cancel'), 'button' => 'btn-danger', 'order' => 6]);
                // }
            }
        }

        // if(!$displayInvoice)
        // {
        //     if($actions->has('create_shipment'))
        //     {
        //         $actions->pull('create_shipment');
        //     }

        //     if($actions->has('arrange_pickup'))
        //     {
        //         $actions->pull('arrange_pickup');
        //     }
        // }

        return [
            'items' => $items->sortByDesc('grouping')->groupBy('grouping')->toArray(),
            'actions' => $actions->sortBy('order'),
            'displayInvoice' => $displayInvoice,
        ];
    }

    protected function get_invoice_delivery_orders($invoices)
    {
        $deliveries = collect([]);
        foreach ($invoices as $invoice) {
            foreach ($invoice->deliveries as $delivery) {
                $deliveries->push($delivery);
            }
        }

        return $deliveries;
    }

    public function order_status_update($parent_id, $store_id)
    {
        $merchant_id = $this->mer_id;

        $data = \Request::all();
        $action = isset($data['action'])? $data['action'] : null;

        $validate = [
            'action' => 'required|in:accept_order,create_shipment,arrange_pickup,cancel_order,refund_order,complete_order',
            // 'order_id' => 'required|array',
            // 'order_id.*' => 'integer',
            'courier_id' => in_array($action , ['create_shipment'])? 'required|integer' : '',
            'tracking_number' => 'required_if:action,create_shipment',
            'appointment_detail' => 'required_if:action,arrange_pickup',
            'remarks' => 'required_if:action,complete_order',
        ];

        // if(in_array($action, ['create_shipment','arrange_pickup']))
        // {
        //     unset($validate['order_id'], $validate['order_id.*']);
        // }

        \Validator::make($data, $validate, [
            'order_id.*.integer' => trans('validation.integer', ['attribute' => 'order id']),
            'tracking_number.required_if' => trans('validation.required'),
            'appointment_detail.required_if' => trans('validation.required')
        ])->validate();

        $validate = false;
        $merchant = MerchantRepo::find_merchant($merchant_id);

        if(isset($data['order_id']))
        {
            $orders_id = array_map('intval', $data['order_id']);
            $validate = OrderRepo::update_transaction_status_order($action, $parent_id, $store_id, $orders_id);
            // dd($validate);
        }
        else
        {
            $orders_id = OrderRepo::update_transaction_status_order($action, $parent_id, $store_id);
            $validate = !$orders_id? false : true;
        }

        if(!$validate || !$merchant)
            return back()->with('error', 'Unable to update order');

        // data validation
        switch ($action) {
            case 'create_shipment':
            case 'arrange_pickup':
                $taxInvoice = OrderRepo::find_orders_invoice($orders_id);
                if(!$taxInvoice)
                    return back()->with('error', 'Unable to update order');

                $data['invoice_id'] = $taxInvoice->id;
                break;
            case 'complete_order':
            $order_id = reset($orders_id);
            $deliveryOrder = OrderRepo::find_delivery_order($order_id);
            dd($order_id);
            if(!$deliveryOrder)
                return back()->with('error', 'Unable to update order');

            $data['delivery_order_id'] = $deliveryOrder->id;
            break;
        }

        $update = OrderRepo::update_batch_status_transaction($action, $orders_id, $data, 'merchant');
        if(!$update)
            return back()->with('error', 'Unable to update order');

        return back()->with('success', 'Order has been update');
        dd($validate);
    }

    public function order_online($type = 'retail')
    {
        $input = \Request::only('transaction_id', 'customer_name', 'start_date', 'end_date', 'status', 'sort');
        \Validator::make($input, [
            'sort' => 'nullable|in:old,new',
            'start_date' => 'nullable|date_format:"d/m/Y"',
            'end_date' => 'nullable|date_format:"d/m/Y"',
        ])->validate();

        $input['merchant_id'] = $this->mer_id;
        $transactions = OrderRepo::get_online_orders($input, $type);
        $total = $this->get_online_orders_charges($transactions);

        $status_list = [
            '0' => trans('localize.awaiting_payment'),
            '1' => trans('localize.processing'),
            '2' => implode(' / ', [trans('localize.packaging'), trans('localize.pending')]),
            '3' => trans('localize.shipped'),
            '4' => trans('localize.completed'),
            '5' => trans('localize.cancelled'),
            '6' => trans('localize.refunded'),
            '7' => trans('localize.installation'),
        ];

        return view('merchant.transaction.online.listing', compact('transactions', 'input', 'total', 'status_list', 'type'));
    }

    public function online_invoices($type = 'retail')
    {
        $input = \Request::only(['transaction_id', 'tax_number', 'customer_name', 'item_type', 'shipment_type', 'sort']);
        \Validator::make($input, [
            'merchant_id' => 'nullable|integer',
            'customer_id' => 'nullable|integer',
            'item_type' => 'nullable|integer',
            'shipment_type' => 'nullable|integer',
            'sort' => 'nullable|in:old,new',
        ])->validate();

        $item_types = json_decode(json_encode([
            '' => trans('localize.all'),
            1 => trans('localize.normal_product'),
            2 => trans('localize.virtual_product')
        ]));

        $shipment_types = json_decode(json_encode([
            '' => trans('localize.all'),
            0 => trans('localize.not_assigned'),
            1 => implode(', ', [trans('localize.shipping_fees_free'), trans('localize.shipping_fees_product'), trans('localize.shipping_fees_transaction')]),
            2 => trans('localize.self_pickup')
        ]));

        $input['merchant_id'] = $this->mer_id;
        $invoices = InvoiceRepo::get_online_invoices($input, $type);

        return view('merchant.transaction.online.invoices', compact('invoices', 'input', 'item_types', 'shipment_types', 'type'));
    }

    public function online_deliveries($type = 'retail')
    {
        $input = \Request::only(['transaction_id', 'do_number', 'customer_name', 'shipment_type', 'sort']);
        \Validator::make($input, [
            'merchant_id' => 'nullable|integer',
            'customer_id' => 'nullable|integer',
            'sort' => 'nullable|in:old,new',
        ])->validate();

        $shipment_types = json_decode(json_encode([
            '' => trans('localize.all'),
            0 => trans('localize.not_assigned'),
            1 => trans('localize.by_courier'),
            2 => trans('localize.self_pickup')
        ]));

        $input['merchant_id'] = $this->mer_id;
        $deliveries = InvoiceRepo::get_online_deliveries($input, $type);

        return view('merchant.transaction.online.deliveries', compact('deliveries', 'input', 'shipment_types', 'type'));
    }

    protected function get_online_orders_charges($transactions)
    {
        $total = [
            'order_price' => 0,
            'platform_charge_value' => 0,
            'service_charge_value' => 0,
            'merchant_charge_value' => 0,
            'shipping_fees_value' => 0,
            'merchant_earn_value' => 0,
        ];

        foreach ($transactions as $transaction) {
            $total['order_price'] += $transaction->items->sum('order_price');
            $total['platform_charge_value'] += $transaction->items->sum('platform_charge_value');
            $total['service_charge_value'] += $transaction->items->sum('service_charge_value');
            $total['merchant_charge_value'] += $transaction->items->sum('merchant_charge_value');
            $total['shipping_fees_value'] += $transaction->items->sum('shipping_fees_value');
            $total['merchant_earn_value'] += $transaction->items->sum('merchant_earn_value');
        }

        return json_decode(json_encode($total));
    }
}