<?php

namespace App\Http\Controllers\Merchant;

use Auth;
use DateInterval;
use DatePeriod;
use DateTime;
use Request;
use Validator;

use App\Http\Controllers\Controller;

use App\Repositories\CalendarRepo;
use App\Repositories\MerchantRepo;
use App\Repositories\StoreRepo;
use App\Repositories\MemberServiceScheduleRepo;

class CalendarController extends Controller
{
    public function __construct(CalendarRepo $calendarrepo, MerchantRepo $merchantrepo, StoreRepo $storerepo, MemberServiceScheduleRepo $schedulerepo)
    {
        $this->calendar = $calendarrepo;
        $this->merchant = $merchantrepo;
        $this->store = $storerepo;
        $this->schedule = $schedulerepo;
    }

    public function viewMerchantCalendar()
    {
        $merchant = auth()->user()->merchant;
        $stores = $merchant->stores;

        // Offdays
        $offdays = $this->calendar->get_merchant_offdays_by_id($merchant->mer_id);
        $offdays_json = $offdays->toJson();

        // Holidays
        $holidays = $this->calendar->get_all_holidays();
        $holidays_json = $holidays->toJson();

        // Whitelisted Holidays
        $ondays = $this->calendar->get_all_merchant_whitelisted_holiday($merchant->mer_id);
        $ondays_json = $ondays->toJson();

        // $all_events_json = json_encode(array_merge($offdays_array, $holidays_array));

        return view('merchant.calendar.index', compact('merchant','stores','offdays','offdays_json','holidays_json','ondays_json'));
    }

    public function removeMerchantOffday($id)
    {
        $merchant = auth()->user()->merchant;

        $offday = $this->calendar->get_offday_by_id($id);

        if ($offday->merchant_id != $merchant->mer_id) {
            abort(404);
        }
        else {
            $offday = $this->calendar->remove_offday($id);
        }

        return back()->with('success', trans('calendar.success.offday-removed'));
    }

    public function addMerchantOffday()
    {
        $data = Request::only('merchant_id','name','start_date','end_date');

        $v = Validator::make($data, [
            'name' => 'required',
            'start_date' => 'required|date|date_format:Y-m-d|before_or_equal:end_date',
            'end_date' => 'required|date|date_format:Y-m-d|after_or_equal:start_date',
        ]);

        $v->after(function($v) use ($data) {
            $start = new DateTime($data['start_date']);
            $end = new DateTime($data['end_date']);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start, $interval, $end);

            foreach ($period as $date) {

                $scheduled_services = $this->schedule->getCustomerServiceScheduleByDate($date->format("Y-m-d H:i:s"));
                
                if ($scheduled_services->count() > 0) {
                    $v->errors()->add('error', trans('calendar.error.pending_scheduled_service', ['date' => $date->format("Y-m-d")]));
                }

            }
        });

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        $offday = $this->calendar->merchant_add_offday($data['merchant_id'],$data['name'],$data['start_date'],$data['end_date']);

        return back()->with('success', trans('calendar.success.offday-added', ['day' => $offday->name, 'start' => $offday->start_date]));
    }

    public function viewStoreSelections()
    {
        $storeuser = Auth::guard('storeusers')->user();
        $stores = $storeuser->stores;

        return view('merchant.store.calendar.index', compact('stores'));
    }

    public function viewStoreCalendar()
    {
        $data = Request::only('store_id');

        $storeuser = Auth::guard('storeusers')->user();
        $merchant = auth()->user()->merchant;

        if ($storeuser){
            $store_ids = $storeuser->stores->map(function($store, $key) {
                return $store->stor_id;
            });
        }
        elseif ($merchant) {
            $store_ids = $merchant->stores->map(function($store, $key) {
                return $store->stor_id;
            });
        }
        
        $is_authenticated = false;

        foreach($store_ids as $store_id) {
            if ($store_id == $data['store_id']) {
                $is_authenticated = true;
                $stor_id = $store_id;
            }
        }

        if (!$is_authenticated) {
            abort(404);
        }

        $store = $this->store->get_store_by_id($data['store_id']);

        // Merchant Offdays
        $merchant_offdays = $this->calendar->get_merchant_offdays_by_id($store->stor_merchant_id);
        $merchant_offdays_json = $merchant_offdays->toJson();

        // Holidays
        $holidays = $this->calendar->get_all_holidays();
        $holidays_json = $holidays->toJson();

        // Whitelisted Holidays By Merchant
        $merchant_ondays = $this->calendar->get_all_merchant_whitelisted_holiday($store->stor_merchant_id);
        $merchant_ondays_json = $merchant_ondays->toJson();

        // Store Offdays
        $offdays = $this->calendar->get_store_offdays_by_id($data['store_id']);
        $offdays_json = $offdays->toJson();

        return view('merchant.store.calendar.view', compact('stor_id','offdays','offdays_json','merchant_offdays_json','holidays_json','merchant_ondays_json'));
    }

    public function addStoreOffday()
    {
        $data = Request::only('store_id','name','start_date','end_date');

        $v = Validator::make($data, [
            'name' => 'required',
            'start_date' => 'required|date|date_format:Y-m-d|before_or_equal:end_date',
            'end_date' => 'required|date|date_format:Y-m-d|after_or_equal:start_date',
        ]);
        
        $v->after(function($v) use ($data) {
            $start = new DateTime($data['start_date']);
            $end = new DateTime($data['end_date']);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start, $interval, $end);

            foreach ($period as $date) {

                $scheduled_services = $this->schedule->getCustomerServiceScheduleByDate($date->format("Y-m-d H:i:s"));
                
                if ($scheduled_services->count() > 0) {
                    $v->errors()->add('error', trans('calendar.error.pending_scheduled_service', ['date' => $date->format("Y-m-d")]));
                }

            }
        });

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }
        dd();
        // Checking for authentication
        $storeuser = Auth::guard('storeusers')->user();
        $merchant = auth()->user()->merchant;

        if ($storeuser) {
            $store_ids = $storeuser->stores->map(function($store, $key) {
                return $store->stor_id;
            });
        }
        elseif ($merchant) {
            $store_ids = $merchant->stores->map(function($store, $key) {
                return $store->stor_id;
            });
        }
        
        $is_authenticated = false;

        foreach($store_ids as $store_id) {

            if ($storeuser){
                if ($store_id == $data['store_id']) {
                    $is_authenticated = true;
                    $stor_id = $store_id;
                    break;
                }
            }
            elseif ($merchant) {
                $store = $this->store->get_store_by_id($store_id);
                if ($store->stor_merchant_id == $merchant->mer_id) {
                    $is_authenticated = true;
                    break;
                }
            }
        }

        if (!$is_authenticated) {
            abort(404);
        }
        // End of checking

        $offday = $this->calendar->store_add_offday($data['store_id'],$data['name'],$data['start_date'],$data['end_date']);

        return back()->with('success', trans('calendar.success.offday-added', ['day' => $offday->name, 'start' => $offday->start_date]));
    }

    public function removeStoreOffday($id)
    {
        $data = Request::only('store_id');

        $offday = $this->calendar->get_offday_by_id($id);

        $merchant = auth()->user()->merchant;
        $storeuser = Auth::guard('storeusers')->user();

        if ($storeuser) {
            $store_ids = $storeuser->stores->map(function($store, $key) {
                return $store->stor_id;
            });
        }
        elseif ($merchant) {
            $store_ids = $merchant->stores->map(function($store, $key) {
                return $store->stor_id;
            });
        }
        $is_authenticated = false;

        foreach($store_ids as $store_id) {

            if ($storeuser){
                if ($store_id == $data['store_id']) {
                    $is_authenticated = true;
                    $stor_id = $store_id;
                    break;
                }
            }
            elseif ($merchant) {
                $store = $this->store->get_store_by_id($store_id);
                if ($store->stor_merchant_id == $merchant->mer_id) {
                    $is_authenticated = true;
                    break;
                }
            }
        }

        if (!$is_authenticated) {
            abort(404);
        }
        else {
            $offday = $this->calendar->remove_offday($id);
        }

        return back()->with('success', trans('calendar.success.offday-removed'));
    }

    public function checkHoliday()
    {
        $data = Request::only('date');
        $merchant = auth()->user()->merchant;

        $whitelisted_holiday = $this->calendar->check_merchant_existing_onday($merchant->mer_id, $data['date']);

        if ($whitelisted_holiday) {

            return [
                'status' => "whitelisted",
                'id' => $whitelisted_holiday->id,
            ];
        }

        return $this->calendar->get_holiday_by_date($data['date']);
    }

    public function whitelistHoliday()
    {
        $data = Request::only('merchant_id','start','end');

        $whitelist = $this->calendar->whitelist_holiday($data['merchant_id'],$data['start'], $data['end']);

        return back()->with('success', trans('calendar.success.whitelist-added', ['date' => $data['start']]));
    }

    public function removeWhitelistedHoliday($id)
    {
        $whitelist_rule = $this->calendar->remove_whitelisted_rule($id);

        return back()->with('success', trans('calendar.success.whitelist-removed'));
    }
}
