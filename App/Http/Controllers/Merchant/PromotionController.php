<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

use App\Models\Promotion;
use App\Models\Store;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\StoreRepo;
use App\Repositories\ProductRepo;
use App\Repositories\PromotionRepo;

class PromotionController extends Controller
{
	private $mer_id;
    private $route;

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(\Auth::user()->merchant) {
                $this->mer_id = \Auth::user()->merchant->mer_id;
                $this->logintype = 'merchants';
                $this->route = 'merchant';
            }

            if(\Auth::user()->storeUser) {
                $this->mer_id = \Auth::user()->storeUser->mer_id;
                $this->logintype = 'storeusers';
                $this->route = 'store';
            }

            return $next($request);
        });

    }

    public function manage()
    {
    	$merchant_id = $this->mer_id;
    	$route = $this->route;

    	$input = \Request::only('id', 'name', 'status', 'sort');
    	$input['merchant_id'] = $merchant_id;

    	$promotions = PromotionRepo::all($input);

    	return view('merchant.promotion.manage', compact('promotions', 'route', 'input'));
    }

    public function add()
    {
    	$merchant_id = $this->mer_id;

    	$stores = StoreRepo::get_online_store_by_merchant_id($merchant_id)->sortBy('stor_name');

    	return view('merchant.promotion.add', compact('stores'));
    }

    public function add_submit()
    {
        $create_uid = $this->mer_id;
        $create_initiator = 'M'; // Merchant
    	$merchant_id = $this->mer_id;

    	$data = \Request::all();
        $messages = [
            'start.required' => 'The start date of promotion date range field is required.',
            'end.required' => 'The end date of promotion date range field is required.',
            'promo_code.required_if' => 'The promo code field is required for promo code',
            'applied_to.required_if' => 'The discount must be applied to either product or shipping fee or both.',
            'products.required_if' => 'The products field is required for flash sale.',
            'products.*.distinct' => 'The selected products must not be the same product.',
        ];
    	$v = \Validator::make($data, [
    		'name' => 'required|unique:promotions',
    		'start' => 'required',
    		'end' => 'required',
    		'promo_type' => ['required', Rule::in(['F', 'P']),],
    		'promo_code' => 'required_if:promo_type,==,P|unique:promotions|min:4|max:20',
            'promo_min_spend' => 'integer',
    		'discount_type' => ['required' , Rule::in([1, 2]),],
    		'value' => 'required|numeric',
            'applied_to' => 'required_if:promo_type,==,P|array|min:1',
            'applied_to.*' => ['required_if:promo_type,==,P', Rule::in(['P', 'S']),],
            'stores' => 'required',
            'stores.*' => 'required|integer|distinct',
            'categories.*' => 'required|integer|distinct',
            'products' => 'required_if:promo_type,==,F|array|min:1',
    		'products.*' => 'required|integer|distinct',
    		'quantity.*' => 'required|integer',
    	], $messages);

        if ($v->fails()) {
           return back()->withInput()->withErrors($v);
        }

        if ($data['promo_type'] === 'F') {
            $tab = 'flash-sale';

            $validate_flashsale = PromotionRepo::validateFlashsaleProducts($data['products'], $data['start'], $data['end']);

            if (!$validate_flashsale->isEmpty()) {
                return back()->withInput()->withErrors('Unable to create flash sales due to same product is selected within the existing flash sales period.');
            }
        }
        elseif ($data['promo_type'] === 'P') {
            $tab = 'promo-code';
        }

        if (\Request::input('save')) {
            $request_approval = false;
        }
        elseif (\Request::input('submit')) {
            $request_approval = true;
        }

        try {
        	$promotion = PromotionRepo::add($data, $merchant_id, $request_approval, $create_initiator, $create_uid);
        } catch (Exception $e) {
        	return back()->withInput()->withErrors('Unable to add promotion. Please try again.');
        }

        return redirect('merchant/promotion/manage')->with('success');
    }

    public function submit($promotion_id)
    {
        $merchant_id = $this->mer_id;

        $promotion = PromotionRepo::get_promotion($merchant_id, $promotion_id);

        if(empty($promotion)) {
            return redirect('merchant/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

        $initiator_type = 'M';

        try {
            $promotion = PromotionRepo::submit($promotion_id, $initiator_type, $merchant_id);
        } catch (Exception $e) {
            return back()->withInput()->withErrors('Unable to submit promotion. Please try again.');
        }

        return back()->with('status', 'Successfully submitted promotion');
    }

    public function edit($promotion_id)
    {
    	$merchant_id = $this->mer_id;

        $promotion = PromotionRepo::get_promotion($merchant_id, $promotion_id);

        if(empty($promotion)) {
            return redirect('merchant/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

    	$stores = StoreRepo::get_online_store_by_merchant_id($merchant_id)->sortBy('stor_name');

        $from = date('d-m-Y H:i:s', strtotime($promotion->started_at));
        $to = date('d-m-Y H:i:s', strtotime($promotion->ended_at));

        $parseFrom =  Carbon::createFromFormat('d-m-Y H:i:s',$from);
        $promotion->started_at = $parseFrom;

        $parseTo =  Carbon::createFromFormat('d-m-Y H:i:s',$to);
        $promotion->ended_at = $parseTo;

        $daterange = $promotion->started_at->format('d-m-Y').' - '.$promotion->ended_at->format('d-m-Y');

        $selected_stores = $promotion->stores;
        $selected_categories = $promotion->categories;
        $selected_products = $promotion->products;

    	return view('merchant.promotion.edit', compact('stores', 'promotion', 'selected_stores', 'selected_categories', 'selected_products', 'daterange'));
    }

    public function edit_submit($promotion_id)
    {
        $merchant_id = $this->mer_id;

        $promotion = PromotionRepo::get_promotion($merchant_id, $promotion_id);

        if(empty($promotion)) {
            return redirect('merchant/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

        $data = \Request::all();
        $messages = [
            'start.required' => 'The start date of promotion date range field is required.',
            'end.required' => 'The end date of promotion date range field is required.',
            'promo_code.required_if' => 'The promo code field is required for promo code',
            'applied_to.required_if' => 'The discount must be applied to either product or shipping fee or both.',
            'products.required_if' => 'The products field is required for flash sale.',
            'products.*.distinct' => 'The selected products must not be the same product.',
        ];
        $v = \Validator::make($data, [
            'name' => 'required|unique:promotions,name,'.$promotion_id,
            'start' => 'required',
            'end' => 'required',
            'promo_type' => ['required', Rule::in(['F', 'P']),],
            'promo_code' => 'sometimes|required|unique:promotions,promo_code,'.$promotion_id.'|min:4|max:20',
            'promo_min_spend' => 'integer',
            'discount_type' => ['required' , Rule::in([1, 2]),],
            'value' => 'required|numeric',
            'applied_to' => 'required_if:promo_type,==,P|array|min:1',
            'applied_to.*' => ['required_if:promo_type,==,P', Rule::in(['P', 'S']),],
            'stores.*' => 'required|integer|distinct',
            'categories.*' => 'required|integer|distinct',
            'products' => 'required_if:promo_type,==,F|array|min:1',
            'products.*' => 'required|integer|distinct',
            'quantity.*' => 'required|integer',
        ], $messages);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        if ($data['promo_type'] == 'F') {
            $validate_flashsale = PromotionRepo::validateFlashsaleProducts($data['products'], $data['start'], $data['end']);

            if (!$validate_flashsale->isEmpty()) {
                return back()->withInput()->withErrors('Unable to create flash sales due to same product is selected within the existing flash sales period.');
            }
        }

        if (\Request::input('save')) {
            $request_approval = false;
        }
        elseif (\Request::input('submit')) {
            $request_approval = true;
        }

        try {
            $promotion = PromotionRepo::update($data, $promotion_id, $request_approval);            
        } catch (Exception $e) {
            return back()->withInput()->withErrors('Unable to edit promotion. Please try again.');
        }

        return redirect('merchant/promotion/manage')->with('success');
    }

    public function cancel($promotion_id)
    {
        $merchant_id = $this->mer_id;

        $promotion = PromotionRepo::get_promotion($merchant_id, $promotion_id);

        if(empty($promotion)) {
            return redirect('merchant/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

        $initiator_type = 'M';

        PromotionRepo::cancel($promotion_id, $initiator_type, $merchant_id);

        return redirect('merchant/promotion/manage')->with('success');
    }

    public function view($promotion_id)
    {
        $merchant_id = $this->mer_id;

        $promotion = PromotionRepo::get_promotion($merchant_id, $promotion_id);

        if(empty($promotion)) {
            return redirect('merchant/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

        $from = date('d-m-Y H:i:s', strtotime($promotion->started_at));
        $to = date('d-m-Y H:i:s', strtotime($promotion->ended_at));

        $parseFrom =  Carbon::createFromFormat('d-m-Y H:i:s',$from);
        $promotion->started_at = $parseFrom;

        $parseTo =  Carbon::createFromFormat('d-m-Y H:i:s',$to);
        $promotion->ended_at = $parseTo;

        $daterange = $promotion->started_at->format('d-m-Y').' - '.$promotion->ended_at->format('d-m-Y');

        $stores = explode(',', $promotion->stores);
        $categories = explode(',', $promotion->categories);
        $products = explode(',', $promotion->products);

        $stores = Store::whereIn('stor_id', $stores)->select('stor_name')->get();
        $categories = Category::whereIn('id', $categories)->select('name_en')->get();
        $products = Product::whereIn('pro_id', $products)->select('pro_title_en')->get();

        return view('merchant.promotion.view', compact('promotion', 'daterange', 'stores', 'categories', 'products'));
    }
}
