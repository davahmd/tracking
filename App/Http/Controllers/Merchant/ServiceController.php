<?php
namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Merchant\Controller;

use App\Repositories\ProductRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\OrderRepo;
use App\Repositories\MemberServiceScheduleRepo;

use App\Models\MemberServiceSchedule;
use App\Models\Product;
use App\Models\ParentOrder;
use App\Models\Order;
use App\Models\Store;
use App\Models\StoreServiceSchedule;
use App\Models\StoreServiceExtra;

use Carbon\Carbon;
use Validator;
use Helper;
use Excel;

class ServiceController extends Controller
{
    private $mer_id;
    private $route;

    public function __construct(CustomerRepo $customerRepo, OrderRepo $orderRepo, MemberServiceScheduleRepo $memberServiceScheduleRepo)
    {
        $this->customer = $customerRepo;
        $this->order = $orderRepo;
        $this->memberServiceSchedule = $memberServiceScheduleRepo;

        $this->middleware(function ($request, $next) {
            if(auth()->user()->merchant) {
                $this->mer_id = auth()->user()->merchant->mer_id;
                $this->logintype = 'merchants';
                $this->route = 'merchant';
            }

            if(auth()->user()->storeUser) {
                $this->mer_id = auth()->user()->storeUser->mer_id;
                $this->logintype = 'storeusers';
                $this->route = 'store';
            }

            return $next($request);
        });
    }

    public function manage_service()
    {
        $mer_id = $this->mer_id;
        $route = $this->route;

        $input = \Request::only('id', 'name', 'status', 'sort', 'order_id');

        // $products = ProductRepo::get_merchant_products($mer_id, $input);
        $input['mid'] = $mer_id;

        $services = MemberServiceScheduleRepo::all($input);

        // $services = MemberServiceScheduleRepo::get_details_by_mer_id($mer_id);

        return view('merchant.service.manage', compact('mer_id', 'input', 'route', 'services'));
    }

    public function merchantRescheduleService($member_service_schedule_id)
    {
        $mer_id = $this->mer_id;
        $route = $this->route;

        $service_details = $this->memberServiceSchedule->get_details_by_service_id($member_service_schedule_id);
        $product_url_slug = Helper::slug_maker($service_details->pro_title_en, $service_details->pro_id);

        return view('merchant.service.reschedule', compact('mer_id', 'route','service_details', 'product_url_slug'));
    }

    public function merchantScheduleService_submit($member_service_schedule_id)
    {

        if (\Request::isMethod('post')) {
            $data = \Request::all();

            Validator::make($data, [
                'schedule' => 'required',
                'timeslot' => 'required',
	        ], [], [
                "schedule" => '[' . $data['service_name']. "] schedule's date",
                "timeslot" => '[' . $data['service_name']. "] schedule's time-slot",
            ])->validate();


            // $utc_datetime = Carbon::parse($data['schedule-date-time'])->format('Y-m-d H:i:s');
            $utc_datetime = Carbon::createFromFormat('d/m/Y h:i A', $data['schedule-date-time'])->format('Y-m-d H:i:s');

            $member_service_update = MemberServiceScheduleRepo::update_member_service_by_merchant($utc_datetime, $member_service_schedule_id);
            $member_service_info = MemberServiceSchedule::find($member_service_schedule_id);
            if ($member_service_info->reschedule_count_merchant > 2) {

                $cancel_service_status = MemberServiceScheduleRepo::update_member_service_status_cancelled($member_service_schedule_id, -1);
                $active_status_exist = MemberServiceSchedule::where('order_id', '=', $member_service_info->order_id)->whereIn('status', array(0,1,2,3))->exists();

                if (!$active_status_exist) {
                    $cancel_order_status = OrderRepo::update_order_status($member_service_info->order_id, 5);
                }

                if ($cancel_service_status && isset($cancel_order_status)) {
                    return back()->with('warning', trans('front.order.warning.schedule_order_cancel', ['schedule' => $data['service_name']]));
                } elseif ($cancel_service_status) {
                    return back()->with('warning', trans('front.order.warning.schedule_cancel', ['schedule' => $data['service_name']]));
                }
            }

            if(!$member_service_update)
                return back()->with('error', trans('front.order.error.schedule'));


            return back()->with('success', trans('front.order.success.schedule', ['schedule' => $data['service_name']]));
        }
    }

    public function merchantScheduleService_update_status($member_service_schedule_id, $status_type)
    {
        $member_service_schedule_status = $this->memberServiceSchedule->update_member_schedule_service_status($member_service_schedule_id, $status_type);
        if(!$member_service_schedule_status)
            return back()->with('error', trans('localize.unable_update_status'));

        return back()->with('success', trans('localize.update_status_successfully'));
    }

    public function viewService($parent_order_id)
    {
        $mer_id = $this->mer_id;
        $route = $this->route;

        $payment_method = Order::select('payment_method')->where('parent_order_id', $parent_order_id)->first()->payment_method;
        $payment_details = $this->order->get_payment_info_by_parent_order_id($parent_order_id);

        $order_info = ParentOrder::find($parent_order_id);
        foreach($order_info->items as $o) {
            $service_stores[$o->order_id] = $this->order->get_service_stores_details_by_pro_id($o->order_pro_id);
            $member_services[$o->order_id] = $this->order->get_member_service_schedule_by_order_id($o->order_id);
            $product_url_slug[$o->order_id] = Helper::slug_maker($o->product->pro_title_en, $o->order_pro_id);
        }

        foreach($order_info->items as $o) {
            if ($mer_id != $o->product->pro_mr_id) {
                return view('errors.404');
            }
        }

        return view('merchant.service.view', compact('order_details', 'payment_method', 'payment_details', 'service_stores', 'order_info', 'member_services', 'service_details', 'product_url_slug'));
    }

}