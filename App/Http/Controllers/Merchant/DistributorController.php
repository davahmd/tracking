<?php

namespace App\Http\Controllers\Merchant;

use App\Models\Merchant;
use App\Models\PivotMerchantDistributor;
use App\Notifications\RetailerRequestApproved;
use App\Notifications\RetailerRequestRejected;

class DistributorController extends Controller
{
    public function index()
    {
        $query = auth()->user()
            ->merchant
            ->retailers()
            ->nameContain(request('name'));

        if(request('id'))
        {
            $query->whereMerId(request('id'));
        }

        if (request('status') != null) {
            $query->wherePivot('status', request('status'));
        }

        return view('merchant.distributor.index', [
            'merchants' => $query->paginate(20)
        ]);
    }

    public function setStatus($status, $merchantId)
    {
        $merchant = Merchant::active()->findOrFail($merchantId);
        $distributor = auth()->user()->merchant;

        $pivot = PivotMerchantDistributor::whereMerchantId($merchantId)
            ->whereDistributorId($distributor->mer_id)
            ->first();

        if ($status >= 0) {
            $distributor->retailers()->updateExistingPivot($merchantId, [
                'status' => $status
            ]);

            if ($pivot && $pivot->status == PivotMerchantDistributor::STATUS_PENDING) {
                $merchant->notify(new RetailerRequestApproved($distributor));
            }
        }
        else {
            $distributor->retailers()->detach($merchantId);

            if ($pivot && $pivot->status == PivotMerchantDistributor::STATUS_PENDING) {
                $merchant->notify(new RetailerRequestRejected($distributor));
            }
        }

        return back()->with('success', trans('common.success'));
    }
}