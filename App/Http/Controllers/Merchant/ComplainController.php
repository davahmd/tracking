<?php

namespace App\Http\Controllers\Merchant;

use App\Models\Order;
use App\Models\Complain;
use Illuminate\Http\Request;
use App\Models\ComplainSubject;

class ComplainController extends Controller
{
    private $complain;

    public function __construct(Complain $complain)
    {
        $this->complain = $complain;
    }

    /**
     * Complain listing.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $onlyMerchant = true;

        $complains = $this->complain->showAll($onlyMerchant);

        $subjects = (new ComplainSubject())->showAll();

        return view('merchant.complain.index', compact('complains', 'subjects'));
    }

    /**
     * Show message conversations.
     *
     * @param Request $request
     * @param Complain $complain
     * @return false|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Throwable
     */
    public function messages(Request $request, Complain $complain)
    {
        $order = $complain->complainable;

        $messages = $complain->getLatestMessages($complain, 10);

        if ($request->ajax()) {
            $view = view('shared.complain.chats', compact('order', 'complain', 'messages'))->render();

            return json_encode([
                'paginator' => $messages,
                'view' => $view
            ]);
        }

        $isAdmin = true;

        return view('merchant.complain.complain_messages', compact('order', 'complain', 'messages', 'isAdmin'));
    }

    /**
     * Create new message.
     *
     * @param Request $request
     * @param Order $order
     * @param Complain $complain
     * @return false|string
     * @throws \Throwable
     */
    public function postMessages(Request $request, Order $order, Complain $complain)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        $merchant = auth()->user()->merchant;

        $attributes = $request->all();

        $attributes['is_creator'] = false;

        try {
            $complainMessage = null;

            \DB::transaction(function() use ($complain, $merchant, $attributes, &$complainMessage) {
                $complainMessage = $complain->addNewComplainMessage($complain, $merchant, $attributes);
            });

        } catch (\Throwable $e) {
            return json_encode([
                'status' => 'failed',
                'message' => trans('localize.internal_server_error.msg')
            ]);
        }

        $view = view('shared.complain.chat', [
            'message' => $complainMessage
        ])->render();

        return json_encode([
            'view' => $view
        ]);
    }

    /**
     * Delete existing complain.
     *
     * @param Complain $complain
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDeleteMessages(Complain $complain)
    {
        try {
            $complain->delete();

        } catch (\Exception $e) {
            return redirect()->back()->with('error', trans('localize.internal_server_error.msg'));
        }

        return redirect()->back()->with('success', trans('localize.complain.delete.complain'));
    }

    /**
     * Escalated to admin.
     *
     * @param Order $order
     * @param Complain $complain
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function postEscalate(Order $order, Complain $complain)
    {
        if ($complain->is_escalated) {
            return back()->with('error', trans('localize.complain.validation.already_escalated'));
        }

        try {
            $complain->toggleEscalate();

        } catch (\Exception $e) {
            return back()->with('error', trans('localize.internal_server_error.msg'));
        }

        return redirect()->route('merchant.complain::messages', $complain)->with('success', trans('localize.complain.global.success_escalated'));
    }
}