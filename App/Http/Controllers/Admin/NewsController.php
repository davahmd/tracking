<?php

namespace App\Http\Controllers\Admin;

use Cache;
use DB;
use Exception;
use File;
use Image;
use Log;
use Ramsey\Uuid\Uuid;
use Storage;

use App\Models\Admin;
use App\Models\News;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;

class NewsController extends Controller
{
    public function manage()
    {
        return view('admin.news.manage');
    }

    public function ajaxDatatable()
    {
        $query = new News;
        $query->setTable($query->getTable().' as ann');
        $admin = (new Admin())->getTable().' as admin';

        $query = $query->leftJoin($admin, "ann.creator_uid", '=', "admin.adm_id")
                        ->select('ann.*', DB::raw('concat(admin.adm_fname, " ", admin.adm_lname) AS creator'))
                        ->orderBy('created_at', 'desc');


        return Datatables::of($query)
            ->editColumn('title', function ($data) {
                $return = '';
                $return .= '<a href="'.url('admin/news', $data->id).'"><b>'.e($data->title_localize).'</b><br>'.strip_tags(str_limit($data->content_localize, 100)).'</a>';

                return $return;
            })
            ->escapeColumns(['creator'])
            ->editColumn('status', function ($data) {
                return $data->status == 1 ? '<span class="text-success">'.trans('localize.news.published').'</span>' : '<span class="text-danger">'.trans('localize.news.unpublished').'</span>';
            })
            ->addColumn('action', function ($data) {
                $html = '';
                $announcement = json_encode([
                    'id' => $data->id,
                    'title_en' => $data->title_en,
                    'title_id' => $data->title_id,
                ]);

                $html = '<a href="' . url('admin/news/' . $data->id ) . '" class="btn btn-sm btn-white"><i class="fa fa-file-text-o"></i> '.trans('localize.view').'</a>';

                $html .= '<a href="' . url('admin/news/' . $data->id . '/edit') . '" class="btn btn-sm btn-white"><i class="fa fa-edit"></i> '.trans('localize.edit').'</a>';

                return $html;
            })
            ->orderColumn('title', 'title_en $1')
            ->orderColumn('desc', 'desc_en $1')
            ->make(true);
    }

    public function show(News $news)
    {
        return view('admin.news.view', compact('news'));
    }

    public function create()
    {
        return view('admin.news.add');
    }

    public function store()
    {
        $this->validateForm(request());

        $filePath = 'images/news/';
        $data = request()->all();

        $news = new News;

        try {
            if (request()->hasFile('image_file_en')) {
                $filename = time() . '.jpg';
                $filePath = $filePath . $filename;

                $image = (string)Image::make(request()->file('image_file_en'))
                    ->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->encode('jpg');

                Storage::put($filePath, $image, 'public');

                // update record filename
                $data['image_en'] = $filename;
                unset($data['image_file_en']);
            }

            if (request()->hasFile('image_file_idn')) {
                $filename = time() . '.jpg';
                $filePath = $filePath . $filename;

                $image = (string)Image::make(request()->file('image_file_idn'))
                    ->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->encode('jpg');

                Storage::put($filePath, $image, 'public');

                // update record filename
                $data['image_idn'] = $filename;
                unset($data['image_file_idn']);
            }

            $data['creator_uid'] = auth('admins')->id();
            $news->create($data);
        }
        catch (Exception $exception) {
            return back()->withInput()->with('error', trans("localize.error"));
        }

        return redirect()->route('manage_news')->with('success', trans('localize.news.msg.add_success', ['title' => $data['title_idn']]));
    }

    public function edit(News $news)
    {
        return view('admin.news.edit', compact('news'));
    }

    public function update(News $news)
    {
        $this->validateForm(request());

        $filePath = 'images/news/';
        $data = request()->all();

        try {
            if (request('delete_image_en')) {
                if (!is_null($news->image_en)) {
                    $deleteFilePath = $filePath . $news->image_en;
                    Storage::delete($deleteFilePath);

                    $news->image_en = null;
                }
            }

            if (request()->hasFile('image_file_en')) {
                if (!is_null($news->image_en)) {
                    $deleteFilePath = $filePath . $news->image_en;
                    Storage::delete($deleteFilePath);
                }

                $filename = time() . '.jpg';
                $filePath = $filePath . $filename;

                $image = (string)Image::make(request()->file('image_file_en'))
                    ->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->encode('jpg');

                Storage::put($filePath, $image, 'public');

                // update record filename
                $data['image_en'] = $filename;
                unset($data['image_file_en']);
            }

            if (request('delete_image_idn')) {
                if (!is_null($news->image_idn)) {
                    $deleteFilePath = $filePath . $news->image_idn;
                    Storage::delete($deleteFilePath);

                    $news->image_idn = null;
                }
            }

            if (request()->hasFile('image_file_idn')) {
                if (!is_null($news->image_idn)) {
                    $deleteFilePath = $filePath . $news->image_idn;
                    Storage::delete($deleteFilePath);
                }

                $filename = time() . '.jpg';
                $filePath = $filePath . $filename;

                $image = (string)Image::make(request()->file('image_file_idn'))
                    ->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->encode('jpg');

                Storage::put($filePath, $image, 'public');

                // update record filename
                $data['image_idn'] = $filename;
                unset($data['image_file_idn']);
            }
            
            unset($data['delete_image_en']);
            unset($data['delete_image_idn']);

            $news->update($data);
        }
        catch (Exception $exception) {
            return back()->withInput()->with('error', trans("localize.error"));
        }

        return back()->with('success', trans('localize.news.msg.edit_success', ['title' => $data['title_idn']]));
    }

    public function uploadImage()
    {
        $filePath = Storage::putFile('images/news', request()->file('file'));

        return Storage::url($filePath);
    }


    private function validateForm(Request $request)
    {
        $this->validate($request, [
            'title_en' => 'required|string',
            'content_en' => 'required|string',
            'title_idn' => 'required|string',
            'content_en' => 'required|string',
            'image_en' => 'mimes:jpeg,jpg,png',
            'image_idn' => 'mimes:jpeg,jpg,png',
            'prio_no' => 'integer|max:255',
            'status' => 'required|integer'
        ], [], [
            'title_en' => trans('localize.news.title').' (English)',
            'content_en' => trans('localize.news.content').' (Indonesian)',
            'title_idn' => trans('localize.news.title').' (English)',
            'content_idn' => trans('localize.news.content').' (Indonesian)',
            'image_en' => trans('localize.news.image').' (English)',
            'image_idn' => trans('localize.news.image').' (Indonesian)',
            'prio_no' => trans('two.prio_no'),
            'status' => trans('one.status'),
        ]);
    }
}