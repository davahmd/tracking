<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

use App\Models\Promotion;
use App\Models\Store;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\StoreRepo;
use App\Repositories\ProductRepo;
use App\Repositories\PromotionRepo;

class PromotionController extends Controller
{
    public function manage()
    {
    	$adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);

        if(in_array('promotionlist', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

    	$input = \Request::only('id', 'name', 'status', 'sort');
    	$promotions = PromotionRepo::all($input);

    	return view('admin.promotion.manage', compact('promotions', 'input'));
    }

    public function add()
    {
        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);

        if(in_array('promotioncreate', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }
        
    	$stores = Store::all()->sortBy('stor_name');

    	return view('admin.promotion.add', compact('stores'));
    }

    public function add_submit()
    {
        $admin_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($admin_id);

        if(in_array('promotioncreate', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

    	$create_uid = $admin_id;
    	$create_initiator = 'A'; // Admin
    	$merchant_id = 0; // Admin

    	$data = \Request::all();
        $messages = [
            'start.required' => 'The start date of promotion date range field is required.',
            'end.required' => 'The end date of promotion date range field is required.',
            'promo_code.required_if' => 'The promo code field is required for promo code',
            'applied_to.required_if' => 'The discount must be applied to either product or shipping fee or both.',
            'products.required_if' => 'The products field is required for flash sale.',
            'products.*.distinct' => 'The selected products must not be the same product.',
        ];
    	$v = \Validator::make($data, [
    		'name' => 'required|unique:promotions',
    		'start' => 'required',
    		'end' => 'required',
    		'promo_type' => ['required', Rule::in(['F', 'P']),],
    		'promo_code' => 'required_if:promo_type,==,P|unique:promotions|min:4|max:20',
    		'promo_min_spend' => 'integer',
    		'discount_type' => ['required' , Rule::in([1, 2]),],
    		'value' => 'required|numeric',
            'applied_to' => 'required_if:promo_type,==,P|array|min:1',
            'applied_to.*' => ['required_if:promo_type,==,P', Rule::in(['P', 'S']),],
            'stores' => 'required',
            'stores.*' => 'required|integer|distinct',
            'categories.*' => 'required|integer|distinct',
            'products' => 'required_if:promo_type,==,F|array|min:1',
    		'products.*' => 'required|integer|distinct',
    		'quantity.*' => 'required|integer',
    	], $messages);

        if ($v->fails()) {
           return back()->withInput()->withErrors($v);
        }

        if ($data['promo_type'] === 'F') {
            $tab = 'flash-sale';

            $validate_flashsale = PromotionRepo::validateFlashsaleProducts($data['products'], $data['start'], $data['end']);

            if (!$validate_flashsale->isEmpty()) {
                return back()->withInput()->withErrors('Unable to create flash sales due to same product is selected within the existing flash sales period.');
            }
        }
        elseif ($data['promo_type'] === 'P') {
            $tab = 'promo-code';
        }

        if (\Request::input('save')) {
            $request_approval = false;
        }
        elseif (\Request::input('submit')) {
            $request_approval = true;
        }

        try {
        	$promotion = PromotionRepo::add($data, $merchant_id, $request_approval, $create_initiator, $create_uid);
        } catch (Exception $e) {
        	return back()->withInput()->withErrors('Unable to add promotion. Please try again.');
        }

        return redirect('admin/promotion/manage')->with('success');
    }

    public function edit($promotion_id)
    {
        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);

        if(in_array('promotionedit', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

    	$merchant_id = 0;

        $promotion = PromotionRepo::get_promotion($merchant_id, $promotion_id);

        if(empty($promotion)) {
            return redirect('admin/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

    	$stores = Store::all()->sortBy('stor_name');

        $from = date('d-m-Y H:i:s', strtotime($promotion->started_at));
        $to = date('d-m-Y H:i:s', strtotime($promotion->ended_at));

        $parseFrom =  Carbon::createFromFormat('d-m-Y H:i:s',$from);
        $promotion->started_at = $parseFrom;

        $parseTo =  Carbon::createFromFormat('d-m-Y H:i:s',$to);
        $promotion->ended_at = $parseTo;

        $daterange = $promotion->started_at->format('d-m-Y').' - '.$promotion->ended_at->format('d-m-Y');

        $selected_stores = $promotion->stores;
        $selected_categories = $promotion->categories;
        $selected_products = $promotion->products;

    	return view('admin.promotion.edit', compact('stores', 'promotion', 'selected_stores', 'selected_categories', 'selected_products', 'daterange'));
    }

    public function edit_submit($promotion_id)
    {
        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);

        if(in_array('promotionedit', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

    	$merchant_id = 0;

        $promotion = PromotionRepo::get_promotion($merchant_id, $promotion_id);

        if(empty($promotion)) {
            return redirect('admin/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

    	$data = \Request::all();
        $messages = [
            'start.required' => 'The start date of promotion date range field is required.',
            'end.required' => 'The end date of promotion date range field is required.',
            'promo_code.required_if' => 'The promo code field is required for promo code',
            'applied_to.required_if' => 'The discount must be applied to either product or shipping fee or both.',
            'products.required_if' => 'The products field is required for flash sale.',
            'products.*.distinct' => 'The selected products must not be the same product.',
        ];
        $v = \Validator::make($data, [
            'name' => 'required|unique:promotions,name,'.$promotion_id,
            'start' => 'required',
            'end' => 'required',
            'promo_type' => ['required', Rule::in(['F', 'P']),],
            'promo_code' => 'required_if:promo_type,==,P|unique:promotions,promo_code,'.$promotion_id.'|min:4|max:20',
            'promo_min_spend' => 'integer',
            'discount_type' => ['required' , Rule::in([1, 2]),],
            'value' => 'required|numeric',
            'applied_to' => 'required_if:promo_type,==,P|array|min:1',
            'applied_to.*' => ['required_if:promo_type,==,P', Rule::in(['P', 'S']),],
            'stores.*' => 'required|integer|distinct',
            'categories.*' => 'required|integer|distinct',
            'products' => 'required_if:promo_type,==,F|array|min:1',
    		'products.*' => 'required|integer|distinct',
    		'quantity.*' => 'required|integer',
        ], $messages);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }

        if ($data['promo_type'] == 'F') {
            $validate_flashsale = PromotionRepo::validateFlashsaleProducts($data['products'], $data['start'], $data['end']);

            if (!$validate_flashsale->isEmpty()) {
                return back()->withInput()->withErrors('Unable to create flash sales due to same product is selected within the existing flash sales period.');
            }
        }

        if (\Request::input('save')) {
            $request_approval = false;
        }
        elseif (\Request::input('submit')) {
            $request_approval = true;
        }

        try {
            $promotion = PromotionRepo::update($data, $promotion_id, $request_approval);            
        } catch (Exception $e) {
            return back()->withInput()->withErrors('Unable to edit promotion. Please try again.');
        }

        return redirect('admin/promotion/manage')->with('success');
    }

    public function view($promotion_id)
    {
        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);

        if(in_array('promotionlist', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

    	$promotion = Promotion::find($promotion_id);

        if(empty($promotion)) {
            return redirect('admin/promotion/manage')->with('error', trans('localize.promotion_not_found'));
        }

    	$from = date('d-m-Y H:i:s', strtotime($promotion->started_at));
        $to = date('d-m-Y H:i:s', strtotime($promotion->ended_at));

        $parseFrom =  Carbon::createFromFormat('d-m-Y H:i:s',$from);
        $promotion->started_at = $parseFrom;

        $parseTo =  Carbon::createFromFormat('d-m-Y H:i:s',$to);
        $promotion->ended_at = $parseTo;

        $daterange = $promotion->started_at->format('d-m-Y').' - '.$promotion->ended_at->format('d-m-Y');

        $stores = explode(',', $promotion->stores);
        $categories = explode(',', $promotion->categories);
        $products = explode(',', $promotion->products);

        $stores = Store::whereIn('stor_id', $stores)->select('stor_name')->get();
        $categories = Category::whereIn('id', $categories)->select('name_en')->get();
        $products = Product::whereIn('pro_id', $products)->select('pro_title_en')->get();

    	return view('admin.promotion.view', compact('promotion', 'daterange', 'stores', 'categories', 'products'));
    }

    public function approve($promotion_id)
    {
    	$admin_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($admin_id);

        if(in_array('promotionsetstatus', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

    	$promotion = PromotionRepo::approve($promotion_id, $admin_id);
        return back()->with('status', 'Successfully approved promotion request');
    }

    public function deny($promotion_id)
    {
    	$admin_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($admin_id);

        if(in_array('promotionsetstatus', $admin_permission) == false){
            return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

		$data = \Request::all();
        $v = \Validator::make($data, [
            'reason' => 'required|min:3,max:1000',
        ]);

        if ($v->fails()) {
            return back()->withInput()->withErrors($v);
        }    	

        try {
        	$promotion = PromotionRepo::deny($promotion_id, $admin_id, $data);
        } catch (Exception $e) {
        	return back()->withInput()->withErrors('Unable to deny request. Please try again.');
        }

        return back()->with('status', 'Successfully denied promotion request');
    }
}
