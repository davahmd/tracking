<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PriceNegotiation;
use App\Repositories\NegotiationRepo;

class NegotiationController extends Controller
{
    public function manage()
    {
    	$input = \Request::only('customer_id', 'customer_name', 'merchant_id', 'merchant_name', 'product_id', 'product_name', 'status', 'sort');
    	$negotiations = NegotiationRepo::all($input);

    	$status_list = [
    		// 0 => trans('localize.nego_status.initiated'),
            10 => trans('localize.nego_status.active'),
            11 => trans('localize.nego_status.mer_accepted'),
            12 => trans('localize.nego_status.cus_accepted'),
            20 => trans('localize.nego_status.completed_customer'),
            21 => trans('localize.nego_status.completed_merchant'),
            30 => trans('localize.nego_status.declined'),
            31 => trans('localize.nego_status.mer_declined'),
            32 => trans('localize.nego_status.cus_declined'),
            40 => trans('localize.nego_status.expired'),
            50 => trans('localize.nego_status.failed'),
            60 => trans('localize.nego_status.counter_offer'),
        ];

    	return view('admin.negotiation.manage', compact('negotiations', 'input', 'status_list'));
    }
}
