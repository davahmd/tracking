<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;

use Illuminate\Support\Facades\Session;

class HomeController extends Controller 
{
    public function index()
    {
        return view('admin.dashboard');
    }

    // public function setlocale()
    // {
    //     $data = \Request::all();
    //     Session::forget('lang');
    //     Session::put('lang', $data['lang']);

    //     // return 'success';
    //     return back();
    // }
}
