<?php

namespace App\Http\Middleware;

use Closure;
use Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CustomerProfileUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $now = Carbon::now('Asia/Kuala_Lumpur');
        $start = Carbon::create(2018, 5, 28, 11, 0, 0, 'Asia/Kuala_Lumpur');
        $automate = $now->gt($start);

        if (Auth::guard('web')->check() && $automate)
        {
            // $customer = Auth::user();

            // if (!$customer->identity_card || !$customer->update_flag || !$customer->email || !$customer->info || !$customer->info->cus_gender || !$customer->info->cus_dob) {
            //     return redirect('/profile/update');
            // }
        }

        return $next($request);
    }
}
