<?php

namespace App\Http\Middleware;

use App\Models\Merchant;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotMerchant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
	{
	    if (Auth::check()) {
            if (!Auth::user()->merchant || Auth::user()->merchant->mer_staus == Merchant::STATUS_BLOCK) {
                return redirect('login');
            }
        }
        elseif (Auth::guard('api')->check()) {
	        if (!Auth::guard('api')->user()->merchant || Auth::guard('api')->user()->merchant->mer_staus == Merchant::STATUS_BLOCK) {
                return response('Unauthorized.', 401);
            }
        }

		return $next($request);
	}
}