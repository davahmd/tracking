<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if ($guard == 'admins') {
                return redirect('admin');
            }

            if ($guard == 'merchants') {
                return redirect('merchant');
            }

            if ($guard == null || $guard == 'web') {
                return redirect('/');
            }
        }

        return $next($request);
    }
}
