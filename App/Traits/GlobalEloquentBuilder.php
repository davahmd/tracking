<?php
namespace App\Traits;

trait GlobalEloquentBuilder
{
    public function scopeWithAndWhereHas($query, $relation, $constraint = null)
    {
        if ($constraint) {
            return $query->whereHas($relation, $constraint)
                ->with([$relation => $constraint]);
        } else {
            return $query->whereHas($relation)
                ->with($relation);
        }
    }
}