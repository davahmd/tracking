<?php
namespace App\Traits;

use Carbon\Carbon;
use PDF;

trait HasOfflineOrder
{
    public function sendOfflineTransactionReferenceResponse($userType, $order, $operation, $batch = false)
    {
        $param = \Request::all();
        $requestUrl = url()->current();

        $param['operation'] = 'pdf';
        $downloadLink = $requestUrl.'?'.http_build_query($param);
        $param['operation'] = 'print';
        $printLink = $requestUrl.'?'.http_build_query($param);

        $response = [
            'order' => $order,
            'isFromApi' => false,
            'batch' => $batch,
            'userType' => $userType,
            'downloadLink' => $downloadLink,
            'printLink' => $printLink,
        ];

        switch ($operation)
        {
            case 'print':
                return view('modals.offline-order.transaction_reference.print', $response)->render();
                break;

            case 'pdf':
                $fileName = 'Transaction_reference-offline.pdf';
                $pdf = PDF::loadView('modals.offline-order.transaction_reference.print', $response);
                return $pdf->download($fileName);
                break;

            case 'api':
                $response['isFromApi'] = true;
                return view('modals.offline-order.transaction_reference.print', $response)->render();
                break;

            default:
                return view('modals.offline-order.transaction_reference.view', $response)->render();
                break;
        }
    }

    public function sendOfflineTaxInvoiceResponse($userType, $order, $operation, $address = null, $batch = false)
    {
        $param = \Request::all();

        $requestUrl = url()->current();
        $param['operation'] = 'pdf';
        $downloadLink = $requestUrl.'?'.http_build_query($param);
        $param['operation'] = 'print';
        $printLink = $requestUrl.'?'.http_build_query($param);

        $response = [
            'order' => $order,
            'isFromApi' => false,
            'batch' => $batch,
            'userType' => $userType,
            'downloadLink' => $downloadLink,
            'printLink' => $printLink,
            'address' => $address
        ];

        switch ($operation)
        {
            case 'print':
                return view('modals.offline-order.tax_invoice.print', $response)->render();
                break;

            case 'pdf':
                $fileName = 'Transaction_reference-offline.pdf';
                $pdf = PDF::loadView('modals.offline-order.tax_invoice.print', $response);
                return $pdf->download($fileName);
                break;

            case 'api':
                $response['isFromApi'] = true;
                return view('modals.offline-order.tax_invoice.print', $response)->render();
                break;

            default:
                return view('modals.offline-order.tax_invoice.view', $response)->render();
                break;
        }
    }
}