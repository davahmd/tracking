<?php

namespace App\Traits;

use App\Models\DeliveryOrder;
use App\Models\TaxInvoice;
use Carbon\Carbon;
use PDF;

trait HasTaxInvoiceDeliveryOrder
{
    public function sendTaxInvoiceResponse($invoice, $userType, $operation, $address, $batch = false)
    {
        $param = \Request::all();
        $requestUrl = url()->current();

        $param['operation'] = 'pdf';
        $downloadLink = $requestUrl.'?'.http_build_query($param);
        $param['operation'] = 'print';
        $printLink = $requestUrl.'?'.http_build_query($param);

        $companies = json_decode(file_get_contents(url('/backend/js/company.json')), true);

        $date = Carbon::create('2017','10','1','0','0','0');

        $isFromApi = false;
        // if($company)
        // {
        //     $address = $merchant;
        //     // $company = 'MERCHANT';
        //     // foreach ($companies as $detail) {
        //     //     if(strtolower($detail->name) == $company)
        //     //     {
        //     //         $address = $detail;
        //     //     }
        //     // }
        // }

        $response = [
            'userType' => $userType,
            'invoice' => $invoice,
            'date' => $date,
            'operation' => $operation,
            'isFromApi' => $isFromApi,
            'address' => $address,
            'companies' => $companies,
            'batch' => $batch,
            'download_link' => $downloadLink,
            'print_link' => $printLink,
        ];

        switch ($operation)
        {
            case 'print':
                return view('modals.order.tax_invoice.print', $response)->render();
                break;

            case 'pdf':
                $fileName = 'Online_tax_invoice.pdf';
                $pdf = PDF::loadView('modals.order.tax_invoice.print', $response);
                return $pdf->download($fileName);
                break;

            case 'api':
                $response['isFromApi'] = true;
                return view('modals.order.tax_invoice.print', $response)->render();
                break;

            default:
                $response['operation'] = 'view';
                return view('modals.order.tax_invoice.view', $response)->render();
                break;
        }
    }

    public function sendOrderDeliveriesResponse($delivery, $userType, $operation, $batch = false)
    {
        $param = \Request::all();
        $requestUrl = url()->current();

        $param['operation'] = 'pdf';
        $downloadLink = $requestUrl.'?'.http_build_query($param);
        $param['operation'] = 'print';
        $printLink = $requestUrl.'?'.http_build_query($param);

        $isFromApi = false;
        $response = [
            'userType' => $userType,
            'delivery' => $delivery,
            'operation' => $operation,
            'isFromApi' => $isFromApi,
            'batch' => $batch,
            'download_link' => $downloadLink,
            'print_link' => $printLink,
        ];

        switch ($operation)
        {
            case 'print':
                return view('modals.order.delivery_order.print', $response)->render();
                break;

            case 'pdf':
                $fileName = 'Online_delivery_order.pdf';
                $pdf = PDF::loadView('modals.order.delivery_order.print', $response);
                return $pdf->download($fileName);
                break;

            case 'api':
                $response['isFromApi'] = true;
                return view('modals.order.delivery_order.print', $response)->render();
                break;

            default:
                $response['operation'] = 'view';
                return view('modals.order.delivery_order.view', $response)->render();
                break;
        }
    }

    public function sendTransactionReference($userType, $transaction, $operation, $merchant = null)
    {
        $default = $transaction->items->first();
        $currencyCode = $default->currency;
        $currencyRate = $default->currency_rate;

        $price = round(max($transaction->items->sum('total_product_price'), 0), 2);
        $shippingFees = round(max($transaction->items->sum('total_product_shipping_fees_value'), 0), 2);
        $platformCharge = round(max($transaction->items->sum('cus_platform_charge_value'), 0), 2);
        $serviceCharge = round(max($transaction->items->sum('cus_service_charge_value'), 0), 2);
        $merchantCharge = round(max($transaction->items->sum('merchant_charge_value'), 0), 2);
        $merchantEarn = round(max($transaction->items->sum('merchant_earn_value'), 0), 2);
        $total = round(max($transaction->items->sum('order_value'), 0), 2);

        $charges = json_decode(json_encode([
            // 'credit' => [
            //     'amount' => $credit,
            //     'shippingFees' => $shippingFees,
            //     'platformCharge' => $platformCharge,
            //     'serviceCharge' => $serviceCharge,
            //     'merchantCharge' => $merchantCharge,
            //     'merchantEarn' => $merchantEarn,
            //     'total' => $total,
            // ],
            'price' => [
                'amount' => round($price, 2),
                'shippingFees' => round($shippingFees, 2),
                'platformCharge' => round($platformCharge, 2),
                'serviceCharge' => round($serviceCharge, 2),
                'merchantCharge' => round($merchantCharge, 2),
                'merchantEarn' => round($merchantEarn, 2),
                'total' => round($total, 2),
            ]
        ]));

        $additional = json_decode(json_encode([
            'currencyCode' => $currencyCode,
            'currencyRate' => $currencyRate,
            'platformChargeRate' => $default->cus_platform_charge_rate,
            'serviceChargeRate' => $default->cus_service_charge_rate,
            'merchantChargeRate' => $default->merchant_charge_percentage,
        ]));

        $param = [];
        $requestUrl = url()->current();
        if($userType == 'admin' && $merchant)
        {
            $param['merchant_id'] = $merchant->mer_id;
        }

        $pageData['title'] = trans('localize.transaction_reference');
        $pageData['include'] = 'modals.order.transaction_reference.data';

        $isFromApi = false;
        switch ($operation)
        {
            case 'print':
                $pageData = json_decode(json_encode($pageData));
                return view('modals.order.transaction_reference.print', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'))->render();
                break;

            case 'pdf':
                $pageData = json_decode(json_encode($pageData));
                $fileName = 'Transaction_reference-'.$transaction->transaction_id.'.pdf';
                $pdf = PDF::loadView('modals.order.transaction_reference.print', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'));
                return $pdf->download($fileName);
                break;

            case 'api':
                $isFromApi = true;
                $pageData = json_decode(json_encode($pageData));
                return view('modals.order.transaction_reference.print', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'))->render();
                break;

            default:
                $param['operation'] = 'print';
                $printLink = $requestUrl.'?'.http_build_query($param);
                $param['operation'] = 'pdf';
                $downloadLink = $requestUrl.'?'.http_build_query($param);

                $pageData['printLink'] = $printLink;
                $pageData['downloadLink'] = $downloadLink;

                $pageData = json_decode(json_encode($pageData));
                return view('modals.order.transaction_reference.view', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'))->render();
                break;
        }
    }
}