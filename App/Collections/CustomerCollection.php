<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class CustomerCollection extends Collection
{
    public function apiScheduleHistoryData()
    {
        return $this->map(function ($customer) {
            $service_arr = [];
            
            foreach($customer->services as $service => $values) {
                $service_info = [
                    'service_id' => $values->id,
                    'service_status' => $values->status,
                    'service_order' => $values->order_id,
                    'service_name' => $values->service_name_current,
                    'service_schedule_time' => $values->schedule_datetime,
                    'reschedule_count_member' => $values->reschedule_count_member,
                ];

                array_push($service_arr, $service_info);
            }

            return [
                // 'id' => $order->id,
                // 'transaction_id' => $order->transaction_id,
                // 'items' => count($order->items),
                // 'price' => $order->items->sum('total_product_price'),
                // 'date' => $order->date,
                'services' => $service_arr,
            ];
        });
    }
}