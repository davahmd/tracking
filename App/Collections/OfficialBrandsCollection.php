<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class OfficialBrandsCollection extends Collection
{
    public function apiData()
    {
        return $this->map(function ($brand) {
            return [
                'id' => $brand->brand_id,
                'name' => $brand->brand_name,
                'description' => $brand->brand_desc,
                'image' => \Storage::url('officialbrands/' . $brand->brand_Img), //$brand->logo_path
            ];
        });
    }
}