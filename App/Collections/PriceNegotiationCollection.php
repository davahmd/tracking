<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

class PriceNegotiationCollection extends Collection
{
    public function apiNegotiationListingData()
    {
        return $this->map(function ($negotiation) {

            $ended_at = $negotiation->expired_at;
            $start = new Carbon(Carbon::now());
            $end = new Carbon($ended_at);

            return [
                'id' => $negotiation->id,
                'session_id' => $negotiation->session_id,
                'product_name' => $negotiation->product->title,
                'quantity' => $negotiation->quantity,
                'currency_code' => config('app.currency_code'),
                'price' => $negotiation->pricing->price,
                'customer_offer_price' => $negotiation->customer_offer_price,
                'merchant_offer_price' => $negotiation->merchant_offer_price,
                'status' => ($negotiation->child) ? $negotiation->child->status : $negotiation->status,
                'image' => $negotiation->product->mainImageUrl(),
                'created_at' => Carbon::parse($negotiation->created_at)->toDateTimeString(),
                'remaining_time' => ($end > $start) ? $end->diffInSeconds($start) : 0,
            ];
            
        });
    }

    public function apiNegotiationHistoryData()
    {
        return $this->map(function ($negotiation) {

            $ended_at = $negotiation->expired_at;
            $start = new Carbon(Carbon::now());
            $end = new Carbon($ended_at);

            return [
                'currency_code' => config('app.currency_code'),
                'customer_offer_price' => $negotiation->customer_offer_price,
                'customer_created_at' => Carbon::parse($negotiation->created_at)->toDateTimeString(),
                'merchant_offer_price' => $negotiation->merchant_offer_price,
                'merchant_created_at' => ($negotiation->merchant_offer_price) ? Carbon::parse($negotiation->updated_at)->toDateTimeString() : null,
                'final_price' => $negotiation->final_price,
                'remaining_time' => ($end > $start) ? $end->diffInSeconds($start) : 0,
            ];
        });
    }
}