<?php

namespace App\Collections;

use App\Services\Notifier\Notifier;
use Illuminate\Database\Eloquent\Collection;

use App\Models\Shipping;
use Helper;
use Carbon\Carbon;

class OrderCollection extends Collection
{
    public function apiData()
    {
        return $this->map(function ($order) {

            $service_info = [];
            $shipping = Shipping::where('ship_order_id', $order->order_id)->first();

            if ($shipping) {
                $shipping_address = $shipping->getFullAddress();
            } else {
                $shipping_address = null;
            }

            if($order->services()->exists()) {

                foreach($order->services as $service) {

                    array_push($service_info, [
                        'service_id' => $service->id,
                        'service_stor_id' => $service->stor_id,
                        'service_stor_name' => $service->store->stor_name,
                        'service_stor_image_link' => $service->store->getImageUrl(),
                        'service_stor_address' => Helper::getStoreAddress($service->stor_id),
                        'service_stor_phone' => $service->store->stor_phone,
                        'service_stor_office_number' => $service->store->stor_office_number,
                        'service_status' => $service->status,
                        'service_name' => $service->service_name_current,
                        'service_schedule_time' => Carbon::parse($service->schedule_datetime)->toDateTimeString(),
                    ]);
                }
            }

            return [
                'merchant' => $order->product->merchant->username,
                'product' => $order->product->pro_title_en,
                'status' => $order->order_status,
                'ordered_at' => $order->order_date,
                'shipped_at' => $order->order_shipment_date,
                'track_no' => $order->order_tracking_no,
                'order_id' => $order->order_id,
                'pro_id' => $order->order_pro_id,
                'stor_id' => $order->product->store->stor_id,
                'stor_name' => $order->product->store->stor_name,
                'stor_image_link' => $order->product->store->getImageUrl(),
                'pro_name' => $order->product->pro_title_en,
                'pro_image_link' => $order->product->mainImageUrl(),
                'pro_qty' => $order->order_qty,
                'currency' => $order->currency,
                'total_pro_price' => number_format($order->product_price * $order->order_qty, 2),
                'order_status' => $order->order_status,
                'shipping' => [
                    'cus_name' => $shipping->ship_name ?? null,
                    'address' => $shipping_address,
                    'cus_phone' => $shipping->ship_phone ?? null,
                ],
                'service' => isset($service_info) ? $service_info : null,
                'can_rate' => [
                    'product' => $order->rateEligibility->product,
                    'store' => $order->rateEligibility->store
                ],
                'rating' => [
                    'product' => $order->productRating ? [
                        'rate' => (int) $order->productRating->rating,
                        'max_rate' => 5,
                        'review' => $order->productRating->review,
                        'images' => $order->productRating->images->pluck('imageLink')->toArray()
                    ]: null,
                    'store' => $order->storeRating ? [
                        'rate' => (int) $order->storeRating->rating,
                        'max_rate' => 5,
                        'review' => $order->storeRating->review,
                        'images' => $order->storeRating->images->pluck('imageLink')->toArray()
                    ]: null,
                ]
            ];
        });
    }
}