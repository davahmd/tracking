<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class CategoryCollection extends Collection
{
    public function apiData()
    {
        return $this->map(function ($category) {
            return [
                'id' => $category->id,
                'name' => $category->name_en,
                'image' => isset($category->image)?\Storage::url('category/image/' . $category->image):null,
                'banner' => isset($category->banner)?\Storage::url('category/banner/' . $category->banner):null,
                'icon' => isset($category->icon)?\Storage::url('category/icon/' . $category->icon):null,
            ];
        });
    }
}