<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class VehicleCollection extends Collection
{
    public function apiCustomerVehicleData()
    {
        return $this->map(function ($vehicle) {

            return [
                'brand' => $vehicle->brand,
                'model' => $vehicle->model,
                'variant' => $vehicle->variant,
                'year' => $vehicle->year,
                'default' => $vehicle->pivot->isdefault,
            ];
            
        });
    }
}