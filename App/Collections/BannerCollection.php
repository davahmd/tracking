<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class BannerCollection extends Collection
{
    public function apiData()
    {
        return $this->map(function ($banner) {
            return [
                'title' => $banner->bn_title,
                'image' => \Storage::url('banner/' . $banner->bn_img),
                'url' => $banner->bn_redirecturl,
            ];
        });
    }
}