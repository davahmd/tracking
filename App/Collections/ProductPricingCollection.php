<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

use App\Models\PromotionProduct;
use App\Repositories\PromotionRepo;

use Carbon\Carbon;

class ProductPricingCollection extends Collection
{
    public function apiData()
    {
        return $this->map(function ($productPrice) {

            $promo_item = PromotionRepo::new_pricing_by_promotion($productPrice->pro_id);

            if ($promo_item) {
                $promo_product = $promo_item;
                $promo_product_info = PromotionProduct::where('promotion_id', $promo_product->id)->where('product_id', $productPrice->pro_id)->first();
                $ended_at = $promo_product->ended_at;
                $start  = new Carbon(Carbon::now());
                $end    = new Carbon($ended_at);
                // $time_remaining = $start->diffInHours($end) . ' hours : ' . $start->diff($end)->format('%I minutes : %S seconds');
                $time_remaining = $end->diffInSeconds($start);

                return [
                    'id' => $productPrice->id,
                    'currency_symbol' => $productPrice->country->co_cursymbol,
                    'display_price' => $promo_product->discount_rate ? number_format($productPrice->price *(1-$promo_product->discount_rate), 2, '.', '') : number_format($productPrice->price - $promo_product->discount_value, 2, '.', ''),
                    'original_price' => $productPrice->price,
                    'discount_rate' => $promo_product->discount_rate ? $promo_product->discount_rate*100 ."%" : rpFormat($promo_product->discount_value),
                    'wholesale_price' => $productPrice->wholesale_price,
                    'negotiable' => (int) $promo_product->negotiable,
                    'remaining_item' => $productPrice->quantity,
                    'min_quantity' => $productPrice->min_quantity,
                    'remaining_flash_item' => $promo_product_info->limit - $promo_product_info->count,
                    'remaining_time_flash' => $time_remaining,
                    'attributes' => json_decode($productPrice->attributes_name),
                    'negotiation' => $productPrice->customerActiveNegotiations()->get()->first()
                ];
            }

            return [
                'id' => $productPrice->id,
                'currency_symbol' => $productPrice->country->co_cursymbol,
                // 'display_price' => $productPrice->getPurchasePrice(),
                'display_price' => $productPrice->price,
                'original_price' => $productPrice->price,
                // 'discount_rate' => $productPrice->getDiscountRate(),
                'discount_rate' => null,
                'wholesale_price' => $productPrice->wholesale_price ? $productPrice->wholesale_price : null,
                'negotiable' => (int) $productPrice->negotiable,
                'min_quantity' => $productPrice->min_quantity,
                'remaining_item' => $productPrice->quantity,
                'attributes' => json_decode($productPrice->attributes_name),
                'negotiation' => $productPrice->customerActiveNegotiations()->get()->first()
            ];
        });
    }
}