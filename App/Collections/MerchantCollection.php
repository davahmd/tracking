<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class MerchantCollection extends Collection
{
    public function apiData()
    {
        if ($this->isNotEmpty() && !$this->first()->relationLoaded('state')) {
            $this->load('state');
        }

        return $this->map(function ($merchant) {
            return [
                'id' => $merchant->mer_id,
                'name' => $merchant->merchantName(),
                'city' => $merchant->city ? $merchant->city->name : null,
                'state' => $merchant->state ? $merchant->state->name : null,
                'image' => $merchant->image_url,
                'retailer_status' => auth('api')->user() ? auth('api')->user()->isRetailerForDistributor($merchant) : false
            ];
        });
    }
}