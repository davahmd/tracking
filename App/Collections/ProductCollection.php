<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

use App\Models\PromotionProduct;
use App\Repositories\PromotionRepo;

use Carbon\Carbon;

class ProductCollection extends Collection
{
    public function apiData()
    {
        $this->load('merchant', 'pricing', 'pricing.country');

        return $this->map(function ($product) {

            $promo_item = PromotionRepo::new_pricing_by_promotion($product->pro_id);
            $productPrice = $product->pricing->first();

            $rating = stdObject($product->compiledRatings(true));

            $promo_products = [];
            $promo_product_info = [];

            if ($promo_item) {
                $promo_product = $promo_item;
                $promo_product_info = PromotionProduct::where('promotion_id', $promo_product->id)->where('product_id', $product->pro_id)->first();
                $ended_at = $promo_product->ended_at;
                $start  = new Carbon(Carbon::now());
                $end    = new Carbon($ended_at);
                // $time_remaining = $start->diffInHours($end) . ' hours : ' . $start->diff($end)->format('%I minutes : %S seconds');
                $time_remaining = $end->diffInSeconds($start);

                return [
                    'id' => $product->pro_id,
                    'name' => $product->title,
                    'image' => $product->mainImageUrl(),
                    'currency_symbol' => $productPrice->country->co_cursymbol,
                    'display_price' => $promo_product->discount_rate ? number_format($product->pricing->first()->price *(1-$promo_product->discount_rate), 2, '.', '') : number_format($product->pricing->first()->price - $promo_product->discount_value, 2, '.', ''),
                    'original_price' => $productPrice->price,
                    'negotiable' => (int) $productPrice->negotiable,
                    'wholesale_price' => number_format($productPrice->wholesale_price, 2, '.', ''),
                    'discount_rate' => $promo_product->discount_rate ? $promo_product->discount_rate*100 ."%" : rpFormat($promo_product->discount_value),
                    // 'remaining_item' => $productPrice->quantity,
                    'remaining_item' => $product->pro_qty,
                    'remaining_flash_item' => $promo_product_info->limit - $promo_product_info->count,
                    'remaining_time_flash' => $time_remaining,
                    'attributes' => json_decode($productPrice->attributes_name),
                    'rating' => [
                        'rate' => $rating->rate,
                        'max_rate' => 5,
                        'total' => $rating->total,
                        'percentage' => $rating->percentage,
                        'cumulative' => $rating->cumulative,
                    ]
                ];
            }

            return [
                'id' => $product->pro_id,
                'name' => $product->title,
                'image' => $product->mainImageUrl(),
                'currency_symbol' => $productPrice->country->co_cursymbol,
                'display_price' => $productPrice->price,
                // 'display_price' => $productPrice->getPurchasePrice(),
                'original_price' => $productPrice->price,
                'negotiable' => (int) $productPrice->negotiable,
                'wholesale_price' => number_format($productPrice->wholesale_price, 2, '.', ''),
                // 'discount_rate' => $productPrice->getDiscountRate(),
                'discount_rate' => null,
                // 'remaining_item' => $productPrice->quantity,
                'remaining_item' => $product->pro_qty,
                'attributes' => json_decode($productPrice->attributes_name),
                'rating' => [
                    'rate' => $rating->rate,
                    'max_rate' => 5,
                    'total' => $rating->total,
                    'percentage' => $rating->percentage,
                    'cumulative' => $rating->cumulative,
                ]
            ];
        });
    }
}