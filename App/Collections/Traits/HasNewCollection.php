<?php

namespace App\Collections\Traits;

trait HasNewCollection
{
    public function newCollection(array $models = [])
    {
        $qualifiedClassName = 'App\\Collections\\' . class_basename(get_class($this)) . 'Collection';

        return new $qualifiedClassName($models);
    }
}