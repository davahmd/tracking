<?php

namespace App\Collections;

use App\Models\Shipping;

use Illuminate\Database\Eloquent\Collection;
use Helper;
use Carbon\Carbon;

class ParentOrderCollection extends Collection
{
    public function apiOrderHistoryData()
    {
        return $this->map(function ($order) {

            $shipping = Shipping::where('ship_order_id', $order->items->first()->order_id)->first();
            $product_arr = [];

            if ($shipping) {
                $shipping_address = $shipping->getFullAddress();
            } else {
                $shipping_address = null;
            }

            foreach($order->items as $item => $values) {
                if($values->services()->exists()) {
                    $service_info = [];
                    foreach($values->services as $service) {

                        array_push($service_info, [
                            'service_id' => $service->id,
                            'service_stor_id' => $service->stor_id,
                            'service_stor_name' => $service->store->stor_name,
                            'service_stor_image_link' => $service->store->getImageUrl(),
                            'service_stor_address' => Helper::getStoreAddress($service->stor_id),
                            'service_status' => $service->status,
                            'service_name' => $service->service_name_current,
                            'service_schedule_time' => Carbon::parse($service->schedule_datetime)->toDateTimeString(),
                        ]);

                    }
                }

                $product_info = [
                    'order_id' => $values->order_id,
                    'pro_id' => $values->order_pro_id,
                    'stor_id' => $values->product->store->stor_id,
                    'stor_name' => $values->product->store->stor_name,
                    'stor_image_link' => $values->product->store->getImageUrl(),
                    'pro_name' => $values->product->pro_title_en,
                    'pro_image_link' => $values->product->mainImageUrl(),
                    'pro_qty' => $values->order_qty,
                    // 'currency' => $values->currency,
                    'currency' => 'Rp',
                    'total_pro_price' => number_format($values->product_price * $values->order_qty, 2),
                    'order_status' => $values->order_status,
                    'service' => isset($service_info) ? $service_info : null,
                    'can_rate' => [
                        'product' => $values->rateEligibility->product,
                        'store' => $values->rateEligibility->store
                    ],
                    'rating' => [
                        'product' => $values->productRating ? [
                            'rate' => (int) $values->productRating->rating,
                            'max_rate' => 5,
                            'review' => $values->productRating->review,
                            'images' => $values->productRating->images->pluck('imageLink')->toArray()
                        ]: null,
                        'store' => $values->storeRating ? [
                            'rate' => (int) $values->storeRating->rating,
                            'max_rate' => 5,
                            'review' => $values->storeRating->review,
                            'images' => $values->storeRating->images->pluck('imageLink')->toArray()
                        ]: null,
                    ],
                    'complain_id' => $values->complainables->isNotEmpty() ? $values->complainables->first()->id : null
                ];

                array_push($product_arr, $product_info);
                unset($service_info);
            }

            return [
                'id' => $order->id,
                'transaction_id' => $order->transaction_id,
                'payment_status' => $order->payment_status,
                'items' => count($order->items),
                // 'currency' => $values->currency,
                'currency' => 'Rp',
                'total_price' => number_format($order->items->sum('total_product_price'), 2),
                'date' => $order->date,
                'shipping_info' => [
                    'shipping_name' => $shipping->ship_name ?? null,
                    'shipping_address' => $shipping_address,
                    'contact_no' => $shipping->ship_phone ?? null,
                ],
                'payment_info' => [
                    'total_items' => $order->items->count(),
                    'total_price' => rpFormat($order->order_amount),
                    'shipping_fee' => $order->shipping_amount,
                    'payment_method' => $order->pg_transaction ? $order->pg_transaction->payment_type . ' - ' . $order->pg_transaction->bank : null,
                    'grand_total' => rpFormat($order->charged_amount),
                ],
                'products' => $product_arr,
            ];
        });
    }
}