<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class RatingCollection extends Collection
{
    public function apiData()
    {
        $this->load('customer', 'images');

        return $this->map(function ($rating)
        {
            return [
                'id' => $rating->id,
                'rating' => (int) $rating->rating,
                'max_rating' => 5,
                'percentage' => $rating->percentage,
                'review' => $rating->review,
                'date' => $rating->created_at,
                'customer' => [
                    'id' => $rating->customer->cus_id,
                    'name' => $rating->customer->cus_name,
                    'avatar' => $rating->customer->getAvatarUrl()
                ],
                'images' => $rating->images->pluck('imageLink')->toArray()
            ];
        });
    }
}