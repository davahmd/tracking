<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class StoreCollection extends Collection
{
    public function apiData()
    {
        if ($this->isNotEmpty() && !$this->first()->relationLoaded('state')) {
            $this->load('state');
        }
        return $this->map(function ($store) {

            $rating = stdObject($store->compiledRatings(true));

            return [
                'id' => $store->stor_id,
                'name' => $store->stor_name,
                'image' => $store->getImageUrl(),
                'city' => title_case($store->stor_city_name),
                'state' => $store->state ? $store->state->name : null,
                'rating' => [
                    'rate' => $rating->rate,
                    'max_rate' => 5,
                    'total' => $rating->total,
                    'percentage' => $rating->percentage,
                    'cumulative' => $rating->cumulative,
                ]
            ];
        });
    }
}