<?php

namespace App\Providers;

use App\Extensions\AccessTokenGuard;
use App\Extensions\TokenUserProvider;
use App\Services\Jwt\Auth\JwtGuard;
use App\Services\Jwt\Auth\MemberJwtGuard;
use App\Services\Jwt\Auth\MerchantJwtGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        // Auth::extend('jwt', function ($app, $name, array $config) {
        //     return new JwtGuard(request());
        // });
        //
        // Auth::extend('jwt-member', function ($app, $name, array $config) {
        //     return new MemberJwtGuard(request());
        // });
        //
        // Auth::extend('jwt-merchant', function ($app, $name, array $config) {
        //     return new MerchantJwtGuard(request());
        // });
    }
}