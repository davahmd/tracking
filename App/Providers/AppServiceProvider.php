<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Hash;
use Blade;
use App\Repositories\CmsRepo;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('valid_hash', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });

        Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            // Contain at least one uppercase/lowercase letters and one number
            return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', (string)$value);
        });

        Validator::extend('username', function ($attribute, $value, $parameters, $validator) {
            // Contain at least one letters letters and number
            return preg_match('/^[a-zA-Z0-9]*$/', (string)$value);
        });

        Blade::directive('permission', function($expression) {
            return "<?php if(\Helper::adminPermission($expression)) { ?>";
        });

        Blade::directive('elsepermission', function() {
            return "<?php } else { ?>";
        });

        Blade::directive('endpermission', function () {
            return "<?php } ?>";
        });

        $footer_cms = CmsRepo::get_footer();
        View::share('footer_cms', $footer_cms);

        \Illuminate\Pagination\AbstractPaginator::defaultView("pagination::bootstrap-4");
        \Illuminate\Pagination\AbstractPaginator::defaultSimpleView("pagination::simple-bootstrap-4");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}