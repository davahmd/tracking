<?php

namespace App\Jobs;

use App\Models\ParentOrder;
use App\Services\Order\BaseProcessTransactionJob;
use Carbon\Carbon;
use Log;

class ProcessUnpaidTransaction extends BaseProcessTransactionJob
{
    /**
     * @return Illuminate\Database\Query\Builder
     */
    protected function query()
    {
        $toDateTimeString = Carbon::now()
            ->subDays(7)
            ->startOfDay()
            ->toDateTimeString();

        return ParentOrder::whereIn('payment_status', [
            ParentOrder::PAYMENT_STATUS_WAITING_PAYMENT_GATEWAY_NOTIFICATION,
            ParentOrder::PAYMENT_STATUS_PENDING
        ])->where('created_at', '<=', $toDateTimeString);
    }
}
