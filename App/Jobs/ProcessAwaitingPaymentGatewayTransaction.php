<?php

namespace App\Jobs;

use App\Models\ParentOrder;
use App\Services\Order\BaseProcessTransactionJob;

class ProcessAwaitingPaymentGatewayTransaction extends BaseProcessTransactionJob
{
    /**
     * @return Illuminate\Database\Query\Builder
     */
    protected function query()
    {
        return ParentOrder::whereIn('payment_status', [
            ParentOrder::PAYMENT_STATUS_WAITING_PAYMENT_GATEWAY_NOTIFICATION
        ]);
    }
}
