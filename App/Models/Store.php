<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Store extends Authenticatable
{
    use HasNewCollection;

    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;

    protected $table = 'nm_store';
    protected $primaryKey = 'stor_id';
    protected $fillable = ['stor_name', 'stor_phone', 'stor_address1', 'stor_address2', 'stor_zipcode' , 'stor_country' , 'stor_city' , 'stor_state' , 'stor_city_name' , 'stor_metadesc' , 'stor_metakeywords' , 'stor_website' , 'stor_type' , 'stor_merchant_id' , 'stor_img' , 'stor_status', 'stor_addedby', 'stor_latitude', 'stor_longitude', 'short_description', 'long_description','office_hour','featured', 'accept_payment', 'listed', 'service', 'stor_office_number', 'map_type', 'default_price', 'time_restriction', 'stor_subdistrict', 'url_slug'];

    public function limit()
    {
        return $this->hasOne(Limit::class, 'id', 'limit_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'co_id', 'stor_country');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'stor_state');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'stor_city');
    }

    public function subdistrict()
    {
        return $this->hasOne(Subdistrict::class, 'id', 'stor_subdistrict');
    }

    public function business_hours()
    {
        return $this->hasMany(StoreBusinessHour::class, 'store_id', 'stor_id');
    }

    public function offdays()
    {
        return $this->hasMany(Offday::class, 'store_id', 'stor_id');
    }

    public function storeBusinessHour($day, $field = null)
    {
        $business_hours = $this->business_hours;
        if($business_hours->isEmpty())
            return null;

        $business_hours = $business_hours->keyBy('day')->get($day);

        if(!$business_hours)
            return null;

        if(!$field)
            return $business_hours;

        if(!in_array($field, ['day', 'open_time', 'close_time', 'status']))
            return null;

        return $business_hours->{$field};
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'stor_merchant_id', 'mer_id');
    }

    public function storeServiceSchedule()
    {
        return $this->belongsTo(StoreServiceSchedule::class, 'id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'pro_sh_id', 'stor_id');
    }

    public function reviews()
    {
        return $this->hasMany(StoreReview::class, 'store_id', 'stor_id');
    }

    public function orders()
    {
        return $this->hasManyThrough(
            Order::class, Product::class,
            'pro_sh_id', 'order_pro_id', 'stor_id'
        );
    }

    public function chats()
    {
        return $this->hasMany(Chat::class, 'store_id', 'stor_id');
    }

    public function scopeActive($query)
    {
        return $query->where('stor_status', self::STATUS_ACTIVE);
    }

    public function soldItems()
    {
        return $this->orders()
            ->where(function ($query) {
                $query->where('order_status', Order::STATUS_COMPLETED)
                    ->orWhere(function ($query) {
                        $query->where('order_status', 7)
                            ->whereHas('services', function ($query) {
                                $query->where((new MemberServiceSchedule)->getTable() . '.status', 4);
                            });
                    });
            });

    }

    public function availableProducts()
    {
        return $this->products()->where('pro_status', Product::STATUS_ACTIVE);
    }

    public function newOrder()
    {
        return $this->orders()->where('order_status', Order::STATUS_PROCESSING);
    }

    public function getFullAddress()
    {
        $address = '';
        $address .= !empty($this->stor_address1) ? $this->stor_address1 . ', ' : '';
        $address .= !empty($this->stor_address2) ? $this->stor_address2 . ', ' : '';
        $address .= $this->stor_city_name ? $this->stor_city_name . ' ' : '';
        $address .= !empty($this->stor_zipcode) ? $this->stor_zipcode : '';

        return $address;
    }

    public function updateImage(UploadedFile $imageFile)
    {
        $oldImagePath = $this->getImagePath();
        $fileName = date('Ymd') . '_' . str_random(4) . '.' . $imageFile->getClientOriginalExtension();

        $imageFile->storeAs('store/' . $this->stor_merchant_id, $fileName);

        $this->update(['stor_img' => $fileName]);

        // remove old image file
        if (Storage::disk('s3')->exists($oldImagePath)) {
            Storage::disk('s3')->delete($oldImagePath);
        }
    }

    public function getImageUrl()
    {
        if (!$this->stor_img) {
            return null;
        }

        return Storage::url($this->getImagePath());
    }

    protected function getImagePath()
    {
        return 'store/' . $this->stor_merchant_id . '/' . $this->stor_img;
    }

    public function scopeNameContain($query, $string)
    {
        if ($string) {
            $query->where('stor_name', 'LIKE', '%' . $string . '%');
        }

        return $query;
    }

    public function scopeFeatured($query, $boolean = true)
    {
        if ($boolean) {
            $query->whereFeatured(true);
        }

        return $query;
    }

    public function services()
    {
        return $this->hasMany(MemberServiceSchedule::class, 'stor_id', 'stor_id');
    }

    public function storeUser()
    {
        return $this->belongsToMany(StoreUser::class, (new StoreUserMapping)->getTable(), 'store_id', 'storeuser_id');
    }

    public function newTransactions()
    {
        return $this->newOrder();
    }

    public function newServices()
    {
        return $this->services()
            ->where(function ($query) {
                $query->where((new MemberServiceSchedule)->getTable() . '.status', MemberServiceSchedule::STATUS_UNSCHEDULED)
                    ->whereHas('order', function ($query) {
                        $query->where((new Order)->getTable() . '.order_status', Order::STATUS_INSTALLATION);
                    })
                    ->orWhere(function ($query) {
                        $query->whereIn((new MemberServiceSchedule)->getTable() . '.status', [
                            MemberServiceSchedule::STATUS_UNSCHEDULED,
                            MemberServiceSchedule::STATUS_MEMBER_SCHEDULED,
                            MemberServiceSchedule::STATUS_MERCHANT_SCHEDULED
                        ]);
                    });
            });
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'reference_id', 'stor_id')->where('reference_type', Rating::REFERENCE_TYPE_STORE);
    }

    public function compiledRatings($array = false)
    {
        $ratings = $this->ratings()->getResults();

        $rate = !$ratings->count() ? 0 : round($ratings->sum('rating') / $ratings->count(), 1);
        $percentage = round($rate * 20, 2);

        $result = [
            'total' => $ratings->count(),
            'rate' => $rate,
            'max_rate' => 5,
            'percentage' => $percentage,
            'cumulative' => []
        ];

        for ($i=1; $i <= 5; $i++)
        {
            $total = $ratings->where('rating', $i)->count();

            $percentage = !$ratings->count() ?: round($total / $ratings->count() * 100, 2);

            $result['cumulative'][$i] = [
                'rate' => $i,
                'total' => $total,
                'percentage' => $percentage,
            ];
        }

        krsort($result['cumulative']);

        if(!$array)
            return json_decode(json_encode($result));

        $result['cumulative'] = array_values($result['cumulative']);

        return $result;
    }

    public function getSlugUrlAttribute()
    {
        return url("/store/{$this->url_slug}");
    }
}
