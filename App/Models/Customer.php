<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;

use App\Services\DeviceToken\Traits\HasDeviceToken;
use App\Services\Jwt\Auth\Traits\HasPersonalAccessToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MemberResetPassword as MemberResetPasswordNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Customer extends Authenticatable
{
    use Notifiable, HasPersonalAccessToken, HasDeviceToken;
    use GlobalEloquentBuilder, HasNewCollection;

    protected $table = 'nm_customer';
    protected $primaryKey = 'cus_id';

    protected $fillable = [
        'user_id', 'cus_name', 'email', 'password', 'username', 'cus_phone', 'cus_address1', 'cus_address2' , 'cus_country' , 'cus_city', 'cus_state', 'cus_city_name','payment_secure_code', 'cus_status', 'cus_postalcode', 'phone_area_code', 'cellphone_verified', 'question_1', 'answer_1', 'question_2', 'answer_2', 'question_3', 'answer_3', 'birthdate', 'gender', 'cus_joindate', 'cus_logintype', 'email_verified', 'identity_card', 'limit_id', 'update_flag', 'cus_subdistrict'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function info()
    {
        return $this->hasOne('App\Models\CustomerInfo');
    }

    public function shipping()
    {
        return $this->hasMany('App\Models\Shipping', 'ship_cus_id', 'cus_id')->orderBy('isdefault', 'desc');
    }

    public function customer_wallets()
    {
        return $this->hasMany('App\Models\CustomerWallet', 'customer_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'co_id', 'cus_country');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'cus_state');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'cus_city');
    }

    public function subdistrict()
    {
        return $this->hasOne(Subdistrict::class, 'id', 'cus_subdistrict');
    }

    public function limit()
    {
        return $this->hasOne(Limit::class, 'id', 'limit_id');
    }

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'customer_vehicle_mappings', 'customer_id', 'vehicle_id')->withPivot('id', 'isdefault', 'name', 'insurance', 'insurance_expired_date', 'road_tax_expired_date', 'mileage')->withTimestamps();
    }

    public function cart()
    {
        return $this->hasMany(Cart::class, 'cus_id', 'cus_id');
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'cus_id');
    }

    public function services()
    {
        return $this->hasMany(MemberServiceSchedule::class, 'cus_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'order_cus_id');
    }

    public function negotiation()
    {
        return $this->hasMany(PriceNegotiation::class, 'customer_id', 'cus_id');
    }
    /* End Relationships */

    public function phone()
    {
        return $this->{'phone_area_code'} . $this->{'cus_phone'};
    }

    /**
     * All Complains's model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function complains()
    {
        return $this->morphMany(Complain::class, 'complainant');
    }

    /**
     * Complain's messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function responders()
    {
        return $this->morphMany(ComplainMessage::class, 'responder');
    }

    public function addShippingAddress(Shipping $shippingAddress)
    {
        return DB::transaction(function () use ($shippingAddress) {
            $this->shipping()->save($shippingAddress);

            if ($shippingAddress->isdefault == 1) {
                $this->shipping()
                    ->where('ship_id', '<>', $shippingAddress->ship_id)
                    ->update(['isdefault' => 0]);
            }

            return $shippingAddress;
        });
    }

   public function routeNotificationForMail()
   {
       return $this->user->email;
   }

    /**
     * This is to replace member reset password link in the email
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MemberResetPasswordNotification($token));
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'customer.'.$this->cus_id;
    }

    public function getAvatarUrl()
    {
        return empty($this->cus_pic) ? asset('asset/images/icon/icon_account2.png') : Storage::url('gallery/customer/'. $this->cus_pic);
    }

    public function scopeSearch($query, $string)
    {
        return $query->has('user')
            ->where(function ($query) use ($string) {
                $query->where('cus_name', 'LIKE', '%' . $string . '%')
                    ->orWhereHas('user', function ($query) use ($string) {
                        return $query->where('email', 'LIKE', '%' . $string . '%');
                });
            });

    }
}
