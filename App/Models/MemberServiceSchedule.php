<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use App\Notifications\ServiceCancelled;
use App\Notifications\ServiceRescheduled;
use App\Notifications\ServiceScheduleReceived;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

use App\Models\Order;

use Helper;
use Illuminate\Support\Facades\Notification;

class MemberServiceSchedule extends Model
{
    const STATUS_UNSCHEDULED = 0;
    const STATUS_MEMBER_SCHEDULED = 1;
    const STATUS_MERCHANT_SCHEDULED = 2;
    const STATUS_CONFIRMED = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_CANCELLED = -1;

    protected $table = 'member_service_schedule';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    protected $fillable = ['cus_id', 'order_id', 'stor_id', 'store_service_extra_id', 'service_name_current', 'schedule_datetime', 'reschedule_count_member', 'reschedule_count_merchant', 'status'];
    protected $dates = ['created_at', 'updated_at'];

    static function status() 
    {
        return [
            -1 => trans('localize.cancelled'),
            0 => trans('localize.unscheduled'),
            1 => trans('localize.scheduled'),
            2 => trans('localize.scheduled'),
            3 => trans('localize.confirmed'),
            4 => trans('localize.completed'),
        ];
    }
    
    public function order(array $arrFields = null)
    {
        $query = $this->belongsTo(Order::class, 'order_id', 'order_id');

        if (!is_null($arrFields)) {
            return $query->select($arrFields);
        }

        return $query;
    }

    public function customer(array $arrFields = null)
    {
        $query = $this->belongsTo(Customer::class, 'cus_id');

        if (!is_null($arrFields)) {
            return $query->select($arrFields);
        }

        return $query;
    }

    public function store(array $arrFields = null)
    {
        $query = $this->belongsTo(Store::class, 'stor_id', 'stor_id');

        if (!is_null($arrFields)) {
            return $query->select($arrFields);
        }

        return $query;
    }

    public function mainImage()
    {
        return $this->hasOne(ProductImage::class, 'pro_id', 'pro_id')->where('main', true);
    }

    public function order_item()
    {
        return $this->belongsTo(Order::class, 'order_id', 'order_id');
    }

    public function mainImageUrl()
    {
        if ($this->mainImage) {
            return \Storage::url('product/' . $this->pro_mr_id . '/' . $this->mainImage->image);
        }

        return null;
    }
}
