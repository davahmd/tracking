<?php

namespace App\Models;

use App\Services\Jwt\Token;
use Illuminate\Database\Eloquent\Model;

class PersonalAccessToken extends Model
{
    public $incrementing = false;

    protected $table = 'personal_access_token';

    protected $guarded = [];

    protected $dates = ['expires_in'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->id = (new Token)->generateIdentifier();
        });
    }

    public function user()
    {
        return $this->morphTo();
    }
}
