<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionLog extends Model
{
    protected $table = 'promotion_logs';
    protected $fillable = ['promotion_id', 'action', 'initiator_type', 'initiator_uid', 'approver_uid', 'reason'];
    protected $primaryKey = 'id';

    public function promotion()
    {
    	return $this->belongsTo(Promotion::class, 'promotion_id', 'id');
    }
}
