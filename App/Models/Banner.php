<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use App\Traits\GlobalEloquentBuilder;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Banner extends Authenticatable
{
    use GlobalEloquentBuilder, HasNewCollection;

    protected $table = 'nm_banner';
    protected $fillable = ['bn_title', 'bn_img', 'bn_status', 'bn_redirecturl', 'bn_open', 'type', 'order', 'bn_type'];
    protected $primaryKey = 'bn_id';

    public function countries()
    {
        return $this->belongsToMany('App\Models\Country', 'nm_banner_countries', 'banner_id', 'country_id');
    }

    public function scopeActive($query)
    {
    	return $query->where('bn_status', true);
    }
}
