<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;
use App;

class OfficialBrands extends Model
{
    use GlobalEloquentBuilder, HasNewCollection;

    protected $table = 'official_brands';
    protected $guarded = ['brand_id'];
    protected $primaryKey = 'brand_id';

    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id', 'brand_id');
    }

    public function scopeActive($query)
    {
        return $query->where('brand_status', true);
    }

    public function status()
    {
        switch ($this->brand_status)
        {
            case '0':
                return trans('localize.Inactive');
                break;

            case '1':
                return trans('localize.Active');
                break;

            default:
                return '';
                break;
        }
    }

    public function getLogoPathAttribute()
    {
        $url = \Storage::url('officialbrands/');
        if(!$this->{'brand_Img'})
        {
            return url('common/images/stock.png');
        }

        return $url . $this->{'brand_Img'};
    }
    
    public function scopeNameContain($query, $string)
    {
        if ($string) {
            $query->where('brand_name', 'LIKE', '%' . $string . '%');
        }

        return $query;
    }

    public function scopeHasProductId($query, $data)
    {
        return $query->whereHas('products', function ($query) use ($data) {
            if (is_array($data)) {
                $query->whereIn((new Product)->getTable() . '.pro_id', $data);
            }
            else {
                $query->where((new Product)->getTable() . '.pro_id', $data);
            }

            $query->has('pricing');
        });
    }

    public function scopeHasActiveProducts($query)
    {
        return $query->whereHas('products', function ($query) {
            $query->where('pro_status', Product::STATUS_ACTIVE)->has('pricing');
        });
    }
}
