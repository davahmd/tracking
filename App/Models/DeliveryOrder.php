<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model
{
    protected $table = 'delivery_orders';
    protected $primaryKey = 'id';

    public function items()
    {
        return $this->belongsToMany(Order::class, 'order_mappings', 'delivery_order_id', 'order_id');
    }

    public function invoice()
    {
        return $this->belongsTo(TaxInvoice::class, 'tax_invoice_id');
    }

    public function do_number()
    {
        // return 'ON'.$this->{'do_number'};
        if($this->invoice)
        {
            return 'ON'.$this->invoice->tax_number;
        }

        return '';
    }

    public function courier()
    {
        return $this->hasOne(Courier::class, 'id', 'courier_id');
    }

    public function shipment_type()
    {
        if(!$this->{'courier_id'} && !$this->{'tracking_number'})
        {
            return trans('localize.self_pickup');
        }

        return trans('localize.by_courier');
    }
}
