<?php

namespace App\Models;

use App\Exceptions\IntegrityException;
use Illuminate\Database\Eloquent\Model;

class ComplainSubject extends Model
{
    protected $fillable = [
        'title'
    ];

    /**
     * Get complain under which title subject.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complains()
    {
        return $this->hasMany(Complain::class, 'subject_id', 'id');
    }

    /**
     * Get all complain subject along with its underlying complains.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function showAll()
    {
        $builder = $this
            ->with(['complains'])
            ->withCount(['complains']);

        return $builder->paginate();
    }

    /**
     * Create new resource.
     *
     * @param $attributes
     * @return ComplainSubject
     * @throws \Exception
     */
    public function createNewSubject($attributes)
    {
        try {
            $instance = new static();

            \DB::transaction(function () use ($instance, $attributes) {
                $instance->create($attributes);
            });

        } catch (\Exception $e) {
            throw $e;
        }

        return $instance;
    }

    /**
     * Update existing resource.
     *
     * @param ComplainSubject $complainSubject
     * @param $attributes
     * @return ComplainSubject
     * @throws \Exception
     */
    public function edit(ComplainSubject $complainSubject, $attributes)
    {
        try {
            \DB::transaction(function () use ($complainSubject, $attributes) {
                $complainSubject->update($attributes);
            });

        } catch (\Exception $e) {
            throw $e;
        }

        return $complainSubject;
    }

    /**
     * Remove model resource.
     *
     * @param ComplainSubject $complainSubject
     * @return bool
     * @throws \Exception
     */
    public function remove(ComplainSubject $complainSubject)
    {
        try {
            $flag = false;

            \DB::transaction(function () use ($complainSubject, &$flag) {

                if ($complainSubject->isAbleToDelete($complainSubject)) {
                    $complainSubject->complains()->delete();

                    $flag = $complainSubject->delete();
                } else {
                    throw new IntegrityException(trans('localize.Complain_title_is_not_deleted'));
                }
            });

        } catch (\Exception $e) {
            throw $e;
        }

        return $flag;
    }

    /**
     * Check whether this model tied to any complain.
     *
     * @param ComplainSubject $complainSubject
     * @return bool
     */
    public function isAbleToDelete(ComplainSubject $complainSubject)
    {
        return $complainSubject->complains()->count() > 0 ? false : true;
    }
}
