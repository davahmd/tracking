<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComplainOrder extends Model
{
    protected $table = 'complain_orders';

    protected $fillable = [
        'nm_order_id',
        'complain_id'
    ];

    /**
     * Get related order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'nm_order_id', 'order_id');
    }
}
