<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComplainMessage extends Model
{
    protected $fillable = [
        'responder_id',
        'responder_type',
        'message',
        'is_creator',
        'complain_id',
    ];

    /**
     * User who wrote down the message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function responder()
    {
        return $this->morphTo('responder');
    }

    /**
     * Decide responder is an admin or regular users.
     *
     * @return bool
     */
    public function isResponderIsAnAdmin()
    {
        return $this->responder instanceof Admin ? true : false;
    }

    /**
     * Decide responder is an merchant or regular users.
     *
     * @return bool
     */
    public function isResponderIsAMerchant()
    {
        return $this->responder instanceof Merchant ? true : false;
    }

    /**
     * Get responder's name.
     *
     * @return mixed|string
     */
    public function getResponderNameAttribute()
    {
        $name = 'Anonymous';

        if ($this->responder instanceof Customer) {
            $name = $this->responder->cus_name;

        } elseif ($this->responder instanceof Admin) {
            $name = $this->responder->adminName();

        } elseif ($this->responder instanceof Merchant) {
            $name = $this->responder->merchantName();
        }

        return $name;
    }
}
