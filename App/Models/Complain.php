<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\Complain\CustomerReceivedComplainMessage;
use Illuminate\Http\Request;

class Complain extends Model
{
    protected $uploadPath = 'complain/images/';

    const MAX_UPLOAD_LIMIT = 5;

    protected $fillable = [
        'complainant_id',
        'complainant_type',
        'complainable_id',
        'complainable_type',
        'subject_id',
        'status',
        'is_escalated'
    ];

    public static function boot()
    {
        parent::boot();

        self::deleted(function(self $complain) {
            $complain->complain_messages()->delete();

            $complain->complain_orders()->delete();

            $complain->complain_images()->delete();
        });
    }

    /**
     * Complain about what(product, order, etc)
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function complainable()
    {
        return $this->morphTo('complainable');
    }

    /**
     * Get someone who make a complain.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function complainant()
    {
        return $this->morphTo('complainant');
    }

    /**
     * Get all customer date from 'complainant'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'complainant_id')
            ->where(sprintf('%s.%s', $this->getTable(), 'complainant_type'), Customer::class);
    }

    /**
     * Get all parent order from 'complainable'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent_order()
    {
        return $this->belongsTo(ParentOrder::class, 'complainable_id')
            ->where(sprintf('%s.%s', $this->getTable(), 'complainable_type'), ParentOrder::class);
    }

    /**
     * Get all order from 'complainable'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'complainable_id')
            ->where(sprintf('%s.%s', $this->getTable(), 'complainable_type'), Order::class);
    }

    /**
     * Complain about what category(title).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo(ComplainSubject::class);
    }

    /**
     * All complain's messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complain_messages()
    {
        return $this->hasMany(ComplainMessage::class);
    }

    /**
     * Get complain's related order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complain_orders()
    {
        return $this->hasMany(ComplainOrder::class);
    }

    /**
     * Get all complain's images.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complain_images()
    {
        return $this->hasMany(ComplainImage::class);
    }

    /**
     * Is complainable has existing complain or not.
     *
     * @param Model $complainable | Could be ParentOrder or Order.
     * @return bool
     */
    public function isComplainExist(Model $complainable)
    {
        return $complainable->complainables->isNotEmpty()
            ? true
            : false;
    }

    /**
     * Adding new complain data.
     *
     * @param Model $complainable | Could be ParentOrder, Order, etc.
     * @param Model $complainant | Someone who initiated the complain. Can be customer, merchant, admin, etc.
     * @param $attributes
     * @return |null
     * @throws \Exception
     */
    public function addNewComplainByComplainable(Model $complainable, Model $complainant, array $attributes)
    {
        try {
            $instant = new static;

            \DB::transaction(function() use (&$instant, $attributes, $complainable, $complainant) {
                $instant->setComplainantInputs($complainant, $attributes);

                $instant = $complainable->complainables()->create($attributes);

                $instant->addNewComplainMessage($instant, $complainant, $attributes);

                $instant->complain_orders()->create($attributes);
            });

        } catch (\InvalidArgumentException $e) {
            throw $e;

        } catch (\Exception $e) {
            throw $e;
        }

        return $instant;
    }

    /**
     * Adding new complain by complainant.
     *
     * @param Model $complainant | Someone who initiated the complain. Can be customer, merchant, admin, etc.
     * @param Model $complainable | Could be ParentOrder, Order, etc.
     * @param $attributes
     * @return |null
     * @throws \Exception
     */
    public function addNewComplainByComplainant(Model $complainant, Model $complainable, array $attributes)
    {
        try {
            $instant = new static;

            \DB::transaction(function() use (&$instant, $attributes, $complainable, $complainant) {
                $instant->setComplainableInputs($complainable, $attributes);

                $instant = $complainant->complains()->create($attributes);

                $instant->addNewComplainMessage($instant, $complainant, $attributes);

                //$instant->complain_orders()->create($attributes);
            });

        } catch (\InvalidArgumentException $e) {
            throw $e;

        } catch (\Exception $e) {
            throw $e;
        }

        return $instant;
    }

    /**
     * Set complainable morph data(Product, Order, or any related model).
     *
     * @param Model $complainable
     * @param $attributes
     * @return mixed
     */
    public function setComplainableInputs(Model $complainable, &$attributes)
    {
        $attributes['complainable_id'] = $complainable->getKey();
        $attributes['complainable_type'] = get_class($complainable);

        return $attributes;
    }

    /**
     * Set complainant morph data(User, Customer, Admin, Merchant, or any related model).
     *
     * @param Model $complainable
     * @param $attributes
     * @return mixed
     */
    public function setComplainantInputs(Model $complainant, &$attributes)
    {
        $attributes['complainant_id'] = $complainant->getKey();
        $attributes['complainant_type'] = get_class($complainant);

        return $attributes;
    }

    /**
     * Create new single complain message.
     *
     * @param Complain $complain
     * @param $attributes
     * @return Model
     */
    public function addNewComplainMessage(Complain $complain, Model $responder, $attributes)
    {
        $attributes['responder_id'] = $responder->getKey();
        $attributes['responder_type'] = get_class($responder);

        $complainMessage = $complain->complain_messages()->create($attributes);

        // Only send email to customer when admin or merchant reply to the complain.
        if ($complainMessage->isResponderIsAnAdmin() || $complainMessage->isResponderIsAMerchant()) {
            $complain->complainant->notify(
                new CustomerReceivedComplainMessage($complain, $complain->complainant)
            );
        }

        return $complainMessage;
    }

    /**
     * Get complainant name.
     *
     * @return mixed|string
     */
    public function getComplainantNameAttribute()
    {
        $name = 'Anonymous';

        if ($this->complainant instanceof Customer) {
            $name = $this->complainant->cus_name;

        } elseif ($this->complainant instanceof Admin) {
            $name = $this->complainant->adminName();

        } elseif ($this->complainant instanceof Merchant) {
            $name = $this->complainant->merchantName();
        }

        return $name;
    }

    /**
     * Get complainant name.
     *
     * @return mixed|string
     */
    public function getComplainantEmailAttribute()
    {
        $email = 'Anonymous';

        if ($this->complainant instanceof Customer) {
            $email = $this->complainant->user->email;

        } elseif ($this->complainant instanceof Admin) {
            $email = $this->complainant->adm_email;

        } elseif ($this->complainant instanceof Merchant) {
            $email = $this->complainant->email;
        }

        return $email;
    }

    /**
     * Show all complain resources.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function showAll($onlyMerchant = false)
    {
        $builder = $this
            ->with(['complainant', 'complainable', 'subject', 'complain_images'])
            ->whereHas('order.product.merchant', function($query) use ($onlyMerchant) {
                $search = request('merchant_search');

                if ($onlyMerchant) {
                    $merchant = auth()->user() ? auth()->user()->merchant : auth('api')->user()->authMerchantUser();
                    $query->where('mer_id', $merchant->getKey());
                }

                if (request()->exists('merchant_search') && $search) {
                    return $query
                        ->where('email', 'like', $search . '%')
                        ->orWhere('mer_fname', '=', $search)
                        ->orWhere('mer_lname', '=', $search)
                        ->orWhere('mer_phone', '=', $search);
                }

            })
            ->whereHas('customer', function($query) {
                $search = request('customer_search');

                if (request()->exists('customer_search') && $search) {
                    return $query
                        ->where('cus_name', 'like', $search . '%')
                        ->orWhere('email', '=', $search)
                        ->orWhere('cus_phone', '=', $search);
                }
            })
            ->wherehas('order', function($query) {
                $search = request('transaction_id');

                if (request()->exists('transaction_id') && $search) {
                    return $query
                        ->where('transaction_id', $search);
                }
            })
            ->wherehas('subject', function($query) {
                $search = request('subject_id');

                if (request()->exists('subject_id') && $search) {
                    return $query
                        ->where('subject_id', $search);
                }
            })
            ->withCount(['complain_messages'])
            ->orderBy('id', 'DESC');

        return $builder->paginate(10);
    }

    /**
     * Get complainable route for details view.
     *
     * @return string|null
     */
    public function getComplainableRoute()
    {
        $route = null;

        if ($this->complainable instanceof ParentOrder) {
            $route = route('transaction.online.listing', ['transaction_id' => $this->complainable->transaction_id]);

        } elseif ($this->complainable instanceof Order) {
            // Skip as for now. May need to add when ready to use.
            $route = null;

        } else {
            // Skip as for now
            $route = null;
        }

        return $route;
    }

    /**
     * Get complainable route for details view.
     *
     * @return string|null
     */
    public function getOrderListingRouteByTransactionId()
    {
        $route = null;

        if ($this->complainable instanceof ParentOrder || $this->complainable instanceof Order) {
            $route = route('transaction.online.listing', ['transaction_id' => $this->complainable->transaction_id]);

        } else {
            // Skip as for now
            $route = null;
        }

        return $route;
    }

    /**
     * Get complainant route(details complainant).
     *
     * @return mixed|string
     */
    public function getComplainantRoute()
    {
        $route = null;

        if ($this->complainant instanceof Customer) {
            $route = route('customer::view', $this->complainant->getKey());

        } elseif ($this->complainant instanceof Admin) {
            // Skip as for now. May need to add when ready to use.
            $route = null;

        } else {
            // Skip as for now. May need to add when ready to use.
            $route = null;
        }

        return $route;
    }

    /**
     * Get merchant's route if `complainable` is Order Model.
     *
     * @return string|null
     */
    public function getMerchantRoute()
    {
        $route = null;

        if ($this->complainable instanceof Order) {
            $route = route('admin.merchant.view', $this->complainable->product->merchant->getKey());
        }

        return $route;
    }

    /**
     * Get latest complain messages.
     *
     * @param Complain $complain
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getLatestMessages(Complain $complain, $perPage = 5)
    {
        return $complain->complain_messages()->orderBy('id', 'DESC')->paginate($perPage);
    }

    /**
     * Delete existing complains.
     *
     * @param Complain $complain
     * @return bool
     * @throws \Exception
     */
    public function deleteComplain(Complain $complain)
    {
        try {
            $flag = false;

            \DB::transaction(function ($complain) use (&$flag) {
                $flag = $complain->delete();
            });
        } catch (\InvalidArgumentException $e) {
            throw $e;

        } catch (\Exception $e) {
            throw $e;
        }

        return $flag;
    }

    /**
     * Upload attachments.
     *
     * @param Complain $complain
     * @param $attachments
     */
    public function uploadAttachments(Complain $complain, $attachments)
    {
        if (!empty($attachments)) {
            foreach ($attachments as $file) {
                $fileName = str_random(10)."_".date('YmdHis');
                $path = $this->uploadPath . $complain->id;
                $upload = uploadImage($file, $path, $fileName);

                if($upload->status) {
                    $image = $upload->file_name;

                    $complain->complain_images()->create([
                        'image' => $image,
                        'path' => $path,
                    ]);
                }
            }
        }
    }

    /**
     * Toggle is_escalated(escalated to admin).
     *
     * @return Complain
     * @throws \Exception
     */
    public function toggleEscalate()
    {
        $instance = $this->getComplainInstance();

        try {
            \DB::transaction(function() use ($instance) {
                $instance->update([
                    'is_escalated' => !$instance->is_escalated
                ]);
            });

        } catch (\Exception $e) {
            throw $e;
        }

        return $instance;
    }

    /**
     * Check either complainable is exactly from creator or not.
     *
     * @param Model $complainable
     * @param Customer $customer
     * @return bool
     */
    public function isMyComplainable(Model $complainable, Customer $customer)
    {
        if ($complainable instanceof Order) {
            $customerId = $complainable->order_cus_id;

        } elseif ($complainable instanceof ParentOrder) {
            $customerId = $complainable->customer_id;

        } else {
            // skip
        }

        if ($customerId != $customer->getKey()) {
            return false;
        }

        return true;
    }

    /**
     * Check whether complain is exactly from creator or not. For complainant use only.
     *
     * @param Complain $complain
     * @param Customer $customer
     * @return bool
     */
    public function isMyComplain(Customer $customer, $complain = null)
    {
        $instance = $this->getComplainInstance($complain);

        if (!$instance->complainant->is($customer)) {
            return false;
        }

        return true;
    }

    /**
     * Is this complaint was raise up for merchant. For merchant use only.
     *
     * @param Merchant $merchant
     * @param null $complain
     * @return bool
     */
    public function isComplainRaiseUpForMe(Merchant $merchant, $complain = null)
    {
        $instance = $this->getComplainInstance($complain);

        if (!$instance->complainable->product->merchant->is($merchant)) {
            return false;
        }

        return true;
    }

    /**
     * Get complain instance.
     *
     * @param null $complain
     * @return Complain|Complain[]|\Illuminate\Database\Eloquent\Collection|Model|null|integer
     */
    public function getComplainInstance($complain = null)
    {
        $instance = null;

        if (is_null($complain)) {
            $instance = $this;

        } else {
            if ($complain instanceof Complain && $complain->exists()) {
                $instance = $complain;
            } else {
                $instance = (new static())->find($complain);
            }
        }

        return $instance;
    }
}
