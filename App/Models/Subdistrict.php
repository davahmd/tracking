<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    protected $table = 'subdistrict';
    protected $fillable = ['name', 'city_id', 'status'];
    protected $primaryKey = 'id';

    public function city()
    {
    	return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->whereStatus(true);
    }
}
