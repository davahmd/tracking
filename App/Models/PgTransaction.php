<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;

class PgTransaction extends Model
{
    use GlobalEloquentBuilder;

    protected $table = 'pg_transaction';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function parent_order()
    {
        return $this->belongsTo(ParentOrder::class, 'id', 'order_id');
    }
}
