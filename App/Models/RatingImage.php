<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingImage extends Model
{
    protected $table = 'rating_images';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    function getImageLinkAttribute()
    {
        return \Storage::url("{$this->path}/{$this->image}");
    }
}
