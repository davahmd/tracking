<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionProduct extends Model
{
    protected $table = 'promotion_products';
    protected $fillable = ['promotion_id', 'product_id', 'limit', 'count'];
    protected $primaryKey = 'id';

    public function promotion()
    {
    	return $this->belongsTo(Promotion::class, 'promotion_id', 'id');
    }

    public function detail()
    {
    	return $this->belongsTo(Product::class, 'product_id', 'pro_id')->select('pro_id AS id', 'pro_title_en');
    }

    public function product_details()
    {
        return $this->belongsTo(Product::class, 'product_id', 'pro_id');
    }

}
