<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ProductImage extends Authenticatable
{

    protected $table = 'nm_product_image';
    protected $fillable = ['pro_id', 'title','image','status','order','main'];
    protected $primaryKey = 'id';

    public function product()
    {
        return $this->hasOne(Product::class, 'pro_id', 'pro_id');
    }

    public function getImagePathAttribute()
    {
        $product = $this->product()->getResults();
        if(!$this->{'image'} || !$product || !$product->pro_mr_id)
        {
            return url('common/images/stock.png');
        }

        return \Storage::url('product/' . $product->pro_mr_id . '/' . $this->image);
    }

    public function getImageLinkAttribute($value)
    {
        $image = $this->{'image'};
        $product = $this->product()->getResults();

        if (!$image) {
            return url('common/images/stock.png');
        }

        if (str_contains($image, 'https://') || str_contains($image, 'http://')) {
            return $image;
        }

        // if (!@file_get_contents($image)) {
        //     return url('common/images/stock.png');
        // }

        return \Storage::url("product/$product->pro_mr_id/$image");
    }
}