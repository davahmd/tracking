<?php

namespace App\Models;

use App\Services\DeviceToken\Traits\HasDeviceToken;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    const STATUS_BLOCK = 0;
    const STATUS_UNBLOCK = 1;

    const ACTIVATION_TRUE = 1;
    const ACTIVATION_FALSE = 0;

    const TYPE_CUSTOMER = 'C';
    const TYPE_MERCHANT = 'M';
    const TYPE_STOREUSER = 'S';

    use Notifiable, HasApiTokens, HasDeviceToken;

    protected $fillable = [
        'email', 'password', 'status', 'remember_token', 'activation'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * Relationship
     */

    public function customer()
    {
        return $this->hasOne(Customer::class, 'user_id', 'id');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class);
    }

    public function storeUser()
    {
        return $this->hasOne(StoreUser::class);
    }

    public function scopeCustomerId($query, $id)
    {
        return $query->whereHas('customer', function ($query) use ($id) {
            return $query->where('cus_id', $id);
        });
    }

    public function authMerchantUser()
    {
        if ($this->merchant) {
            return $this->merchant;
        }
        elseif ($this->storeUser) {
            return $this->storeUser;
        }

        return null;
    }

    public function isRetailerForDistributor(Merchant $distributor)
    {
        if ($this->merchant) {
            return $distributor->approvedRetailers()->where('mer_id', $this->merchant->mer_id)->exists();
        }

        return false;
    }
}
