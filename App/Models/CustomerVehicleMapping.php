<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerVehicleMapping extends Model
{
    protected $table = 'customer_vehicle_mappings';
    protected $guarded = ['id'];
    protected $fillable = ['customer_id','vehicle_id','isdefault','name','insurance','insurance_expired_date','road_tax_expired_date','mileage'];

    protected $primaryKey = 'id';

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'cus_id');
    }

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'id', 'vehicle_id');
    }

    public function scopeNotDefault($query)
    {
        return $query->where('isdefault', 0);
    }
}
