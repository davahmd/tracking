<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;

class ParentOrder extends Model
{
    const PAYMENT_STATUS_WAITING_PAYMENT_GATEWAY_NOTIFICATION = 0;
    const PAYMENT_STATUS_CAPTURED = 1;
    const PAYMENT_STATUS_CHALLENGED = 2;
    const PAYMENT_STATUS_SETTLEMENT = 3;
    const PAYMENT_STATUS_PENDING = 4;
    const PAYMENT_STATUS_DENY = 5;
    const PAYMENT_STATUS_EXPIRED = 6;
    const PAYMENT_STATUS_CANCELED = 7;
    const PAYMENT_STATUS_NOT_FOUND_OR_CANCELED_BY_SYSTEM = 8;

    use GlobalEloquentBuilder, HasNewCollection;

    protected $table = 'parent_orders';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function customer()
    {
        return $this->hasOne(Customer::class, 'cus_id', 'customer_id');
    }

    public function items()
    {
        return $this->hasMany(Order::class, 'parent_order_id', 'id');
    }

    public function shipping_address()
    {
        return $this->hasOne(Shipping::class, 'parent_order_id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(TaxInvoice::class, 'parent_order_id');
    }

    /**
     * Get all complains. But by default one order should have one complain only.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function complainables()
    {
        return $this->morphMany(Complain::class, 'complainable');
    }

    /**
     * First complain created by parent order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function getFirstComplain()
    {
        return $this->complainables()->orderBy('id', 'ASC')->limit(1);
    }

    public function getIsDeliveryOrderExistAttribute()
    {
        $flagExist = false;

        foreach($this->invoices as $taxInvoice) {
           if ($taxInvoice->deliveries->count()) {
                $flagExist = true;
           }
        }

        return $flagExist;
    }

    public function deliveries()
    {
        return $this->hasManyThrough(DeliveryOrder::class, TaxInvoice::class, 'parent_order_id', 'tax_invoice_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany(OrderOnlineLog::class, 'parent_order_id', 'id');
    }

    public function pg_transaction()
    {
        return $this->hasOne(PgTransaction::class, 'order_id', 'id');
    }

    public function updateCompletedNegotiationStatus()
    {
        if ($this->items) {
            foreach ($this->items as $order) {
                if ($order->negotiation) {
                    if ($order->negotiation->status == PriceNegotiation::STATUS['ACCEPTED_BY_MERCHANT']) {
                        $newStatus = PriceNegotiation::STATUS['COMPLETED_WITH_ORIGINAL_PRICE'];
                    }
                    elseif ($order->negotiation->status == PriceNegotiation::STATUS['ACCEPTED_BY_CUSTOMER']) {
                        $newStatus = PriceNegotiation::STATUS['COMPLETED_WITH_NEGOTIATED_PRICE'];
                    }

                    $order->negotiation->update([
                        'status' => $newStatus
                    ]);
                }
            }
        }
    }
}
