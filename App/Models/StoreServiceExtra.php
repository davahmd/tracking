<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class StoreServiceExtra extends Authenticatable
{

    protected $table = 'store_service_extra';
    protected $primaryKey = 'id';
    protected $fillable = ['store_service_schedule_id', 'pro_id', 'service_name', 'service_price'];

}
