<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /* Relationships */
    public function customer(array $arrFields = null)
    {
        return $this->belongsTo(Customer::class, 'cus_id', 'cus_id');
    }

    public function store(array $arrFields = null)
    {
        return $this->belongsTo(Store::class, 'store_id', 'stor_id');
    }

    public function product(array $arrFields = null)
    {
        return $this->belongsTo(Product::class, 'pro_id', 'pro_id');
    }

    public function schedule(array $arrFields = null)
    {
        return $this->belongsTo(MemberServiceSchedule::class, 'schedule_id');
    }
    /* End Relationships */
}