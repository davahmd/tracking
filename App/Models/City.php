<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class City extends Authenticatable
{

    protected $table = 'city';
    protected $fillable = ['name', 'state_id', 'status'];
    protected $primaryKey = 'id';

    public function state()
    {
    	return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function subdistricts()
    {
    	return $this->hasMany(Subdistrict::class, 'city_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->whereStatus(true);
    }
}
