<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class PricingAttributeMapping extends Authenticatable
{
    protected $table = 'nm_pricing_attribute_mappings';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public function product_attribute()
    {
        return $this->hasOne(ProductAttribute::class, 'id', 'attribute_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'pro_id', 'pro_id');
    }

    public function pricing()
    {
        return $this->belongsTo(ProductPricing::class, 'pricing_id');
    }
}