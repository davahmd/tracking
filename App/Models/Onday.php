<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Onday extends Model
{
    protected $table = 'ondays';
    protected $guarded = [];
}
