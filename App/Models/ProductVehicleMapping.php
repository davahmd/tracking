<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVehicleMapping extends Model
{
	protected $table = 'product_vehicle_mappings';
	protected $guarded = ['id'];
	protected $primaryKey = 'id';
}
