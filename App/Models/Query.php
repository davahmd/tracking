<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $table = 'queries';
    protected $guarded = [];

    const STATUS = [
        'ACTIVE' => 1,
        'REPLIED' => 2,
    ];

    public function admin()
    {
        return $this->hasOne(Admin::class, 'adm_id', 'replied_by');
    }
}
