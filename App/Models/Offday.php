<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offday extends Model
{
    
    protected $table = 'offdays';
    protected $guarded = [];

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'mer_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'stor_id');
    }
}
