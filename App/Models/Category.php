<?php

namespace App\Models;

use App;
use App\Collections\Traits\HasNewCollection;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasNewCollection;

    protected $guarded = ['created_at', 'updated_at'];

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne('App\Models\Category', 'id', 'parent_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'nm_product_category', 'category_id', 'product_id')->withTimestamps();
    }

    public function getNameAttribute($value)
    {
        $default = $this->{'name_en'};
        $title = $this->{'name_'.App::getLocale()};

        if (empty($title)) {
            return $default;
        } else {
            return $title;
        }
    }

    public function wallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function isMainCategory()
    {
        if ($this->parent_id == 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public function scopeHasProductId($query, $data)
    {
        return $query->whereHas('products', function ($query) use ($data) {
            if (is_array($data)) {
                $query->whereIn((new Product)->getTable() . '.pro_id', $data);
            }
            else {
                $query->where((new Product)->getTable() . '.pro_id', $data);
            }
        });
    }

    public function scopeHasActiveProducts($query)
    {
        return $query->whereHas('products', function ($query) {
            $query->where('pro_status', Product::STATUS_ACTIVE)->has('pricing');
        });
    }
}