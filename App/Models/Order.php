<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use App\Repositories\AttributeRepo;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;

class Order extends Model
{
    use GlobalEloquentBuilder, HasNewCollection;

    const STATUS_UNPAID = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_PACKAGING = 2;
    const STATUS_SHIPPED = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_FAIL_OR_CANCELED = 5;
    const STATUS_REFUNDED = 6;
    const STATUS_INSTALLATION = 7;

    protected $table = 'nm_order';
    protected $primaryKey = 'order_id';
    protected $guarded = [];

    public function generatedCode()
    {
        return $this->hasMany(GeneratedCode::class, 'order_id');
    }

    public function coupons()
    {
        return $this->generatedCode()->where('type', 2);
    }

    public function tickets()
    {
        return $this->generatedCode()->where('type', 3);
    }

    public function ecards()
    {
        return $this->generatedCode()->where('type', 4);
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'cus_id', 'order_cus_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'order_pro_id', 'pro_id');
    }

    public function courier()
    {
        return $this->hasOne(Courier::class, 'id', 'order_courier_id');
    }

    public function services()
    {
        return $this->hasMany(MemberServiceSchedule::class, 'order_id', 'order_id');
    }

    public function negotiation()
    {
        return $this->belongsTo(PriceNegotiation::class, 'nego_id');
    }

    public function total()
    {
        return collect([
            'original' => [
                'amount' => round(max($this->{'product_price'}, 0), 2),
                'total' => round(max($this->{'total_product_price'}, 0), 2)
            ],
            'price' => round(max($this->{'order_value'}, 0), 2),
            'platform_charge_rate' => round(max($this->{'cus_platform_charge_rate'}, 0), 4),
            'platform_charge_value' => round(max($this->{'cus_platform_charge_value'}, 0), 2),
            'service_charge_rate' => round(max($this->{'cus_service_charge_rate'}, 0), 4),
            'service_charge_value' => round(max($this->{'cus_service_charge_value'}, 0), 2),
            'merchant_charge_rate' => round(max($this->{'merchant_charge_percentage'}, 0), 4),
            'merchant_charge_value' => round(max($this->{'merchant_charge_value'}, 0), 2),
            'shipping_fees_value' => round(max($this->{'total_product_shipping_fees'} ,0), 2),
            'merchant_earn_value' => round(max($this->{'order_value'} - ($this->{'merchant_charge_value'} + $this->{'cus_service_charge_value'} + $this->{'cus_platform_charge_value'}) ,0), 2),
            'product_price' => round(max($this->{'total_product_price'}, 0), 2),
        ]);
    }

    public function status()
    {
        if($this->{'order_type'} == 1)
        {
            switch ($this->{'order_status'}) {
                case 0:
                    return trans('localize.awaiting_payment');
                    break;

                case 1:
                    return trans('localize.processing');
                    break;

                case 2:
                    return trans('localize.packaging');
                    break;

                case 3:
                    if($this->{'product_shipping_fees_type'} <3)
                    {
                        return trans('localize.shipped');
                    }
                    else
                    {
                        return trans('localize.arranged');
                    }
                    break;

                case 4:
                    return trans('localize.completed');
                    break;

                case 5:
                    return trans('localize.cancelled');
                    break;

                case 6:
                    return trans('localize.refunded');
                    break;

                case 7:
                    return trans('localize.installation');
                    break;
            }
        }

        if(in_array($this->{'order_type'}, [3,4,5]))
        {
            switch ($this->{'order_status'}) {

                case 2:
                    return trans('localize.pending');
                    break;

                case 4:
                    return trans('localize.completed');
                    break;

                case 5:
                    return trans('localize.cancelled');
                    break;

                case 6:
                    return trans('localize.refunded');
                    break;
            }
        }

        return null;
    }

    public function shipping_type()
    {
        switch ($this->{'product_shipping_fees_type'}) {
            case 0:
                return trans('localize.shipping_fees_free');
                break;

            case 1:
                return trans('localize.shipping_fees_product');
                break;

            case 2:
                return trans('localize.shipping_fees_transaction');
                break;

            case 3:
                return trans('localize.self_pickup');
                break;

            default:
                return null;
                break;
        }
    }

    public function mapping()
    {
        return $this->hasOne(OrderMapping::class, 'order_id', 'order_id');
    }

    public function getParseOrderAttributeAttribute()
    {
        if (is_null($this->order_attributes)) {
            return '';
        }

        $str = '';
        $attributes = json_decode($this->order_attributes);

        foreach ($attributes as $parent => $child)
        {
            $str .= "<b>$parent : </b> $child </br>";
        }

        return $str;
    }

    public function order_mappings()
    {
        return $this->hasOne(OrderMapping::class, 'order_id', 'order_id');
    }

    public function type()
    {
        switch ($this->{'order_type'}) {
            case 1:
                return trans('localize.normal_product');
                break;

            case 2:
                return 'Deals';
                break;

            case 3:
                return trans('localize.coupon');
                break;

            case 4:
                return trans('localize.ticket');
                break;

            case 5:
                return trans('localize.e-card.name');
                break;

            default:
                return null;
                break;
        }
    }

    public function pricing()
    {
        return $this->hasOne(ProductPricing::class, 'id', 'order_pricing_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'co_curcode', 'currency');
    }

    public function logs()
    {
        return $this->hasMany(OrderOnlineLog::class, 'order_id', 'order_id');
    }

    public function parentOrder()
    {
        return $this->belongsTo(ParentOrder::class, 'parent_order_id');
    }

    public function invoices()
    {
        return $this->belongsToMany(TaxInvoice::class, 'order_mappings', 'order_id', 'tax_invoice_id');
    }

    public function scopeWhereHasProductForStores($query, Collection $stores)
    {
        return $query->whereHas('product.store', function ($query) use ($stores) {
            $query->whereIn('stor_id', $stores->pluck('stor_id')->toArray());
        });
    }

    public function scopeWithInvoicesForMerchantId($query, $merchantId)
    {
        return $query->with(['invoices' => function ($query) use ($merchantId) {
            $query->where('merchant_id', $merchantId);
        }]);
    }

    public function scopeCompleted($query)
    {
        return $query->where(function ($query) {
            $query->whereOrderStatus(self::STATUS_COMPLETED)
                ->orWhere(function ($query) {
                    $query->where('order_status', 7)
                        ->whereHas('services', function ($query) {
                            $query->where((new MemberServiceSchedule)->getTable() . '.status', 4);
                        });
                });
        });
    }

    public function scopeOrderStatus($query, $string)
    {
        return $query->whereIn('order_status', explode(',', $string));
    }

    public function scopeIsWholesale($query, $boolean)
    {
        if ($boolean) {
            return $query->where('wholesale', $boolean);
        }

        return $query;
    }

    public function convertOrderAttributes()
    {
        $attributes = json_decode($this->order_attributes);
        $array = [];

        if ($attributes) {
            foreach ($attributes as $key => $attribute) {
                $array[] = [
                    'type' => $key,
                    'value' => $attribute
                ];
            }
        }

        return $array;
    }

    public function getTotalDiscount()
    {
        return $this->product_original_price - $this->product_price;
    }

    public function getGrandTotal()
    {
        return $this->total_product_price + $this->total_product_shipping_fees;
    }

    public function scopeSold($query)
    {
        return $query->where('order_status', '<>', Order::STATUS_FAIL_OR_CANCELED);
    }

    public function shipping()
    {
        return $this->hasOne(Shipping::class, 'ship_order_id', 'order_id');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'order_id', 'order_id');
    }

    public function getProductRatingAttribute()
    {
        return $this->ratings->where('reference_type', Rating::REFERENCE_TYPE_PRODUCT)->first();
    }

    public function getStoreRatingAttribute()
    {
        return Rating::where('parent_order_id', $this->parent_order_id)->where('reference_type', Rating::REFERENCE_TYPE_STORE)->first();
    }

    /**
     * Get all complains. But by default one order should have one complain only.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function complainables()
    {
        return $this->morphMany(Complain::class, 'complainable');
    }

    /**
     * First complain created by order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function getFirstComplain()
    {
        return $this->complainables()->orderBy('id', 'ASC')->limit(1);
    }

    public function getRateEligibilityAttribute()
    {
        return stdObject([
            'product' => $this->order_status == self::STATUS_COMPLETED && !$this->productRating,
            'store' => $this->order_status == self::STATUS_COMPLETED && !$this->storeRating,
        ]);
    }
}
