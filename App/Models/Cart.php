<?php

namespace App\Models;

use App\Repositories\CourierRepo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\GlobalEloquentBuilder;

class Cart extends Authenticatable
{
    use GlobalEloquentBuilder;

    protected $table = 'temp_cart';
    protected $fillable = ['token', 'user_id', 'product_id', 'quantity' , 'remarks', 'pricing_id', 'cus_id','service_ids', 'flashsale_id', 'wholesale', 'nego_id'];
    protected $hidden = ['currency', 'currency_rate', 'purchasing_price', 'product_price','attributes','attributes_name', 'color_id', 'size_id'];


    public function customer()
    {
        return $this->belongsTo(Customer::class, 'cus_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'pro_id');
    }

    public function pricing()
    {
        return $this->belongsTo(ProductPricing::class, 'pricing_id', 'id');
    }

    public function shipping()
    {
        return $this->hasOne(Shipping::class, 'ship_id', 'ship_id');
    }

    public function negotiation()
    {
        // return $this->hasMany(PriceNegotiation::class, 'token', 'cart_token');
        return $this->belongsTo(PriceNegotiation::class, 'nego_id');
    }

    public static function addItem($customerId, $quantity, ProductPricing $productPricing)
    {
        $cartItem = self::firstOrCreate([
            'cus_id' => $customerId,
            'product_id' => $productPricing->pro_id,
            'pricing_id' => $productPricing->id,
        ]);

        $cartItem->increment('quantity', $quantity ?: 1);

        return $cartItem;
    }

    public function getTotalPrice()
    {
        if (!$this->relationLoaded('pricing')) {
            $this->load('pricing');
        }

        return $this->pricing->getPurchasePrice() * $this->quantity;
    }

    public function scopeOwnerBy($query, $customer_id = 0, $token = null)
    {
        if(isset($customer_id) && !empty($customer_id))
        {
            $query->where('cus_id', '=', $customer_id);
        }
        else
        {
            if(!empty($token))
            {
                $query->where('token', '=', $token)->whereNull('cus_id');
            }
            else
            {
                $query->whereNull('token')->whereNull('cus_id');
            }
        }

        return $query;
    }

    public function scopeHasActiveStore($query)
    {
        return $query->whereHas('product.store', function ($query) {
            $query->where('stor_status', Store::STATUS_ACTIVE);
        });
    }

    public function scopeHasActiveProduct($query)
    {
        return $query->whereHas('product', function ($query) {
            $query->where('pro_status', Store::STATUS_ACTIVE);
        });
    }

    public function scopeHasServices($query)
    {
        return $query->where(function ($query) {
            return $query->whereNotNull('service_ids')
                ->orWhere('service_ids', '<>', '');
        });
    }

    public static function updateShippingChargesForCustomer($customer)
    {
        if ($customer instanceof Customer) {
            $customer = $customer->cus_id;
        }

        self::hasActiveProduct()
            ->hasActiveStore()
            ->whereNull('service_ids')
            ->where('cus_id', $customer)
            ->get()
            ->each
            ->updateShippingCharge();
    }

    public function updateShippingCharge()
    {
        $store = $this->product->store;
        $shipping = $this->shipping;

        $param['origin'] = !$store->stor_subdistrict ? $store->stor_city : $store->stor_subdistrict;
        $param['originType'] = !$store->stor_subdistrict ? 'city' : 'subdistrict';
        $param['destination'] = !$shipping->ship_subdistrict_id ? $shipping->ship_ci_id : $shipping->ship_subdistrict_id;
        $param['destinationType'] = !$shipping->ship_subdistrict_id ? 'city' : 'subdistrict';
        $param['weight'] = $this->weight_per_qty;
        $param['courier'] = $this->ship_code;

        $result = CourierRepo::get_couriers_fee($param);

        foreach ($result[0]->costs as $cost) {
            if ($this->ship_service == $cost->service) {
                $this->shipping_fee = $cost->cost[0]->value;
            }
        }

        $this->Save();
    }

    public static function courierInfoNotUpdatedWhereCustomer($customer, $wholesale)
    {
        if ($customer instanceof Customer) {
            $customer = $customer->cus_id;
        }

        return self::where('cus_id', $customer)
            ->whereNull('ship_code')
            ->where('wholesale', $wholesale)
            ->where(function ($query) {
                $query->whereNull('service_ids')
                    ->orWhere('service_ids', '');
            })
            ->exists();
    }

}
