<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComplainImage extends Model
{
    protected $fillable = [
        'complain_id',
        'image',
        'path',
    ];

    /**
     * Get image link magic property.
     *
     * @return string
     */
    function getImageLinkAttribute()
    {
        return \Storage::url("{$this->path}/{$this->image}");
    }
}
