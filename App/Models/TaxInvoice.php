<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TaxInvoice extends Model
{
    protected $table = 'tax_invoices';
    protected $primaryKey = 'id';

    public function items()
    {
        return $this->belongsToMany(Order::class, 'order_mappings', 'tax_invoice_id', 'order_id');
    }

    public function order_mappings() {
        return $this->hasMany(OrderMapping::class, 'tax_invoice_id', 'id');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'mer_id', 'merchant_id');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'cus_id', 'customer_id');
    }

    public function deliveries()
    {
        return $this->hasMany(DeliveryOrder::class, 'tax_invoice_id', 'id');
    }

    public function delivery()
    {
        return $this->hasOne(DeliveryOrder::class, 'tax_invoice_id', 'id');
    }

    public function tax_number($running = 'ONS')
    {
        $country = $this->country()->getResults();
        if($country)
            return "{$country->co_code}-{$running}{$this->{'tax_number'}}";

        return "{$running}-{$this->{'tax_number'}}";
    }

    public function parent_order()
    {
        return $this->belongsTo(ParentOrder::class, 'parent_order_id', 'id');
    }

    public function item_type()
    {
        switch ($this->{'item_type'}) {
            case 1:
                return trans('localize.normal_product');
                break;

            case 2:
                return trans('localize.virtual_product');
                break;

            default:
                return '';
                break;
        }

        return '';
    }

    public function shipment_type()
    {
        switch ($this->{'shipment_type'}) {
            case 0:
                return trans('localize.not_assigned');
                break;

            case 1:
                return implode(',', [trans('localize.shipping_fees_free'), trans('localize.shipping_fees_product'), trans('localize.shipping_fees_transaction')]);
                break;

            case 2:
                return trans('localize.self_pickup');
                break;

            default:
                return '';
                break;
        }

        return '';
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'co_id', 'country_id');
    }

    public function scopeOrderStatus($query, $orderStatus)
    {
        return $query->whereHas('items', function ($query) use ($orderStatus) {
            $query->where('order_status', $orderStatus);
        });
    }

    public function scopeWhereHasStores($query, Collection $stores)
    {
        return $query->whereHas('items', function ($query) use ($stores) {
            $query->whereHasStores($stores);
        });
    }

    public function updateItemStatus()
    {
        $this->items->each->update([
            'order_status' => 1
        ]);
    }
}
