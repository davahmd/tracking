<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use App\Services\DeviceToken\Traits\HasDeviceToken;
use App\Services\Jwt\Auth\Traits\HasPersonalAccessToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MerchantResetPassword as MerchantResetPasswordNotification;

class Merchant extends Authenticatable
{
    use Notifiable, HasPersonalAccessToken, HasNewCollection, HasDeviceToken;

    const STATUS_BLOCK = 0;
    const STATUS_ACTIVE = 1;

    protected $guard = "merchants";
    protected $table = 'nm_merchant';
    protected $primaryKey = 'mer_id';

    protected $fillable = [
        'mer_type', 'mer_lname', 'mer_fname', 'email', 'username', 'password', 'mer_phone', 'mer_address1', 'mer_address2', 'mer_ci_id', 'mer_co_id', 'mer_payment', 'mer_commission', 'mer_vtoken', 'mer_staus', 'bank_acc_name', 'bank_acc_no', 'bank_name', 'bank_country', 'bank_address', 'bank_swift', 'bank_europe', 'mer_state', 'mer_city_name', 'mer_platform_charge', 'mer_service_charge', 'app_session', 'app_session_date', 'api_token', 'mer_office_number', 'mer_referrer', 'mer_referrer_phone', 'zipcode', 'bank_gst', 'mer_subdistrict', 'user_id', 'slug', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function guarantor()
    {
        return $this->hasOne(MerchantGuarantor::class, 'merchant_id', 'mer_id');
    }

    public function referrer()
    {
        return $this->hasOne(MerchantReferrer::class, 'merchant_id', 'mer_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'co_id', 'mer_co_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'mer_state');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'mer_ci_id');
    }

    public function subdistrict()
    {
        return $this->hasOne(Subdistrict::class, 'id', 'mer_subdistrict');
    }

    public function country_bank()
    {
        return $this->hasOne(Country::class, 'co_id', 'bank_country');
    }

    public function stores()
    {
        // Change to hasOne on Zona because of requirement
        // If error occurs due to past data allowed to have more than one, use hasMany relationship back
        // return $this->hasMany(Store::class, 'stor_merchant_id', 'mer_id');
        return $this->hasMany(Store::class, 'stor_merchant_id', 'mer_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'pro_mr_id', 'mer_id');
    }

    public function wholeSaleProducts()
    {
        return $this->products()->whereHas('pricing', function ($query) {
            return $query->whereNotNull('wholesale_price');
        });
    }

    public function orders()
    {
        return $this->hasManyThrough(
            Order::class, Product::class,
            'pro_mr_id', 'order_pro_id', 'mer_id'
        );
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class, 'merchant_id', 'mer_id');
    }

    public function storeUser()
    {
        return $this->hasManyThrough(
            StoreUser::class, Store::class,
            'stor_id', 'id', 'store_id'
        );
    }

    public function distributors()
    {
        return $this->belongsToMany(self::class, 'merchant_distributor', 'merchant_id', 'distributor_id')
            ->withPivot('status')
            ->withTimestamps();
    }

    public function approvedDistributors()
    {
        return $this->distributors()->wherePivot('status', 1);
    }

    public function retailers()
    {
        return $this->belongsToMany(self::class, 'merchant_distributor', 'distributor_id', 'merchant_id')
            ->withPivot('status')
            ->withTimestamps();
    }

    public function approvedRetailers()
    {
        return $this->retailers()->wherePivot('status', 1);
    }

    public function offdays()
    {
        return $this->hasMany(Offday::class, 'merchant_id', 'mer_id');
    }

    public function negotiation()
    {
        return $this->hasMany(PriceNegotiation::class, 'merchant_id', 'mer_id');
    }

    /* End Relationships */
    public function completedOrders()
    {
        return $this->orders()->where('order_status', Order::STATUS_COMPLETED);
    }

    public function activeStores()
    {
        return $this->stores()->where('stor_status', Store::STATUS_ACTIVE);
    }

    /**
     * All complains's model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function complains()
    {
        return $this->morphOne(Complain::class, 'complainant');
    }

    /**
     * Complain's messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function responders()
    {
        return $this->morphOne(ComplainMessage::class, 'responder');
    }

    public function scopeActive($query)
    {
        return $query->where('mer_staus', self::STATUS_ACTIVE);
    }

    public function scopeOthers($query)
    {
        if (auth()->user() && auth()->user()->merchant) {
            return $query->where('mer_id', '<>', auth()->user()->merchant->mer_id);
        }

        return $query;
    }

    public function isBlocked()
    {
        return $this->mer_staus == 0 ? true : false;
    }

    public function scopeNameContain($query, $string)
    {
        if ($string) {
            return $query->where('mer_fname', 'LIKE', '%' . $string . '%');
        }

        return $query;
    }

    public function full_name()
    {
        return $this->merchantName();
    }

    public function merchantName()
    {
        $fname = $this->{'mer_fname'};
        $lname = $this->{'mer_lname'};

        if(preg_match("/\p{Han}+/u", $fname) && preg_match("/\p{Han}+/u", $lname))
            return $fname.$lname;

        return $fname . ' ' . $lname;
    }

    public function fullAddress()
    {
        if (!$this->relationLoaded('state')) {
            $this->load('state');
        }

        $address = '';
        $address .= !empty($this->mer_address1) ? $this->mer_address1 : '';
        $address .= !empty($this->mer_address2) ? ', ' . $this->mer_address2 : '';
        $address .= !empty($this->mer_city_name) ? ', ' . $this->mer_city_name : '';
        $address .= ', ' . $this->state->name . ' ' . $this->zipcode;
        $address .= ', ' . $this->country->co_name;

        return $address;

    }

    public function routeNotificationForMail()
    {
        // $environment = app()->environment();
        //
        // if ($environment == 'production' || $environment == 'staging' || config('mail.host') == 'smtp.mailtrap.io') {
        //     return $this->user->email;
        // }
        //
        // return '';

        return $this->user->email;
    }

    /**
     * This is to replace merchant reset password link in the email
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MerchantResetPasswordNotification($token));
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'merchant.'.$this->mer_id;
    }

    public function getImageUrlAttribute()
    {
        if (!$this->image) {
            return url('common/images/stock.png');
        }

        return \Storage::url("merchant/avatar/{$this->mer_id}/{$this->image}");
    }

    public function scopeIsDistributor($query, $boolean)
    {
        if (! is_null($boolean)) {
            return $query->whereIsDistributor($boolean);
        }

        return $query;
    }
}
