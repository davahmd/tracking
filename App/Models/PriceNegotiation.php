<?php

namespace App\Models;

use App\Services\Cart\CartService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Collections\Traits\HasNewCollection;
use Illuminate\Database\Eloquent\Collection;

class PriceNegotiation extends Model
{
    use HasNewCollection;

    protected $table = 'price_negotiations';
    protected $guarded = [];

    const STATUS = [
        'INITIATED' => 0,

        'ACTIVE' => 10,
        'ACCEPTED_BY_MERCHANT' => 11,
        'ACCEPTED_BY_CUSTOMER' => 12,

        'COMPLETED_WITH_NEGOTIATED_PRICE' => 20,
        'COMPLETED_WITH_ORIGINAL_PRICE' => 21,

        'DECLINED' => 30,
        'DECLINED_BY_MERCHANT' => 31,
        'DECLINED_BY_CUSTOMER' => 32,

        'EXPIRED' => 40,

        'FAILED' => 50,

        'COUNTER_OFFER' => 60,
    ];

    public static function statusType()
    {
        return [
            0 => trans('localize.nego_status.initiated'),
            10 => trans('localize.nego_status.active'),
            11 => trans('localize.nego_status.mer_accepted'),
            12 => trans('localize.nego_status.cus_accepted'),
            20 => trans('localize.nego_status.completed_customer'),
            21 => trans('localize.nego_status.completed_merchant'),
            30 => trans('localize.nego_status.declined'),
            31 => trans('localize.nego_status.mer_declined'),
            32 => trans('localize.nego_status.cus_declined'),
            40 => trans('localize.nego_status.expired'),
            50 => trans('localize.nego_status.failed'),
            60 => trans('localize.nego_status.counter_offer'),
        ];
    }

    // ---------- DEFINE RELATIONSHIP ---------- //

    public function product()
    {
        return $this->hasOne(Product::class, 'pro_id', 'product_id');
    }

    public function pricing()
    {
        return $this->hasOne(ProductPricing::class, 'id', 'pricing_id');
    }

    public function attribute()
    {
        return $this->hasOne(ProductAttribute::class, 'id', 'attribute_id');
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class, 'token', 'cart_token');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'cus_id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'mer_id');
    }

    public function child()
    {
        return $this->hasOne(self::class, 'session_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'session_id');
    }
    // ---------- END OF RELATIONSHIP ---------- //

    public function accept($status)
    {
        if ($this->isAcceptedStatus($status)) {
            $this->update([
                'status' => $status
            ]);

            return $this->fresh();
        }

    }

    public function decline($status)
    {
        if ($this->isDeclinedStatus($status)) {
            $this->update([
                'status' => $status
            ]);

            return $this->fresh();
        }
    }

    public function isAcceptedStatus($status)
    {
        return in_array($status, [
            $this::STATUS['ACCEPTED_BY_CUSTOMER'],
            $this::STATUS['ACCEPTED_BY_MERCHANT']
        ]);
    }

    public function isDeclinedStatus($status)
    {
        return in_array($status, [
            $this::STATUS['DECLINED_BY_MERCHANT'],
            $this::STATUS['DECLINED_BY_CUSTOMER'],
            $this::STATUS['DECLINED']
        ]);
    }

    public function scopeExceptCounterOffer($query)
    {
        return $query->where('status', '<>', $this::STATUS['COUNTER_OFFER']);
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS['ACTIVE'])
            ->where('expired_at', '>', Carbon::now()->toDateTimeString());
    }

    public function scopeDeclined($query)
    {
        return $query->whereIn('status', [
            self::STATUS['DECLINED_BY_CUSTOMER'],
            self::STATUS['DECLINED_BY_MERCHANT'],
        ]);
    }

    public function scopeCompleted($query)
    {
        return $query->whereIn('status', [
            self::STATUS['COMPLETED_WITH_NEGOTIATED_PRICE'],
            self::STATUS['COMPLETED_WITH_ORIGINAL_PRICE']
        ]);
    }

    public function scopeExpired($query)
    {
        return $query->where(function ($query) {
            return $query->where('status', self::STATUS['EXPIRED'])
                ->orWhere('expired_at', '<', Carbon::now()->toDateTimeString());
        });
    }

    public function acceptByCustomer()
    {
        return $this->update([
            'status' => $this::STATUS['ACCEPTED_BY_CUSTOMER']
        ]);
    }

    public function rejectByCustomer()
    {
        return $this->update([
            'status' => $this::STATUS['DECLINED_BY_CUSTOMER']
        ]);
    }

    public function getIsExpiredAttribute($value)
    {
        if ($this->status == self::STATUS['EXPIRED'] || Carbon::parse($this->expired_at)->lte(Carbon::now())) {
            return 1;
        }

        return 0;
    }
}
