<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Helper;

use Carbon\Carbon;

class Promotion extends Model
{
    protected $table = 'promotions';
    protected $fillable = ['name', 'merchant_id', 'stores', 'categories', 'products', 'promo_type', 'promo_code', 'promo_code_limit', 'promo_min_spend', 'promo_discount_max', 'discount_rate', 'discount_value', 'applied_to', 'started_at', 'ended_at', 'request', 'status', 'create_initiator', 'create_uid'];
    protected $primaryKey = 'id';

    public function merchant()
    {
    	return $this->belongsTo(Merchant::class, 'merchant_id', 'mer_id');
    }

    public function promotion_products()
    {
    	return $this->hasMany(PromotionProduct::class, 'promotion_id', 'id');
    }

    public function log()
    {
        return $this->hasOne(PromotionLog::class, 'promotion_id', 'id')->where('action', 'C');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeFlash($query)
    {
        return $query->where('promo_type', 'F');
    }

    public function scopeWithinPeriod($query)
    {
        $today_datetime = Carbon::parse(Helper::UTCtoTZ(Carbon::now()))->toDatetimeString();

        return $query->where('started_at', '<=', $today_datetime)->where('ended_at', '>=', $today_datetime);
    }
}
