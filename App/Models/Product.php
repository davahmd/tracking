<?php

namespace App\Models;

use App;
use App\Collections\Traits\HasNewCollection;
use App\Traits\GlobalEloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use GlobalEloquentBuilder, HasNewCollection;

    protected $table = 'nm_product';
    protected $guarded = ['pro_id'];
    protected $primaryKey = 'pro_id';

    const CONDITION_NEW = 1;
    const CONDITION_USED = 2;
    const CONDITION_REFURBISHED = 3;

    const STATUS_BLOCK = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INCOMPLETE = 2;

    public function getTitleAttribute($value)
    {
        $default_title = $this->{'pro_title_en'};
        $title = $this->{'pro_title_'.App::getLocale()};

        if (empty($title)) {
            return $default_title;
        } else {
            return $title;
        }
    }

    public function getDescAttribute($value)
    {
        $default_desc = $this->{'pro_desc_en'};
        $desc = $this->{'pro_desc_'.App::getLocale()};

        if (empty($desc)) {
            return $default_desc;
        } else {
            return $desc;
        }
    }

    public function getShortDescAttribute($value)
    {
        $defaultShortDesc = $this->{'short_desc_en'};
        $shortDesc = $this->{'short_desc_'.App::getLocale()};

        if (empty($shortDesc))
            return $defaultShortDesc;
        return $shortDesc;
    }

    public function getLowestPriceAttribute()
    {
        return $this->pricing()->active()->min('price');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'pro_id', 'pro_id')->orderBy('main', 'desc')->orderBy('order', 'asc');
    }

    public function mainImage()
    {
        return $this->hasOne(ProductImage::class, 'pro_id', 'pro_id')->where('main', true);
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'mer_id', 'pro_mr_id');
    }

    public function store(array $arrFields = null)
    {
        $query = $this->belongsTo(Store::class, 'pro_sh_id', 'stor_id');

        if (!is_null($arrFields)) {
            return $query->select($arrFields);
        }

        return $query;
    }

    public function pricing()
    {
        return $this->hasMany(ProductPricing::class, 'pro_id', 'pro_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'nm_product_category', 'product_id', 'category_id');
    }

    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class, 'pro_id', 'pro_id');
    }

    public function brand()
    {
        return $this->hasOne(OfficialBrands::class, 'brand_id', 'brand_id');
    }

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'product_vehicle_mappings', 'pro_id', 'vehicle_id')->select(['vehicles.id', 'brand'])->withTimestamps();
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'order_pro_id', 'pro_id');
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'product_id', 'pro_id');
    }

    public function storeServiceExtra()
    {
        return $this->hasMany(StoreServiceExtra::class, 'pro_id', 'pro_id');
    }

    public function chats($arrWheres = null, array $arrFields = null)
    {
        $query = $this->hasMany(Chat::class, 'pro_id', 'pro_id');

        if (!is_null($arrWheres)) {
            $query = $query->where($arrWheres);
        }

        if (!is_null($arrFields)) {
            $query = $query->select($arrFields);
        }

        return $query;
    }

    public function quantityLog()
    {
        return $this->hasMany(ProductQuantityLog::class, 'pro_id', 'pro_id');
    }

    public function status()
    {
        switch ($this->pro_status)
        {
            case '0':
                return trans('localize.Inactive');
                break;

            case '1':
                return trans('localize.Active');
                break;

            case '2':
                return trans('localize.incomplete');
                break;

            case '3':
                return trans('localize.pending_review');
                break;

            default:
                return '';
                break;
        }
    }

    public function type()
    {
        switch ($this->pro_type)
        {
            case '1':
                return trans('localize.normal_product');
                break;

            case '2':
                return trans('localize.coupon');
                break;

            case '3':
                return trans('localize.ticket');
                break;

            case '4':
                return trans('localize.e-card.name');
                break;

            default:
                return '';
                break;
        }
    }

    public function negotiations()
    {
        return $this->hasMany(PriceNegotiation::class, 'product_id', 'pro_id');
    }

    public function mainImageUrl()
    {
        if ($this->mainImage) {
            return \Storage::url('product/' . $this->pro_mr_id . '/' . $this->mainImage->image);
        }

        return null;
    }

    public static function product_conditions()
    {
        return [
            self::CONDITION_NEW => trans('localize.condition.'.self::CONDITION_NEW),
            self::CONDITION_USED => trans('localize.condition.'.self::CONDITION_USED),
            self::CONDITION_REFURBISHED => trans('localize.condition.'.self::CONDITION_REFURBISHED),
        ];
    }

    public function product_condition_name()
    {
        switch ($this->condition)
        {
            case self::CONDITION_NEW:
                return trans('localize.condition.'.self::CONDITION_NEW);
                break;

            case self::CONDITION_USED:
                return trans('localize.condition.'.self::CONDITION_USED);
                break;

            case self::CONDITION_REFURBISHED:
                return trans('localize.condition.'.self::CONDITION_REFURBISHED);
                break;

            default:
                return '';
                break;
        }
    }

    public function scopeActive($query)
    {
        return $query->where('pro_status', self::STATUS_ACTIVE);
    }

    public function scopeHasCategoryId($query, $id)
    {
        if ($id) {
            return $query->whereHas('categories', function ($query) use ($id) {
                $category =Category::find($id);
                $ids[] = $id;

                if ($category && $category->child_list) {
                    $ids = array_merge($ids, explode(',', $category->child_list));
                }

                $query->whereIn('category_id', $ids);
            });
        }
        else {
            return $query;
        }
    }

    public function scopeHasWholeSalePrice($query)
    {
        return $query->whereHas('pricing', function ($query) {
            return $query->whereNotNull('wholesale_price');
        });
    }

    public function scopeHasPriceBetween($query, $priceStart, $priceEnd, $status = null)
    {
        if ($priceStart || $priceEnd) {
            $query->whereHas('pricing', function ($query) use ($priceStart, $priceEnd, $status) {
                if ($priceStart) {
                    $query->where('price', '>=', $priceStart);
                }

                if ($priceEnd) {
                    $query->where('price', '<=', $priceEnd);
                }

                if ($status) {
                    $query->where('status', $status);
                }

                $query->orderBy('price');
            });
        }

        return $query;
    }

    public function scopeCondition($query, $condition)
    {
        if ($condition) {
            $query->where('condition', $condition);
        }

        return $query;
    }

    public function scopeBrandId($query, $brandId)
    {
        if ($brandId) {
            $query->where($this->getTable() . '.brand_id', $brandId);
        }
    }

    public function scopeDeliveryFrom($query, $cityName)
    {
        if ($cityName) {
            $query->whereHas('store', function ($query) use ($cityName) {
                $query->where('stor_city_name', $cityName);
            });
        }

        return $query;
    }

    public function scopeWithAvailablePricing($query, $pricingId = null)
    {
        return $query->with(['pricing' => function ($query) use ($pricingId) {
            $query->where('status', ProductPricing::STATUS_ACTIVE);

            if ($pricingId) {
                $query->where('id', $pricingId);
            }
        }]);
    }

    public function scopeStoreId($query, $storeId)
    {
        if ($storeId) {
            $query->where('pro_sh_id', $storeId);
        }

        return $query;
    }

    public function scopeMerchantId($query, $merchantId)
    {
        if ($merchantId) {
            $query->where('pro_mr_id', $merchantId);
        }

        return $query;
    }

    public function scopeNameContain($query, $string)
    {
        if ($string) {
            $query->where('pro_title_en', 'LIKE',  '%' . $string . '%');
        }

        return $query;
    }

    public function scopeFeatured($query, $boolean = true)
    {
        if ($boolean) {
            $query->whereFeatured(true);
        }

        return $query;
    }

    public function scopeHasCar($query, $carId, $carBrand)
    {
        if ($carId || $carBrand) {
            $query->whereHas('vehicles', function ($query) use ($carId, $carBrand) {
                if ($carId) {
                    $query->where('vehicles.id', $carId);
                }

                if ($carBrand) {
                    $query->where('vehicles.brand', $carBrand);
                }
            });
        }

        return $query;
    }

    public function scopeSortBy($query, $sortBy, $sortOrder)
    {
        switch ($sortBy) {
            case 'latest':
                $query->orderBy('created_at', $sortOrder ?: 'desc');
                break;
            case 'popular':
                $query->leftJoin('nm_order', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                    ->select('nm_product.*')
                    ->addSelect(DB::raw('count(nm_product.pro_id) as sort'))
                    ->orderBy('sort', $sortOrder ?: 'desc')
                    ->groupBy('nm_product.pro_id');
                break;
            case 'price':
                $query->leftJoin('nm_product_pricing', 'nm_product.pro_id', '=', 'nm_product_pricing.pro_id')
                    ->select('nm_product.*')
                    ->addSelect(DB::raw('min( case when now() between nm_product_pricing.discounted_from and nm_product_pricing.discounted_to then nm_product_pricing.discounted_price else price end) as sort'))
                    ->orderBy('sort', $sortOrder ?: 'asc')
                    ->groupBy('nm_product.pro_id');
                break;
            default:
        }

//        dd($query->toSql());

        return $query;
    }

    public function scopeStoreActive($query)
    {
        return $query->leftJoin('nm_store', 'stor_id', 'pro_sh_id')
            ->where('stor_status', 1);
    }

    public function scopeMerchantActive($query)
    {
        return $query->leftJoin('nm_merchant', 'mer_id', 'pro_mr_id')
            ->where('mer_staus', 1);
    }

    public function soldOrders()
    {
        return $this->orders()->sold();
    }

    public function scopeAvailable($query)
    {
        return $query->active()
            ->whereHas('merchant', function ($query) {
                $query->where('mer_staus', Merchant::STATUS_ACTIVE);
            })
            ->whereHas('store', function ($query) {
                $query->where('stor_status', Store::STATUS_ACTIVE);
            })
            ->whereHas('pricing', function ($query) {
                $query->where('status', ProductPricing::STATUS_ACTIVE);
            })
            ->whereHas('categories');
    }

    public function scopeProvideInstallation($query, $check)
    {
        if ($check) {
            return $query->where('installation', 1);
        }

        return $query;
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'reference_id', 'pro_id')->where('reference_type', Rating::REFERENCE_TYPE_PRODUCT);
    }

    public function compiledRatings($array = false)
    {
        $ratings = $this->ratings()->getResults();

        $rate = !$ratings->count() ? 0 : round($ratings->sum('rating') / $ratings->count(), 1);
        $percentage = round($rate * 20, 2);

        $result = [
            'total' => $ratings->count(),
            'rate' => $rate,
            'max_rate' => 5,
            'percentage' => $percentage,
            'cumulative' => []
        ];

        for ($i=1; $i <= 5; $i++)
        {
            $total = $ratings->where('rating', $i)->count();

            $percentage = !$ratings->count() ? 0 : round($total / $ratings->count() * 100, 2);

            $result['cumulative'][$i] = [
                'rate' => $i,
                'total' => $total,
                'percentage' => $percentage,
            ];
        }

        krsort($result['cumulative']);

        if(!$array)
            return json_decode(json_encode($result));

        $result['cumulative'] = array_values($result['cumulative']);

        return $result;
    }

    public function getSlugUrlAttribute()
    {
        $slug = \Helper::slug_maker($this->pro_title_en, $this->pro_id);

        return url("/product/$slug");
    }
}
