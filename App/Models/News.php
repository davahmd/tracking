<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use App\Helpers\Helper;

class News extends Model
{
    // status
    const STATUS_PUBLISH = 1;
    const STATUS_NOT_PUBLISH = 0;

    protected $table = 'news';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $dates = ['start_date', 'end_date'];

    protected $appends = ['title_localize', 'content_localize', 'image_localize'];

    public function creator()
    {
        return $this->belongsTo(Admin::class, 'creator_uid');
    }

    public function scopePublish($query)
    {
        return $query->where('status', 1);
    }

    public function scopeNotPublish($query)
    {
        return $query->where('status', 0);
    }

    public function scopeBetweenStartDateEndDate($query)
    {
        $date = Carbon::now()->startOfDay();

        return $query->where('start_date', '<=', $date)
            ->where('end_date', '>=', $date);
    }

    public function scopeAvailable($query)
    {
        $currentTime = Carbon::now(config('app.operation_timezone'));

        $query->where('status', static::STATUS_PUBLISH)
                ->where(function($query) {
                    $query->where('start_date', null)
                            ->orWhere('start_date', '<=', $currentTime);

                })
                ->where(function($query) {
                    $query->where('end_date', null)
                            ->orWhere('end_date', '>=', $currentTime);
                });
    }

    public function getTitleLocalizeAttribute()
    {
        return $this->getLocalize('title');
    }

    public function getContentLocalizeAttribute()
    {
        return $this->getLocalize('content');
    }

    public function getImageLocalizeAttribute()
    {
        return $this->getLocalize('image');
    }


    private function getLocalize($attribute)
    {
        $locale = str_replace('-', '_', app()->getLocale());
        $value = isset($this->attributes["{$attribute}_{$locale}"]) ? $this->attributes["{$attribute}_{$locale}"] : null;

        if (is_null($value) || empty($value)) {
            $value = isset($this->attributes["{$attribute}_idn"]) ? $this->attributes["{$attribute}_idn"] : '';
        }

        return $value;
    }

    public function setStartDateAttribute($value)
    {
        if (empty($value)) {
            $value = null;
        }

        $this->attributes['start_date'] = $value;
    }

    public function setEndDateAttribute($value)
    {
        if (empty($value)) {
            $value = null;
        }

        $this->attributes['end_date'] = $value;
    }

    static function loadMore($skip = 0, $take = 10)
    {
        return static::where('status', self::STATUS_PUBLISH)
            ->orderBy('prio_no', 'desc')
            ->orderBy('created_at', 'desc')
            ->skip($skip)
            ->take($take)
            ->select('id', 'title_en', 'title_idn', 'content_en', 'content_idn', 'image_en', 'image_idn')
            ->get();
    }

   
}