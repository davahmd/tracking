<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PivotMerchantDistributor extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_BLOCKED = 2;

    protected $table = 'merchant_distributor';

    protected $guarded = [];
}
