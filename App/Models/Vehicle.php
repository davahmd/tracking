<?php

namespace App\Models;

use App\Collections\Traits\HasNewCollection;
use App\Traits\GlobalEloquentBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
	use SoftDeletes, GlobalEloquentBuilder, HasNewCollection;

    protected $table = 'vehicles';
    protected $fillable = ['brand', 'model', 'variant', 'year', 'brand_image', 'vehicle_image'];
    protected $dates = ['deleted_at'];

    public function scopeHasProductId($query, $data)
    {
        return $query->whereHas('products', function ($query) use ($data) {
            if (is_array($data)) {
                $query->whereIn((new Product)->getTable() . '.pro_id', $data);
            }
            else {
                $query->where((new Product)->getTable() . '.pro_id', $data);
            }
        });
    }

    public function mappings()
    {
        return $this->belongsToMany(CustomerVehicleMapping::class, 'product_vehicle_mappings', 'vehicle_id', 'vehicle_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_vehicle_mappings', 'vehicle_id', 'pro_id');
    }

    public function scopeGetBrandList($query)
    {
        return $query->distinct('brand')
            ->orderBy('brand')
            ->pluck('brand');
    }

    public function scopeHasActiveProducts($query)
    {
        return $query->whereHas('products', function ($query) {
            $query->where('pro_status', Product::STATUS_ACTIVE)->has('pricing');
        });
    }
}
