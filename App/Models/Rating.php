<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Collections\Traits\HasNewCollection;

class Rating extends Model
{
    use HasNewCollection;

    const REFERENCE_TYPE_PRODUCT = 1;
    const REFERENCE_TYPE_STORE = 2;

    protected $table = 'ratings';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    function images()
    {
        return $this->hasMany(RatingImage::class, 'rating_id');
    }

    function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'cus_id');
    }

    function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'order_id');
    }

    function getStarTagAttribute()
    {
        $result = '';

        for ($i=0; $i < $this->rating; $i++)
        {
            $result .= '<i class="fa fa-star shine"></i>';
        }

        for ($i=0; $i < (5 - $this->rating); $i++)
        {
            $result .= '<i class="fa fa-star"></i>';
        }

        return $result;
    }

    function getReferenceTypeTextAttribute()
    {
        return $this->reference_type == self::REFERENCE_TYPE_PRODUCT ? trans('localize.RATING.product') : trans('localize.RATING.store');
    }

    function getPercentageAttribute()
    {
        return $this->rating * 20;
    }

    function scopeAvailable($query)
    {
        return $query->where('display', 1);
    }

    function scopeRating($query, $rating)
    {
        if($rating && $rating > 0 && $rating <= 5)
        {
            return $query->where('rating', $rating);
        }

        return $query;
    }

    function scopeSortBy($query, $sortBy)
    {
        switch ($sortBy)
        {
            case 'latest':
                $query->orderBy('created_at', 'desc');
                break;

            case 'oldest':
                $query->orderBy('created_at', 'asc');
                break;

            case 'rating_asc':
                $query->orderBy('rating', 'asc');
                break;

            case 'rating_desc':
                $query->orderBy('rating', 'desc');
                break;

            default:
                $query->orderBy('created_at', 'desc');
                break;
        }

        return $query;
    }

    function toggleDisplay()
    {
        $this->display = !$this->display;
        return $this;
    }
}
