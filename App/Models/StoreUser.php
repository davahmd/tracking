<?php

namespace App\Models;

use App\Services\DeviceToken\Traits\HasDeviceToken;
use App\Services\Jwt\Auth\Traits\HasPersonalAccessToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\StoreUserResetPassword as StoreUserResetPasswordNotification;
use App\Traits\GlobalEloquentBuilder;

class StoreUser extends Authenticatable
{
    use Notifiable, HasPersonalAccessToken, GlobalEloquentBuilder;
    use Notifiable, HasPersonalAccessToken, HasDeviceToken;

    protected $guard = "storeusers";
    protected $table = 'nm_store_users';
    protected $guarded = ['created_at', 'updated_at'];
    protected $primaryKey = 'id';
    
    /* Relationships */
    public function assigned_stores()
    {
        return $this->hasMany(StoreUserMapping::class, 'storeuser_id', 'id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'mer_id', 'mer_id');
    }

    public function stores()
    {
        return $this->belongsToMany(Store::class, (new StoreUserMapping)->getTable(), 'storeuser_id', 'store_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /* End Relationships */

    public function activeStores()
    {
        return $this->stores()->where('stor_status', Store::STATUS_ACTIVE);
    }

    public function routeNotificationForMail()
    {
        $environment = app()->environment();

        if ($environment == 'production' || $environment == 'staging' || config('mail.host') == 'smtp.mailtrap.io') {
            return $this->email;
        }

        return '';
    }

    /**
     * This is to replace merchant reset password link in the email
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new StoreUserResetPasswordNotification($token));
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'customer.'.$this->cus_id;
    }
}