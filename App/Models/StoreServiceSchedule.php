<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class StoreServiceSchedule extends Authenticatable
{

    protected $table = 'store_service_schedule';
    protected $primaryKey = 'id';
    protected $fillable = ['stor_id', 'pro_id', 'interval_period', 'appoint_start', 'appoint_end' , 'schedule_skip_count'];

}
