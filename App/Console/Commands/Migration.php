<?php

namespace App\Console\Commands;

use App\Models\Shipping;
use Illuminate\Console\Command;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\Limit;
use App\Models\Order;
use App\Models\LimitAction;
use App\Models\MerchantVTokenLog;
use App\Models\ParentOrder;
use App\Models\TaxInvoice;
use App\Models\OrderMapping;
use App\Models\DeliveryOrder;
use Carbon\Carbon;
use App\Repositories\ProductPricingRepo;
use App\Models\Category;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Models\ProductPricing;
use App\Models\OrderOffline;
use App\Models\Wallet;
use App\Models\Country;

class Migration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:migration {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migration for selected module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $option = $this->argument('module');

            switch ($option) {
                case 'limit':
                    $this->migrate_store_limit();
                    break;

                case 'online_refunded':
                    $this->migrate_order_refunded_status();
                    break;

                case 'online_transactions':
                    $this->info('Clear required table');
                    ParentOrder::truncate();
                    TaxInvoice::truncate();
                    DeliveryOrder::truncate();
                    OrderMapping::truncate();
                    \DB::table('nm_order')
                    ->update([
                        'parent_order_id' => 0
                    ]);
                    \DB::table('nm_shipping')
                    ->update([
                        'parent_order_id' => 0
                    ]);
                    $this->info('Table cleared!');
                    $this->info('');

                    $this->info('Start migration...' . date('d M Y h:i a'));
                    $this->migrate_online_transactions();
                    $this->migrate_online_tax_invoices();
                    $this->migrate_online_shipments();
                    $this->migrate_order_shipping();
                    $this->info('Module migration complete' . date('d M Y h:i a'));
                    break;

                case 'pricing_sku':

                    $this->info('Clear required table');
                    \DB::table('nm_product_pricing')
                    ->update([
                        'sku' => null
                    ]);
                    \DB::table('categories')
                    ->update([
                        'count' => 0
                    ]);
                    $this->info('Table cleared!');
                    $this->info('');

                    $this->migrate_product_pricing_sku();
                    break;

                case 'category_code':

                    $random = false;
                    if ($this->confirm('Random category code?')) {
                        $random = true;
                    }

                    $this->generate_category_code($random);
                    break;

                case 'offline_tax_invoice':

                    $this->info('Reset offline tax invoice number');
                    \DB::table('order_offline')->update([
                        'tax_inv_no' => null
                    ]);
                    $this->info('Done!');
                    $this->info('');

                    $this->info('Start migration...' . date('d M Y h:i a'));
                    $this->migrate_offline_tax_invoice();
                    $this->info('Module migration complete' . date('d M Y h:i a'));
                    break;

                case 'migrate_delivery_orders_date':

                    $this->info('Start migration...' . date('d M Y h:i a'));
                    $this->migrate_delivery_orders_date();
                    $this->info('Complete' . date('d M Y h:i a'));
                    break;

                case 'migrate_order_deliveries':

                    $this->info('Start migration order shipment date...' . date('d M Y h:i a'));
                    $this->migrate_order_shipment_date();
                    $this->info('Complete' . date('d M Y h:i a'));

                    $this->info('Start migration delivery order type...' . date('d M Y h:i a'));
                    $this->migrate_delivery_order_type();
                    $this->info('Complete' . date('d M Y h:i a'));
                    break;

                default:
                $this->info('Invalid module');
                    break;
            }

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function migrate_store_limit()
    {
        $stores = Store::where('stor_type', '>', 0)
        ->orWhere('single_limit', '>', 0)
        // ->orWhere('daily_limit', '>', 0)
        // ->orWhere('daily_trans', '>', 0)
        ->orWhere('monthly_limit', '>', 0)
        ->orWhere('monthly_trans', '>', 0)
        ->orWhere('accept_payment', 1)
        ->get();

        foreach ($stores as $store) {
            $data = [
                'daily' => 0.00,
                'weekly' => 0.00,
                'monthly' => 0.00,
                'yearly'=> 0.00,
            ];

            // if($store->daily_trans > 0.00) {
            //     $data['daily'] = $store->daily_trans;
            //     $data['weekly'] = $store->daily_trans;
            //     $data['monthly'] = $store->daily_trans;
            //     $data['yearly'] = $store->daily_trans;
            // }

            if($store->monthly_trans > 0.00) {
                $data['monthly'] += $store->monthly_trans;
                $data['yearly'] += $store->monthly_trans;
            }

            $limit = Limit::create($data);
            $store->limit_id = $limit->id;
            $store->save();

            if($store->single_limit > 0.00) {
                LimitAction::create([
                    'limit_id' => $limit->id,
                    'type' => 1,
                    'action' => 2,
                    'amount' => $store->single_limit,
                    'number_transaction' => 0,
                    'order' => '1.2'
                ]);
            }

            // if($store->daily_limit > 0.00) {
            //     LimitAction::create([
            //         'limit_id' => $limit->id,
            //         'type' => 2,
            //         'action' => 2,
            //         'amount' => $store->daily_limit,
            //         'number_transaction' => 0,
            //         'order' => '2.2'
            //     ]);
            // }

            if($store->monthly_limit > 0.00) {
                LimitAction::create([
                    'limit_id' => $limit->id,
                    'type' => 4,
                    'action' => 2,
                    'amount' => $store->monthly_limit,
                    'number_transaction' => 0,
                    'order' => '4.2'
                ]);
            }

            $this->info('Store id : ' . $store->stor_id . ' updated');
        }
    }

    public function migrate_order_refunded_status()
    {
        $this->info('Start migration...' . date('d M Y h:i a'));
        $orders = Order::select('nm_order.order_id', 'nm_order.order_status', 'nm_merchant.mer_id')
        ->leftJoin('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')
        ->leftJoin('nm_merchant', 'nm_merchant.mer_id', '=', 'nm_product.pro_mr_id')
        ->where('nm_order.order_status', 5)
        ->get();

        $update = [];
        foreach ($orders as $order) {
            $refunded = false;
            $check = MerchantVTokenLog::where('mer_id', $order->mer_id)
            ->where('order_id', $order->order_id)
            ->where('debit_amount', '>', 0)
            ->count();

            if($check > 0) {
                $this->info('Order id : '.$order->order_id.' insert into update queue');
                array_push($update, $order->order_id);
            }
        }

        if(!empty($update)) {
            Order::whereIn('order_id', $update)->update(['order_status' => 6]);
            $this->info('Finish update for online order!');
        } else {
            $this->info('Nothing to be updated for online order.');
        }

        $this->info('Migration complete!  ' . date('d M Y h:i a'));
    }

    public function migrate_online_transactions()
    {
        $this->info('Start online transaction migration...');

        $items = Order::orderBy('order_date', 'ASC')->get()->groupBy('transaction_id');
        foreach ($items as $transaction_id => $orders)
        {
            $parent = new ParentOrder;
            $parent->customer_id = $orders->first()->order_cus_id;
            $parent->transaction_id = $transaction_id;
            $parent->date = $orders->first()->order_date;
            $parent->save();

            $orders_id = $orders->pluck('order_id')->toArray();

            Order::whereIn('order_id', $orders_id)->update([
                'parent_order_id' => $parent->id,
            ]);
        }

        $this->info('Online transaction migration complete!');
    }

    public function migrate_online_tax_invoices()
    {
        $this->info('Start online tax invoices migration...');
        $transactions = ParentOrder::with(['items' => function($q) {
            $q->selectRaw("nm_order.parent_order_id, nm_order.order_id, nm_merchant.mer_id as merchant_id,
                CASE WHEN order_type = 1 THEN 1 ELSE 2 END as item_type,
                CASE WHEN product_shipping_fees_type = 3 THEN 2 ELSE 1 END as shipment_type,
                CASE WHEN nm_country.co_id IS NOT NULL THEN nm_country.co_id ELSE 0 END as country_id
            ")
            ->leftJoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
            ->leftJoin('nm_merchant', 'nm_product.pro_mr_id', '=', 'nm_merchant.mer_id')
            ->leftJoin('nm_country', 'nm_country.co_curcode', '=', 'nm_order.currency')
            ->whereIn('order_type', [1,3,4,5]);
        }])->get();

        $results = [];
        foreach ($transactions as $transaction)
        {
            $default = $transaction->items->first();

            //group by merchant
            $byMerchants = $transaction->items->groupBy('merchant_id');
            foreach ($byMerchants as $merchant_id => $byMerchant)
            {
                //group by item type
                $byItems = $byMerchant->groupBy('item_type');
                foreach($byItems as $item_type => $byItem)
                {
                    $orders_id = [];
                    if($item_type == 1)
                    {
                        //group by shipment_type
                        $byShippings = $byItem->groupBy('shipment_type');
                        foreach ($byShippings as $shipment_type => $byShipping) {
                            $orders_id = $byShipping->pluck('order_id')->toArray();
                            $results[] = [
                                'parent_order_id' => $transaction->id,
                                'date' => $transaction->date,
                                'customer_id' => $transaction->customer_id,
                                'merchant_id' => $merchant_id,
                                'item_type' => $item_type,
                                'shipment_type' => $shipment_type,
                                'order_id' => $orders_id,
                                'country_id' => $default->country_id,
                            ];
                        }

                    } else {
                        $orders_id = $byItem->pluck('order_id')->toArray();
                        $results[] = [
                            'parent_order_id' => $transaction->id,
                            'date' => $transaction->date,
                            'customer_id' => $transaction->customer_id,
                            'merchant_id' => $merchant_id,
                            'item_type' => $item_type,
                            'shipment_type' => 0,
                            'order_id' => $orders_id,
                            'country_id' => $default->country_id,
                        ];
                    }
                }
            }
        }

        $year = 0;
        $month = 0;

        foreach ($results as $data)
        {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $data['date']);
            $year = $date->format('y');
            $month = $date->format('m');
            $runningNo = 1;

            $taxInvoicesNumber = TaxInvoice::where('country_id', $data['country_id'])->where('tax_number', 'LIKE', $year.$month.'%')->orderBy('tax_number', 'desc')->value('tax_number');

            if($taxInvoicesNumber)
            {
                $runningNo = (integer)substr($taxInvoicesNumber, 4) + 1;
            }

            $invoice = new TaxInvoice;
            $invoice->tax_number = $year.$month.str_pad($runningNo, 4, '0', STR_PAD_LEFT);
            $invoice->parent_order_id = $data['parent_order_id'];
            $invoice->customer_id = $data['customer_id'];
            $invoice->merchant_id = $data['merchant_id'];
            $invoice->item_type = $data['item_type'];
            $invoice->shipment_type = $data['shipment_type'];
            $invoice->country_id = $data['country_id'];
            $invoice->created_at = $data['date'];
            $invoice->save();

            $timestamp = Carbon::now('utc');
            $items = [];
            foreach ($data['order_id'] as $order_id) {
                $items[] = [
                    'tax_invoice_id' => $invoice->id,
                    'order_id' => $order_id,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp,
                ];
            }

            OrderMapping::insert($items);
        }

        $this->info('Online tax invoices migration complete!');
    }

    public function migrate_online_shipments()
    {
        $this->info('Start online shipments migration...');

        $results = $this->getOnlineShipmentResults('withVirtual');

        $year = 0;
        $month = 0;
        $runningNo = 1;
        foreach ($results as $data)
        {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $data['shipment_date']);
            $dYear = $date->format('y');
            $dMonth = $date->format('m');

            if($year != $dYear || $month != $dMonth)
            {
                $runningNo = 1;
                $year = $dYear;
                $month = $dMonth;
            }

            $do = new DeliveryOrder;
            $do->type = $data['type'];
            $do->tax_invoice_id = $data['tax_invoice_id'];
            $do->do_number = $year.$month.str_pad($runningNo, 4, '0', STR_PAD_LEFT);
            $do->courier_id = $data['courier_id'];
            $do->tracking_number = $data['tracking_number'];
            $do->appointment_detail = $data['appointment_detail'];
            $do->shipment_date = $data['shipment_date'];
            $do->save();

            OrderMapping::whereIn('order_id', $data['orders_id'])->update([
                'delivery_order_id' => $do->id,
            ]);

            $runningNo++;
        }

        $this->info('Online shipments migration complete!');
    }

    public function migrate_order_shipping()
    {
        $this->info('Start online shipping address migration...');

        $orders = Order::orderBy('order_date', 'ASC')->get();

        foreach ($orders as $order) {
            Shipping::where('ship_order_id', '=', $order->order_id)->update([
                'parent_order_id' => $order->parent_order_id
            ]);
        }

        $this->info('Online shipping address migration complete!');
    }

    public function generate_category_code($random = false)
    {
        $this->info('Start generate category code...' . date('d M Y h:i a'));

        if(!$random)
        {
            Category::query()->update([
                'code' => null,
            ]);

            $updates = [
                3 => 'B',
                    14 => 'BFC',
                    22 => 'BC',
                    66 => 'BPC',
                        69 => 'BPCBB',
                        70 => 'BPCHC',
                    67 => 'BF',
                        71 => 'BFMMF',
                        72 => 'BFWWF',
                    68 => 'BBT',
                10 => 'XW',
                    11 => 'XWC',
                    26 => 'XWJ',
                    51 => 'XWLB',
                34 => 'HN',
                35 => 'H',
                    36 => 'HC',
                    64 => 'HPC',
                38 => 'XEV',
                39 => 'CV',
                42 => 'EC',
                43 => 'ELC',
                52 => 'XBD',
                53 => 'TIC',
                54 => 'P',
                    74 => 'PF',
                    75 => 'PS',
                    76 => 'PE',
                55 => 'XMP',
                56 => 'XF',
                    57 => 'XFW',
                        59 => 'XFWJ',
                    58 => 'XFM',
                62 => 'FA',
                    63 => 'FAS',
                    65 => 'FAJ',
                    73 => 'FAB',
            ];

            $bar = $this->output->createProgressBar(count($updates));

            foreach ($updates as $id => $code) {
                $category = Category::find($id);
                if($category)
                {
                    $category->code = $code;
                    $category->save();
                }

                $bar->advance();
            }

            $bar->finish();

        } else {

            $categories = Category::all();
            $bar = $this->output->createProgressBar(count($categories));

            $generatedCode = [];
            foreach ($categories as $category) {

                $found = false;
                $code = '';
                do {
                    $code = $this->randomString(5);
                    if(!in_array($code, $generatedCode))
                    {
                        $found = true;
                        $generatedCode[] = $code;
                    }
                } while (!$found);

                $category->code = $code;
                $category->save();

                $bar->advance();
            }

            $bar->finish();
        }

        $this->info(' ' . date('d M Y h:i a'));
    }

    public function migrate_product_pricing_sku()
    {

        $this->info('Start product pricing sku migration...' . date('d M Y h:i a'));

        ProductPricingRepo::update_sku_items();

        $this->info('Completed...' . date('d M Y h:i a'));

        $this->info('Start product order sku migration...' . date('d M Y h:i a'));
        $orders = Order::with('pricing')->get();
        $bar = $this->output->createProgressBar(count($orders));
        foreach ($orders as $order)
        {
            if($order->pricing && $order->pricing->sku)
            {
                \DB::table('nm_order')
                ->where('order_id', $order->order_id)
                ->update([
                    'sku' => $order->pricing->sku
                ]);
            }
            $bar->advance();
        }
        $bar->finish();

        $this->info(' ' . date('d M Y h:i a'));
    }

    public function getOnlineShipmentResults($operation = 'withOutVirtual')
    {
        $results = [];

        switch ($operation) {
            case 'withOutVirtual':
                $invoices = TaxInvoice::whereHas('items', function($q) {
                    $q->whereNotNull('order_tracking_no');
                    $q->where('order_status', '>', 2);
                })
                ->with(['items' => function ($q) {
                    $q->selectRaw("
                        nm_order.order_id, order_tracking_no, order_courier_id,
                        CASE WHEN order_shipment_date IS NULL THEN nm_order.created_at ELSE order_shipment_date END AS shipment_date
                    ");
                }])
                ->where('item_type', 1)
                ->get();

                $byShipments = $invoices->groupBy('shipment_type');

                foreach ($byShipments as $shipment_type => $taxInvoices)
                {
                    foreach ($taxInvoices as $invoice)
                    {
                        $byTrackings = $invoice->items->groupBy('order_tracking_no');
                        foreach ($byTrackings as $items)
                        {
                            $item = $items->first();
                            if($shipment_type == 1)
                            {
                                $results[] = [
                                    'tax_invoice_id' => $invoice->id,
                                    'tracking_number' => $item->order_tracking_no,
                                    'appointment_detail' => null,
                                    'courier_id' => $item->order_courier_id? $item->order_courier_id : 0,
                                    'shipment_date' => $item->shipment_date,
                                    'orders_id' => $items->pluck('order_id')->toArray(),
                                ];
                            } else {
                                $results[] = [
                                    'tax_invoice_id' => $invoice->id,
                                    'tracking_number' => null,
                                    'appointment_detail' => $item->order_tracking_no,
                                    'courier_id' => 0,
                                    'shipment_date' => $item->shipment_date,
                                    'orders_id' => $items->pluck('order_id')->toArray(),
                                ];
                            }
                        }
                    }
                }
                break;

            case 'withVirtual':
                $invoices = TaxInvoice::whereHas('items')
                ->with(['items' => function ($q) {
                    $q->selectRaw("
                        nm_order.order_id, order_tracking_no, CASE WHEN order_courier_id IS NULL THEN 0 ELSE order_courier_id END as order_courier_id, order_status,
                        CASE WHEN order_shipment_date IS NULL THEN nm_order.created_at ELSE order_shipment_date END AS shipment_date,
                        CASE WHEN order_tracking_no IS NOT NULL THEN TO_BASE64(order_tracking_no) END AS order_tracking_no_encoded
                    ");
                }])
                ->get();

                $invoices = $invoices->groupBy('item_type')->transform(function($orders) {
                    return $orders->groupBy('shipment_type');
                });

                foreach ($invoices as $type => $types)
                {
                    foreach ($types as $shipment => $shipments)
                    {
                        foreach ($shipments as $invoice)
                        {
                            switch ($type) {
                                case 1:
                                    $deliveries = $invoice->items->groupBy('order_tracking_no_encoded');
                                    foreach ($deliveries as $tracking => $items)
                                    {

                                        $item = $items->first();
                                        $shipmentType = ($item->order_courier_id && $tracking && !str_contains(strtolower($item->order_tracking_no), ['self', 'pickup', 'pick', 'up', 'collect']))? 1 : 2;

                                        $results[] = [
                                            'type' => $shipmentType,
                                            'tax_invoice_id' => $invoice->id,
                                            'tracking_number' => $shipmentType == 1? $item->order_tracking_no : null,
                                            'appointment_detail' => $shipmentType == 2? $item->order_tracking_no : null,
                                            'courier_id' => $shipmentType == 1 && $item->order_courier_id? $item->order_courier_id : 0,
                                            'shipment_date' => $item->shipment_date,
                                            'orders_id' => $items->pluck('order_id')->toArray(),
                                        ];
                                    }
                                    break;

                                case 2:
                                    $items = $invoice->items;
                                    $item = $items->first();
                                    $orders_id = $items->pluck('order_id')->toArray();

                                    $results[] = [
                                        'type' => 0,
                                        'tax_invoice_id' => $invoice->id,
                                        'tracking_number' => null,
                                        'appointment_detail' => null,
                                        'courier_id' => 0,
                                        'shipment_date' => $item->shipment_date,
                                        'orders_id' => $items->pluck('order_id')->toArray(),
                                    ];
                                    break;
                            }

                        }
                    }
                }
                break;
        }

        return $results;
    }

    public function randomString($length)
    {
        $random_string = "";
        $valid_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $num_valid_chars = strlen($valid_chars);

        for ($i = 0; $i < $length; $i++)
        {
            $random_pick = mt_rand(1, $num_valid_chars);
            $random_char = $valid_chars[$random_pick-1];
            $random_string .= $random_char;
        }

        return $random_string;
    }

    public function migrate_offline_tax_invoice()
    {
        $onlyWallet = Wallet::where('generate_offline_invoice', 1)->pluck('id');

        //include transaction without wallet and special wallet
        $onlyWallet->push(0);
        $onlyWallet->push(99);
        $onlyWallet = $onlyWallet->all();

        $orders = OrderOffline::whereIn('status', [1,4])->whereIn('wallet_id', $onlyWallet)->get();

        $bar = $this->output->createProgressBar(count($orders));

        $byCountries = $orders->groupBy('currency');

        foreach ($byCountries as $currency => $orders)
        {
            $year = 0;
            $month = 0;
            $count = 0;

            $country = Country::where('co_curcode', $currency)->first();
            $timezone = $country->timezone;

            foreach ($orders as $key => $order)
            {
                $date = Carbon::createFromTimestamp(strtotime($order->created_at))->timezone($timezone);

                if($month != $date->format('m') || $year != $date->format('y'))
                {
                    $count = 1;
                    $year = $date->format('y');
                    $month = $date->format('m');
                }

                $invoice_number = $year.$month.str_pad($count, 4, '0', STR_PAD_LEFT);

                \DB::table('order_offline')
                ->where('id', $order->id)
                ->update([
                    'tax_inv_no' => $invoice_number
                ]);

                $count++;

                $bar->advance();
            }
        }

        $bar->finish();
        $this->info('');

        return true;
    }

    public function migrate_delivery_orders_date()
    {
        $deliveries = DeliveryOrder::with('items', 'invoice')
        ->whereBetween('shipment_date', ['2018-07-09 10:29:37', '2018-07-09 10:29:48'])
        ->get();

        $bar = $this->output->createProgressBar(count($deliveries));

        foreach ($deliveries as $do)
        {
            $item = $do->items->sortBy('order_shipment_date')->first();
            $invoice = $do->invoice;

            \DB::table('delivery_orders')
            ->where('id', $do->id)
            ->update([
                'shipment_date' => $item->order_shipment_date ? $item->order_shipment_date : $invoice->created_at,
                'updated_at' => $do->updated_at
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info('');
    }

    public function migrate_order_shipment_date()
    {
        $orders = Order::withAndWhereHas('mapping.delivery_order')
        ->where(function ($query) {
            $query->where('product_shipping_fees_type', '<>', 3)
            ->whereIn('order_status', [3,4])
            ->where('order_type', 1)
            ->whereNull('order_shipment_date');
        })->get();

        $bar = $this->output->createProgressBar(count($orders));

        foreach ($orders as $order)
        {
            $delivery = $order->mapping->delivery_order;

            \DB::table('nm_order')
            ->where('order_id', $order->order_id)
            ->update([
                'order_shipment_date' => $delivery->shipment_date,
                'updated_at' => $order->updated_at
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info('');
    }

    public function migrate_delivery_order_type()
    {
        $deliveries = DeliveryOrder::with('invoice')->select('delivery_orders.*')
        ->leftJoin('tax_invoices', 'tax_invoices.id', '=', 'delivery_orders.tax_invoice_id')
        ->where(function ($query) {
                $query->where('delivery_orders.type', 2)
                ->where('delivery_orders.courier_id', '>', 0)
                ->whereRaw("delivery_orders.type <> tax_invoices.shipment_type");
        })->get();

        $bar = $this->output->createProgressBar(count($deliveries));

        foreach ($deliveries as $do)
        {
            \DB::table('delivery_orders')
            ->where('id', $do->id)
            ->update([
                'type' => $do->invoice->shipment_type,
                'updated_at' => $do->updated_at
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->info('');
    }
}
