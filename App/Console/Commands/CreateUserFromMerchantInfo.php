<?php

namespace App\Console\Commands;

use App\Models\Customer;
use App\Models\Merchant;
use App\Models\StoreUser;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateUserFromMerchantInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Customer::cursor() as $customer) {
            DB::transaction(function () use ($customer) {
                $existingUser = User::where('email', $customer->email)->first();

                if ($existingUser ) {
                    if (!$existingUser->customer) {
                        $customer->update([
                            'user_id' => $existingUser->id
                        ]);
                    }
                } else {
                    if ($customer->email) {
                        $user = User::forceCreate([
                            'email' => $customer->email,
                            'password' => $customer->password,
                            'status' => User::STATUS_UNBLOCK,
                            'activation' => User::ACTIVATION_TRUE,
                            'updated_at' => $customer->updated_at,
                            'created_at' => $customer->created_at
                        ]);

                        $customer->update([
                            'user_id' => $user->id
                        ]);
                    }
                }
            });
        }

        foreach (Merchant::cursor() as $merchant) {
            DB::transaction(function () use ($merchant) {

                $existingUser = User::where('email', $merchant->email)->first();

                if ($existingUser) {
                    if (!$existingUser->merchant) {
                        $merchant->update([
                            'user_id' => $existingUser->id
                        ]);
                    }

                    if (!$existingUser->customer) {
                        $existingUser->customer()->create([
                            'cus_name' => $merchant->mer_fname . ' ' . $merchant->mer_lname
                        ]);
                    }
                }
                else {
                    if ($merchant->email) {
                        $user = User::forceCreate([
                            'email' => $merchant->email,
                            'password' => $merchant->password,
                            'status' => User::STATUS_UNBLOCK,
                            'activation' => User::ACTIVATION_TRUE,
                            'updated_at' => $merchant->updated_at,
                            'created_at' => $merchant->created_at
                        ]);

                        $user->customer()->create([
                            'cus_name' => $merchant->mer_fname . ' ' . $merchant->mer_lname
                        ]);

                        $merchant->update(['user_id' => $user->id]);
                    }
                }
            });
        }

        foreach (StoreUser::cursor() as $storeUser) {
            DB::transaction(function () use ($storeUser) {
                $existingUser = User::where('email', $storeUser->email)->first();

                if ($existingUser) {
                    if (!$existingUser->storeUser && !$existingUser->merchant) {
                        $storeUser->update([
                            'user_id' => $existingUser->id
                        ]);
                    }

                    if (!$existingUser->customer) {
                        $existingUser->customer()->create([
                            'cus_name' => $existingUser->name
                        ]);
                    }
                } else {
                    if ($storeUser->email) {
                        $user = User::forceCreate([
                            'email' => $storeUser->email,
                            'password' => $storeUser->password,
                            'status' => User::STATUS_UNBLOCK,
                            'activation' => User::ACTIVATION_TRUE,
                            'updated_at' => $storeUser->updated_at,
                            'created_at' => $storeUser->created_at
                        ]);

                        $user->customer()->create([
                            'cus_name' => $storeUser->name
                        ]);

                        $storeUser->update(['user_id' => $user->id]);
                    }
                }
            });
        }
    }
}
