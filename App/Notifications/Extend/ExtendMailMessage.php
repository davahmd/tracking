<?php

namespace App\Notifications\Extend;

use Illuminate\Notifications\Messages\MailMessage;

class ExtendMailMessage extends MailMessage
{
    public $orders = null;

    public $service = null;

    public $negotiation = null;

    public function orders($orders = null)
    {
        $this->orders = $orders;

        return $this;
    }

    public function service($services = null)
    {
        $this->service = $services->first();

        return $this;
    }

    public function negotiation($negotiation = null)
    {
        $this->negotiation = $negotiation;

        return $this;
    }

    public function toArray()
    {
        return [
            'level' => $this->level,
            'subject' => $this->subject,
            'greeting' => $this->greeting,
            'salutation' => $this->salutation,
            'introLines' => $this->introLines,
            'outroLines' => $this->outroLines,
            'actionText' => $this->actionText,
            'actionUrl' => $this->actionUrl,
            'orders' => $this->orders,
            'service' => $this->service,
            'negotiation' => $this->negotiation
        ];
    }
}