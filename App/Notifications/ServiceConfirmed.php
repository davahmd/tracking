<?php

namespace App\Notifications;

use App\Notifications\Extend\ExtendMailMessage;
use App\Notifications\Traits\NotificationTrait;
use Edujugon\PushNotification\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class ServiceConfirmed extends Notification implements ShouldQueue
{
    use Queueable, NotificationTrait;

    protected $service;

    public function __construct(Collection $service)
    {
        $this->service = $service;
    }

    public function via($notifiable)
    {
        return ['broadcast', 'database', FcmChannel::class, 'mail'];
    }

    public function toMail($notifiable)
    {
        return (new ExtendMailMessage)
            ->greeting('Hi,')
            ->subject($this->text($notifiable))
            ->line($this->text($notifiable))
            ->service($this->service)
            ->orders(collect()->push($this->service->first()->order))
            ->action(trans('localize.view') . ' #' . $this->service->first()->id, url('/'));
    }

    public function toFcm($notifiable)
    {
        return (new PushMessage)
            ->title(config('app.name'))
            ->body(strip_tags($this->text($notifiable)))
            ->extra($this->toArray($notifiable));
    }

    public function toArray($notifiable)
    {
        return [
            'type' => 'service',
            'service_id' => $this->service->first()->id,
            'trans_title' => 'trans("' . $this->transId($notifiable) . '.title")',
            'trans_body' => 'trans("' . $this->transId($notifiable) . '.body", ["service_id" => ' . $this->service->first()->id . '])',
            'event' => kebab_case(class_basename(get_class($this))),
            'image' => $this->service->first()->order->product->mainImageUrl(),
            'link' => url('/')
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage($this->toArray($notifiable));
    }

    protected function transId($notifiable)
    {
        return 'notifications.' . $this->notifiableType($notifiable) . '.service.confirmed';
    }
}
