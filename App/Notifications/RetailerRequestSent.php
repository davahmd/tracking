<?php

namespace App\Notifications;

use App\Models\Merchant;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RetailerRequestSent extends Notification
{
    use Queueable;

    protected $distributor;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Merchant $distributor)
    {
        $this->distributor = $distributor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hi, ' . $notifiable->full_name())
            ->line('Your request to become a retailer has been sent.')
            ->line('Distributor Name: ' . '<strong>' . $this->distributor->full_name() . '</strong>')
            ->action('View ' . $this->distributor->full_name(), url('merchants/' . $this->distributor->mer_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
