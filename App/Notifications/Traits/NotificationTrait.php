<?php

namespace App\Notifications\Traits;

use App\Models\Customer;
use App\Models\Merchant;

trait NotificationTrait
{
    protected function notifiableType($notifiable)
    {
        return $notifiable instanceof Customer ? 'customer' : 'merchant';
    }

    protected function text($notifiable)
    {
        return eval('return ' . $this->toArray($notifiable)['trans_body'] . ';');
    }

    protected function actionLink($notifiable)
    {
        if ($notifiable instanceof Customer) {
            return (string) route('nego-detail', $this->negotiation);
        }
        elseif ($notifiable instanceof Merchant) {
            return (string) route('merchant.negotiation.view', $this->negotiation);
        }
    }
}