<?php

namespace App\Notifications;

use App\Models\PriceNegotiation;
use App\Notifications\Extend\ExtendMailMessage;
use App\Notifications\Traits\NotificationTrait;
use Edujugon\PushNotification\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PriceNegotiationCounterOffer extends Notification
{
    use Queueable, NotificationTrait;

    /**
     * @var PriceNegotiation
     */
    protected $negotiation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(PriceNegotiation $negotiation)
    {
        $this->negotiation = $negotiation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', FcmChannel::class, 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new ExtendMailMessage)
            ->greeting('Hi,')
            ->subject($this->text($notifiable))
            ->line($this->text($notifiable))
            ->negotiation($this->negotiation)
            ->action('View Negotiation #' . $this->negotiation->id, $this->actionLink($notifiable));
    }

    public function toFcm($notifiable)
    {
        return (new PushMessage)
            ->title(config('app.name'))
            ->body(strip_tags($this->text($notifiable)))
            ->extra($this->toArray($notifiable));
    }

    public function toArray($notifiable)
    {
        return [
            'type' => 'price_negotiation',
            'negotiation_id' => $this->negotiation->id,
            'trans_title' => 'trans("' . $this->transId($notifiable) . '.title")',
            'trans_body' => 'trans("' . $this->transId($notifiable) . '.body")',
            'event' => kebab_case(class_basename(get_class($this))),
            'image' => $this->negotiation->product->mainImageUrl(),
            'link' => url('/')
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage($this->toArray($notifiable));
    }

    protected function transId($notifiable)
    {
        return 'notifications.' . $this->notifiableType($notifiable) . '.price-negotiation.counter-offer';
    }
}
