<?php

namespace App\Notifications\Complain;

use App\Models\Complain;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CustomerReceivedComplainMessage extends Notification
{
    use Queueable;

    public $complain;

    public $complainant;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Complain $complain, Model $complainant)
    {
        $this->complain = $complain;
        $this->complainant = $complainant;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $responder = $this->complain->complain_messages->last()->responder;

        return (new MailMessage)
            ->subject(trans('localize.complain.mail.subject', ['responder' => trans('localize.' . class_basename($responder))]))
            ->line(trans('localize.complain.mail.line_1'))
            ->action(trans('localize.complain.mail.action'), route('complain::complain-message', [$this->complain->complainable->getKey(), $this->complain->getKey()]))
            ->line(trans('localize.complain.mail.line_2'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
