<?php

namespace App\Notifications;

use App\Notifications\Extend\ExtendMailMessage;
use App\Notifications\Traits\NotificationTrait;
use Edujugon\PushNotification\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;
use App\Models\Customer;

class OrderCompleted extends Notification implements ShouldQueue
{
    use Queueable, NotificationTrait;

    protected $orders;

    public function __construct(Collection $orders)
    {
        $this->orders = $orders;
    }

    public function via($notifiable)
    {
        return ['broadcast', 'database', FcmChannel::class, 'mail'];
    }

    public function toMail($notifiable)
    {
        $mail = (new ExtendMailMessage)
            ->greeting('Hi,')
            ->subject($this->text($notifiable))
            ->line($this->text($notifiable))
            ->orders($this->orders);

        if ($notifiable instanceof Customer) {
            $mail
                ->action(trans('localize.view') . ' #' . $this->orders->first()->transaction_id, route('order-detail', [$this->orders->first()->transaction_id]))
                ->line(trans('notifications.customer.order.completed.review.line_1'))
                ->line(trans('notifications.customer.order.completed.review.line_2'));
        }
        else
        {
            $mail->action(trans('localize.view') . ' #' . $this->orders->first()->transaction_id, url('/'));
        }

        return $mail;
    }

    public function toFcm($notifiable)
    {
        return (new PushMessage)
            ->title(config('app.name'))
            ->body(strip_tags($this->text($notifiable)))
            ->extra($this->toArray($notifiable));
    }

    public function toArray($notifiable)
    {
        return [
            'type' => 'transaction',
            'transaction_id' => $this->orders->first()->transaction_id,
            'trans_title' => 'trans("' . $this->transId($notifiable) . '.title")',
            'trans_body' => 'trans("' . $this->transId($notifiable) . '.body", ["transaction_id" => ' . "'" . $this->orders->first()->transaction_id . "'" .'])',
            'event' => kebab_case(class_basename(get_class($this))),
            'image' => $this->orders->first()->product->mainImageUrl(),
            'link' => url('/')
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage($this->toArray($notifiable));
    }

    protected function transId($notifiable)
    {
        return 'notifications.' . $this->notifiableType($notifiable) . '.order.completed';
    }
}
