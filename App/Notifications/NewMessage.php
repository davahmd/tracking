<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;

use App\Models\Chat;
use App\Helpers\Helper;

use Edujugon\PushNotification\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;

class NewMessage extends Notification
{
    use Queueable;

    /**
     * @var Chat
     */
    protected $chat;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        return (new PushMessage)
            ->title(config('app.name'))
            ->body($this->chat->message)
            ->extra([
                'id' => $this->chat->id,
                'store' => [
                    'id' => $this->chat->store->stor_id,
                    'name' => $this->chat->store->stor_name
                ],
                'customer' => [
                    'id' => $this->chat->customer->cus_id,
                    'name' => $this->chat->customer->cus_name,
                ],
                'product' => [
                    'id' => $this->chat->product->pro_id,
                    'name' => $this->chat->product->title
                ],
                // 'schedule' => [
                //     'id' => $this->chat->schedule->id,
                //     'service_name' => $this->chat->schedule->service_name_current
                // ],
                'sender_type' => $this->chat->sender_type,
                'timestamp' => $this->chat->created_at,
                'type' => 'chat'
            ]);
    }

    public function toBroadcast($notifiable)
    {
        $customer = $this->chat->customer()->first(['cus_name']);
        $store = $this->chat->store()->first(['stor_name']);

        return new BroadcastMessage([
            'data' => [
                'cus_name' => $customer->cus_name,
                'store_name' => $store->stor_name,
                'chat' => $this->chat,
            ]
        ]);
    }
}
