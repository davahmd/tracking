<?php

namespace App\Notifications;

use App\Notifications\Extend\ExtendMailMessage;
use App\Notifications\Traits\NotificationTrait;
use Edujugon\PushNotification\Channels\FcmChannel;
use Edujugon\PushNotification\Messages\PushMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class OrderReceived extends Notification implements ShouldQueue
{
    use Queueable, NotificationTrait;

    protected $orders;

    public function __construct(Collection $orders)
    {
        $this->orders = $orders;
    }

    public function via($notifiable)
    {
        return ['broadcast', 'database', FcmChannel::class, 'mail'];
    }

    public function toMail($notifiable)
    {
        return (new ExtendMailMessage)
            ->greeting('Hi,')
            ->subject($this->text($notifiable))
            ->line($this->text($notifiable))
            ->orders($this->orders)
            ->action(trans('localize.view') . ' #' . $this->orders->first()->transaction_id, url('/'));
    }

    public function toFcm($notifiable)
    {
        return (new PushMessage)
            ->title(config('app.name'))
            ->body(strip_tags($this->text($notifiable)))
            ->extra($this->toArray($notifiable));
    }

    public function toArray($notifiable)
    {
        return [
            'type' => 'transaction',
            'transaction_id' => $this->orders->first()->transaction_id,
            'trans_title' => 'trans("' . $this->transId($notifiable) . '.title")',
            'trans_body' => 'trans("' . $this->transId($notifiable) . '.body", ["transaction_id" => ' . "'" . $this->orders->first()->transaction_id . "'" .'])',
            'event' => kebab_case(class_basename(get_class($this))),
            'image' => $this->orders->first()->product->mainImageUrl(),
            'link' => url('/')
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage($this->toArray($notifiable));
    }

    protected function transId($notifiable)
    {
        return 'notifications.' . $this->notifiableType($notifiable) . '.order.received';
    }
}
