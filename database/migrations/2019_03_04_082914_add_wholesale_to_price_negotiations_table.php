<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWholesaleToPriceNegotiationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('price_negotiations', function (Blueprint $table) {
            $table->boolean('wholesale')->default(0)->after('merchant_offer_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('price_negotiations', function (Blueprint $table) {
            $table->drop('wholesale');
        });
    }
}
