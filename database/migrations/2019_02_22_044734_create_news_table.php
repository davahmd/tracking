<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE `news` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `title_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'title english',
            `title_idn` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'title simplified chinese',
            `content_en` text CHARACTER SET utf8 DEFAULT NULL COMMENT 'content english',
            `content_idn` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Content Simplified Chinese',
            `image_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `image_idn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `prio_no` tinyint(3) unsigned DEFAULT 0,
            `status` tinyint(3) unsigned NOT NULL,
            `creator_uid` int(10) unsigned NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
            `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
            PRIMARY KEY (`id`),
            KEY `announcements__created_at__status__idx` (`created_at`,`status`)
            ) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('app.env') == 'local') {
            Schema::dropIfExists('news');
        }
    }
}
