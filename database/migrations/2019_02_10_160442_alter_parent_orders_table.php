<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParentOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parent_orders', function (Blueprint $table) {
            $table->decimal('discounted_order_amount', 17, 2)->after('order_amount')->default(0);
            $table->decimal('discounted_shipping_amount', 17, 2)->after('shipping_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
