<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNmCustomerTableAddUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_customer', function (Blueprint $table) {
            $table->integer('user_id')->after('cus_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_customer', function (Blueprint $table) {
            if (Schema::hasColumn('nm_customer', 'user_id')) {
                $table->dropColumn('user_id');
            }
        });
    }
}
