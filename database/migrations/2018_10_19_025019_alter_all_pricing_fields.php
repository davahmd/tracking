<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterAllPricingFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE `zona`.`invoice_items`
            CHANGE COLUMN `merchant_charge_amount` `merchant_charge_amount` DECIMAL(17,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `gst_amount` `gst_amount` DECIMAL(17,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`limit_actions`
            CHANGE COLUMN `amount` `amount` DECIMAL(17,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`limits`
            CHANGE COLUMN `daily` `daily` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `weekly` `weekly` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `monthly` `monthly` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `yearly` `yearly` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`merchant_vtoken_log`
            CHANGE COLUMN `credit_amount` `credit_amount` DECIMAL(17,2) NOT NULL COMMENT 'topup' ,
            CHANGE COLUMN `debit_amount` `debit_amount` DECIMAL(17,2) NOT NULL COMMENT 'deduction' ,
            CHANGE COLUMN `currency_rate` `currency_rate` DECIMAL(17,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_country`
            CHANGE COLUMN `co_rate` `co_rate` DECIMAL(17,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `co_offline_rate` `co_offline_rate` DECIMAL(17,2) NOT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_merchant`
            CHANGE COLUMN `earning` `earning` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_order`
            CHANGE COLUMN `currency_rate` `currency_rate` DECIMAL(17,2) NOT NULL ,
            CHANGE COLUMN `product_original_price` `product_original_price` DECIMAL(17,2) NOT NULL COMMENT 'product price before discount:nm_product_pricing->price' ,
            CHANGE COLUMN `product_price` `product_price` DECIMAL(17,2) NOT NULL COMMENT 'product price after discount if any:nm_product_pricing->price||nm_product_pricing->discounted_price' ,
            CHANGE COLUMN `total_product_price` `total_product_price` DECIMAL(17,2) NOT NULL COMMENT 'total of product_price * order_qty' ,
            CHANGE COLUMN `product_shipping_fees` `product_shipping_fees` DECIMAL(17,2) NULL DEFAULT NULL COMMENT 'product shipping fees : nm_product_pricing->shipping_fees' ,
            CHANGE COLUMN `total_product_shipping_fees` `total_product_shipping_fees` DECIMAL(17,2) NULL DEFAULT NULL COMMENT 'total of product_shipping_fees * order_qty' ,
            CHANGE COLUMN `cus_platform_charge_value` `cus_platform_charge_value` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `cus_service_charge_value` `cus_service_charge_value` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `merchant_charge_value` `merchant_charge_value` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `order_value` `order_value` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_product_pricing`
            CHANGE COLUMN `price` `price` DECIMAL(17,2) NOT NULL ,
            CHANGE COLUMN `shipping_fees` `shipping_fees` DECIMAL(17,2) NULL DEFAULT '0.00' ,
            CHANGE COLUMN `discounted_price` `discounted_price` DECIMAL(17,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_store`
            CHANGE COLUMN `single_limit` `single_limit` DECIMAL(17,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `monthly_limit` `monthly_limit` DECIMAL(17,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `monthly_trans` `monthly_trans` DECIMAL(17,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `default_price` `default_price` DECIMAL(17,2) NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_withdraw_request`
            CHANGE COLUMN `wd_total_wd_amt` `wd_total_wd_amt` DECIMAL(17,2) NOT NULL ,
            CHANGE COLUMN `wd_submited_wd_amt` `wd_submited_wd_amt` DECIMAL(17,2) NOT NULL ,
            CHANGE COLUMN `wd_admin_comm_amt` `wd_admin_comm_amt` DECIMAL(17,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `wd_balance_after` `wd_balance_after` DECIMAL(17,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`store_service_extra`
            CHANGE COLUMN `service_price` `service_price` DECIMAL(17,2) UNSIGNED NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`temp_cart`
            CHANGE COLUMN `purchasing_price` `purchasing_price` DECIMAL(17,2) NOT NULL ,
            CHANGE COLUMN `product_price` `product_price` DECIMAL(17,2) NOT NULL ,
            CHANGE COLUMN `online_platform_charge_value` `online_platform_charge_value` DECIMAL(17,2) NOT NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            ALTER TABLE `zona`.`invoice_items`
            CHANGE COLUMN `merchant_charge_amount` `merchant_charge_amount` FLOAT(11,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `gst_amount` `gst_amount` FLOAT(11,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`limit_actions`
            CHANGE COLUMN `amount` `amount` DECIMAL(11,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`limits`
            CHANGE COLUMN `daily` `daily` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `weekly` `weekly` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `monthly` `monthly` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `yearly` `yearly` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`merchant_vtoken_log`
            CHANGE COLUMN `credit_amount` `credit_amount` DECIMAL(11,2) NOT NULL COMMENT 'topup' ,
            CHANGE COLUMN `debit_amount` `debit_amount` DECIMAL(11,2) NOT NULL COMMENT 'deduction' ,
            CHANGE COLUMN `currency_rate` `currency_rate` DECIMAL(11,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_country`
            CHANGE COLUMN `co_rate` `co_rate` DECIMAL(11,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `co_offline_rate` `co_offline_rate` DECIMAL(11,2) NOT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_merchant`
            CHANGE COLUMN `earning` `earning` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_order`
            CHANGE COLUMN `currency_rate` `currency_rate` DECIMAL(11,2) NOT NULL ,
            CHANGE COLUMN `product_original_price` `product_original_price` DECIMAL(11,2) NOT NULL COMMENT 'product price before discount:nm_product_pricing->price' ,
            CHANGE COLUMN `product_price` `product_price` DECIMAL(11,2) NOT NULL COMMENT 'product price after discount if any:nm_product_pricing->price||nm_product_pricing->discounted_price' ,
            CHANGE COLUMN `total_product_price` `total_product_price` DECIMAL(11,2) NOT NULL COMMENT 'total of product_price * order_qty' ,
            CHANGE COLUMN `product_shipping_fees` `product_shipping_fees` DECIMAL(11,2) NULL DEFAULT NULL COMMENT 'product shipping fees : nm_product_pricing->shipping_fees' ,
            CHANGE COLUMN `total_product_shipping_fees` `total_product_shipping_fees` DECIMAL(11,2) NULL DEFAULT NULL COMMENT 'total of product_shipping_fees * order_qty' ,
            CHANGE COLUMN `cus_platform_charge_value` `cus_platform_charge_value` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `cus_service_charge_value` `cus_service_charge_value` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `merchant_charge_value` `merchant_charge_value` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ,
            CHANGE COLUMN `order_value` `order_value` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_product_pricing`
            CHANGE COLUMN `price` `price` DECIMAL(11,2) NOT NULL ,
            CHANGE COLUMN `shipping_fees` `shipping_fees` DECIMAL(11,2) NULL DEFAULT '0.00' ,
            CHANGE COLUMN `discounted_price` `discounted_price` DECIMAL(11,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_store`
            CHANGE COLUMN `single_limit` `single_limit` DECIMAL(11,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `monthly_limit` `monthly_limit` DECIMAL(11,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `monthly_trans` `monthly_trans` DECIMAL(11,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `default_price` `default_price` DECIMAL(11,2) NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`nm_withdraw_request`
            CHANGE COLUMN `wd_total_wd_amt` `wd_total_wd_amt` DECIMAL(11,2) NOT NULL ,
            CHANGE COLUMN `wd_submited_wd_amt` `wd_submited_wd_amt` DECIMAL(11,2) NOT NULL ,
            CHANGE COLUMN `wd_admin_comm_amt` `wd_admin_comm_amt` DECIMAL(11,2) NULL DEFAULT NULL ,
            CHANGE COLUMN `wd_balance_after` `wd_balance_after` DECIMAL(11,2) NULL DEFAULT NULL ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`store_service_extra`
            CHANGE COLUMN `service_price` `service_price` DECIMAL(11,2) UNSIGNED NULL DEFAULT '0.00' ;
        ");

        DB::statement("
            ALTER TABLE `zona`.`temp_cart`
            CHANGE COLUMN `purchasing_price` `purchasing_price` DECIMAL(11,2) NOT NULL ,
            CHANGE COLUMN `product_price` `product_price` DECIMAL(11,2) NOT NULL ,
            CHANGE COLUMN `online_platform_charge_value` `online_platform_charge_value` DECIMAL(11,2) NOT NULL;
        ");
    }
}
