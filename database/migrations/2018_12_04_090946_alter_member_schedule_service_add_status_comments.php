<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMemberScheduleServiceAddStatusComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `member_service_schedule` 
        CHANGE COLUMN `status` `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '0=unscheduled;1=member scheduled;2=merchant_scheduled;3=confirmed;4=completed;\n-1=cancelled' ;
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
