<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNmMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            $table->decimal('earning', 10, 2)->after('mer_commission')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            if (Schema::hasColumn('nm_merchant', 'earning')) {
                $table->dropColumn('earning');
            }
        });
    }
}
