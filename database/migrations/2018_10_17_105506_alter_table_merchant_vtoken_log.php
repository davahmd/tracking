<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterTableMerchantVtokenLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        ALTER TABLE merchant_vtoken_log
        CHANGE COLUMN credit_amount credit_amount DECIMAL(10,2) NOT NULL COMMENT 'topup' ,
        CHANGE COLUMN debit_amount debit_amount DECIMAL(10,2) NOT NULL COMMENT 'deduction',
        ADD COLUMN `currency` VARCHAR(10) NULL AFTER `remark`,
        ADD COLUMN `currency_rate` DECIMAL(10,2) NULL AFTER `currency`;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
        ALTER TABLE merchant_vtoken_log
        CHANGE COLUMN credit_amount credit_amount DECIMAL(11,4) NOT NULL COMMENT 'topup' ,
        CHANGE COLUMN debit_amount debit_amount DECIMAL(11,4) NOT NULL COMMENT 'deduction',
        DROP COLUMN `currency_rate`,
        DROP COLUMN `currency`;
        ");
    }
}
