<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParentOrderFigureFormat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `parent_orders` 
        CHANGE COLUMN `order_amount` `order_amount` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
        CHANGE COLUMN `credit_used` `credit_used` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
        CHANGE COLUMN `shipping_amount` `shipping_amount` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
        CHANGE COLUMN `service_amount` `service_amount` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
        CHANGE COLUMN `discounted_amount` `discounted_amount` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ,
        CHANGE COLUMN `charged_amount` `charged_amount` DECIMAL(17,2) NOT NULL DEFAULT '0.00' ;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
