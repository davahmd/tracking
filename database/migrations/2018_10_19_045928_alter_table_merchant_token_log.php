<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMerchantTokenLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_vtoken_log', function (Blueprint $table) {
            $table->decimal('amount')->after('currency_rate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_vtoken_log', function (Blueprint $table) {
            if (Schema::hasColumn('merchant_vtoken_log', 'amount')) {
                $table->dropColumn('amount');
            }
        });
    }
}
