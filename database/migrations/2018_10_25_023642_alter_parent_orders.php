<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParentOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parent_orders', function (Blueprint $table) {
            $table->string('order_type')->after('customer_id')->default('S')->comment = 'S:sales;R:refund';
            $table->float('order_amount')->after('order_type')->default(0);
            $table->float('credit_used')->after('order_amount')->default(0);
            $table->float('shipping_amount')->after('credit_used')->default(0);
            $table->float('service_amount')->after('shipping_amount')->default(0);
            $table->string('discounted_code')->after('service_amount')->nullable();
            $table->float('discounted_amount')->after('discounted_code')->default(0);
            $table->float('charged_amount')->after('discounted_amount')->default(0);
            $table->string('status')->after('charged_amount')->nullable()->comment = 'P:paid;X:pending pg;C:cancel;';
            $table->string('pg_ref')->after('status')->nullable();
        });

        Schema::table('nm_order', function (Blueprint $table) {
            $table->string('service_ids')->after('total_product_shipping_fees_credit')->nullable;
            $table->string('ship_code')->after('service_ids')->nullable;
            $table->string('shipping_note')->after('order_courier_id')->nullable;
            $table->string('ship_service')->after('shipping_note')->nullable;
            $table->string('ship_eta')->after('ship_service')->nullable;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
