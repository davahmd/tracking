<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE `chats` (
            `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            `cus_id` int(10) unsigned NOT NULL,
            `store_id` int(10) unsigned NOT NULL,
            `pro_id` int(10) unsigned NOT NULL,
            `schedule_id` int(10) unsigned NOT NULL,
            `sender_type` char(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'U: customer\nS: store\n\n\n',
            `message` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
            `sent_at` timestamp NULL DEFAULT NULL,
            `received_at` timestamp NULL DEFAULT NULL,
            `cus_read_at` timestamp NULL DEFAULT NULL,
            `store_read_at` timestamp NULL DEFAULT NULL,
            `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
            `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
