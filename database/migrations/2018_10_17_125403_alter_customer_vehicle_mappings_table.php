<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerVehicleMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_vehicle_mappings', function (Blueprint $table) {
            $table->integer('mileage')->after('isdefault')->unsigned()->nullable();
            $table->date('road_tax_expired_date')->after('isdefault')->nullable();
            $table->date('insurance_expired_date')->after('isdefault')->nullable();
            $table->string('insurance')->after('isdefault')->nullable();
            $table->string('name')->after('isdefault')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
