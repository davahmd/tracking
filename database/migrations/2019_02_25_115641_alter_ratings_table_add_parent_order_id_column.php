<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Rating;

class AlterRatingsTableAddParentOrderIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ratings', function (Blueprint $table) {
            $table->integer('parent_order_id')->after('customer_id')->default(0);
            $table->index('parent_order_id', 'parent_order_idx');
        });

        Rating::all()
        ->map(function ($rating)
        {
            $rating->parent_order_id = $rating->order ? $rating->order->parent_order_id : 0;
            $rating->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $table) {
            $table->dropColumn('parent_order_id');
        });
    }
}
