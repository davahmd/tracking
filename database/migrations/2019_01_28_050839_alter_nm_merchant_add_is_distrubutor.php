<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNmMerchantAddIsDistrubutor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            $table->tinyInteger('is_distributor')->default(0)->comment('0-no|1-yes')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            if (Schema::hasColumn('nm_merchant', 'is_distributor')) {
                $table->dropColumn('is_distributor');
            }
        });
    }
}
