<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE `promotions` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
          `merchant_id` int(10) unsigned NOT NULL,
          `stores` text COLLATE utf8_unicode_ci,
          `categories` text COLLATE utf8_unicode_ci,
          `products` text COLLATE utf8_unicode_ci,
          `promo_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'P: promo code\nF: flash sale',
          `promo_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
          `promo_code_count` int(10) unsigned DEFAULT '0',
          `promo_code_limit` int(10) unsigned DEFAULT NULL,
          `promo_discount_max` int(10) unsigned DEFAULT NULL,
          `discount_rate` decimal(5,4) unsigned DEFAULT NULL,
          `discount_value` int(10) unsigned DEFAULT NULL,
          `applied_to` tinyint(1) DEFAULT NULL COMMENT '0:product\\n1:shipping\\n2:both',
          `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `ended_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '-1: cancelled\n0: inactive\n1: active',
          `request` char(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'null: no request\nA: request approval\nC: request cancellaton',
          `create_initiator` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'A: admin\nM: merchant\nS: store user',
          `create_uid` int(10) unsigned NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`),
          UNIQUE KEY `promotion_code_unique` (`promo_code`)
        ) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
