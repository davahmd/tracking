<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNmProductTableForModuleManageProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_product', function (Blueprint $table) {
            $table->integer('brand_id')->after('limit_quantity')->nullable()->index();
            $table->float('weight', 11, 2)->after('brand_id')->nullable();
            $table->float('height', 11, 2)->after('weight')->nullable();
            $table->float('width', 11, 2)->after('height')->nullable();
            $table->float('length', 11, 2)->after('width')->nullable();
            $table->integer('condition')->after('length')->nullable();
            $table->tinyInteger('installation')->after('condition')->nullable();
            $table->tinyInteger('featured')->after('installation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_product', function (Blueprint $table) {
            $table->dropColumn(['brand_id', 'weight', 'height', 'width', 'length', 'condition', 'installation', 'featured']);
        });
    }
}
