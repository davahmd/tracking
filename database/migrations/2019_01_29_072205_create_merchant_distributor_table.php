<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantDistributorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_distributor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_id');
            $table->integer('merchant_id');
            $table->tinyInteger('status')
                ->default(0)
                ->comment('0-Pending Approval, 1-Active, 2-Block');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_distributor');
    }
}
