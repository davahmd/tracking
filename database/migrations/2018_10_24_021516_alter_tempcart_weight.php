<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTempcartWeight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_cart', function (Blueprint $table) {
            if (Schema::hasColumn('temp_cart', 'total_weight')) {
                $table->dropColumn('total_weight');
            }
            $table->bigInteger('weight_per_qty')->after('attributes_name')->nullable();
            $table->string('shipping_note')->after('shipping_fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
