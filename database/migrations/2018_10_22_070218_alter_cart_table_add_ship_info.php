<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCartTableAddShipInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_cart', function (Blueprint $table) {
            $table->integer('total_weight')->after('attributes_name')->nullable();
            $table->float('shipping_fee',20,2)->after('total_weight')->nullable();
            $table->string('ship_code',150)->after('shipping_fee')->nullable();
            $table->string('ship_service',150)->after('ship_code')->nullable();
            $table->string('ship_eta',150)->after('ship_service')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
