<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNmProductAddProTitleIndAndShortDescInd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_product', function (Blueprint $table) {
            $table->string('pro_title_ind', 150)->after('pro_title_en')->nullable();
            $table->string('pro_desc_ind', 500)->after('pro_desc_en')->nullable();
            $table->string('short_desc_ind', 500)->after('short_desc_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_product', function (Blueprint $table) {
            if (Schema::hasColumn('nm_product', 'pro_title_ind')) {
                $table->dropColumn('pro_title_ind');
            }

            if (Schema::hasColumn('nm_product', 'pro_desc_ind')) {
                $table->dropColumn('pro_desc_ind');
            }

            if (Schema::hasColumn('nm_product', 'short_desc_ind')) {
                $table->dropColumn('short_desc_ind');
            }
        });
    }
}
