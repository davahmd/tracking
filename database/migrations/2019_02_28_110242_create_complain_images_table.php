<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplainImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complain_images', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('complain_id');
            $table->string('image')->nullable();
            $table->string('path')->nullable();

            $table->timestamps();

            $table->index('complain_id', 'complain_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complain_images');
    }
}
