<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParentOrdersTableAddPlatformAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parent_orders', function (Blueprint $table) {
            $table->decimal('platform_amount', 17, 2)->default(0)->after('service_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parent_orders', function (Blueprint $table) {
            //
        });
    }
}
