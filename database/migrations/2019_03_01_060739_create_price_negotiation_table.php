<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceNegotiationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('price_negotiations');

        Schema::create('price_negotiations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->nullable();
            $table->integer('product_id');
            $table->integer('pricing_id');
            $table->integer('attribute_id');
            $table->integer('quantity')->nullable();
            $table->integer('customer_id');
            $table->integer('customer_offer_price');
            $table->integer('merchant_id');
            $table->integer('merchant_offer_price')->nullable();
            $table->tinyInteger('status');
            $table->timestamp('expired_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_negotiations');
    }
}
