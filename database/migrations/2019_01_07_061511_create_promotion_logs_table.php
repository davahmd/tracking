<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE `promotion_logs` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `promotion_id` int(10) unsigned NOT NULL,
          `action` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'A: approve\nC: cancel',
          `initiator_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'A: admin\nM: merchant\nS: store user',
          `initiator_uid` int(10) unsigned NOT NULL,
          `approver_uid` int(10) unsigned NOT NULL,
          `reason` text COLLATE utf8_unicode_ci,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
