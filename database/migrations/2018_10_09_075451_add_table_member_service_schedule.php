<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableMemberServiceSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('nm_store', function($table) {
        //     $table->tinyInteger('service')->after('listed')->default(0)->unsigned();
        // });

        // Schema::create('member_service_schedule', function(Blueprint $table)
        // {
        //     $table->increments('id')->index();
        //     $table->integer('cus_id');
        //     $table->integer('order_id');
        //     $table->integer('stor_id');
        //     $table->integer('store_service_extra_id');
        //     $table->string('service_name_current', 255);
        //     $table->dateTime('schedule_datetime')->nullable();
        //     $table->tinyInteger('reschedule_count_member')->default(0)->unsigned();
        //     $table->tinyInteger('reschedule_count_merchant')->default(0)->unsigned();
        //     $table->tinyInteger('status')->default(0);
        //     $table->timestamps();
        // });

        DB::statement("CREATE TABLE `member_service_schedule` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `cus_id` int(10) unsigned NOT NULL,
            `order_id` int(10) unsigned NOT NULL,
            `stor_id` int(10) unsigned DEFAULT NULL,
            `store_service_extra_id` int(10) unsigned DEFAULT NULL,
            `service_name_current` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
            `schedule_datetime` datetime DEFAULT NULL,
            `reschedule_count_member` tinyint(2) unsigned DEFAULT '0',
            `reschedule_count_merchant` tinyint(2) unsigned DEFAULT '0',
            `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=unscheduled;1=member scheduled;2=merchant_scheduled;3=confirmed;\n-1=cancelled',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('nm_store', function($table) {
        //     $table->dropColumn('service');
        // });
        // Schema::dropIfExists('member_service_schedule');
    }
}
