<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePgTransactionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE `pg_transaction` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `order_id` BIGINT(20) NOT NULL,
                `pg_transaction_id` VARCHAR(100) NOT NULL,
                `gross_amount` FLOAT NOT NULL,
                `status_code` INT NOT NULL,
                `status_message` VARCHAR(255) NOT NULL,
                `payment_type` VARCHAR(45) NOT NULL,
                `transaction_time` TIMESTAMP NOT NULL,
                `transaction_status` VARCHAR(45) NOT NULL,
                `fraud_status` VARCHAR(45) NOT NULL,
                `approval_code` VARCHAR(45) NULL,
                `masked_card` VARCHAR(45) NULL,
                `bank` VARCHAR(45) NULL,
                `card_type` VARCHAR(45) NULL,
                `bill_key` VARCHAR(45) NULL COMMENT 'midtrans echannel',
                `biller_code` VARCHAR(45) NULL COMMENT 'midtrans echannel',
                PRIMARY KEY (`id`));"
        );

        DB::statement("ALTER TABLE `pg_transaction` 
        CHANGE COLUMN `status_code` `status_code` INT(11) NOT NULL COMMENT '200-Success\n201-challenge or pending\n202-denied\n400-validation error\n404-requested resource is not found\n406-duplicate order id\n500-internal server error' ;");
        
        DB::statement("ALTER TABLE `parent_orders` 
        ADD COLUMN `payment_status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0=waiting pg notification; 1=captured; 2=challenged; 3=settlement; 4=pending; 5=deny; 6=expired;7=canceled;' AFTER `updated_at`;");

}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
