<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCategoriesAddIndonesianNameAndShortDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('name_ind', 150)->after('name_en')->nullable();
            $table->string('short_desc_en', 255)->after('name_my')->nullable();
            $table->string('short_desc_ind', 255)->after('short_desc_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            if (Schema::hasColumn('categories', 'name_ind')) {
                $table->dropColumn('name_ind');
            }

            if (Schema::hasColumn('categories', 'short_desc_en')) {
                $table->dropColumn('short_desc_en');
            }

            if (Schema::hasColumn('categories', 'short_desc_ind')) {
                $table->dropColumn('short_desc_ind');
            }
        });
    }
}
