<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterNmOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE nm_order
            CHANGE COLUMN remarks remarks TEXT NULL DEFAULT NULL AFTER updated_at,
            CHANGE COLUMN order_pro_color order_pro_color INT(11) NULL AFTER remarks,
            CHANGE COLUMN order_pro_size order_pro_size INT(11) NULL AFTER order_pro_color,
            CHANGE COLUMN order_shipping_add order_shipping_add TEXT NULL AFTER order_pro_size,
            CHANGE COLUMN order_vtokens order_vtokens DECIMAL(11,4) NULL AFTER order_shipping_add,
            CHANGE COLUMN order_vcoins order_vcoins INT(11) NULL DEFAULT NULL AFTER order_vtokens,
            CHANGE COLUMN payer_status payer_status VARCHAR(50) NULL DEFAULT NULL AFTER order_vcoins,
            CHANGE COLUMN order_paytype order_paytype SMALLINT(6) NULL DEFAULT NULL COMMENT '1-paypal' AFTER payer_status,
            CHANGE COLUMN order_amt order_amt DECIMAL(10,2) NULL AFTER order_paytype,
            CHANGE COLUMN order_tax order_tax DECIMAL(10,2) NULL AFTER order_amt,
            CHANGE COLUMN currency_code currency_code VARCHAR(10) NULL DEFAULT NULL AFTER order_tax,
            CHANGE COLUMN token_id token_id VARCHAR(30) NULL DEFAULT NULL AFTER currency_code,
            CHANGE COLUMN payment_ack payment_ack VARCHAR(10) NULL DEFAULT NULL AFTER token_id,
            CHANGE COLUMN payer_email payer_email VARCHAR(50) NULL DEFAULT NULL AFTER payment_ack,
            CHANGE COLUMN payer_id payer_id VARCHAR(50) NULL DEFAULT NULL AFTER payer_email,
            CHANGE COLUMN payer_name payer_name VARCHAR(100) NULL DEFAULT NULL AFTER payer_id,
            CHANGE COLUMN merchant_charge_vtoken merchant_charge_vtoken DECIMAL(11,4) NULL DEFAULT NULL AFTER payer_name;
        ");

        DB::statement("
            ALTER TABLE nm_order
            CHANGE COLUMN order_pricing_id order_pricing_id INT(11) NOT NULL DEFAULT '0' AFTER order_pro_id,
            CHANGE COLUMN merchant_charge_percentage merchant_charge_percentage DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT 'merchant commission' AFTER cus_service_charge_value;
        ");

        DB::statement("ALTER TABLE nm_order ADD COLUMN merchant_charge_value DECIMAL(10,2) NOT NULL DEFAULT 0 AFTER merchant_charge_percentage; ");

        DB::statement("
            ALTER TABLE nm_order
            CHANGE COLUMN order_value order_value DECIMAL(10,2) NOT NULL DEFAULT 0 AFTER merchant_charge_value,
            CHANGE COLUMN product_original_price product_original_price DECIMAL(10,2) NOT NULL COMMENT 'product price before discount:nm_product_pricing->price' ,
            CHANGE COLUMN product_price product_price DECIMAL(10,2) NOT NULL COMMENT 'product price after discount if any:nm_product_pricing->price||nm_product_pricing->discounted_price' ,
            CHANGE COLUMN total_product_price total_product_price DECIMAL(10,2) NOT NULL COMMENT 'total of product_price * order_qty' ,
            CHANGE COLUMN product_shipping_fees_type product_shipping_fees_type TINYINT(4) NULL COMMENT '0 - free;\\n1 - per_product;\\n2 - per_transaction;\\n3 - self_pickup' ,
            CHANGE COLUMN product_shipping_fees product_shipping_fees DECIMAL(10,2) NULL COMMENT 'product shipping fees : nm_product_pricing->shipping_fees' ,
            CHANGE COLUMN total_product_shipping_fees total_product_shipping_fees DECIMAL(10,2) NULL COMMENT 'total of product_shipping_fees * order_qty' ,
            CHANGE COLUMN cus_platform_charge_rate cus_platform_charge_rate DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT 'Admin Fees' ,
            CHANGE COLUMN cus_platform_charge_value cus_platform_charge_value DECIMAL(10,2) NOT NULL DEFAULT 0 ,
            CHANGE COLUMN cus_service_charge_rate cus_service_charge_rate DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT 'GST/handling fees' ,
            CHANGE COLUMN cus_service_charge_value cus_service_charge_value DECIMAL(10,2) NOT NULL DEFAULT 0;
        ");

        DB::statement("
            ALTER TABLE nm_order
            CHANGE COLUMN order_vcredit order_vcredit DECIMAL(11,4) NULL AFTER merchant_charge_vtoken,
            CHANGE COLUMN total_product_shipping_fees_credit total_product_shipping_fees_credit DECIMAL(10,4) NULL DEFAULT '0.0000' AFTER order_vcredit;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
