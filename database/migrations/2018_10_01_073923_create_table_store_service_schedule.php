<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStoreServiceSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('store_service_schedule', function(Blueprint $table)
        // {
        //     $table->increments('id')->index();
        //     $table->integer('pro_id');
        //     $table->integer('stor_id');
        //     $table->tinyInteger('interval_period')->nullable();
        //     $table->time('appoint_start')->nullable();
        //     $table->time('appoint_end')->nullable();
        //     $table->tinyInteger('schedule_skip_count')->default(1);
        //     $table->timestamps();
        // });

        // Schema::create('store_service_extra', function(Blueprint $table)
        // {
        //     $table->increments('id')->index();
        //     $table->integer('pro_id');
        //     $table->integer('store_service_schedule_id');
        //     $table->text('service_name');
        //     $table->decimal('service_price', 10, 2);
        //     $table->timestamps();
        // });

        DB::statement("ALTER TABLE `nm_store` 
        ADD COLUMN `service` TINYINT(4) UNSIGNED NULL DEFAULT '0' AFTER `listed`;
        ");

        DB::statement("CREATE TABLE `store_service_schedule` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `pro_id` int(10) unsigned NOT NULL,
            `stor_id` int(10) unsigned NOT NULL,
            `interval_period` tinyint(2) unsigned DEFAULT NULL,
            `appoint_start` time DEFAULT NULL,
            `appoint_end` time DEFAULT NULL,
            `schedule_skip_count` tinyint(2) unsigned NOT NULL DEFAULT '1',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

        DB::statement("CREATE TABLE `store_service_extra` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `pro_id` int(11) NOT NULL,
            `store_service_schedule_id` int(10) unsigned NOT NULL,
            `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `service_price` decimal(10,2) unsigned DEFAULT '0.00',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('store_service_schedule');
        // Schema::dropIfExists('store_service_extra');
    }
}
