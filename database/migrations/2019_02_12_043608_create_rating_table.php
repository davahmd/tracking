<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('order_id');
            $table->integer('reference_id');
            $table->smallInteger('reference_type')->default(1);
            $table->smallInteger('rating')->default(1);
            $table->string('review')->nullable();
            $table->smallInteger('display')->default(1);
            $table->timestamps();

            $table->index('customer_id', 'customer_idx');
            $table->index('order_id', 'order_idx');
            $table->index('reference_id', 'reference_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
