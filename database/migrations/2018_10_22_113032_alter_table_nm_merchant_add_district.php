<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNmMerchantAddDistrict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            $table->smallInteger('mer_subdistrict')->after('mer_co_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            if (Schema::hasColumn('nm_merchant', 'mer_subdistrict')) {
                $table->dropColumn('mer_subdistrict');
            }
        });
    }
}
