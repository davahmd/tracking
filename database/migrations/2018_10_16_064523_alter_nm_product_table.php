<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNmProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_product', function (Blueprint $table) {
            $table->text('pro_keyword')->after('pro_mdesc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_product', function (Blueprint $table) {
            if (Schema::hasColumn('nm_product', 'pro_keyword')) {
                $table->dropColumn('pro_keyword');
            }
        });
    }
}
