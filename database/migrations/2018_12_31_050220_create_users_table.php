<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 255);
            $table->string('password', 255);
            $table->tinyInteger('activation')->default(0)->comment('0-inactive|1-activate');
            $table->tinyInteger('status')->default(0)->comment('0-blocked|1-unblocked');
            $table->dateTime('last_login_at')->nullable();
            $table->string('last_login_ip', 255)->nullable();
            $table->string('remeber_token', 255)->nullable();
            $table->tinyInteger('failed_login')->nullable();
            $table->tinyInteger('account_locked')->nullable()->default(0)->comment('0-unlock|1-locked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
