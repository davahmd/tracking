<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNmStoreAddDistrict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_store', function (Blueprint $table) {
            $table->smallInteger('stor_subdistrict')->after('stor_city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_store', function (Blueprint $table) {
            if (Schema::hasColumn('nm_store', 'stor_subdistrict')) {
                $table->dropColumn('stor_subdistrict');
            }
        });
    }
}
