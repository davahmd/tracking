<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('store_review');

        Schema::create('store_review', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->default(0);
            $table->integer('cus_id')->default(0);
            $table->text('review')->nullable();
            $table->tinyInteger('rating')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_review');
    }
}
