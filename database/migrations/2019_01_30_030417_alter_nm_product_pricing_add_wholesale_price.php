<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNmProductPricingAddWholesalePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_product_pricing', function (Blueprint $table) {
            $table->decimal('wholesale_price', 17, 2)->nullable()->after('length');
            $table->integer('min_quantity')->nullable()->default(1)->after('wholesale_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_product_pricing', function (Blueprint $table) {
            if (Schema::hasColumn('nm_product_pricing', 'wholesale_price')) {
                $table->dropColumn('wholesale_price');
            }

            if (Schema::hasColumn('nm_product_pricing', 'min_quantity')) {
                $table->dropColumn('min_quantity');
            }
        });
    }
}
