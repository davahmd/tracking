<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplainsTable extends Migration
{
    protected $table = 'complains';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function(Blueprint $table) {
            $table->increments('id');
            $table->morphs('complainant');
            $table->morphs('complainable');
            $table->unsignedInteger('subject_id');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();

            $table->index('subject_id');
            $table->index('complainant_id');
            $table->index('complainant_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
