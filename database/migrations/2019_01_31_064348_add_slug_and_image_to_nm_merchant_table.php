<?php

use App\Models\Merchant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugAndImageToNmMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            $table->string('slug')->nullable()->unique()->after('remember_token');
            $table->string('image')->nullable()->after('bank_gst');
        });

        foreach (Merchant::cursor() as $merchant) {
            $slug = $merchant->mer_fname . ' ' . $merchant->mer_lname;
            $indicator = null;

            while(Merchant::where('slug', $slug)->exists()) {
                $indicator = $indicator + 1;
            }

            $merchant->slug = str_slug($indicator ? $slug . '-' . $indicator : $slug);
            $merchant->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_merchant', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->dropColumn('slug');
        });
    }
}
