<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNmOrderTableAddDiscountedValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_order', function (Blueprint $table) {
            $table->integer('promo_id')->after('order_pricing_id')->nullable();
            $table->integer('flashsale_id')->after('order_pricing_id')->nullable();
            $table->decimal('discounted_product_value', 17, 2)->after('product_price')->default(0);
            $table->decimal('discounted_shipping_fee', 17, 2)->after('product_shipping_fees')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
