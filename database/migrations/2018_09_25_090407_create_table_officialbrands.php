<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOfficialbrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      //Table official_brands
    //   Schema::create('official_brands', function(Blueprint $table)
    //   {
    //       $table->increments('brand_id');
    //       $table->string('brand_name', 150);
    //       $table->text('brand_mkeywords')->nullable();
    //       $table->text('brand_mdesc')->nullable();
    //       $table->text('brand_slug')->nullable();
    //       $table->mediumText('brand_desc')->nullable();
    //       $table->tinyInteger('brand_status');
    //       $table->string('brand_Img', 500);
    //       $table->timestamps();
    //   });

    DB::statement("CREATE TABLE `official_brands` (
        `brand_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `brand_name` varchar(150) NOT NULL,
        `brand_mkeywords` text,
        `brand_mdesc` text,
        `brand_slug` text,
        `brand_desc` mediumtext,
        `brand_status` tinyint(4) NOT NULL COMMENT '1=> Active, 0 => Block, 2=>Incomplete',
        `brand_Img` varchar(500) DEFAULT NULL,
        `brand_logo_count` int(11) NOT NULL,
        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`brand_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('official_brands');
    }
}
