<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplainMessagesTable extends Migration
{
    protected $table = 'complain_messages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('complain_id');
            $table->morphs('responder');
            $table->text('message');
            $table->boolean('is_creator');
            $table->timestamps();

            $table->index('responder_id');
            $table->index('responder_type');
            $table->index('is_creator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
