<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStoredProceduresForTotalProductDiscountedAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(" 
        DROP procedure IF EXISTS `cart_checkout`;

        DELIMITER $$
        CREATE PROCEDURE `cart_checkout`(IN cusid INT, IN country INT, IN trans_id VARCHAR(8), IN sname VARCHAR(225) CHARSET utf8, IN saddress1 VARCHAR(225) CHARSET utf8, IN saddress2 VARCHAR(225) CHARSET utf8, IN scity VARCHAR(225), IN scountry INT, IN spostal VARCHAR(10), IN stel VARCHAR(20), IN sstate INT, IN pay_method INT, IN parentOrderId INT, IN sdistrict INT, IN wholesale INT)
        BEGIN
            DECLARE tc_finished INT DEFAULT 0 ;
            DECLARE cartID INT;
            DECLARE customerID INT;
            DECLARE productID INT;
            DECLARE pricingID INT;
            DECLARE qty INT;
            DECLARE proprice DECIMAL(17,2);
            DECLARE proprice_ori DECIMAL(17,2);
            DECLARE order_id INT;
            DECLARE propurchase INT;
            DECLARE proname NVARCHAR(255);
            DECLARE commission DECIMAL(17,2);
            DECLARE charged DECIMAL(17,2);
            DECLARE p_charge DECIMAL(17,2);
            DECLARE pcharge_amt DECIMAL(17,2);
            DECLARE s_charge DECIMAL(17,2);
            DECLARE scharge_amt DECIMAL(17,2);
            DECLARE pro_price DECIMAL(17,2);
            DECLARE pro_dprice DECIMAL(17,2);
            DECLARE pro_dfrom DATETIME;
            DECLARE pro_dto DATETIME;
            DECLARE countryID INT;
            DECLARE currency_code VARCHAR(10);
            DECLARE currency_symbol VARCHAR(10);
            DECLARE currency_rate DECIMAL(17,2);
            DECLARE tot_proprice DECIMAL(17,2);
            DECLARE today DATETIME;
            DECLARE order_value DECIMAL(17,2);
            DECLARE total_order_value DECIMAL(17,2);
            DECLARE json_attributes_name TEXT;
            DECLARE json_attributes_id TEXT;
            DECLARE price_quantity INT;
            DECLARE pro_quantity INT;
            DECLARE ship_fees_type INT;
            DECLARE ship_fees DECIMAL(17,2);
            DECLARE ship_fees_total DECIMAL(17,2);
            DECLARE product_type INT;
            DECLARE mer_id INT;
            DECLARE country_name NVARCHAR(50);
            DECLARE state_name NVARCHAR(50);
            DECLARE order_status INT DEFAULT 1;
            DECLARE order_type INT DEFAULT 1;
            DECLARE coupon_val VARCHAR(100);
            DECLARE isShippingExist INT DEFAULT 0;
            DECLARE sku_code VARCHAR(255);
            -- for zona
            DECLARE substrict_name NVARCHAR(50);
            DECLARE p_ship_id INT;
            DECLARE courier_id NVARCHAR(50);
            DECLARE shipping_fee DECIMAL(17,2);
            DECLARE ship_code NVARCHAR(50);
            DECLARE shipping_note NVARCHAR(50);
            DECLARE ship_service NVARCHAR(50);
            DECLARE ship_eta NVARCHAR(255);
            DECLARE service_ids NVARCHAR(50);
            DECLARE parent_order_amount DECIMAL(17,2) DEFAULT 0;
            DECLARE parent_shipping_amount DECIMAL(17,2) DEFAULT 0;
            DECLARE w_price DECIMAL(17,2) DEFAULT 0;
            DECLARE c_wholesale INT;
            
            -- for promotion
            DECLARE flashsaleID INT;
            DECLARE promoID INT;
            
            DECLARE fs_discount_rate DECIMAL(5,4);
            DECLARE fs_discount_value INT;
            DECLARE fs_procount INT;
            DECLARE fs_prolimit INT;
            
            DECLARE p_discount_rate DECIMAL(5,4);
            DECLARE p_discount_value INT;
            DECLARE p_applied_to INT;
            DECLARE p_count INT;
            DECLARE p_limit INT;
            
            DECLARE product_dvalue DECIMAL(17,2);
            DECLARE ship_dfee DECIMAL(17,2);
            DECLARE tot_proprice_ori DECIMAL(17,2);
            DECLARE tot_shipfee_ori DECIMAL(17,2);
            DECLARE tot_product_dvalue DECIMAL(17,2);
            
            DECLARE parent_discounted_order_amount DECIMAL(17,2) DEFAULT 0;
            DECLARE parent_discounted_shipping_amount DECIMAL(17,2) DEFAULT 0;
            -- end for promotion
            
            -- end for zona
        
            DECLARE curs CURSOR FOR SELECT tc.id, tc.cus_id, tc.product_id, tc.quantity, tc.pricing_id, tc.flashsale_id, tc.promo_id, nmt.mer_platform_charge, nmt.mer_service_charge,
            (SELECT co_name FROM nm_country WHERE co_id = scountry) as country,
            (SELECT name FROM nm_state WHERE id = sstate) as state,
            -- for zona
            (SELECT name FROM subdistrict WHERE id = sdistrict) as subdistrict,
            tc.shipping_fee,
            tc.ship_code,
            tc.shipping_note,
            tc.ship_service,
            tc.ship_eta,
            tc.service_ids,
            tc.ship_id,
            tc.wholesale
            -- end for zona
            FROM temp_cart tc
            LEFT JOIN nm_product np ON np.pro_id = tc.product_id
            LEFT JOIN nm_product_pricing npp ON npp.id = tc.pricing_id
            LEFT JOIN nm_merchant nmt ON nmt.mer_id = np.pro_mr_id
            LEFT JOIN nm_store nst ON nst.stor_id = np.pro_sh_id
            WHERE tc.cus_id = cusid AND npp.country_id = country AND npp.status = 1 AND nmt.mer_staus = 1 AND nst.stor_status = 1 AND np.pro_status = 1 AND tc.wholesale = wholesale;
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET tc_finished = 1;
        
            DROP TEMPORARY TABLE IF EXISTS tblResults;
            CREATE TEMPORARY TABLE IF NOT EXISTS tblResults  (
                product_type INT,
                product_name NVARCHAR(255),
                quantity INT,
                order_vc DECIMAL(17,2),
                payment_method INT,
                order_amt DECIMAL(17,2),
                currency_symbol VARCHAR(10),
                json_attributes TEXT,
                serial_number TEXT,
                ship_name NVARCHAR(100),
                ship_address1 NVARCHAR(255),
                ship_address2 NVARCHAR(255),
                ship_city NVARCHAR(50),
                ship_country NVARCHAR(50),
                ship_state NVARCHAR(50),
                ship_postalcode VARCHAR(10),
                ship_phone VARCHAR(20)
            );
        
            OPEN curs;
        
            get_cart: LOOP
            FETCH curs into cartID, customerID, productID, qty, pricingID, flashsaleID, promoID, p_charge, s_charge, country_name, state_name,
            -- for zona
            substrict_name, shipping_fee, ship_code, shipping_note, ship_service, ship_eta,service_ids,p_ship_id, c_wholesale;
            -- end for zona
                IF tc_finished = 1 THEN LEAVE get_cart; END IF;
        
        --      -- get product pricing details
                SELECT price, discounted_price, discounted_from, discounted_to, country_id, quantity, attributes, attributes_name, shipping_fees_type, shipping_fees, CASE WHEN coupon_value IS NULL THEN '0' ELSE coupon_value END, sku, wholesale_price
                INTO pro_price, pro_dprice, pro_dfrom, pro_dto, countryID, price_quantity, json_attributes_id, json_attributes_name, ship_fees_type, ship_fees, coupon_val, sku_code, w_price
                FROM nm_product_pricing
                WHERE id = pricingID AND country_id = country;
        
                -- get country rate
                SELECT co_cursymbol, co_curcode, co_rate INTO currency_symbol, currency_code, currency_rate FROM nm_country WHERE co_id = countryID;
        
                SET today = NOW();
        
                -- product original price
                SET proprice_ori = pro_price;
                
                -- set product_dvalue & ship_dfee as 0
                SET product_dvalue = 0;
                SET ship_dfee = 0;
                
                IF wholesale = 0 THEN
                
                    IF flashsaleID IS NOT NULL THEN
                    
                        SELECT discount_rate, discount_value INTO fs_discount_rate, fs_discount_value
                        FROM promotions
                        WHERE id = flashsaleID AND promo_type = 'F' AND started_at <= today AND ended_at >= today AND status = 1;
                        
                        SELECT `count`, `limit` INTO fs_procount, fs_prolimit
                        FROM promotion_products
                        WHERE promotion_id = flashsaleID and product_id = productID;
                        
                        IF fs_procount < fs_prolimit THEN
                            IF fs_discount_rate IS NOT NULL THEN
                                SET product_dvalue = pro_price * fs_discount_rate;
                            ELSEIF fs_discount_value IS NOT NULL THEN
                                SET product_dvalue = fs_discount_value;
                            END IF;
                        END IF;
                            
                    END IF;
                    
                    IF product_dvalue > 0 THEN
                        SET proprice = pro_price - product_dvalue;
                    ELSE
                        SET proprice = pro_price;
                    END IF;
                    
                    IF promoID IS NOT NULL THEN
                    
                        SELECT discount_rate, discount_value, promo_code_count, promo_code_limit, applied_to INTO p_discount_rate, p_discount_value, p_count, p_limit, p_applied_to
                        FROM promotions
                        WHERE id = promoID AND promo_type = 'P' AND started_at <= today AND ended_at >= today AND status = 1;
                    
                        -- promo applied to product
                        IF p_applied_to=0 THEN
                            IF p_limit IS NULL THEN
        
                                IF p_discount_rate IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue + (proprice * p_discount_rate);
                                ELSEIF p_discount_value IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue +  p_discount_value;
                                END IF;
                            
                            ELSEIF p_count < p_limit THEN
                            
                                IF p_discount_rate IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue + (proprice * p_discount_rate);
                                ELSEIF p_discount_value IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue +  p_discount_value;
                                END IF;
                            
                            END IF;
                            
                        -- promo applied to shipping
                        ELSEIF p_applied_to=1 THEN
                            IF p_limit IS NULL THEN
                            
                                IF p_discount_rate IS NOT NULL THEN
                                    SET ship_dfee = shipping_fee * p_discount_rate;
                                ELSEIF p_discount_value IS NOT NULL THEN
                                    SET ship_dfee = p_discount_value;
                                END IF;
                            
                            ELSEIF p_count < p_limit THEN
                            
                                IF p_discount_rate IS NOT NULL THEN
                                    SET ship_dfee = shipping_fee * p_discount_rate;
                                ELSEIF p_discount_value IS NOT NULL THEN
                                    SET ship_dfee = p_discount_value;
                                END IF;
                            
                            END IF;
                         
                        -- promo applied to both product & shipping
                        ELSE
                            
                            IF p_limit IS NULL THEN
                            
                                IF p_discount_rate IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue + (proprice * p_discount_rate);
                                    SET ship_dfee = shipping_fee * p_discount_rate;
                                ELSEIF p_discount_value IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue +  p_discount_value;
                                    SET ship_dfee = p_discount_value;
                                END IF;
                            
                            ELSEIF p_count < p_limit THEN
                            
                                IF p_discount_rate IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue + (proprice * p_discount_rate);
                                    SET ship_dfee = shipping_fee * p_discount_rate;
                                ELSEIF p_discount_value IS NOT NULL THEN
                                    SET product_dvalue = product_dvalue +  p_discount_value;
                                    SET ship_dfee = p_discount_value;
                                END IF;
                            
                            END IF;
                            
                        END IF;
                    
                    END IF;
                    
                    IF product_dvalue > 0 THEN
                        SET proprice = proprice_ori - product_dvalue;
                    ELSE
                        SET proprice = proprice_ori;
                    END IF;
                
                ELSE
                
                    SET proprice = w_price;
                
                END IF;
        
                
                -- get total product price
                SET tot_proprice = proprice * qty;
                SET tot_proprice_ori = proprice_ori * qty;

                -- get total discounted amount
                SET tot_product_dvalue = product_dvalue * qty;
        
                -- for zona
                -- sum up total order amount
                SET parent_order_amount = parent_order_amount + tot_proprice_ori;
                SET parent_discounted_order_amount = parent_discounted_order_amount + tot_product_dvalue;
                -- end for zona
        
                -- get order value with platform charge
                SET order_value = tot_proprice;
        
                -- get shipping fees
                -- IF (ship_fees_type = 1) THEN
                --    SET ship_fees_total = ROUND(( ship_fees * qty ), 2);
                -- ELSEIF (ship_fees_type = 2) THEN
                --    SET ship_fees_total = ROUND(ship_fees, 2);
                -- ELSE
                --    SET ship_fees_total = 0.00;
                -- END IF;
        
                -- for zona
                -- get shipping fees
                IF shipping_fee > 0 THEN
                    IF ship_dfee > 0 THEN
                        IF promoID IS NOT NULL THEN
                            SET ship_fees_total = shipping_fee - ship_dfee;
                            SET tot_shipfee_ori = shipping_fee;
                        ELSE
                            SET ship_fees_total = shipping_fee;
                            SET tot_shipfee_ori = shipping_fee;
                        END IF;
                    ELSE
                        SET ship_fees_total = shipping_fee;
                        SET tot_shipfee_ori = shipping_fee;
                    END IF;
                ELSE
                    SET ship_fees_total = 0;
                    SET tot_shipfee_ori = 0;
                END IF;
        
                SET parent_shipping_amount = parent_shipping_amount + tot_shipfee_ori;
                SET parent_discounted_shipping_amount = parent_discounted_shipping_amount + ship_dfee;
        
                -- get courier id
                IF ship_code IS NOT NULL THEN
        
                    SELECT id INTO courier_id FROM nm_courier WHERE BINARY `code` = BINARY ship_code;
        
                END IF;
                -- end for zona
        
                -- get platform charge
                SET pcharge_amt = ROUND(( order_value * (p_charge / 100) ), 2);
        
                -- get service charge
                SET scharge_amt = ROUND(( (order_value + pcharge_amt) * (s_charge / 100)), 2);
        
                -- get order total value
                SET total_order_value = ROUND(( order_value + pcharge_amt + scharge_amt + ship_fees_total ), 2);
        
                -- get product details
                SELECT pro_no_of_purchase, pro_title_en, pro_qty, pro_type, pro_mr_id
                INTO propurchase, proname, pro_quantity, product_type, mer_id
                FROM nm_product nmp WHERE nmp.pro_id = productID;
        
                -- get commission and its value
                SELECT mer_commission INTO commission FROM nm_product nmp LEFT JOIN nm_merchant nmm ON nmp.pro_mr_id = nmm.mer_id WHERE nmp.pro_id = productID;
                SET charged = ROUND((commission * (order_value / 100)), 2);
                -- SELECT order_value ,pcharge_amt,scharge_amt , ship_fees_total, ROUND(( order_value + pcharge_amt + scharge_amt + ship_fees_total ), 2);
                SET order_status = 0;
                SET order_type = 1;
        
                -- IF(product_type = 2) THEN
                --  SET order_status = 2;
                --  SET order_type = 3;
                -- ELSEIF(product_type = 3) THEN`
                --  SET order_status = 2;
                --  SET order_type = 4;
                -- ELSEIF(product_type = 4) THEN
                --  SET order_status = 2;
                --  SET order_type = 5;
                -- END IF;
        
                -- save order detail
                INSERT INTO nm_order
                (order_cus_id, order_pro_id, order_type, transaction_id, order_qty, order_date, order_status, created_at, updated_at, merchant_charge_percentage, merchant_charge_value, payment_method, currency, currency_rate, product_original_price, product_price, discounted_product_value, total_product_price, cus_platform_charge_rate, cus_platform_charge_value, cus_service_charge_rate, cus_service_charge_value, order_value, order_attributes, order_attributes_id, order_pricing_id, product_shipping_fees_type, product_shipping_fees, discounted_shipping_fee, total_product_shipping_fees, parent_order_id, sku
                -- for zona
                ,order_courier_id,ship_code,shipping_note,ship_service,ship_eta,service_ids, flashsale_id, promo_id, wholesale
                -- end for zona
                )
                VALUES
                (customerID, productID, order_type, trans_id, qty, NOW(), order_status, NOW(), NOW(), commission, charged, pay_method, currency_code, currency_rate, proprice_ori, proprice, product_dvalue, tot_proprice, p_charge, pcharge_amt, s_charge, scharge_amt, total_order_value, json_attributes_name, json_attributes_id, pricingID, ship_fees_type, ship_fees, ship_dfee, ship_fees_total, parentOrderId, sku_code
                -- for zona
                ,courier_id,ship_code,shipping_note,ship_service,ship_eta,service_ids, flashsaleID, promoID, c_wholesale
                -- end for zona
                );
                SET order_id = LAST_INSERT_ID();
        
                -- insert into generated codes table for product coupon/ticket type
        
                -- SET @generated_serial = null;
                -- IF(product_type = 2 or product_type = 3) THEN
        
                --  SET @count = qty;
                --  SET @generated_serial = '';
        
                --  WHILE (@count > 0) DO
                --      SET @found_code = 0;
                --      SET @allowedChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        
                --      WHILE (@found_code = 0) DO
                --          SET @serial_number = '';
                --          SET @i = 0;
        
                --          WHILE (@i < 16) DO
                --              SET @serial_number = CONCAT(@serial_number, substring(@allowedChars, FLOOR(RAND() * LENGTH(@allowedChars) + 1), 1));
                --              SET @i = @i + 1;
                --          END WHILE;
        
                --          IF(product_type = 3) THEN
                --              SET @serial_number = CONCAT(productID, '_', @serial_number);
                --          END IF;
        
                --          IF((SELECT count(*) FROM generated_codes WHERE serial_number = @serial_number) = 0) THEN
                --              SET @found_code = 1;
                --          END IF;
        
                --      END WHILE;
        
                --      INSERT INTO generated_codes (type, order_id, customer_id, merchant_id, serial_number, value, status) VALUES (product_type, order_id, customerID, mer_id, @serial_number, coupon_val, 1);
                --      SET @count := @count - 1;
        
                --      SET @generated_serial = CONCAT(@generated_serial, ',', @serial_number);
        
                --  END WHILE;
        
                -- ELSEIF(product_type = 4) THEN
        
                --  SET @count = qty;
                --  SET @generated_serial = '';
        
                --  WHILE (@count > 0) DO
                --      SET @serial_number = '';
        
                --      SELECT id, serial_number INTO @code_id, @serial_number FROM generated_codes WHERE status = 0 and merchant_id = mer_id and product_id = productID limit 1;
        
                --      SET @generated_serial = CONCAT(@generated_serial, ',', @serial_number);
        
                --      UPDATE generated_codes SET order_id = order_id, customer_id = customerID, status = 1 WHERE id = @code_id;
        
                --      SET @count := @count - 1;
                --  END WHILE;
        
                -- END IF;
        
                -- SELECT count(*) FROM nm_shipping WHERE parent_order_id = parentOrderId INTO isShippingExist;
        
                IF p_ship_id IS NOT NULL THEN
                    -- save shipping detail
                    INSERT INTO nm_shipping
                        (ship_name, ship_address1, ship_address2, ship_city_name, ship_country, ship_state_id, ship_postalcode, ship_phone, ship_order_id, ship_cus_id, created_at, updated_at, parent_order_id, ship_subdistrict_id)
                        SELECT ship_name, ship_address1, ship_address2, ship_city_name,  ship_country, ship_state_id, ship_postalcode, ship_phone, order_id, ship_cus_id, NOW(), NOW(), parent_order_id, ship_subdistrict_id
                        FROM nm_shipping WHERE ship_id = p_ship_id;
                        -- VALUES (sname, saddress1, saddress2, scity, scountry, sstate, spostal, stel, order_id, customerID, NOW(), NOW(), parentOrderId, sdistrict);
                END IF;
        
                -- update product : increase no of purchase
                SET propurchase := propurchase + qty;
                SET pro_quantity := pro_quantity - qty;
                UPDATE nm_product SET pro_no_of_purchase = propurchase, pro_qty = pro_quantity WHERE pro_id = productID;
                
                -- update flashsale: increase count of flashsale product
                IF flashsaleID IS NOT NULL THEN
                    SET fs_procount := fs_procount + qty;
                    UPDATE promotion_products SET `count` = fs_procount WHERE promotion_id = flashsaleID AND product_id = productID; 
                END IF;
                
                -- update promo: increase count of promo
                IF promoID IS NOT NULL THEN
                    SET p_count := p_count + 1;
                    UPDATE promotions SET promo_code_count = p_count WHERE id = promoID;
                END IF;
        
                -- update pricing quantity
                SET price_quantity := GREATEST(0, price_quantity- qty);
        
                IF json_attributes_id IS NULL THEN
                    UPDATE nm_product_pricing SET quantity = price_quantity WHERE pro_id = productID;
                ELSE
                    UPDATE nm_product_pricing SET quantity = price_quantity WHERE attributes = json_attributes_id AND pro_id = productID;
                END IF;
        
                INSERT INTO nm_product_quantity_log (pro_id, attributes, debit, credit, current_quantity, remarks) VALUES (productID, json_attributes_name, qty, 0, price_quantity, concat('Customer Purchase, Transaction ID : ', trans_id));
        
                -- clear temp_cart
                DELETE FROM temp_cart WHERE id = cartID;
        
                INSERT INTO tblResults VALUES (product_type, proname, qty, CAST(total_order_value AS DECIMAL(17,2)), pay_method, order_value, currency_symbol, json_attributes_name, @generated_serial, sname, saddress1, saddress2, scity, country_name, state_name, spostal, stel);
        
            END LOOP get_cart;
            CLOSE curs;
        
            -- for zona
            UPDATE parent_orders SET order_amount = parent_order_amount, discounted_order_amount = parent_discounted_order_amount, shipping_amount = parent_shipping_amount, discounted_shipping_amount = parent_discounted_shipping_amount, charged_amount = (parent_order_amount + parent_shipping_amount), status = 'P' WHERE id=parentOrderId;
            -- end for zona
        
            SELECT * FROM tblResults;
        END$$
        DELIMITER ;


        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}