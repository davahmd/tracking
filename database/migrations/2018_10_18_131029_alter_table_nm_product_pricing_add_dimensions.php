<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNmProductPricingAddDimensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_product_pricing', function (Blueprint $table) {
            $table->decimal('weight', 11, 2)->after('delivery_days')->nullable();
            $table->decimal('height', 11, 2)->after('weight')->nullable();
            $table->decimal('width', 11, 2)->after('height')->nullable();
            $table->decimal('length', 11, 2)->after('width')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_product_pricing', function (Blueprint $table) {
            if (Schema::hasColumn('nm_product_pricing', 'weight')) {
                $table->dropColumn('weight');
            }

            if (Schema::hasColumn('nm_product_pricing', 'height')) {
                $table->dropColumn('height');
            }

            if (Schema::hasColumn('nm_product_pricing', 'width')) {
                $table->dropColumn('width');
            }

            if (Schema::hasColumn('nm_product_pricing', 'length')) {
                $table->dropColumn('length');
            }
        });
    }
}
