<?php

use Illuminate\Database\Seeder;

class AdminComplainPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission')->insert([
        	'permission_group' => 'Complain Title - Settings',
        	'display_sorting' => 94.100,
        	'permission_name' => 'settingcomplaintitlelist',
        	'display_name' => 'View Complain Title',
        	'description' => 'Allow admin user to view complain list & detail',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
            'permission_group' => 'Complain Title - Settings',
            'display_sorting' => 94.200,
            'permission_name' => 'settingcomplaintitlecreate',
            'display_name' => 'Create Complain Title',
            'description' => 'Allow admin user to create complain title',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
            'permission_group' => 'Complain Title - Settings',
            'display_sorting' => 94.300,
            'permission_name' => 'settingcomplaintitleedit',
            'display_name' => 'Edit Complain Title',
            'description' => 'Allow admin user to edit complain title',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
            'permission_group' => 'Complain Title - Settings',
            'display_sorting' => 94.400,
            'permission_name' => 'settingcomplaintitledelete',
            'display_name' => 'Delete Complain Title',
            'description' => 'Allow admin user to delete complain title',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
