<?php

use Illuminate\Database\Seeder;

class AdminVehiclePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission')->insert([
        	'permission_group' => 'Vehicles - Settings',
        	'display_sorting' => 92.100,
        	'permission_name' => 'settingvehiclelist',
        	'display_name' => 'View Vehicles',
        	'description' => 'Allow admin user to view vehicles list & detail',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);

		DB::table('permission')->insert([
        	'permission_group' => 'Vehicles - Settings',
        	'display_sorting' => 92.200,
        	'permission_name' => 'settingvehiclecreate',
        	'display_name' => 'Create New Vehicles',
        	'description' => 'Allow admin user to create new vehicles',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
        	'permission_group' => 'Vehicles - Settings',
        	'display_sorting' => 92.300,
        	'permission_name' => 'settingvehicleedit',
        	'display_name' => 'Edit Vehicles',
        	'description' => 'Allow admin user to edit existing vehicles',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);        

        DB::table('permission')->insert([
        	'permission_group' => 'Vehicles - Settings',
        	'display_sorting' => 92.400,
        	'permission_name' => 'settingvehicledelete',
        	'display_name' => 'Delete Vehicles',
        	'description' => 'Allow admin user to delete vehicles',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
