<?php

use Illuminate\Database\Seeder;

use App\Models\Courier;

class CourierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Courier::truncate();
        $json = File::get('database/data/courier_services.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Courier::create([
                'code' => $obj->courier,
                'name' => $obj->courier_name,
                'logo' => $obj->asset,
                'country_id' => 1,
                'status'=>1
            ]);
        }
    }
}
