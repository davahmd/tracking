<?php

use Illuminate\Database\Seeder;

use App\Models\Subdistrict;

class SubdistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subdistrict::truncate();
        $json = File::get('database/data/subdistricts.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Subdistrict::create([
                'state_id' => (int) $obj->state_id,
                'city_id' => (int) $obj->city_id,
                'id' => (int) $obj->subdistrict_id,
                'name' => $obj->subdistrict_name,
                'status' => 1 // 1:Active
            ]);
        }
    }
}
