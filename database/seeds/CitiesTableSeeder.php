<?php

use Illuminate\Database\Seeder;

use App\Models\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::truncate();
        $json = File::get('database/data/cities.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            City::create([
                'id' => (int) $obj->city_id,
                'name' => $obj->city_name,
                'state_id' => (int) $obj->state_id,
                'postal_code' => (int) $obj->postal_code,
                'type' => $obj->type,
                'status' => 1 // 1:Active
            ]);
        }
    }
}
