<?php

use Illuminate\Database\Seeder;

class AdminPromotionPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission')->insert([
        	'permission_group' => 'Promotions',
        	'display_sorting' => 93.100,
        	'permission_name' => 'promotionlist',
        	'display_name' => 'View Promotion',
        	'description' => 'Allow admin user to view promotion list & detail',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);

		DB::table('permission')->insert([
        	'permission_group' => 'Promotions',
        	'display_sorting' => 93.200,
        	'permission_name' => 'promotioncreate',
        	'display_name' => 'Create Promotion',
        	'description' => 'Allow admin user to create promotion',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
        	'permission_group' => 'Promotions',
        	'display_sorting' => 93.300,
        	'permission_name' => 'promotionedit',
        	'display_name' => 'Edit Promotion',
        	'description' => 'Allow admin user to edit promotion',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]); 

        DB::table('permission')->insert([
            'permission_group' => 'Promotions',
            'display_sorting' => 93.400,
            'permission_name' => 'promotionsetstatus',
            'display_name' => 'Set Promotion Status',
            'description' => 'Allow admin user to set promotion status',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
