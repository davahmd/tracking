<?php

use Illuminate\Database\Seeder;

class UpdateAdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // customer
        DB::table('permission')->where('display_sorting', '46.000')->delete();
        DB::table('permission')->where('display_sorting', '47.000')->delete();
        DB::table('permission')->where('display_sorting', '48.000')->delete();

        // change transaction online text
        DB::table('permission')->where('permission_group', 'Transactions Online And Coupon Order')->update(['permission_group' => 'Transactions Online']);

        DB::table('permission')->where('display_sorting', '55.000')->update(['display_name' => 'View Transaction Online', 'description' => 'Allow admin user to view transaction online']);

        DB::table('permission')->where('display_sorting', '56.000')->update(['display_name' => 'Export Transaction Online', 'description' => 'Allow admin user to export transaction online']);

        DB::table('permission')->where('display_sorting', '59.000')->update(['display_name' => 'Cancel Transaction Online', 'description' => 'Allow admin user to cancel transaction online order']);

        DB::table('permission')->where('display_sorting', '60.000')->update(['display_name' => 'Refund Transaction Online', 'description' => 'Allow admin user to make transaction online order refund']);

        // remove coupon
        DB::table('permission')->where('display_sorting', '60.120')->delete();
        DB::table('permission')->where('display_sorting', '60.200')->delete();

        // transactions offline
        DB::table('permission')->where('permission_name', 'transactionofflineorderslist')->delete();
        DB::table('permission')->where('display_sorting', '61.100')->delete();
        DB::table('permission')->where('display_sorting', '61.200')->delete();

        // merchants
        DB::table('permission')->where('display_sorting', '71.800')->delete();
        DB::table('permission')->where('display_sorting', '71.810')->delete();
        DB::table('permission')->where('display_sorting', '71.801')->delete();
        DB::table('permission')->where('display_sorting', '71.901')->delete();
        DB::table('permission')->where('display_sorting', '71.902')->delete();

        // report
        DB::table('permission')->where('permission_group', 'Report')->delete();
    }
}
