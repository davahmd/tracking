<?php

use Illuminate\Database\Seeder;

use App\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::truncate();
        $json = File::get('database/data/states.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            State::create([
                'id' => (int) $obj->id,
                'name' => $obj->name,
                'status' => 1, // 1:Active
                'country_id' => 1 // 1:Indonesia
            ]);
        }
    }
}
