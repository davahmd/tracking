<?php

use Illuminate\Database\Seeder;

class NewAdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission')->insert([
            'permission_group' => 'Official Brands - Settings',
            'display_sorting' => 34.100,
            'permission_name' => 'settingofficialbrandslist',
            'display_name' => 'View Official Brands',
            'description' => 'Allow admin user to view Official Brands',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
            'permission_group' => 'Official Brands - Settings',
            'display_sorting' => 34.200,
            'permission_name' => 'settingofficialbrandscreate',
            'display_name' => 'Create Official Brands',
            'description' => 'Allow admin user to create Official Brands',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
            'permission_group' => 'Official Brands - Settings',
            'display_sorting' => 34.300,
            'permission_name' => 'settingofficialbrandsedit',
            'display_name' => 'Edit Official Brands',
            'description' => 'Allow admin user to edit Official Brands',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('permission')->insert([
            'permission_group' => 'Official Brands - Settings',
            'display_sorting' => 34.400,
            'permission_name' => 'settingofficialbrandssetstatus',
            'display_name' => 'Set Official Brands Status',
            'description' => 'Allow admin user to set Official Brands Status',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
