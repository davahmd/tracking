@extends('admin.layouts.master')

@section('title', trans('localize.manage_retailers'))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@lang('localize.manage_retailers')</h2>
            <ol class="breadcrumb">
                <li>
                    @lang('localize.retailers')
                </li>
                <li>
                    {{ trans('localize.manage') }}
                </li>
                <li>
                    <strong>{{ $merchant->mer_id }} - {{ $merchant->merchantName() }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">

        @include('admin.common.notifications')

        <div class="row">
            <div class="col-lg-12">

                <div class="ibox">
                    <div class="ibox-content">
                        <form action="{{ url()->current() }}" method="GET">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="id" class="form-control" placeholder="{{ trans('localize.id') }}" value="{{ request('id') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="{{ trans('localize.name') }}" value="{{ request('name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control" name="status" id="status">
                                            <option value="" selected>{{ trans('localize.status') }} {{ trans('localize.all') }}</option>
                                            <option value="0" {{ request('status') === '0' ? 'selected' : '' }}>{{ trans('localize.pending') }}</option>
                                            <option value="1" {{ request('status') === '1' ? 'selected' : '' }}>{{ trans('localize.approved') }}</option>
                                            <option value="2" {{ request('status') === '2' ? 'selected' : '' }}>{{ trans('localize.block') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button class="btn btn-block btn-outline btn-primary">{{ trans('localize.filter') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="ibox">
                    <div class="ibox-content">
                        <div class="ibox-tools">
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">@lang('localize.id')</th>
                                    <th class="text-center">@lang('localize.Merchant')</th>
                                    <th class="text-center">@lang('localize.email')</th>
                                    <th class="text-center">@lang('localize.date')</th>
                                    <th class="text-center">@lang('localize.status')</th>
                                    <th class="text-center">@lang('localize.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($retailers as $merchant)
                                    <tr class="text-center">
                                        <td>{{ $merchant->mer_id }}</td>
                                        <td width="50%">{{ $merchant->merchantName() }}</td>
                                        <td>{{ $merchant->user->email }}</td>
                                        <td>{{ \Helper::UTCtoTZ($merchant->pivot->created_at) }}</td>
                                        <td>
                                            @if($merchant->pivot->status == 0)
                                                <label class="text-navy">@lang('localize.pending')</label>
                                            @elseif($merchant->pivot->status == 1)
                                                <label class="text-success">@lang('localize.Approved')</label>
                                            @elseif($merchant->pivot->status == 2)
                                                <label class="text-warning">@lang('localize.block')</label>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-white btn-sm btn-block" href="{{ url('admin/merchant/view', [$merchant->mer_id]) }}" target="_blank">
                                                <span><i class="fa fa-file-text-o"></i> @lang('localize.view')</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                                @include('layouts.partials.table-pagination', ['listings' => $retailers])

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')

@endsection

@section('script')
    <script>

    </script>
@endsection
