@extends('admin.layouts.master')

@section('title', trans('localize.news.edit_news'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.news.edit_news')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/news/manage">{{trans('admin.nav.news')}}</a>
            </li>
            <li>
                {{trans('localize.edit')}}
            </li>
            <li class="active">
                <strong>{{$news->id}} - {{$news->title_localize}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    @include('admin.common.notifications')

    <div class="row">
        <div class="tabs-container">

            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="panel-body">
                        <div class="col-lg-12 nopadding">
                            <div class="col-sm-3 pull-right">
                                <button class="btn btn-block btn-primary" id="submit_top">{{trans('localize.update')}}</button>
                            </div><br><br><br>
                        </div>
                        <form class="form" id="edit" action="{{ url('admin/news/'.$news->id.'/edit') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins well">
                                <div class="ibox-title">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#ind">@lang('localize.Indonesian')</a></li>
                                        <li class=""><a href="#en">@lang('localize.English')</a></li>
                                    </ul>
                                </div>

                                <div class="ibox-content">
                                <div class="tab-content">
                                    <input type="hidden" name="id" value="{{$news->id}}" id="id">
                                    <div class="form-group tab-pane fade in active" id="ind">
                                        <label class="control-label">{{trans('localize.news.title')}} (@lang('localize.Indonesian'))<span style="color:red;">*</span></label>
                                        <input id="title_idn" placeholder="{{trans('localize.news.title')}}" class="form-control desc" name='title_idn' maxlength="150" value="{!! old('title_idn', $news->title_idn) !!}" autocomplete="off">
                                        <br>
                                        <label class="control-label">{{trans('localize.news.content')}} (@lang('localize.Indonesian'))<span style="color:red;">*</span></label>
                                        <textarea id="content_idn" placeholder="{{trans('localize.news.content')}}" class="form-control desc"  name='content_idn' rows="100">{!! old('content_idn', $news->content_idn) !!}</textarea>
                                        <label class="control-label">{{trans('localize.news.main_image')}} (@lang('localize.Indonesian'))</label>
                                        @if ($news->image_idn)
                                            <div>
                                                <img src="{{ \Storage::url('images/news/'.$news->image_idn) }}">
                                            </div>
                                            <br>
                                            <input type="checkbox" name="delete_image_idn"> @lang('localize.delete')
                                            <br><br>
                                        @endif
                                        <span class="btn btn-default btn-block">
                                            <input type="file" name="image_file_idn" id="image_file_idn" accept="image/*">
                                        </span>
                                        <label>@lang('localize.news.main_image_note')</label>
                                    </div>

                                    <div class="form-group tab-pane fade" id="en">
                                        <label class="control-label">{{trans('localize.news.title')}} (@lang('localize.English'))<span style="color:red;">*</span></label>
                                        <input id="title_en" placeholder="{{trans('localize.news.title')}}" class="form-control desc" name='title_en' maxlength="150" value="{!! old('title_en', $news->title_en) !!}" autocomplete="off">
                                        <br>
                                        <label class="control-label">{{trans('localize.news.content')}} (@lang('localize.English'))<span style="color:red;">*</span></label>
                                        <textarea id="content_en" placeholder="{{trans('localize.news.content')}}" class="form-control compulsary desc" name='content_en' rows="100">{!! old('content_en', $news->content_en) !!}</textarea>
                                        <label class="control-label">{{trans('localize.news.main_image')}} (@lang('localize.English'))</label>
                                        @if ($news->image_en)
                                            <div>
                                                <img src="{{ \Storage::url('images/news/'.$news->image_en) }}">
                                            </div>
                                            <br>
                                            <input type="checkbox" name="delete_image_en"> @lang('localize.delete')
                                            <br><br>
                                        @endif
                                        <span class="btn btn-default btn-block">
                                            <input type="file" name="image_file_en" id="image_file_en" accept="image/*">
                                        </span>
                                    </div>

                                    <label class="control-label">{{trans('localize.news.prio_no')}}</label>
                                    <input type="number" name="prio_no" class="form-control title" value="{{ old('prio_no', $news->prio_no) }}" min="0">
                                    <label>@lang('localize.news.priority_note')</label>
                                    <br><br>
                                    <label class="control-label">{{trans('localize.news.status')}}</label>
                                    <select class="form-control" name="status">
                                        <option value="1"{{ $news->status == 1 ? ' selected' : ''}}>@lang('localize.news.published')</option>
                                        <option value="0"{{ $news->status == 0 ? ' selected' : ''}}>@lang('localize.news.unpublished')</option>
                                    </select>
                                    <br>
                                </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <div class="col-lg-offset-9 col-lg-3">
                            <button class="btn btn-block btn-primary" id="submit">{{trans('localize.update')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('style')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('script')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script type="text/javascript">
    function sendFile(file, textarea_id) {
        data = new FormData();
        data.append("file", file);

        $.ajax({
            data: data,
            type: "POST",
            url: "/admin/news/uploadimage",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                $('#' + textarea_id).summernote('insertImage', url);
            }
        });
    }

    $(document).ready(function() {
        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });

        $('#content_en, #content_idn').summernote({
            height: 500,
                callbacks: {
                    onImageUpload: function(files) {
                        file_name = files[0].name;
                        var fileExtension = ['jpeg', 'jpg', 'png'];

                        if ($.inArray(file_name.split('.').pop().toLowerCase(), fileExtension) == -1) {
                            swal("{{trans('localize.error')}}", "{{trans('localize.imgError')}}", "error");
                            $(this).css('border', '1px solid red').focus().val('');
                        } else {
                            var textarea_id = $(this).attr('id');
                            sendFile(files[0], textarea_id);
                        }
                    }
                }
        });

        $('#submit, #submit_top').click(function(event) {
            $(':input').each(function(e) {
                if ($(this).hasClass('compulsary')) {
                    if (!$(this).val()) {
                        if($(this).is('textarea')) {
                            swal("{{trans('localize.error')}}", "{{trans('localize.fieldrequired')}}\n{{trans('localize.description_english')}}", "error");
                        } else {
                            $(this).attr('placeholder', '{{trans("localize.fieldrequired")}}').css('border', '1px solid red').focus();
                        }
                        $(this).closest('.form-group').addClass('has-error');
                        event.preventDefault();
                        return false;
                    }
                }
                $(this).css('border', '');
                $(this).closest('.form-group').removeClass('has-error');
            });

            $("#edit").submit();
        });
    });
</script>
@endsection
