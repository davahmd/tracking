@extends('admin.layouts.master')

@section('title', trans('localize.news.manage_news'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.news.manage_news')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/new/manage">{{trans('admin.nav.news')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.manage')}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
        <div class="col-lg-12">
            @include('admin.common.notifications')
            <div class="ibox">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="main-table" class="table table-striped table-bordered datatable-admin">
                            <thead>
                            <tr>
                                <th>@lang('localize.news.created_at')</th>
                                <th>@lang('localize.news.title')...@lang('localize.news.content')</th>
                                <th>@lang('localize.news.creator')</th>
                                <th>@lang('localize.news.prio_no')</th>
                                <th>@lang('localize.news.status')</th>
                                <th>@lang('localize.action')</th>
                            </tr>
                            </thead>
                        </table>


                        {{-- <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">#ID</th>
                                    <th class="text-nowrap" witdh="30%">{{trans('localize.product')}} {{trans('localize.Name')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Merchants')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.store')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Details')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.quantity_sold')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.category')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.sku_code')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.image')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Action')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Status')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $key => $product)
                                    <tr class="text-center">
                                        <td>{{ $product->pro_id }}</td>
                                        <td class="text-left">{{ $product->pro_title_en}}</td>
                                        <td class="text-left">
                                            <a class="nolinkcolor" href="/admin/merchant/view/{{ $product->pro_mr_id }}" data-toggle="tooltip" title="View this merchant">{{ $product->pro_mr_id }} - {{ $product->mer_fname }}</a>
                                        </td>
                                        <td class="text-left">
                                            @if(!empty($product->stor_id))
                                                <a class="nolinkcolor" href="/admin/store/edit/{{$product->mer_id}}/{{$product->stor_id}}" data-toggle="tooltip" title="Edit this store">{{$product->stor_id}} - {{$product->stor_name}}</a>
                                            @else
                                                <span class="text-danger">{{trans('localize.store_unavailable')}}</span>
                                            @endif
                                        </td>
                                        <td nowrap class="text-left">
                                            <p>
                                                @lang('localize.type') :
                                                @if($product->pro_type == 1)
                                                    @lang('localize.normal_product')
                                                @elseif($product->pro_type == 2)
                                                    @lang('localize.coupon')
                                                @elseif($product->pro_type == 3)
                                                    @lang('localize.ticket')
                                                @elseif($product->pro_type == 4)
                                                    @lang('localize.e-card.name')
                                                @endif
                                            </p>
                                            <p>{{trans('localize.quantity')}} : {{ $product->pro_qty }}</p>
                                            <p>
                                                <a class="btn btn-outline btn-link btn-xs" href="{{ url('admin/transaction/online/histories')."?".http_build_query(['product_id' => $product->pro_id]) }}" target="_blank">
                                                    <i class="fa fa-line-chart"></i> @lang('localize.View_Sold_Products')
                                                </a>
                                            </p>
                                        </td>
                                        <td class="text-nowrap">{{ $product->pro_no_of_purchase }}</td>
                                        <td class="text-nowrap">{{ $product->category }}</td>
                                     
                                        <td nowrap>
                                            @foreach ($product->pricing as $price)
                                            <p class="focushover text-navy" data-placement="auto bottom" style='cursor: pointer;' data-html='true' data-content=
                                            "
                                                <p class='text-center text-navy'>{{ $price->country->co_name }}</p>
                                                <p>@lang('localize.price') : {{ $price->country->co_cursymbol }} {{$price->price}}</p>
                                                <p>@lang('localize.quantity') : {{ $price->quantity }}</p>
                                                @if($price->attributes_name)
                                                @foreach(json_decode($price->attributes_name) as $at => $value)
                                                <p>{{ $at .' : '. $value }}</p>
                                                @endforeach
                                                @endif
                                                @if($price->discounted_price != 0.00)
                                                    <p>@lang('localize.discounted') : {{ $price->country->co_cursymbol }} {{ $price->discounted_price }}</p>
                                                    <p>@lang('localize.from') : {{ \Helper::UTCtoTZ($price->discounted_from) }}</p>
                                                    <p>@lang('localize.to') : {{ \Helper::UTCtoTZ($price->discounted_to) }}</p>
                                                @endif
                                            ">{{ $price->sku }}
                                            </p>
                                            @endforeach
                                        </td>
                                        @php
                                            $mer_id = $product->mer_id;
                                            $image = productImageLink($product->image, $mer_id);
                                        @endphp
                                        <td width="10%">
                                            <a href="{{ $image }}" title="{{ "{$product->pro_id} - {$product->pro_title_en}" }}" data-gallery=""><img src="{{ $image }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive img-thumbnail"></a>
                                        </td>
                                        <td class="text-nowrap" width="5%">
                                            <p>
                                            <a class="btn btn-white btn-sm" href="{{ url('admin/product/edit', [$product->pro_mr_id, $product->pro_id]) }}"><span><i class="fa fa-edit"></i> {{trans('localize.Edit')}}</span></a>
                                                <a class="btn btn-white btn-sm" href="{{ url('admin/product/view', [$product->pro_mr_id, $product->pro_id]) }}"><span><i class="fa fa-file-text-o"></i> {{trans('localize.view')}}</span></a>
                                            </p>

                                            @if($product->pro_type == 4)
                                            <p>
                                                <a style="width:100%" class="btn btn-white btn-sm" href="{{ url('admin/product/code/listing', [$product->pro_mr_id, $product->pro_id]) }}"><span><i class="fa fa-barcode"></i> @lang('localize.e-card.view')</span></a>
                                            </p>
                                            @endif

                                            <p>
                                                @if($product->pro_status == 1)
                                                    <a style="width:100%" class="btn btn-white btn-sm text-warning" href="/update_product_status/{{$product->pro_id}}/0"><span><i class="fa fa-refresh"></i>  {{trans('localize.set_to_inactive')}}</span></a>
                                                @elseif ($product->pro_status == 0)
                                                    <a style="width:100%" class="btn btn-white btn-sm text-navy" href="/update_product_status/{{$product->pro_id}}/1"><span><i class="fa fa-refresh"></i>  {{trans('localize.set_to_active')}}</span></a>
                                                @elseif ($product->pro_status == 3)
                                                    <p><a style="width:100%" class="btn btn-white btn-sm text-warning" href="/update_product_status/{{$product->pro_id}}/0"><span><i class="fa fa-refresh"></i>  {{trans('localize.set_to_inactive')}}</span></a></p>
                                                    <p><a style="width:100%" class="btn btn-white btn-sm text-navy" href="/update_product_status/{{$product->pro_id}}/1"><span><i class="fa fa-refresh"></i>  {{trans('localize.set_to_active')}}</span></a></p>
                                                @endif
                                            </p>
                                        </td>
                                        <td>
                                            @if ($product->pro_status == 1)
                                                <a target="_blank" class="btn btn-outline btn-link btn-sm btn-block" href="{{ route('product-preview', Helper::slug_maker($product->pro_title_en, $product->pro_id)) }}"><span><i class='fa fa-search'></i> {{trans('localize.preview_live')}}</span></a>
                                                <span class="text-nowrap text-navy"><i class='fa fa-check'></i> {{trans('localize.Active')}}</span>
                                            @elseif ($product->pro_status == 0)
                                                <span class="text-nowrap text-warning"><i class='fa fa-ban'></i> {{trans('localize.Inactive')}}</span>
                                            @elseif ($product->pro_status == 2)
                                                <span class="text-nowrap text-danger"><i class='fa fa-wrench'></i> {{trans('localize.incomplete')}}</span>
                                            @elseif ($product->pro_status == 3)
                                                <span class="text-nowrap text-danger"><i class='fa fa-warning'></i> {{trans('localize.pending_review')}}</span>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $products])

                        </table> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
@endsection

@section('script')
<script src="/backend/js/plugins/datatables/js/dataTables.min.js"></script>
<script src="/backend/js/plugins/datatables/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
        $("#main-table").DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/news/datatable',
            "error": function(response) {
                swal("{{trans('localize.error')}}", "{{trans('localize.fieldrequired')}}\n{{trans('localize.description_english')}}", "error");
            },
            columns: [
                {data:'created_at',width:'100px'},
                {data:'title'},
                {data:'creator',width:'200px'},
                {data:'prio_no',width:'35px'},
                {data:'status',width:'35px'},
                {data:'action',width:'200px',orderable:false,searchable:false}
            ],
            "language": {
                url: "@lang('common.lang-datatables')"
            },
            searching: false
        });
        
    });
</script>
@endsection