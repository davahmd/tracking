@extends('admin.layouts.master')

@section('title', trans('localize.news.view_news'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.news.view_news')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/news/manage">{{trans('admin.nav.news')}}</a>
            </li>
            <li class="active">
                <strong>{{$news->id}} - {{$news->title_localize}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    @include('admin.common.notifications')

    <div class="row">
        <div class="tabs-container">

            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="panel-body">
                        <div class="col-lg-offset-11 col-lg-1">
                            <a href="{{ url('admin/news/'.$news->id.'/edit') }}" class="btn btn-block btn-primary">{{trans('localize.edit')}}</a>
                        </div>
                        <br><br><br>
                        <form class="form">
                        {{ csrf_field() }}
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins well">
                                <div class="ibox-title">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#ind">@lang('localize.Indonesian')</a></li>
                                        <li class=""><a href="#en">@lang('localize.English')</a></li>
                                    </ul>
                                </div>

                                <div class="ibox-content">
                                <div class="tab-content">
                                    <input type="hidden" name="id" value="{{$news->id}}" id="id">
                                    <div class="form-group tab-pane fade in active" id="ind">
                                        <label class="control-label">{{trans('localize.news.title')}} (@lang('localize.Indonesian'))</label>
                                        <input id="title_idn" placeholder="{{trans('localize.news.title')}}" class="form-control desc" name='title_idn' maxlength="150" value="{!! old('title_idn', $news->title_idn) !!}" readonly>
                                        <br>
                                        <label class="control-label">{{trans('localize.news.content')}} (@lang('localize.Indonesian'))</label>
                                        <textarea id="content_idn" placeholder="{{trans('localize.news.content')}}" class="form-control desc"  name='content_idn' rows="100">{!! old('content_idn', $news->content_idn) !!}</textarea>
                                        <label class="control-label">{{trans('localize.news.main_image')}} (@lang('localize.Indonesian'))</label>
                                        @if ($news->image_en)
                                            <div>
                                                <img src="{{ \Storage::url('images/news/'.$news->image_idn) }}">
                                                <label>@lang('localize.news.main_image_note')</label>
                                            </div>
                                            <br>
                                        @endif
                                    </div>

                                    <div class="form-group tab-pane fade" id="en">
                                        <label class="control-label">{{trans('localize.news.title')}} (@lang('localize.English'))</label>
                                        <input id="title_en" placeholder="{{trans('localize.news.title')}}" class="form-control desc" name='title_en' maxlength="150" value="{!! old('title_en', $news->title_en) !!}" readonly>
                                        <br>
                                        <label class="control-label">{{trans('localize.news.content')}} (@lang('localize.English'))</label>
                                        <textarea id="content_en" placeholder="{{trans('localize.news.content')}}" class="form-control compulsary desc" name='content_en' rows="100">{!! old('content_en', $news->content_en) !!}</textarea>
                                        <label class="control-label">{{trans('localize.news.main_image')}} (@lang('localize.English'))</label>
                                        @if ($news->image_en)
                                            <div>
                                                <img src="{{ \Storage::url('images/news/'.$news->image_en) }}">
                                            </div>
                                            <br>
                                        @endif
                                    </div>

                                    <label class="control-label">{{trans('localize.news.prio_no')}}</label>
                                    <input type="text" name="prio_no" class="form-control title" value="{{ old('prio_no', $news->prio_no) }}" readonly>
                                    <label>@lang('localize.news.priority_note')</label>
                                    <br><br>
                                    <label class="control-label">{{trans('localize.news.status')}}</label>
                                    <input class="form-control" value="{{ $news->status == 0 ? trans('localize.news.unpublished') : trans('localize.news.published') }}" readonly>
                                    <br>
                                </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <div class="col-lg-offset-11 col-lg-1">
                            <a href="{{ url('admin/news/'.$news->id.'/edit') }}" class="btn btn-block btn-primary">{{trans('localize.edit')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('style')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('script')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });

        $('#content_en, #content_idn').summernote({
            height: 500,
        });

        $('#content_en').summernote('disable');
        $('#content_idn').summernote('disable');
    });
</script>
@endsection
