@extends('admin.layouts.master')

@section('title', trans('localize.manage_promotions'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{trans('localize.manage_promotions')}}</h2>
        <ol class="breadcrumb">
            <li>
                {{trans('localize.promotion')}}
            </li>
            <li class="active">
                <strong>{{trans('localize.manage_promotions')}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="ibox-content m-b-sm border-bottom">
        <div class="row">
            <form id="filter" action="{{ url('admin/promotion/manage') }}" method="GET">
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="text" value="{{$input['name']}}" placeholder="{{trans('localize.campaign_name')}}" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <select class="form-control" id="status" name="status">
                            <option value="" {{ ($input['status'] == "") ? 'selected' : '' }}>{{trans('localize.status')}}</option>
                            <option value="1" {{ ($input['status'] == "1") ? 'selected' : '' }}>{{trans('localize.active')}}</option>
                            <option value="0" {{ ($input['status'] == "0") ? 'selected' : '' }}>{{trans('localize.inactive')}}</option>
                            <option value="-1" {{ ($input['status'] == "-1") ? 'selected' : '' }}>{{trans('localize.cancelled')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <select class="form-control" id="sort" name="sort" style="font-family:'FontAwesome', sans-serif;">
                            {{--<option value="id_asc" {{ ($input['sort'] == "id_desc") ? 'selected' : '' }}>{{trans('localize.#id')}} : &#xf162;</option>
                            <option value="id_desc" {{ ($input['sort'] == "" || $input['sort'] == "id_desc") ? 'selected' : '' }}>{{trans('localize.#id')}} : &#xf163;</option>--}}
                            <option value="name_asc" {{ ($input['sort'] == "name_asc") ? 'selected' : '' }}>{{trans('localize.campaign_name')}} : &#xf15d;</option>
                            <option value="name_desc" {{ ($input['sort'] == "name_desc") ? 'selected' : '' }}>{{trans('localize.campaign_name')}} : &#xf15e;</option>
                            <option value="new" {{($input['sort'] == 'new') ? 'selected' : ''}}>{{trans('localize.newest')}}</option>
                            <option value="old" {{($input['sort'] == 'old') ? 'selected' : ''}}>{{trans('localize.oldest')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-block btn-outline btn-primary" id="filter">{{trans('localize.search')}}</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @include('admin.common.success')
            @include('admin.common.error')
            <div class="ibox">
                <div class="ibox-content">
                    <div class="table-responsive">
                        {{--@if($logintype == 'merchants')
                        <div class="col-lg-2 col-lg-offset-10" style="padding-right: 0;">
                            <a href="/{{ $route }}/product/add" class="btn btn-primary btn-block btn-md">@lang('localize.add_promotions')</a>
                        </div>
                        <hr>
                        @endif--}}
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">#</th>
                                    <th class="text-center text-nowrap">{{trans('localize.date')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.created_by')}}</th>
                                    <th class="text-nowrap" width="30%">{{trans('localize.campaign_name')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.type')}}</th>
                                    <th class="text-center text-nowrap" width="15%">{{trans('localize.period')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.discount_amount')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.coverage')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.status')}}</th>
                                    <th class="text-center text-nowrap" width="15%">{{trans('localize.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($promotions as $key => $promotion)
                                    <tr class="text-center">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $promotion->created_at }}</td>
                                        <td>
                                        	@if ($promotion->merchant_id == 0)
                                        		Zona
                                        	@else
                                        		<a href="/admin/merchant/view/{{ $promotion->merchant->mer_id }}">{{$promotion->merchant->username}}</a>
                                        	@endif
                                        </td>
                                        <td class="text-left">{{ $promotion->name}}</td>
                                        <td class="text-nowrap">{{ ($promotion->promo_type == 'F') ? 'Flash Sale' : 'Promo Code' }}</td>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $promotion->started_at)->format('d/m/Y') . ' - ' . \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $promotion->ended_at)->format('d/m/Y') }}</td>
                                        <td>
                                        	{{ $promotion->discount_rate ? $promotion->discount_rate*100 . '% Discount' : rpFormat($promotion->discount_value) }}<br>
                                        	{{ $promotion->promo_discount_max ? '(max: ' . rpFormat($promotion->promo_discount_max) . ')' : '' }}
                                        </td>
                                        <td>
                                            @php
                                                $stores = explode(',', $promotion->stores)
                                            @endphp

                                            @if ($promotion->categories && $promotion->products)
                                                Category & Product
                                            @elseif ($promotion->categories)
                                                Category
                                            @elseif ($promotion->products)
                                                Product
                                            @else
                                            	Store: {{ count($stores) }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($promotion->status == -1)
                                                <span class="text-nowrap text-danger"><i class='fa fa-wrench'></i> @lang('localize.cancelled')</span>

                                            @elseif ($promotion->status == 0)
                                                <span class="text-nowrap text-warning"><i class='fa fa-ban'></i> {{trans('localize.inactive')}}</span><br>
                                                <span class="badge badge-warning">{{ ($promotion->request == 'A') ? 'Waiting for Approval' : '' }}</span>
                                            @elseif ($promotion->status == 1)
                                                @if ($promotion->ended_at < \Carbon\Carbon::now())
                                                <span class="text-nowrap text-warning"><i class='fa fa-ban'></i> {{trans('localize.expired')}}</span>
                                                @else
                                                <span class="text-nowrap text-navy"><i class='fa fa-check'></i> {{trans('localize.active')}}</span><br>
                                                <span class="badge badge-primary">{{ ($promotion->request == 'C') ? 'Cancellation Requested' : '' }}</span>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                        	@if ($promotion->status == 0 && $promotion->merchant_id == 0)
                                        	<p>
                                            	<a href="{{ url('admin/promotion/edit', [$promotion->id]) }}" class="btn btn-white btn-sm btn-block">
                                            	<span><i class="fa fa-file-text-o"></i> {{trans('localize.edit')}}</span></a>
                                            </p>
                                            @endif
                                            <p>
                                                <a href="{{ url('admin/promotion/view', [$promotion->id]) }}" class="btn btn-white btn-sm btn-block"><span><i class="fa fa-file-text-o"></i> {{trans('localize.view')}}</span></a>
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $promotions])

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
@endsection

@section('script')
<script src="/backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.js"></script>
<script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.min.js"></script>

<script>
    $(document).ready(function() {

        $(".focushover").popover({ trigger: "hover" });

        var link = $('#search_type').val();
        $.get('/'+link, function(data){
            $("#name").typeahead({
                source: data
            });
        },'json');

        $('#search_type a').on('click', function(e) {
            e.preventDefault();
            $("#search_type li").removeClass("select");
            $(this).closest('li').addClass('select');
            $('#search_type').val($(this).data('value'));

            var link = $(this).data('value');
            $("#name").typeahead('destroy');
            $.get('/'+link, function(data){
                $("#name").typeahead({
                    source: data
                });
            },'json');
        });

    });

    function set_value() {
        var link = $('#search_type').val();
        $("#name").typeahead('destroy');

        $.get('/'+link, function(data){
            $("#name").typeahead({
                source: data
            });
        },'json');
    }

</script>
@endsection
