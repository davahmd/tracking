@extends('admin.layouts.master')
@section('title', 'View Promotion')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.view')}} {{trans('localize.promotion')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/promotion/manage">{{trans('localize.promotion')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.view')}} {{trans('localize.promotion')}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

    @include('admin.common.notifications')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{trans('localize.promotion_info')}}</h5>
                    <div class="ibox-tools">
                    	@if ($promotion->status == 0 && $promotion->merchant_id == 0)
                        <a href="/admin/promotion/edit/{{$promotion->id}}" class="btn btn-primary btn-sm">{{trans('localize.Edit')}} {{trans('localize.promotion')}}</a>
                        @endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-horizontal">
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.campaign_name')}}</label>
                            <div class="col-lg-10"><p class="form-control-static">{{ $promotion->name ? $promotion->name : 'Campaign Unavaiable' }}</p></div>
                    	</div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.merchant_name')}}</label>
                            <div class="col-lg-10"><p class="form-control-static">{{ ($promotion->merchant_id == 0) ? 'Zona' : $promotion->merchant->username }}</p></div>
                        </div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.period')}}</label>
                    		<div class="col-lg-10"><p class="form-control-static">{{ $promotion->started_at ? $promotion->started_at->format('d/m/Y') : 'Period Unavailable' }} - {{ $promotion->ended_at ? $promotion->ended_at->format('d/m/Y') : 'Period Unavailable' }}</p></div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.store')}}</label>
                    		<div class="col-lg-10">
                                <p class="form-control-static">
                                    <ul class="list-group"> 
                                    @forelse ($stores as $store)
                                        <li class="list-group-item">{{ $store->stor_name }}</li>
                                    @empty
                                        Not Available
                                    @endforelse
                                    </ul>
                                </p>
                            </div>
                    	</div>

                    	@if ($promotion->promo_type === 'P')
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.category')}}</label>
                    		<div class="col-lg-10">
                                <p class="form-control-static">
                                    <ul class="list-group">
                                    @forelse ($categories as $category)
                                        <li class="list-group-item">{{ $category->name_en }}</li>
                                    @empty
                                        Not Available
                                    @endforelse
                                    </ul>
                                </p>
                            </div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.product')}}</label>
                    		<div class="col-lg-10">
                                <p class="form-control-static">
                                    <ul class="list-group-item">
                                    @forelse ($products as $product)
                                        <li class="list-group-item">{{ $product->pro_title_en }}</li>
                                    @empty
                                        Not Available
                                    @endforelse
                                    </ul>
                                </p>
                            </div>
                    	</div>
                    	@endif

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.discount_type')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				{{ ($promotion->discount_rate) ? trans('localize.percentage') : '' }}
                    				{{ ($promotion->discount_value) ? trans('localize.amount') : '' }}	
	                    		</p>
	                    	</div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.value')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				{{ ($promotion->discount_rate) ? $promotion->discount_rate*100 . '%' : '' }}
                    				{{ ($promotion->discount_value) ? rpFormat($promotion->discount_value) : '' }}
	                    		</p>
	                    	</div>
                    	</div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.promo_min_spend')}}</label>
                            <div class="col-lg-10">
                                <p class="form-control-static">{{ ($promotion->promo_min_spend) ? rpFormat($promotion->promo_min_spend) : 'No Minimum Spend' }}</p>
                            </div>
                        </div>

                    	@if ($promotion->promo_type === 'P')
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.applies_to')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				<label>
						            	<input class="i-checks" type="checkbox" class="form-control" {{ ($promotion->applied_to != 1) ? 'checked' : '' }} disabled> Products
						            </label>
						            <label>
						            	<input class="i-checks" type="checkbox" class="form-control" {{ ($promotion->applied_to != 0) ? 'checked' : '' }} disabled> Shipping Fee
						            </label>
                    			</p>
                    		</div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.promo_code')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				{{ $promotion->promo_code }}
                    			</p>
                    		</div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.quantity')}}</label>
                    		<div class="col-lg-10"><p class="form-control-static">{{ ($promotion->promo_code_limit) ? $promotion->promo_code_limit : 'No Limit' }}</p></div>
                    	</div>

                    	@endif

                    	@if ($promotion->promo_type === 'F')
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.added_product')}}</label>
                    		<div class="col-lg-10">
                    			<table class="table">
                    				<thead>
                    					<th>{{trans('localize.product')}}</th>
                    					<th>{{trans('localize.quantity')}}</th>
                    				</thead>
                    				<tbody>
                    					@foreach ($promotion->promotion_products as $product)
                    					<tr>
                    						<td>{{ $product->detail->pro_title_en }}</td>
                    						<td>{{ $product->limit }}</td>
                    					</tr>
                    					@endforeach
                    				</tbody>
                    			</table>
                    		</div>
                    	</div>
                    	@endif

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.status')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                				@if ($promotion->status == -1)
                					<span class="badge badge-danger">Cancelled</span>
		                    	@elseif ($promotion->status == 0)
		                    		<span class="badge badge-warning">Inactive</span>
                                    <span class="badge badge-warning">{{ ($promotion->request == 'A') ? 'Waiting for Approval' : '' }}</span>
		                    	@elseif ($promotion->status == 1)
		                    		<span class="badge badge-success">Active</span>
                                    <span class="badge badge-primary">{{ ($promotion->request == 'C') ? 'Cancellation Requested' : '' }}</span>
		                    	@endif
	                    		</p>
	                    	</div>
                    	</div>

                        @if($promotion->status == -1)
                            @if($promotion->log->reason)
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.reason')}}</label>
                                <div class="col-lg-10">
                                    <p class="form-control-static">
                                        {{ $promotion->log->reason }}
                                    </p>
                                </div>
                            </div>
                            @endif
                        @endif

                        @if ($promotion->request != null)
                        
                            @if ($promotion->status == 0 && $promotion->request == 'A')
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.action')}}</label>
                                <div class="col-lg-10">
                                        <button type="button" class="btn btn-success button_submit" data-promotion-id="{{ $promotion->id }}">{{trans('localize.approve')}}</button>
                                </div>
                            </div>
                            @endif

                            @if ($promotion->status != -1)
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.action')}}</label>
                                <div class="col-lg-10">
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#denyPromotion">{{trans('localize.decline')}}</button>
                                </div>
                            </div>
                            @endif

                        @elseif ($promotion->status == 1)
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.action')}}</label>
                                <div class="col-lg-10">
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#denyPromotion">{{trans('localize.terminate')}}</button>
                                </div>
                            </div>
                        @elseif ($promotion->merchant_id == 0)

                            @if ($promotion->status == 0)
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.action')}}</label>
                                <div class="col-lg-10">
                                    <p class="form-control-static">
                                        <button type="button" class="btn btn-success button_submit" data-promotion-id="{{ $promotion->id }}">{{trans('localize.approve')}}</button>
                                    </p>
                                </div>
                            </div>
                            @endif

                            @if ($promotion->status != -1)
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.action')}}</label>
                                <div class="col-lg-10">
                                    <p class="form-control-static">
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#denyPromotion">{{trans('localize.terminate')}}</button>
                                    </p>
                                </div>
                            </div>
                            @endif

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="denyPromotion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action='/admin/promotion/deny/{{ $promotion->id }}' method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{trans('localize.disapprove')}} {{trans('localize.promotion')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">{{trans('localize.reason')}} <span class="text-danger">*</span></label>
            <textarea rows="8" class="form-control text-noresize" placeholder="{{trans('localize.reason')}}" name="reason"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('.button_submit').on('click', function(){
        var promotion_id = $(this).data('promotion-id');
        swal({
            title: "{{trans('localize.sure')}}",
            text: "{{trans('localize.confirm_approve_promotion')}}",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d9534f",
            confirmButtonText: "{{trans('localize.yes_approve_it')}}",
            cancelButtonText: "{{trans('localize.cancel')}}",
            closeOnConfirm: false
        }, function(){
            var url = '/admin/promotion/approve/' + promotion_id;
            window.location.href = url;
        });
    });
});
</script>
@endsection