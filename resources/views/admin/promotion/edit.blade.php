@extends('admin.layouts.master')

@section('title', 'Edit Promotion')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.edit_promotion')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/promotion/manage">{{trans('localize.promotion')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.edit_promotion')}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul id="tabMenu" class="nav nav-tabs">
                	@if ($promotion->promo_type == 'F')
                    <li class="active"><a data-toggle="tab" href="#flash-sale"> @lang('localize.flash_sale')</a></li>
                    @elseif ($promotion->promo_type == 'P')
                    <li class="active"><a data-toggle="tab" href="#promo-code"> @lang('localize.promo_code')</a></li>
                    @endif
                </ul>
                
				<div class="tab-content">
					@if ($promotion->promo_type == 'F')
                    <div id="flash-sale" class="tab-pane active">
                        <div class="panel-body">
						    <form class="form" action='/admin/promotion/edit/{{$promotion->id}}' method="POST" enctype="multipart/form-data">
						    	{{ csrf_field() }}
								@include('admin.common.errors')
						    	<div class="row">
						            <div class="col-lg-12">
						            	<div class="col-lg-6 no-spacing">
									        <div class="form-group">
									        	<label class="control-label">{{trans('localize.campaign_name')}} <span class='text-danger'>*</span></label>
									        	<input type="text" placeholder="{{trans('localize.campaign_name')}}" class="form-control compulsory" name="name" id="name" value="{{ old('name', $promotion->name)}}">
									        </div>
									        <div class="form-group">
									        	<label class="control-label">@lang('localize.promotion_date_range') <span class="text-danger">*</span></label>
									        	<input type="text" id="modal_daterange" name="modal_daterange" value="{{ old('modal_daterange', $daterange) }}" placeholder="Promotion date range" class="form-control compulsory" autocomplete="off" readonly>
									            <input type="hidden" name="start" value="{{ old('start', $promotion->started_at) }}" id="modal_sdate">
									            <input type="hidden" name="end" value="{{ old('end', $promotion->ended_at) }}" id="modal_edate">
									        </div>
									        <div class="form-group">
									            <label class="control-label">@lang('localize.store')</label>
									            <p class="form-control-static">
									            	<select class="form-control js-select-store" multiple="multiple" name="stores[]">
										            @foreach($stores as $store)
									            	<option value="{{ $store->stor_id }}">{{ $store->stor_name }}</option>
										            @endforeach
										           	</select>
									           		<button type="button" class="btn-block btn-primary select-all-stores">{{trans('localize.select_all')}}</button>
										        </p>
									        </div>
									        <div class="form-group">
					                        	<label class="control-label">@lang('localize.discount_type') <span class="text-danger">*</span></label>
				                                <p class="form-control-static">
				                                    <label class="installation">
				                                        <input class="i-checks installation-checked" type="radio" value="1" name="discount_type" {{ ($promotion->discount_rate != null) ? 'checked' : '' }}> @lang('localize.percentage')
				                                    </label> <br>
				                                    <label class="installation">
				                                        <input class="i-checks" type="radio" value="2" name="discount_type" {{ ($promotion->discount_value != null) ? 'checked' : '' }}> @lang('localize.amount')
				                                    </label>
				                                </p>
					                        </div>

					                        <div class="form-group">
					                        	<label class="control-label">{{trans('localize.value')}} <span class="text-danger">*</span></label>
					                        	<input type="number" class="form-control compulsory" placeholder="Percentage/Discounted Amount" name="value" value="{{old('value', ($promotion->discount_rate != null) ? $promotion->discount_rate*100 : $promotion->discount_value )}}">
					                        </div>
									    </div>
								    </div>

							        <div class="col-lg-12">
							            <div class="col-lg-12 no-spacing">
							                <div class="panel panel-default">
							                    <div class="panel-heading">
							                        <h5>@lang('localize.product')</h5>
							                     </div>
							                     <div class="panel-body">
							                     	<div class="form-group">
							                     		<label class="control-label">{{trans('localize.product')}}</label>
							                     		<select id="flash-sale-select-product" class="form-control js-select-product" name="preselect_products[]">
							                     			<option disabled>{{trans('localize.please_select_store_first')}}</option>
							                     		</select>
							                     		<div id='loader-select-product' class='text-center' style="display: none;">
										                    <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
										                </div>
							                     	</div>

							                        <div class="form-group">
							                        	<label class="control-label">{{trans('localize.quantity')}}</label>
							                        	<input type="number" class="form-control" name="quantity">
							                        </div>

							                        <div class="col-lg-3 pull-right">
							                        	<button type="button" class="btn btn-block btn-primary" id="add-product">{{trans('localize.add_product')}}</button>
							                        </div>

							                        <table class="table" id="product-table">
						                        		<thead>
						                        			<th>Product Name</th>
						                        			<th>Quantity</th>
						                        		</thead>
						                        		<tbody>
						                        			@if(old('products'))
							                        			@for($i=0; $i < count(old('products')); $i++)
							                        			<tr id="{{ old('products')[$i] }}">
							                        				<td>{{ old('product_name')[$i] }}</td>
							                        				<td>{{ old('quantity')[$i] }}</td>
							                        				<input type="hidden" name="product_name[]" value="{{ old('product_name')[$i] }}">
							                        				<input type="hidden" name="products[]" value="{{ old('products')[$i] }}">
							                        				<input type="hidden" name="quantity[]" value="{{ old('quantity')[$i] }}">
							                        				<td>
							                        					<i style="cursor: pointer;" class="fa fa-trash-o remove-product" onclick="removeProduct({{old('products')[$i]}}); return false;"></i>
							                        				</td>
							                        			</tr>
							                        			@endfor
							                        		@else
							                        			@foreach ($promotion->promotion_products as $product)
							                        			<tr id="{{ $product->detail->id }}">
							                        				<td>{{ $product->detail->pro_title_en }}</td>
							                        				<td>{{ $product->limit }}</td>
							                        				<input type="hidden" name="products[]" value="{{ $product->detail->id }}">
							                        				<input type="hidden" name="product_name[]" value="{{ $product->detail->pro_title_en }}">
							                        				<input type="hidden" name="quantity[]" value="{{ $product->limit }}">
							                        				<td><i style="cursor: pointer;" class="fa fa-trash-o remove-product" onClick="removeProduct({{$product->detail->id}}); return false;"></i></td>
							                        			</tr>
							                        			@endforeach
							                        		@endif
						                        		</tbody>
						                        	</table>
							                    </div>
							                </div>
							            </div>
							        </div>
							    </div>

							    <input type="hidden" name="promo_type" value="F">

						        <div class="row">
						            <div class="col-lg-3 pull-right">
						                <input type="submit" class="btn btn-block btn-primary" id="submit" name="submit" value="{{trans('localize.submit')}}">
						            </div>
						            <div class="col-lg-3 pull-right">
						                <input type="submit" class="btn btn-block btn-default" id="submit" name="save" value="{{trans('localize.save_as_draft')}}">
						            </div>
						        </div>

						    </form>
						</div>
					</div>
					@elseif ($promotion->promo_type == 'P')
					<div id="promo-code" class="tab-pane active">
						<div class="panel-body">
							<form class="form" action='/admin/promotion/edit/{{$promotion->id}}' method="POST" enctype="multipart/form-data">
						    	{{ csrf_field() }}
								@include('admin.common.errors')
						    	<div class="row">
						            <div class="col-lg-12">
						            	<div class="col-lg-6 no-spacing">
									        <div class="form-group">
									        	<label class="control-label">{{trans('localize.campaign_name')}} <span class='text-danger'>*</span></label>
									        	<input type="text" placeholder="{{trans('localize.campaign_name')}}" class="form-control compulsory" name="name" id="name" value="{{old('name', $promotion->name)}}">
									        </div>
									        <div class="form-group">
									        	<label class="control-label">@lang('localize.promotion_date_range') <span class="text-danger">*</span></label>
									        	<input type="text" id="modal_daterange" name="modal_daterange" value="{{$daterange}}" placeholder="Promotion date range" class="form-control compulsory" autocomplete="off" readonly>
									            <input type="hidden" name="start" value="{{ old('start', $promotion->started_at) }}" id="modal_sdate">
									            <input type="hidden" name="end" value="{{ old('end', $promotion->ended_at) }}" id="modal_edate">
									        </div>
									        <div class="form-group">
									            <label class="control-label">@lang('localize.store')</label>
									            <p class="form-control-static">
									            	<select class="form-control js-select-store" multiple="multiple" name="stores[]">
										            @foreach($stores as $store)
									            	<option value="{{ $store->stor_id }}">{{ $store->stor_name }}</option>
										            @endforeach
										           	</select>
									           		<button type="button" class="btn-block btn-primary select-all-stores">{{trans('localize.select_all')}}</button>
										        </p>
									        </div>

									    </div>
								    </div>

								    <div class="col-lg-12">
								    	<div class="col-lg-12 no-spacing">
								    		<div class="panel panel-default">
								    			<div class="panel-heading">
								    				<h5>{{trans('localize.discount')}}</h5>
								    			</div>
								    			<div class="panel-body">
								    				<div class="form-group">
							                        	<label class="control-label">@lang('localize.discount_type') <span class="text-danger">*</span></label>
						                                <p class="form-control-static">
						                                    <label class="installation">
						                                        <input class="i-checks installation-checked" type="radio" value="1" name="discount_type" {{ old('discount_type', 0)? 'checked' : '' }}> @lang('localize.percentage')
						                                    </label> <br>
						                                    <label class="installation">
						                                        <input class="i-checks" type="radio" value="2" name="discount_type" {{ !old('discount_type', 0)? 'checked' : '' }}> @lang('localize.amount')
						                                    </label>
						                                </p>
							                        </div>

							                        <div class="form-group">
							                        	<label class="control-label">{{trans('localize.value')}} <span class="text-danger">*</span></label>
							                        	<input type="number" class="form-control compulsory" placeholder="Percentage/Discounted Amount" name="value" value="{{old('value', ($promotion->discount_rate != null) ? $promotion->discount_rate*100 : $promotion->discount_value )}}">
							                        </div>

							                        <div class="form-group">
							                        	<label class="control-label">{{trans('localize.promo_min_spend')}} <span class="text-danger">*</span></label>
							                        	<input type="number" class="form-control" placeholder="{{trans('localize.promo_min_spend')}}" name="promo_min_spend" value="{{ old('promo_min_spend', $promotion->promo_min_spend) }}">
							                        </div>

							                        <div class="form-group">
							                        	<label class="control-label">@lang('localize.applies_to') <span class="text-danger">*</span></label>
											            <p class="form-control-static">
								                        	<label>
												            	<input class="i-checks" type="checkbox" class="form-control" name="applied_to[]" value="P" {{ ($promotion->applied_to != 1) ? 'checked' : '' }}> Products
												            </label>
												            <label>
												            	<input class="i-checks" type="checkbox" class="form-control" name="applied_to[]" value="S" {{ ($promotion->applied_to != 0) ? 'checked' : '' }}> Shipping Fee
												            </label>
												        </p>
							                        </div>
								    			</div>
								    		</div>
								    	</div>
								    </div>

								    <div class="col-lg-12">
								    	<div class="col-lg-12 no-spacing">
								    		<div class="panel panel-default">
								    			<div class="panel-heading">
								    				<h5>{{trans('localize.promo_code')}}</h5>
								    			</div>
								    			<div class="panel-body">
								    				<div class="form-group">
							                        	<label class="control-label">{{trans('localize.promo_code')}} <span class="text-danger">*</span></label>
							                        	<input type="text" class="form-control" placeholder="{{trans('localize.promo_code')}}" name="promo_code" maxlength="20" value="{{old('promo_code', $promotion->promo_code)}}">
							                        </div>
							                        <div class="form-group">
							                        	<label class="control-label">{{trans('localize.quantity')}} ({{trans('localize.optional')}})</label>
							                        	<input type="number" class="form-control" placeholder="{{trans('localize.quantity')}} ({{trans('localize.optional')}})" name="promo_code_limit" value="{{old('promo_code_limit', $promotion->promo_code_limit)}}">
							                        </div>
								    			</div>
								    		</div>
								    	</div>
								    </div>

							        <div class="col-lg-12">
							            <div class="col-lg-12 no-spacing">
							                <div class="panel panel-default">
							                    <div class="panel-heading">
							                        <h5>@lang('localize.category_product')</h5>
							                    </div>
												<div class="panel-body">

													<div class="form-group select-tool-width-100">
														<label class="control-label">{{trans('localize.category')}}</label>
														<select class="form-control js-select-category" name="categories[]">
															<option disabled>{{trans('localize.please_select_store_first')}}</option>
														</select>
														<div class='text-center loader-select-category' style="display: none;">
										                    <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
										                </div>
													</div>

							                     	<div class="form-group select-tool-width-100">
							                     		<label class="control-label">{{trans('localize.product')}}</label>
							                     		<select class="form-control js-select-product" name="products[]">
							                     			<option disabled>{{trans('localize.please_select_store_first')}}</option>
							                     		</select>
							                     		<div class='text-center loader-select-product' style="display: none;">
										                    <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
										                </div>
							                     	</div>

							                    </div>
							                </div>
							            </div>
							        </div>
							    </div>

								<input type="hidden" name="promo_type" value="P">

								<div class="row">
						            <div class="col-lg-3 pull-right">
						                <input type="submit" class="btn btn-block btn-primary" id="submit" name="submit" value="{{trans('localize.submit')}}">
						            </div>
						            <div class="col-lg-3 pull-right">
						                <input type="submit" class="btn btn-block btn-default" id="submit" name="save" value="{{trans('localize.save_as_draft')}}">
						            </div>
						        </div>

							</form>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
<script>
$(document).ready(function() {

	daterangePicker();

	oldInput();

	selectAllStores();

	var selectedStores = [];

	// Flash Sale
	$('#flash-sale select[name="stores[]"]').on('change', function(event) {

		$('#loader-select-product').show();

		selectedStores = $(this).val();

		if (!selectedStores) {
			$('#flash-sale select[name="preselect_products[]"]').empty();
		}
		else {
	    	$.ajax({
		        type: 'POST',
		        url: '/get_multiple_store_products',
		        data: {
		        	stores: selectedStores,
		        	start: $('#modal_sdate').val(),
		        	end: $('#modal_edate').val(),
		        },
		        success: function(data) {
		        	$('#flash-sale select[name="preselect_products[]"]').empty();
		            $('#flash-sale select[name="preselect_products[]"]').append('<option disabled>{{trans("localize.select_product")}}</option>');
		            for (var i = 0; i < data.length; i++) {
		                $('#flash-sale select[name="preselect_products[]"]').append('<option value="'+data[i].pro_id+'">'+data[i].pro_title_en+'</option>');
		            }

		            $('#flash-sale .js-select-product').select2({
	    				placeholder: '-- {{trans("localize.select_product")}} --',
	    			})

		            $('#loader-select-product').hide();
		        }
		    })
	    }
    });

	// Promo Code
    $('#promo-code select[name="stores[]"]').on('change', function(event) {

    	$('#promo-code .loader-select-category').show();
		$('#promo-code .loader-select-product').show();

		selectedStores = $(this).val();

		if (!selectedStores) {
			$('#flash-sale select[name="categories[]"]').empty();
			$('#flash-sale select[name="products[]"]').empty();
		}
		else {
	    	$.ajax({
	    		type: 'POST',
	    		url: '/get_multiple_store_categories',
	    		data: {stores: selectedStores},
	    		success: function(data) {
	    			$('#promo-code select[name="categories[]"]').empty();
	    			for (var i=0; i < data.length; i++) {
	    				$('#promo-code select[name="categories[]"]').append('<option value="'+data[i].id+'">'+data[i].name_en+'</option>');
	    			}

	    			$('#promo-code .js-select-category').select2({
	    				placeholder: '-- {{trans("localize.select_category")}} --',
	    			});

	    			$('#promo-code .loader-select-category').hide();
	    		}
	    	})

	    	$.ajax({
		        type: 'POST',
		        url: '/get_multiple_store_products',
		        data: {stores: selectedStores},
		        success: function(data) {
		        	$('#promo-code select[name="products[]"]').empty();
		            $('#promo-code select[name="products[]"]').append('<option disabled>{{trans("localize.select_product")}}</option>');
		            for (var i = 0; i < data.length; i++) {
		                $('#promo-code select[name="products[]"]').append('<option value="'+data[i].pro_id+'">'+data[i].pro_title_en+'</option>');
		            }

		            $('#promo-code .js-select-product').select2({
	    				placeholder: '-- {{trans("localize.select_product")}} --',
	    			});

		            $('#promo-code .loader-select-product').hide();
		        }
		    })
	    }
    });    

    var stores = '{{$selected_stores}}';
	preSelectedStores = stores.split(',');
	$('#flash-sale select[name="stores[]"], #promo-code select[name="stores[]"]').val(preSelectedStores);
	$('#flash-sale select[name="stores[]"], #promo-code select[name="stores[]"]').trigger('change');

	var categories = '{{$selected_categories}}';
	preSelectedCategories = categories.split(',');

	var products = '{{$selected_products}}';
	preSelectedProducts = products.split(',');

	$.ajax({
		type: 'POST',
		url: '/get_multiple_store_categories',
		data: {stores: preSelectedStores},
		success: function(data) {
			$('#promo-code select[name="categories[]"]').empty();
			for (var i=0; i < data.length; i++) {
				$('#promo-code select[name="categories[]"]').append('<option value="'+data[i].id+'">'+data[i].name_en+'</option>');
			}

			$('#promo-code .js-select-category').select2({
				placeholder: '-- {{trans("localize.select_category")}} --',
			});

			$('#promo-code .loader-select-category').hide();

			$('#promo-code select[name="categories[]"]').val(preSelectedCategories);
			$('#promo-code select[name="categories[]"]').trigger('change');
		}
	})

	$.ajax({
        type: 'POST',
        url: '/get_multiple_store_products',
        data: {stores: preSelectedStores},
        success: function(data) {
        	$('#promo-code select[name="products[]"]').empty();
            $('#promo-code select[name="products[]"]').append('<option disabled>{{trans("localize.select_product")}}</option>');
            for (var i = 0; i < data.length; i++) {
                $('#promo-code select[name="products[]"]').append('<option value="'+data[i].pro_id+'">'+data[i].pro_title_en+'</option>');
            }

            $('#promo-code .js-select-product').select2({
				placeholder: '-- {{trans("localize.select_product")}} --',
			});

            $('#promo-code .loader-select-product').hide();

            $('#promo-code select[name="products[]"]').val(preSelectedProducts);
			$('#promo-code select[name="products[]"]').trigger('change');
        }
    })
	

    $('#add-product').click(function() {

    	var data = $('#flash-sale-select-product').select2('data');
    	var quantity = $('input[name="quantity"]').val();

    	if (quantity < 1) {
    		return;
    	}

    	// loop selectedProducts first
    	for (var i = 0; i < data.length; i++) {

			$('#product-table tbody').append('<tr id="'+data[i].id+'">' + 
				'<td>'+ data[i].text +'</td>' + 
				'<td>'+ quantity + '</td>' +
				'<input type="hidden" name="products[]" value="' + data[i].id + '">' +
				'<input type="hidden" name="product_name[]" value="' + data[i].text + '">' +
				'<input type="hidden" name="quantity[]" value="' + quantity + '">' +
				'<td><i style="cursor: pointer;" class="fa fa-trash-o remove-product" onClick="removeProduct('+ data[i].id +'); return false;"></i></td>' +
			'</tr>');
		}

    	$('.js-select-product').val(null).trigger('change');
    	$('input[name="quantity"]').val(null);
    });
});

function removeProduct(id) {
	jQuery('#' + id).remove();
}

function oldInput() {

	var promo_type = '{{ old("promo_type") }}';

	if (promo_type == 'F') {
		$('#tabMenu a[href="#flash-sale"]').tab('show');
	} else if (promo_type == 'P') {
		$('#tabMenu a[href="#promo-code"]').tab('show');	
	}

	var old_stores = JSON.parse('{!! json_encode(old("stores")) !!}');
	var old_categories = JSON.parse('{!! json_encode(old("categories")) !!}');
	var old_products = JSON.parse('{!! json_encode(old("products")) !!}');

	$('#flash-sale select[name="stores[]"], #promo-code select[name="stores[]"]').val(old_stores);
	$('#flash-sale select[name="stores[]"], #promo-code select[name="stores[]"]').trigger('change');

	$('.js-select-product, .js-select-category, .js-select-store').select2({
		allowClear: true,
		multiple: true,
		placeholder: '-- {{trans("localize.please_select_store_first")}} --'
	});

	$.ajax({
        type: 'POST',
        url: '/get_multiple_store_categories',
        data: {stores: old_stores},
        success: function(data) {
        	$('#flash-sale select[name="categories[]"]').empty();
        	$('#promo-code select[name="categories[]"]').empty();
            for (var i = 0; i < data.length; i++) {
                $('#flash-sale select[name="categories[]"]').append('<option value="'+data[i].id+'">'+data[i].name_en+'</option>');
                $('#promo-code select[name="categories[]"]').append('<option value="'+data[i].id+'">'+data[i].name_en+'</option>');
            }

            $('#flash-sale .js-select-category, #promo-code .js-select-category').select2({
				placeholder: '-- {{trans("localize.select_category")}} --',
			});

			$('#promo-code select[name="categories[]"]').val(old_categories);
			$('#promo-code select[name="categories[]"]').trigger('change');
        }
    })

	$.ajax({
        type: 'POST',
        url: '/get_multiple_store_products',
        data: {
        	stores: old_stores,
        	start: $('#modal_sdate').val(),
        	end: $('#modal_edate').val(),
        },
        success: function(data) {
        	$('#flash-sale select[name="preselect_products[]"]').empty();
        	$('#promo-code select[name="products[]"]').empty();
            for (var i = 0; i < data.length; i++) {
                $('#flash-sale select[name="preselect_products[]"]').append('<option value="'+data[i].pro_id+'">'+data[i].pro_title_en+'</option>');
                $('#promo-code select[name="products[]"]').append('<option value="'+data[i].pro_id+'">'+data[i].pro_title_en+'</option>');
            }

            $('#flash-sale .js-select-product, #promo-code .js-select-product').select2({
				placeholder: '-- {{trans("localize.select_product")}} --',
			});

            $('#promo-code select[name="products[]"]').val(old_products);
			$('#promo-code select[name="products[]"]').trigger('change');
        }
    })
}

function selectAllStores() {

	$('#flash-sale .select-all-stores').click(function() {
		$('#flash-sale .js-select-store > option').prop('selected', true).trigger('change');
	});

	$('#promo-code .select-all-stores').click(function() {
		$('#promo-code .js-select-store > option').prop('selected', true).trigger('change');
	});
}

function daterangePicker() {

	var start = new Date("{{ old('start') }}");
    var end = new Date("{{ old('end') }}");

    var validate_start_date = "{{ old('start') }}";
    var validate_end_date = "{{ old('end') }}";

	if (validate_start_date == '' || validate_end_date == '') {
		$('input[name="modal_daterange"]').daterangepicker({
	        timePicker: false,
	        timePickerIncrement: 1,
	        autoUpdateInput : false,
	        startDate: new Date('{{ $promotion->started_at }}'),
	        endDate: new Date('{{ $promotion->ended_at }}'),
	        locale: {
	            format: 'DD-MM-YYYY h:mm A'
	        }
	    });
	}
	else {
		$('input[name="modal_daterange"]').daterangepicker({
	        timePicker: false,
	        timePickerIncrement: 1,
	        autoUpdateInput : false,
	        startDate: start,
	        endDate: end,
	        locale: {
	            format: 'DD-MM-YYYY h:mm A'
	        }
	    });
	}

	// Flash Sale
    $('#flash-sale input[name="modal_daterange"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        $('#flash-sale #modal_sdate').val($('#flash-sale #modal_daterange').data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm:ss'));
        $('#flash-sale #modal_edate').val($('#flash-sale #modal_daterange').data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm:ss'));
    });

    $('#flash-sale input[name="modal_daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $('#flash-sale #modal_sdate').val('');
        $('#flash-sale #modal_edate').val('');
    });

    // Promo Code
    $('#promo-code input[name="modal_daterange"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        $('#promo-code #modal_sdate').val($('#promo-code #modal_daterange').data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm:ss'));
        $('#promo-code #modal_edate').val($('#promo-code #modal_daterange').data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm:ss'));
    });

    $('#promo-code input[name="modal_daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $('#promo-code #modal_sdate').val('');
        $('#promo-code #modal_edate').val('');
    });
}
</script>
@endsection