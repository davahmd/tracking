@extends('layouts.backend_auth_master')

@section('title', trans('admin.login.title'))

@section('content')
<div class="admin-login-container">
    <div class="panel-login">
        <div class="brand">
            <img src="{{ asset('asset/images/logo/logo_admin.png') }}">
        </div>

        <div class="panel-title">@lang('admin.login.title')</div>

        <div class="form-container">

            @include('admin.common.errors')

            <form role="form" method="POST" action="{{ url('admin/login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label">@lang('localize.username')</label>
                    <input type="text" name="username" class="form-control" value="{{ old('username') }}">
                </div>

                <div class="form-group">
                    <label class="control-label">@lang('localize.password')</label>
                    <input type="password" name="password" class="form-control" >
                </div>

                <div class="form-group">
                    <label class="control-label">@lang('localize.captcha')</label>
                    <input type="text" name="captcha" class="form-control" maxlength="6" autocomplete="off">
                    <p class="captcha text-center">{!!captcha_img('flat')!!}</p>
                </div>
                
                <div class="action">
                    <button type="submit" class="btn btn-primary btn-block">@lang('localize.login')</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
