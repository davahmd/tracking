@extends('layouts.front_auth_master')

@section('content')
<div class="login-container forgot-password">
    <div class="content-box text-left">
        <form role="form" class="pd-y-md pd-x-lg border" method="POST" action="{{ url('admin/password/reset') }}">
            {{ csrf_field() }}

            <div class="text-center">
                <h2>@lang('localize.newpassword')</h2>
            </div>
            <br>

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label>@lang('localize.email')</label>
                <input type="email" name="email" class="form-control" placeholder="@lang('localize.email')" value="{{ $email or old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block text-danger">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label>@lang('localize.newpassword')</label>
                <input type="password" name="password" class="form-control" placeholder="@lang('localize.newpassword')">
                @if ($errors->has('password'))
                    <span class="help-block text-danger">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label>@lang('localize.confirmnewpassword')</label>
                <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('localize.confirmnewpassword')">
                @if ($errors->has('password_confirmation'))
                    <span class="help-block text-danger">
                        {{ $errors->first('password_confirmation') }}
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-login btn-block">@lang('localize.updatepassword')</button>
            <br/>

            <div class="text-center">
                <a href="{{ url('admin/login') }}">@lang('localize.back')</a>
            </div>
        </form>
    </div>
</div>
@endsection
