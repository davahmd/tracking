<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ config('app.name') }} - @yield('title')</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="country_state_lib_version" content="{{ Cache::get('country_state_lib_version_number') }}">

        <link rel="shortcut icon" href="{{ asset('common/images/favicon.png') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/font-awesome/css/font-awesome.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/sweetalert/sweetalert.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/daterangepicker/daterangepicker-bs3.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/iCheck/custom.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/animate.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/style.css') }}?v=1.1">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/custom.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/lib/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" >

        <link rel="stylesheet" type="text/css" href="{{ asset('common/lib/css/select2.min.css') }}?v=1.0"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('common/lib/css/bootstrap-fileinput.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('common/lib/css/tagsinput.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/typeahead/jquery.typeahead.min.css') }}" />

        <style type="text/css">
            .gllpMap {
                height: 228px;
            }
        </style>
        @yield('style')
    </head>

    <body>

        @include('admin.partial.nav')
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:700px">
                <div class="modal-content load_modal">
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal -->
        <div class="modal fade" id="myModal-static" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content load_modal">
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div id="spinner" class="loading" style="display:none;">
            <div class="sk-spinner sk-spinner-cube-grid">
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
                <div class="sk-cube"></div>
            </div>
        </div>

        <div id="blueimp-gallery" class="blueimp-gallery" style="display:none;">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">x</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>

        <!-- Mainly scripts -->
        <script type="text/javascript" src="{{ asset('backend/js/jquery-2.1.1.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
        <script src="//momentjs.com/downloads/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

        <!-- Custom and plugin javascript -->
        <script type="text/javascript" src="{{ asset('backend/js/inspinia.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/pace/pace.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/iCheck/icheck.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('common/js/country_state_f2.js') }}"></script>
        <script type="text/javascript" src="{{ asset('common/js/common_function.js') }}"></script>

        <script type="text/javascript" src="{{ asset('common/lib/js/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('common/lib/js/bootstrap-fileinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/Typehead/jquery.typeahead.min.js') }}"></script>

        @include('layouts.web.translation_script')

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).ready(function () {

                $( ".locale" ).each(function(index) {
                    $(this).on("click", function(){
                        var lang = $(this).attr('data-lang');
                        $.ajax({
                            type: 'post',
                            data: { 'lang': lang },
                            url: '{{ url('/home/setlocale') }}',
                            success: function (responseText) {
                                if (responseText) {
                                    if (responseText == "success") {
                                        window.location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                });

                $( ".countryloc" ).each(function(index) {
                    $(this).on("click", function(){
                        var id = $(this).attr('data-id');
                        $.ajax({
                            type: 'post',
                            data: { 'id': id },
                            url: '{{url('/home/setcountry')}}',
                            success: function (responseText) {
                                if (responseText) {
                                    if (responseText == "success") {
                                        location.reload();
                                        //window.location.replace(window.location.href);
                                    }
                                }
                            }
                        });
                    });
                });
            });

        </script>

        @yield('script')

    </body>
</html>
