@extends('admin.layouts.master')
@section('title', trans('localize.manage_services'))
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.manage_service')}}</h2>
        <ol class="breadcrumb">
            <li>
                {{trans('localize.product')}}
            </li>
            <li>
                {{trans('localize.manage_service')}}
            </li>
            <li class="active">
                <strong>{{ $product['details']->pro_id }} - {{ $product['details']->pro_title_en }}</strong>
            </li>
        </ol>
    </div>
</div>
@if(!$service_stores_schedule)
<div class="wrapper wrapper-content animated fadeInRight ecommerce">

        @include('admin.common.notifications')
    
        <div class="row">
            <div class="tabs-container">
    
                @include('admin.product.nav-tabs', ['link' => 'service'])
    
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                        <h5>@lang('localize.service_info')</h5>
                                </div>
                                <div class="panel-body">
                                    <form id="edit_service" class="form-horizontal" action="{{ url('admin/product/service', [$pro_id, 'create']) }}" method="POST">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label class="control-label">{{trans('localize.store')}}<span style="color:red;">*</span></label>
                                            @if(isset($service_stores))
                                                <select class="store_list" id="stor_id" name="stor_id" style="width:100%;">
                                                    <option value="">-- {{trans('localize.select_store')}} --</option>
                                                    @foreach ($service_stores as $service_store)
                                                        <option value="{{$service_store->stor_id}}" data-address="{{$service_store->stor_address1}}" {{($service_store->stor_id == old('stor_id')) ? 'selected' : ''}}>{{$service_store->stor_name}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('localize.interval_periods')}}<span style="color:red;">*</span></label>
                                            <select class="interval_list" id="interval_period" name="interval_period" style="width:100%">
                                                <option value="">-- {{trans('localize.pick_minutes')}} --</option>
                                                <option value="15" {{old('interval_period') == 15 ? 'selected' : ''}}>15 Minutes</option>
                                                <option value="30" {{old('interval_period') == 30 ? 'selected' : ''}}>30 Minutes</option>
                                                <option value="60" {{old('interval_period') == 60 ? 'selected' : ''}}>60 Minutes</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('localize.appoint_start')}}<span style="color:red;">*</span></label>
                                            <div class='input-group date' id='appoint_start'>
                                                {{-- <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span> --}}
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-primary" > <i class="fa fa-calendar"></i></button>
                                                </span>
                                                <input type='text' class="form-control" name="appoint_start" id="appoint_start_time" value="{{old('appoint_start') ? : ''}}" autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('localize.appoint_end')}}<span style="color:red;">*</span></label>
                                            <div class='input-group date' id='appoint_end'>
                                                {{-- <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span> --}}
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-primary" > <i class="fa fa-calendar"></i></button>
                                                </span>
                                                <input type='text' class="form-control" name="appoint_end" id="appoint_end_time" value="{{old('appoint_end') ? : ''}}" autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('localize.schedule_skip_count')}}<span style="color:red;">*</span></label>
                                            <input min="1" type="number" oninput="validity.valid||(value='');" class="form-control" name="schedule_skip_count" id="schedule_skip_count" value="{{old('schedule_skip_count') ? : ''}}">
                                        </div>
                                    </form>
                                    <div id="button_back_view" style="display:none;">
                                        <div class="col-lg-12 nopadding" style="margin-bottom: 18px;">
                                            <div class="col-md-2 col-md-offset-10">
                                                <button type="button" class="btn btn-primary btn-block btn-md btn-outline" id="button_back">@lang('localize.mer_back')</button>
                                            </div>
                                        </div>
    
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins well">
                                                <div class="ibox-title">
                                                    <h5>@lang('localize.extra_services')</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        {{ csrf_field() }}
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">@lang('localize.service_name')</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control" placeholder="@lang('localize.service_name_input')" name="service_name" id="service_name">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">@lang('localize.service_price')</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control" placeholder="@lang('localize.service_price_input')" name="service_price" id="service_price">
                                                                </div>
                                                            </div>
                                                            <input type='hidden' name="stor_id" value=""/>
                                                            <input type='hidden' name="interval_period" value=""/>
                                                            <input type='hidden' name="appoint_start" value=""/>
                                                            <input type='hidden' name="appoint_end" value=""/>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($edit_permission)
                                    <div class="form-group">
                                        <label class="control-label" id="label_extra_services">{{trans('localize.extra_services')}}</label>
                                        <div class="col-lg-12 nopadding" id="button_add_view" style="display:block;margin-bottom: 18px;">
                                            <div class="col-md-2 col-md-offset-10">
                                                <button type="button" class="btn btn-primary btn-block btn-md btn-outline" id="button_add">@lang('localize.add_service')</button>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-lg-12" id="table_list">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center text-nowrap" colspan="2">@lang('localize.extra_services')</th>
                                                        <th class="text-center text-nowrap" ></th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center text-nowrap">@lang('localize.service_name')</th>
                                                        <th class="text-center text-nowrap">@lang('localize.service_price')</th>
                                                        <th class="text-center text-nowrap">@lang('localize.action')</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="service-list">
                                                    @foreach ($service_list as $key => $item)
                                                    <tr class="text-center">
                                                        <td style="width:40%;" nowrap>
                                                            {{ $item->service_name }}
                                                        </td>
                                                        <td style="width:40%;" nowrap>
                                                            {{ $item->service_price }}
                                                        </td>
                                                        <td nowrap>
                                                            @if($edit_permission)
                                                            <button class="btn btn-white btn-sm text-primary" data-id="{{ $item->id }}" data-action="edit_service"><i class="fa fa-pencil"></i> @lang('localize.Edit')</button>
                                                            <button class="btn btn-white btn-sm text-danger" data-id="{{ $item->id }}" data-action="delete_service"><i class="fa fa-trash"></i> @lang('localize.delete')</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-2">
                                            <button id="btn_edit_service" type="button" class="btn btn-primary pull-right">@lang('localize.update')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    @include('admin.common.notifications')

    <div class="row">
        <div class="tabs-container">

            @include('admin.product.nav-tabs', ['link' => 'service'])

            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                    <h5>@lang('localize.service_info')</h5>
                            </div>
                            <div class="panel-body">
                                {{-- <form id="edit_service" class="form-horizontal" action="{{ url('admin/product/service', [$service_stores_schedule->id, $pro_id, 'edit']) }}" method="POST"> --}}
                                @if(!$service_stores_schedule)
                                    <form id="edit_service" class="form-horizontal" action="{{ url('admin/product/service', [$pro_id, 'create']) }}" method="POST">
                                        @else
                                        <form id="edit_service" class="form-horizontal" action="{{ url('admin/product/service', [$service_stores_schedule->id, $pro_id, 'edit']) }}" method="POST">
                                            @endif
                                {{-- <form id="edit_service" class="form-horizontal" action="{{ url('admin/product/service', [$pro_id, 'edit']) }}" method="POST"> --}}
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="control-label">{{trans('localize.store')}}<span style="color:red;">*</span></label>
                                        @if(isset($service_stores))
                                            <select class="store_list" id="stor_id" name="stor_id" style="width:100%;">
                                                <option value="">-- {{trans('localize.select_store')}} --</option>
                                                @foreach ($service_stores as $service_store)
                                                    <option value="{{$service_store->stor_id}}" data-address="{{$service_store->stor_address1}}" {{($service_store->stor_id == old('stor_id', $service_stores_schedule->stor_id)) ? 'selected' : ''}}>{{$service_store->stor_name}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">{{trans('localize.interval_periods')}}<span style="color:red;">*</span></label>
                                        <select class="interval_list" id="interval_period" name="interval_period" style="width:100%">
                                            <option value="">-- {{trans('localize.pick_minutes')}} --</option>
                                            <option value="15" {{old('interval_period', $service_stores_schedule->interval_period) == 15 ? 'selected' : ''}}>15 Minutes</option>
                                            <option value="30" {{old('interval_period', $service_stores_schedule->interval_period) == 30 ? 'selected' : ''}}>30 Minutes</option>
                                            <option value="60" {{old('interval_period', $service_stores_schedule->interval_period) == 60 ? 'selected' : ''}}>60 Minutes</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">{{trans('localize.appoint_start')}}<span style="color:red;">*</span></label>
                                        <div class='input-group date' id='appoint_start'>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            <input type='text' class="form-control" name="appoint_start" id="appoint_start_time" value="{{old('appoint_start', $service_stores_schedule->appoint_start) ? : ''}}" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">{{trans('localize.appoint_end')}}<span style="color:red;">*</span></label>
                                        <div class='input-group date' id='appoint_end'>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                            <input type='text' class="form-control" name="appoint_end" id="appoint_end_time" value="{{old('appoint_end', $service_stores_schedule->appoint_end) ? : ''}}" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">{{trans('localize.schedule_skip_count')}}<span style="color:red;">*</span></label>
                                        <input min="1" type="number" oninput="validity.valid||(value='');" class="form-control" name="schedule_skip_count" id="schedule_skip_count" value="{{old('schedule_skip_count', $service_stores_schedule->schedule_skip_count) ? : ''}}">
                                    </div>
                                </form>
                                <div id="button_back_view" style="display:none;">
                                    <div class="col-lg-12 nopadding" style="margin-bottom: 18px;">
                                        <div class="col-md-2 col-md-offset-10">
                                            <button type="button" class="btn btn-primary btn-block btn-md btn-outline" id="button_back">@lang('localize.mer_back')</button>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins well">
                                            <div class="ibox-title">
                                                <h5>@lang('localize.extra_services')</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="row">
                                                    {{-- <form id="add_service" class="form-horizontal" action="{{ url('admin/product/service', [$service_stores_schedule->id, $pro_id, 'add']) }}" method="POST"> --}}
                                                        @if(!$service_stores_schedule)
                                                        @else
                                                        <form id="add_service" class="form-horizontal" action="{{ url('admin/product/service', [$service_stores_schedule->id, $pro_id, 'add']) }}" method="POST">
                                                            @endif
                                                    {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">@lang('localize.service_name')</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" placeholder="@lang('localize.service_name_input')" name="service_name" id="service_name">
                                                            </div>
                                                        </div>
                                                        {{--<div class="form-group">
                                                            <label class="col-sm-2 control-label">@lang('localize.service_price')</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" placeholder="@lang('localize.service_price_input')" name="service_price" id="service_price">
                                                            </div>
                                                        </div>--}}
                                                        <input type='hidden' name="stor_id" value=""/>
                                                        <input type='hidden' name="interval_period" value=""/>
                                                        <input type='hidden' name="appoint_start" value=""/>
                                                        <input type='hidden' name="appoint_end" value=""/>
                                                        @if(!$service_stores_schedule)
                                                        @else
                                                        <div class="form-group">
                                                            <div class="col-sm-9 col-sm-offset-2">
                                                                <button id="btn_add_service" type="button" class="btn btn-outline btn-primary pull-right">@lang('localize.add_service')</button>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($edit_permission)
                                <div class="form-group">
                                    <label class="control-label" id="label_extra_services" >{{trans('localize.extra_services')}}</label>
                                    <div class="col-lg-12 nopadding" id="button_add_view" style="display:block;margin-bottom: 18px;">
                                        <div class="col-md-2 col-md-offset-10">
                                            <button type="button" class="btn btn-primary btn-block btn-md btn-outline" id="button_add">@lang('localize.add_service')</button>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="col-lg-12" id="table_list">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center text-nowrap" colspan="1">@lang('localize.extra_services')</th>
                                                    <th class="text-center text-nowrap" ></th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center text-nowrap">@lang('localize.service_name')</th>
                                                    {{--<th class="text-center text-nowrap">@lang('localize.service_price')</th>--}}
                                                    <th class="text-center text-nowrap">@lang('localize.action')</th>
                                                </tr>
                                            </thead>
                                            <tbody id="service-list">
                                                @foreach ($service_list as $key => $item)
                                                <tr class="text-center">
                                                    <td style="width:40%;" nowrap>
                                                        {{ $item->service_name }}
                                                    </td>
                                                    {{--<td style="width:40%;" nowrap>
                                                        {{ $item->service_price }}
                                                    </td>--}}
                                                    <td nowrap>
                                                        @if($edit_permission)
                                                        <button class="btn btn-white btn-sm text-primary" data-id="{{ $item->id }}" data-action="edit_service"><i class="fa fa-pencil"></i> @lang('localize.Edit')</button>
                                                        <button class="btn btn-white btn-sm text-danger" data-id="{{ $item->id }}" data-action="delete_service"><i class="fa fa-trash"></i> @lang('localize.delete')</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-2">
                                        <button id="btn_edit_service" type="button" class="btn btn-primary pull-right">@lang('localize.update')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@stop

@section('style')
<link href="/backend/css/plugins/daterangepicker/custom-daterangepicker.css" rel="stylesheet">
<link href="/web/lib/select2/css/select2.min.css" rel="stylesheet">
<link href="/web/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<style>
.table > tbody > tr > td {
     vertical-align: middle;
}
.table > thead > tr > th {
     vertical-align: middle;
}
</style>
@endsection

@section('script')
<script src="/backend/js/plugins/daterangepicker/moment.min.js"></script>
<script src="/backend/js/plugins/daterangepicker/custom-daterangepicker.js"></script>
<script src="/backend/js/custom.js?v3"></script>
<script src="/web/lib/select2/js/select2.full.min.js"></script>
<script src="/web/js/bootstrap-datetimepicker.min.js"></script>

<script>
    // $('#appoint_start_time, #appoint_end_time').datetimepicker({
    //     format: 'HH:mm',
    // });

    $(document).ready(function() {
        
        $('#appoint_start, #appoint_end').datetimepicker({
            format: 'HH:mm',
        });

        $('#appoint_start_time').click(function() {
            $('#appoint_start').data("DateTimePicker").toggle();
        });

        $('#appoint_end_time').click(function() {
            $('#appoint_end').data("DateTimePicker").toggle();
        });

        $('#button_add').click(function() {
            $('#button_back_view').show();
            $('#button_add_view').hide();
            $('#label_extra_services').hide();
            $('#table_list').hide();
        });

        $('#button_back').click(function() {
            $('#button_back_view').hide();
            $('#button_add_view').show();
            $('#label_extra_services').show();
            $('#table_list').show();
        });

        $('#btn_add_service').on('click', function() {
            var appoint_start = $('#appoint_start_time').val();
            var appoint_end = $('#appoint_end_time').val();
            var stor_id = $('#stor_id').val();
            var interval_period = $('#interval_period').val();

            $("[name=appoint_start]").val(appoint_start);
            $("[name=appoint_end]").val(appoint_end);
            $("[name=stor_id]").val(stor_id);
            $("[name=interval_period]").val(interval_period);

            $("#add_service").submit();
        });

        $('#btn_edit_service').on('click', function() {
            $("#edit_service").submit();
        });

        $('button').on('click', function() {
            var service_id = $(this).attr('data-id');
            var this_action = $(this).attr('data-action');

            if (this_action == 'edit_service') {
                edit_service(service_id, "{{ $pro_id }}");
            } else if(this_action == 'delete_service') {
                swal({
                    title: "{{ trans('localize.sure') }}",
                    text: "{{ trans('localize.confirm_delete_service') }}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#d9534f",
                    confirmButtonText: "{{ trans('localize.yes_delete_it') }}",
                    cancelButtonText: "{{ trans('localize.cancel') }}",
                    closeOnConfirm: true
                }, function(){
                    var url = '/product/service/delete/' + service_id + '/normal?product_id={{$pro_id}}';
                    window.location.href = url;
                });
            }
        });

        function formatState (state) {

            if (!state.id) {
            return state.text;
            }

            var $state = $(
            '<span style="color:black;font-weight:bold">' + state.text + '</span>'
            +'<span>' + '&nbsp;&nbsp;&nbsp;&nbsp;' + $(state.element).data('address') +'</span>'
            );
            return $state;
        };

        $(".store_list").select2({
            templateResult: formatState
        });

        $('.interval_list').select2();
    });
</script>
@endsection
