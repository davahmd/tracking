@extends('admin.layouts.master')

@section('title', trans('localize.manage') . ' ' . trans('localize.vehicle'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.Edit')}} {{trans('localize.product')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/product/manage">{{trans('localize.product')}}</a>
            </li>
            <li>
                {{trans('localize.Edit')}}
            </li>
            <li class="active">
                <strong>{{$product['details']->pro_id}} - {{$product['details']->pro_title_en}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    @include('admin.common.notifications')

    <div class="row">
        <div class="tabs-container">

            @include('admin.product.nav-tabs', ['link' => 'vehicle'])

            <div class="tab-content">
            	<div class="tab-pane active">
            		<div class="panel-body">

            			<div class="ibox float-e-margins">
            				<div class="ibox-tools">
            					<a class="collapse-link">
            						<div class="btn btn-primary btn-outline">@lang('localize.add_new_vehicle')</div>
            					</a>
            				</div>
            				<div class="ibox-content" style="display:none; border-style:none;">
                                <div class="row">
                                    <form class="form-horizontal" action="/admin/product/vehicle" method="POST">
                                    {{ csrf_field() }}
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">@lang('localize.Brand')</label>
                                            <div class="col-sm-9">
                                                <select class="form-control js-option-tags" name="brand" style="width: 50%;">
                                                    <option disabled selected>{{trans('localize.select_brand')}}</option>
                                                	@foreach($brands as $brand)
                                                	<option value="{{$brand}}">{{$brand}}</option>
                                                	@endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">@lang('localize.Model')</label>
                                            <div class="col-sm-9">
                                                <select class="form-control js-option-tags" name="model" style="width: 50%;">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">@lang('localize.Variant')</label>
                                            <div class="col-sm-9">
                                                <select class="form-control js-option-tags" name="variant" style="width: 50%;">
                                                	<option></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">@lang('localize.Year')</label>
                                            <div class="col-sm-9">
                                                <select class="form-control js-option-tags" name="vehicle" style="width: 50%;">
                                                	<option></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-9 col-sm-offset-2">
                                                <input type="hidden" name="pro_id" value="{{$pro_id}}">
                                                <button type="submit" class="btn btn-outline btn-primary pull-right">@lang('localize.add_vehicle')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-lg-12" id="table_list">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>{{trans('localize.Brand')}}</th>
                                                <th>{{trans('localize.Model')}}</th>
                                                <th>{{trans('localize.Variant')}}</th>
                                                <th>{{trans('localize.Year')}}</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($vehicles as $vehicle)
                                            <tr>
                                                <td>{{$vehicle->brand}}</td>
                                                <td>{{$vehicle->model}}</td>
                                                <td>{{$vehicle->variant}}</td>
                                                <td>{{$vehicle->year}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-circle btn-sm btn-outline pull-right button_delete" pro-id="{{$product['details']->pro_id}}" vehicle-id="{{$vehicle->id}}" data-action="delete_vehicle"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                        @include('layouts.partials.table-pagination', ['listings' => $vehicles])

                                    </table>
                                </div>
                            </div>

            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
	$('.js-option-tags').select2({
		tags: false
	});

	$('select[name="brand"]').change(function(e) {
        $('select[name="model"]').empty();
        $('select[name="variant"]').empty();
        $('select[name="vehicle"]').empty();

		$.ajax({
			type: 'GET',
			url: '/get_model/'+$(this).val(),
			success: function(data) {
                $('select[name="model"]').append('<option disabled selected>{{trans("localize.select_model")}}</option>');
				for (var i = 0; i < data.length; i++) {
                    $('select[name="model"]').append('<option>'+data[i]+'</option>');
                }
			}
		})
	});

    $('select[name="model"]').change(function(e) {
        $('select[name="variant"]').empty();
        $('select[name="vehicle"]').empty();

        var brand = $('select[name="brand"]').val();

        $.ajax({
            type: 'GET',
            url: '/get_variant/'+ brand +'/'+$(this).val(),
            success: function(data) {
                $('select[name="variant"]').append('<option disabled selected>{{trans("localize.select_variant")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('select[name="variant"]').append('<option>'+data[i]+'</option>');
                }
            }
        })
    });

    $('select[name="variant"]').change(function(e) {
        $('select[name="vehicle"]').empty();

        var model = $('select[name="model"]').val();

        $.ajax({
            type: 'GET',
            url: '/get_year/'+ model +'/'+$(this).val(),
            success: function(data) {
                $('select[name="vehicle"]').append('<option disabled selected>{{trans("localize.select_year")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('select[name="vehicle"]').append('<option value="'+data[i].id+'">'+data[i].year+'</option>');
                }
            }
        })
    });

    $('.button_delete').on('click', function(){
        var pro_id = $(this).attr('pro-id');
        var vehicle_id = $(this).attr('vehicle-id');
        swal({
            title: "{{trans('localize.sure')}}",
            text: "{{trans('localize.confirm_delete_vehicle')}}",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d9534f",
            confirmButtonText: "{{trans('localize.yes_delete_it')}}",
            cancelButtonText: "{{trans('localize.cancel')}}",
            closeOnConfirm: false
        }, function(){
            var url = '/admin/product/vehicle/delete/' + pro_id + '/' + vehicle_id;
            window.location.href = url;
        });
    });
});
</script>
@endsection