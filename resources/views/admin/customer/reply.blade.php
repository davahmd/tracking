@extends('admin.layouts.master')

@section('title', 'Reply Query')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.reply')}} {{trans('localize.query')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">{{trans('localize.customer')}}</a>
            </li>
            <li class="">
                <a>{{trans('localize.view')}} {{trans('localize.queries')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.reply')}} {{trans('localize.queries')}}</strong>
            </li>
        </ol>
    </div>
</div>

@include('admin.common.error')

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('localize.query')</h5>
                </div>
                <div class="ibox-content">
                    <form>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">@lang('localize.date') : </label>
                                <input class="form-control" value="{{ date_format($query->created_at, 'd-m-Y H.ia') }}" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label">@lang('localize.name') : </label>
                                <input class="form-control" value="{{ $query->name }}" disabled>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label">@lang('localize.email') : </label>
                                <input class="form-control" value="{{ $query->email }}" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">@lang('localize.subject') : </label>
                                <input class="form-control" value="{{ $query->subject }}" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">@lang('localize.message') : </label>
                                <textarea class="form-control" style="resize:none;" rows="10" disabled>{{ $query->content }}</textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('localize.reply')</h5>
                </div>
                <div class="ibox-content">
                    <form id="form-reply" action="/admin/customer/inquiries/reply" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">@lang('localize.name') : </label>
                                <input class="hidden" name="query_id" value="{{ $query->id }}">
                                <input class="form-control" value="{{ $admin->adm_lname }}" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">@lang('localize.subject') : </label>
                                <input class="form-control" value="RE: {{ $query->subject }}" disabled>
                            </div>
                        </div>
                        @if ($query->reply == null)
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">@lang('localize.message') : </label>
                                <textarea class="form-control" style="resize:none;" rows="10" name="reply"></textarea>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">@lang('localize.message') : </label>
                                <textarea class="form-control" style="resize:none;" rows="10" name="reply" disabled>{{ $query->reply }}</textarea>
                            </div>
                        </div>
                        @endif
                        <div class="action text-center">
                            <button type="submit" class="btn btn-primary" {{isset($query->reply)?'disabled':''}}>@lang('localize.reply')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
