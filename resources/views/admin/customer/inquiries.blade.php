@extends('admin.layouts.master')
@section('title', trans('localize.manage') . ' ' . trans('localize.Inquiries'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.manage')}} {{trans('localize.Inquiries')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">{{trans('localize.customer')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.manage')}} {{trans('localize.Inquiries')}}</strong>
            </li>
        </ol>
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" id="filter" action='/admin/customer/inquiries' method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.search')}}</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{$input['search']}}" placeholder="{{trans('localize.Search_in_table')}}" class="form-control" id="search" name="search">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.sort')}}</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="sort" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                    <option value="new" {{($input['sort'] == "" || $input['sort'] == 'new') ? 'selected' : ''}}>{{trans('localize.Newest')}}</option>
                                    <option value="old" {{($input['sort'] == 'old') ? 'selected' : ''}}>{{trans('localize.Oldest')}}</option>
                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary" id="filter">{{trans('localize.search')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        @include('admin.common.success')
            <div class="ibox">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">#No</th>
                                    <th width="15%" class="text-center text-nowrap">{{trans('localize.Name')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.email')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.subject')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                    $i = (( $inquiries->currentPage() - 1 ) * $inquiries->perPage() ) + $i;
                                ?>
                                @foreach($inquiries as $inquiry)
                                <tr class="text-center">
                                    <td>{{$inquiry->id }}
                                        @if($inquiry->seen == false)
                                        <span><label class="badge" style="color: red;">New!</label></span>
                                        @endif
                                    </td>
                                    <td>{{$inquiry->name}}</td>
                                    <td>{{$inquiry->email}}</td>
                                    <td>{{$inquiry->subject}}</td>
                                    <td>
                                        <a href="/admin/customer/inquiries/view/{{$inquiry->id}}" class="fa fa-reply fa-2x"><i></i></a>
                                        @if($delete_permission)
                                        <a href="/admin/customer/inquiries/delete/{{$inquiry->id}}" class="fa fa-trash fa-2x" style="color: red;"><i></i></a>
                                        @endif
                                    </td>
                                </tr>
                                    <?php $i++; ?>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $inquiries])

                        </table>
                        <div class="text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>
@endsection
