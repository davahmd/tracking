@extends('admin.layouts.master')

@section('title', trans('localize.online_listing'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.price_negotiation')</h2>
        {{--  <ol class="breadcrumb">
            <li>
                @lang('localize.price_negotiation')
            </li>
            <li>@lang('localize.online_orders')</li>
            <li class="active">
                <strong>@lang('localize.listing')</strong>
            </li>
        </ol>  --}}
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
     <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" action="{{ route('admin.negotiation.manage') }}" method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.customer') @lang('localize.id')" name="customer_id" value="{{ $input['customer_id'] }}">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.customer')" name="customer_name" value="{{ $input['customer_name'] }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.Merchant') @lang('localize.id')" name="merchant_id" value="{{ $input['merchant_id'] }}">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.Merchant')" name="merchant_name" value="{{ $input['merchant_name'] }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.product') @lang('localize.id')" name="product_id" value="{{ $input['product_id'] }}">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.product')" name="product_name" value="{{ $input['product_name'] }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.status')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="status">
                                <option value="">@lang('localize.all')</option>
                                @foreach($status_list as $key => $value)
                                <option value="{{ $key }}" {{ (!is_null($input['status']) && $input['status'] == $key) ? 'selected' : ''}}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.sort')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="new" {{ (!$input['sort'] || $input['sort'] == 'new') ? 'selected' : ''}}>@lang('localize.Newest')</option>
                                <option value="old" {{ ($input['sort'] == 'old') ? 'selected' : ''}}>@lang('localize.Oldest')</option>
                                <option value="customer_name_asc" {{ ($input['sort'] == 'customer_name_asc') ? 'selected':'' }}>{{trans('localize.customer')}} {{trans('localize.name')}} : &#xf15d;</option>
                                <option value="customer_name_desc" {{ ($input['sort'] == 'customer_name_desc') ? 'selected':'' }}>{{trans('localize.customer')}} {{trans('localize.name')}} : &#xf15e;</option>
                                <option value="merchant_name_asc" {{ ($input['sort'] == 'merchant_name_asc') ? 'selected':'' }}>{{trans('localize.Merchant')}} {{trans('localize.name')}} : &#xf15d;</option>
                                <option value="merchant_name_desc" {{ ($input['sort'] == 'merchant_name_desc') ? 'selected':'' }}>{{trans('localize.Merchant')}} {{trans('localize.name')}} : &#xf15e;</option>
                                <option value="product_name_asc" {{ ($input['sort'] == 'product_name_asc') ? 'selected':'' }}>{{trans('localize.product')}} {{trans('localize.name')}} : &#xf15d;</option>
                                <option value="product_name_desc" {{ ($input['sort'] == 'product_name_desc') ? 'selected':'' }}>{{trans('localize.product')}} {{trans('localize.name')}} : &#xf15e;</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary">@lang('localize.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            @include('merchant.common.notifications')

            <div class="ibox">

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <th nowrap class="text-center">@lang('localize.#id')</th>
                                    <th nowrap class="text-center">@lang('localize.customer')</th>
                                    <th nowrap class="text-center">@lang('localize.store')</th>
                                    <th nowrap class="text-center">@lang('localize.product')</th>
                                    <th nowrap class="text-center">@lang('localize.original_price')</th>
                                    <th nowrap class="text-center">@lang('localize.quantity')</th>
                                    <th nowrap class="text-center">@lang('localize.price_offer')</th>
                                    <th nowrap class="text-center">@lang('localize.status')</th>
                                    <th nowrap class="text-center">@lang('localize.expired_at')</th>
                                    <th nowrap class="text-center">@lang('localize.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($negotiations as $negotiation)
                                <tr>
                                    <td class="text-center">{{ $loop->remaining + 1 }}</td>
                                    <td class="text-center">{{ $negotiation->customer->cus_id }} - {{ $negotiation->customer->cus_name }}</td>
                                    <td class="text-center">{{ $negotiation->product->store->stor_id }} - {{ $negotiation->product->store->stor_name }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('product-preview', Helper::slug_maker($negotiation->product->pro_title_en, $negotiation->product->pro_id)) }}" target="_blank"> 
                                            {{ $negotiation->product->pro_id }} - {{ $negotiation->product->title }}
                                        </a>
                                    </td>
                                    <td class="text-center">{{ rpFormat($negotiation->pricing->price) }}</td>
                                    <td class="text-center">{{ $negotiation->quantity }}</td>
                                    <td class="text-center">
                                        @if ($negotiation->merchant_offer_price)
                                            @lang('localize.Merchant'): {{ rpFormat($negotiation->merchant_offer_price) }} {{ ($negotiation->final_price == true) ? '(FINAL)' : '' }}
                                        @else
                                            @lang('localize.customer'): {{ rpFormat($negotiation->customer_offer_price) }}
                                        @endif
                                    </td>
                                    <td class="text-center">{{ \App\Models\PriceNegotiation::statusType()[$negotiation->status] }}</td>
                                    <td class="text-center">{{ \App\Helpers\Helper::UTCtoTZ($negotiation->expired_at) }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-primary btn-xs btn-view" data-toggle="modal" data-target="#nego-detail" data-id="{{ $negotiation->id }}" data-store-name="{{ $negotiation->product->store->stor_name }}" data-store-id="{{ $negotiation->product->store->stor_id }}"><i class="fa fa-comments"></i> @lang('localize.view')</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $negotiations])

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="nego-detail" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang('localize.price_negotiation')</h4>
            </div>
            <div class="modal-body">
                <form id="form-nego" method="POST">
                {{ csrf_field() }}
                <section class="product">
                    <div class="thumb">
                        
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.customer')</label>
                        <input class="form-control customer-name" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.store')</label>
                        <input class="form-control store-name" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.product')</label>
                        <input class="form-control product-name" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.quantity')</label>
                        <input class="form-control quantity" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.price')</label>
                        <input class="form-control price" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.status')</label>
                        <input class="form-control status" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.expiry_date')</label>
                        <input class="form-control expired-date" readonly>
                    </div>
                </section>

                <hr>

                <h4 class="modal-title">@lang('localize.negotiation_history')</h4>

                <div class="table-responsive">
                    <table class="table table-stripped table-bordered">
                        <thead>
                            <th>Date</th>
                            <th>Customer/Merchant</th>
                            <th>Price</th>
                            {{--<th>Expiry Date</th>--}}
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                </form>
            </div>
      </div>

    </div>
</div>
@endsection

@section('style')
<style>
    td {
        vertical-align: middle !important;
    }
</style>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
<script>
$(document).ready(function() {
    $('.btn-view').on('click', function() {
        var this_id = $(this).data('id');

        $.ajax({
            type: 'GET',
            url: '/get_admin_negotiation_detail/' + this_id,
            success: function(data) {

                console.log(data);

                $('#nego-detail .customer-name').val('');
                $('#nego-detail .store-name').val('');
                $('#nego-detail .product-name').val('');
                $('#nego-detail .quantity').val('');
                $('#nego-detail .price').val('');
                $('#nego-detail .status').val('');

                switch(data[data.length-1].status) {
                    case '0':
                        var status = "{{trans('localize.nego_status.initiated')}}";
                        break;
                    case '10':
                        var status = "{{trans('localize.nego_status.active')}}";
                        break;
                    case '11':
                        var status = "{{trans('localize.nego_status.mer_accepted')}}";
                        break;
                    case '12':
                        var status = "{{trans('localize.nego_status.cus_accepted')}}";
                        break;
                    case '20':
                        var status = "{{trans('localize.nego_status.completed')}}";
                        break;
                    case '30':
                        var status = "{{trans('localize.nego_status.declined')}}";
                        break;
                    case '31':
                        var status = "{{trans('localize.nego_status.mer_declined')}}";
                        break;
                    case '32':
                        var status = "{{trans('localize.nego_status.cus_declined')}}";
                        break;
                    case '40':
                        var status = "{{trans('localize.nego_status.expired')}}";
                        break;
                    default:
                        var status = "{{trans('localize.nego_status.failed')}}";
                }

                $('#nego-detail .customer-name').val(data[0].customer.cus_id + ' - ' + data[0].customer.cus_name);
                $('#nego-detail .store-name').val(data[0].product.store.stor_id + ' - ' + data[0].product.store.stor_name);
                $('#nego-detail .product-name').val(data[0].product.pro_id + ' - ' + data[0].product.pro_title_en);
                $('#nego-detail .quantity').val(data[0].quantity);
                $('#nego-detail .price').val(rpFormat('{{ config("app.currency_code") }}', data[0].pricing.price));
                $('#nego-detail .status').val(status);
                $('#nego-detail .expired-date').val(data[0].expired_at_tz);

                $('#nego-detail table tbody').empty();

                $.each(data, function(i) {;
                    $('#nego-detail table tbody').append(
                        '<tr>' +
                            '<td>' + data[i].created_at_tz + '</td>' +
                            '<td>' + data[i].customer.cus_name + '</td>' +
                            '<td>' + rpFormat('{{ config("app.currency_code") }}', data[i].customer_offer_price) + '</td>' +
                            // '<td>' + data[i].expired_at + '</td>' +
                        '</tr>' + (data[i].merchant_offer_price ? 
                        '<tr>' +
                            '<td>' + data[i].updated_at_tz + '</td>' +
                            '<td>' + data[i].product.store.stor_name + '</td>' +
                            '<td>' + rpFormat('{{ config("app.currency_code") }}', data[i].merchant_offer_price) + '</td>' +
                            // '<td>' + data[i].expired_at + '</td>' +
                        '</tr>' : '')
                    );
                });
            }
        });
    });
});
</script>
@endsection
