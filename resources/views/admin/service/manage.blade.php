@extends('admin.layouts.master')

@section('title', trans('admin.nav.services'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{trans('admin.nav.services')}}</h2>
    </div>
</div>

<div class="row wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="ibox-content m-b-sm border-bottom">
        <div class="row">
            <form id="filter" action="{{ url( 'admin/service/manage') }}" method="GET">
                <div class="col-sm-1">
                    <div class="form-group">
                        <input type="text" value="{{$input['id']}}" placeholder="{{trans('localize.#id')}}" class="form-control" id="id" name="id">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="text" value="{{$input['name']}}" placeholder="{{trans('localize.product_name')}}" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <input type="text" value="{{$input['order_id']}}" placeholder="{{trans('localize.order_id')}}" class="form-control" name="order_id">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <select class="form-control" id="status" name="status">
                            <option value="" {{ ($input['status'] == "") ? 'selected' : '' }}>{{trans('localize.status')}}</option>
                            <option value="1" {{ ($input['status'] == "1") ? 'selected' : '' }}>{{trans('localize.scheduled_by_member')}}</option>
                            <option value="0" {{ ($input['status'] == "0") ? 'selected' : '' }}>{{trans('localize.unscheduled')}}</option>
                            <option value="2" {{ ($input['status'] == "2") ? 'selected' : '' }}>{{trans('localize.scheduled')}}</option>
                            <option value="3" {{ ($input['status'] == "3") ? 'selected' : '' }}>{{trans('localize.confirmed')}}</option>
                            <option value="-1" {{ ($input['status'] == "-1") ? 'selected' : '' }}>{{trans('localize.cancelled')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <select class="form-control" id="sort" name="sort" style="font-family:'FontAwesome', sans-serif;">
                            <option value="id_asc" {{ ($input['sort'] == "id_desc") ? 'selected' : '' }}>{{trans('localize.#id')}} : &#xf162;</option>
                            <option value="id_desc" {{ ($input['sort'] == "" || $input['sort'] == "id_desc") ? 'selected' : '' }}>{{trans('localize.#id')}} : &#xf163;</option>
                            <option value="name_asc" {{ ($input['sort'] == "name_asc") ? 'selected' : '' }}>{{trans('localize.productName')}} : &#xf15d;</option>
                            <option value="name_desc" {{ ($input['sort'] == "name_desc") ? 'selected' : '' }}>{{trans('localize.productName')}} : &#xf15e;</option>
                            <option value="new" {{($input['sort'] == 'new') ? 'selected' : ''}}>{{trans('localize.newest')}}</option>
                            <option value="old" {{($input['sort'] == 'old') ? 'selected' : ''}}>{{trans('localize.oldest')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-block btn-outline btn-primary" id="filter">{{trans('localize.search')}}</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @include('admin.common.success')
            @include('admin.common.error')
            <div class="ibox">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">{{trans('localize.#id')}}</th>
                                    <th class="text-nowrap" width="60%">{{trans('localize.productName')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.service_name')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.store')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.details')}}</th>
                                    <th class="text-center text-nowrap" width="10%">{{trans('localize.action')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.status')}}</th>
                                    {{-- <th class="text-center text-nowrap">{{trans('localize.preview_live')}}</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($services as $key => $service)
                                    <tr class="text-center">
                                        <td>{{ $service->id}}</td>
                                        <td class="text-left">{{ $service->pro_title_en}}</td>
                                        <td class="text-nowrap">{{ $service->service_name_current }}</td>
                                        <td class="text-nowrap">{{ $service->stor_id ? $service->stor_name : trans('localize.store_unavailable') }}</td>
                                        {{--  <td>{{ $product->pro_no_of_purchase}}</td>  --}}
                                        <td class="text-left text-nowrap">
                                            <p>
                                                @lang('localize.type') :
                                                @if($service->pro_type == 1)
                                                    @lang('localize.normal_product')
                                                @elseif($service->pro_type == 2)
                                                    @lang('localize.coupon')
                                                @elseif($service->pro_type == 3)
                                                    @lang('localize.ticket')
                                                @elseif($service->pro_type == 4)
                                                    @lang('localize.e-card.name')
                                                @endif
                                            </p>
                                            <p>@lang('localize.quantity') : {{ $service->order_qty }}</p>
                                            <p>@lang('localize.order_id') : {{ $service->parent_order_id }}</p>
                                            <p>@lang('localize.transaction_id') : {{ $service->transaction_id }}</p>
                                        </td>
                                        <td class="text-nowrap">
                                            {{-- <p>
                                                <a class="btn btn-white btn-sm btn-block" href="{{ url($route . '/product/edit', [$service->pro_id]) }}"><span><i class="fa fa-edit"></i> {{trans('localize.edit')}}</span></a>
                                            </p> --}}
                                            <p>
                                                <a href="{{ url( 'admin/service/view', [$service->parent_order_id]) }}" class="btn btn-white btn-sm btn-block"><span><i class="fa fa-file-text-o"></i> {{trans('localize.view')}}</span></a>
                                            </p>
                                        </td>
                                        <td>
                                            @if (($service->status) == 1)
                                                <p>
                                                    <span class="text-nowrap text-navy"><i class='fa fa-calendar'></i> {{trans('localize.scheduled_by_member')}}</span>
                                                </p>
                                                <p>
                                                    <a href="{{ url('admin/service/confirm', [$service->id, 3]) }}" class="btn btn-white btn-sm btn-block"><span><i class="fa fa-file-text-o"></i> {{trans('localize.confirm')}}</span></a>
                                                </p>
                                                <p>
                                                    <a href="{{ url('admin/service/reschedule', [$service->id]) }}" class="btn btn-white btn-sm btn-block"><span><i class="fa fa-file-text-o"></i> {{trans('localize.reschedule')}}</span></a>
                                                </p>
                                            @elseif (($service->status) == 2)
                                                <p>
                                                    <span class="text-nowrap text-info"><i class='fa fa-calendar'></i> {{trans('localize.scheduled')}}</span>
                                                </p>
                                                <p>
                                                    <a href="{{ url('admin/service/reschedule', [$service->id]) }}" class="btn btn-white btn-sm btn-block"><span><i class="fa fa-file-text-o"></i> {{trans('localize.reschedule')}}</span></a>
                                                </p>
                                            @elseif (($service->status) == 0)
                                                <span class="text-nowrap text-warning"><i class='fa fa-warning'></i> {{trans('localize.unscheduled')}}</span>
                                            @elseif (($service->status) == 3)
                                                <p>
                                                    <span class="text-nowrap text-success"><i class='fa fa-check'></i> @lang('localize.confirmed')</span>
                                                </p>
                                                <p>
                                                    <a href="{{ url('admin/service/reschedule', [$service->id]) }}" class="btn btn-white btn-sm btn-block"><span><i class="fa fa-file-text-o"></i> {{trans('localize.reschedule')}}</span></a>
                                                </p>
                                            @elseif (($service->status) == -1)
                                                <span class="text-nowrap text-danger"><i class='fa fa-ban'></i> @lang('localize.cancelled')</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $services])

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
@endsection

@section('script')
<script src="/backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.js"></script>
<script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.min.js"></script>

<script>
    $(document).ready(function() {

        $(".focushover").popover({ trigger: "hover" });

        var link = $('#search_type').val();
        $.get('/'+link, function(data){
            $("#name").typeahead({
                source: data
            });
        },'json');

        $('#search_type a').on('click', function(e) {
            e.preventDefault();
            $("#search_type li").removeClass("select");
            $(this).closest('li').addClass('select');
            $('#search_type').val($(this).data('value'));

            var link = $(this).data('value');
            $("#name").typeahead('destroy');
            $.get('/'+link, function(data){
                $("#name").typeahead({
                    source: data
                });
            },'json');
        });

    });

    function set_value() {
        var link = $('#search_type').val();
        $("#name").typeahead('destroy');

        $.get('/'+link, function(data){
            $("#name").typeahead({
                source: data
            });
        },'json');
    }

</script>
@endsection
