@extends('admin.layouts.master')
@section('title', 'Reschedule Services')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{trans('localize.reschedule_service')}}</h2>
        <ol class="breadcrumb">
            <li>
                {{trans('localize.manage_service')}}
            </li>
            <li class="active">
                <strong>{{trans('localize.reschedule_service')}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        @include('admin.common.notifications')
        @include('front.common.warning')
    <div class="ibox-content m-b-sm border-bottom">
        <div class="main-content-container">
            <div class="order-top-layer shadow-sm mr-btm-sm">
                <div>
                    <h6>@lang('localize.order_id'): {{$service_details->parent_order_id}}</h6>
                    <p>{{$service_details->order_date}}</p>
                </div>
                <div class="mr-top-sm">
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#collapse_{{$service_details->order_id}}">
                                    {{$service_details->pro_title_en}}
                                </a>
                            </div>
                            <div id="collapse_{{$service_details->order_id}}" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="order-item">
                                        <div style="padding: 10px 15px;">
                                            <div class="product-layer">
                                                <div class="d-flex">
                                                        <a href="{{ route('product', ['url_slug' => $product_url_slug]) }}" target="_blank"><img class="product-image" src="{{ $service_details->mainImageUrl() }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/></a>
                                                    <div class="product-content">
                                                        <a target="_blank" href="{{ route('product', ['url_slug' => $product_url_slug]) }}"><h6 class="product-title">{{$service_details->pro_title_en}}</h6></a>
                                                        <p class="info">
                                                            @lang('localize.quantity'): <span class="red">{{$service_details->order_qty}}</span> | @lang('localize.total'): <span class="red">{{ rpFormat($service_details->product_price) }}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="schedule-layer">
                                                <div class="schedule-titlebar d-flex align-items-center">
                                                    <h5>{{$service_details->service_name_current}}</h5>
                                                </div>
                                                <form id="member_schedule_service" action="{{ route('admin-schedule-service-submit', ['member_service_schedule_id' => $service_details->id]) }}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="service_name" value="{{$service_details->service_name_current}}">
                                                <input type="hidden" id="schedule_skip_count" value="{{$service_details->schedule_skip_count}}">
                                                <input type="hidden" id="order_date" value="{{$service_details->order_date}}">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-item">
                                                                <label>@lang('localize.service_location')</label>
                                                                <div class="custom-input">
                                                                    <input type="text" id="stor_id_service_{{$service_details->id}}" value="{{$service_details->stor_name}}" data-service-id = "{{$service_details->id}}" data-appoint-start="{{$service_details->appoint_start}}" data-appoint-end="{{$service_details->appoint_end}}" data-interval-period="{{$service_details->interval_period}}" data-stor-id="{{$service_details->stor_id}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>@lang('localize.date')</label>
                                                                <div class='input-group date' id='schedule_datepicker_{{$service_details->id}}'>
                                                                    <input type='text' class="form-control" id="schedule_date_{{$service_details->id}}" name="schedule" data-service-id = "{{$service_details->id}}" value="{{old('schedule_date', $service_details->schedule_datetime ? date('d/m/Y', strtotime($service_details->schedule_datetime)) : '' ) }}" autocomplete="off"/>
                                                                    <button type="button" class="btn btn-red" > <i class="far fa-calendar-alt"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-item">
                                                                <label>@lang('localize.time')</label>
                                                                <div class="custom-radio-input" id="schedule_timeline_{{$service_details->id}}">
                                                                    <div id="schedule-radio-button_{{$service_details->id}}" ></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-item">
                                                                <label>@lang('localize.schedule_date')</label>
                                                                <div class="custom-input">
                                                                    <input type="text" name="schedule-date-time" id="schedule-date-time_{{$service_details->id}}" value="{{old('schedule-date-time', $service_details->schedule_datetime ? Carbon\Carbon::parse($service_details->schedule_datetime)->format('d/m/Y g:i A') : '' )}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-action">
                                                        @if($service_details->reschedule_count_merchant < 2)
                                                        <div class="form-item">
                                                            <label>@lang('localize.reschedule_count_remaining') = {{2-$service_details->reschedule_count_merchant}}</label>
                                                        </div>
                                                        {{-- @elseif($service_details->reschedule_count_merchant == 2)
                                                        <div class="form-item">
                                                            <label>@lang('localize.this_reschedule_will_cancel_the_service')</label>
                                                        </div>               --}}
                                                        @endif
                                                        @if($service_details->status == 0)
                                                        <button type="submit" class="btn btn-red" >
                                                            @lang('localize.confirm_schedule')<i class="material-icons">check</i>
                                                        </button>
                                                        @elseif($service_details->status == 1 && $service_details->reschedule_count_merchant == 2|| $service_details->status == 2 && $service_details->reschedule_count_merchant == 2)
                                                        <button type="submit" class="btn btn-warning" >
                                                            @lang('localize.cancel_schedule')<i class="material-icons">cancel</i>
                                                        </button>
                                                        @elseif($service_details->status == 1 || $service_details->status == 2)
                                                        <button type="submit" class="btn btn-warning" >
                                                            @lang('localize.reschedule')<i class="material-icons">update</i>
                                                        </button>
                                                        @elseif($service_details->status == 3)
                                                        <button type="submit" class="btn btn-red" disabled>
                                                            @lang('localize.confirmed')<i class="material-icons">done_outline</i>
                                                        </button>
                                                        @elseif($service_details->status == -1)
                                                        <button type="submit" class="btn btn-warning" disabled>
                                                            @lang('localize.cancelled')<i class="material-icons">cancel</i>
                                                        </button>
                                                        @endif
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
<link href="/asset/css/dashboard.css" rel="stylesheet">
<link href="/asset/css/default.css?v2" rel="stylesheet">
<link href="/asset/css/dropdown_bs4.css" rel="stylesheet">
<link href="/asset/css/master.css" rel="stylesheet">
<link href="/asset/css/custom_select.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/backend/css/plugins/daterangepicker/custom-daterangepicker.css" rel="stylesheet">
<link href="/web/lib/select2/css/select2.min.css" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" type="text/css" >
<link rel="stylesheet" href="{{ asset('asset/css/fontawesome/all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/font-awesome/css/font-awesome.css') }}">
<link href="/asset/css/merchant_reschedule.css?v1" rel="stylesheet">
{{-- <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap.css') }}"> --}}
<style>
.datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active:hover, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover {
    background-image: none;
    background-color: #dc3545;
}
.card-header:first-child {
    border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
.card-header {
    padding: 0.75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, 0.03);
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
}
.input-group {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: 100%;
}
.btn:not(:disabled):not(.disabled) {
    cursor: pointer;
}
.text-white {
    color: #fff !important;
}
.text-center {
    text-align: center !important;
}
.bg-danger {
    background-color: #dc3545 !important;
}
.bg-secondary {
    background-color: #6c757d !important;
}
</style>
@endsection

@section('script')
<script src="/backend/js/plugins/daterangepicker/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="/asset/js/dropdown_bs4.js"></script>
<script src="/asset/js/custom_select.js"></script>
<script src="/asset/js/zona.app.js"></script>
<script src="/web/lib/select2/js/select2.full.min.js"></script>
<script src="/backend/js/plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="/backend/js/plugins/daterangepicker/moment-duration-format.min.js"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var dateToday = new Date();
        var schedule_skip_count = $('#schedule_skip_count').val();
        var order_date = new Date(moment($('#order_date').val()).format("YYYY-MM-DD"));
        var buffer_date = new Date(order_date.setDate(order_date.getDate() + parseInt(schedule_skip_count)));
        var min_date = buffer_date;
     
        if (dateToday > buffer_date) {
            min_date = dateToday;
        }

        function formatState (state) {

            if (!state.id) {
            return state.text;
            }

            var $state = $(
            '<span style="color:black;font-weight:bold">' + state.text + '</span>'
            +'<span>' + '&nbsp;&nbsp;&nbsp;&nbsp;' + $(state.element).data('address') +'</span>'
            );
            return $state;
        };

        $(".store_list").select2({
            templateResult: formatState
        });
        
        $.each($("[id^=stor_id_service_]"), function(){

            var store_info = $(this);
            var service_id = store_info.attr('data-service-id');
        //    $("#schedule-radio-button_"+service_id).html("");
            // $('#schedule-date-time_'+service_id).val('');
            // $('#schedule_date_'+service_id).val('');


            var store_interval_period = store_info.attr('data-interval-period');
            var store_appoint_start = store_info.attr('data-appoint-start');
            var store_appoint_end = store_info.attr('data-appoint-end');

            var ms = moment(store_appoint_end,"HH:mm:ss").diff(moment(store_appoint_start,"HH:mm:ss"));
            var d = moment.duration(ms);
            var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

            var total_minutes = moment.duration(s).asMinutes();
            var stages = total_minutes / store_interval_period;

            var timeschedule_seconds = moment.duration(store_appoint_start).asSeconds();
            var duration = moment.duration(timeschedule_seconds, 'seconds');

            var timeschedule_array = [moment(duration.format("hh:mm:ss"), "hh:mm:ss").format("h:mm A")];

            for(i=0; i<stages-1; i++) {
                timeschedule_seconds += parseInt(store_interval_period*60);
                duration = moment.duration(timeschedule_seconds, 'seconds');

                // timeschedule_array[i+1] = duration.format("hh:mm:ss");
                timeschedule_array[i+1] = moment(duration.format("hh:mm:ss"), "hh:mm:ss").format("h:mm A");
            }
            
            var myArray = timeschedule_array;

            var wrapper = document.getElementById('schedule-radio-button_'+service_id);
            var time = $('#schedule-date-time_'+service_id).val().substring(11);

            var pick_date = new Date(moment($('#schedule_datepicker_'+service_id).find('input[name="schedule"]').val(), "DD/MM/YYYY").format("YYYY-MM-DD"));
            var current_date = new Date(moment(dateToday).format("YYYY-MM-DD"));

            for(var i = 0; i < myArray.length; i++) {
                var radio = document.createElement('input');
                var label = document.createElement('label');
                radio.type = 'radio';
                radio.name = 'timeslot';
                radio.value = myArray[i];
                radio.style = "visibility:hidden; display:none;";
                radio.setAttribute("data-toggle", "toggle");

                // label.setAttribute("href", "#");
                label.setAttribute("class", "btn btn-default text-center");
                label.setAttribute("name", "radio-label");
                label.setAttribute("style", "border:1px black solid; margin-right:2px; width:100px;")
                // label.setAttribute("data-toggle", "toggle");
                label.innerHTML = myArray[i];  

                if (pick_date < current_date) {

                    label.setAttribute("class", "btn btn-default text-center text-white bg-secondary");
                    radio.setAttribute("disabled","true");

                } else if (pick_date.getTime() === current_date.getTime()) {

                    var from = moment(dateToday).format('MM/DD/YYYY, h:mm:ss A');
                    var to = moment(dateToday).format('MM/DD/YYYY') + ' ' + myArray[i];

                    if (new Date(from).getTime() > new Date(to).getTime()) {
                        label.setAttribute("class", "btn btn-default text-center text-white bg-secondary");
                        radio.setAttribute("disabled","true");
                    }

                }

                if (time == myArray[i]) {
                    label.setAttribute("class", "btn btn-default text-center text-white bg-danger");
                    radio.setAttribute("checked","true");
                } 
                
                wrapper.appendChild(label);
                label.appendChild(radio);
            }
            var radios = $('#schedule-radio-button_'+service_id).find('input[name="timeslot"]');

            radios.change(function() {

                $('#schedule-radio-button_'+service_id).find('label[name=radio-label]').attr("class","btn btn-default text-center");
                $(this).closest("label").attr({"class": "btn btn-default text-center text-white bg-danger"});

                // $('#schedule-date-time').val('');
                var $checked = radios.filter(function() {
                    return $(this).prop('checked');
                });
                // $('#schedule-date-time_'+service_id).val($checked.val());
// console.log($('[id^=schedule_datepicker]').find('input[name="schedule"]').val());
                if ($('#schedule_datepicker_'+service_id).find('input[name="schedule"]').val() == '') {
                    $('#schedule-date-time_'+service_id).val($checked.val());
                } else {
                    $('#schedule-date-time_'+service_id).val($('#schedule_datepicker_'+service_id).find('input[name="schedule"]').val() + ' ' + $checked.val());
                }

                if ($('#schedule_datepicker_'+service_id).find('input[name="schedule"]').val() == moment(dateToday).format("DD/MM/YYYY")) {
                    for (i = 0; i < radios.length; ++i) {
                        var from = moment(dateToday).format('MM/DD/YYYY, h:mm:ss A');
                        var to = moment(dateToday).format('MM/DD/YYYY') + ' ' + radios[i].defaultValue;

                        if (new Date(from).getTime() > new Date(to).getTime()) {
                            $(radios[i]).closest("label").attr({"class": "btn btn-default text-center text-white bg-secondary"});
                            $(radios[i]).closest("input").prop('disabled', true);
                        }
                    }
                }
            });
            // $radios.val(time);
            // $radios.change();


        });

        $('[id^=schedule_datepicker]').datepicker({
            format:"dd/mm/yyyy",
            startDate: min_date
            // startDate: dateToday
        });
        
        var $datepicker = $('input[name="schedule"]');
        $datepicker.change(function(){
            var service_id = $(this).attr('data-service-id');
            var radios = $('#schedule-radio-button_'+service_id).find('input[name="timeslot"]');
            var pick_date = new Date(moment($('#schedule_datepicker_'+service_id).find('input[name="schedule"]').val(), "DD/MM/YYYY").format("YYYY-MM-DD"));
            var current_date = new Date(moment(dateToday).format("YYYY-MM-DD"));

            if (pick_date.getTime() == current_date.getTime()) {

                for (i = 0; i < radios.length; ++i) {
                    var from = moment(dateToday).format('MM/DD/YYYY, h:mm:ss A');
                    var to = moment(dateToday).format('MM/DD/YYYY') + ' ' + radios[i].defaultValue;

                    if (new Date(from).getTime() > new Date(to).getTime()) {
                        $(radios[i]).closest("label").attr({"class": "btn btn-default text-center text-white bg-secondary"});
                        $(radios[i]).closest("input").prop('disabled', true);
                    }
                }

            } else {

                for (i = 0; i < radios.length; ++i) {
                    $(radios[i]).closest("label").attr({"class": "btn btn-default text-center"});
                    $(radios[i]).closest("input").prop('disabled', false);
                }
            }

            // if (jQuery('input[type=radio][name=timeslot]', '#schedule-radio-button').length) {
            if ($('#schedule-radio-button_'+service_id).find("input[name='timeslot']:checked").val()) {
                $('#schedule-date-time_'+service_id).val($('#schedule_datepicker_'+service_id).find('input[name="schedule"]').val() + ' ' + $('#schedule-radio-button_'+service_id).find("input[name='timeslot']:checked").val());
            } else {
                $('#schedule-date-time_'+service_id).val($('#schedule_datepicker_'+service_id).find('input[name="schedule"]').val());
            }
        });
    });
</script>
@endsection

