@extends('admin.layouts.master')
@section('title', 'View Services')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{trans('localize.view_service')}}</h2>
        <ol class="breadcrumb">
            <li>
                {{trans('localize.view_service')}}
            </li>
            <li class="active">
                <strong>{{trans('localize.view_service')}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        @include('admin.common.notifications')
        @include('front.common.warning')
    <div class="ibox-content m-b-sm border-bottom">
        <div class="main-content-container">
            <div class="order-top-layer shadow-sm mr-btm-sm">
                <div>
                    {{-- <h6>@lang('localize.order_id'): {{$order_info->id}}</h6> --}}
                    <h6>@lang('localize.transaction_id'): {{$order_info->transaction_id}}</h6>
                    <p>{{$order_info->date}}</p>
                </div>
                <div class="mr-top-sm">
                    <div id="accordion">
                        @foreach($order_info->items as $o)
                        <div class="card panel">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#collapse_{{$o->order_id}}">
                                    {{$o->product->pro_title_en}}
                                </a>
                            </div>
                            <div id="collapse_{{$o->order_id}}" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="order-item">
                                        <div style="padding: 10px 15px;">
                                            {{-- <div class="top-layer">
                                                <p>@lang('localize.package_by') <a href="javascript:void(0)">{{$o->courier->name}}</a></p>
                                                <p>@lang('localize.tracking_number'): {{$o->order_tracking_no}}</p>
                                            </div> --}}
                                            <div class="product-layer">
                                                <div class="d-flex">
                                                    <div class="product-image-container">
                                                        <img class="product-image" src="{{ \Storage::disk('s3')->url('product/' . $o->product->pro_mr_id . '/' . $o->product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                                                    </div>
                                                    <div class="product-content">
                                                        @foreach($product_url_slug as $p => $key)
                                                            @if($p == $o->order_id)
                                                            <a target="_blank" href="/product/{{ Helper::slug_maker($o->product->pro_title_en, $o->product->pro_id) }}"><h6 class="product-title">{{$o->product->pro_title_en}}</h6></a>
                                                            @endif
                                                        @endforeach
                                                        <p class="info">
                                                            <label>@lang('localize.quantity'): <span class="red">{{$o->order_qty}}</span></label>
                                                            <label>@lang('localize.total'): <span class="red">{{rpFormat($o->product_price)}}</span></label>
                                                        </p>
                                                        <p class="info">
                                                            <label>@lang('localize.customer'): <span class="red">{{$o->customer->cus_name}}</span></label>
                                                        </p>
                                                        <p class="info">
                                                            <label>@lang('localize.contact_no'): <span class="red">{{$o->customer->phone_area_code . $o->customer->cus_phone}}</span></label>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($member_services[$o->order_id] as $m =>$keys)
                                            <div class="schedule-layer">
                                                <div class="schedule-titlebar d-flex align-items-center">
                                                    <h5>{{$keys->service_name_current}}</h5>
                                                </div>
                                                <input type="hidden" name="service_name" value="{{$keys->service_name_current}}">
                                                <div class="product-layer">
                                                    <div class="product-content cart-item-content">
                                                    <p class="info">
                                                        <label>@lang('localize.service_location'): <span class="red">{{$keys->stor_name}}</span></label>
                                                    </p>
                                                    <p class="info">
                                                        <label>@lang('localize.schedule_date'): <span class="red">{{$keys->schedule_datetime ? Carbon\Carbon::parse($keys->schedule_datetime)->format('d/m/Y g:i A') : 'Not Scheduled Yet'}}</span></label>
                                                    </p>
                                                    <p class="info">
                                                        @if($keys->status == 0)
                                                            <label>@lang('localize.status'): <span class="red">@lang('localize.unscheduled')</span></label>
                                                        @elseif($keys->status == 1)
                                                            <label>@lang('localize.status'): <span class="red"> @lang('localize.scheduled_by_member')</span></label>
                                                        @elseif($keys->status == 2)
                                                            <label>@lang('localize.status'): <span class="red">@lang('localize.scheduled_by_merchant')</span></label>
                                                        @elseif($keys->status == 3)
                                                            <label>@lang('localize.status'): <span class="red"> @lang('localize.confirmed')</span></label>
                                                        @elseif($keys->status == -1)
                                                            <label>@lang('localize.status'): <span class="red"> @lang('localize.cancelled')</span></label>
                                                        @endif

                                                        <label>@lang('localize.reschedule_count_remaining') [@lang('localize.merchant_user')]: <span class="red">{{2-$keys->reschedule_count_merchant}}</span></label>
                                                    </p>           
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
<link href="/asset/css/dashboard.css" rel="stylesheet">
<link href="/asset/css/default.css?v2" rel="stylesheet">
<link href="/asset/css/dropdown_bs4.css" rel="stylesheet">
<link href="/asset/css/master.css" rel="stylesheet">
<link href="/asset/css/custom_select.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/backend/css/plugins/daterangepicker/custom-daterangepicker.css" rel="stylesheet">
<link href="/web/lib/select2/css/select2.min.css" rel="stylesheet">
<link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" type="text/css" >
<link rel="stylesheet" href="{{ asset('asset/css/fontawesome/all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/font-awesome/css/font-awesome.css') }}">
<link href="/asset/css/merchant_reschedule.css?v1" rel="stylesheet">
{{-- <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap.css') }}"> --}}
<style>
.datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active:hover, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover {
    background-image: none;
    background-color: #dc3545;
}
.card-header:first-child {
    border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
.card-header {
    padding: 0.75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, 0.03);
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
}
.input-group {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: 100%;
}
.btn:not(:disabled):not(.disabled) {
    cursor: pointer;
}
.text-white {
    color: #fff !important;
}
.text-center {
    text-align: center !important;
}
.bg-danger {
    background-color: #dc3545 !important;
}
</style>
@endsection

@section('script')
<script src="/backend/js/plugins/daterangepicker/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="/asset/js/dropdown_bs4.js"></script>
<script src="/asset/js/custom_select.js"></script>
<script src="/asset/js/zona.app.js"></script>
<script src="/web/lib/select2/js/select2.full.min.js"></script>
<script src="/backend/js/plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="/backend/js/plugins/daterangepicker/moment-duration-format.min.js"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript">
    
</script>
@endsection

