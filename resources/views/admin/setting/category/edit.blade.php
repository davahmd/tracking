@extends('admin.layouts.master')
@section('title', trans('localize.edit') . ' ' . trans('localize.category'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.manage')}} {{trans('localize.Category')}}</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/setting/category/listing">@lang('localize.listing')</a>
            </li>
            @foreach ($breadcrumbs as $key => $category)
                @if ($key < count($breadcrumbs)-1)
                    <li class="breadcrumb-item">
                        <a href="/admin/setting/category/listing/{{$category->id}}">{{ $category->name }}</a>
                    </li>
                @else
                    <li class="breadcrumb-item active">
                        <strong>{{trans('localize.Edit')}} {{trans('localize.Category')}} : {{ $category->name }}</strong>
                    </li>
                @endif
            @endforeach
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{trans('localize.General_Info')}}</h5>
                </div>

                <div class="ibox-content">
                    @include('admin.common.success')
                    @include('admin.common.errors')
                    <form id='form' class="form-horizontal" action='/admin/setting/category/edit/{{$category->id}}' method="POST" enctype="multipart/form-data">
                         {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Name')}} ({{trans('localize.English')}})</label>
                            <div class="col-lg-10">
                                <input type="text" id="name-en" placeholder="{{trans('localize.Name')}} ({{trans('localize.English')}})" class="form-control compulsary" name='name_en'  value="{{empty(old('name_en'))?$category->name_en:old('name_en')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Name')}} ({{trans('localize.Indonesian')}})</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="{{trans('localize.Name')}} ({{trans('localize.Indonesian')}})" class="form-control" name='name_ind' value="{{empty(old('name_ind'))?$category->name_ind:old('name_ind')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.short_description')}} ({{trans('localize.English')}})</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="{{trans('localize.short_description')}} ({{trans('localize.English')}})" class="form-control compulsary" name='short_desc_en' value="{{empty(old('short_desc_en'))?$category->short_desc_en:old('short_desc_en')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.short_description')}} ({{trans('localize.Indonesian')}})</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="{{trans('localize.short_description')}} ({{trans('localize.Indonesian')}})" class="form-control" name='short_desc_ind' value="{{empty(old('short_desc_ind'))?$category->short_desc_ind:old('short_desc_ind')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Url_Slug')}}</label>
                            <div class="col-lg-10">
                                <input type="text" id="url-slug" placeholder="{{ trans('localize.Url_Slug') }}" class="form-control" name='url_slug' value="{{ empty(old('url_slug'))?$category->url_slug:old('url_slug')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">@lang('localize.code') <span style="color:red;">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="@lang('localize.code')" class="form-control compulsary" name='code' value="{{ old('code', $category->code) }}">
                                <label style="margin-top: 3px;"><span style="color:#337ab7;">invoice purpose</span></label>
                            </div>
                        </div>
                        <div class="if_featured">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.image')}} </label>
                                <div class="col-lg-10">
                                    <div class="col-xs-5 col-md-3">
                                        <img src="{{ \Storage::url('category/image/' . $category->image) }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive">
                                    </div>
                                    <div class="col-xs-7 col-md-9">
                                        <input type="file" class="form-control" name="image">
                                        <label style="margin-top: 3px;"><span style="color:#337ab7;">{{trans('localize.Best_size_for_image')}} : 160 x 90</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.Banner')}} </label>
                                <div class="col-lg-10">
                                    <div class="col-xs-5 col-md-3">
                                        <img src="{{ \Storage::url('category/banner/' . $category->banner) }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive">
                                    </div>
                                    <div class="col-xs-7 col-md-9">
                                        <input type="file" class="form-control" name="banner">
                                        <label style="margin-top: 3px;"><span style="color:#337ab7;">{{trans('localize.Best_size_for_banner')}} : 1022 x 244</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.icon')}} </label>
                                <div class="col-lg-10">
                                    <div class="col-xs-5 col-md-3">
                                        <img src="{{ \Storage::url('category/icon/' . $category->icon) }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive">
                                    </div>
                                    <div class="col-xs-7 col-md-9">
                                        <input type="file" class="form-control" name="icon">
                                        <label style="margin-top: 3px;"><span style="color:#337ab7;">{{trans('localize.Best_size_for_icon')}} : 64 x 64</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Featured')}} </label>
                            <div class="col-lg-10">
                                <div class="i-checks">
                                    <label>
                                        <input type="radio" value="1" name="featured" {{(empty(old('featured')) && $category->featured) || old('featured')=='1'?'checked':''}} > <i></i> {{trans('localize.Yes')}}
                                    </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="radio" value="0" name="featured" {{(empty(old('featured')) && !$category->featured) || old('featured')=='0'?'checked':''}}> <i></i> {{trans('localize.No')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Status')}} </label>
                            <div class="col-lg-10">
                                <div class="i-checks">
                                    <label>
                                        <input type="radio" value="1" name="status" {{(empty(old('status')) && $category->status) || old('status')=='1'?'checked':''}} > <i></i> {{trans('localize.Active')}}
                                    </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="radio" value="0" name="status" {{(empty(old('status')) && !$category->status) || old('status')=='0'?'checked':''}}> <i></i> {{trans('localize.Inactive')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                @if($edit_permission)
                                <button class="btn btn-sm btn-primary form_submit" type="button">{{trans('localize.Update')}}</button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')

<script>
$(document).ready(function() {

    $('.form_submit').click(function() {
        var isValid = true;

        $(':input').each(function(e) {
            if ($(this).hasClass('compulsary')) {
                if (!$(this).val()) {
                    $(this).attr('placeholder', '{{trans('localize.fieldrequired')}}').css('border', '1px solid red').focus();
                    isValid = false;
                    return false;
                }
            }

            $(this).css('border', '');
        });

        if (isValid) {
            $("#form").submit();
        }
    });

    $('#name-en').blur(function(){
        if($('#url-slug').val() == ''){
            var slug = $('#name-en').val().toLowerCase().trim().replace(/&/g, 'and').replace(/\//g, '-').replace(/ /g, '-');
            $('#url-slug').val(slug);
        }
    });

});
</script>
@endsection