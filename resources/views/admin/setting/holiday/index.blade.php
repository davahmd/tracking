@extends('admin.layouts.master')
@section('title', 'Manage Calendar')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/fullcalendar.css') }}">
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('backend/css/fullcalendar.print.css') }}">
<style type="text/css">
td.fc-sat, td.fc-sun {
    background-color: rgba(255, 0, 0, 0.2);
}

</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.calendar')}}</h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            @include('admin.common.success')
            @include('admin.common.errors')      
            <div class="ibox">
                <div class="ibox-content">
                    <div class="container text-center" style="height: 500px; width: auto">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-title" style="text-transform: capitalize;">
                    @lang('calendar.holiday-list')
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered table-hovered">
                        <thead>
                            <tr>
                                <th width="50%">@lang('localize.name')</th>
                                <th width="50%">@lang('localize.date')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($holidays as $holiday)
                            <tr>
                                <td>{{ $holiday->title }}</td>
                                @if(date('d-m-Y', strtotime($holiday->start)) == date('d-m-Y',strtotime($holiday->end . "-1 days")))
                                <td>{{ date('d-m-Y', strtotime($holiday->start)) }}</td>
                                @else
                                <td>{{ date('d-m-Y', strtotime($holiday->start)) }} - {{ date('d-m-Y',strtotime($holiday->end . "-1 days")) }}</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-holiday">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal" id="add-holiday-form" action='/admin/setting/holiday/add' method="POST">
        {{ csrf_field() }}
        <div class="modal-header">
            <h4 class="modal-title" style="text-transform: capitalize;"><strong>@lang('calendar.add-holiday')</strong></h4>
        </div>
        
        <div class="modal-body">
            <div class="form-group">
                <label class="col-md-2 control-label">@lang('localize.name')</label>
                <div class="col-md-10">
                    <input type="text" placeholder="etc: {{ trans('calendar.holidays.labor-day') }}..." class="form-control" id="name" name='name' >
                    <input type="hidden" name="start_date" id="start-date">
                    <input type="hidden" name="end_date" id="end-date">
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="form-group">
                <div class="col-md-3 pull-right">
                    <button class="btn btn-block btn-primary" id="add" type="button">@lang('localize.add')</button>
                </div>
                <div class="col-md-3 pull-right">
                    <button class="btn btn-block btn-default" id="cancel" type="button">@lang('localize.cancel')</button>
                </div>
            </div>
        </div>
        </form>
    </div>
  </div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="{{ asset('backend/lib/fullcalendar/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/fullcalendar/fullcalendar.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/fullcalendar/locale-all.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootbox.min.js') }}"></script>
<script type="text/javascript">

    var occupied = false;
    var locale = "{{ config('app.locale') }}";

    if (locale == "ind") {
        locale = 'id';
    }
    
    $('#calendar').fullCalendar({
        locale: locale,
        header: {
            left: '',
            center: 'title',
            right: 'prev,next today',
        },
        defaultDate: '{{ date("Y-m-d") }}',
        height: 'parent',
        aspectRation: 1.5,
        selectable: true,
        events: <?php echo $holidays_json; ?>,
        selectOverlap: function(event) {
            
            if (typeof(event) != 'undefined') {

                occupied = true;

                swal({
                    title: "{{ trans('localize.error') }}",
                    text: "{{ trans('calendar.error.overlap') }}",
                    type: "error",
                });

                return false;
            }

        },
        dayClick: function(date) {

            if (!occupied) {
                $('#start-date').val(date.format('Y-MM-DD'));
                $('#end-date').val(date.format('Y-MM-DD'));
                $('#add-holiday').modal('show');
            }

        },
        select: function(start,end) {
            $('#start-date').val(start.format('Y-MM-DD'));
            $('#end-date').val(end.format('Y-MM-DD'));
            $('#add-holiday').modal('show');
        },
        eventClick: function(event) {
            swal({
                title: event.title,
                text: "{{ trans('calendar.warning.delete-holiday') }}",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "OK",
                cancelButtonText: "{{ trans('localize.cancel') }}",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    window.location.replace("/admin/setting/holiday/remove/" + event.id);
                }
            });
        },
    });

    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#cancel').click(function(){
        $('#start-date').val("");
        $('#end-date').val("");
        $('#add-holiday').modal('hide');
    });

    $('#add').click(function(){
        $('#add-holiday-form').submit();
    });

</script>
@endsection