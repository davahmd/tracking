@extends('admin.layouts.master')
@section('title', trans('localize.manage') . ' ' . trans('localize.vehicle'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.manage')}} {{trans('localize.vehicle')}}</h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" id="filter" action='/admin/setting/vehicle' method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.Brand')}}</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{$input['brand']}}" placeholder="{{trans('localize.Brand')}}" class="form-control" name="brand">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.Model')}}</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{$input['model']}}" placeholder="{{trans('localize.Model')}}" class="form-control" name="model">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.Variant')}}</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{$input['variant']}}" placeholder="{{trans('localize.Variant')}}" class="form-control" name="variant">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.Year')}}</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{$input['year']}}" placeholder="{{trans('localize.Year')}}" class="form-control" name="year">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.sort')}}</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="sort" name="sort" style="font-family:'FontAwesome', sans-serif;">
                            		<option value="brand_asc" {{ ($input['sort'] == 'brand_asc') ? 'selected':'' }}>{{trans('localize.Brand')}} {{trans('localize.Name')}} : &#xf15d;</option>
                            		<option value="brand_desc" {{ ($input['sort'] == 'brand_desc') ? 'selected':'' }}>{{trans('localize.Brand')}} {{trans('localize.Name')}} : &#xf15e;</option>
                            		<option value="model_asc" {{ ($input['sort'] == 'model_asc') ? 'selected':'' }}>{{trans('localize.Model')}} {{trans('localize.Name')}} : &#xf15d;</option>
                            		<option value="model_desc" {{ ($input['sort'] == 'model_desc') ? 'selected':'' }}>{{trans('localize.Model')}} {{trans('localize.Name')}} : &#xf15e;</option>
                            		<option value="variant_asc" {{ ($input['sort'] == 'variant_asc') ? 'selected':'' }}>{{trans('localize.Variant')}} {{trans('localize.Name')}} : &#xf15d;</option>
                            		<option value="variant_desc" {{ ($input['sort'] == 'variant_desc') ? 'selected':'' }}>{{trans('localize.Variant')}} {{trans('localize.Name')}} : &#xf15e;</option>
                            		<option value="year_asc" {{ ($input['sort'] == 'year_asc') ? 'selected':'' }}>{{trans('localize.Year')}} {{trans('localize.Name')}} : &#xf15d;</option>
                            		<option value="year_desc" {{ ($input['sort'] == 'year_desc') ? 'selected':'' }}>{{trans('localize.Year')}} {{trans('localize.Name')}} : &#xf15e;</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary" id="filter">{{trans('localize.search')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                @include('admin.common.success')
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            @if($add_permission)
                            <a href="/admin/setting/vehicle/add" class="btn btn-primary btn-xs">{{trans('localize.create_new_vehicle')}}</a>
                            @endif
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="footable table table-stripped" data-page-size="20" data-filter=#filter>
                                <thead>
                                    <tr>
	                                		<th>{{trans('localize.Brand')}}</th>
	                                		<th>{{trans('localize.Model')}}</th>
	                                		<th>{{trans('localize.Variant')}}</th>
	                                		<th>{{trans('localize.Year')}}</th>
	                                		<th>{{trans('localize.Image')}}</th>
	                                    <th data-hide="phone" data-sort-ignore="true">{{trans('localize.Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($vehicles as $vehicle)
                                    <tr>
                                        <td>{{$vehicle->brand}}</td>
                                        <td>{{$vehicle->model}}</td>
                                        <td>{{$vehicle->variant}}</td>
                                        <td>{{$vehicle->year}}</td>
                                        <td>
                                        	@if($vehicle->vehicle_image)
                                        	<i class="fa fa-check"></i>
                                        	@endif
                                        </td>
                                        <td>
                                            <a href="/admin/setting/vehicle/edit/{{$vehicle->id}}" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> {{trans('localize.Edit')}} </a>
                                            @if($delete_permission)
                                            <button type="button" class="btn btn-white btn-sm button_delete" data-id="{{ $vehicle->id }}" data-post="data-php"><i class="fa fa-close"></i> {{trans('localize.Delete')}}</button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                                @include('layouts.partials.table-pagination', ['listings' => $vehicles])

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {

        // $('.footable').footable();

        $('.button_delete').on('click', function(){
            var this_id = $(this).attr('data-id');
            swal({
                title: "{{trans('localize.sure')}}",
                text: "{{trans('localize.Confirm_to_delete_this_vehicle')}}",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "{{trans('localize.yes_delete_it')}}",
                cancelButtonText: "{{trans('localize.cancel')}}",
                closeOnConfirm: false
            }, function(){
                var url = '/admin/setting/vehicle/delete/' + this_id;
                window.location.href = url;
            });
        });

    });
</script>
@endsection
