@extends('admin.layouts.master')
@section('title', trans('localize.edit') . ' ' . trans('localize.vehicle'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.Edit')}} {{trans('localize.vehicle')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">{{trans('localize.dashboard')}}</a>
            </li>
            <li>
                <a href="/admin/setting/vehicle">{{trans('localize.vehicle')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.Edit')}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{trans('localize.General_Info')}}</h5>
                </div>

                <div class="ibox-content">
                    @include('admin.common.success')
                    @include('admin.common.errors')
                    <form id='form' class="form-horizontal" action='/admin/setting/vehicle/edit/{{$vehicle->id}}' method="POST" enctype="multipart/form-data">
                         {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Brand')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control js-option-tags" name="brand">
                                	@foreach($brands as $brand)
                                	<option value="{{$brand}}" {{($vehicle->brand == $brand) ?'selected':''}}>{{$brand}}</option>
                                	@endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Model')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control js-option-tags" name="model">
                                	@foreach($models as $model)
                                	<option value="{{$model}}" {{($vehicle->model == $model) ?'selected':''}}>{{$model}}</option>
                                	@endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Variant')}}</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="{{trans('localize.Variant')}}" class="form-control compulsary" name='variant' value='{{$vehicle->variant}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Year')}}</label>
                            <div class="col-lg-10">
                        		<select class="form-control js-option-tags" name="year">
                        			@foreach($years as $year)
                        			<option value="{{$year}}" {{($vehicle->year == $year) ?'selected':''}}>{{$year}}</option>
                        			@endforeach
                        		</select>
                            </div>
                        </div>
                        <div class="form-group">
                        	<label class="col-lg-2 control-label">{{trans('localize.Image')}}</label>
                        	<div class="col-lg-10">
                        		@if($vehicle->vehicle_image)
	                        	<div id="vehicle-image-div">
	                        		<img class="img-responsive" src="{{ Storage::url('gallery/vehicle/'.$vehicle->vehicle_image) }}">
	                        		<button type="button" class="btn btn-sm btn-primary btn-outline pull-right">{{trans('localize.edit_image')}}</button>
	                        	</div>
	                        	@endif
	                        	<div id="upload-image-div" class="@if($vehicle->vehicle_image) hidden @endif">
	                        		<input type="file" class="form-control-file" name="image">
	                        		@if($vehicle->vehicle_image)
	                        		<button type="button" class="btn btn-sm btn-primary btn-outline pull-right">{{trans('localize.show_image')}}</button>
	                        		@endif
	                        	</div>
                        	</div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                @if($edit_permission)
                                <button class="btn btn-sm btn-primary form_submit" type="button">{{trans('localize.Update')}}</button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('script')
<script>
    $(document).ready(function() {

    	$('input[name="image"]').fileinput();

    	$('.js-option-tags').select2({
	    	tags: true
	    });

	    $('#vehicle-image-div button').click(function() {
	    	$('#vehicle-image-div').addClass('hidden');
	    	$('#upload-image-div').removeClass('hidden');
	    });

	    $('#upload-image-div button').click(function() {
	    	$('#vehicle-image-div').removeClass('hidden');
	    	$('#upload-image-div').addClass('hidden');
	    });

		$('.form_submit').click(function() {
                var isValid = true;

                $(':input').each(function(e) {
                    if ($(this).hasClass('compulsary')) {
                        if (!$(this).val()) {
                            $(this).attr('placeholder', '{{trans('localize.fieldrequired')}}').css('border', '1px solid red').focus();
                            isValid = false;
                            return false;
                        }
                    }

                    $(this).css('border', '');
                });

                if (isValid) {
                    $("#form").submit();
                }
            });
    });
</script>
@endsection