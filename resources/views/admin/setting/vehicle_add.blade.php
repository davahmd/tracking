@extends('admin.layouts.master')
@section('title', trans('localize.add') . ' ' . trans('localize.vehicle'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.create_new_vehicle')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">{{trans('localize.dashboard')}}</a>
            </li>
            <li>
                <a href="/admin/setting/vehicle">{{trans('localize.vehicle')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.Create')}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{trans('localize.General_Info')}}</h5>
                </div>

                <div class="ibox-content">
                    @include('admin.common.errors')
                    <form id='form' class="form-horizontal" action='/admin/setting/vehicle/add' method="POST" enctype="multipart/form-data">
                         {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Brand')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control js-option-tags" name="brand">
                                	@foreach($brands as $brand)
                                	<option value="{{$brand}}" {{(old('brand') == $brand)?'selected':''}}>{{$brand}}</option>
                                	@endforeach
                                </select>
                            </div>
                        </div>
                        <div id="logo-upload-div" class="form-group @if(!Session::has('logo')) hidden @endif">
                            <label class="col-lg-2 control-label">{{trans('localize.Logo')}}</label>
                            <div class="col-lg-10">
                                <input type="file" class="form-control-file" name="logo">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Model')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control js-option-tags" name="model">
                                	@foreach($models as $model)
                                	<option value="{{$model}}" {{(old('model') == $model)?'selected':''}}>{{$model}}</option>
                                	@endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Variant')}}</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="{{trans('localize.Variant')}}" class="form-control compulsary" name='variant' value='{{old('variant')}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Year')}}</label>
                            <div class="col-lg-10">
                        		<select class="form-control js-option-tags" name="year">
                        			@foreach($years as $year)
                        			<option value="{{$year}}" {{(old('year') == $year)?'selected':''}}>{{$year}}</option>
                        			@endforeach
                        		</select>
                            </div>
                        </div>
                        <div class="form-group">
                        	<label class="col-lg-2 control-label">{{trans('localize.Image')}}</label>
                        	<div class="col-lg-10">
	                        	<div id="upload-image-div">
	                        		<input type="file" class="form-control-file" name="image">
	                        	</div>
                        	</div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-sm btn-primary form_submit" type="button">{{trans('localize.Create')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('script')
<script>
    $(document).ready(function() {

    var brands = {!!$brands!!};

    $('select[name="brand"]').change(function(e) {

    	var selectedBrand = this.options[e.target.selectedIndex].text

    	if (jQuery.inArray(selectedBrand, brands) != '-1') {
    		$('#logo-upload-div').addClass('hidden');
    	} else {
    		$('#logo-upload-div').removeClass('hidden');
    	}

    });

    $('#logo-upload-div input').fileinput();
    $('#upload-image-div input').fileinput();

    // function formatVehicle (vehicle) {
    // 	if (!vehicle.id) {
    // 		return vehicle.text;
    // 	}
    // 	var baseUrl = '/public/assets/images/vehicle';
    // 	var $vehicle = $(
    // 		'<span><img src="' + baseUrl + '/' + vehicle.element.value.toLowerCase() + '/' + vehicle.element.value.toLowerCase() + '.png" class="img-flag" /> ' + vehicle.text + '</span>'
    // 	);
    // 	return $vehicle;
    // };

    // $('.js-brand-templating').select2({
    // 	templateResult: formatVehicle
    // });

    $('.js-option-tags').select2({
    	tags: true
    });

		$('.form_submit').click(function() {
                var isValid = true;

                $(':input').each(function(e) {
                    if ($(this).hasClass('compulsary')) {
                        if (!$(this).val()) {
                            $(this).attr('placeholder', '{{trans('localize.fieldrequired')}}').css('border', '1px solid red').focus();
                            isValid = false;
                            return false;
                        }
                    }

                    $(this).css('border', '');
                });

                if (isValid) {
                    $("#form").submit();
                }
            });
    });
</script>
@endsection