@extends('admin.layouts.master')

@section('title', trans('localize.complain.listing.page_title_subject'))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>{{trans('localize.complain.listing.page_title_subject')}}</h2>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                @include('admin.common.success')
                @include('admin.common.status')
                @include('admin.common.error')

                <div class="ibox">

                    <div class="ibox-title" style="display: block;">
                        <div class="ibox-tools" style="margin-bottom:10px;">
                            @if($add_permission)
                            <a href="{{ route('setting.complain::create-title') }}" class="btn btn-primary btn-xs">{{trans('localize.complain.listing.create_new_title')}}</a>
                            @endif
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="{{trans('localize.Search_in_table')}}">
                            <table class="footable table table-stripped" data-page-size="20" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th class="text-center text-nowrap" width="5%">#ID</th>
                                    <th class="text-nowrap" width="60%">@lang('localize.complain.table.title')</th>
                                    <th class="text-center text-nowrap" width="10%">@lang('localize.complain.table.no_of_complaints')</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($titles->isEmpty())
                                    <tr>
                                        <td colspan="6">@lang('localize.table_no_record')</td>
                                    </tr>
                                @endif

                                @foreach ($titles as $title)
                                    <tr>
                                        <td>{{$title->getKey()}}</td>
                                        <td>
                                            {{$title->title}}
                                        </td>
                                        <td class="text-center">
                                            {{$title->complains_count}}
                                        </td>
                                        <td class="text-center">
                                            @if ($edit_permission)
                                                <a class="btn btn-white btn-sm button_delete"
                                                   href="{{ route('setting.complain::edit-title', [$title]) }}">
                                                    <span><i class="fa fa-pencil"></i> @lang('localize.edit')</span>
                                                </a>
                                            @endif

                                            @if ($delete_permission)
                                                <a class="btn btn-white btn-sm button_delete"
                                                   href="javascript:void(0);" onclick="if (confirm(window.translations.sure)) {console.log($(this).next('form').submit())}">
                                                    <span><i class="fa fa-close"></i> @lang('localize.delete')</span>
                                                </a>
                                                <form action="{{ route('setting.complain::post-delete-title', [$title]) }}" method="post">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
    <script src="/backend/js/plugins/footable/footable.all.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.footable').footable();
        });
    </script>
@endsection