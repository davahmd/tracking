@extends('admin.layouts.master')
@section('title', trans('localize.add') . ' ' . trans('localize.complain.complain_subject'))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>{{trans('localize.complain.form.subject_title_add')}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/admin">{{trans('localize.dashboard')}}</a>
                </li>
                <li>
                    <a href="{{ route('setting.complain::titles') }}">{{trans('localize.complain.complain_subject')}}</a>
                </li>
                <li class="active">
                    <strong>{{trans('localize.Create')}}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @include('admin.common.errors')
            @include('admin.common.success')
            <div class="col-lg-12">
                <div class="tabs-container">
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <form id='form' class="form-horizontal" action='{{ route('setting.complain::post-create-title') }}' method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="col-lg-12 no-spacing">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h5>@lang('localize.General_Info')</h5>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">{{trans('localize.Title')}}</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" placeholder="{{trans('localize.Title')}}" class="form-control compulsary" name='title' value='{{old('title')}}'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-6 pull-right">
                                                    <button class="btn btn-block btn-primary form_submit" type="button">{{trans('localize.Create')}}</button>
                                                </div>
                                                <div class="col-lg-6 pull-right">
                                                    <button type="button" onclick="reset()" class="btn btn-block btn-default">{{trans('localize.reset')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script>
        $(document).ready(function() {

            $('.form_submit').click(function() {
                var isValid = true;

                $(':input').each(function(e) {
                    if ($(this).hasClass('compulsary')) {
                        if (!$(this).val()) {
                            $(this).attr('placeholder', '{{trans('localize.fieldrequired')}}').css('border', '1px solid red').focus();
                            isValid = false;
                            return false;
                        }
                    }

                    $(this).css('border', '');
                });

                if (isValid) {
                    $("#form").submit();
                }
            });
        });
    </script>
@endsection