@extends('admin.layouts.master')

@section('title', trans('localize.edit') . ' ' . trans('localize.official_brands'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.edit_official_brands')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/setting/officialbrands/manage">{{trans('localize.official_brands')}}</a>
            </li>
            <li>
                {{trans('localize.Edit')}}
            </li>
            <li class="active">
                <strong>{{$officialbrand->brand_id}} - {{$officialbrand->brand_name}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    @include('admin.common.notifications')

    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <form class="form" id="edit_brand" action="{{ url('admin/setting/officialbrands/edit', [$officialbrand->brand_id]) }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="col-lg-12 no-spacing">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h5>@lang('localize.official_brands_info')</h5>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <label class="control-label">@lang('localize.brands_name')</label>
                                                        <input type="text" class="form-control" name="brands_name" value="{{ old('brands_name', $officialbrand->brand_name) }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">{{trans('localize.logo')}} </label>
                                                        <div class="row">
                                                            <div class="col-xs-5 col-md-3">
                                                                <img src="{{ $officialbrand->logoPath }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive">
                                                            </div>
                                                            <div class="col-xs-7 col-md-9">
                                                                <span class='btn btn-default btn-block'><input type='file' id='file' name='file' class="files"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">{{trans('localize.description')}}</label>
                                                        <textarea id="brand_desc" placeholder="{{trans('localize.description')}}" class="form-control"  name='brand_desc' rows="20">{{ old('brand_desc', $officialbrand->brand_desc) }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-12 no-spacing">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h5>@lang('localize.meta_info')</h5>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <label class="control-label">{{trans('localize.mer_meta_keyword')}}</label>
                                                        <input type="text" class="form-control" placeholder="{{trans('localize.mer_meta_keyword')}}" name='metakeyword' id='metakeyword' value="{{ old('metakeyword', $officialbrand->brand_mkeywords) }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">{{trans('localize.mer_meta_description')}}</label>
                                                        <textarea rows="8" class="form-control text-noresize" placeholder="{{trans('localize.mer_meta_description')}}" name='metadescription' id='metadescription'>{{ old('metadescription', $officialbrand->brand_mdesc) }}</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">{{trans('localize.slug')}}</label>
                                                        <input type="text" class="form-control" placeholder="{{trans('localize.slug')}}" name='slug' id='slug' value="{{ old('slug', $officialbrand->brand_slug) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-6 pull-right">
                                                <button class="btn btn-block btn-primary" id="submit">{{trans('localize.update')}}</button>
                                            </div>
                                            <div class="col-lg-6 pull-right">
                                                <button type="button" onclick="reset()" class="btn btn-block btn-default">{{trans('localize.reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.files').change(function() {
            var fileExtension = ['jpeg', 'jpg', 'png'];

            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                swal("@lang('localize.swal_error')", "{{trans('localize.imgError')}}", "error");
                $(this).css('border', '1px solid red').focus().val('');
            } else if (($(this)[0].files[0].size) > 1000000){
                swal("@lang('localize.swal_error')", "{{trans('localize.imgSizeError')}}", "error");
                $(this).css('border', '1px solid red').focus().val('');
            }
        });

        $('#submit,#submit_top').click(function() {

            $(':input').each(function(e) {
                if ($(this).hasClass('compulsary')) {
                    if (!$(this).val() || $(this).val() == 0) {
                        if($(this).is('textarea')) {
                            swal("@lang('localize.swal_error')", "{{trans('localize.fieldrequired')}}\n{{trans('localize.description_english')}}", "error");
                        } else {
                            $(this).attr('placeholder', '{{trans('localize.fieldrequired')}}').css('border', '1px solid red').focus();
                        }
                        $(this).closest('.form-group').addClass('has-error');
                        event.preventDefault();
                        return false;
                    }
                }

                if ($(this).hasClass('files')) {
                    var fileExtension = ['jpeg', 'jpg', 'png'];
                    if($(this).val()) {
                        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                            swal("@lang('localize.swal_error')", "{{trans('localize.imgError')}}", "error");
                            $(this).css('border', '1px solid red').focus();
                            event.preventDefault();
                            return false;
                        } else if (($(this)[0].files[0].size) > 1000000){
                            swal("@lang('localize.swal_error')", "{{trans('localize.imgSizeError')}}", "error");
                            $(this).css('border', '1px solid red').focus();
                            event.preventDefault();
                            return false;
                        }
                    }
                }

                $(this).css('border', '');
                $(this).closest('.form-group').removeClass('has-error');
            });

            $("#add_product").submit();
        });
    });
</script>
@endsection
