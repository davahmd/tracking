@extends('admin.layouts.master') @section('title', trans('localize.view') . ' ' . trans('localize.official_brands')) @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.view')}} {{trans('localize.official_brands')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/setting/officialbrands/manage">{{trans('localize.official_brands')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.view')}} {{trans('localize.official_brands')}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{trans('localize.official_brands_info')}}</h5>
                    <div class="ibox-tools">
                        <a href="/admin/setting/officialbrands/edit/{{$officialbrand->brand_id}}" class="btn btn-primary btn-sm">{{trans('localize.Edit')}} {{trans('localize.official_brands')}}</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.official_brands')}} {{trans('localize.name')}}</label>
                            <div class="col-lg-10"><p class="form-control-static">{{$officialbrand->brand_name}}</p></div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.official_brands')}} {{trans('localize.mer_meta_keyword')}}</label>
                            <div class="col-lg-10"><p class="form-control-static">{{$officialbrand->brand_mkeywords}}</p></div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.official_brands')}} {{trans('localize.mer_meta_description')}}</label>
                            <div class="col-lg-10"><p class="form-control-static">{{$officialbrand->brand_mdesc}}</p></div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.official_brands')}} {{trans('localize.slug')}} </label>
                            <div class="col-lg-10"><p class="form-control-static">{{$officialbrand->brand_slug}}</p></div>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Description')}} </label>
                            <div class="col-lg-10"><p class="form-control-static">{!! $officialbrand->brand_desc !!}</p></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('localize.official_brands')}} {{trans('localize.logo')}}</label>
                            <div class="col-lg-10">
                                <img class="img-responsive img-thumbnail" src="{{ $officialbrand->logoPath }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive img-thumbnail">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop