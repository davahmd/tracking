@extends('admin.layouts.master')

@section('title', trans('localize.manage') . ' ' . trans('localize.official_brands'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.manage')}} {{trans('localize.official_brands')}}</h2>
        <ol class="breadcrumb">
            <li>
                {{trans('localize.official_brands')}}
            </li>
            <li class="active">
                <strong>{{trans('localize.manage')}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" id="filter" action='/admin/setting/officialbrands/manage' method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.official_brands')}} ID</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{$input['id']}}" placeholder="{{trans('localize.Search_By_OfficialBrand_ID')}}" class="form-control number" id="id" name="id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.official_brands')}} Name</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{$input['name']}}" placeholder="{{trans('localize.Search_by_OfficialBrand_Name')}}" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.Status')}}</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="status" name="status">
                                <option value="" {{ ($input['status'] == "") ? 'selected' : '' }}>{{trans('localize.All')}}</option>
                                <option value="1" {{ ($input['status'] == "1") ? 'selected' : '' }}>{{trans('localize.Active')}}</option>
                                <option value="0" {{ ($input['status'] == "0") ? 'selected' : '' }}>{{trans('localize.Blocked')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{trans('localize.sort')}}</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="sort" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="id_asc" {{ ($input['sort'] == "id_desc") ? 'selected' : '' }}> #ID : &#xf162;</option>
                                <option value="id_desc" {{ ($input['sort'] == "" || $input['sort'] == "id_desc") ? 'selected' : '' }}>#ID : &#xf163;</option>
                                <option value="new" {{($input['sort'] == 'new') ? 'selected' : ''}}>{{trans('localize.Newest')}}</option>
                                <option value="old" {{($input['sort'] == 'old') ? 'selected' : ''}}>{{trans('localize.Oldest')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary" id="filter">{{trans('localize.search')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('admin.common.success')
            @include('admin.common.status')
            @include('admin.common.error')
            <div class="ibox">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        @if(in_array('settingofficialbrandscreate',$admin_permission))
                        <a href="{{ url('/admin/setting/officialbrands/add') }}" class="btn btn-primary btn-xs">{{trans('localize.add_official_brands')}}</a>
                        @endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">#ID</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Name')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.mer_meta_keyword')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.mer_meta_description')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.slug')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.created_date')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.image')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Action')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.Status')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($officialbrands as $officialbrand)
                                <tr class="text-center">
                                    <td>{{$officialbrand->brand_id}}</td>
                                    <td>{{$officialbrand->brand_name}}</td>
                                    <td>{{$officialbrand->brand_mkeywords}}</td>
                                    <td>{{$officialbrand->brand_mdesc}}</td>
                                    <td>{{$officialbrand->brand_slug}}</td>
                                    <td>{{\Helper::UTCtoTZ($officialbrand->created_at)}}</td>
                                    @php
                                    $image = $officialbrand->brand_Img ? $officialbrand->logoPath : '';
                                    @endphp
                                    <td width="10%">
                                        <a href="{{ $image }}" title="{{ "{$officialbrand->brand_id} - {$officialbrand->brand_name}" }}" data-gallery=""><img src="{{ $image }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive img-thumbnail"></a>
                                    </td>
                                    <td class="text-nowrap">
                                        <p>
                                            @if(in_array('settingofficialbrandsedit',$admin_permission))
                                            <a class="btn btn-white btn-sm" href="/admin/setting/officialbrands/edit/{{$officialbrand->brand_id}}"><span><i class="fa fa-edit"></i> {{trans('localize.Edit')}}</span></a>
                                            @endif
                                            <a class="btn btn-white btn-sm" href="/admin/setting/officialbrands/view/{{$officialbrand->brand_id}}"><span><i class="fa fa-file-text-o"></i> {{trans('localize.view')}}</span></a>
                                        </p>
                                        @if(in_array('settingofficialbrandssetstatus',$admin_permission))
                                        <p>
                                            @if ($officialbrand->brand_status == 1)
                                                {{-- <a href="/admin/customer/block/{{$customer->cus_id}}/blocked"><i class='fa fa-check fa-2x'></i></a> --}}
                                                <a style="width:100%" class="btn btn-white btn-sm text-warning" href="/admin/setting/officialbrands/block/{{$officialbrand->brand_id}}/blocked"><span><i class="fa fa-refresh"></i>  {{trans('localize.set_to_inactive')}}</span></a>
                                            @else
                                                {{-- <a href="/admin/customer/block/{{$customer->cus_id}}/unblocked"><i class='fa fa-ban fa-2x'></i></a> --}}
                                                <a style="width:100%" class="btn btn-white btn-sm text-navy" href="/admin/setting/officialbrands/block/{{$officialbrand->brand_id}}/unblocked"><span><i class="fa fa-refresh"></i>  {{trans('localize.set_to_active')}}</span></a>
                                            @endif
                                        </p>
                                        @endif
                                    </td>
                                    @if ($officialbrand->brand_status == 1)
                                        <td class="text-nowrap text-navy"><i class='fa fa-check'></i> {{trans('localize.Active')}}</td>
                                    @else
                                        <td class="text-nowrap text-danger"><i class='fa fa-ban'></i> {{trans('localize.Inactive')}}</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $officialbrands])

                        </table>
                        <div class="text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
<script src="/backend/js/plugins/footable/footable.all.min.js"></script>
<script src="/backend/js/custom.js"></script>
<script>
$(document).ready(function() {

    $('.number').keydown(function (e) {-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

});
</script>
@endsection