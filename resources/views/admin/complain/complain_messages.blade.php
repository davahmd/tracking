@extends('admin.layouts.master')

@section('title',  trans('localize.complain.chat.title'))

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/blueimp/css/blueimp-gallery.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/default.css?v2.1') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/widget/complain.css') }}" />
    <style>
        .total-container p {
            font-size: 13px !important;
        }
        .adm-logo {
            position: relative;
            top: -8px;
        }
    </style>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2> {{trans('localize.complain.chat.title')}}</h2>
            <ol class="breadcrumb">
                <li>
                    {{trans('localize.complain.complain')}}
                </li>
                <li>
                    <a href="{{ route('admin.complain::index') }}">{{trans('localize.complain.listing.page_title')}}</a>
                </li>
                <li class="active">
                    {{trans('localize.complain.chat.title')}}
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                @include('shared.complain.complain', [
                    'messages' => $messages,
                    'order' => $order,
                    'send_url' => '/admin/complains/' . $order->order_id . '/messages/' . $complain->id,
                    'is_admin' => true,
                    'is_merchant' => false
                ])
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('common/js/widget/complain.js') }}"></script>
@endsection