@extends('admin.layouts.master')

@section('title', 'Manage Lucky Draw')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.lucky_draw')}}</h2>
        <ol class="breadcrumb">
            <li>
                {{trans('localize.lucky_draw')}}
            </li>
            <li class="active">
                <strong>{{trans('localize.manage')}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
        <div class="col-lg-12">

            <div class="ibox float-e-margins border-bottom">
                <a class="collapse-link nolinkcolor">
                <div class="ibox-title ibox-title-filter">
                    <h5>{{trans('localize.Search_Filter')}}</h5>
                    <div class="ibox-tools">
                        <i class="fa fa-chevron-down"></i>
                    </div>
                </div>
                </a>
                <div class="ibox-content ibox-content-filter" style="display:none;">
                    <div class="row">
                        <form class="form-horizontal" id="filter" action= "{{ url('admin/lucky_draw/manage') }}" method="GET">
                            <div class="form-group">
                                <label class="col-sm-1 control-label">{{trans('localize.search')}}</label>
                                <div class="col-sm-5">
                                    <input type="text" value="{{$input['email']}}" placeholder="{{trans('localize.email')}}" class="form-control" name="email">
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" value="{{$input['phone']}}" placeholder="{{trans('localize.phone')}}" class="form-control" name="phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">{{trans('localize.Status')}}</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="status">
                                        <option value="" {{ ($input['status'] == "") ? 'selected' : '' }}>{{trans('localize.all')}}</option>
                                        <option value="unclaimed" {{ ($input['status'] == "unclaimed") ? 'selected' : '' }}>{{trans('localize.unclaimed')}}</option>
                                        <option value="unredeem" {{ ($input['status'] == "unredeem") ? 'selected' : '' }}>{{trans('localize.notredeem')}}</option>
                                        <option value="redeemed" {{ ($input['status'] == "redeemed") ? 'selected' : '' }}>{{trans('localize.redeemed')}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <button type="submit" class="btn btn-block btn-outline btn-primary" data-action="filter">{{trans('localize.search')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @include('admin.common.notifications')

            <div class="ibox">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <a  onclick="window.open('{{ url('/admin/lucky_draw/print') }}', 'newwindow', 'width=750, height=500'); return false;" href="" class="btn btn-primary btn-sm">{{trans('localize.print_qr')}}</a>
                        <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" aria-expanded="false" style="color:#fff">{{trans('localize.print_dummy')}}</a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a  onclick="window.open('{{ url('/admin/lucky_draw/dummy?total=10') }}', 'newwindow', 'width=750, height=500'); return false;" href="javascript:void(0)">10 QR Code</a></li>
                            <li><a  onclick="window.open('{{ url('/admin/lucky_draw/dummy?total=30') }}', 'newwindow', 'width=750, height=500'); return false;" href="javascript:void(0)">30 QR Code</a></li>
                            <li><a  onclick="window.open('{{ url('/admin/lucky_draw/dummy?total=50') }}', 'newwindow', 'width=750, height=500'); return false;" href="javascript:void(0)">50 QR Code</a></li>
                            <li><a  onclick="window.open('{{ url('/admin/lucky_draw/dummy?total=100') }}', 'newwindow', 'width=750, height=500'); return false;" href="javascript:void(0)">100 QR Code</a></li>
                            <li><a  onclick="window.open('{{ url('/admin/lucky_draw/dummy?total=500') }}', 'newwindow', 'width=750, height=500'); return false;" href="javascript:void(0)">500 QR Code</a></li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">#ID</th>
                                    <th class="text-center">{{trans('localize.type')}}</th>
                                    <th class="text-center">{{trans('localize.value')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.description')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.email')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.phone')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.claim_date')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.redemption')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($luckydraws as $key => $ld)
                                <tr class="text-center">
                                    <td>{{ $ld->id }}</td>
                                    <td>{{ ($ld->type == 1) ? trans('localize.normal_reward') : trans('common.credit_name') }}</td>
                                    <td>{{ $ld->value }}</td>
                                    <td>{{ $ld->desc }}</td>
                                    @if ($ld->claim_by > 0)
                                    <td>{{ ($ld->email) ? $ld->email : '-' }}</td>
                                    <td>{{ ($ld->phone_area_code) ? $ld->phone_area_code . $ld->cus_phone : '-' }}</td>
                                    <td>{{ \Helper::UTCtoTZ($ld->claim_date) }}</td>
                                    <td>
                                        @if ($ld->status != 1)
                                        <button type="button" class="btn btn-primary btn-sm button_redeemed" data-id="{{ $ld->id  }}" data-post="data-php">@lang('localize.redeemed')</button>
                                        @else
                                        {{ \Helper::UTCtoTZ($ld->redemption_date) }}
                                        @endif
                                    </td>
                                    @else
                                    <td colspan=4>@lang('localize.unclaimed')</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $luckydraws])

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('style')
    <link href="/backend/css/plugins/footable/footable.core.css" rel="stylesheet">
@endsection

@section('script')
    <script src="/backend/js/plugins/footable/footable.all.min.js"></script>

<script>
    $(document).ready(function() {
        $('.button_redeemed').on('click', function(){
            var this_id = $(this).attr('data-id');
            swal({
                title: "{{trans('localize.sure')}}",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "{{trans('localize.yes')}}",
                cancelButtonText: "{{trans('localize.cancel')}}",
                closeOnConfirm: false
            }, function(){
                var url = '/admin/lucky_draw/redeem/' + this_id;
                window.location.href = url;
            });
        });
    });
</script>
@endsection