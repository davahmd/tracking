<tr class="text-center">
    <td>{{ $order->order_id }}</td>
    <td>
        {{ $order->product ? $order->product->id : '' }} - {{ $order->product ? $order->product->name : '' }}
        @if($order->options)
        <p><br>
        @foreach ($order->options as $parent => $child)
            <b>{{ $parent }} : </b> {{ $child }} @if(!$loop->last)<br>@endif
        @endforeach
        </p>
        @endif
    </td>
    <td>{{ ($order->currency_code = 'IDR') ? rpFormat($order->total->original->amount) : $order->currency_code . ' ' . number_format($order->total->original->amount, 2) }}</td>
    <td>{{ $order->quantity }}</td>
    <td>{{ ($order->currency_code = 'IDR') ? rpFormat($order->total->original->total) : $order->currency_code . ' ' . number_format($order->total->original->total, 2) }}</td>
    <td class="text-left text-nowrap">
        <dl class="dl-horizontal" style="margin-bottom:0;">
            {{--<dt>@lang('localize.commission') ({{ $order->total->platform_charge_rate }}%)</dt>
            <dd>{{ ($order->currency_code == 'IDR') ? rpFormat($order->total->platform_charge_value) : $order->currency_code . ' ' . number_format($order->total->platform_charge_value, 2) }}</dd>--}}
            <dt>@lang('localize.gst') ({{ $order->total->service_charge_rate }}%)</dt>
            <dd>{{ ($order->currency_code == 'IDR') ? rpFormat($order->total->service_charge_value) : $order->currency_code . ' ' . number_format($order->total->service_charge_value, 2) }}</dd>
            <dt>@lang('localize.merchant_charge') ({{ $order->total->merchant_charge_rate }}%)</dt>
            <dd>{{ ($order->currency_code == 'IDR') ? rpFormat($order->total->merchant_charge_value) : $order->currency_code . ' ' . number_format($order->total->merchant_charge_value, 2) }}</dd>
            <dt>@lang('localize.shipping_fees')</dt>
            <dd>{{ ($order->currency_code == 'IDR') ? rpFormat($order->total->shipping_fees_value) : $order->currency_code . ' ' . number_format($order->total->shipping_fees_value, 2) }}</dd>
        </dl>
    </td>
    <td>{{ ($order->currency_code = 'IDR') ? rpFormat($order->total->price) : $order->currency_code . ' ' . number_format($order->total->price, 2) }}</td>
    <td>{{ $order->store_name }}</td>
    <td class="text-left text-nowrap">
        <dl class="dl-horizontal" style="margin-bottom:0;">
            @if($order->service_info->service_name)
            <dt>@lang('localize.service_name')</dt>
            <dd>{{ $order->service_info->service_name }}</dd>
            <dt>@lang('localize.schedule_date')</dt>
            <dd>{{ $order->service_info->schedule_datetime }}</dd>
            <dt>@lang('localize.timezone')</dt>
            <dd>{{ $order->service_info->timezone }}</dd>
            @else
            <dt>@lang('localize.courier')</dt>
            <dd>{{ $order->courier_info->courier }}</dd>
            <dt>@lang('localize.shipping_days')</dt>
            <dd>{{ $order->courier_info->shipping_day }}</dd>
            <dt>@lang('localize.shipping_note')</dt>
            <dd>{{ $order->courier_info->shipping_note}}</dd>
            @endif

        </dl>
    </td>
    <td>{{ ($order->currency_code = 'IDR') ? rpFormat($order->total->merchant_earn_value) : $order->currency_code . ' ' . number_format($order->total->merchant_earn_value, 2) }}</td>
    {{--  <td>{{ $order->shipping_type->text }}</td>  --}}
    <td>{{ $order->status->text }}</td>
    @if($group == 0)
    <td>
        @if($order->order_type == 3)
        <button type="button" class="btn btn-success btn-xs btn-block btn-code-view" data-id="{{ $order->order_id }}" data-action="view_coupon">@lang('localize.view_coupon')</button>
        @elseif($order->order_type == 4)
        <button type="button" class="btn btn-success btn-xs btn-block btn-code-view" data-id="{{ $order->order_id }}" data-action="view_ticket">@lang('localize.view_ticket')</button>
        @elseif($order->order_type == 5)
        <button type="button" class="btn btn-success btn-xs btn-block btn-code-view" data-id="{{ $order->order_id }}" data-action="view_ecard">@lang('localize.e-card.view')</button>
        @endif
    </td>
    @endif
    <td width="1%">
        <div class="i-checks">
            <label>
                <input type="checkbox" class="input_checkbox" data-status="{{ $order->status->code }}" data-order-type="{{ $order->order_type }}" data-shipment-type="{{ $order->shipping_type->code }}" name="order_id[]" value="{{ $order->order_id }}" disabled>
            </label>
        </div>
    </td>
</tr>