@extends('admin.layouts.master')

@section('title', trans('localize.online_histories'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.online_transaction_history')</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.transaction')
            </li>
            <li>@lang('localize.online_orders')</li>
            <li class="active">
                <strong>@lang('localize.history')</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
     <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" action="{{ route('admin.transaction.online.histories', ['type' => $type]) }}" method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['order_id'] }}" placeholder="@lang('localize.ID.order')" name="order_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['transaction_id'] }}" placeholder="@lang('localize.ID.transaction')" name="transaction_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['product_id'] }}" placeholder="@lang('localize.ID.product')" name="product_id">
                        </div>
                        <div class="col-sm-3 col-sm-offset-2">
                            <input type="text" class="form-control" value="{{ $input['customer_id'] }}" placeholder="@lang('localize.ID.customer')" name="customer_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['merchant_id'] }}" placeholder="@lang('localize.ID.merchant')" name="merchant_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['store_id'] }}" placeholder="@lang('localize.ID.store')" name="store_id">
                        </div>
                        <div class="col-sm-3 col-sm-offset-2">
                            <input type="text" class="form-control" value="{{ $input['do_number'] }}" placeholder="@lang('localize.do.number')" name="do_number">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['product_name'] }}" placeholder="@lang('localize.productName')" name="product_name">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['sku_code'] }}" placeholder="@lang('localize.sku_code')" name="sku_code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.transaction_date')</label>
                        <div class="col-sm-9">
                            <div class="input-daterange input-group">
                                <input type="text" class="form-control" name="start_date" id="sdate" placeholder="@lang('localize.startDate')" value="{{ $input['start_date'] }}"/>
                                <span class="input-group-addon">@lang('localize.to')</span>
                                <input type="text" class="form-control" name="end_date" id="edate" placeholder="@lang('localize.endDate')" value="{{ $input['end_date'] }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.type')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="type">
                                <option value="">@lang('localize.all')</option>
                                @foreach($types as $key => $value)
                                <option value="{{ $key }}" {{ $input['type'] == $key? 'selected' : ''}}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.status')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="status">
                                <option value="">@lang('localize.all')</option>
                                @foreach($status_list as $key => $value)
                                <option value="{{ $key }}" {{ (!is_null($input['status']) && $input['status'] == $key) ? 'selected' : ''}}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.sort')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="new" {{ (!$input['sort'] || $input['sort'] == 'new') ? 'selected' : ''}}>@lang('localize.Newest')</option>
                                <option value="old" {{ ($input['sort'] == 'old') ? 'selected' : ''}}>@lang('localize.Oldest')</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.merchant_country')</label>
                        <div class="col-sm-9">
                            <p class="form-control-static">
                            <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="merchant_countries[]" value="0" {{ (isset($input['merchant_countries']) && (in_array("0", $input['merchant_countries']))? 'checked' : '' ) }}>&nbsp; @lang('localize.no_country')</label>&nbsp;
                            @foreach($countries as $country)
                                <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="merchant_countries[]" value="{{ $country->co_id }}" {{ (isset($input['merchant_countries']) && (in_array($country->co_id, $input['merchant_countries']))? 'checked' : '' ) }}>&nbsp; {{ $country->co_name }}</label>
                            @endforeach
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.customer_country')</label>
                        <div class="col-sm-9">
                            <p class="form-control-static">
                            <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="customer_countries[]" value="0" {{ (isset($input['customer_countries']) && (in_array("0", $input['customer_countries']))? 'checked' : '' ) }}>&nbsp; @lang('localize.no_country')</label>&nbsp;
                            @foreach($countries as $country)
                                <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="customer_countries[]" value="{{ $country->co_id }}" {{ (isset($input['customer_countries']) && (in_array($country->co_id, $input['customer_countries']))? 'checked' : '' ) }}>&nbsp; {{ $country->co_name }}</label>
                            @endforeach
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary">@lang('localize.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            @include('admin.common.notifications')

            <div class="ibox">

                @permission('transactiononlineorderslistexport', $permissions)
                <div class="ibox-title" style="display: block;">
                    <div class="ibox-tools" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"> @lang('localize.export.all') <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('admin.export.transaction.online.histories', ['type' => $type]) . "?export=all&export_as=csv&" . http_build_query($input) }}">Csv</a></li>
                                <li><a href="{{ route('admin.export.transaction.online.histories', ['type' => $type]) . "?export=all&export_as=xlsx&" . http_build_query($input) }}">Xlsx</a></li>
                                <li><a href="{{ route('admin.export.transaction.online.histories', ['type' => $type]) . "?export=all&export_as=xls&" . http_build_query($input) }}">Xls</a></li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-white btn-sm dropdown-toggle"> @lang('localize.export.page') <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('admin.export.transaction.online.histories', ['type' => $type]) . "?export=page&export_as=csv&" . http_build_query($input) }}">Csv</a></li>
                                <li><a href="{{ route('admin.export.transaction.online.histories', ['type' => $type]) . "?export=page&export_as=xlsx&" . http_build_query($input) }}">Xlsx</a></li>
                                <li><a href="{{ route('admin.export.transaction.online.histories', ['type' => $type]) . "?export=page&export_as=xls&" . http_build_query($input) }}">Xls</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endpermission

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <th nowrap class="text-center">@lang('localize.#id')</th>
                                    <th nowrap class="text-center">@lang('localize.transID')</th>
                                    <th nowrap class="text-center">@lang('localize.do.number') / @lang('localize.sku_code')</th>
                                    <th nowrap class="text-center">@lang('localize.product')</th>
                                    <th nowrap class="text-center">@if($type == 'retail') @lang('localize.customer') @else @lang('localize.retailer') @endif / @lang('localize.Merchant') / @lang('localize.store')</th>
                                    <th nowrap class="text-center">@lang('localize.quantity')</th>
                                    <th nowrap class="text-center">@lang('localize.amount')</th>
                                    <th nowrap class="text-center">@lang('localize.charges') / @lang('localize.earning')</th>
                                    <th nowrap class="text-center">@lang('localize.details')</th>
                                </tr>
                            </thead>
                            @php
                                $order = array();
                            @endphp
                            @foreach($orders as $order)
                            <tbody>
                                <tr class="text-center">
                                    <td>
                                        {{ $order->order_id }}
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/transaction/online') . '/' . $type . '/listing?transaction_id=' . $order->transaction_id }}" class="nolinkcolor" data-toggle="tooltip" title="View transaction details" target="_blank">{{ $order->transaction_id }}</a>
                                    </td>
                                    <td nowrap>
                                        <p>
                                            <b>@lang('localize.do.number')</b><br>
                                            @if($order->mapping && $order->mapping->invoice)
                                            {{ $order->mapping->invoice->tax_number('ON') }}
                                            @endif
                                        </p>

                                        <p>
                                            <b>@lang('localize.sku_code')</b><br>
                                            {{ $order->sku }}
                                        </p>
                                    </td>
                                    <td style="min-width:250px;">
                                        @if($order->product)
                                        <a href="{{ url('admin/product/view', [$order->product->pro_mr_id, $order->product->pro_id]) }}" class="nolinkcolor" data-toggle="tooltip" title="View product details" target="_blank">{{ "{$order->product->pro_id} - {$order->product->title}" }}</a>
                                        @if(!empty($order->order_attributes))
                                        <br><br>
                                        <p>
                                            {!! $order->parseOrderAttribute !!}
                                        </p>
                                        @endif
                                        @else
                                        @lang('localize.product_not_found')
                                        @endif
                                    </td>
                                    <td>
                                        <p>
                                            @if($type == 'retail')
                                                <b>@lang('localize.customer')</b><br>
                                                @if($order->customer)
                                                    <a href="{{ url('admin/customer/view', [$order->customer->cus_id]) }}" class="nolinkcolor" data-toggle="tooltip" title="View customer details" target="_blank">{{ "{$order->customer->cus_id} - {$order->customer->cus_name}" }}</a>
                                                @else
                                                    @lang('localize.Customer_not_found')
                                                @endif
                                            @else
                                                <b>@lang('localize.retailer')</b><br>
                                                @if($order->customer->user->merchant)
                                                    <a href="{{ url('admin/merchant/view', [$order->customer->user->merchant->mer_id]) }}" class="nolinkcolor" data-toggle="tooltip" title="View customer details" target="_blank">{{ "{$order->customer->user->merchant->mer_id} - {$order->customer->user->merchant->full_name()}" }}</a>
                                                @else
                                                    @lang('localize.Customer_not_found')
                                                @endif
                                            @endif
                                        </p>

                                        <p>
                                            <b>@lang('localize.Merchant')</b><br>
                                            @if($order->product && $order->product->merchant)
                                            <a href="{{ url('admin/merchant/view', [$order->product->merchant->mer_id]) }}" class="nolinkcolor" data-toggle="tooltip" title="View merchant details" target="_blank">{{ "{$order->product->merchant->mer_id} - {$order->product->merchant->merchantName()}" }}</a>
                                            @else
                                            @lang('localize.Merchant_not_found')
                                            @endif
                                        </p>

                                        <p>
                                            <b>@lang('localize.store')</b><br>
                                            @if($order->product && $order->product->store)
                                            <a href="{{ url('admin/store/view', [$order->product->store->stor_id]) }}" class="nolinkcolor" data-toggle="tooltip" title="View store details" target="_blank">{{ "{$order->product->store->stor_id} - {$order->product->store->stor_name}" }}</a>
                                            @else
                                            @lang('localize.store_not_found')
                                            @endif
                                        </p>
                                    </td>
                                    <td>
                                        {{ $order->order_qty }}
                                    </td>
                                    <td nowrap>
                                        {{ ($order->currency = 'IDR') ? rpFormat($order->order_price) : $order->currency . ' ' . number_format($order->order_price , 2) }}
                                    </td>
                                    <td class="text-left text-nowrap">
                                        <dl class="dl-horizontal" style="margin-bottom:0;">
                                            {{--<dt>@lang('localize.commission') ({{ round($order->platform_charge_rate) }}%)</dt>
                                            <dd>{{ ($order->currency = 'IDR') ? rpFormat($order->platform_charge_value) : $order->currency . ' ' . number_format($order->platform_charge_value , 2) }}</dd>--}}
                                            <dt>@lang('localize.gst') ({{ round($order->service_charge_rate) }}%)</dt>
                                            <dd>{{ ($order->currency = 'IDR') ? rpFormat($order->service_charge_value) : $order->currency . ' ' . number_format($order->service_charge_value , 2) }}</dd>
                                            <dt>@lang('localize.merchant_charge') ({{ round($order->merchant_charge_rate) }}%)</dt>
                                            <dd>{{ ($order->currency = 'IDR') ? rpFormat($order->merchant_charge_value) : $order->currency . ' ' . number_format($order->merchant_charge_value , 2) }}</dd>
                                            <dt>@lang('localize.shipping_fees')</dt>
                                            <dd>{{ ($order->currency = 'IDR') ? rpFormat($order->shipping_fees_value) : $order->currency . ' ' . number_format($order->shipping_fees_value , 2) }}</dd>
                                            <dt>@lang('localize.merchant_earning')</dt>
                                            <dd>{{ ($order->currency = 'IDR') ? rpFormat($order->merchant_earn_value) : $order->currency . ' ' . number_format($order->merchant_earn_value , 2) }}</dd>
                                        </dl>
                                    </td>
                                    <td nowrap>
                                        <p>
                                            <b>@lang('localize.status')</b><br>
                                            {{ $order->status() }}
                                        </p>

                                        <p>
                                            <b>@lang('localize.type')</b><br>
                                            {{ $order->type() }}
                                        </p>

                                        <p>
                                            <b>@lang('localize.transaction_date')</b><br>
                                            {{ \Helper::UTCtoTZ($order->created_at) }}
                                        </p>
                                    </td>
                            </tbody>
                            @endforeach
                            @if ($order)
                            <tr class="text-center">
                                <td colspan="6" class="text-right font-bold">@lang('localize.total')</td>
                                <td class="font-bold">{{ ($order->currency_code = 'IDR') ? rpFormat($total->order_price) : $order->currency . ' ' . number_format($total->order_price, 2) }}</td>
                                <td class="text-left text-nowrap">
                                    <dl class="dl-horizontal" style="margin-bottom:0;">
                                        <dt>@lang('localize.commission')</dt>
                                        <dd>{{ ($order->currency_code = 'IDR') ? rpFormat($total->platform_charge_value) : $order->currency . ' ' . number_format($total->platform_charge_value, 2) }}</dd>
                                        <dt>@lang('localize.gst')</dt>
                                        <dd>{{ ($order->currency_code = 'IDR') ? rpFormat($total->service_charge_value) : $order->currency . ' ' . number_format($total->service_charge_value , 2) }}</dd>
                                        <dt>@lang('localize.merchant_charge')</dt>
                                        <dd>{{ ($order->currency_code = 'IDR') ? rpFormat($total->merchant_charge_value) : $order->currency . ' ' . number_format($total->merchant_charge_value, 2) }}</dd>
                                        <dt>@lang('localize.shipping_fees')</dt>
                                        <dd>{{ ($order->currency_code = 'IDR') ? rpFormat($total->shipping_fees_value) : $order->currency . ' ' . number_format($total->shipping_fees_value, 2) }}</dd>
                                        <dt>@lang('localize.merchant_earning')</dt>
                                        <dd>{{ ($order->currency_code = 'IDR') ? rpFormat($total->merchant_earn_value) : $order->currency . ' ' . number_format($total->merchant_earn_value, 2) }}</dd>
                                    </dl>
                                </td>
                                <td colspan="100"></td>
                            </tr>
                            @endif

                            @include('layouts.partials.table-pagination', ['listings' => $orders])

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
    td {
        vertical-align: middle !important;
    }
</style>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
@endsection
