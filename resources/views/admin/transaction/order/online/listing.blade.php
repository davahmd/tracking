@extends('admin.layouts.master')

@section('title', trans('localize.online_listing'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.online_transaction_listing')</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.transaction')
            </li>
            <li>@lang('localize.online_orders')</li>
            <li class="active">
                <strong>@lang('localize.listing')</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
     <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" action="{{ url('admin/transaction/online') . '/' . $type . '/listing' }}" method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['transaction_id'] }}" placeholder="@lang('localize.transID')" name="transaction_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['customer_id'] }}" placeholder="@lang('localize.customer_id')" name="customer_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['merchant_id'] }}" placeholder="@lang('localize.merchant_id')" name="merchant_id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.transaction_date')</label>
                        <div class="col-sm-9">
                            <div class="input-daterange input-group">
                                <input type="text" class="form-control" name="start_date" id="sdate" placeholder="@lang('localize.startDate')" value="{{ $input['start_date'] }}"/>
                                <span class="input-group-addon">@lang('localize.to')</span>
                                <input type="text" class="form-control" name="end_date" id="edate" placeholder="@lang('localize.endDate')" value="{{ $input['end_date'] }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.status')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="status">
                                <option value="">@lang('localize.all')</option>
                                @foreach($status_list as $key => $value)
                                <option value="{{ $key }}" {{ (!is_null($input['status']) && $input['status'] == $key) ? 'selected' : ''}}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.sort')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="new" {{ (!$input['sort'] || $input['sort'] == 'new') ? 'selected' : ''}}>@lang('localize.Newest')</option>
                                <option value="old" {{ ($input['sort'] == 'old') ? 'selected' : ''}}>@lang('localize.Oldest')</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.merchant_country')</label>
                        <div class="col-sm-9">
                            <p class="form-control-static">
                            <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="merchant_countries[]" value="0" {{ (isset($input['merchant_countries']) && (in_array("0", $input['merchant_countries']))? 'checked' : '' ) }}>&nbsp; @lang('localize.no_country')</label>&nbsp;
                            @foreach($countries as $country)
                                <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="merchant_countries[]" value="{{ $country->co_id }}" {{ (isset($input['merchant_countries']) && (in_array($country->co_id, $input['merchant_countries']))? 'checked' : '' ) }}>&nbsp; {{ $country->co_name }}</label>
                            @endforeach
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.customer_country')</label>
                        <div class="col-sm-9">
                            <p class="form-control-static">
                            <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="customer_countries[]" value="0" {{ (isset($input['customer_countries']) && (in_array("0", $input['customer_countries']))? 'checked' : '' ) }}>&nbsp; @lang('localize.no_country')</label>&nbsp;
                            @foreach($countries as $country)
                                <label style="cursor: pointer;"><input type="checkbox" class="i-checks input_checkbox" name="customer_countries[]" value="{{ $country->co_id }}" {{ (isset($input['customer_countries']) && (in_array($country->co_id, $input['customer_countries']))? 'checked' : '' ) }}>&nbsp; {{ $country->co_name }}</label>
                            @endforeach
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary">@lang('localize.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            @include('admin.common.notifications')

            <div class="ibox">

                @permission('transactiononlineorderslistexport', $permissions)
                <div class="ibox-title" style="display: block;">
                    <div class="ibox-tools" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"> @lang('localize.export.all') <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('admin.export.online.transaction', ['type' => $type]) . "?export=all&export_as=csv&" . http_build_query($input) }}">Csv</a></li>
                                <li><a href="{{ route('admin.export.online.transaction', ['type' => $type]) . "?export=all&export_as=xlsx&" . http_build_query($input) }}">Xlsx</a></li>
                                <li><a href="{{ route('admin.export.online.transaction', ['type' => $type]) . "?export=all&export_as=xls&" . http_build_query($input) }}">Xls</a></li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-white btn-sm dropdown-toggle"> @lang('localize.export.page') <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('admin.export.online.transaction', ['type' => $type]) . "?export=page&export_as=csv&" . http_build_query($input) }}">Csv</a></li>
                                <li><a href="{{ route('admin.export.online.transaction', ['type' => $type]) . "?export=page&export_as=xlsx&" . http_build_query($input) }}">Xlsx</a></li>
                                <li><a href="{{ route('admin.export.online.transaction', ['type' => $type]) . "?export=page&export_as=xls&" . http_build_query($input) }}">Xls</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endpermission

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <th nowrap class="text-center">@lang('localize.#id')</th>
                                    <th nowrap class="text-center">@lang('localize.transID')</th>
                                    <th nowrap class="text-center">@lang('localize.detail')</th>
                                    <th nowrap class="text-center">@lang('localize.Merchants')</th>
                                    <th nowrap class="text-center">@lang('localize.store')</th>
                                    <th nowrap class="text-center">@lang('localize.amount')</th>
                                    <th nowrap class="text-center">@lang('localize.charges')</th>
                                    <th nowrap class="text-center">@lang('localize.merchant_earning')</th>
                                    <th nowrap class="text-center">@lang('localize.action')</th>
                                </tr>
                            </thead>
                            @php
                                $item = array();
                            @endphp
                            @foreach($transactions as $transaction)
                            <tbody>
                                <tr class="text-center">
                                    <td rowspan="{{ $transaction->items->count() }}">{{ $transaction->id }}</td>
                                    <td rowspan="{{ $transaction->items->count() }}">
                                        <span style="cursor: pointer; color: #337ab7;" onclick="view_order_details({{ $transaction->id }}, 'admin')">{{ $transaction->transaction_id }}</span>
                                    </td>
                                    <td rowspan="{{ $transaction->items->count() }}">
                                        <p>
                                            @if($type == 'retail')
                                                <b>@lang('localize.customer')</b><br>
                                                {{ $transaction->customer->cus_id }} - {{ $transaction->customer->customer_name }}
                                            @elseif($type == 'wholesale')
                                                <b>@lang('localize.retailer')</b><br>
                                                {{ $transaction->customer->user->merchant->mer_id }} - {{ $transaction->customer->user->merchant->full_name() }}
                                            @endif
                                        </p>
                                        <p>
                                            <b>@lang('localize.transaction_date')</b><br>
                                            {{ \Helper::UTCtoTZ($transaction->transaction_date) }}
                                        </p>
                                    </td>
                                    @foreach($transaction->items->groupBy('store_id') as $store_id => $items)
                                    @if(!$loop->first)
                                    <tr class="text-center">
                                    @endif
                                    @php
                                    $item = $items->first();
                                    $normalProduct = $items->whereIn('order_type', [1,2]);
                                    $virtualProduct = $items->whereIn('order_type', [3,4,5]);
                                    if($normalProduct->count() > 0)
                                    {
                                        $status = $normalProduct->sortBy('order_status')->first()->status();
                                    } else
                                    {
                                        $status = $virtualProduct->sortBy('order_status')->first()->status();
                                    }
                                    @endphp
                                    <td>
                                        <span style="cursor: pointer; color: #337ab7;" onclick="view_order_details({{ $transaction->id }}, 'admin', {{ $item->merchant_id }})">{{ $item->merchant_id }} - {{ $item->merchant_name }}</span>
                                    </td>
                                    <td>{{ $item->store_id . ' - ' . $item->store_name }}</td>
                                    <td>{{ ($item->currency_code = 'IDR') ? rpFormat($items->sum('order_price')) : $item->currency_code . ' ' . number_format($items->sum('order_price'), 2) }}</td>
                                    <td class="text-left text-nowrap">
                                        <dl class="dl-horizontal" style="margin-bottom:0;">
                                            {{--<dt>@lang('localize.platform_charges') ({{ round($item->platform_charge_rate) }}%)</dt>
                                            <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($items->sum('platform_charge_value')) : $item->currency_code . ' ' . number_format($items->sum('platform_charge_value'), 2) }}</dd>--}}
                                            <dt>@lang('localize.gst') ({{ round($item->service_charge_rate) }}%)</dt>
                                            <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($items->sum('service_charge_value')) : $item->currency_code . ' ' . number_format($items->sum('service_charge_value'), 2) }}</dd>
                                            <dt>@lang('localize.merchant_charge') ({{ round($item->merchant_charge_rate) }}%)</dt>
                                            <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($items->sum('merchant_charge_value')) : $item->currency_code . ' ' . number_format($items->sum('merchant_charge_value'), 2) }}</dd>
                                            <dt>@lang('localize.shipping_fees')</dt>
                                            <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($items->sum('shipping_fees_value')) : $item->currency_code . ' ' . number_format($items->sum('shipping_fees_value'), 2) }}</dd>
                                        </dl>
                                    </td>
                                    <td>{{ ($item->currency_code = 'IDR') ? rpFormat($items->sum('merchant_earn_value')) : $item->currency_code . ' ' . number_format($items->sum('merchant_earn_value'), 2) }}</td>
                                    <td>
                                        <p>
                                            {{ $status }}
                                        </p>
                                        <p>
                                            
                                            <a href="{{ url('admin/transaction/online/detail',[$item->parent_order_id, $item->store_id]) }}" class="btn btn-info btn-xs btn-block" data-toggle="modal" data-id="1" data-post="data-php" data-action="details">@lang('localize.view_order')</a>
                                        </p>
                                    </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                            @endforeach
                            @if ($item)
                            <tr class="text-center">
                                <td colspan="5" class="text-right font-bold">@lang('localize.total')</td>
                                <td class="font-bold">{{ ($item->currency_code = 'IDR') ? rpFormat($total->order_price) : $item->currency_code . ' ' . number_format($total->order_price, 2) }}</td>
                                <td class="text-left text-nowrap">
                                    <dl class="dl-horizontal" style="margin-bottom:0;">
                                        <dt>@lang('localize.commission')</dt>
                                        <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($total->platform_charge_value) : $item->currency_code . ' ' . number_format($total->platform_charge_value, 2) }}</dd>
                                        <dt>@lang('localize.gst')</dt>
                                        <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($total->service_charge_value) : $item->currency_code . ' ' . number_format($total->service_charge_value , 2) }}</dd>
                                        <dt>@lang('localize.merchant_charge')</dt>
                                        <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($total->merchant_charge_value) : $item->currency_code . ' ' . number_format($total->merchant_charge_value, 2) }}</dd>
                                        <dt>@lang('localize.shipping_fees')</dt>
                                        <dd>{{ ($item->currency_code = 'IDR') ? rpFormat($total->shipping_fees_value) : $item->currency_code . ' ' . number_format($total->shipping_fees_value, 2) }}</dd>
                                    </dl>
                                </td>
                                <td class="font-bold">{{ ($item->currency_code = 'IDR') ? rpFormat($total->merchant_earn_value) : $item->currency_code . ' ' . number_format($total->merchant_earn_value, 2) }}</td>
                                <td colspan="100"></td>
                            </tr>
                            @endif

                            @include('layouts.partials.table-pagination', ['listings' => $transactions])

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
    td {
        vertical-align: middle !important;
    }
</style>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
@endsection
