<div class="row">
    <div class="col-lg-12">
        <div class="section-review">
            <div class="review-panel">
                <div class="rating">
                    <div class="current-rate">
                        <h2>{{ $compiledRatings->rate }}/5</h2>
                        @include('front.partial.star.tag', ['rating' => $compiledRatings])
                        <label>{{ $compiledRatings->total }} @lang('localize.rating')</label>
                    </div>
                    <div class="rate-detail">

                        @foreach ($compiledRatings->cumulative as $i => $rating)
                        <div class="list">
                            @include('front.partial.star.tag', ['percentage' => $i * 20])
                            @include('front.partial.star.bar')
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="review">
                    <h4>@lang('localize.review')</h4>

                    @foreach ($ratings as $rating)
                    @include('front.partial.rating')
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        @include('layouts.partials.table-pagination', ['listings' => $ratings, 'noDataDisplay' => false])
    </div>
</div>

@section('style')
<link rel="stylesheet" href="{{ asset('backend/css/plugins/blueimp/css/blueimp-gallery.min.css') }}">
<link rel="stylesheet" href="{{ asset('common/lib/css/rating.css?v=1.10') }}">
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/custom.js') }}"></script>
@endsection