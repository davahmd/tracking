<div class="complain-container bg-white border pd-md">
    <div class="complain-titlebar d-flex align-items-center">
        <h5>@lang('localize.complain.form.conversation_messages')</h5>
    </div>

    <div class="row">
        <div class="col-sm-5">
            <div class="account-container bg-white border px-3 pb-3">
                <div class="sub-heading f-black">@lang('localize.title')</div>
                {{ $complain->subject->title }}

                <div class="sub-heading f-black">@lang('localize.information')</div>
                <div class="total-container border">
                    <div class="mr-top-sm">
                        <div class="list">
                            <p>@lang('localize.complain.view.complain_at')</p>
                            <p>{{ \Helper::UTCtoTZ($complain->created_at) }}</p>
                        </div>
                        <div class="list">
                            <p>@lang('localize.complain.view.last_reply')</p>
                            <p>{{ \Helper::UTCtoTZ($messages->first()->created_at) }}</p>
                        </div>
                        <div class="list">
                            <p>@lang('localize.complain.view.complain_by')</p>
                            <p>{{ $complain->complainant_name }}</p>
                        </div>
                        <div class="list">
                            <p>@lang('localize.complain.view.transaction_id')</p>
                            <p>
                                @php
                                    if ($is_admin) {
                                        $route = route('transaction.online.listing', ['transaction_id' => $order->transaction_id]);
                                    } elseif ($is_merchant) {
                                        $route = route('merchant.online.transaction', ['type' => 'retail', 'transaction_id' => $order->transaction_id]);
                                    } else {
                                        $route = route('order-detail', $order->transaction_id);
                                    }
                                @endphp
                                <a href="{{ $route }}" target="_">{{ $order->transaction_id }}</a>
                            </p>
                        </div>
                        <div class="list">
                            <p>@lang('localize.complain.global.escalated_to_admin')</p>
                            <p>{{ $complain->is_escalated ? trans('localize.yes') : trans('localize.no') }}</p>
                        </div>
                    </div>
                </div>

                {{--<div class="sub-heading f-black">@lang('localize.complain.view.related_product')</div>--}}
                {{--<div class="total-container border">--}}
                    {{--<div class="mr-top-sm">--}}
                        {{--@if ($complain->complain_orders->isNotEmpty())--}}
                            {{--@foreach ($complain->complain_orders as $order)--}}
                                {{--<div class="list">--}}
                                    {{--<p>#{{ $order->order->product->getKey() }}</p>--}}
                                    {{--<p>--}}
                                        {{--@php--}}
                                            {{--$route = isset($is_admin) && $is_admin ? route('product.view', [$order->order->product->pro_mr_id, $order->order->product->getKey(), $order->order->product->pro_id]) : $order->order->product->slug_url;--}}
                                        {{--@endphp--}}
                                        {{--<a href="{{ $route }}" target="_blank">@lang('localize.complain.view.open')</a>--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--@else--}}
                            {{--<div class="list">--}}
                                {{--<p>@lang('localize.table_no_record')</p>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="sub-heading f-black">@lang('localize.complain.view.attachments')</div>
                <div class="total-container border">
                    @if ($complain->complain_images->isNotEmpty())
                        <div class="row">
                            @foreach ($complain->complain_images as $image)
                                <div class="col-sm-4 p-1">
                                    <a href="{{ $image->image_link }}" data-gallery="">
                                        <img src="{{ $image->image_link }}" onerror="this.onerror=null;this.src='/common/images/stock.png';" class="img-responsive img-thumbnail" style="height:50px; width:auto; min-width: 50px;">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="list">
                            <p>@lang('localize.table_no_record')</p>
                        </div>
                    @endif
                </div>
            </div>
            @if (!$is_admin && !$complain->is_escalated)
                <div class="pt-3">
                    <button class="btn btn-red btn-block" onclick="if (confirm(window.translations.sure)) { $(this).next('form').submit()}">
                        {{ strtoupper(trans('localize.complain.global.escalate_to_admin')) }}
                    </button>

                    <form action="{{ $escalate_url }}" method="POST">
                        {{ csrf_field() }}
                    </form>
                </div>
            @endif
        </div>
        <div class="col-sm-7 mt-sm-0 mt-2">
            <div class="ibox border message-container" data-has-more-page="{{ $messages->hasMorePages() }}" data-next-page-url="{{$messages->nextPageUrl()}}">
                <div class="ibox-content bg-grey scrollable">
                    <div>
                        <div class="feed-activity-list">
                            <button class="btn btn-red btn-block m-t btn-load-more d-none"><i class="fa fa-arrow-up"></i> @lang('localize.complain.chat.load_previous')</button>
                            <br/>
                            @include('shared.complain.chats', [
                                'messages' => $messages
                            ])
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-10">
                            <textarea class="form-control float-right" id="message"></textarea>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn float-right btn-primary btn-send mt-2 mt-sm-0" data-url="{{ $send_url }}" data-lang-send="{{ trans('localize.send') }}">
                                @lang('localize.send')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>