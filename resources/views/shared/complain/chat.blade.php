<div class="ibox border message-chat">
    <div class="ibox-title border-0 p-2">
        @if (!$message->isResponderIsAnAdmin() && !$message->isResponderIsAMerchant())
            <div>
                <img alt="image" class="rounded-circle hoverZoomLink" src="{{$message->responder ? $message->responder->getAvatarUrl() : null}}" width="38" height="38">
                {{ ucwords($message->responder_name) }}
                <span class="float-right time time-user">{{ \Helper::UTCtoTZ($message->created_at) }}</span>
            </div>
        @else
            <span class="float-left time">{{ \Helper::UTCtoTZ($message->created_at) }}</span>
            <div class="float-right">
                <span class="rounded-circle {{ $message->isResponderIsAMerchant() ? 'rounded-char-merchant' : 'rounded-char-admin' }} adm-logo">
                    {{ $message->isResponderIsAMerchant() ? 'M' : 'A' }}
                </span>
            </div>
        @endif
    </div>
    <div class="ibox-content bg-white message-body">
        {{ $message->message }}
    </div>
</div>