@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Hello!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{!! $line !!}

@endforeach

@if (isset($service))
- Location: {{ $service->store->stor_name }}
- Date: {{ $service->schedule_datetime }}
@endif

@if (isset($orders))

@if (!is_null($orders->first()->courier) && !isset($service))
- Courier: {{ $orders->first()->courier->name }}
- Tracking No: {{ $orders->first()->order_tracking_no }}
- Shipment Date: {{ $orders->first()->order_shipment_date }}
@endif

@component('mail::table')
    | Product                               | Quantity                  |
    |:------------------------------------- | -------------------------:|
    @foreach($orders as $order)
    | {{ $order->product->pro_title_en }}   | {{ $order->order_qty }}   |
    @endforeach
@endcomponent
@endif

{{--Negotiation--}}
@if (isset($negotiation))
@component('mail::table')
    | Product                               | Original Price            | Negotiation Price |
    |:------------------------------------- | -------------------------:| -----------------:|
    | {{ $negotiation->product->title }} | {{ rpFormat($negotiation->pricing->price) }} | {{ rpFormat($negotiation->merchant_offer_price ?? $negotiation->customer_offer_price) }} |
@endcomponent
@endif

{{-- Action Button --}}
@if (isset($actionText))
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'red';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endif

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

<!-- Salutation -->
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('email.regards'),<br>@lang('common.mall_name')
@endif

<!-- Subcopy -->
@if (isset($actionText))
@component('mail::subcopy')
@lang('email.subcopy', [ 'actionText' => $actionText, 'actionUrl' => $actionUrl ])
@endcomponent
@endif
@endcomponent
