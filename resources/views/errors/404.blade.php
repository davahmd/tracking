@extends('layouts.front_master')

@section('top-header')
@include('layouts.partials.front_top_header')
@endsection

@section('content')
<div class="flash-sales-container">
    <div class="container pd-y-lg">
        <div class="topbar d-flex flex-column">
            <h4>Our site is under construction. Stay tuned for more update!</h4>
            <div class="red-head"></div>

            <div class="flash-sale-time text-center">
                <img src="{{ asset('common/images/comingsoon.png') }}" />
                <br/>
                <button class="btn btn-red" onclick="window.history.back()">Back To Previous Page</button>
            </div>
        </div>
    </div>
</div>
@endsection
