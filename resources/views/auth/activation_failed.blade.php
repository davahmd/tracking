@extends('layouts.front_auth_master')

@section('content')
<div class="login-container forgot-password">
    <div class="content-box text-center">
        <img src="{{ asset('assets/images/icon_error.png') }}">
        <br>
        <br>
        <h2>@lang('localize.error'), @lang('localize.invalid_token')</h2>
        <br/>
        <a class="btn btn-login btn-block" href="{{ url('/') }}">@lang('localize.back')</a>
    </div>
</div>
@endsection