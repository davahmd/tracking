@extends('layouts.front_auth_master')

@section('content')
<div class="login-container">
	<div class="content-box text-left">
		@include('layouts.partials.status')
		<form role="form" class="pd-y-md pd-x-lg border" method="POST" action="{{ url('/resend/activation', [$id]) }}">
			{{ csrf_field() }}

			<div class="text-center">
				<h2>@lang('localize.resend_activation_email')</h2>			
			</div>
			<br>

			<div class="form-group">
				<label>@lang('localize.email')</label>
				<input type="email" name="email" class="form-control" placeholder="@lang('localize.email')" value="{{ old('email') }}" autocorrect="off" autocapitalize="off" autofocus="">
			</div>

			<button type="submit" class="btn btn-login btn-block">@lang('localize.send')</button>
		</form>
	</div>
</div>
@endsection
