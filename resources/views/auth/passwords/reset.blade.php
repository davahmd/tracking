@extends('layouts.front_auth_master')

@section('content')
<div class="login-container forgot-password">
        <div class="content-box text-left">
            <form class="pd-y-md pd-x-lg border" role="form" method="POST" action="{{ (empty($phone))? url('password/reset') : url('password/phone/reset', [$phone]) }}">
                {{ csrf_field() }}
    
                <input type="hidden" name="token" value="{{ $token }}">
                @include('front.common.notifications')
                <div class="text-center">
                    <h2>Update Password</h2>
                </div>
                <br>
    
                @if(empty($phone))
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="@lang('localize.email')" value="{{ old('email') }}" autocorrect="off" autocapitalize="off" autofocus="">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
                @endif
    
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="@lang('localize.newpassword')" autocorrect="off" autocapitalize="off" autofocus="">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>
    
                <div class="form-group">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('localize.confirmnewpassword')" autocorrect="off" autocapitalize="off" autofocus="">
                </div>
    
                <input type="submit" class="btn btn-login btn-block" value="@lang('localize.updatepassword')">
            </form>
    </div>
</div>
@endsection