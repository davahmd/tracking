@extends('layouts.front_auth_master')

@section('content')
<div class="login-container forgot-password">
    <div class="content-box text-left">
        <form class="pd-y-md pd-x-lg border" method="POST" action="{{url('password_reset_email_submit')}}">
            {{ csrf_field() }}
            @include('front.common.notifications')
            <div class="text-center">
                <h2>Forgot Password</h2>
            </div>
            <p class="mr-y-md">Please enter your registered email below. We will send the reset link to your email.</p>
            <div class="form-group">
                <label>Email Address</label>
                <div class="custom-input">
                    <input type="email" name="email" class="form-control" placeholder="@lang('localize.email')" value="{{ old('email') }}" autocorrect="off" autocapitalize="off" autofocus="">
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-login btn-block" value="@lang('localize.submit')">
            </div>
            <span class="info mr-y-sm">Do not have an account? <a href="{{ route('register') }}">Sign Up here</a></span>
        </form>
    </div>
</div>
@endsection