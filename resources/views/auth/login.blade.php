@extends('layouts.front_auth_master')

@section('title', trans('localize.login'))

@section('style')
<style>
.login-container .language-dropdown {
    list-style: none;
    text-align: right;
}
.login-container .language-dropdown img {
    height: 30px;
    width: 30px;
}
.field-icon {
  float: right;
  left: -18px;
  margin-top: -30px;
  position: relative;
  z-index: 2;
}
</style>
@endsection

@section('content')
<div class="login-container">
    <div class="content-box text-left">
        <form class="pd-y-md pd-x-lg border" action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}
            <ul class="navbar-secondary"> 
                <li class="dropdown language-dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown">
                        <img src="{{ asset('asset/images/icon/icon_globe.png') }}">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="/home/setlocale?lang=en">@lang('common.english')</a>
                        <a class="dropdown-item" href="/home/setlocale?lang=ind">@lang('common.indonesian')</a>
                    </div>
                </li>
            </ul>
            <div class="logo-container text-center">
                <a href="{{ route('home') }}"><img src="{{ asset('asset/images/logo/logo.png') }}" /></a>
                @include('layouts.partials.status')
            </div>
            <div class="form-group">
                <label>@lang('localize.email_add')</label>
                <div class="custom-input">
                    <input type="email" placeholder="{{ trans('localize.email') }}" name="login" value="{{ old('login') }}" required>
                </div>
            </div>
            <div class="form-group">
                <label>@lang('localize.password')</label>
                <div class="custom-input">
                    <input class="password" type="password" placeholder="{{ trans('localize.password') }}" name="password" required>
                    <span id="show-pass" class="fa fa-eye field-icon" aria-hidden="true"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="rmb"><input type="checkbox" name="remember">&nbsp;@lang('localize.remember')</label>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-login"  style="text-transform: uppercase;">@lang('front.button.login')</button>
            </div>
            <a href="{{route('password-reset')}}" class="btn-forget">@lang('localize.forgot_password')?</a>
            <hr>
            <div class="text-center">
                <a href="/" class="info btn btn-greyblue"><i class="fas fa-home"></i> @lang('localize.back_to_home')</a>
                <br/>
                <span href="javascript:void(0)" class="info mr-y-sm">@lang('front.auth.hint.go_to_register') <a href="{{ route('register') }}">@lang('front.auth.hint.register_here')</a></span>
            </div>
        </form>
    </div>
    <!-- <div class="content-box d-flex align-items-center">
        <div class="d-flex flex-column pd-y-sm pd-x-lg" style="width: 100%;">
        <a href="login_phone.html" class="btn btn-ph">LOG IN with PHONE NUMBER</a>
        <p class="mr-y-sm">or log in with</p>
        <div class="social-login d-flex justify-content-between">
            <a href="javascript:void(0)" class="btn btn-fb flex-fill"><span><i class="fab fa-facebook"></i></span> &ensp;Facebook</a>
            <a href="javascript:void(0)" class="btn btn-gp flex-fill"><span><i class="fab fa-google"></i></span> &ensp;Google</a>
        </div>
        <span href="javascript:void(0)" class="info mr-y-sm">Do not have an account? <a href="{{ route('register') }}">Sign Up here</a></span>
        </div>
    </div> -->
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#show-pass").on('click', function(event) {
                event.preventDefault();
                if($('.password').attr("type") == "text"){
                    $('.password').attr('type', 'password');
                    $('.custom-input span').addClass( "fa-eye-slash" );
                    $('.custom-input span').removeClass( "fa-eye" );
                }else if($('.password').attr("type") == "password"){
                    $('.password').attr('type', 'text');
                    $('.custom-input span').addClass( "fa-eye" );
                    $('.custom-input span').removeClass( "fa-eye-slash" );
                }
            });
        });
    </script>
@endsection
