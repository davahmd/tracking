@extends('layouts.front_auth_master')

@section('content')
<div class="login-container forgot-password">
    <div class="content-box text-center">
        <img src="{{ asset('assets/images/icon/icon_success.png') }}">
        <br>
        <br>
        <h2>@lang('localize.member.account_activation.success')</h2>
        <p>@lang('localize.member.account_activation.msg')</p>
        <br/>
        <a class="btn btn-login btn-block" href="{{ url('login') }}">@lang('localize.login')</a>
    </div>
</div>
@endsection