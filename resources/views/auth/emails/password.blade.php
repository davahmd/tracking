@extends('layouts.front_email')

@section('content')
<tr>
    <td width="50"></td>
    <td valign="middle" width="450">
        <p style="font-size:18px; color:#4a4a4a;">@lang('email.forgot_password.greeting', ['cus_name' => $user->name]),</p>
        <div style="font-size:15px;color:#4a4a4a">

            {!! trans('email.forgot_password.msg') !!}
            
            <center style="margin:5px 0 15px">
                <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}" target="_blank" style="background-color:#ff1818; border: 0; border-radius: 8px; padding: 14px 60px; font-weight: bold; color: #FFF; text-decoration: none;">@lang('email.forgot_password.btn_reset')</a>
            </center>
            
            <p><span style="display:inline-block;height:10px;width:10px;background:#ff1818;margin-right:5px;"></span> Zona Team</p>
        </div>
    </td>
    <td width="50"></td>
</tr>
@endsection
