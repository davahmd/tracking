@extends('layouts.mobile_master')

@section('title', trans('front.nav.news_promotions'))

@section('style')
<style>
    img {
        width:100% !important
    }
</style>
@endsection

@section('content')
<div id="main">
    <div class="container" style="background-color:#fff;">
        <div class="section-news">
            <div class="news-detail">
                <div class="news-img">
                    <img src="{{ \Storage::url('images/news/'.$news->image_localize) }}">
                </div>
                <div class="title">{!! $news->title_localize !!}</div>
                
                {!! $news->content_localize !!}
            </div>
        </div>
    </div>
</div>
@endsection