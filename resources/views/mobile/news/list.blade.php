@extends('layouts.mobile_master')

@section('title', trans('front.nav.news_promotions'))

@section('content')
<div id="main">
    <div class="container" style="background-color:#fff;">
        <div class="section-news">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('front.nav.news_promotions')</h4>
                <div class="red-head"></div>
            </div>
            <div class="news">
                @foreach ($news as $key => $item)
                <div class="list">
                    <div class="thumb">
                        <img src="{{ \Storage::url('images/news/'.$item->image_localize) }}" onerror="this.onerror=null;this.src='/asset/images/banner/default.jpg';">
                    </div>
                    <div class="news-summary" data-count="{{ $key + 1 }}">
                        <div class="news-detail">
                            <a href="{{ url('mobile/news/'.$item->id) }}"><div class="title">{{ $item->title_localize }}</div></a>
                            {!! $item->content_localize !!}
                        </div>
                        <a class="read-more" href="{{ url('mobile/news/'.$item->id) }}">@lang('localize.news.read_more')</a>
                    </div>
                </div>
                @endforeach
                <div class="d-flex justify-content-center">
                    <button id="load-more" class="btn btn-red">@lang('localize.news.load_more')</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(function() {
    let renderlistDom = (id, title, content, image, count) => {
        let url = `{{ Storage::url("images/news") }}/${image}`;
        return `<div class="list"><div class="thumb"><img src="${url}" onerror="this.onerror=null;this.src='/asset/images/banner/default.jpg;'"></div>`
            + `<div class="news-summary" data-count="${count}"><div class="news-detail"><a href="/news/${id})"><div class="title">${title}</div></a>${content}</div>`
            + `<a class="read-more" href="/news/${id}">@lang("localize.news.read_more")</a></div></div>`
    };

    $('#load-more').click(() => {
        let lastNewsElement = $('.news .list:last');
        let currentCount = $('.news .list .news-summary:last').data('count');

        $.get('/news/load-more', {
            count: currentCount
        })
        .done((ret) => {
            let data = JSON.parse(ret);
            $.each(data, (index, item) => {
                let tmpCount = currentCount + index + 1;
                lastNewsElement.after(renderlistDom(item.id, item.title_localize, item.content_localize, item.image_localize, tmpCount));
            });

            if (data.length < 10) {
                $('#load-more').hide();
            }
        });
    });
});
</script>
@endsection