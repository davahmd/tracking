@extends('merchant.layouts.master') @section('title', trans('localize.store') . ' ' . trans('localize.review_rating')) @section('content')
@php
    $compiledRatings = $store->compiledRatings();
@endphp
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.review_rating')</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/store/manage">@lang('localize.manage')</a>
            </li>
            <li>
                @lang('localize.review_rating')
            </li>
            <li class="active">
                <strong>{{ $store->stor_name }}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('localize.review')</h5>
                </div>
                <div class="ibox-content">
                    @include('shared.rating.data', ['toggleDisplay' => true, 'userType' => 'merchant'])
                </div>
            </div>
        </div>
    </div>
</div>
@stop