@extends('merchant.layouts.master')
@section('title', 'Manage Calendar')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/fullcalendar.css') }}">
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('backend/css/fullcalendar.print.css') }}">
<style type="text/css">
td.fc-sat, td.fc-sun {
    background-color: rgba(255, 0, 0, 0.2);
}

</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.calendar')}}</h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            @include('admin.common.success')
            @include('admin.common.errors')      
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                    @foreach($stores as $store)
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{ $store->stor_name }}</h3>
                            </div>
                            <div class="card-body">
                                <p class="card-content"> - {{ $store->stor_address1 }}</p>
                                <br>
                                <div class="card-footer text-center">
                                    <a class="btn btn-xs btn-success" href="/store/calendar/view?store_id={{ $store->stor_id }}">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection