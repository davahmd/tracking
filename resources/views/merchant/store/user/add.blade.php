@extends('merchant.layouts.master')

@section('title', 'Add Store User')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>{{trans('localize.Add')}} {{trans('localize.store_user')}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/merchant/store/user">{{trans('localize.manage')}} {{trans('localize.store_user')}}</a>
                </li>
                <li class="active">
                    <strong>{{trans('localize.Add')}}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInUp">
        @include('admin.common.errors')
        @include('admin.common.error')
        @include('admin.common.success')
        @include('admin.common.status')

        @if ($check['customer'])
            <div class="alert alert-info alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                This email already used by a customer, proceed to add store user if want to join the account.
            </div>
        @endif
        <div class="row">
            <form id="create_submit" class="form" action='/merchant/store/user/add' method="POST">
                {{ csrf_field() }}
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>{{trans('localize.store_user')}} {{trans('localize.information')}}</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="form-group">
                                <label class="control-label">{{trans('localize.email')}}</label>
                                <input type="email" class="form-control" name="email" id="email" value="{{old('email', $email)}}" readonly>
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{trans('localize.Name')}} <span class='text-danger'>*</span></label>
                                <input type="text" class="form-control" name="name" id="name" value="{{old('name')}}">
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{trans('localize.Phone')}} </label>
                                <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone')}}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('localize.Stores')}} </label>
                                @foreach($stores->chunk(4) as $row)
                                    <div class="row">
                                        @foreach($row as $key => $store)
                                            <div class="col-md-3 m-b-xs">
                                                <input type="checkbox" class="i-checks" name="store_list[]" value="{{ $store->stor_id }}">
                                                <label>{{ $store->stor_name }}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 m-b">
                    <div class="col-md-2 pull-right">
                        <button type="button" id="submit_form" class="btn btn-primary">@lang('localize.submit')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="/backend/js/custom.js"></script>
    <script>

        $(document).ready(function() {

            $('#submit_form').click(function() {
                var usernameReg = /^[a-zA-Z0-9]*$/;

                if($('#name').val() == '') {
                    swal('Error!',"{{trans('localize.name_error')}}",'error');
                    return false;
                }

                var checkedNum = $('input[name="store_list[]"]:checked').length;
                if (!checkedNum) {
                    swal('Error!',"{{trans('localize.store_list_error')}}",'error');
                    return false;
                }

                swal({
                    title: "{{trans('localize.sure')}}",
                    text: "{{trans('localize.confirm_create_new_store_user')}}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#5cb85c",
                    confirmButtonText: "{{trans('localize.yes')}}",
                    cancelButtonText: "{{trans('localize.cancel')}}",
                    closeOnConfirm: true
                }, function(isConfirm) {
                    if(isConfirm) {
                        console.log('asd');
                        $("#create_submit").submit();
                    } else {
                        return false;
                    }
                });
            });

        });

    </script>
@endsection
