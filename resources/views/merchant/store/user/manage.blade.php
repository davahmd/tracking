@extends('merchant.layouts.master')

@section('title', trans('localize.manage_store_user'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>@lang('localize.manage_store_user')</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.store_user')
            </li>
            <li class="active">
                <strong>{{trans('localize.manage')}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
        <div class="col-lg-12">
            @include('merchant.common.success')
            @include('merchant.common.error')
            @include('merchant.common.errors')
            <div class="ibox">
                <div class="ibox-content">
                    <div class="ibox-tools">
                        <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#create_user">@lang('localize.create_account')</button>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">{{trans('localize.name')}}</th>
                                    <th class="text-center">{{trans('localize.phone')}}</th>
                                    <th class="text-center">{{trans('localize.username')}}</th>
                                    <th class="text-center">{{trans('localize.email')}}</th>
                                    <th class="text-center">@lang('localize.store_permission')</th>
                                    <th class="text-center">{{trans('localize.status')}}</th>
                                    <th class="text-center">{{trans('localize.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr class="text-center">
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-left">
                                        <ul>
                                            @foreach($user->stores as $store)
                                            <li><a class="nolinkcolor" href="/merchant/store/edit/{{ $store->stor_id}}" data-toggle="tooltip" title="" data-original-title="{{ trans('localize.update_store') }}">{{ $store->stor_name }}</a></li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>
                                        @if($user->status == 1)
                                        <label class="text-navy">@lang('localize.active')</label>
                                        @elseif($user->status == 0)
                                        <label class="text-warning">@lang('localize.inactive')</label>
                                        @endif
                                    </td>
                                    <td style="width:1%;">
                                        <a href="/merchant/store/user/edit/{{ $user->id }}" class="btn btn-white btn-block btn-sm"><i class="fa fa-pencil"></i> @lang('localize.edit')</a>
                                        @if($user->status == 0)
                                        <a href="/merchant/store/user/status/{{ $user->id }}/1" class="btn btn-white btn-block btn-sm text-navy"><i class="fa fa-refresh"></i> @lang('localize.set_to_active')</a>
                                        @elseif($user->status == 1)
                                        <a href="/merchant/store/user/status/{{ $user->id }}/0" class="btn btn-white btn-block btn-sm text-warning"><i class="fa fa-refresh"></i> @lang('localize.set_to_inactive')</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $users])

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="create_user" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <div class="row">
                    <div class="col-sm-3"><img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:35px;"></div>
                    <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3>@lang('localize.create_account')</h3></div>
                </div>
            </div>
            <div class="modal-body">
                <form id="create_user_submit" class="form-horizontal" action='/merchant/store/user/check' method="POST">
                {{ csrf_field() }}
                    <div class="form-group" id="emaildiv">
                        <label class="col-lg-3 control-label">@lang('localize.email') <span style="color:red;">*</span></label>
                        <div class="col-lg-9">
                            <input type="email" placeholder="@lang('localize.enter_email')" name="email" id="email" class="form-control" onchange="check_email_ajax(this.value);">
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">@lang('localize.closebtn')</button>
                <button type="button" id="submit" class="btn btn-primary">@lang('localize.create_account')</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>
@endsection

@section('script')
<script>
    $(document).ready(function() {

        $("#setpassword").change(function() {
            if(this.checked) {
                $("#confirmpassworddiv").show();
                $("#passworddiv").show();
                $("#emaildiv").hide();
                $('#email').removeAttr('value');

            }else{
                $("#confirmpassworddiv").hide();
                $("#passworddiv").hide();
                $("#emaildiv").show();
                $('#confirmpassword').removeAttr('value');
                $('#password').removeAttr('value');
            }
        });

        $('#submit').click(function() {
            if($('#email').val() == '') {
                swal("{{trans('localize.error')}}","{{trans('localize.email_error')}}",'error');
                return false;
            }

            swal({
                title: "{{trans('localize.sure')}}",
                text: "{{trans('localize.confirm_create_new_store_user')}}",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#5cb85c",
                confirmButtonText: "{{trans('localize.yes')}}",
                cancelButtonText: "{{trans('localize.cancel')}}",
                closeOnConfirm: true
            }, function(isConfirm) {
                if(isConfirm) {
                    $("#create_user_submit").submit();
                } else {
                    return false;
                }
            });

        });
    });

    function check_email_ajax(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var cemail = $('#email');

        if (!emailReg.test(cemail.val())) {
            swal("{{trans('localize.error')}}", "{{trans('localize.email_validation_error')}}", "error");
            cemail.css('border', '1px solid red');
            cemail.focus();
            return false;
        } else {
            var passemail = 'email=' + cemail.val();
            $.ajax({
                type: 'get',
                data: passemail,
                url: '/store_emailcheck',
                success: function (responseText) {
                    if (responseText) {
                        if (responseText > 0) {
                            swal("{{trans('localize.error')}}", cemail.val()+"{{trans('localize.email_check_error')}}", "error");
                            cemail.css('border', '1px solid red');
                            cemail.val('');
                            cemail.focus();
                            return false;
                        } else {
                            cemail.css('border', '');
                        }
                    }
                }
            });
        }
    }
</script>
@endsection
