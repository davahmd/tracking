@extends('layouts.backend_auth_master')

@section('content')
<div class="merchant-login-container">
    <div class="panel-login">
        <div class="brand">
            <img src="{{ asset('asset/images/logo/logo_merchant.png') }}">
        </div>
        
        <div class="panel-title selection">
            <ul>
                <li><a href="/merchant/login">@lang('localize.login_as_merchant')</a></li>
                <li class="active"><a href="/store/login">@lang('localize.login_as_store_user')</a></li>
            </ul>
        </div>

        <div class="form-container">
            @include('layouts.partials.status')
            <form role="form" method="POST" action="{{ url('store/login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label">Username</label>
                    <input type="text" name="username" class="form-control"  value="">
                </div>

                <div class="form-group">
                    <label class="control-label">Password</label>
                    <input type="password" name="password" class="form-control" >
                </div>
                <div class="action">
                    <button type="submit" class="btn btn-primary btn-block">@lang('localize.login')</button>
                </div>
            </form>

            <a href="{{ url('store/password/reset') }}" class="btn-forget">@lang('localize.forgot')</a>
        </div>
    </div>
</div>
@endsection
