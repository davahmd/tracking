@extends('layouts.backend_auth_master')

@section('style')
<style>
.field-icon {
  float: right;
  margin-left: -30px;
  margin-top: -30px;
  margin-right: 10px;
  position: relative;
  z-index: 2;
}
</style>
@endsection

@section('content')
<div class="merchant-login-container">
    <div class="panel-login">
        <div class="brand">
            <img src="{{ asset('asset/images/logo/logo_merchant.png') }}">
        </div>
        <div class="panel-title selection">
            <ul>
                <li class="active"><a href="/merchant/login">@lang('localize.login_as_merchant')</a></li>
                <li><a href="/store/login">@lang('localize.login_as_store_user')</a></li>
            </ul>
        </div>

        <div class="form-container">
            @include('layouts.partials.status')
            <form role="form" method="POST" action="{{ url('merchant/login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label">Username</label>
                    <input type="text" name="username" class="form-control" value="{{ old('username') }}">
                </div>

                <div class="form-group">
                    <label class="control-label">Password</label>
                    <input class="password" type="password" name="password" class="form-control">
                    <span id="show-pass" class="fa fa-eye field-icon" aria-hidden="true"></span>
                </div>

                <div class="action">
                    <button type="submit" class="btn btn-primary btn-block">@lang('localize.login')</button>
                </div>
            </form>

            <a href="{{ url('merchant/password/reset') }}" class="btn-forget">@lang('localize.forgot')</a>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#show-pass").on('click', function(event) {
                event.preventDefault();
                if($('.password').attr("type") == "text"){
                    $('.password').attr('type', 'password');
                    $('.custom-input span').addClass( "fa-eye-slash" );
                    $('.custom-input span').removeClass( "fa-eye" );
                }else if($('.password').attr("type") == "password"){
                    $('.password').attr('type', 'text');
                    $('.custom-input span').addClass( "fa-eye" );
                    $('.custom-input span').removeClass( "fa-eye-slash" );
                }
            });
        });
    </script>
@endsection