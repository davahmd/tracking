@extends('layouts.backend_auth_master')

@section('content')
<div class="merchant-login-container">
    <div class="panel-login">
        <div class="brand">
            <img src="{{ asset('asset/images/logo/logo_mechant.png') }}">
        </div>

        <div class="panel-title">Forget Password</div>

        <div class="form-container">
            @include('layouts.partials.status')
            <form role="form" method="POST" action="{{ url('merchant/password/reset') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <label class="control-label">@lang('localize.email')</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label class="control-label">@lang('localize.newpassword')</label>
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group">
                    <label class="control-label">@lang('localize.confirmnewpassword')</label>
                    <input type="password" name="password_confirmation" class="form-control">
                </div>

                <div class="action">
                    <button type="submit" class="btn btn-primary btn-block">@lang('localize.updatepassword')</button>
                </div>
            </form>
            <a href="{{ url('merchant/login') }}" class="btn-forget">@lang('localize.back')</a>
        </div>
    </div>
</div>

@endsection
