@extends('layouts.backend_auth_master')

@section('content')
<div class="merchant-login-container">
    <div class="panel-login">
        <div class="brand">
            <img src="{{ asset('asset/images/logo/logo_merchant.png') }}">
        </div>

        <div class="panel-title">@lang('localize.newpassword')</div>
        
        <div class="form-container">
            @include('layouts.partials.status')
              
            <form role="form" method="POST" action="{{ url('store/password/reset') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label">@lang('localize.email')</label>
                    <input type="email" name="email" class="form-control" value="{{ $email or old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="control-label">@lang('localize.newpassword')</label>
                    <input type="password" name="password" class="form-control">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label">@lang('localize.confirmnewpassword')</label>
                    <input type="password" name="password_confirmation" class="form-control">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            {{ $errors->first('password_confirmation') }}
                        </span>
                    @endif
                </div>

                <div class="action">
                    <button type="submit" class="btn btn-primary">@lang('localize.updatepassword')</button>
                </div>
            </form>

            <a href="{{ url('store/login') }}" class="btn-forget">@lang('localize.back')</a>
        </div>
    </div>
</div>
@endsection
