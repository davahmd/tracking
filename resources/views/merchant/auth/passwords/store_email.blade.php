@extends('layouts.backend_auth_master')

@section('content')
<div class="merchant-login-container">
    <div class="panel-login">
        <div class="brand">
            <img src="{{ asset('asset/images/logo/logo_merchant.png') }}">
        </div>

        <div class="panel-title">Forget Password</div>

        <div class="form-container">
            @include('layouts.partials.status')
            <form role="form" method="POST" action="{{ url('store/password/email') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label">@lang('localize.username')</label>
                    <input type="text" name="username" class="form-control" value="{{ old('username') }}">
                </div>
                
                <div class="action">
                    <button type="submit" class="btn btn-primary btn-block">@lang('localize.send_reset_email')</button>
                </div>
            </form>
            <a  href="{{ url('store/login') }}" class="btn-forget">@lang('localize.back')</a>
        </div>
    </div>
</div>
@endsection
