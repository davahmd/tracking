@extends('merchant.layouts.master')
@section('title', 'Manage Calendar')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/fullcalendar.css') }}">
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('backend/css/fullcalendar.print.css') }}">
<style type="text/css">
td.fc-sat, td.fc-sun {
    background-color: rgba(255, 0, 0, 0.2);
}
</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.calendar')}}</h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            @include('merchant.common.success')
            @include('merchant.common.errors')      
            <div class="ibox">
                <div class="ibox-content">
                    <div class="container text-center" style="height: 500px; width: auto;">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-title" style="text-transform: capitalize;">
                    @lang('calendar.offday-list')
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered table-hovered">
                        <thead>
                            <tr>
                                <th width="50%">@lang('localize.name')</th>
                                <th width="50%">@lang('localize.date')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($offdays->count() > 0)
                            @foreach($offdays as $offday)
                            <tr>
                                <td>{{ $offday->title }}</td>
                                @if(date('d-m-Y', strtotime($offday->start)) == date('d-m-Y',strtotime($offday->end . "-1 days")))
                                <td>{{ date('d-m-Y', strtotime($offday->start)) }}</td>
                                @else
                                <td>{{ date('d-m-Y', strtotime($offday->start)) }} - {{ date('d-m-Y',strtotime($offday->end . "-1 days")) }}</td>
                                @endif
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td class="text-center" colspan="2">No data</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <form id="form-whitelist" action="/merchant/calendar/offday/whitelist" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="merchant_id" value="{{ $merchant->mer_id }}">
                        <input type="hidden" id="whitelist-start" name="start" value="">
                        <input type="hidden" id="whitelist-end" name="end" value="">
                    </form>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                    @foreach($stores as $store)
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header" style="height: 80px">
                                <h3 class="card-title">{{ $store->stor_name }}</h3>
                            </div>
                            <div class="card-body"  style="height: 150px;">
                                <p class="card-content"> - {{ $store->stor_address1 }}</p>
                                <br>
                            </div>
                            <div class="card-footer text-center">
                                <a class="btn btn-xs btn-success" href="/merchant/calendar/store/view?store_id={{ $store->stor_id }}">View</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-holiday">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal" id="add-holiday-form" action='/merchant/calendar/offday/add' method="POST">
        {{ csrf_field() }}
        <div class="modal-header">
            <h4 class="modal-title" style="text-transform: capitalize;"><strong>@lang('calendar.add-offday')</strong></h4>
        </div>
        
        <div class="modal-body">
            <div class="form-group">
                <label class="col-md-2 control-label">@lang('localize.name')</label>
                <div class="col-md-10">
                    <input type="text" placeholder="etc: {{ trans('calendar.holidays.labor-day') }}..." class="form-control" id="name" name='name'>
                    <input type="hidden" name="merchant_id" value="{{ $merchant->mer_id }}">
                    <input type="hidden" name="start_date" id="start-date">
                    <input type="hidden" name="end_date" id="end-date">
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="form-group">
                <div class="col-md-3 pull-right">
                    <button class="btn btn-block btn-primary" id="add" type="button">@lang('localize.add')</button>
                </div>
                <div class="col-md-3 pull-right">
                    <button class="btn btn-block btn-default" id="cancel" type="button">@lang('localize.cancel')</button>
                </div>
            </div>
        </div>
        </form>
    </div>
  </div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="{{ asset('backend/lib/fullcalendar/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/fullcalendar/fullcalendar.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/fullcalendar/locale-all.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootbox.min.js') }}"></script>
<script type="text/javascript">

    var locale = "{{ config('app.locale') }}";
    var action = "addOffday";
    if (locale == "ind") {
        locale = 'id';
    }
    
    $('#calendar').fullCalendar({
        locale: locale,
        header: {
            left: '',
            center: 'title',
            right: 'prev,next today',
        },
        defaultDate: '{{ date("Y-m-d") }}',
        height: 'parent',
        aspectRation: 1.5,
        handleWindowResize: true,
        selectable: true,
        eventSources: [
            {
                events: <?php echo $holidays_json; ?>,
                color: 'red',
                className: 'fc-day-holiday',
            },
            {
                events: <?php echo $offdays_json; ?>,
                color: 'yellow',
                textColor: 'black',
                className: 'fc-day-offday',
            },
            {
                events: <?php echo $ondays_json; ?>,
                color: 'lightgreen',
                className: 'fc-day-whitelist',
                rendering: 'background',
            },
        ],
        selectOverlap: function(event) {

            if (typeof(event) != 'undefined') {

                var eventClass = event.source.className;

                if (eventClass.includes('fc-day-holiday') || eventClass.includes('fc-day-whitelist')) {

                    return true;
                }

                swal({
                    title: "{{ trans('localize.error') }}",
                    text: "{{ trans('calendar.error.overlap') }}",
                    type: "error",
                });

                return false;
                
            }

        },
        select: function(start,end) {

            $.get('/merchant/calendar/offday/check?date=' + start.format('Y-MM-DD')).done(function(holiday){

                if (holiday) {
                    if (holiday.status === "whitelisted") {
                        swal({
                            title: "{{ trans('calendar.warning.whitelisted-day') }}",
                            text: "{{ trans('calendar.warning.remove-whitelisted-day') }}",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "OK",
                            cancelButtonText: "{{ trans('localize.cancel') }}",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                window.location.replace("/merchant/calendar/offday/whitelist/remove/" + holiday.id);
                            }
                        });
                    }
                    else {
                        swal({
                            title: holiday.name,
                            text: "{{ trans('calendar.warning.whitelist-holiday') }}",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "OK",
                            cancelButtonText: "{{ trans('localize.cancel') }}",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $('#whitelist-start').val(start.format('Y-MM-DD'));
                                $('#whitelist-end').val(end.format('Y-MM-DD'));
        
                                $('#form-whitelist').submit();
                            }
                        });
                    }
                }
                else {
                    $('#start-date').val(start.format('Y-MM-DD'));
                    $('#end-date').val(end.format('Y-MM-DD'));
                    $('#add-holiday').modal('show');
                }
            });
        },
        eventClick: function(event) {

            if ($(this).hasClass('fc-day-offday')){
                swal({
                    title: event.title,
                    text: "{{ trans('calendar.warning.delete-holiday') }}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "OK",
                    cancelButtonText: "{{ trans('localize.cancel') }}",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        window.location.replace("/merchant/calendar/offday/remove/" + event.id);
                    }
                });
            }
        },
    });

    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#cancel').click(function(){
        $('#start-date').val("");
        $('#end-date').val("");
        $('#add-holiday').modal('hide');
    });

    $('#add').click(function(){
        $('#add-holiday-form').submit();
    });

    $('#calendar').fullCalendar({
        locale: locale,
    })

</script>
@endsection