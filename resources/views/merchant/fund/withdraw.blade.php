@extends('merchant.layouts.master')

@section('title', trans('localize.fund_withdraw'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.withdraw_fund_request')</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.fund')
            </li>
            <li class="active">
                <strong>@lang('localize.withdraw_fund_request')</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>@lang('localize.withdraw_fund_request')</h5>
                    <div class="ibox-tools">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">@lang('localize.view_bank_info')</button>
                    </div>
                </div>
                <div class="ibox-content">

                    @if ($requested && count($errors) == 0)
                        <div class="alert alert-danger">
                            <strong>@lang('localize.whoopsomethingwhenwrong')</strong><br>
                            <ul>
                                <li>@lang('localize.alreadywithdraw').</li>
                            </ul>
                        </div>
                    @endif

                    @php
                        // $commission = ($merchant->mer_type != 1)? (floatval($commission_amt) / 100) : 0;
                        $commission = 0;
                        $admin_commission = floatval($merchant->earning) * $commission;
                        $max_withdrawal = floatval($merchant->earning) - $admin_commission;
                        $exchangeRate = $merchant->mer_type == 1 ? $merchant->country->co_offline_rate :$merchant->country->co_rate;
                    @endphp

                    <form class="form-horizontal" action="{{ url('merchant/fund/withdraw') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                        <label class="col-lg-3 control-label">@lang('localize.merchant_earning')</label>
                            <div class="col-lg-9">
                                <label class="control-label">{{ rpFormat($merchant->earning) }}</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">@lang('localize.fund_withdrawal')</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" id="withdraw" name="withdraw" step="1" min="1" max="{{ $max_withdrawal }}" pattern="[0-9]" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">@lang('localize.statement_reference')</label>
                            <div class="col-lg-9">
                                <input type="file" class="form-control files" name="file" id="file">
                                <hr>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"></label>
                            <div class="col-lg-9">
                                <label id="balance" class="control-label">{{ rpFormat($merchant->earning) }}</label>
                                <label class="control-label">@lang('localize.balance_after_withdrawal')</label>
                            </div>
                        </div>

                        @if (!$requested)
                            <div class="form-group">
                                <div class="col-lg-2 pull-right">
                                    <button class="btn btn-block btn-primary" type="submit" id="total_to_pay_submit">@lang('localize.submit')</button>
                                </div>
                            </div>
                            <hr/>
                            <div class="alert alert-info" role="alert">
                                <strong>Disclaimer:</strong>
                                <br/>You can submit a fund withdrawal request after buyer confirming on shipment receiving.
                                <br/>You shall receive your payment after 5 days of the fund request.
                                <br/>If you request is made after 17:00 (GMT +8) on Friday, your request will be processed on coming Monday.
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('localize.bank_information')</h4>
            </div>
            <div class="modal-body">
                <div class="row form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-5 control-label">@lang('localize.account_holder_name')</label>
                        <div class="col-sm-7">
                             <p class="form-control-static">{{ $merchant->bank_acc_name}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label">@lang('localize.account_number')</label>
                        <div class="col-sm-7">
                             <p class="form-control-static">{{ $merchant->bank_acc_no}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label">@lang('localize.name_of_bank')</label>
                        <div class="col-sm-7">
                             <p class="form-control-static">{{ $merchant->bank_name}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label">@lang('localize.country_of_bank')</label>
                        <div class="col-sm-7">
                             <p class="form-control-static">{{ $merchant->co_name}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label">@lang('localize.address_of_bank')</label>
                        <div class="col-sm-7">
                             <p class="form-control-static">{{ $merchant->bank_address}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label">@lang('localize.Bank_swift_code_BIC_ABA')</label>
                        <div class="col-sm-7">
                             <p class="form-control-static">{{ $merchant->bank_swift}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label">@lang('localize.if_europe_bank_IBAN')</label>
                        <div class="col-sm-7">
                             <p class="form-control-static">{{ $merchant->bank_europe}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('localize.closebtn')</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('style')
<link href="/backend/lib/wysiwyg/wysihtml5.min.css" rel="stylesheet">
@endsection

@section('script')
<!-- plugins -->
<script src="/backend/lib/wysiwyg/wysihtml5x-toolbar.min.js"></script>
<script src="/backend/lib/wysiwyg/handlebars.runtime.min.js"></script>
<script src="/backend/lib/wysiwyg/wysihtml5.min.js"></script>

<script>
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

	$(document).ready(function() {

        var availableAmount = parseFloat("{{ ceil($merchant->earning) }}");

        $('.files').change(function() {
            var fileExtension = ['jpeg', 'jpg', 'png', 'pdf'];

            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                swal("@lang('localize.error')", "@lang('localize.file.error.type', ['values' => 'jpeg, jpg, png, pdf'])", "error");
                $(this).css('border', '1px solid red').focus().val('');
            } else if (($(this)[0].files[0].size) > 2000000){
                swal("@lang('localize.error')", "@lang('localize.file.error.maximum_size')", "error");
                $(this).css('border', '1px solid red').focus().val('');
            }
        });

        $('#withdraw').keypress(function (evt) {
            return isNumber(event, this)
        });

        $('#withdraw').keyup(function (e)
        {
            var withdraw = sanitizeMinMax($('#withdraw').val());
            var balance = parseFloat(availableAmount - withdraw);
            var currency_code = "{{ config('app.currency_code') }} ";
            $('#balance').text(currency_code + addCommas(balance));
        });

		$('#total_to_pay_submit').click(function()
        {
            if(isNaN($('#withdraw').val()) || !$('#withdraw').val() || parseFloat($('#withdraw').val()) > parseFloat(availableAmount) || $('#withdraw').val() <= 0)
			{
                $('#withdraw').css('border', '1px solid red').focus();
				return false;
			}
            $('#withdraw').css('border', '');

            var fileExtension = ['jpeg', 'jpg', 'png', 'pdf'];
            if(!$('#file').val()) {
                $('#file').css('border', '1px solid red').focus();
				return false;
            }

            if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                swal("@lang('localize.error')", "@lang('localize.file.error.type', ['values' => 'jpeg, jpg, png, pdf'])", "error");
                $('#file').css('border', '1px solid red').focus();
                return false;
            } else if (($('#file')[0].files[0].size) > 2000000){
                swal("@lang('localize.error')", "@lang('localize.file.error.maximum_size')", "error");
                $('#file').css('border', '1px solid red').focus();
                return false;
            }

            $('#spinner').show();
		});

        function isNumber(evt, element)
        {
            var charCode = (evt.which) ? evt.which : event.keyCode

            if (
                //(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function sanitizeMinMax(value)
        {
            if(!$.trim(value).length)
            {
                return parseFloat(0).toFixed(2);
            }

            value = parseFloat(value).toFixed(2);

            if(parseFloat(value) < 0)
            {
                value = 0.01;
                $('#withdraw').val(value);
            }

            if(parseFloat(value) > parseFloat(availableAmount))
            {
                value = availableAmount;
                $('#withdraw').val(value);
            }

            return parseFloat(value).toFixed(2);
        }

	});
</script>
@endsection
