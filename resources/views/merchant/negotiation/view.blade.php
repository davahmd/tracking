@extends('merchant.layouts.master')

@section('title', trans('localize.online_listing'))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>@lang('localize.price_negotiation')</h2>
            {{--  <ol class="breadcrumb">
                <li>
                    @lang('localize.price_negotiation')
                </li>
                <li>@lang('localize.online_orders')</li>
                <li class="active">
                    <strong>@lang('localize.listing')</strong>
                </li>
            </ol>  --}}
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">

        <div class="row">
            <div class="col-lg-12">

                @include('merchant.common.notifications')

                <div class="row">
                    <div class="col-md-6">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>Detail</h5>
                            </div>
                            <div class="ibox-content">
                                <form id="form-nego" action="{{ route('merchant.negotiation.offer', $negotiation) }}" method="POST">
                                    {{ csrf_field() }}
                                    <section class="product">
                                        <div class="form-group">
                                            <label>@lang('localize.status')</label>
                                            @if($negotiation->is_expired)
                                                <input class="form-control" value="{{ $negotiation->statusType()[$negotiation::STATUS['EXPIRED']] }}" readonly>
                                            @else
                                                <input class="form-control" value="{{ $negotiation->statusType()[$negotiation->status] }}" readonly>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('localize.customer')</label>
                                            <input class="form-control" value="{{ $negotiation->customer->cus_name }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('localize.product')</label>
                                            <input class="form-control" value="{{ $negotiation->product->title }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('localize.quantity')</label>
                                            <input class="form-control" value="{{ $negotiation->quantity }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('localize.price')</label>
                                            <input class="form-control" value="{{ $negotiation->merchant_offer_price ? rpFormat($negotiation->merchant_offer_price) : rpFormat($negotiation->customer_offer_price) }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('localize.expiry_date')</label>
                                            <input type="text" class="form-control offer-price" name="merchant_offer_price" value="{{ \App\Helpers\Helper::UTCtoTZ($negotiation->expired_at) }}" readonly>
                                        </div>
                                    </section>

                                    @if($negotiation->status == $negotiation::STATUS['ACTIVE'] && !$negotiation->merchant_offer_price && !$negotiation->is_expired)
                                        <section class="form">
                                            <div class="action">
                                                <a class="btn btn-accept-modal btn-success" href="{{ route('merchant.negotiation.accept', $negotiation) }}">
                                                    @lang('localize.accept') <i class="fa fa-spinner fa-spin"></i>
                                                </a>
                                            </div>

                                            <div class="hidden">
                                                <input type="hidden" name="nego_product_id">
                                                <input type="hidden" id="nego-pricing-id" name="nego_pricing_id">
                                            </div>
                                        </section>
                                        <hr>
                                        <div class="form-group">
                                            <label>@lang('localize.offer_price')</label>
                                            <input type="number" min="" max="" class="form-control offer-price" name="merchant_offer_price">
                                        </div>


                                        {{--<div class="form-group">--}}
                                            {{--<input type="checkbox" id="expiryDate">--}}
                                            {{--<label class="form-check-label" for="expiryDate">@lang('localize.expiry_date')</label>--}}
                                        {{--</div>--}}

                                        {{--<div class="form-group expired" style="display: none">--}}
                                            {{--<div class='input-group date' id='datetimepicker'>--}}
                                                {{--<input type='text' class="form-control" />--}}
                                                {{--<span class="input-group-addon">--}}
                                                {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                                            {{--</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group">
                                            <button type="button" class="btn btn-final btn-danger">
                                                @lang('localize.final_price')
                                                <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn btn-offer btn-primary">
                                                @lang('localize.offer')
                                                <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>History</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-stripped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Customer/Merchant</th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($negotiation->merchant_offer_price)
                                            <tr>
                                                <td>{{ Helper::UTCtoTZ($negotiation->updated_at) }}</td>
                                                <td>{{ $negotiation->merchant->full_name() }}</td>
                                                <td>{{ rpFormat($negotiation->merchant_offer_price) }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td>{{ Helper::UTCtoTZ($negotiation->created_at) }}</td>
                                            <td>{{ $negotiation->customer->cus_name }}</td>
                                            <td>{{ rpFormat($negotiation->customer_offer_price) }}</td>
                                        </tr>
                                        @if($negotiation->parent)
                                            @if($negotiation->parent->merchant_offer_price)
                                                <tr>
                                                    <td>{{ Helper::UTCtoTZ($negotiation->parent->updated_at) }}</td>
                                                    <td>{{ $negotiation->parent->merchant->full_name() }}</td>
                                                    <td>{{ rpFormat($negotiation->parent->merchant_offer_price) }}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td>{{ Helper::UTCtoTZ($negotiation->parent->created_at) }}</td>
                                                <td>{{ $negotiation->parent->customer->cus_name }}</td>
                                                <td>{{ rpFormat($negotiation->parent->customer_offer_price) }}</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        td {
            vertical-align: middle !important;
        }

        .fa-spin {
            display: none;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
@endsection

@section('script')
    <script src="/backend/js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#datetimepicker').datetimepicker();

            $('.btn-final').click(function () {
                $('#form-nego').append("<input type='hidden' name='final' value='1'>");
                $('#form-nego .btn-final').attr('disabled', true);
                $('#form-nego .btn-offer').attr('disabled', true);
                $('#form-nego .btn-accept-modal').attr('disabled', true);
                $('#form-nego .btn-final .fa-spin').css('display', 'block');
                $('#form-nego').submit();
            });

            $('.btn-offer').click(function() {
                $('#form-nego .btn-final').attr('disabled', true);
                $('#form-nego .btn-offer').attr('disabled', true);
                $('#form-nego .btn-accept-modal').attr('disabled', true);
                $('#form-nego .btn-offer .fa-spin').css('display', 'block');
                $('#form-nego').submit();
            });

            $('.btn-accept-modal').click(function() {
                $('#form-nego .btn-final').attr('disabled', true);
                $('#form-nego .btn-offer').attr('disabled', true);
                $('#form-nego .btn-accept-modal').attr('disabled', true);
                $('#form-nego .btn-accept-modal .fa-spin').css('display', 'block');
                $('#form-nego').submit();
            });

            $('#expiryDate').change(function () {
                if($(this).is(":checked")) {
                    $('.expired').show();

                } else {
                    $('.expired').hide();
                }
            });
        });
    </script>
@endsection
