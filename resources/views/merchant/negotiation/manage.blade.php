@extends('merchant.layouts.master')

@section('title', trans('localize.online_listing'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.price_negotiation')</h2>
        {{--  <ol class="breadcrumb">
            <li>
                @lang('localize.price_negotiation')
            </li>
            <li>@lang('localize.online_orders')</li>
            <li class="active">
                <strong>@lang('localize.listing')</strong>
            </li>
        </ol>  --}}
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
     <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" action="{{ route('merchant.negotiation.manage') }}" method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.customer') @lang('localize.id')" name="customer_id" value="{{ $input['customer_id'] }}">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.customer')" name="customer_name" value="{{ $input['customer_name'] }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.product') @lang('localize.id')" name="product_id" value="{{ $input['product_id'] }}">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="@lang('localize.product')" name="product_name" value="{{ $input['product_name'] }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.status')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="status">
                                <option value="">@lang('localize.all')</option>
                                @foreach($status_list as $key => $value)
                                <option value="{{ $key }}" {{ (!is_null($input['status']) && $input['status'] == $key) ? 'selected' : ''}}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.sort')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="new" {{ (!$input['sort'] || $input['sort'] == 'new') ? 'selected' : ''}}>@lang('localize.Newest')</option>
                                <option value="old" {{ ($input['sort'] == 'old') ? 'selected' : ''}}>@lang('localize.Oldest')</option>
                                <option value="customer_name_asc" {{ ($input['sort'] == 'customer_name_asc') ? 'selected':'' }}>{{trans('localize.customer')}} {{trans('localize.name')}} : &#xf15d;</option>
                                <option value="customer_name_desc" {{ ($input['sort'] == 'customer_name_desc') ? 'selected':'' }}>{{trans('localize.customer')}} {{trans('localize.name')}} : &#xf15e;</option>
                                <option value="product_name_asc" {{ ($input['sort'] == 'product_name_asc') ? 'selected':'' }}>{{trans('localize.product')}} {{trans('localize.name')}} : &#xf15d;</option>
                                <option value="product_name_desc" {{ ($input['sort'] == 'product_name_desc') ? 'selected':'' }}>{{trans('localize.product')}} {{trans('localize.name')}} : &#xf15e;</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary">@lang('localize.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            @include('merchant.common.notifications')

            <div class="ibox">

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <th nowrap class="text-center">@lang('localize.#id')</th>
                                    <th nowrap class="text-center">@lang('localize.customer')</th>
                                    <th nowrap class="text-center">@lang('localize.product')</th>
                                    <th nowrap class="text-center">@lang('localize.original_price')</th>
                                    <th nowrap class="text-center">@lang('localize.quantity')</th>
                                    <th nowrap class="text-center">@lang('localize.price_offer')</th>
                                    <th nowrap class="text-center">@lang('localize.status')</th>
                                    <th nowrap class="text-center">@lang('localize.expired_at')</th>
                                    <th nowrap class="text-center">@lang('localize.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($negotiations as $negotiation)
                                <tr>
                                    <td>{{ $loop->remaining + 1 }}</td>
                                    <td>{{ $negotiation->customer->cus_id }} - {{ $negotiation->customer->cus_name }}</td>
                                    <td>{{ $negotiation->product->title }}</td>
                                    <td class="text-center">{{ rpFormat($negotiation->pricing->price) }}</td>
                                    <td class="text-center">{{ $negotiation->quantity }}</td>
                                    <td class="text-center">{{ $negotiation->merchant_offer_price ? rpFormat($negotiation->merchant_offer_price) : rpFormat($negotiation->customer_offer_price) }}</td>
                                    <td class="text-center">
                                        @if($negotiation->is_expired)
                                            {{ $negotiation::statusType()[$negotiation::STATUS['EXPIRED']] }}
                                        @else
                                            {{ $negotiation::statusType()[$negotiation->status] }}
                                            @if($negotiation->status == 10 && $negotiation->merchant_offer_price != null)
                                                <br>@lang('localize.awaiting_customer_confirmation')
                                            @endif
                                        @endif
                                    </td>
                                    <td class="text-center">{{ \Helper::UTCtoTZ($negotiation->expired_at) }}</td>
                                    <td class="text-center">
                                        @if ($negotiation->status == 10 && $negotiation->merchant_offer_price == null && !$negotiation->is_expired)
                                            <button class="btn btn-success btn-xs btn-accept" data-id="{{ $negotiation->id }}"><i class="fa fa-check"></i> @lang('localize.accept')</button>
                                            <button class="btn btn-danger btn-xs btn-decline" data-id="{{ $negotiation->id }}"><i class="fa fa-times"></i> @lang('localize.decline')</button>
                                        @endif
                                        {{--<button class="btn btn-primary btn-xs btn-view" data-toggle="modal" data-target="#nego-detail" data-id="{{ $negotiation->id }}"><i class="fa fa-comments"></i> @lang('localize.view')</button>--}}
                                            <a  class="btn btn-primary btn-xs" href="{{ route('merchant.negotiation.view', $negotiation) }}"><i class="fa fa-comments"></i> @lang('localize.view')</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $negotiations])

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="nego-detail" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang('localize.price_negotiation')</h4>
            </div>
            <div class="modal-body">
                <form id="form-nego" method="POST">
                {{ csrf_field() }}
                <section class="product">
                    <div class="thumb">
                        
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.customer')</label>
                        <input class="form-control customer-name" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.product')</label>
                        <input class="form-control product-name" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.quantity')</label>
                        <input class="form-control quantity" readonly>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.price')</label>
                        <input class="form-control price" readonly>
                    </div>
                </section>

                <section class="form">
                    <div class="action">
                        <a type="button" class="btn btn-accept-modal btn-success" href="">
                            @lang('localize.accept')
                            <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="hidden">
                        <input type="hidden" name="nego_product_id">
                        <input type="hidden" id="nego-pricing-id" name="nego_pricing_id">
                    </div>
                </section>

                <hr>

                <div class="form-group">
                    <label>@lang('localize.offer_price')</label>
                    <input type="number" min="" max="" class="form-control offer-price" name="merchant_offer_price">
                </div>

                <div class="form-group">
                    <input type="checkbox" id="expiryDate">
                    <label class="form-check-label" for="expiryDate">@lang('localize.expiry_date')</label>
                </div>

                <div class="form-group">
                    <div class='input-group date' id='datetimepicker'>
                        <input type='text' class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-final btn-danger">
                        @lang('localize.final_price')
                        <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-offer btn-primary">
                        @lang('localize.offer')
                        <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                    </button>
                </div>

                    <hr>

                    <h4 class="modal-title">@lang('localize.negotiation_history')</h4>

                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                            <th>Date</th>
                            <th>Customer/Merchant</th>
                            <th>Price</th>
                            <th>Expiry Date</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </form>
            </div>
      </div>

    </div>
</div>
@endsection

@section('style')
<style>
    td {
        vertical-align: middle !important;
    }

    .fa-spin {
        display: none;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script>
$(document).ready(function() {
    $('.btn-accept').on('click', function(){
        var this_id = $(this).attr('data-id');
        swal({
            title: "{{trans('localize.sure')}}",
            text: "{{trans('localize.nego_msg.confirm_to_accept')}}",
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "{{trans('localize.Accept')}}",
            cancelButtonText: "{{trans('localize.cancel')}}",
            closeOnConfirm: false
        }, function(){
            var url = '/merchant/negotiation/accept/' + this_id;
            window.location.href = url;
        });
    });

    $('.btn-decline').on('click', function(){
        var this_id = $(this).attr('data-id');
        swal({
            title: "{{trans('localize.sure')}}",
            text: "{{trans('localize.nego_msg.confirm_to_decline')}}",
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "{{trans('localize.decline')}}",
            cancelButtonText: "{{trans('localize.cancel')}}",
            closeOnConfirm: false
        }, function(){
            var url = '/merchant/negotiation/decline/' + this_id;
            window.location.href = url;
        });
    });

    $('.btn-view').on('click', function() {
        var this_id = $(this).data('id');

        $.ajax({
            type: 'GET',
            url: '/get_negotiation_detail/' + this_id,
            success: function(data) {
                console.log(data);
                var url = '/merchant/negotiation/offer/' + this_id;

                if (data.attempt === 2) {
                    $('.btn-offer').hide();
                } else {
                    $('.btn-offer').show();
                }

                $('#nego-detail form').attr('action', url);
                $('#nego-detail .customer-name').val(data.customer.cus_name);
                $('#nego-detail .product-name').val(data.product.pro_title_en);
                $('#nego-detail .quantity').val(data.quantity);
                $('#nego-detail .price').val(rpFormat('{{ config("app.currency_code") }}', data.customer_offer_price));
                $('#nego-detail .offer-price').attr('min', data.customer_offer_price);
                $('#nego-detail .offer-price').attr('max', data.pricing.price);
                $('#nego-detail .btn-accept-modal').attr('href', '/merchant/negotiation/accept/' + this_id)
            }
        });

        $.ajax({
            type: 'GET',
            url: '/get_admin_negotiation_detail/' + this_id,
            success: function(data) {
                $('#nego-detail table tbody').empty();

                $.each(data, function(i) {;
                    $('#nego-detail table tbody').append(
                        '<tr>' +
                        '<td>' + data[i].created_at + '</td>' +
                        '<td>' + data[i].customer.cus_name + '</td>' +
                        '<td>' + rpFormat('{{ config("app.currency_code") }}', data[i].customer_offer_price) + '</td>' +
                        '<td>' + data[i].expired_at + '</td>' +
                        '</tr>' + (data[i].merchant_offer_price ?
                        '<tr>' +
                        '<td>' + data[i].updated_at + '</td>' +
                        '<td>' + data[i].product.store.stor_name + '</td>' +
                        '<td>' + rpFormat('{{ config("app.currency_code") }}', data[i].merchant_offer_price) + '</td>' +
                        '<td>' + data[i].expired_at + '</td>' +
                        '</tr>' : '')
                    );
                });
            }
        });
    });

    $('#datetimepicker').datetimepicker();

    $('.btn-final').click(function () {
        $('#nego-detail form').append("<input type='hidden' name='final' value='1'>");
        $('#nego-detail .btn-final').attr('disabled', true);
        $('#nego-detail .btn-final .fa-spin').css('display', 'block');
        $('#nego-detail form').submit();
    });

    $('.btn-offer').click(function() {
        $('#nego-detail .btn-offer').attr('disabled', true);
        $('#nego-detail .btn-offer .fa-spin').css('display', 'block');
        $('#nego-detail form').submit();
    });

    $('.btn-accept-modal').click(function() {
        $('#nego-detail .btn-accept-modal').attr('disabled', true);
        $('#nego-detail .btn-accept-modal .fa-spin').css('display', 'block');
        $('#nego-detail form').submit();
    });
});
</script>
@endsection
