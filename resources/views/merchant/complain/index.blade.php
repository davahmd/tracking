@extends('merchant.layouts.master')

@section('title', trans('localize.complain.listing.page_title'))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>{{trans('localize.complain.complaint')}}</h2>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="ibox float-e-margins border-bottom">
            <a class="collapse-link nolinkcolor">
                <div class="ibox-title ibox-title-filter">
                    <h5>@lang('localize.Search_Filter')</h5>
                    <div class="ibox-tools">
                        <i class="fa fa-chevron-down"></i>
                    </div>
                </div>
            </a>
            <div class="ibox-content ibox-content-filter" style="display:none;">
                <div class="row">
                    <form class="form-horizontal" id="filter" action='{{ route('merchant.complain::index') }}' method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{trans('localize.customer')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ old('customer_search') }}" placeholder="{{trans('localize.Search_By_Customer_Common_Fields')}}" class="form-control" name="customer_search">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{trans('localize.ID.transaction')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ old('transaction_id') }}" placeholder="{{trans('localize.ID.transaction')}}" class="form-control" id="transaction_id" name="transaction_id">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{trans('localize.Title')}}</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="subject_id" name="subject_id">
                                    <option value="" {{ (old('subject_id') ==  '') ? 'selected' : '' }}>@lang('localize.selectOption')</option>
                                    @foreach ($subjects as $subject)
                                        <option value="{{ $subject->id }}" {{ (old('subject_id') ==  $subject->id) ? 'selected' : '' }}>{{$subject->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-2">
                                <button type="submit" class="btn btn-block btn-outline btn-primary" id="filter">{{trans('localize.search')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                @include('merchant.common.success')
                @include('merchant.common.error')
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center text-nowrap">#ID</th>
                                        <th class="text-center text-nowrap">@lang('localize.complain.table.transaction_id')</th>
                                        <th class="text-nowrap text-center">@lang('localize.complain.table.complainant')</th>
                                        <th class="text-center text-nowrap">@lang('localize.complain.table.subject')</th>
                                        <th class="text-center text-nowrap">@lang('localize.complain.table.escalated')</th>
                                        <th class="text-center text-nowrap">@lang('localize.complain.table.replies')</th>
                                        <th class="text-center text-nowrap">{{trans('localize.Action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($complains->isEmpty())
                                        <tr>
                                            <td colspan="6">@lang('localize.table_no_record')</td>
                                        </tr>
                                    @endif

                                    @foreach ($complains as $complain)
                                        <tr>
                                            <td>{{$complain->getKey()}}</td>
                                            <td>
                                                {{$complain->complainable->transaction_id ?: 'N/A'}}
                                            </td>
                                            <td>
                                                <strong>{{ trans('localize.name') }}</strong><br/>
                                                {{$complain->complainant_name}}

                                                <br/><br/>
                                                <strong>{{ trans('localize.email') }}</strong><br/>
                                                {{$complain->complainant_email}}
                                            </td>
                                            <td class="text-nowrap">
                                                {{$complain->subject->title}}
                                            </td>
                                            <td class="text-nowrap">
                                                {{$complain->is_escalated ? trans('localize.yes') : trans('localize.no')}}
                                            </td>
                                            <td class="text-center">{{$complain->complain_messages_count}}</td>
                                            <td class="text-center" width="5%">
                                                <p>
                                                    <a class="btn btn-info btn-sm"
                                                       href="{{ route('merchant.complain::messages', [$complain]) }}">
                                                        <span><i class="fa fa-comment"></i> @lang('localize.respond')</span>
                                                    </a>
                                                </p>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    @include('layouts.partials.table-pagination', ['listings' => $complains])
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="/backend/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
@endsection

@section('script')
    <script src="/backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
    <script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.js"></script>
    <script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.min.js"></script>

    <script>


    </script>
@endsection
