@extends('merchant.layouts.master')

@section('title', trans('localize.chat'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{trans('localize.chat')}}</h2>
    </div>
</div>

<div class="row wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="chat-container">

                @foreach($store_groups as $store_group)
                    @php
                        if (count($store_group) == 0) {
                            continue;
                        }
                        $store_group = $store_group->sortByDesc('id');
                        $store_chat = $store_group->first();
                        $store_unread = $store_group->where('sender_type', 'U')->where('read_at', null)->first() ? true : false;
                    @endphp

                    <div id="chat_store_{{ $store_chat->store_id }}">
                        <div class="chat-item" data-target="#chat_store_{{ $store_chat->store_id }}_cus" aria-expanded="false">
                            <a href="javascript:void(0)">
                                <div class="d-flex pd-sm">
                                    <div class="chat-image">
                                        <img src="{{ \Storage::url('store/'.$store_chat->store_id.'/'.$store_chat->store->stor_img) }}" onerror="this.src = '{{ asset('asset/images/logo/logo_merchant.png') }}'">
                                    </div>
                                    <div class="chat-content flex-grow-1">
                                        <h3 class="chat-title">{{ $store_chat->store->stor_name ?? '' }}</h3>
                                        <p class="chat-text">
                                            @php
                                                $name = $store_chat->sender_type == 'U' ? $store_chat->customer->cus_name : $store_chat->store->stor_name;
                                                $msg = $store_chat->message ?? '';
                                                $timestamp = \App\Helpers\DateTimeHelper::systemToOperationDateTime($store_chat->created_at, 'j/m/Y g:h A');
                                            @endphp
                                            {{ $name }}: {!! preg_replace('/[\r\n]/', '<br>', str_limit($msg ?? '', 100, '...')) !!}
                                        </p>
                                        <p class="info">{{ $timestamp }}</p>
                                    </div>
                                </div>
                            </a>

                            <div class="collapse" id="chat_store_{{ $store_chat->store_id }}_cus">
                                @php
                                    $store_group = $store_group->groupBy('cus_id')->all();
                                @endphp

                                @foreach ($store_group as $customer_group)
                                    @php
                                        if (count($customer_group) == 0) {
                                            continue;
                                        }
                                        $customer_group = $customer_group->sortByDesc('id');
                                        $customer_chat = $customer_group->first();
                                        $customer_unread = $customer_group->where('sender_type', 'U')->where('read_at', null)->first() ? true : false;
                                    @endphp

                                    <div id="chat_store_{{ $customer_chat->store_id }}_cus_{{ $customer_chat->cus_id }}" class="chat-item" data-target="#chat_store_{{ $customer_chat->store_id }}_cus_{{ $customer_chat->cus_id }}_pro" aria-expanded="false">
                                        <a href="javascript:void(0)">
                                            <div class="d-flex pd-sm">
                                                <div class="chat-image sub-image">
                                                    <img src="{{ $customer_chat->customer->getAvatarUrl() }}" onerror="this.src = '{{ asset('asset/images/icon/icon_account2.png') }}';">
                                                </div>
                                                <div class="chat-content flex-grow-1">
                                                    <h3 class="chat-title">{{ $customer_chat->customer->cus_name ?? '' }}</h3>
                                                </div>
                                            </div>
                                        </a>

                                        <div class="collapse" id="chat_store_{{ $store_chat->store_id }}_cus_{{ $customer_chat->cus_id }}_pro">
                                            @php
                                                $customer_group = $customer_group->groupBy('pro_id')->all();
                                            @endphp

                                            @foreach ($customer_group as $product_group)
                                                @php
                                                    if (count($product_group) == 0) {
                                                        continue;
                                                    }
                                                    $product_group = $product_group->sortByDesc('id');
                                                    $product_chat = $product_group->first();
                                                    $product_unread = $product_group->where('sender_type', 'U')->where('read_at', null)->first() ? true : false;
                                                @endphp

                                                <div id="chat_store_{{ $product_chat->store_id }}_cus_{{ $product_chat->cus_id }}_pro_{{ $product_chat->pro_id }}" class="chat-item">
                                                    <a class="chat-open" href="{{ route('merchant-chat-conversation', ['customer_id' => $product_chat->cus_id, 'product_id' => $product_chat->pro_id]) }}" target="_blank" data-store="{{ $product_chat->store_id }}" data-cus="{{ $product_chat->cus_id }}">
                                                        <div class="d-flex pd-sm">
                                                            <div class="chat-image sub-sub-image">
                                                                <img src="{{ $product_chat->product->mainImageUrl() }}" onerror="this.src = '{{ asset('asset/images/product/default.jpg') }}'">
                                                            </div>
                                                            <div class="chat-content flex-grow-1">
                                                                <h3 class="chat-title">{{ $product_chat->product->title }}</h3>
                                                                <p class="chat-text">
                                                                    @php
                                                                        $name = $product_chat->sender_type == 'U' ? $product_chat->customer->cus_name : $product_chat->store->stor_name;
                                                                        $msg = $product_chat->message;
                                                                        $timestamp = \App\Helpers\DateTimeHelper::systemToOperationDateTime($product_chat->created_at, 'd/m/Y g:h A');
                                                                    @endphp
                                                                    {{ $name }}: {!! preg_replace('/[\r\n]/', '<br>', str_limit($msg ?? '', 100, '...')) !!}
                                                                </p>
                                                                <p class="info">{{ $timestamp }}</p>
                                                            </div>
                                                        </div>
                                                        <span class="notice{{ $product_unread ? '' : ' hidden' }}"></span>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>

                                        <span class="notice{{ $customer_unread ? '' : ' hidden' }}"></span>
                                    </div>

                                @endforeach
                            </div>
                            
                            <span class="notice{{ $store_unread ? '' : ' hidden' }}"></span>
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(function() {
    $('.chat-open').click((e) => {
        chatModule.merchantOpenChat(e);
    });

    $('.chat-item').click((e) => {
        let target = $(e.currentTarget).data('target');
        $(target).collapse('toggle');
        e.stopPropagation();
    });
})
</script>
@endsection