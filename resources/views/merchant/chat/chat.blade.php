@extends('merchant.layouts.master')

@section('title', trans('localize.chat'))

{{-- @section('style')
<style type="text/css">

</style>
@endsection --}}

@section('content')
<div class="row wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8">
            <div class="chat-container single">
                <div class="chat-item bg-white">
                    <div class="chat-item-top pd-sm">
                        <div class="chat-image">
                            <img src="{{ $customer_img }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                        </div>
                        <div class="chat-product-box">
                            <div class="chat-content">
                                <h3 class="chat-title">{{ $customer_name }}</h3>
                                <p class="info">{{ $latest_timestamp }}</p>
                                <p id="connection" class="text-danger">@lang('localize.connection_issue')</p>
                            </div>
                        </div>
                        <div class="product">
                            <div class="chat-image xs">
                                <img src="{{ $product_img }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                            </div>
                            <h4><a href="{{ url('product/'.\Helper::slug_maker($product_title, $product_id)) }}" target="_blank">{{ $product_name }}</a></h4>
                            @foreach ($services as $service)
                                <br><a href="javascript:void(0)" style="line-height: 0px;">{{ $service->service_name_current }}: {{ \Carbon\Carbon::parse($service->schedule_datetime)->format('j/m/y g:i A') }}</a>
                            @endforeach
                        </div>
                    </div>
                    <div id="chat-item-body" class="chat-item-body">
                    
                        <ul>
                            @foreach($chats as $chat)
                                <li class="chat-{{ ($chat->sender_type == 'S') ? 'right' : 'left' }}">
                                    <div class="chat-inner" data-chat="{{ $chat->id }}">
                                        <p>{!! preg_replace('/[\r\n]/', '<br>', $chat->message) !!}</p>
                                        <span class="date">{{ Helper::UTCtoTZ($chat->created_at, 'j/m/y g:i A') }}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                    <div class="chat-item-bottom pd-sm d-flex">
                        <textarea id="msg" class="flex-grow-1" rows="1" placeholder="@lang('localize.write_message')" maxlength="500"></textarea>
                        <button id="send" type="button" class="btn btn-red"><span id="i_send">@lang('localize.send')<i class="material-icons">send</i></span><i id="i_spinner" class="fa fa-spinner fa-pulse hidden" style="margin: 0 auto"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('asset/js/autosize.min.js') }}"></script>
<script>
$(function() {
    scrollToBottom('chat-item-body');
    $('#msg').focus();
    autosize($('#msg'));

    $('#msg').keydown((e) => {
        if (e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            $('#send').click();
        }
    });

    $('#send').click(() => {
        let msgValue = $('#msg').val().trim();
        if (!msgValue) {
            $('#msg').val('');
            autosize.update($('#msg'));
            return;
        }

        $('#i_send').addClass('hidden');
        $('#i_spinner').removeClass('hidden');

        $.ajax({
            url: '/merchant/chat/' + '{{ $customer_id }}' + '/' + '{{ $product_id }}',
            type: 'post',
            dataType: 'json',
            data: {
                msg: msgValue
            }
        })
        .done((data) => {
            if (data.status === true) {
                let message = $('<div/>').text(msgValue).html();
                $('#chat-item-body ul').append(chatModule.buildMessage('right', message));
                $('.info').html(moment().format('D/M/YY h:mm A'))
                scrollToBottom('chat-item-body');
                $('#msg').val('');
                autosize.update($('#msg'));
            }
        })
        .fail((data) => {
            setInterval(() => {
                if ($('#connection').css('visibility') == 'hidden') {
                    $('#connection').css('visibility', 'visible');
                }
                else {
                    $('#connection').css('visibility', 'hidden');
                }
            }, 1000);
        })
        .always((data) => {
            $('#i_spinner').addClass('hidden');
            $('#i_send').removeClass('hidden');
        });
    });

    Echo.join('chat.{{ $customer_id }}.{{ $store_id }}.{{ $product_id }}')
        .listen('MessageCreated', (e) => {
            $('#chat-item-body ul').append(chatModule.buildMessage(e.chat.sender_type == 'U' ? 'left' : 'right', e.chat.message));
            scrollToBottom('chat-item-body');
        });
});
</script>
@endsection