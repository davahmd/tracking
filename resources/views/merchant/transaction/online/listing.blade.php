@extends('merchant.layouts.master')

@section('title', trans('localize.online_listing'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.online_transaction_listing')</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.transaction')
            </li>
            <li>@lang('localize.online_orders')</li>
            <li class="active">
                <strong>@lang('localize.listing')</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
     <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" action="{{ route('merchant.online.transaction', ['type' => $type]) }}" method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['transaction_id'] }}" placeholder="@lang('localize.transID')" name="transaction_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['customer_name'] }}" placeholder="@lang('localize.customer')" name="customer_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.transaction_date')</label>
                        <div class="col-sm-9">
                            <div class="input-daterange input-group">
                                <input type="text" class="form-control" name="start_date" id="sdate" placeholder="@lang('localize.startDate')" value="{{ $input['start_date'] }}"/>
                                <span class="input-group-addon">@lang('localize.to')</span>
                                <input type="text" class="form-control" name="end_date" id="edate" placeholder="@lang('localize.endDate')" value="{{ $input['end_date'] }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.status')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="status">
                                <option value="">@lang('localize.all')</option>
                                @foreach($status_list as $key => $value)
                                <option value="{{ $key }}" {{ (!is_null($input['status']) && $input['status'] == $key) ? 'selected' : ''}}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.sort')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="new" {{ (!$input['sort'] || $input['sort'] == 'new') ? 'selected' : ''}}>@lang('localize.Newest')</option>
                                <option value="old" {{ ($input['sort'] == 'old') ? 'selected' : ''}}>@lang('localize.Oldest')</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary">@lang('localize.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            @include('merchant.common.notifications')

            <div class="ibox">

                <div class="ibox-title" style="display: block;">
                    <div class="ibox-tools" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"> @lang('localize.export.all') <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('merchant.export.online.transaction', ['type' => $type]) . "?export=all&export_as=csv&" . http_build_query($input) }}">Csv</a></li>
                                <li><a href="{{ route('merchant.export.online.transaction', ['type' => $type]) . "?export=all&export_as=xlsx&" . http_build_query($input) }}">Xlsx</a></li>
                                <li><a href="{{ route('merchant.export.online.transaction', ['type' => $type]) . "?export=all&export_as=xls&" . http_build_query($input) }}">Xls</a></li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-white btn-sm dropdown-toggle"> @lang('localize.export.page') <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('merchant.export.online.transaction', ['type' => $type]) . "?export=page&export_as=csv&" . http_build_query($input) }}">Csv</a></li>
                                <li><a href="{{ route('merchant.export.online.transaction', ['type' => $type]) . "?export=page&export_as=xlsx&" . http_build_query($input) }}">Xlsx</a></li>
                                <li><a href="{{ route('merchant.export.online.transaction', ['type' => $type]) . "?export=page&export_as=xls&" . http_build_query($input) }}">Xls</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <th nowrap class="text-center">@lang('localize.#id')</th>
                                    <th nowrap class="text-center">@lang('localize.transID')</th>
                                    <th nowrap class="text-center">
                                        @if($type == 'retail')
                                            @lang('localize.customer')
                                        @else
                                            @lang('localize.retailer')
                                        @endif
                                    </th>
                                    <th nowrap class="text-center">@lang('localize.store')</th>
                                    <th nowrap class="text-center">@lang('localize.amount')</th>
                                    <th nowrap class="text-center">@lang('localize.merchant_charge')</th>
                                    <th nowrap class="text-center">@lang('localize.shipping_fees')</th>
                                    <th nowrap class="text-center">@lang('localize.merchant_earning')</th>
                                    <th nowrap class="text-center">@lang('localize.date')</th>
                                    <th nowrap class="text-center">@lang('localize.action')</th>
                                </tr>
                            </thead>
                            @foreach($transactions as $transaction)
                                    <tr class="text-center">
                                        <td rowspan="{{ $transaction->items->groupBy('store_id')->count() }}">{{ $transaction->id }}</td>
                                        <td rowspan="{{ $transaction->items->groupBy('store_id')->count() }}">
                                            {{-- <span style="cursor: pointer; color: #337ab7;" onclick="view_order_details({{ $transaction->id }}, 'merchant')">{{ $transaction->transaction_id }}</span> --}}
                                            {{ $transaction->transaction_id }}
                                        </td>
                                        <td rowspan="{{ $transaction->items->groupBy('store_id')->count() }}">
                                            @if($type == 'retail')
                                                {{ $transaction->customer->customer_name }}
                                            @else
                                                {{ $transaction->customer->user->merchant->full_name() }}
                                            @endif
                                        </td>
                                        @foreach($transaction->items->groupBy('store_id') as $store_id => $items)
                                    @if(!$loop->first)
                                    <tr class="text-center">
                                    @endif
                                    @php
                                        $item = $items->first();
                                        $normalProduct = $items->whereIn('order_type', [1,2]);
                                        $virtualProduct = $items->whereIn('order_type', [3,4,5]);
                                        if($normalProduct->count() > 0)
                                        {
                                            $status = $normalProduct->sortBy('order_status')->first()->status();
                                        }
                                        else
                                        {
                                            $status = $virtualProduct->sortBy('order_status')->first()->status();
                                        }
                                    @endphp
                                        <td>{{ $item->store_id . ' - ' . $item->store_name }}</td>
                                        <td>{{ ($item->currency_code == 'IDR') ? rpFormat($items->sum('order_price')) : $item->currency_code . ' ' . number_format($items->sum('order_price'), 2) }}</td>
                                        <td>{{ ($item->currency_code == 'IDR') ? rpFormat($items->sum('merchant_charge_value')) : $item->currency_code . ' ' . number_format($items->sum('merchant_charge_value'), 2) }}</td>
                                        <td>{{ ($item->currency_code == 'IDR') ? rpFormat($items->sum('shipping_fees_value')) : $item->currency_code . ' ' . number_format($items->sum('shipping_fees_value'), 2) }}</td>
                                        <td>{{ ($item->currency_code == 'IDR') ? rpFormat($items->sum('merchant_earn_value')) : $item->currency_code . ' ' . number_format($items->sum('merchant_earn_value'), 2) }}</td>
                                        <td>{{ \Helper::UTCtoTZ($transaction->transaction_date) }}</td>
                                        <td>
                                            <p>
                                                {{ $status }}
                                            </p>
                                            <p>
                                                <a href="{{ route('merchant.online.transaction.detail', [$item->parent_order_id, $item->store_id]) }}" class="btn btn-info btn-xs btn-block">@lang('localize.view_order')</a>
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            <tr class="text-center">
                                <td colspan="4" class="text-right font-bold">@lang('localize.total')</td>
                                <td class="font-bold">{{ rpFormat($total->order_price) }}</td>
                                <td class="font-bold">{{ rpFormat($total->merchant_charge_value) }}</td>
                                <td class="font-bold">{{ rpFormat($total->shipping_fees_value) }}</td>
                                <td class="font-bold">{{ rpFormat($total->merchant_earn_value) }}</td>
                                <td colspan="100"></td>
                            </tr>
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $transactions])

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
    td {
        vertical-align: middle !important;
    }
</style>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
@endsection
