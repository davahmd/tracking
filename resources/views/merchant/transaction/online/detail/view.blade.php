@extends('merchant.layouts.master')

@section('title', trans('localize.transDetail'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.transDetail')</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.transaction')
            </li>
            <li>
                @lang('localize.online_orders')
            </li>
            <li class="active">
                <strong>@lang('localize.detail')</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-8 text-right">
        <p>
            <strong>@lang('localize.transID')</strong><br> {{ $transaction->transaction_id }}
        </p>
        <p>
            <i class="fa fa-calendar m-r-sm"></i><b>@lang('localize.transaction_date')</b><br> {{ \Helper::UTCtoTZ($transaction->date) }}
        </p>
    </div>

</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">

        <div class="col-lg-12">
            @include('merchant.common.notifications')
        </div>

        @if($shipping)
        <div class="col-lg-6">
            <div class="contact-box">
                <div class="col-sm-12">
                    <legend><h3>@lang('localize.shipping_information')</h3></legend>
                    <p>
                        <strong>{{ $shipping->ship_name }}</strong>
                    </p>
                    <p>
                        <i class="fa fa-map-marker m-r-sm"></i><b>@lang('localize.shippingaddress')</b><br>
                        {!! ucwords(implode('<br>', array_map('trim', array_filter([$shipping->ship_address1, $shipping->ship_address2, $shipping->ship_city_name])))) !!} <br>
                        {{ ucwords(trim(implode(', ', array_filter([$shipping->ship_postalcode, $shipping->state? $shipping->state->name : null, $shipping->country? $shipping->country->co_name : null])))) }}
                    </p>
                    <p>
                        <i class="fa fa-phone m-r-sm"></i> {{ $shipping->phone() }}
                    </p>
                    <p><br></p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        @endif
        @if($merchant)
        <div class="col-lg-6">
            <div class="contact-box">
                <div class="col-sm-12">
                    <legend><h3>@lang('localize.mer_information')</h3></legend>
                    <p>
                        <strong>{{ $merchant->mer_id }} - {{ $merchant->mer_fname }} {{ $merchant->mer_lname }}</strong>
                    </p>
                    <p>
                        <i class="fa fa-map-marker m-r-sm"></i><b>@lang('localize.address')</b> <br>
                        {!! ucwords(implode('<br>', array_map('trim', array_filter([$merchant->mer_address1, $merchant->mer_address2, $merchant->mer_city_name])))) !!} <br>
                        {{ ucwords(trim(implode(', ', array_filter([$merchant->zipcode, $merchant->state? $merchant->state->name : null, $merchant->country? $merchant->country->co_name : null])))) }}
                    </p>
                    <p>
                        <i class="fa fa-envelope-o m-r-sm"></i> {{ $merchant->email? $merchant->email : '-' }}
                    </p>
                    <p>
                        <i class="fa fa-phone m-r-sm"></i> {{ $merchant->mer_phone }} &nbsp;&nbsp;
                        @if($merchant->mer_office_number)
                        <i class="fa fa-building-o m-r-sm"></i> {{ $merchant->mer_office_number }}
                        @endif
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        @endif

        <form id="update_orders" action="{{ route('merchant.update.order.status', [$transaction->id, $store_id]) }}" method="POST">
        {{ csrf_field() }}
        <div class="col-lg-12">

            <div class="ibox">

                @if($actions)
                <div class="ibox-title text-right">
                    <div class="btn-group btn-group-md" data-toggle="buttons">
                        @foreach ($actions as $value => $action)
                        <label type="button" class="btn {{ $action->button }} btn-outline">
                            <input type="radio" name="action" value="{{ $value }}" class="btn-toggle">{{ $action->text }}
                        </label>
                        @endforeach
                    </div>
                </div>
                @endif

                <div class="ibox-content">

                    @foreach ($orders as $group => $items)

                    @if(in_array($group, [1,2]))
                    <h3>@lang('localize.shipping_fees_type') :
                        @if($group == 1)
                        @lang('localize.shipping_fees_free'), @lang('localize.shipping_fees_product'), @lang('localize.shipping_fees_transaction')
                        @elseif($group == 2)
                        @lang('localize.self_pickup')
                        @endif
                    </h3>
                    @else
                    <h3>@lang('localize.product_types') : @lang('localize.virtual')</h3>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">

                            <thead>
                                <tr>
                                    <th class="text-center">@lang('localize.#id')</th>
                                    <th class="text-center" width="20%">@lang('localize.product')</th>
                                    <th class="text-center">@lang('localize.price')</th>
                                    <th class="text-center">@lang('localize.quantity')</th>
                                    <th class="text-center">@lang('localize.subtotal')</th>
                                    <th class="text-center">@lang('localize.merchant_charge')</th>
                                    <th class="text-center">@lang('localize.shipping_fees')</th>
                                    <th class="text-center">@lang('localize.amount') @lang('localize.Paid')</th>
                                    <th class="text-center">@lang('localize.merchant_earning')</th>
                                    <th class="text-center">@lang('localize.courier_info') / @lang('localize.service_info')</th>
                                    <th class="text-center">@lang('localize.status')</th>
                                    @if($group == 0)
                                    <th class="text-center">@lang('localize.action')</th>
                                    @endif
                                    <th class="text-center text-nowrap">
                                        {{--  <div class="i-checks">
                                            <label>
                                                <input type="checkbox" id="check_all">
                                            </label>
                                        </div>  --}}
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($items as $order)
                                @include('merchant.transaction.online.detail.partial.table_data', ['order' => $order, 'group' => $group])
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endforeach

                    <div class="update-orders" style="display: none;">

                        <div class="col-lg-12 nopadding shipment-form">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3><i class="fa fa-truck"></i> <span id="form-heading">{{ ucwords(trans('localize.create_shipment')) }}</span></h3>
                                </div>
                                <div class="panel-body m-md">
                                    <div class="form-group invoice-input-div">
                                        <label>@lang('localize.invoice.number')</label>
                                        <input type="text" class="form-control invoice_number-text invoice-input required" readonly>
                                    </div>

                                    <div class="form-group courier-input-div">
                                        <label>@lang('localize.corier')</label>
                                        <select class="form-control courier-input required" name="courier_id">
                                            <option value="" selected>-- @lang('localize.selectcorier') --</option>
                                            @foreach ($couriers as $courier)
                                            <option value="{{ $courier->id }}">{{ $courier->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group tracking-input-div">
                                        <label> @lang('localize.trackingno').</label>
                                        <input type="text" class="form-control tracking-input required" name="tracking_number">
                                    </div>

                                    <div class="form-group appointment-input-div">
                                        <label>@lang('localize.remarks')</label>
                                        <textarea type="text" class="form-control appointment-input required" name="appointment_detail" row="5" style="resize: none;" rows="5"></textarea>
                                    </div>

                                    <div class="form-group remarks-input-div">
                                        <label>@lang('localize.remarks')</label>
                                        <textarea type="text" class="form-control remarks-input" name="remarks" row="5" style="resize: none;" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 pull-right nopadding no-margin">
                            <div class="col-lg-6">
                                <button type="button" id="cancel_action" class="btn btn-danger btn-outline btn-block" style="display: none;" >@lang('localize.cancel')</button>
                            </div>
                            <div class="col-lg-6" style="padding-right: 0;">
                                <button type="button" id="submit_button" class="btn btn-primary btn-success btn-block">@lang('localize.submit')</button>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

        @if($displayInvoice)
        <div class="col-lg-12 nopadding">

            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1"> @lang('localize.invoice.tax_history')</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-2"> @lang('localize.do.history')</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-stripped table-bordered">
                                    <thead>
                                        <th class="text-center">@lang('localize.invoice.number')</th>
                                        <th class="text-center">@lang('localize.items')</th>
                                        <th class="text-center">@lang('localize.date')</th>
                                        <th class="text-center">@lang('localize.action')</th>
                                    </thead>
                                    <tbody>

                                        @foreach($invoices as $invoice)
                                        @if(!empty($invoice->items->first()))
                                        <tr class="text-center">
                                            <td>{{ $invoice->tax_number() }}</td>
                                            <td>
                                                @foreach($invoice->items as $order)
                                                @lang('localize.order_id') - {{ $order->order_id }}@if(!$loop->last)<br>@endif
                                                @endforeach
                                            </td>
                                            <td>{{ \Helper::UTCtoTZ($invoice->created_at) }}</td>
                                            <td>
                                                <button type="button" class="btn btn-white btn-block btn-sm" data-toggle="modal" data-id="{{ $invoice->id }}" data-post="data-php" data-action="details"><i class="fa fa-file-text-o"></i> @lang('localize.view_details')</button>
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-stripped table-bordered">
                                    <thead>
                                        <th class="text-center">@lang('localize.do.number')</th>
                                        {{-- <th class="text-center">@lang('localize.invoice.number')</th> --}}
                                        <th class="text-center">@lang('localize.items')</th>
                                        <th class="text-center">@lang('localize.date')</th>
                                        <th class="text-center">@lang('localize.action')</th>
                                    </thead>
                                    <tbody>

                                        @foreach($deliveries as $do)
                                        <tr class="text-center">
                                            {{-- <td>{{ $do->do_number() }}</td> --}}
                                            <td>{{ $do->invoice->tax_number('ON') }}</td>
                                            <td>
                                                @foreach($do->items as $order)
                                                @lang('localize.order_id') - {{ $order->order_id }}@if(!$loop->last)<br>@endif
                                                @endforeach
                                            </td>
                                            <td>{{ \Helper::UTCtoTZ($do->created_at) }}</td>
                                            <td>
                                                 <button type="button" class="btn btn-white btn-block btn-sm" data-toggle="modal" data-id="{{ $do->id }}" data-post="data-php" data-action="deliveries"><i class="fa fa-file-text-o"></i> @lang('localize.view_details')</button>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endif
        </form>
    </div>
</div>
@endsection

@section('style')
<style>
    td{
        vertical-align: middle !important;
    }
    th {
        white-space: nowrap !important;
    }
</style>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>

<script>
$(document).ready(function() {

    var action_text = null;

    $('#cancel_action').on('click', function() {
        $('.btn-toggle').removeAttr('checked');
        $('.btn.btn-outline').removeClass('active');
        $('.update-orders').hide();
        $('.input_checkbox').iCheck('uncheck');
        $('.input_checkbox').iCheck('disable');
        $(this).hide();
        action_text = null;
    });

    $('button').on('click', function(){
            var this_id = $(this).attr('data-id');
            var this_action = $(this).attr('data-action');
            var type = 'merchant';

            if (this_action == 'details') {
                view_tax_online_order(this_id,type);
            }else if(this_action == 'deliveries'){
                view_deliveries_order(this_id,type);
            }
        });

    $('.btn-toggle').on('change', function(event) {

        $('#cancel_action').show();
        var action = $(this).val();

        $('.update-orders').show();
        $('.shipment-form').hide();
        $('.input_checkbox').iCheck('uncheck');
        $('.input_checkbox').iCheck('disable');
        $('.invoice-input').removeClass('required');
        $('.tracking-input').removeClass('required');
        $('.courier-input').removeClass('required');
        $('.appointment-input').removeClass('required');
        $('.remarks-input').removeClass('required');

        switch (action) {
            case 'accept_order':
                action_text = "@lang('localize.update_order.accept_order')";
                auto_checkedBox(1, true, 1, null, true);
                break;

            case 'create_shipment':
                action_text = "@lang('localize.update_order.create_shipment')";
                $('#form-heading').text("{{ ucwords(trans('localize.create_shipment')) }}");
                $('.shipment-form').show();
                $('.invoice-input-div').show();
                $('.tracking-input-div').show();
                $('.courier-input-div').show();
                $('.appointment-input-div').hide();
                $('.remarks-input-div').hide();
                $('.invoice-input').addClass('required');
                $('.tracking-input').addClass('required');
                $('.courier-input').addClass('required');
                $('.appointment-input').removeClass('required');
                $('.remarks-input').removeClass('required');
                auto_checkedBox(2, true, 1, [0,1,2], false);
                get_invoice(1, 1);
                break;

            case 'arrange_pickup':
                action_text = "@lang('localize.update_order.arrange_pickup')";
                $('#form-heading').text("{{ ucwords(trans('localize.arrange_for_self_pickup')) }}");
                $('.shipment-form').show();
                $('.invoice-input-div').show();
                $('.tracking-input-div').hide();
                $('.courier-input-div').hide();
                $('.appointment-input-div').show();
                $('.remarks-input-div').hide();
                $('.invoice-input').addClass('required');
                $('.tracking-input').removeClass('required');
                $('.courier-input').removeClass('required');
                $('.appointment-input').addClass('required');
                $('.remarks-input').removeClass('required');
                auto_checkedBox(2, true, 1, 3, true);
                get_invoice(1, 2);
                break;

            case 'cancel_order':
                action_text = "@lang('localize.update_order.cancel_order')";
                auto_checkedBox([1,2,3], false, [1,3,4], null, true);
                break;

            case 'refund_order':
                action_text = "@lang('localize.update_order.refund_order')";
                auto_checkedBox(4, false, [1,3,4], null, true);
                break;

            case 'complete_order':
                action_text = "@lang('localize.update_order.complete_order')";
                $('#form-heading').text("{{ ucwords(trans('localize.product_is_received')) }}");
                $('.shipment-form').show();
                $('.invoice-input-div').hide();
                $('.tracking-input-div').hide();
                $('.courier-input-div').hide();
                $('.appointment-input-div').hide();
                $('.remarks-input-div').show();
                $('.invoice-input').removeClass('required');
                $('.tracking-input').removeClass('required');
                $('.courier-input').removeClass('required');
                $('.appointment-input').removeClass('required');
                $('.remarks-input').addClass('required');
                auto_checkedBox(3, true, 1, 3, true);
                break;

            default:
                event.preventDefault();
                swal("@lang('localize.swal_error')", "@lang('localize.invalid_operation')", 'error');
                $('.update-orders').hide();
                break;
        }
    });

    $('#submit_button').click(function () {

        var formValid = true;
        if (!$("#update_orders :checkbox:checked").is(":checked")) {
            swal("@lang('localize.swal_error')", "@lang('localize.please_tick_checkbox')", "error");
            return false;
        }

        $('#update_orders :input').each(function() {
            if ($(this).hasClass('required')) {
                if (!$(this).val()) {
                    $(this).closest('.form-group').addClass('has-error');
                    formValid = false;
                    return false;
                }
            }
            $(this).closest('.form-group').removeClass('has-error');
        });

        if(formValid)
        {
            swal({
                title: window.translations.sure,
                text: action_text + "</br><span class='text-danger'>@lang('localize.undone_action')</span>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#5cb85c",
                confirmButtonText: window.translations.yes,
                cancelButtonText: window.translations.cancel,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                html: true,
            }, function(){
                $('#spinner').show();
                $('#update_orders').submit();
            });
        }
    });

    $('.btn-code-view').on('click', function(event) {
        event.preventDefault();

        var this_id = $(this).attr('data-id');
        var this_action = $(this).attr('data-action');

        if (this_action == 'view_coupon') {true
            get_code_number_listing(this_id, 'merchant', 'coupons');
        } else if (this_action == 'view_ticket') {
            get_code_number_listing(this_id, 'merchant', 'tickets');
        } else if (this_action == 'view_ecard') {
            get_code_number_listing(this_id, 'merchant', 'ecard');
        }
    });

    function auto_checkedBox(status, checked = true, order_type = null, shipment_type = null, disabled = false)
    {
        $('.input_checkbox').each(function () {

            var valid = false;

            var dataStatus = parseInt($(this).attr('data-status'));
            var dataOrderType = parseInt($(this).attr('data-order-type'));
            var dataShipmentType = parseInt($(this).attr('data-shipment-type'));

            if($.isArray(status) && $.inArray(dataStatus, status) !== -1)
            {
                valid = true;
            }
            else {
                if(dataStatus == status)
                {
                    valid = true
                }
            }

            if(valid && order_type)
            {
                valid = false;
                if($.isArray(order_type) && $.inArray(dataOrderType, order_type) !== -1)
                {
                    valid = true;
                } else {
                    if(dataOrderType == order_type) {
                        valid = true;
                    }
                }
            }

            if(valid && shipment_type)
            {
                valid = false;
                if($.isArray(shipment_type) && $.inArray(dataShipmentType, shipment_type) !== -1)
                {
                    valid = true;
                } else {
                    if(dataShipmentType == shipment_type) {
                        valid = true;
                    }
                }
            }

            if(valid)
            {
                $(this).iCheck('enable');
                if(checked) {
                    $(this).iCheck('check');
                }

                if(disabled) {
                    $(this).iCheck('disable');
                }
            }
        });
    }

    function get_invoice(item_type, shipment_type)
    {
        var invoices = {!! json_encode($invoices) !!};
        $(invoices).each(function (index, invoice) {
            if(invoice.item_type == item_type && invoice.shipment_type == shipment_type)
            {
                $('.invoice_number-id').val(invoice.id);
                $('.invoice_number-text').val(invoice.tax_number_full);
            }
        });
    }
});
</script>
@endsection