@extends('merchant.layouts.master')

@section('title', trans('localize.manage_products'))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{trans('localize.manage_products')}}</h2>
        <ol class="breadcrumb">
            <li>
                {{trans('localize.product')}}
            </li>
            <li class="active">
                <strong>{{trans('localize.manage_products')}}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="ibox-content m-b-sm border-bottom">
        <div class="row">
            <form id="filter" action="{{ url($route . '/product/manage') }}" method="GET">
                <div class="col-sm-1">
                    <div class="form-group">
                        <input type="text" value="{{$input['id']}}" placeholder="{{trans('localize.#id')}}" class="form-control" id="id" name="id">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="text" value="{{$input['name']}}" placeholder="{{trans('localize.product_name')}}" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <input type="text" value="{{$input['sku']}}" placeholder="{{trans('localize.sku_code')}}" class="form-control" name="sku">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <select class="form-control" id="status" name="status">
                            <option value="" {{ ($input['status'] == "") ? 'selected' : '' }}>{{trans('localize.status')}}</option>
                            <option value="1" {{ ($input['status'] == "1") ? 'selected' : '' }}>{{trans('localize.active')}}</option>
                            <option value="0" {{ ($input['status'] == "0") ? 'selected' : '' }}>{{trans('localize.inactive')}}</option>
                            <option value="2" {{ ($input['status'] == "2") ? 'selected' : '' }}>{{trans('localize.incomplete')}}</option>
                            <option value="3" {{ ($input['status'] == "3") ? 'selected' : '' }}>{{trans('localize.pending_review')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <select class="form-control" id="sort" name="sort" style="font-family:'FontAwesome', sans-serif;">
                            <option value="id_asc" {{ ($input['sort'] == "id_desc") ? 'selected' : '' }}>{{trans('localize.#id')}} : &#xf162;</option>
                            <option value="id_desc" {{ ($input['sort'] == "" || $input['sort'] == "id_desc") ? 'selected' : '' }}>{{trans('localize.#id')}} : &#xf163;</option>
                            <option value="name_asc" {{ ($input['sort'] == "name_asc") ? 'selected' : '' }}>{{trans('localize.productName')}} : &#xf15d;</option>
                            <option value="name_desc" {{ ($input['sort'] == "name_desc") ? 'selected' : '' }}>{{trans('localize.productName')}} : &#xf15e;</option>
                            <option value="new" {{($input['sort'] == 'new') ? 'selected' : ''}}>{{trans('localize.newest')}}</option>
                            <option value="old" {{($input['sort'] == 'old') ? 'selected' : ''}}>{{trans('localize.oldest')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-block btn-outline btn-primary" id="filter">{{trans('localize.search')}}</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @include('merchant.common.success')
            @include('merchant.common.error')
            <div class="ibox">
                <div class="ibox-content">
                    <div class="table-responsive">
                        @if($logintype == 'merchants')
                        <div class="col-lg-2 col-lg-offset-10" style="padding-right: 0;">
                            <a href="/{{ $route }}/product/add" class="btn btn-primary btn-block btn-md">@lang('localize.add_products')</a>
                        </div>
                        <hr>
                        @endif
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">{{trans('localize.#id')}}</th>
                                    <th class="text-nowrap" width="70%">{{trans('localize.productName')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.store')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.details')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.category')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.sku_code')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.product_image')}}</th>
                                    <th class="text-center text-nowrap" width="15%">{{trans('localize.action')}}</th>
                                    <th class="text-center text-nowrap">{{trans('localize.status')}}</th>
                                    {{-- <th class="text-center text-nowrap">{{trans('localize.preview_live')}}</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $key => $product)
                                    <tr class="text-center">
                                        <td>{{ $product->pro_id}}</td>
                                        <td class="text-left">{{ $product->pro_title_en}}</td>
                                        <td class="text-nowrap">{{ $product->store ? $product->store->stor_name : trans('localize.store_unavailable') }}</td>
                                        {{--  <td>{{ $product->pro_no_of_purchase}}</td>  --}}
                                        <td class="text-left text-nowrap">
                                            <p>
                                                @lang('localize.type') :
                                                @if($product->pro_type == 1)
                                                    @lang('localize.normal_product')
                                                @elseif($product->pro_type == 2)
                                                    @lang('localize.coupon')
                                                @elseif($product->pro_type == 3)
                                                    @lang('localize.ticket')
                                                @elseif($product->pro_type == 4)
                                                    @lang('localize.e-card.name')
                                                @endif
                                            </p>
                                            <p>@lang('localize.quantity') : {{ $product->pro_qty }}</p>
                                        </td>
                                        <td class="text-nowrap">{{ $product->categories? $product->categories->first()->name : '' }}</td>
                                        <td class="text-nowrap">
                                            @foreach ($product->pricing as $price)
                                            <p class="focushover text-navy" data-placement="auto bottom" style='cursor: pointer;' data-html='true' data-content=
                                            "
                                                <p class='text-center text-navy'>{{ $price->country->co_name }}</p>
                                                <p>@lang('localize.price') : {{ ($price->country->co_cursymbol == 'Rp') ? rpFormat($price->price) : $price->country->co_cursymbol . ' ' . number_format($price->price, 2) }}</p>
                                                <p>@lang('localize.quantity') : {{ $price->quantity }}</p>
                                                @if($price->attributes_name)
                                                @foreach(json_decode($price->attributes_name) as $at => $value)
                                                <p>{{ '$at : $value' }}</p>
                                                @endforeach
                                                @endif
                                                @if($price->discounted_price != 0.00)
                                                    <p>@lang('localize.discounted') : {{ ($price->country->co_cursymbol == 'Rp') ? rpFormat($price->discounted_price) : $price->country->co_cursymbol . ' ' . number_format($price->discounted_price, 2) }}</p>
                                                    <p>@lang('localize.from') : {{ \Helper::UTCtoTZ($price->discounted_from) }}</p>
                                                    <p>@lang('localize.to') : {{ \Helper::UTCtoTZ($price->discounted_to) }}</p>
                                                @endif
                                            ">{{ $price->sku }}
                                            </p>
                                            @endforeach
                                        </td>
                                        @php
                                            $image = $product->images? $product->images->first()->imagePath : '';
                                        @endphp
                                        <td width="10%">
                                            <a href="{{ $image }}" title="{{ "{$product->pro_id} - {$product->pro_title_en}" }}" data-gallery=""><img src="{{ $image }}" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';" class="img-responsive img-thumbnail"></a>
                                        </td>
                                        <td class="text-nowrap">
                                            <p>
                                                <a class="btn btn-white btn-sm btn-block" href="{{ url($route . '/product/edit', [$product->pro_id]) }}"><span><i class="fa fa-edit"></i> {{trans('localize.edit')}}</span></a>
                                            </p>
                                            <p>
                                                <a href="{{ url($route . '/product/view', [$product->pro_id]) }}" class="btn btn-white btn-sm btn-block"><span><i class="fa fa-file-text-o"></i> {{trans('localize.view')}}</span></a>
                                            </p>

                                            <p>
                                                <a style="width:100%" class="btn btn-white btn-sm" href="{{ route('merchant.product.review', [$product->pro_id]) }}"><span><i class="fa fa-star"></i> @lang('localize.review')</span></a>
                                            </p>

                                            @if($product->pro_type == 4)
                                            <p>
                                                <a style="width:100%" class="btn btn-white btn-sm" href="{{ url($route.'/product/code/listing', [$product->pro_id]) }}"><span><i class="fa fa-barcode"></i> @lang('localize.e-card.view')</span></a>
                                            </p>
                                            @endif

                                        </td>
                                        <td>
                                            @if (($product->pro_status) == 1)
                                                <a target="_blank" class="btn btn-outline btn-link btn-sm btn-block" href="/products/detail/{{$product->pro_id}}"><span><i class='fa fa-search'></i> {{trans('localize.preview')}}</span></a>
                                                <span class="text-nowrap text-navy"><i class='fa fa-check'></i> {{trans('localize.active')}}</span>
                                            @elseif (($product->pro_status) == 0)
                                                <span class="text-nowrap text-warning"><i class='fa fa-ban'></i> {{trans('localize.inactive')}}</span>
                                            @elseif (($product->pro_status) == 2)
                                                <span class="text-nowrap text-danger"><i class='fa fa-wrench'></i> @lang('localize.incomplete')</span>
                                            @elseif (($product->pro_status) == 3)
                                                <span class="text-nowrap text-danger"><i class='fa fa-warning'></i> @lang('localize.pending_review')</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $products])

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="/backend/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
@endsection

@section('script')
<script src="/backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.js"></script>
<script src="/backend/js/plugins/Typehead/bootstrap3-typeahead.min.js"></script>

<script>
    $(document).ready(function() {

        $(".focushover").popover({ trigger: "hover" });

        var link = $('#search_type').val();
        $.get('/'+link, function(data){
            $("#name").typeahead({
                source: data
            });
        },'json');

        $('#search_type a').on('click', function(e) {
            e.preventDefault();
            $("#search_type li").removeClass("select");
            $(this).closest('li').addClass('select');
            $('#search_type').val($(this).data('value'));

            var link = $(this).data('value');
            $("#name").typeahead('destroy');
            $.get('/'+link, function(data){
                $("#name").typeahead({
                    source: data
                });
            },'json');
        });

    });

    function set_value() {
        var link = $('#search_type').val();
        $("#name").typeahead('destroy');

        $.get('/'+link, function(data){
            $("#name").typeahead({
                source: data
            });
        },'json');
    }

</script>
@endsection
