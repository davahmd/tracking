@extends('merchant.layouts.master')
@section('title', 'View Promotion')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>{{trans('localize.view')}} {{trans('localize.promotion')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/merchant/promotion/manage">{{trans('localize.promotion')}}</a>
            </li>
            <li class="active">
                <strong>{{trans('localize.view')}} {{trans('localize.promotion')}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{trans('localize.promotion_info')}}</h5>
                    <div class="ibox-tools">
                    	@if($promotion->status == 0 && $promotion->request != 'A')
                        <a href="/merchant/promotion/edit/{{$promotion->id}}" class="btn btn-primary btn-sm">{{trans('localize.Edit')}} {{trans('localize.promotion')}}</a>
                        @endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-horizontal">
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.campaign_name')}}</label>
                            <div class="col-lg-10"><p class="form-control-static">{{ $promotion->name ? $promotion->name : 'Campaign Unavaiable' }}</p></div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.period')}}</label>
                    		<div class="col-lg-10"><p class="form-control-static">{{ $promotion->started_at ? $promotion->started_at->format('d/m/Y') : 'Period Unavailable' }} - {{ $promotion->ended_at ? $promotion->ended_at->format('d/m/Y') : 'Period Unavailable' }}</p></div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.store')}}</label>
                    		<div class="col-lg-10">
                                <p class="form-control-static">
                                    @if (empty($products) && empty($categories))
                                    <ul class="list-group">
                                    @forelse ($stores as $store)
                                        <li class="list-group-item">{{ $store->stor_name }}</li>
                                    @empty
                                    @endforelse
                                    </ul>
                                    @else
                                        Not Available
                                    @endif
                                </p>
                            </div>
                    	</div>

                    	@if ($promotion->promo_type === 'P')
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.category')}}</label>
                    		<div class="col-lg-10">
                                <p class="form-control-static">
                                    @if (empty($product))
                                    <ul class="list-group">
                                    @forelse ($categories as $category)
                                        <li class="list-group-item">{{ $category->name_en }}</li>
                                    @empty
                                    @endforelse
                                    </ul>
                                    @else
                                        Not Available
                                    @endif
                                </p>
                            </div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.product')}}</label>
                    		<div class="col-lg-10">
                                <p class="form-control-static">
                                    <ul class="list-group">
                                    @forelse ($products as $product)
                                        <li class="list-group-item">{{ $product->pro_title_en }}</li>
                                    @empty
                                        Not Available
                                    @endforelse
                                    </ul>
                                </p>
                            </div>
                    	</div>
                    	@endif

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.discount_type')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				{{ ($promotion->discount_rate) ? trans('localize.percentage') : '' }}
                    				{{ ($promotion->discount_value) ? trans('localize.amount') : '' }}	
	                    		</p>
	                    	</div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.value')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				{{ ($promotion->discount_rate) ? $promotion->discount_rate*100 . '%' : '' }}
                    				{{ ($promotion->discount_value) ? rpFormat($promotion->discount_value) : '' }}
	                    		</p>
	                    	</div>
                    	</div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.promo_min_spend')}}</label>
                            <div class="col-lg-10">
                                <p class="form-control-static">{{ ($promotion->promo_min_spend) ? rpFormat($promotion->promo_min_spend) : 'No Minimum Spend' }}</p>
                            </div>
                        </div>

                    	@if ($promotion->promo_type === 'P')
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.applies_to')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				<label>
						            	<input class="i-checks" type="checkbox" class="form-control" {{ ($promotion->applied_to != 1) ? 'checked' : '' }} disabled> Products
						            </label>
						            <label>
						            	<input class="i-checks" type="checkbox" class="form-control" {{ ($promotion->applied_to != 0) ? 'checked' : '' }} disabled> Shipping Fee
						            </label>
                    			</p>
                    		</div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.promo_code')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                    				{{ $promotion->promo_code }}
                    			</p>
                    		</div>
                    	</div>

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.quantity')}}</label>
                    		<div class="col-lg-10"><p class="form-control-static">{{ ($promotion->promo_code_limit) ? $promotion->promo_code_limit : 'No Limit' }}</p></div>
                    	</div>

                    	@endif

                    	@if ($promotion->promo_type === 'F')
                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.added_product')}}</label>
                    		<div class="col-lg-10">
                    			<table class="table">
                    				<thead>
                    					<th>{{trans('localize.product')}}</th>
                    					<th>{{trans('localize.quantity')}}</th>
                    				</thead>
                    				<tbody>
                    					@foreach ($promotion->promotion_products as $product)
                    					<tr>
                    						<td>{{ $product->detail->pro_title_en }}</td>
                    						<td>{{ $product->limit }}</td>
                    					</tr>
                    					@endforeach
                    				</tbody>
                    			</table>
                    		</div>
                    	</div>
                    	@endif

                    	<div class="form-group">
                    		<label class="col-lg-2 control-label">{{trans('localize.status')}}</label>
                    		<div class="col-lg-10">
                    			<p class="form-control-static">
                				@if ($promotion->status == -1)
                					<span class="badge badge-danger">Cancelled</span>
		                    	@elseif ($promotion->status == 0)
		                    		<span class="badge badge-warning">Inactive</span>
                                    <span class="badge badge-warning">{{ ($promotion->request == 'A') ? 'Waiting for Approval' : '' }}</span> 
		                    	@elseif ($promotion->status == 1)
		                    		<span class="badge badge-success">Active</span>
                                    <span class="badge badge-primary">{{ ($promotion->request == 'C') ? 'Cancellation Requested' : '' }}</span>
		                    	@endif
	                    		</p>
	                    	</div>
                    	</div>

                        @if($promotion->status == -1)
                            @if($promotion->log->reason)
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('localize.reason')}}</label>
                                <div class="col-lg-10">
                                    <p class="form-control-static">
                                        {{ $promotion->log->reason }}
                                    </p>
                                </div>
                            </div>
                            @endif
                        @endif

                        @if ($promotion->status == 0 && $promotion->request == null)
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.action')}}</label>
                            <div class="col-lg-10">
                                <p class="form-control-static">
                                    <button data-promotion-id="{{ $promotion->id }}" class="btn btn-success button_submit">{{trans('localize.submit')}}</button>
                                    <button data-promotion-id="{{ $promotion->id }}" class="btn btn-danger button_cancel">{{trans('localize.cancel')}}</button>
                                </p>
                            </div>
                        </div>
                        @elseif ($promotion->status == 1 && $promotion->request == null)
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.action')}}</label>
                            <div class="col-lg-10">
                                <p class="form-control-static">
                                    <button data-promotion-id="{{ $promotion->id }}" class="btn btn-danger button_cancel">{{trans('localize.cancel')}}</button>
                                </p>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$('.button_submit').on('click', function(){
    var promotion_id = $(this).data('promotion-id');
    swal({
        title: "{{trans('localize.sure')}}",
        text: "{{trans('localize.confirm_submit_promotion')}}",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d9534f",
        confirmButtonText: "{{trans('localize.yes_submit_it')}}",
        cancelButtonText: "{{trans('localize.cancel')}}",
        closeOnConfirm: false
    }, function(){
        var url = '/merchant/promotion/submit/' + promotion_id;
        window.location.href = url;
    });
});

$('.button_cancel').on('click', function(){
    var promotion_id = $(this).data('promotion-id');
    swal({
        title: "{{trans('localize.sure')}}",
        text: "{{trans('localize.confirm_cancel_promotion')}}",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d9534f",
        confirmButtonText: "{{trans('localize.yes_cancel_it')}}",
        cancelButtonText: "{{trans('localize.cancel')}}",
        closeOnConfirm: false
    }, function(){
        var url = '/merchant/promotion/cancel/' + promotion_id;
        window.location.href = url;
    });
});
</script>
@endsection