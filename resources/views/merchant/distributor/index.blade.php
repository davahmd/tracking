@extends('merchant.layouts.master')

@section('title', trans('localize.manage_retailers'))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@lang('localize.manage_retailers')</h2>
            <ol class="breadcrumb">
                <li>
                    @lang('localize.retailers')
                </li>
                <li class="active">
                    <strong>{{ trans('localize.manage') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">

        <div class="row">
            <div class="col-lg-12">
                @include('merchant.common.success')
                @include('merchant.common.error')
                @include('merchant.common.errors')
                <div class="ibox">
                    <div class="ibox-content">
                        <form action="{{ route('retailer-list') }}" method="GET">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="id" class="form-control" placeholder="{{ trans('localize.id') }}" value="{{ request('id') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="{{ trans('localize.name') }}" value="{{ request('name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {{--<input type="text" class="form-control" placeholder="{{ trans('localize.status') }}">--}}
                                        <select class="form-control" name="status" id="status">
                                            <option value="" selected>{{ trans('localize.status') }} {{ trans('localize.all') }}</option>
                                            <option value="0" {{ request('status') === '0' ? 'selected' : '' }}>{{ trans('localize.pending') }}</option>
                                            <option value="1" {{ request('status') === '1' ? 'selected' : '' }}>{{ trans('localize.approved') }}</option>
                                            <option value="2" {{ request('status') === '2' ? 'selected' : '' }}>{{ trans('localize.block') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button class="btn btn-block btn-outline btn-primary">{{ trans('localize.filter') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="ibox">
                    <div class="ibox-content">
                        <div class="ibox-tools">
                            {{--<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#create_user">@lang('localize.create_account')</button>--}}
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">{{trans('localize.id')}}</th>
                                    <th class="text-center">{{trans('localize.Merchant')}}</th>
                                    <th class="text-center">{{trans('localize.stores')}}</th>
                                    <th class="text-center">{{trans('localize.date')}}</th>
                                    <th class="text-center">{{trans('localize.status')}}</th>
                                    <th class="text-center">{{trans('localize.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($merchants as $merchant)
                                    <tr class="text-center">
                                        <td>{{ $merchant->mer_id }}</td>
                                        <td>{{ $merchant->merchantName() }}</td>
                                        <td>
                                            <ul>
                                            @foreach ($merchant->stores as $store)
                                                <li><a href="{{ route('store.profile', ['stor_slug' => $store->url_slug]) }}" target="_BLANK">{{$store->stor_name}}</a></li>
                                            @endforeach
                                            </ul>
                                        </td>
                                        <td>{{ \Helper::UTCtoTZ($merchant->pivot->created_at) }}</td>
                                        <td>
                                            @if($merchant->pivot->status == 0)
                                                <label class="text-navy">@lang('localize.pending')</label>
                                            @elseif($merchant->pivot->status == 1)
                                                <label class="text-success">@lang('localize.Approved')</label>
                                            @elseif($merchant->pivot->status == 2)
                                                <label class="text-warning">@lang('localize.block')</label>
                                            @endif
                                        </td>
                                        <td>
                                            @if($merchant->pivot->status == 0)
                                                <a href="{{ route('retailer-change-status', ['status' => 1, 'merchantId' => $merchant->mer_id]) }}" class="btn btn-white btn-sm text-navy"><i class="fa fa-check"></i> @lang('localize.approve')</a>
                                                <a href="{{ route('retailer-change-status', ['status' => -1, 'merchantId' => $merchant->mer_id]) }}" class="btn btn-white btn-sm text-warning"><i class="fa fa-ban"></i> @lang('localize.decline')</a>
                                            @else
                                                @if($merchant->pivot->status == 1)
                                                    <a href="{{ route('retailer-change-status', ['status' => 2, 'merchantId' => $merchant->mer_id]) }}" class="btn btn-white btn-sm text-warning"><i class="fa fa-refresh"></i> @lang('localize.block')</a>
                                                @elseif($merchant->pivot->status == 2)
                                                    <a href="{{ route('retailer-change-status', ['status' => 1, 'merchantId' => $merchant->mer_id]) }}" class="btn btn-white btn-sm text-warning"><i class="fa fa-refresh"></i> @lang('localize.set_to_active')</a>
                                                @endif
                                                <a href="{{ route('retailer-change-status', ['status' => -1, 'merchantId' => $merchant->mer_id]) }}" class="btn btn-white btn-sm text-danger"><i class="fa fa-trash"></i> @lang('localize.remove')</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                                @include('layouts.partials.table-pagination', ['listings' => $merchants])

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')

@endsection

@section('script')
    <script>

    </script>
@endsection
