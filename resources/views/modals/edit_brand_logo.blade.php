<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="text-center">@lang('localize.edit_logo')</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12 text-center">
                <?php
                    $path = $logo->logo;
                    if (!str_contains($logo->logo, 'http://'))
                        $path = $url.$logo->logo;
                ?>
                <img alt="image" class="img-circle" src="{{$path}}" width="140px" height="130px" onerror="if (this.src != 'error.jpg') this.src = '/common/images/stock.png';"><br><br>
            </div>
        </div>
        <form class="form-horizontal" action='/brand_logo/edit' method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label">@lang('localize.logo_title')</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="logo_name" value="{{$logo->logo_name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">@lang('localize.image_file')</label>
                <div class="col-sm-9">
                    <span class='btn btn-default btn-block'><input type='file' name='file'></span>
                    <small>@lang('localize.or_choose_new_image_to_replace')</small>
                </div>
            </div>
    
            @if($logo->main == 0)
            <div class="form-group">
                <label class="col-sm-3 control-label">@lang('localize.status')</label>
                <div class="col-sm-9">
                    <select name='status' class='form-control'>
                        <option value='1' {{($logo->status == 1)? 'selected': ''}}>@lang('localize.active')</option>
                        <option value='0' {{($logo->status == 0)? 'selected': ''}}>@lang('localize.inactive')</option>
                    </select>
                </div>
            </div>
            @else
            <input type="hidden" name="status" value="1">
            @endif
            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <input type="hidden" name="id" value="{{$logo->id}}">
                    <input type="hidden" name="old_logo" value="{{$logo->logo}}">
                    <button type="submit" class="btn btn-outline btn-primary pull-right">@lang('localize.edit_logo')</button>
                </div>
            </div>
        </form>
    </div>