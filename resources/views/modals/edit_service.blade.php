<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="row">
            <div class="col-sm-3"><img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:35px;"></div>
            <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3>@lang('localize.edit_service')</h3></div>
        </div>
    </div>
    
    <div class="modal-body">
        <div class="row">
            <form class="form-horizontal" action="{{ url('update_service', $service_id) }}" method="POST" enctype="multipart/form-data" id="service_form">
            {{ csrf_field() }}
            <div class="table-responsive col-sm-12">
                <table class="table table-striped" id="service_table">
                    <tbody>
                        <tr>
                            <td style="width:70%;">
                                <label class="col-sm-3">@lang('localize.service_name')</label>
                                <input type="text" class="form-control" name="service_name" id="service_name" value="{{ $lists['service_name'] }}" placeholder="Service's Name">
                                <input type="hidden" name="old_service_name" value="{{ $lists['service_name'] }}">
                            </td>
                        </tr>
                        {{--<tr>
                            <td style="width:70%;">
                                <label class="col-sm-3">@lang('localize.service_price')</label>
                                <input type="text" class="form-control" name="service_price" id="service_price" value="{{ $lists['service_price'] }}" placeholder="Service's Price">
                                <input type="hidden" name="old_service_price" value="{{ $lists['service_price'] }}">
                            </td>
                        </tr>--}}
                        <tr>
                            <td><button type="button" class="btn btn-block btn-primary" id="submit_edit">@lang('localize.update')</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="" class="btn btn-default" type="button">{{trans('localize.close_')}}</a>
        {{-- <button data-dismiss="modal" class="btn btn-default" type="button">{{trans('localize.close_')}}</button> --}}
    </div>
    
    <script>
    
    $(document).ready(function() {
    
        $("#submit_edit").click(function(event) {
            if($('#service').val() == '') {
                $('#service').attr('placeholder', '{{trans('localize.fieldrequired')}}');
                $('#service').css('border', '1px solid red');
                return false;
            } else {
                $('#service').css('border', '');
            }
    
            $('#service_form').submit();
        });
    });
    </script>
    