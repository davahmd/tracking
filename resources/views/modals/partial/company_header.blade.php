<div class="row text-center" style="padding: 15px; border-bottom: 1px solid #e5e5e5;">
    <img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:125px; float: left;">
    <h3 class="text-uppercase">{{ config('app.company.name') }} <small>({{ config('app.company.reg_no') }})</small></h3>
    <span style="font-size: 13px;">
    (@lang('localize.gst_id_no') : {{ env('GST') }})<br>
    {!! config('app.company.address') !!} <br>
    @lang('localize.tel') : {{ env('CONTACT_NO') }}
    </span>
</div>