@php
$total = round(($order->merchant_platform_charge_token + $order->customer_charge_token) * $order->currency_rate, 2);

$tax = 0;
if($gstRate > 0)
{
    $tax = round($total - ($total / (1 + ($gstRate/100))), 2);
}
$amount = round($total - $tax, 2);
@endphp
<div class="row taxInvoiceCustomer" style="display: {{ $customerDisplay? '' : 'none' }};">

    <div class="col-sm-12">

        <table class="table table-bordered-custom" style="width:100%;">
            <tr style="background-color: #e8e8e8;">
                <th class="text-center" style="padding: 15px !important; width:70%;" nowrap>@lang('localize.description')</th>
                <th class="text-center text-nowrap" style="padding: 15px !important; width:1%;">@lang('localize.amount') ({{ $order->currency }})</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <td class="text-left">@lang('localize.offline_commission')</td>
                <td class="text-center">{{ number_format($total, 2) }}</td>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <td class="text-left">@lang('localize.transaction_reference_no'): {{ $order->inv_no }}</td>
                <td class="text-center"></td>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <th class="text-left">@lang('localize.grand_total') @if(!$inRange)(@lang('localize.including_gst'))@endif</th>
                <th class="text-center" style="border:3px solid #292929;">{{ number_format($total, 2) }}</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

        </table>
    </div>

    <div class="col-sm-12">
        @if(!$inRange)
        <table width="60%">
            <tr>
                <th class="text-center"><u>@lang('localize.GST_summary')</u></th>
                <th class="text-center"><u>@lang('localize.amount') ({{ $order->currency }})</u></th>
                <th class="text-center"><u>@lang('localize.tax') ({{ $order->currency }})</u></th>
            </tr>
            <tr>
                <th class="text-center">@lang('localize.sr', ['rate' => $gstRate])</th>
                <th class="text-center">{{ number_format($amount, 2) }}</th>
                <th class="text-center">{{ number_format($tax, 2) }}</th>
            </tr>
        </table>
        @endif
        <br><br>
        * This tax invoice is computer generated and no signature is required.
    </div>
</div>