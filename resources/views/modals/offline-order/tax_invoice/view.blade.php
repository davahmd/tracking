@if($userType == 'admin' && !$batch)
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#tax_inv" id="taxInvoice">@lang('localize.tax_invoice')</a></li>
    <li><a data-toggle="tab" href="#tax_inv_em" id="taxInvoiceCustomer">@lang('localize.tax_invoice') @lang('localize.customer')</a></li>
</ul>
@endif

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="row">
        <div class="col-sm-3"><img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:35px;"></div>
        <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase">@lang('localize.tax_invoice')</h3></div>
    </div>
</div>

<div class="modal-body">
    @if(!$batch)
    @include('modals.offline-order.tax_invoice.data')
    @else
    @php
        $orders = $order;
    @endphp
    @foreach ($orders as $order)
        @include('modals.offline-order.tax_invoice.data')
        @if(!$loop->last)
        <hr>
        @endif
    @endforeach
    @endif
</div>

<div class="modal-footer">
    <a href="{{ $downloadLink }}" class="btn btn-primary" id="download">@lang('localize.download')</a>

    <a href="{{ $printLink }}" class="btn btn-primary" id="print" onclick="window.open('{{ $printLink }}', 'newwindow', 'width=750, height=500'); return false;">@lang('localize.print')</a>

    <button data-dismiss="modal" class="btn btn-default" type="button">@lang('localize.closebtn')</button>
</div>

@if($userType == 'admin' && !$batch)
<script>

var default_download = "{{ $downloadLink }}";
var default_print = "{{ $printLink }}";

$('#taxInvoice').on('click', function () {
    $('.taxInvoiceCustomer, .taxNumberCustomer').hide();
    $('.taxInvoice, .taxNumber').show();

    setDefaultLink();
});

$('#taxInvoiceCustomer').on('click', function () {
    $('.taxInvoice, .taxNumber').hide();
    $('.taxInvoiceCustomer, .taxNumberCustomer').show();

    var download = default_download + '&address=customer';
    var print = default_print + '&address=customer';

    $('#download').attr("href", download);
    $('#print').attr("href", print);
    $('#print').attr("onclick", "window.open('"+print+"', 'newwindow', 'width=750, height=500'); return false;");
});

function setDefaultLink()
{
    $('#download').attr("href", default_download);
    $('#print').attr("href", default_print);
    $('#print').attr("onclick", "window.open('"+default_print+"', 'newwindow', 'width=750, height=500'); return false;");
}
</script>
@endif