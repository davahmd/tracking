
@php
$merchant = $order->merchant;
$customer = $order->customer;
$customerDisplay = $userType == 'admin' && !$address && !$batch && $address != 'merchant'? false : true;

$from = Carbon\Carbon::create(2017, 7, 1)->startOfDay();
$to = Carbon\Carbon::create(2017, 9, 30)->endOfDay();
$inRange = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at)->between($from, $to);

list($whole, $decimal) = explode('.', $order->customer_charge_percentage);
$gstRate = $decimal > 0? $order->customer_charge_percentage : $whole;
@endphp

<div class="row">
    <div class="col-sm-12 text-center text-uppercase" style="margin-bottom:20px;margin-top:15px;">
        <h1><strong>@lang('localize.tax_invoice')</strong></h1>
    </div>
</div>

<div class="row" style="padding-right: 15px; padding-left: 15px;">

    @include('modals.offline-order.tax_invoice.bill_to')

    <div class="col-xs-4" style="border:1px solid #737373; padding: 10px;">
        <table>
            <tr>
                <th>@lang('localize.invoice_no')</th>
                <th style="padding: 0 4px 0;"> : </th>

                @if($userType == 'admin' && !$address && !$batch)
                <td class="taxNumber"> {{ $order->tax_invoice_number() }}</td>
                <td class="taxNumberCustomer" style="display: none;"> {{ $order->tax_invoice_number('OFC') }}</td>
                @else
                @if($address == 'merchant' || $userType == 'merchant')
                <td class="taxNumber"> {{ $order->tax_invoice_number() }}</td>
                @else
                <td class="taxNumberCustomer"> {{ $order->tax_invoice_number('OFC') }}</td>
                @endif
                @endif
            </tr>
            <tr>
                <th>@lang('localize.date')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ \Helper::UTCtoTZ($order->created_at, 'd-M-Y') }}</td>
            </tr>
            <tr>
                <th>@lang('localize.Page')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td> 1 </td>
            </tr>
        </table>
    </div>
</div>

<div style="margin-top:30px;"></div>

@if($userType == 'admin' && !$address && !$batch)
@include('modals.offline-order.tax_invoice.ivCustomer')
@include('modals.offline-order.tax_invoice.ivMerchant')
@else
@if($address == 'merchant' || $userType == 'merchant')
@include('modals.offline-order.tax_invoice.ivMerchant')
@else
@include('modals.offline-order.tax_invoice.ivCustomer')
@endif
@endif