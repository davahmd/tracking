@php
    $merchant = $order->merchant;
    $customer = $order->customer;
    $charges = $order->charges();
@endphp

<div class="row">
    <div class="col-xs-6">
        <h4>{{ config('app.company.name') }} <small>({{ config('app.company.reg_no') }})</small></h4>
        {!! config('app.company.address') !!}
        <br><b>@lang('localize.gst_no') : </b> {{ env('GST') }}
    </div>
    <div class="col-xs-6" style="border-left:1px solid #eee;">
        <h4>@lang('localize.merchant_detail')</h4>
        @if($merchant)
        <b>{{ $merchant->full_name() }}</b> <br>
        {!! ucwords(implode('<br>', array_map('trim', array_filter([$merchant->mer_address1, $merchant->mer_address2, $merchant->mer_city])))) !!} <br>
        {{ ucwords(trim(implode(', ', array_filter([$merchant->zipcode, $merchant->state? $merchant->state->name : null, $merchant->country? $merchant->country->co_name : null])))) }} <br>
        @else
        Merchant not Found!
        @endif
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-xs-6">
        <h4>@lang('localize.paymentdetail')</h4>
        <b>@lang('localize.date') : </b>{{ \Helper::UTCtoTZ($order->created_at) }} <br>
        <b>@lang('localize.offline_order_id') : </b>{{ $order->id }}<br/>
        <b>@lang('localize.status') : </b> {{ $order->status() }}
    </div>
    <div class="col-xs-6" style="border-left:1px solid #eee;">
        <h4>@lang('localize.customer_detail')</h4>
        <b>@lang('localize.name') : </b>{{ $customer? $customer->cus_name : '' }} <br>
        <b>@lang('localize.email') : </b>{{ $customer? $customer->email : '' }} <br>
        <b>@lang('localize.phone') : </b>{{ $customer? $customer->phone() : '' }} <br>
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-sm-12">
        <table border=0 width="100%">
            <tr>
                <th width="35%" style="padding-left:5px;">@lang('localize.merchant_invoice_no')</th>
                <th width="5%">:</th>
                <td colspan='3'>
                    {{ $order->inv_no }}
                </td>
            </tr>

            <tr><td colspan='5'>&nbsp;</td></tr>
            <tr class="text-center">
                <th colspan='2'>&nbsp;</th>
                <td><b>@lang('common.credit_name')</b></td>
                <td><b>@lang('localize.amount') ({{ $order->currency }})</b></td>
                <td></td>
            </tr>
            <tr class="text-center">
                <th style="padding-left:5px;">@lang('common.credit_name')</td>
                <th>:</th>
                <td>{{ number_format($charges->credit->amount, 4) }}</td>
                <td>{{ number_format($charges->price->amount, 2) }}</td>
                <td> * </td>
            </tr>

            @if($userType == 'admin')
            <tr><td colspan='5'><br/></td></tr>
            <tr class="text-center" style="border-left:1pt solid #d1cfcf; border-right:1pt solid #d1cfcf; border-top:1pt solid #d1cfcf; padding:1px;">
                <th style="padding-left:5px;">@lang('localize.commission') {{$order->merchant_platform_charge_percentage}}%</th>
                <th>:</th>
                <td>{{ number_format($charges->credit->platformCharge, 4) }}</td>
                <td>{{ number_format($charges->price->platformCharge, 2) }}</td>
                <td> * </td>
            </tr>
            <tr class="text-center" style="border-left:1pt solid #d1cfcf; border-right:1pt solid #d1cfcf;">
                <th style="padding-left:5px;">@lang('localize.gst') {{$order->customer_charge_percentage}}%</td>
                <th>:</th>
                <td>{{ number_format($charges->credit->customerCharge, 4) }}</td>
                <td>{{ number_format($charges->price->customerCharge, 2) }}</td>
                <td> * </td>
            </tr>

            <tr class="text-center" style="border-left:1pt solid #d1cfcf; border-right:1pt solid #d1cfcf; border-bottom:1pt solid #d1cfcf;">
                <th style="padding-left:5px;">@lang('localize.order_credit_total')</td>
                <th>:</th>
                <td>{{ number_format($charges->credit->total, 4) }}</td>
                <td>{{ number_format($charges->price->total, 2) }}</td>
                <td></td>
            </tr>
            <tr><td colspan='5'><br/></td></tr>
            @endif

            <tr class="text-center">
                <th style="padding-left:5px;">@lang('localize.merchant_charge') {{$order->merchant_charge_percentage}}%</td>
                <th>:</th>
                <td>{{ number_format($charges->credit->merchantCharge, 4) }}</td>
                <td>{{ number_format($charges->price->merchantCharge, 2) }}</td>
                <td> * </td>
            </tr>

            <tr><td colspan='5'><br/></td></tr>

            <tr class="text-center">
                <th style="padding-left:5px;">@lang('localize.merchant_earned_credit')</td>
                <th>:</th>
                <td><strong>{{ number_format($charges->credit->merchantEarn, 4) }}</strong></td>
                <td><strong>{{ number_format($charges->price->merchantEarn, 2) }}</strong></td>
            </tr>

            <tr><td colspan='5'><br/></td></tr>

            <tr>
                <th style="padding-left:5px;">@lang('localize.remarks')</td>
                <th>:</th>
                <td colspan='2'>{{$order->remark}}</td>
            </tr>
        </table>
    </div>

    <div class="col-sm-12">
        <br>
        * GST inclusive if applicable<br>
        This invoice is computer generated and no signature is required.
    </div>
</div>