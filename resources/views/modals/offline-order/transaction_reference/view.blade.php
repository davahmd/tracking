<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="row">
        <div class="col-sm-3"><img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:35px;"></div>
    <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase">@lang('localize.transaction_reference')</h3></div>
    </div>
</div>

<div class="modal-body">

    @if(!$batch)
    @include('modals.offline-order.transaction_reference.data')
    @else
    @php
        $orders = $order;
    @endphp
    @foreach ($orders as $order)
        @include('modals.offline-order.transaction_reference.data')
        @if(!$loop->last)
        <hr>
        @endif
    @endforeach
    @endif

</div>

<div class="modal-footer">
    @if(isset($downloadLink))
    <a href="{{ $downloadLink }}" class="btn btn-primary">@lang('localize.download')</a>
    @endif

    @if(isset($printLink))
    <a href="javascript:void(0)" class="btn btn-primary" onclick="window.open('{{ $printLink }}', 'newwindow', 'width=750, height=500'); return false;">@lang('localize.print')</a>
    @endif

    <button data-dismiss="modal" class="btn btn-default" type="button">@lang('localize.closebtn')</button>
</div>