<!doctype html>
<html lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8;"/>
        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('common/images/favicon.png') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.css') }}" media="all">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/custom.css') }}" media="all">

        <style type="text/css">
        .page-break {
            page-break-after: always;
            page-break-inside: avoid;
        }
        </style>

    </head>

    <body onload="{{ !$isFromApi? 'window.print()' : '' }}" style="background-color:white;">
        @if(!$batch)
        <div class="container">
            <div class="row text-center" style="padding: 15px; border-bottom: 1px solid #e5e5e5;">
                <img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:70px; float: left;">
                <h2 class="text-uppercase">
                    @lang('localize.transaction_reference')
                </h2>
            </div>

            <br>

            @include('modals.offline-order.transaction_reference.data')

        </div>
        @else

        @php
            $orders = $order;
        @endphp
        @foreach ($orders as $order)
        <div class="container">
            <div class="row text-center" style="padding: 15px; border-bottom: 1px solid #e5e5e5;">
                <img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:70px; float: left;">
                <h2 class="text-uppercase">
                    @lang('localize.transaction_reference')
                </h2>
            </div>

            <br>

            @include('modals.offline-order.transaction_reference.data')

        </div>
        @if(!$loop->last)
        <div class="page-break"></div>
        @endif
        @endforeach

        @endif
    </body>
</html>
