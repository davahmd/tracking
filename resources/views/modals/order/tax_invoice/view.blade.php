@if($userType == 'admin' && !$batch)
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#tax_inv" id="taxInvoice">@lang('localize.tax_invoice') 1</a></li>
    {{--<li><a data-toggle="tab" href="#tax_inv_em" id="taxInvoiceEM">@lang('localize.taxgit statu_invoice') 2</a></li>--}}
</ul>
@endif

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="row">
        <div class="col-sm-3"><img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:35px;"></div>
        <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase">@lang('localize.tax_invoice')</h3></div>
    </div>
</div>

<div class="modal-body">
    @if(!$batch)
    @include('modals.order.tax_invoice.data')
    @else
    @php
        $invoices = $invoice;
    @endphp
    @foreach ($invoices as $invoice)
        @include('modals.order.tax_invoice.data')
        @if(!$loop->last)
        <hr>
        @endif
    @endforeach
    @endif
</div>

<div class="modal-footer">
    <a href="javascript:void(0)" class="btn btn-primary" id="download">@lang('localize.download')</a>

    <a href="javascript:void(0)" class="btn btn-primary" id="print">@lang('localize.print')</a>

    <button data-dismiss="modal" class="btn btn-default" type="button">@lang('localize.closebtn')</button>
</div>

@if($userType == 'admin' && !$batch)
<script>

var link = "{{ url('online/invoice', [$invoice->id, $userType]) }}";
var default_download = link + '?operation=pdf';
var default_print = link + '?operation=print';

setDefaultLink();

$('#taxInvoice').on('click', function () {
    // $('#taxNumberChar').text('ONS');
    $('.taxInvoiceEM').hide();
    $('.taxInvoice').show();

    setDefaultLink();
});

$('#taxInvoiceEM').on('click', function () {
    // $('#taxNumberChar').text('ONC');
    $('.taxInvoice').hide();
    $('.taxInvoiceEM').show();

    var download = default_download + '&company=true';
    var print = default_print + '&company=true';
    $('#download').attr("href", download);
    $('#print').attr("href", print);
    $('#print').attr("onclick", "window.open('"+print+"', 'newwindow', 'width=750, height=500'); return false;");
    // var company = $('option:selected', '#company');
    // renderCompanyAdress(company);
});

{{--
$('#company').change(function () {
    var company = $('option:selected', this);
    renderCompanyAdress(company);
});

function renderCompanyAdress(company)
{
    var download = default_download + '&company=' + company.data('name');
    var print = default_print + '&company=' + company.data('name');

    $('#company_name').text(company.data('company-name'));
    $('#company_add').text(company.data('address'));
    $('#company_contact').text(company.data('tel'));
    $('#company_gst').text(company.data('gst-no'));
    $('#download').attr("href", download);
    $('#print').attr("href", print);
    $('#print').attr("onclick", "window.open('"+print+"', 'newwindow', 'width=750, height=500'); return false;");
}
--}}

function setDefaultLink()
{
    $('#download').attr("href", default_download);
    $('#print').attr("href", default_print);
    $('#print').attr("onclick", "window.open('"+default_print+"', 'newwindow', 'width=750, height=500'); return false;");
}
</script>
@else

<script>
var default_download = "{!! $download_link !!}";
var default_print = "{!! $print_link !!}";

setDefaultLink();

function setDefaultLink()
{
    $('#download').attr("href", default_download);
    $('#print').attr("onclick", "window.open('"+default_print+"', 'newwindow', 'width=750, height=500'); return false;");
}
</script>
@endif