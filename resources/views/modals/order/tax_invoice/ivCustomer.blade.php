@php
$total = $invoice->items->sum(function ($item) use ($gstRate) {
    return round($item->priceAmount + $item->priceShippingFees + $item->pricePlatformCharge + $item->priceServiceCharge, 2);
});

$tax = 0;
if($gstRate > 0)
{
    $tax = round($total - ($total / (1 + ($gstRate/100))), 2);
}
$amount = round($total - $tax, 2);
@endphp

<div class="taxInvoice">
<div class="row" style="padding-right: 15px; padding-left: 15px;">

    <div class="col-xs-7" style="padding: 0; padding-right: 15px;">
        <table>
            <tr>
                <th style="vertical-align:top;">@lang('localize.bill_to')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>{{ $customer->cus_name }}</td>
            </tr>
            <tr>
                <th style="vertical-align:top;">@lang('localize.address')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>
                    {!! ucwords(implode('<br>', array_map('trim', array_filter([$customer->cus_address1,$customer->cus_address2, $customer->cus_city_name])))) !!} <br>
                    {{ ucwords(trim(implode(', ', array_filter([$customer->cus_postalcode, $customer->state? $customer->state->name : null, $customer->country? $customer->country->co_name : null])))) }}
                </td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>@lang('localize.contact_number')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>{{ $customer->phone() }}</td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>@lang('localize.ship_to')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>{{ ($shipping) ? $shipping->ship_name : '' }}</td>
            </tr>
        </table>
    </div>

    <div class="col-xs-5" style="border:1px solid #737373; padding: 10px;">
        <table>
            <tr>
                <th>@lang('localize.invoice_no')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td class="taxNumber" nowrap> {{ $invoice->tax_number() }}</td>
            </tr>
            <tr>
                <th>@lang('localize.date')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ \Helper::UTCtoTZ($parent->date, 'd-M-Y', $isFromApi) }}</td>
            </tr>
            <tr>
                <th>@lang('localize.Page')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td> 1 </td>
            </tr>

            <tr class="taxInvoice">
                <th>@lang('localize.do.number')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ $invoice->tax_number('ON') }}</td>
            </tr>
        </table>
    </div>
</div>

<div style="margin-top:30px;"></div>

<div class="row">

    <div class="col-sm-12">
        <table class="table table-bordered-custom">
            <tr style="background-color: #e8e8e8;">
                <th class="text-center" style="padding: 15px !important; width:1%;" nowrap>@lang('localize.item_no')</th>
                <th class="text-center" style="padding: 15px !important; width:50%;">@lang('localize.description')</th>
                <th class="text-center" style="padding: 15px !important;">@lang('localize.quantity')</th>
                <th class="text-center" style="padding: 15px !important;" nowrap>@lang('localize.unit_price')</th>
                <th class="text-center" style="padding: 15px !important;" nowrap>@lang('localize.amount')</th>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            @foreach($invoice->items as $item)
            <tr>
                <td class="text-center">{{ $count }}</td>
                <td class="text-left">{{ $item->product->pro_title_en }}</td>
                <td class="text-center">{{ $item->order_qty }}</td>
                <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->priceUnit) : $currency . ' ' . number_format($item->priceUnit, 2) }}</td>
                <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->priceAmount) : $currency . ' ' . number_format($item->priceAmount, 2) }}</td>
            </tr>

            @if($item->priceShippingFees > 0)
            @php
                $count++;
            @endphp
            <tr>
                <td class='text-center'>{{ $count }}</td>
                <td class="text-left">@lang('localize.shipping_fees')</td>
                <td class='text-center'> 1 </td>
                <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->priceShippingFees) : $currency . ' ' . number_format($item->priceShippingFees, 2) }}</td>
                <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->priceShippingFees) : $currency . ' ' . number_format($item->priceShippingFees, 2) }}</td>
            </tr>
            @endif

            @if($item->pricePlatformCharge > 0)
                @php
                    $count++;
                @endphp
                <tr>
                    <td class='text-center'>{{ $count }}</td>
                    <td class="text-left">@lang('localize.platform_charges')</td>
                    <td class='text-center'> 1 </td>
                    <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->pricePlatformCharge) : $currency . ' ' . number_format($item->pricePlatformCharge, 2) }}</td>
                    <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->pricePlatformCharge) : $currency . ' ' . number_format($item->pricePlatformCharge, 2) }}</td>
                </tr>
            @endif

            @if($item->priceServiceCharge > 0)
                @php
                    $count++;
                @endphp
                <tr>
                    <td class='text-center'>{{ $count }}</td>
                    <td class="text-left">@lang('localize.gst')</td>
                    <td class='text-center'> 1 </td>
                    <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->priceServiceCharge) : $currency . ' ' . number_format($item->priceServiceCharge, 2) }}</td>
                    <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($item->priceServiceCharge) : $currency . ' ' . number_format($item->priceServiceCharge, 2) }}</td>
                </tr>
            @endif

            @if(!$loop->last)
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            @endif

            @php
                $count++;
            @endphp
            @endforeach

            @for ($i = 0; $i < 3; $i++)
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            @endfor

            <tr>
                <th>&nbsp;</th>
                <th style="padding-left:10px">@lang('localize.grand_total')</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th class='text-center' style="border:3px solid #292929;">{{ ($currency == 'IDR') ? rpFormat($total) : $currency . ' ' . number_format($total, 2) }}</th>
            </tr>

            @for ($i = 0; $i < 2; $i++)
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            @endfor
        </table>
    </div>

    <div class="col-sm-12">
        <br><br><br>
        * This tax invoice is computer generated and no signature is required.
    </div>
</div>
</div>