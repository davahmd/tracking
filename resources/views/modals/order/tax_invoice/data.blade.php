@php
$currency = $invoice->items->first()->currency;
$parent = $invoice->parent_order;
$customer = $invoice->customer;
$merchant = $invoice->merchant;
$shipping = $parent? $parent->shipping_address : null;

$count = 1;
$default = $invoice->items->first();

list($whole, $decimal) = explode('.', $default->cus_service_charge_rate);
$gstRate = $decimal > 0? $default->cus_service_charge_rate : $whole;
$currencyRate = $default->currency_rate;

$from = Carbon\Carbon::create(2017, 7, 1)->startOfDay();
$to = Carbon\Carbon::create(2017, 9, 30)->endOfDay();
$inRange = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $default->created_at)->between($from, $to);

$invoice->items->map(function ($item) use ($currencyRate, $gstRate) {
    $item->creditUnit = 0;
    if($currencyRate > 0)
    {
        $item->creditUnit = round($item->product_price / $currencyRate, 4);
        // if(!$inRange)
        // {
        //     $item->creditUnit = round($item->creditUnit + ($item->creditUnit * ($gstRate/100)), 4);
        // }
    }
    $item->creditAmount = round($item->creditUnit * $item->order_qty, 4);
    $item->creditShippingFees = round($item->total_product_shipping_fees / $currencyRate, 4);
    $item->creditPlatformCharge = round($item->cus_platform_charge_value, 4);
    $item->creditServiceCharge = round($item->cus_service_charge_value, 4);
    $item->creditMerchantCharge = round($item->merchant_charge_vtoken, 4);
    $item->creditMerchantEarn = round(max(0, $item->order_vtokens - ($item->merchant_charge_vtoken + $item->cus_service_charge_value + $item->cus_platform_charge_value)), 4);

    $item->priceUnit = round($item->creditUnit * $currencyRate, 2);
    $item->priceAmount = round($item->creditAmount * $currencyRate, 2);
    $item->priceShippingFees = round($item->creditShippingFees * $currencyRate, 2);
    $item->pricePlatformCharge = round($item->creditPlatformCharge * $currencyRate, 2);
    $item->priceServiceCharge = round($item->creditServiceCharge * $currencyRate, 2);
    $item->priceMerchantCharge = round($item->creditMerchantCharge * $currencyRate, 2);
    $item->priceMerchantEarn = round($item->creditMerchantEarn * $currencyRate, 2);
    return $item;
});
@endphp

<div class="row">
    <div class="col-sm-12 text-center text-uppercase" style="margin-bottom:20px;margin-top:15px;">
        <h1><strong>@lang('localize.tax_invoice')</strong></h1>
    </div>
</div>

@if($operation == 'view' && !$batch && $userType == 'admin')
@include('modals.order.tax_invoice.ivCustomer')
@include('modals.order.tax_invoice.ivEM')
@else
    @if(!$address)
    @include('modals.order.tax_invoice.ivCustomer')
    @else
    @include('modals.order.tax_invoice.ivEM')
    @endif
@endif
