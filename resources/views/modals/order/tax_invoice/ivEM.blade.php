@php
$total = $invoice->items->sum(function ($item) use ($gstRate, $inRange) {

    if(!$inRange)
    {
        return round($item->pricePlatformCharge + ($item->pricePlatformCharge * ($gstRate/100)), 4);
    }

    return round($item->pricePlatformCharge + $item->priceServiceCharge, 4);
});

$tax = 0;
if($gstRate > 0)
{
    $tax = round($total - ($total / (1 + ($gstRate/100))), 2);
}
$amount = round($total - $tax, 2);
@endphp

<div class="taxInvoiceEM" style="{{ $operation == 'view' && !$batch && $userType == 'admin'? 'display: none;' : '' }}">
<div class="row" style="padding-right: 15px; padding-left: 15px;">

    <div class="col-xs-7" style="padding: 0; padding-right: 15px;">
        <table>
            <tr>
                <th style="vertical-align:top;">@lang('localize.bill_to')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>{{ $customer->cus_name }}</td>
            </tr>
            <tr>
                <th style="vertical-align:top;">@lang('localize.address')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>
                    {!! ucwords(implode('<br>', array_map('trim', array_filter([$customer->cus_address1,$customer->cus_address2, $customer->cus_city_name])))) !!} <br>
                    {{ ucwords(trim(implode(', ', array_filter([$customer->cus_postalcode, $customer->state? $customer->state->name : null, $customer->country? $customer->country->co_name : null])))) }}
                </td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>@lang('localize.contact_number')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>{{ $customer->phone() }}</td>
            </tr>
        </table>
    </div>

    <div class="col-xs-5" style="border:1px solid #737373; padding: 10px;">
        <table>
            <tr>
                <th>@lang('localize.invoice_no')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td class="taxNumber" nowrap> {{ $invoice->tax_number('ONC') }}</td>
            </tr>
            <tr>
                <th>@lang('localize.date')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ \Helper::UTCtoTZ($parent->date, 'd-M-Y', $isFromApi) }}</td>
            </tr>
            <tr>
                <th>@lang('localize.Page')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td> 1 </td>
            </tr>
        </table>
    </div>
</div>

<div style="margin-top:30px;"></div>

<div class="row">

    <div class="col-sm-12">
        <table class="table table-bordered-custom">
            <tr style="background-color: #e8e8e8;">
                <th class="text-text" style="padding: 15px !important; width:90%;" nowrap>@lang('localize.description')</th>
                <th class="text-center" style="padding: 15px !important;" nowrap>@lang('localize.amount')</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <td class="text-left">@lang('localize.online_commission')</td>
                <td class="text-center">{{ ($currency == 'IDR') ? rpFormat($total) : $currency . ' ' . number_format($total, 2) }}</td>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <td class="text-left" style="padding-left:10px">@lang('localize.transaction_reference_no') : {{ $invoice->tax_number('ONS') }}</th>
                <td>&nbsp;</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <th class="text-left" style="padding-left:10px">@lang('localize.grand_total') @if(!$inRange)(@lang('localize.including_gst'))@endif</th>
                <th class='text-center' style="border:3px solid #292929;">{{ ($currency == 'IDR') ? rpFormat($total) : $currency . ' ' . number_format($total, 2) }}</th>
            </tr>

            @for ($i = 0; $i < 2; $i++)
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            @endfor
        </table>
    </div>

    <div class="col-sm-12">
        @if($parent->date >= $date)
        <table width="60%">
            <tr>
                <th class="text-center"><u>@lang('localize.GST_summary')</u></td>
                <th class="text-center"><u>@lang('localize.amount')</u></td>
                <th class="text-center"><u>@lang('localize.tax')</u></td>
            </tr>
            <tr>
                <th class="text-center">@lang('localize.sr', ['rate' => $gstRate])</td>
                <th class="text-center">{{ ($currency == 'IDR') ? rpFormat($amount) : $currency . ' ' . number_format($amount, 2) }}</th>
                <th class="text-center">{{ ($currency == 'IDR') ? rpFormat($tax) : $currency . ' ' . number_format($tax, 2) }}</th>
            </tr>
        </table>
        @endif
        <br><br><br>
        * This tax invoice is computer generated and no signature is required.
    </div>
</div>
</div>