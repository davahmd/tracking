@php
$shipping = $delivery->invoice && $delivery->invoice->parent_order && $delivery->invoice->parent_order->shipping_address ? $delivery->invoice->parent_order->shipping_address : null;
$items = $delivery->items;
@endphp

<div class="row">
    <div class="col-sm-12 text-center text-uppercase" style="margin-bottom:20px;margin-top:15px;">
        <h1><strong>@lang('localize.delivery_order')</strong></h1>
    </div>
</div>

<div class="row" style="padding-right: 15px; padding-left: 15px;">

    <div class="col-xs-8" style="padding: 0;">
        <table>
            @if($shipping)
            <tr>
                <th>@lang('localize.delivery_to')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ $shipping->ship_name }}</td>
            </tr>
            <tr>
                <th style="vertical-align: top !important;">@lang('localize.delivery_address')</th>
                <th style="vertical-align: top !important; padding: 0 4px 0;"> : </th>
                <td>
                    {!! ucwords(implode('<br>', array_map('trim', array_filter([$shipping->ship_address1, $shipping->ship_address2, $shipping->ship_city_name])))) !!} <br>
                    {{ ucwords(trim(implode(', ', array_filter([$shipping->ship_postalcode, $shipping->state? $shipping->state->name : null, $shipping->country? $shipping->country->co_name : null])))) }}
                </td>
            </tr>
            <tr>
                <th>@lang('localize.contact_number')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ $shipping->phone() }}</td>
            </tr>
            @endif

            <tr>
                <th>@lang('localize.delivery_date')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>
                    @if(in_array($delivery->type, [1,2]))
                    {{ ($delivery->appointment_detail || $delivery->courier)? \Helper::UTCtoTZ($delivery->shipment_date, 'd-M-Y', $isFromApi) : '' }}
                    @endif
                </td>
            </tr>

            <tr>
                <th>@lang('localize.ship_company')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>
                    @if(in_array($delivery->type, [1,2]) && $delivery->courier)
                    {{ $delivery->courier->name }}
                    @endif
                </td>
            </tr>
            <tr>
                <th>@lang('localize.trackingno')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>
                    {{ $delivery->tracking_number }}
                </td>
            </tr>

            @if($delivery->appointment_detail)
            <tr>
                <th>@lang('localize.remarks')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ $delivery->appointment_detail }}</td>
            </tr>
            @endif

            <tr>
                <th>@lang('localize.status')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>
                    @if($items->whereIn('order_status', [4,5,6])->count() > 0)
                        @if($items->whereIn('order_status', [4])->count() > 0)
                        @lang('localize.completed')
                        @elseif($items->whereIn('order_status', [5])->count() > 0)
                        @lang('localize.cancelled')
                        @elseif($items->whereIn('order_status', [6])->count() > 0)
                        @lang('localize.refunded')
                        @endif
                    @endif
                </td>
            </tr>
        </table>
    </div>

    <div class="col-xs-4" style="border:1px solid #737373; padding: 10px;">
        <table>
            <tr>
                <th>@lang('localize.do.number')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ $delivery->invoice->tax_number('ON') }}</td>
            </tr>
            <tr>
                <th>@lang('localize.date')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>{{ \Helper::UTCtoTZ($delivery->created_at, 'd-M-Y', $isFromApi) }}</td>
            </tr>
            <tr>
                <th>@lang('localize.Page')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td> 1 </td>
            </tr>
        </table>
    </div>
</div>

<div class="row" style="margin-top:30px;">

    <div class="col-sm-12">
        <table class="table table-bordered-custom">
            <tr style="background-color: #e8e8e8;">
                <th class="text-center" style="padding: 15px !important; width:1%;" nowrap>@lang('localize.item_no')</th>
                <th class="text-center" style="padding: 15px !important; width:1%;" nowrap>@lang('localize.sku_code')</th>
                <th class="text-center" style="padding: 15px !important;" >@lang('localize.product_description')</th>
                <th class="text-center" style="padding: 15px !important;" >@lang('localize.quantity')</th>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            @foreach($items as $item)
            <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td class="text-center text-nowrap">{{ $item->sku }}</td>
                <td class="text-left">
                    {{ $item->product->pro_title_en }}
                    @if(!is_null($item->order_attributes))
                    <p>
                        {!! $item->parseOrderAttribute !!}
                    </p>
                    @endif

                    @if(in_array($userType, ['admin', 'user']) && $item->ecards->count() > 0)
                    <p style="margin-top:5px;">
                        <strong>@lang('localize.e-card.serial_number') : </strong><br>
                        {!! implode('<br>', $item->generatedCode->pluck('serial_number')->toArray()) !!}
                    </p>
                    @endif
                </td>
                <td class="text-center">{{ $item->order_qty }}</td>
            </tr>
            @endforeach

            @for ($i = 0; $i < 2; $i++)
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            @endfor

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th class="text-left">@lang('localize.total')</th>
                <th class='text-center' style="border:3px solid #292929;">{{ $items->sum('order_qty') }}</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </table>
    </div>

    <div class="col-sm-12">
        <br><br><br>
        * This delivery order is computer generated and no signature is required.
    </div>
</div>