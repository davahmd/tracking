<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="row">
        <div class="col-sm-3"><img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:35px;"></div>
        <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase">@lang('localize.delivery_order')</h3></div>
    </div>
</div>

<div class="modal-body">
    @if(!$batch)
    @include('modals.order.delivery_order.data')
    @else
    @php
        $deliveries = $delivery;
    @endphp
    @foreach ($deliveries as $delivery)
        @include('modals.order.delivery_order.data')
        @if(!$loop->last)
        <hr>
        @endif
    @endforeach
    @endif
</div>

<div class="modal-footer">
    <a href="javascript:void(0)" class="btn btn-primary" id="download">@lang('localize.download')</a>

    <a href="javascript:void(0)" class="btn btn-primary" id="print">@lang('localize.print')</a>

    <button data-dismiss="modal" class="btn btn-default" type="button">@lang('localize.closebtn')</button>
</div>

<script>
var default_download = "{!! $download_link !!}";
var default_print = "{!! $print_link !!}";

setDefaultLink();

function setDefaultLink()
{
    $('#download').attr("href", default_download);
    $('#print').attr("onclick", "window.open('"+default_print+"', 'newwindow', 'width=750, height=500'); return false;");
}
</script>