<!doctype html>
<html lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('common/images/favicon.png') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.css') }}" media="all">

        <style type="text/css">
            .page-break {
                page-break-after: always;
                page-break-inside: avoid;
            }
            .container {
                width: 750px;
            }
        </style>
    </head>

    <body onload="{{ !$isFromApi? 'window.print()' : '' }}" >
        <div class="container">
            <div class="row text-center" style="padding: 15px; border-bottom: 1px solid #e5e5e5;">
                <img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:70px; float: left;">
                <h2 class="text-uppercase">
                    {{ $pageData->title }}
                </h2>
            </div>

            <br>

            @include($pageData->include)

        </div>
    </body>
</html>
