<div class="row">
    <div class="col-xs-{{ $merchant? '6' : '12 text-center' }}">
        <h4>{{ config('app.company.name') }} <small>({{ config('app.company.reg_no') }})</small></h4>
        {!! config('app.company.address') !!}
        <br><b>@lang('localize.gst_no') : </b> {{ env('GST') }}
    </div>
    @if($merchant)
    <div class="col-xs-6" style="border-left:1px solid #eee;">
        <h4>{{trans('localize.merchant_detail')}}</h4>
        <b>{{ $merchant->full_name() }}</b> <br>
        {!! ucwords(implode('<br>', array_map('trim', array_filter([$merchant->mer_address1, $merchant->mer_address2, $merchant->mer_city])))) !!} <br>
        {{ ucwords(trim(implode(', ', array_filter([$merchant->zipcode, $merchant->state? $merchant->state->name : null, $merchant->country? $merchant->country->co_name : null])))) }} <br>
    </div>
    @endif
</div>

<hr/>

<div class="row">
    <div class="col-xs-6">
        <h4>@lang('localize.paymentdetail')</h4>
        <b>@lang('localize.date') : </b>{{ \Helper::UTCtoTZ($transaction->date) }} <br>
    </div>
    <div class="col-xs-6" style="border-left:1px solid #eee;">
        <h4>{{trans('localize.shippingaddress')}}</h4>
        @if($transaction->shipping_address)
        @php
            $address = $transaction->shipping_address;
        @endphp
        <b>{{ $address->ship_name }}</b><br>
        {!! ucwords(implode('<br>', array_map('trim', array_filter([$address->ship_address1, $address->ship_address2, $address->ship_city_name])))) !!} <br>
        {{ ucwords(trim(implode(', ', array_filter([$address->ship_postalcode, $address->state? $address->state->name : null, $address->country? $address->country->co_name : null])))) }} <br>
        <b>@lang('localize.phone') : </b>{{ $address->phone() }}
        @endif
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-sm-12">
        <table border=0 width="100%">
            <tr>
                <th width="35%" style="padding-left:5px;">@lang('localize.do.number_')</th>
                <th width="5%">:</th>
                <td colspan='3'>
                    @php
                        $do = $transaction->invoices->map(function ($iv) {
                            $iv->tax_number = $iv->tax_number('ON');
                            return $iv;
                        })->pluck('tax_number')->toArray();

                        array_walk($do, function(&$value, $key) { $value = "$value"; } );
                    @endphp
                    {{ implode(', ', $do) }}
                </td>
            </tr>

            <tr><td colspan='5'>&nbsp;</td></tr>

            {{-- <tr>
                <th colspan='2'>&nbsp;</th>
                <td class='text-left'><b>@lang('localize.amount') ({{ $additional->currencyCode }})</b></td>
                <td></td>
            </tr> --}}
            <tr>
                <th style="padding-left:5px;">@lang('localize.amount')</td>
                <th>:</th>
                <td class='text-left'>{{ ($additional->currencyCode == 'IDR') ? rpFormat($charges->price->amount) : $additional->currencyCode . ' ' . number_format(round($charges->price->amount, 2), 2) }}</td>
                {{-- <td> * </td> --}}
            </tr>
            <tr>
                <th style="padding-left:5px;">@lang('localize.shipping_fees')</td>
                <th>:</th>
                <td class='text-left'>{{ ($additional->currencyCode == 'IDR') ? rpFormat($charges->price->shippingFees) : $additional->currencyCode . ' ' . number_format(round($charges->price->shippingFees, 2), 2) }}</td>
                {{-- <td> * </td> --}}
            </tr>

            @if(!$merchant)
            <tr><td colspan='5'><br/></td></tr>
            <tr class="text-left" style="border-top:1pt solid #d1cfcf; padding:1px;">
                <th style="padding-left:5px;">@lang('localize.platform_charges') {{ $additional->platformChargeRate }}%</th>
                <th>:</th>
                <td class='text-left'>{{ ($additional->currencyCode == 'IDR') ? rpFormat($charges->price->platformCharge) : $additional->currencyCode . ' ' . number_format(round($charges->price->platformCharge, 2), 2) }}</td>
                {{-- <td> * </td> --}}
            </tr>
            <tr class="text-left" style="">
                <th style="padding-left:5px;">@lang('localize.service_charge') {{ $additional->serviceChargeRate }}%</td>
                <th>:</th>
                <td class='text-left'>{{ ($additional->currencyCode == 'IDR') ? rpFormat($charges->price->serviceCharge) : $additional->currencyCode . ' ' . number_format(round($charges->price->serviceCharge, 2), 2) }}</td>
                {{-- <td> * </td> --}}
            </tr>
            <tr style="border-bottom:1pt solid #d1cfcf;">
                <th style="padding-left:5px;">@lang('localize.total_amount')</td>
                <th>:</th>
                <td class='text-left'>{{ ($additional->currencyCode == 'IDR') ? rpFormat($charges->price->total) : $additional->currencyCode . ' ' . number_format(round($charges->price->total, 2), 2) }}</td>
                {{-- <td></td> --}}
            </tr>
            <tr><td colspan='5'><br/></td></tr>
            @endif

            {{-- <tr>
                <th style="padding-left:5px;">@lang('localize.merchant_charge') {{ $merchant? $additional->merchantChargeRate.'%' : '' }}</td>
                <th>:</th>
                <td class='text-left'>{{ number_format($charges->credit->merchantCharge, 4) }}</td>
                <td class='text-left'>{{ number_format($charges->price->merchantCharge, 2) }}</td>
                <td> * </td>
            </tr> --}}
            <tr><td colspan='5'><br/></td></tr>
            {{-- <tr>
                <th style="padding-left:5px;">@lang('localize.merchant_earned_credit')</td>
                <th>:</th>
                <th class='text-left'>{{ number_format($charges->credit->merchantEarn, 4) }}</th>
                <th class='text-left'>{{ number_format($charges->price->merchantEarn, 2) }}</th>
            </tr> --}}

        </table>
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-sm-12 text-center">
        <h4>@lang('localize.invoicedetail')</h4>
        <p>
            <span>@lang('localize.thisshipmentcontainfolowingitem')</span>
        </p>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table width="100%">

            <tr style="border-bottom: 1px solid #949494">
                <th class="text-left" style="padding: 10px;">@lang('localize.items')</th>
                <th class="text-center" style="padding: 10px;">@lang('localize.type')</th>
                <th class="text-center" style="padding: 10px;">@lang('localize.quantity')</th>
                <th class="text-center" nowrap style="padding: 10px;">@lang('localize.amount')</th>
            </tr>

            @foreach ($transaction->items as $order)
            <tr class="text-center">
                <td class="text-left" width="50%;" style="padding: 10px;">
                    {{ $order->product? $order->product->title : '' }}
                    @if($order->options)
                    <p><br>
                    @foreach ($order->options as $parent => $child)
                        <b>{{ $parent }} : </b> {{ $child }} @if(!$loop->last)<br>@endif
                    @endforeach
                    </p>
                    @endif
                </td>
                <td style="padding: 10px;" nowrap>{{ $order->type() }}</td>
                <td style="padding: 10px;">{{ $order->order_qty }}</td>
                {{-- @if($merchant)
                <td style="padding: 10px;">{{ number_format($order->credit + $order->total_product_shipping_fees_credit, 4) }}</td>
                @else
                <td style="padding: 10px;">{{ number_format($order->order_vtokens, 4) }}</td>
                @endif --}}
                <td style="padding: 10px;">{{ ($additional->currencyCode == 'IDR') ? rpFormat($order->order_value) : $additional->currencyCode . ' ' . number_format($order->order_value, 2) }}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="10" style="padding: 10px;"></td>
            </tr>
            <tr class="text-center" style="border-top:1px solid #d1cfcf;">
                <td class="text-right" colspan="3" style="padding: 10px;"><strong>@lang('localize.total_amount')</strong></td>
                {{-- @if($merchant)
                <td style="padding: 10px;"><strong>{{ number_format($transaction->items->sum('credit') + $transaction->items->sum('total_product_shipping_fees_credit'), 4) }}</strong></td>
                @else
                <td style="padding: 10px;"><strong>{{ number_format($transaction->items->sum('order_vtokens'), 4) }}</strong></td>
                @endif --}}
                <td style="padding: 10px;">{{ ($additional->currencyCode == 'IDR') ? rpFormat($transaction->items->sum('order_value')) : $additional->currencyCode . ' ' . number_format($transaction->items->sum('order_value'), 2) }}</td>
            </tr>

        </table>
    </div>

    {{-- <hr>

    <div class="col-sm-12">
        * GST inclusive if applicable<br>
        This invoice is computer generated and no signature is required.
    </div> --}}

</div>