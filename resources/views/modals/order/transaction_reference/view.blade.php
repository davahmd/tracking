<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="row">
        <div class="col-sm-3"><img src="{{ url('/common/images/logo.png') }}" alt="" style="width:auto; height:35px;"></div>
    <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase">{{ $pageData->title }}</h3></div>
    </div>
</div>

<div class="modal-body">
    @include($pageData->include)
</div>

<div class="modal-footer">
    @if($pageData->downloadLink)
    <a href="{{ $pageData->downloadLink }}" class="btn btn-primary">@lang('localize.download')</a>
    @endif

    @if($pageData->printLink)
    <a href="javascript:void(0)" class="btn btn-primary" onclick="window.open('{{ $pageData->printLink }}', 'newwindow', 'width=750, height=500'); return false;">@lang('localize.print')</a>
    @endif

    <button data-dismiss="modal" class="btn btn-default" type="button">@lang('localize.closebtn')</button>
</div>
