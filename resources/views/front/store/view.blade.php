@extends('layouts.front_master')

@section('title', $store->stor_name)

@section('content')
@php
    $compiledRatings = $store->compiledRatings();
@endphp
<div id="main">
    <div class="container">
        {{--<div class="banner-container mr-top-md">
            <!-- banner -->
        </div>--}}
        <div class="coverboard">
            <div class="merchant-detail">
                <div class="merchant-logo-frame">
                    <img class="merchant-image" src="{{ $store->getImageUrl() }}" onerror="if (this.src != 'error.jpg') this.src = '/asset/images/product/default.jpg';"/>
                </div>
                <div class="merchant-main">
                    <h5 style="margin-bottom: 0;">{{ $store->stor_name }}</h5>
                    <span class="card-rating">
                        @include('front.partial.star.tag', ['rating' => $compiledRatings])
                    </span>
                    {{--<p class="merchant-online">Online 20 hours ago</p>--}}
                    {{-- <a href="javascript:void(0)" class="btn btn-blue">Follow</a> --}}
                    {{-- <a href="javascript:void(0)" class="btn btn-blue">Chat</a> --}}
                </div>
            </div>
            <div class="merchant-features">
                {{--<div class="features-list"><p>@lang('common.favourite')</p><h3>23k</h3></div>--}}
                <div class="features-list"><p>@lang('common.pro_sold')</p><h3>{{ ($store->products->sum('pro_no_of_purchase') == 0)?'-':$store->products->sum('pro_no_of_purchase') }}</h3></div>
                <div class="features-list"><p>@lang('common.since')</p><h3>{{ date_format($store->created_at, 'M Y') }}</h3></div>
                <div class="features-list"><p>@lang('common.ratings')</p><h3>{{ $compiledRatings->rate }}</h3></div>
            </div>
            <div class="more">
                <a href="/store/profile/{{ $store->url_slug }}" class="view-more">@lang('front.nav.store_profile')&ensp;<span><i class="fa fa-arrow-right"></i></span>
            </div>
        </div>

        <div class="tab-container mr-top-md">
            {{--<ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-product">@lang('front.store.profile')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-review">@lang('common.rate_and_rev')</a>
                </li>
            </ul>--}}
            <!-- Tab panes -->
            <div class="tab-content">
                <div id="tab-product" class="tab-pane active">
                    <div class="best-product-container mr-y-lg">
                        <div class="topbar d-flex">
                            <h4>@lang('front.section.latest_products')</h4>
                            <div class="red-head"></div>
                        </div>
                        <div class="item-container">
                            @foreach($latest_products as $latest_product)
                                <div class="card">
                                    <a href="/product/{{ Helper::slug_maker($latest_product->pro_title_en, $latest_product->pro_id) }}">
                                        <div class="card-thumb">
                                            @if (array_key_exists($latest_product->pro_id, $promo_latest_products) && ($promo_item_info[$latest_product->pro_id]->limit > $promo_item_info[$latest_product->pro_id]->count) && ($promo_latest_products[$latest_product->pro_id]->started_at <= $today_datetime && $promo_latest_products[$latest_product->pro_id]->ended_at >= $today_datetime))
                                                <span class="discount-rate bg-orange">{{ $promo_latest_products[$latest_product->pro_id]->discount_rate ? 100*($promo_latest_products[$latest_product->pro_id]->discount_rate) . '% OFF' : rpFormat($promo_latest_products[$latest_product->pro_id]->discount_value) . ' OFF' }}</span>
                                            @endif
                                            <img class="card-img-top" src="{{ \Storage::url('product/' . $latest_product->pro_mr_id . '/' . $latest_product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                                        </div>
                                        <div class="card-body">
                                            @if ($latest_product->pro_qty < 1)
                                            <span class="sold-out-tag">@lang('localize.outStock')</span>
                                            @endif
                                            <h6 class="card-title">{{ $latest_product->title }}</h6>
                                            {{-- <div class="price-layer">
                                                <span class="with_discounted" style="display: {{ ($latest_product->pricing->first()->getPurchasePrice() <> $latest_product->pricing->first()->price)? 'block' : 'none' }};">
                                                    <h6 class="cut black">
                                                        <small>
                                                            <span class="discounted_price">{{ rpFormat($latest_product->pricing->first()->price) }}</span>
                                                        </small>
                                                    </h6>
                                                    <h5 class="red">
                                                        <div>
                                                            <span class="discounted_purchase_price">{{ rpFormat($latest_product->pricing->first()->getPurchasePrice()) }}</span>
                                                        </div>
                                                    </h5>
                                                </span>
                                                <h5 class="red">
                                                    <span class="without_discounted" style="display: {{ ($latest_product->pricing->first()->getPurchasePrice() == $latest_product->pricing->first()->price)? 'block' : 'none' }};">
                                                        <div>
                                                            <span class="discounted_purchase_price">{{ rpFormat($latest_product->pricing->first()->getPurchasePrice()) }}</span>
                                                        </div>
                                                    </span>
                                                </h5>
                                            </div> --}}
                                            <div class="price-layer">
                                                @if (array_key_exists($latest_product->pro_id, $promo_latest_products) && ($promo_item_info[$latest_product->pro_id]->limit > $promo_item_info[$latest_product->pro_id]->count) && ($promo_latest_products[$latest_product->pro_id]->started_at <= $today_datetime && $promo_latest_products[$latest_product->pro_id]->ended_at >= $today_datetime))
                                                    <span class="with_discounted">
                                                        <h5 class="red">
                                                            <div>
                                                                <span class="discounted_purchase_price">{{ $promo_latest_products[$latest_product->pro_id]->discount_rate ? rpFormat($latest_product->pricing->first()->price *(1-$promo_latest_products[$latest_product->pro_id]->discount_rate)) : rpFormat($latest_product->pricing->first()->price - $promo_latest_products[$latest_product->pro_id]->discount_value) }}</span>
                                                            </div>
                                                        </h5>
                                                        <h6 class="cut black">
                                                            <small>
                                                                <span class="discounted_price">{{ rpFormat($latest_product->pricing->first()->price) }}</span>
                                                            </small>
                                                        </h6>
                                                    </span>
                                                @else
                                                    <h5 class="red">
                                                        <span class="without_discounted">
                                                            <div>
                                                                <span class="discounted_purchase_price">{{ rpFormat($latest_product->pricing->first()->price) }}</span>
                                                            </div>
                                                        </span>
                                                    </h5>
                                                @endif
                                            </div>
                                            {{--<p class="card-text red">{{ rpFormat($latest_product->lowest_price) }}</p>--}}
                                            {{-- <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span> --}}
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="more">
                            <a href="/store/{{ $store->url_slug }}/latest-products" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span>
                        </div>
                    </div>
                    <div class="promotion-product-container mr-y-lg">
                        <div class="topbar d-flex">
                            <h4 class="mr-auto">@lang('front.section.most_popular')</h4>
                            <div class="red-head"></div>
                        </div>
                        <div class="item-container">
                            @foreach($trending_products as $trending_product)
                            <div class="card">
                                <a href="/product/{{ Helper::slug_maker($trending_product->pro_title_en, $trending_product->pro_id) }}">
                                    <div class="card-thumb">
                                        @if (array_key_exists($trending_product->pro_id, $promo_trending_products) && ($promo_item_info[$trending_product->pro_id]->limit > $promo_item_info[$trending_product->pro_id]->count) && ($promo_trending_products[$trending_product->pro_id]->started_at <= $today_datetime && $promo_trending_products[$trending_product->pro_id]->ended_at >= $today_datetime))
                                            <span class="discount-rate bg-orange">{{ $promo_trending_products[$trending_product->pro_id]->discount_rate ? 100*($promo_trending_products[$trending_product->pro_id]->discount_rate) . '% OFF' : rpFormat($promo_trending_products[$trending_product->pro_id]->discount_value) . ' OFF' }}</span>
                                        @endif
                                        {{-- <span class="discount-rate bg-orange">Negotiable!</span> --}}
                                        <img class="card-img-top" src="{{ \Storage::url('product/' . $trending_product->pro_mr_id . '/' . $trending_product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                                    </div>
                                    <div class="card-body">
                                        @if ($trending_product->pro_qty < 1)
                                        <span class="sold-out-tag">@lang('localize.outStock')</span>
                                        @endif
                                        <h6 class="card-title">{{ $trending_product->title }}</h6>
                                        <div class="d-flex flex-column">
                                            {{-- <div class="price-layer">
                                                <span class="with_discounted" style="display: {{ ($trending_product->pricing->first()->getPurchasePrice() <> $trending_product->pricing->first()->price)? 'block' : 'none' }};">
                                                    <h6 class="cut black">
                                                        <small>
                                                            <span class="discounted_price">{{ rpFormat($trending_product->pricing->first()->price) }}</span>
                                                        </small>
                                                    </h6>
                                                    <h5 class="red">
                                                        <div>
                                                            <span class="discounted_purchase_price">{{ rpFormat($trending_product->pricing->first()->getPurchasePrice()) }}</span>
                                                        </div>
                                                    </h5>
                                                </span>
                                                <h5 class="red">
                                                    <span class="without_discounted" style="display: {{ ($trending_product->pricing->first()->getPurchasePrice() == $trending_product->pricing->first()->price)? 'block' : 'none' }};">
                                                        <div>
                                                            <span class="discounted_purchase_price">{{ rpFormat($trending_product->pricing->first()->getPurchasePrice()) }}</span>
                                                        </div>
                                                    </span>
                                                </h5>
                                            </div> --}}
                                            <div class="price-layer">
                                                @if (array_key_exists($trending_product->pro_id, $promo_trending_products) && ($promo_item_info[$trending_product->pro_id]->limit > $promo_item_info[$trending_product->pro_id]->count) && ($promo_trending_products[$trending_product->pro_id]->started_at <= $today_datetime && $promo_trending_products[$trending_product->pro_id]->ended_at >= $today_datetime))
                                                    <span class="with_discounted">
                                                        <h5 class="red">
                                                            <div>
                                                                <span class="discounted_purchase_price">{{ $promo_trending_products[$trending_product->pro_id]->discount_rate ? rpFormat($trending_product->pricing->first()->price *(1-$promo_trending_products[$trending_product->pro_id]->discount_rate)) : rpFormat($trending_product->pricing->first()->price - $promo_trending_products[$trending_product->pro_id]->discount_value) }}</span>
                                                            </div>
                                                        </h5>
                                                        <h6 class="cut black">
                                                            <small>
                                                                <span class="discounted_price">{{ rpFormat($trending_product->pricing->first()->price) }}</span>
                                                            </small>
                                                        </h6>
                                                    </span>
                                                @else
                                                    <h5 class="red">
                                                        <span class="without_discounted">
                                                            <div>
                                                                <span class="discounted_purchase_price">{{ rpFormat($trending_product->pricing->first()->price) }}</span>
                                                            </div>
                                                        </span>
                                                    </h5>
                                                @endif
                                            </div>
                                            {{--<p class="card-text red">{{ rpFormat($trending_product->lowest_price) }}</p>--}}
                                            {{-- <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span> --}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @break($loop->iteration == 6)
                            @endforeach
                        </div>
                       <div class="more">
                            <a href="/store/{{ $store->url_slug }}/most-popular" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span>
                        </div>
                    </div>
                    {{-- <div class="latest-product-container mr-y-lg">
                        <div class="topbar d-flex">
                            <h4 class="mr-auto">@lang('front.section.most_favourited')</h4>
                            <div class="red-head"></div>
                        </div>
                        <div class="item-container">
                            <div class="card">
                                <a href="javascript:void(0)">
                                    <img class="card-img-top" src="images/product/5.png" alt="Card image">
                                    <div class="card-body">
                                        <span class="discount-rate bg-orange">44% OFF</span>
                                        <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                                        <div class="d-flex flex-column">
                                        <p class="card-text black cut"><small>RP 650.000</small></p>
                                        <p class="card-text red">RP 510.000</p>
                                        <span class="card-rating">
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star"></i>
                                            <span class="shine" style="font-size: .6rem; font-weight: bold;">(510)</span>
                                        </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="more">
                            <a href="merchant_product.html" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span>
                        </div>
                    </div> --}}
                </div>
                <div id="tab-review" class="tab-pane fade">
                    <div class="topbar d-flex mr-top-md">
                        <h4>@lang('front.section.store_rate', ['stor' => $store->stor_name])</h4>
                        <div class="red-head"></div>
                    </div>
                    <div class="rate-container">
                        <div class="rate-total text-center">
                            <h1>4.0<small>/5</small></h1>
                            <span class="card-rating">
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star"></i>
                            </span>
                            <p style="margin-bottom: 0;">47 Rating</p>
                        </div>
                        <div class="rate-status">
                            <div class="d-flex align-items-center">
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                </span>
                                <div class="progress" style="height:10px; width:60%;">
                                    <div class="progress-bar" style="width:7%"></div>
                                </div>
                                <span style="margin-left: 15px;">7</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                                <div class="progress" style="height:10px; width:60%;">
                                    <div class="progress-bar" style="width:28%"></div>
                                </div>
                                <span style="margin-left: 15px;">28</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                                <div class="progress" style="height:10px; width:60%;">
                                    <div class="progress-bar" style="width:10%"></div>
                                </div>
                                <span style="margin-left: 15px;">10</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                                <div class="progress" style="height:10px; width:60%;">
                                    <div class="progress-bar" style="width:4%"></div>
                                </div>
                                <span style="margin-left: 15px;">4</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                                <div class="progress" style="height:10px; width:60%;">
                                    <div class="progress-bar" style="width:1%"></div>
                                </div>
                                <span style="margin-left: 15px;">1</span>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="topbar d-flex mr-top-md">
                        <h4>@lang('front.section.reviews')</h4>
                        <div class="red-head"></div>
                    </div>
                    <div class="review-container">
                        <div class="write-review">
                            <textarea placeholder="Write your review here.."></textarea>
                        </div>
                        <div class="d-flex justify-content-between">
                            <span class="card-rating">
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <span class="black" style="font-size: .8rem; margin-left: 5px;">Give your rate</span>
                            </span>
                            <button class="btn btn-blue" type="submit">Submit</button>
                        </div>
                        <div class="review-list mr-y-md">
                            <div class="review-item d-flex">
                                <img class="review-item-img" src="images/product/15.png" />
                                <div class="review-item-content">
                                    <h6 class="review-item-name">Wheel Rim OEM Audi A4 18 Inch Set</h6>
                                    <span class="card-rating">
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                                    <div class="review-message">Barang bagus, kondisi sangat mulus walaupun bekas, seperti baru, seller top!
                                    </div>
                                    <span class="review-user">by <a href="javascript:void(0)">Evan McKinney</a></span>
                                    <span class="review-datetime">19 April 2018, 20:22</span>
                                </div>
                            </div>
                            <div class="review-item d-flex">
                                <img class="review-item-img" src="images/product/15.png" />
                                <div class="review-item-content">
                                    <h6 class="review-item-name">Wheel Rim OEM Audi A4 18 Inch Set</h6>
                                    <span class="card-rating">
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star shine"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                                    <div class="review-message">Barang bagus, kondisi sangat mulus walaupun bekas, seperti baru, seller top!
                                    </div>
                                    <span class="review-user">by <a href="javascript:void(0)">Dennis Hardy</a></span>
                                    <span class="review-datetime">19 April 2018, 20:22</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection