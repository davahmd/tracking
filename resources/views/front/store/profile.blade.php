@extends('layouts.front_master')

@section('title', $store->stor_name)

@section('content')
<div id="main">
    <div class="container">
        {{--<div class="banner-container mr-top-md">
            <!-- banner -->
        </div>--}}
        <div class="coverboard">
            <div class="merchant-detail">
                <div class="d-flex">
                    <div class="merchant-logo-frame">
                        <img class="merchant-image" src="{{ $store->getImageUrl() }}" onerror="if (this.src != 'error.jpg') this.src = '/asset/images/product/default.jpg';" />
                    </div>
                    <div class="merchant-main">
                        <h5 style="margin-bottom: 0;">{{ $store->stor_name }}</h5>
                        @include('front.partial.star.tag', ['rating' => $compiledRatings])
                        {{--<p class="merchant-online">Online 20 hours ago</p>--}}
                    </div>
                </div>
                <br>
                @if (!empty($store->short_description))
                <div class="merchant-quote"><q>{{ $store->short_description }}</q></div>
                @endif
            </div>
            <div class="merchant-info">
                <div class="session session-activity">
                    <div class="inner">
                        <p class="session-title">@lang('localize.activity')</p>
                        {{-- <div class="session-item">
                            <p class="">Last Seen</p>
                            <p style="color:#858585;">An hour ago</p>
                        </div> --}}
                        <div class="session-item">
                            <p>@lang('common.since')</p>
                            <p style="color:#858585;">{{ date_format($store->created_at, 'd M Y') }}</p>
                        </div>
                        <div class="session-item text-right">
                            <p>@lang('common.operation_hour')</p>
                            <p style="color:#858585;">
                            @for ($i = 0; $i < 7; $i++)
                                @lang('localize.day_of_the_week.'.$i):
                                @if($store->storeBusinessHour($i)['status'] == 1)
                                    {{ $store->storeBusinessHour($i)['open_time'] }} - {{ $store->storeBusinessHour($i)['close_time'] }}
                                @else
                                    Closed
                                @endif
                                <br>
                            @endfor
                            </p>
                        </div>
                        <div class="session-item">
                            <p>@lang('common.delivery_from')</p>
                            <p style="color:#858585;">{{ $store->city->name }}, {{ $store->state->name }}</p>
                        </div>
                    </div>
                </div>
                <div class="session session-address">
                    <div class="inner">
                        <p class="session-title">@lang('front.store.address')</p>
                        <div class="session-item">
                            <h6>{{ $store->stor_name }}</h6>
                            <div class="d-flex" style="padding: 4px 10px;">
                                <span><i class="fa fa-map-marked-alt"></i></span>
                                <p>{{ $store->stor_address1 }}<Br/>{{ $store->stor_address2 }}</p>
                            </div>
                            <div class="d-flex" style="padding: 4px 10px;">
                                <span><i class="fa fa-phone"></i></span>
                                <p>{{ $store->stor_phone }}</p>
                            </div>
                            @if (!empty($store->stor_website))
                            <div class="d-flex" style="padding: 4px 10px;">
                                <span><i class="fa fa-link"></i></span>
                                <p><a href="{{ $store->stor_website }}" target="_blank">{{ !empty($store->stor_website)?$store->stor_website:'-' }}</a></p>
                            </div>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="session-item">
                        <p class="see-more"><a href="javascript:void(0)" data-toggle="modal" data-target="#branchModal">See Branches (12)</a></p>
                    </div>--}}

                    {{-- <!-- The Modal -->
                    <div class="modal fade" id="branchModal">
                        <div class="modal-dialog modal-md modal-dialog-centered">
                            <div class="modal-content branch-address-modal">
                                <div class="topbar d-flex flex-row">
                                    <h4 class="modal-title mr-auto">Other Branches</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="session-item mr-top-sm">
                                    <h6>Iceberg Window Films (Branch Jakarta)</h6>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">place</i>
                                        <p>Komplek Gamma, Jl. Alpha Beta Blok C3 no. 44 Leuwigajah, Cimahi Selatan, Kota Cimahi Jawa Barat 40533</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">call</i>
                                        <p>083822561496</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">email</i>
                                        <p>hello@icebergwindows.com</p>
                                    </div>
                                </div>
                                <div class="session-item">
                                    <h6>Iceberg Window Films (Branch Medan)</h6>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">place</i>
                                        <p>Komplek Gamma, Jl. Alpha Beta Blok C3 no. 44 Leuwigajah, Cimahi Selatan, Kota Cimahi Jawa Barat 40533</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">call</i>
                                        <p>083822561496</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">email</i>
                                        <p>hello@icebergwindows.com</p>
                                    </div>
                                </div>
                                <div class="session-item">
                                    <h6>Iceberg Window Films (Branch Surabaya)</h6>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">place</i>
                                        <p>Komplek Gamma, Jl. Alpha Beta Blok C3 no. 44 Leuwigajah, Cimahi Selatan, Kota Cimahi Jawa Barat 40533</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">call</i>
                                        <p>083822561496</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">email</i>
                                        <p>hello@icebergwindows.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                {{-- <div class="session session-courier">
                    <div class="inner">
                        <p class="session-title">Courier Support</p>
                        <div class="session-item">
                            <p class="">DHL</p>
                        </div>
                        <div class="session-item">
                            <p class="">JNE</p>
                        </div>
                        <div class="session-item">
                            <p class="">JNT</p>
                        </div>
                        <div class="session-item">
                            <p class="">FEDEX</p>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="session session-statistic">
                    <div class="inner">
                        <p class="session-title">@lang('common.statistic')</p>
                        <div class="session-item">
                            <p class="">Transaction Done</p>
                            <p style="color:#858585;">223,545</p>
                        </div>
                        <div class="session-item">
                            <p>Total Storefronts</p>
                            <p style="color:#858585;">12</p>
                        </div>
                        <div class="session-item">
                            <p>Total Products</p>
                            <p style="color:#858585;">245</p>
                        </div>
                        <div class="session-item">
                            <p>Average Rating</p>
                            <p style="color:#858585;">4.8/5</p>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="more">
                <a href="{{ route('store.product', $store->url_slug) }}" class="view-more">@lang('front.nav.store_products')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
            </div>
        </div>
        <br>

        <!----  NEW SECTION : RATING & REVIEWS --->
        <div class="section-review">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('localize.review_rating')</h4>
                <div class="red-head"></div>
            </div>
            <div class="review-panel">
                <div class="rating">
                    <div class="current-rate">
                        <h2>{{ $compiledRatings->rate }}/5</h2>
                        @include('front.partial.star.tag', ['rating' => $compiledRatings])
                        <label>{{ $compiledRatings->total }} @lang('localize.rating')</label>
                    </div>
                    <div class="rate-detail">

                        @foreach ($compiledRatings->cumulative as $i => $rating)
                        <div class="list">
                            @include('front.partial.star.tag', ['percentage' => $i * 20])
                            @include('front.partial.star.bar')
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="review">
                    <h4>@lang('localize.review')</h4>

                    <div class="review-content">
                        @foreach ($ratings as $rating)
                        @include('front.partial.rating')
                        @endforeach
                    </div>

                    <ul class="pagination">
                        {{ $ratings->links() }}
                    </ul>

                </div>
            </div>
        </div>
        <!----  END NEW SECTION  --->
    </div>
</div>
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/blueimp/css/blueimp-gallery.min.css') }}">
@endsection

@section('script')
<script src="{{ asset('backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
@endsection