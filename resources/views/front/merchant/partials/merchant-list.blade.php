@foreach($merchants as $merchant)
    <div class="card">
        @if(auth()->user()->isRetailerForDistributor($merchant))
            <span class="distributor-check bg-green"><i class="fa fa-check"></i></span>
        @endif
        <a href="/merchants/{{ $merchant->mer_id }}">
            <img class="card-img" src="{{ $merchant->imageUrl }}" onerror="this.onerror=null;this.src='/asset/images/brand/default.jpg';">
        </a>
    </div>
@endforeach