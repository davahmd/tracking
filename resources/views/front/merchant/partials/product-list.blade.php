@foreach($products as $product)
    <div class="card">
        <a href="{{ route('product-wholesale', Helper::slug_maker($product->pro_title_en, $product->pro_id)) }}">
            <div class="card-thumb">
                <img class="card-img-top" src="{{ \Storage::url('product/' . $product->pro_mr_id . '/' . $product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
            </div>
            <div class="card-body">
                @if ($product->pro_qty < 1)
                    <span class="sold-out-tag">@lang('localize.outStock')</span>
                @endif
                <h6 class="card-title">{{ $product->title }}</h6>
                <div class="price-layer">
                    {{--<span class="with_discounted" style="display: {{ ($product->pricing->first()->getPurchasePrice() <> $product->pricing->first()->price)? 'block' : 'none' }};">--}}
                    <span class="with_discounted">
                        <h6 class="cut black">
                            <small>
                                <span class="discounted_price">{{ rpFormat($product->pricing->first()->getPurchasePrice()) }}</span>
                            </small>
                        </h6>
                        <h5 class="red">
                            <div>
                                @if(auth()->user()->isRetailerForDistributor($product->merchant))
                                    <span class="discounted_purchase_price">{{ rpFormat($product->pricing->first()->wholesale_price) }}</span>
                                @else
                                    <span class="discounted_purchase_price">{{ config('app.currency_code') }} ???</span>
                                @endif
                            </div>
                        </h5>
                    </span>
                    {{--<h5 class="red">--}}
                        {{--<span class="without_discounted" style="display: {{ ($product->pricing->first()->getPurchasePrice() == $product->pricing->first()->price)? 'block' : 'none' }};">--}}
                            {{--<div>--}}
                                {{--<span class="discounted_purchase_price">{{ rpFormat($product->pricing->first()->wholesale_price) }}</span>--}}
                            {{--</div>--}}
                        {{--</span>--}}
                    {{--</h5>--}}
                </div>
                {{--<p class="card-text red">{{ rpFormat($product->lowest_price) }}</p>--}}
                {{-- <span class="card-rating">
                    <i class="fa fa-star shine"></i>
                    <i class="fa fa-star shine"></i>
                    <i class="fa fa-star shine"></i>
                    <i class="fa fa-star shine"></i>
                    <i class="fa fa-star"></i>
                </span> --}}
            </div>
        </a>
    </div>
@endforeach