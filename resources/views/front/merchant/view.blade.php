@extends('layouts.front_master')

@section('title', $merchant->merchantName())

@section('style')
    <style>
        .coverboard + .tab-container {
            background-color: transparent;
        }

        .coverboard + .tab-container .tab-content {
            padding: 0;
        }
    </style>
@endsection

@section('content')
    <div id="main">
        <div class="container">
            {{--<div class="banner-container mr-top-md">
                <!-- banner -->
            </div>--}}
            <div class="coverboard">
                <div class="merchant-detail">
                    <div class="merchant-logo-frame">
                        <img class="merchant-image" src="{{ $merchant->imageUrl }}" onerror="if (this.src != 'error.jpg') this.src = '/asset/images/product/default.jpg';"/>
                    </div>
                    <div class="merchant-main">
                        <h5 style="margin-bottom: 0;">{{ $merchant->merchantName() }}</h5>

                        {{--<span class="card-rating">--}}
                    {{--@php $rating = $store->reviews->avg('rating'); @endphp--}}
                            {{--@foreach(range(1,5) as $i)--}}
                                {{--<span class="fa-stack" style="width:1em">--}}
                            {{--<i class="far fa-star fa-stack-1x"></i>--}}
                                    {{--@if($rating >0)--}}
                                        {{--@if($rating >0.5)--}}
                                            {{--<i class="fa fa-star shine"></i>--}}
                                        {{--@else--}}
                                            {{--<i class="fa fa-star-half shine"></i>--}}
                                        {{--@endif--}}
                                    {{--@endif--}}
                                    {{--@php $rating--; @endphp--}}
                        {{--</span>--}}
                            {{--@endforeach--}}

                    {{--</span>--}}
                        {{-- <p class="merchant-online">Online 20 hours ago</p> --}}
                        {{--<a href="javascript:void(0)" class="btn btn-blue">Follow</a>--}}
                        {{--<a href="javascript:void(0)" class="btn btn-blue">Chat</a>--}}
                        @if(auth()->user()->merchant)
                            @if($distributorRelationship)
                                @if($distributorRelationship->pivot->status == 0)
                                        <span><i class="fa fa-circle-thin"></i> Request Sent</span>
                                @elseif($distributorRelationship->pivot->status == 1)
                                    <span><i class="fa fa-check-circle"></i> Distributor</span>
                                @endif
                            @else
                                <form action="{{ url('merchants/distributors/' . $merchant->mer_id . '/request') }}" method="POST">
                                    {{ csrf_field() }}
                                    <button class="btn btn-blue" type="submit">Request To Be A Retailer</button>
                                </form>
                            @endif
                        @endif
                    </div>
                </div>

                {{--<div class="merchant-features">--}}
                    {{--<div class="features-list"><p>@lang('common.pro_sold')</p><h3>{{ $merchant->completedOrders()->count() }}</h3></div>--}}
                    {{--<div class="features-list"><p>@lang('common.since')</p><h3>{{ date_format($merchant->created_at, 'M Y') }}</h3></div>--}}
                    {{--<div class="features-list"><p>@lang('common.ratings')</p><h3>-</h3></div>--}}
                {{--</div>--}}
                <div class="more">
                    <a href="/merchants/profile/{{ $merchant->mer_id }}" class="view-more">@lang('front.nav.mer_profile')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
                </div>

            </div>

            <div class="tab-container mr-top-md">
            {{--<ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-product">@lang('front.store.profile')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-review">@lang('common.rate_and_rev')</a>
                </li>
            </ul>--}}
            <!-- Tab panes -->
                <div class="tab-content">
                    <div id="tab-product" class="tab-pane active">
                        <div class="best-product-container mr-y-lg">
                            <div class="topbar d-flex">
                                <h4>@lang('localize.Products')</h4>
                                <div class="red-head"></div>
                            </div>
                            <div class="item-container">
                                @include('front.merchant.partials.product-list', ['products' => $products])
                            </div>
                            {{--<div class="more">--}}
                                {{--<a href="/store/{{ $store->url_slug }}/latest-products" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>--}}
                            {{--</div>--}}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection