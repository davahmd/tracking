@extends('layouts.front_master')
@section('header')
    @include('layouts.web.header.main')
@endsection

@section('title', 'Distributors ')

@section('style')
    <style type="text/css">
        #loader {
            color: red;
        }

        .distributor-check:not([href]):not([tabindex]) {
            position: absolute;
            z-index: 1;
            top: 5px;
            right: 10px;
            padding: .25rem .4rem;
            border-radius: 50%;
            color: #ffffff;
            font-size: 0.7em;
        }
    </style>
@endsection


@section('content')
    <div id="main" class="index-container">
        <div class="official-merchant-container container pd-y-lg">
            <div class="topbar d-flex flex-row-reverse">
                <form class="form-inline">
                    <select name="page" id="page" class="form-control">
                        <option value="{{ url('merchants/distributors') }}">Distributors</option>
                        <option value="{{ url('merchants/distributors/products') }}">Products</option>
                    </select>
                </form>
            </div>
            <div class="topbar d-flex">
                <h4>@lang('Distributors')</h4>
                <div class="red-head"></div>
            </div>
            <div class="merchant-container stores">
                @include('front.merchant.partials.merchant-list', ['merchants' => $merchants])
            </div>

            <div id='loader' class='text-center' style="display: none;">
                <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
            </div>

            @if($merchants->nextPageUrl())
                <div class="btmbar d-flex justify-content-center">
                    <button class="btn btn-red" id="load-more-content" data-url="{{ $merchants->nextPageUrl() }}">@lang('front.button.load_more')</button>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#load-more-content').click(function () {
            $.ajax({
                method: 'GET',
                url: $('#load-more-content').data('url'),
                beforeSend: function () {
                    $('#load-more-content').hide();
                    $('#loader').show();
                },
                success: function (data) {
                    $('.merchant-container').append(data.html);
                    $('#loader').hide();

                    if (data.next_url != null) {
                        $('#load-more-content').data('url', data.next_url)
                        $('#load-more-content').show();
                    }
                }
            });
        });
        
        $('#page').change(function () {
            location.href = $(this).val();
        });
    </script>
@endsection