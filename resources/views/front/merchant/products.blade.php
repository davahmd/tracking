@extends('layouts.front_master')

@section('title', trans('front.section.distributors'))

@section('style')
    <style>
        .coverboard + .tab-container {
            background-color: transparent;
        }

        .coverboard + .tab-container .tab-content {
            padding: 0;
        }
    </style>
@endsection

@section('content')
    <div id="main">
        <div class="container">
            <div class="tab-container mr-top-md">
            {{--<ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-product">@lang('front.store.profile')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-review">@lang('common.rate_and_rev')</a>
                </li>
            </ul>--}}
            <!-- Tab panes -->
                <div class="tab-content">
                    <div id="tab-product" class="tab-pane active">
                        <div class="best-product-container mr-y-lg">
                            <div class="topbar d-flex flex-row-reverse">
                                <form class="form-inline">
                                    <select name="page" id="page" class="form-control">
                                        <option value="{{ url('merchants/distributors') }}">Distributors</option>
                                        <option value="{{ url('merchants/distributors/products') }}" @if(request()->is('*/products')) selected="selected" @endif>Products</option>
                                    </select>
                                </form>
                            </div>
                            <div class="topbar d-flex">
                                <h4>@lang('localize.Distributor_Products')</h4>
                                <div class="red-head"></div>
                            </div>
                            <div class="item-container">
                                @include('front.merchant.partials.product-list', ['products' => $products])
                            </div>
                            {{--<div class="more">--}}
                            {{--<a href="/store/{{ $store->url_slug }}/latest-products" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>--}}
                            {{--</div>--}}

                            <div id='loader' class='text-center' style="display: none;">
                                <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
                            </div>

                            @if($products->nextPageUrl())
                                <div class="btmbar d-flex justify-content-center">
                                    <button class="btn btn-red" id="load-more-content" data-url="{{ $products->nextPageUrl() }}">@lang('front.button.load_more')</button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#load-more-content').click(function () {
            $.ajax({
                method: 'GET',
                url: $('#load-more-content').data('url'),
                beforeSend: function () {
                    $('#load-more-content').hide();
                    $('#loader').show();
                },
                success: function (data) {
                    $('.item-container').append(data.html);
                    $('#loader').hide();

                    if (data.next_url != null) {
                        $('#load-more-content').data('url', data.next_url)
                        $('#load-more-content').show();
                    }
                }
            });
        });

        $('#page').change(function () {
            location.href = $(this).val();
        });
    </script>
@endsection