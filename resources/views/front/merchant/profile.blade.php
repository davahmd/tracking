@extends('layouts.front_master')

@section('title', $merchant->merchantName())

@section('content')
<div id="main">
    <div class="container">
        {{--<div class="banner-container mr-top-md">
            <!-- banner -->
        </div>--}}
        <div class="coverboard">
            <div class="merchant-detail full">
                <div class="d-flex">
                    <div class="merchant-logo-frame">
                        <img class="merchant-image" src="{{ $merchant->imageUrl }}" onerror="if (this.src != 'error.jpg') this.src = '/asset/images/product/default.jpg';"/>
                    </div>
                    <div class="merchant-main">
                        <h5 style="margin-bottom: 0;">{{ $merchant->merchantName() }}</h5>
                        {{-- <span class="card-rating">
                        @php $rating = $merchant->reviews->avg('rating'); @endphp
                        @foreach(range(1,5) as $i)
                            <span class="fa-stack" style="width:1em">
                                <i class="far fa-star fa-stack-1x"></i>
                                @if($rating >0)
                                    @if($rating >0.5)
                                        <i class="fa fa-star shine"></i>
                                    @else
                                        <i class="fa fa-star-half shine"></i>
                                    @endif
                                @endif
                                @php $rating--; @endphp
                            </span>
                        @endforeach
                        </span> --}}
                        {{--<p class="merchant-online">Online 20 hours ago</p>--}}
                    </div>
                </div>
                <br>
                @if (!empty($merchant->short_description))
                <div class="merchant-quote"><q>{{ $merchant->short_description }}</q></div>
                @endif
            </div>
            <div class="merchant-info">
                <div class="session session-activity">
                    <div class="inner">
                        <p class="session-title">@lang('localize.activity')</p>
                        {{-- <div class="session-item">
                            <p class="">Last Seen</p>
                            <p style="color:#858585;">An hour ago</p>
                        </div> --}}
                        <div class="session-item">
                            <p>@lang('common.since')</p>
                            <p style="color:#858585;">{{ date_format($merchant->created_at, 'd M Y') }}</p>
                        </div>
                        <div class="session-item text-right">
                            <p>@lang('common.operation_hour')</p>
                            <p style="color:#858585;">{!! nl2br($merchant->office_hour) !!}</p>
                        </div>
                        <div class="session-item">
                            <p>@lang('common.delivery_from')</p>
                            <p style="color:#858585;">{{ $merchant->state->name . ', ' . $merchant->country->co_name }}</p>
                        </div>
                    </div>
                </div>
                <div class="session session-address">
                    <div class="inner">
                        <p class="session-title">@lang('front.store.address')</p>
                        <div class="session-item">
                            <h6>{{ $merchant->merchantName() }}</h6>
                            <div class="d-flex" style="padding: 4px 10px;">
                                <span><i class="fa fa-map-marked-alt"></i></span>
                                <p>{{ $merchant->fullAddress() }}</p>
                            </div>
                            <div class="d-flex" style="padding: 4px 10px;">
                                <span><i class="fa fa-phone"></i></span>
                                <p>{{ $merchant->mer_phone }}</p>
                            </div>
                            <div class="d-flex" style="padding: 4px 10px;">
                                <span><i class="fa fa-link"></i></span>
                                <p><a href="{{ $merchant->stor_website }}" target="_blank">{{ !empty($merchant->stor_website)?$merchant->stor_website:'-' }}</a></p>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="session-item">
                        <p class="see-more"><a href="javascript:void(0)" data-toggle="modal" data-target="#branchModal">See Branches (12)</a></p>
                    </div>--}}

                    <!-- The Modal -->
                    <div class="modal fade" id="branchModal">
                        <div class="modal-dialog modal-md modal-dialog-centered">
                            <div class="modal-content branch-address-modal">
                                <div class="topbar d-flex flex-row">
                                    <h4 class="modal-title mr-auto">Other Branches</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="session-item mr-top-sm">
                                    <h6>Iceberg Window Films (Branch Jakarta)</h6>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">place</i>
                                        <p>Komplek Gamma, Jl. Alpha Beta Blok C3 no. 44 Leuwigajah, Cimahi Selatan, Kota Cimahi Jawa Barat 40533</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">call</i>
                                        <p>083822561496</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">email</i>
                                        <p>hello@icebergwindows.com</p>
                                    </div>
                                </div>
                                <div class="session-item">
                                    <h6>Iceberg Window Films (Branch Medan)</h6>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">place</i>
                                        <p>Komplek Gamma, Jl. Alpha Beta Blok C3 no. 44 Leuwigajah, Cimahi Selatan, Kota Cimahi Jawa Barat 40533</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">call</i>
                                        <p>083822561496</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">email</i>
                                        <p>hello@icebergwindows.com</p>
                                    </div>
                                </div>
                                <div class="session-item">
                                    <h6>Iceberg Window Films (Branch Surabaya)</h6>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">place</i>
                                        <p>Komplek Gamma, Jl. Alpha Beta Blok C3 no. 44 Leuwigajah, Cimahi Selatan, Kota Cimahi Jawa Barat 40533</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">call</i>
                                        <p>083822561496</p>
                                    </div>
                                    <div class="d-flex" style="padding: 4px 10px;">
                                        <i class="material-icons">email</i>
                                        <p>hello@icebergwindows.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="session session-courier">
                    <div class="inner">
                        <p class="session-title">Courier Support</p>
                        <div class="session-item">
                            <p class="">DHL</p>
                        </div>
                        <div class="session-item">
                            <p class="">JNE</p>
                        </div>
                        <div class="session-item">
                            <p class="">JNT</p>
                        </div>
                        <div class="session-item">
                            <p class="">FEDEX</p>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="session session-statistic">
                    <div class="inner">
                        <p class="session-title">@lang('common.statistic')</p>
                        <div class="session-item">
                            <p class="">Transaction Done</p>
                            <p style="color:#858585;">223,545</p>
                        </div>
                        <div class="session-item">
                            <p>Total Storefronts</p>
                            <p style="color:#858585;">12</p>
                        </div>
                        <div class="session-item">
                            <p>Total Products</p>
                            <p style="color:#858585;">245</p>
                        </div>
                        <div class="session-item">
                            <p>Average Rating</p>
                            <p style="color:#858585;">4.8/5</p>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="more">
                <a href="{{ route('merchant.product', $merchant->mer_id) }}" class="view-more">@lang('front.merchant.product')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
            </div>
        </div>
        <br>
    </div>
</div>
@endsection