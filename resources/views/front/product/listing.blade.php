@extends('layouts.front_master')

@section('title')
    @if(!empty($category))
        {{ $category->name_en }}
    @else
        trans('front.section.'.$type.'_products')
    @endif
@endsection

@section('content')
<div id="main" class="category-container">
    <div class="container">
        @if(!empty($category))
        <!-- <div class="banner-container mr-top-md"> -->
            {{--<img class="banner-container mr-top-md" src="{{ \Storage::url('category/banner/' . $category->banner) }}" onerror="this.onerror=null;this.src='/asset/images/banner/default.jpg';">--}}
        <!-- </div> -->
        <div class="pager-bar mr-top-md">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('front.nav.homepage')</a></li>
                @if(!$category->isMainCategory())
                <li class="breadcrumb-item"><a href="/category/{{ $category->parent->url_slug }}">{{ $category->parent->name_en }}</a></li>
                @endif
                <li class="breadcrumb-item active">{{ $category->name_en }}</li>
            </ul>
        </div>
        @if($category->children->count() > 0)
        <div class="subcategory-container mr-y-md">
            @if ($category->isMainCategory())
            <div class="topbar d-flex">
                <h4>@lang('front.section.sub_categories')</h4>
                <div class="red-head"></div>
            </div>
            @endif
            <div class="card-columns pd-y-sm">
                @if ($category->isMainCategory())
                    @foreach($category->children as $sub_category)
                    <div class="card">
                        <a href="/category/{{ $sub_category->url_slug }}">
                            <div class="thumb">
                                <img class="card-img-top" src="{{ \Storage::url('category/image/' .         $sub_category->image) }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                            </div>
                        <div class="card-body text-center">
                            <p class="card-text">{{ $sub_category->name_en }}</p>
                        </div>
                        </a>
                    </div>
                    @endforeach
                @else
                    @foreach($category->parent->children as $subcat)
                    <div class="card">
                        <a href="/category/{{ $subcat->url_slug }}/">
                        <img class="card-img-top" src="{{ \Storage::url('category/image/' . $subcat->image) }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                        <div class="card-body text-center">
                            <p class="card-text">{{$subcat->name_en}}</p>
                        </div>
                        </a>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        @endif

        {{--<div class="best-product-container mr-y-lg">
            <div class="topbar d-flex">
                <h4>@lang('front.section.most_reviewed')</h4>
                <div class="red-head"></div>
            </div>
            <div class="item-container">
                @foreach($category->products as $product)
                <div class="card">
                    <a href="/product/{{ Helper::slug_maker($product->pro_title_en, $product->pro_id) }}">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ \Storage::url('product/' . $product->pro_mr_id . '/' . $product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">{{ $product->pro_title_en }}</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP {{ $product->lowest_price }}</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                @break($loop->iteration == 6)
                @endforeach
            </div>
            <div class="more">
                <a href="javascript:void(0)" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
            </div>
        </div>--}}

        {{--<div class="promotion-product-container mr-y-lg">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('front.section.most_purchased')</h4>
                <div class="red-head"></div>
            </div>
            <div class="item-container">
                @foreach($category->products as $product)
                <div class="card">
                    <a href="/product/{{ Helper::slug_maker($product->pro_title_en, $product->pro_id) }}">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ \Storage::url('product/' . $product->pro_mr_id . '/' . $product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">{{ $product->pro_title_en }}</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP {{ $product->lowest_price }}</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                @break($loop->iteration == 6)
                @endforeach
            </div>
            <div class="more">
                <a href="javascript:void(0)" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
            </div>
        </div>--}}

        {{--<div class="latest-product-container mr-y-lg">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('front.section.most_favourited')</h4>
                <div class="red-head"></div>
            </div>
            <div class="item-container">
                @foreach($category->products as $product)
                <div class="card">
                    <a href="/product/{{ Helper::slug_maker($product->pro_title_en, $product->pro_id) }}">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ \Storage::url('product/' . $product->pro_mr_id . '/' . $product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">{{ $product->pro_title_en }}</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP {{ $product->lowest_price }}</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                @break($loop->iteration == 6)
                @endforeach
            </div>
            <div class="more">
                <a href="javascript:void(0)" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
            </div>
        </div>--}}
        @endif

        <div class="category-detail-container pd-y-md">
            <div class="filter-toggler">@lang('front.nav.cat.filter')</div>
            <div class="filter-container">
                <div class="selected-tag-box mr-btm-md">
                    <h6>@lang('front.nav.cat.tag')</h6>
                    <!-- <span class="tag">
                        <label class="label-tag">Car Brand: Audi</label>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </span>
                    <span class="tag">
                        <label class="label-tag">Tyre: Dunlop</label>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </span>
                    <span class="tag">
                        <label class="label-tag">Location: Klang</label>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </span> -->
                    <div class="form-inline">
                        <input class="form-control full-length" type="text" data-role="tagsinput" name="keyword" placeholder="{{ trans('common.keyword') }}" />
                        <!-- <i class="fas fa-arrow-right"></i> -->
                        <!-- <input class="form-control full-length puppet-input" type="text" placeholder="Add Keyword"> -->
                    </div>

                </div>

                @if (!empty($product_brands))
                <div class="filter-box mr-btm-md">
                    <h6>@lang('localize.product_brands')</h6>
                    @foreach($product_brands as $key => $brand)
                        @if ($key < 6)
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="{{ $brand->brand_name }}" name="official_brand[]" value="{{ $brand->brand_id }}" />
                            <label class="custom-control-label" for="{{ $brand->brand_name }}">&ensp;{{ $brand->brand_name }}</label>
                        </div>
                        @endif
                    @endforeach
                    <div class="collapse" id="collapseMoreProductBrands">
                        @foreach($product_brands as $key => $brand)
                            @if ($key > 5)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="{{ $brand->brand_name }}" name="official_brand[]" value="{{ $brand->brand_id }}" />
                                <label class="custom-control-label" for="{{ $brand->brand_name }}">&ensp;{{ $brand->brand_name }}</label>
                            </div>
                            @endif
                        @endforeach
                    </div>
                    @if (count($product_brands) > 5)
                        <a href="#collapseMoreProductBrands" class="view-more" data-toggle="collapse">@lang('localize.view_more')</a>
                    @endif
                </div>
                @endif

                @if (!empty($car_brands))
                <div class="filter-box mr-btm-md">
                    <h6>@lang('localize.car_brands')</h6>
                    @foreach($car_brands as $key => $car_brand)
                        @if ($key < 6)
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="{{ $car_brand }}" name="car_brand[]" value="{{ $car_brand }}" />
                            <label class="custom-control-label" for="{{ $car_brand }}">&ensp;{{ $car_brand }}</label>
                        </div>
                        @endif
                    @endforeach
                    <div class="collapse" id="collapseMoreCarBrands">
                        @foreach ($car_brands as $key => $car_brand)
                            @if ($key > 5)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="{{ $car_brand }}" name="car_brand[]" value="{{ $car_brand }}" />
                                <label class="custom-control-label" for="{{ $car_brand }}">&ensp;{{ $car_brand }}</label>
                            </div>
                            @endif
                        @endforeach
                    </div>
                    @if (count($car_brands) > 5)
                        <a href="#collapseMoreCarBrands" class="view-more" data-toggle="collapse">@lang('localize.view_more')</a>
                    @endif
                </div>
                @endif
                {{-- <div class="filter-box mr-btm-md">
                    <h6>Location</h6>
                    <div class="form-inline">
                        <input class="form-control full-length" type="text" placeholder="Location Keyword" />
                        <i class="fas fa-arrow-right"></i>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="carbrand4"/>
                        <label class="custom-control-label" for="carbrand4">Firestone</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="carbrand5"/>
                        <label class="custom-control-label" for="carbrand5">Goodyear</label>
                    </div>
                </div> --}}
                <div class="filter-box mr-btm-md">
                    <h6>@lang('localize.conditions')</h6>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="new" name="condition[]" value="1" />
                        <label class="custom-control-label" for="new">&ensp;@lang('localize.condition.1')</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="used" name="condition[]" value="2" />
                        <label class="custom-control-label" for="used">&ensp;@lang('localize.condition.2')</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="refurbished" name="condition[]" value="3" />
                        <label class="custom-control-label" for="refurbished">&ensp;@lang('localize.condition.3')</label>
                    </div>
                </div>
                <div class="filter-box mr-btm-md" style="text-transform: capitalize;">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="installation" name="installation" value="1" />
                        <label class="custom-control-label" for="installation">&ensp;@lang('localize.owns_a_workshop')</label>
                    </div>
                </div>
                <div class="filter-box mr-btm-md">
                    <h6>@lang('localize.price_range')</h6>
                    <div class="form-inline">
                        <input class="form-control half-length" name="min_price" type="number" min="0" placeholder="{{ trans('common.min') }}" />
                        <i>-</i>
                        <input class="form-control half-length" name="max_price" type="number" min="0" placeholder="{{ trans('common.max') }}" />
                    </div>
                </div>
                {{-- <div class="filter-box mr-btm-md">
                    <h6>Rating</h6>
                    <span class="card-rating">
                        <i class="fa fa-star shine"></i>
                        <i class="fa fa-star shine"></i>
                        <i class="fa fa-star shine"></i>
                        <i class="fa fa-star shine"></i>
                        <i class="fa fa-star"></i>
                    </span>
                    <span class="shine">(510)</span>
                </div> --}}
            </div>
            <div class="main-content-container">
                <div class="top-filter-box align-items-center mr-btm-sm">
                    <a href="javascript:void(0)" class="btn btn-red" data-toggle="modal" data-target="#carModal">@lang('localize.search_product_by_your_car')</a>
                    {{--<div class="tag">
                        <label class="your-car">Audi A7 Sportback 2.0 TFSI quattro 2017</label>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>--}}
                    <div class="form-inline ml-auto">
                        <form action="/products">
                            <label>@lang('front.nav.cat.sort_by')&nbsp;&nbsp;</label>
                            <!-- category id -->
                            <input type="hidden" name="category" value="{{$category->id}}">
                            <select class="form-control" id="sorting" name="sort">
                                <option value="latest" {{ ($data['sort'] == 'latest') ? 'selected':'' }}>@lang('front.nav.sort.latest_update')</option>
                                <option value="popularity" {{ ($data['sort'] == 'popularity') ? 'selected':'' }}>@lang('front.nav.sort.most_popular')</option>
                                <option value="price_low_to_high">@lang('front.nav.sort.price_low_hi')</option>
                                <option value="price_high_to_low">@lang('front.nav.sort.price_hi_low')</option>
                                {{--<option>Most Reviews</option>--}}
                                {{--<option>Top Recommended</option>--}}
                            </select>
                        </form>
                    </div>
                </div>

                <div id="recent-products" class="item-container side-listing">
                    @foreach ($products as $product)
                        <div class="card col-md-3">
                            <a href="/product/{{ $product->pro_slug }}">
                                <div class="card-thumb">
                                    @if (array_key_exists($product->pro_id, $promo_products) && ($promo_product_info[$product->pro_id]->limit > $promo_product_info[$product->pro_id]->count) && ($promo_products[$product->pro_id]->started_at <= $today_datetime && $promo_products[$product->pro_id]->ended_at >= $today_datetime))
                                        <span class="discount-rate bg-orange">{{ $promo_products[$product->pro_id]->discount_rate ? 100*($promo_products[$product->pro_id]->discount_rate) . '% OFF' : rpFormat($promo_products[$product->pro_id]->discount_value) . ' OFF' }}</span>
                                    @endif
                                    <img class="card-img-top" src="{{ \Storage::url('product/'.$product->pro_mr_id.'/'.$product->mainImage->image) }}" onerror="if (this.src != 'error.jpg') this.src = '/asset/images/product/default.jpg';" alt="Card image">
                                </div>
                                <div class="card-body">
                                    {{--<span class="discount-rate bg-orange">44% OFF</span>--}}
                                    @if ($product->pro_qty < 1)
                                    <span class="sold-out-tag">@lang('localize.outStock')</span>
                                    @endif
                                    <h6 class="card-title">{{ $product->title }}</h6>
                                    <div class="d-flex flex-column">
                                        {{-- <div class="price-layer">
                                            <span class="with_discounted" style="display: {{ ($product->pricing->first()->getPurchasePrice() <> $product->pricing->first()->price)? 'block' : 'none' }};">
                                                <h6 class="cut black">
                                                    <small>
                                                        <span class="discounted_price">{{ rpFormat($product->pricing->first()->price) }}</span>
                                                    </small>
                                                </h6>
                                                <h5 class="red">
                                                    <div>
                                                        <span class="discounted_purchase_price">{{ rpFormat($product->pricing->first()->getPurchasePrice()) }}</span>
                                                    </div>
                                                </h5>
                                            </span>
                                            <h5 class="red">
                                                <span class="without_discounted" style="display: {{ ($product->pricing->first()->getPurchasePrice() == $product->pricing->first()->price)? 'block' : 'none' }};">
                                                    <div>
                                                        <span class="discounted_purchase_price">{{ rpFormat($product->pricing->first()->getPurchasePrice()) }}</span>
                                                    </div>
                                                </span>
                                            </h5>
                                        </div> --}}
                                        <div class="price-layer">
                                            @if (array_key_exists($product->pro_id, $promo_products) && ($promo_product_info[$product->pro_id]->limit > $promo_product_info[$product->pro_id]->count) && ($promo_products[$product->pro_id]->started_at <= $today_datetime && $promo_products[$product->pro_id]->ended_at >= $today_datetime))
                                                <span class="with_discounted">
                                                    <h5 class="red">
                                                        <div>
                                                            <span class="discounted_purchase_price">{{ $promo_products[$product->pro_id]->discount_rate ? rpFormat($product->pricing->first()->price *(1-$promo_products[$product->pro_id]->discount_rate)) : rpFormat($product->pricing->first()->price - $promo_products[$product->pro_id]->discount_value) }}</span>
                                                        </div>
                                                    </h5>
                                                    <h6 class="cut black">
                                                        <small>
                                                            <span class="discounted_price">{{ rpFormat($product->pricing->first()->price) }}</span>
                                                        </small>
                                                    </h6>
                                                </span>
                                            @else
                                                <h5 class="red">
                                                    <span class="without_discounted">
                                                        <div>
                                                            <span class="discounted_purchase_price">{{ rpFormat($product->pricing->first()->price) }}</span>
                                                        </div>
                                                    </span>
                                                </h5>
                                            @endif
                                        </div>
                                        {{--<p class="card-text black cut"><small>RP 650.000</small></p>--}}
                                        {{--<p class="card-text red">{{ rpFormat($product->lowest_price) }}</p>--}}
                                        {{--<span class="card-rating">
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star shine"></i>
                                            <i class="fa fa-star"></i>
                                            <span class="shine" style="font-size: .6rem; font-weight: bold;">(510)</span>
                                        </span>--}}
                                    </div>
                                    {{-- @if (array_key_exists($product->pro_id, $promo_products) && ($promo_product_info[$product->pro_id]->limit > $promo_product_info[$product->pro_id]->count) && ($promo_products[$product->pro_id]->started_at <= $today_datetime && $promo_products[$product->pro_id]->ended_at >= $today_datetime))
                                        <p class="product-left-amount">
                                            <span class="red">{{$promo_product_info[$product->pro_id]->limit - $promo_product_info[$product->pro_id]->count }}</span> Barang Tersisa!
                                        </p>
                                    @endif --}}
                                </div>
                            </a>
                        </div>
                    @break($loop->iteration == 12)
                    @endforeach
                </div>

                <br>

                <div id='loader' class='text-center'>
                    <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
                </div>

                <div class="btmbar d-flex justify-content-center">
                    <a href="javascript:void(0)" id="load-more-recent" class="btn btn-red">@lang('front.button.load_more')</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="carModal">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content select-car-modal">

            <!-- From Favourite List -->
            <div class="fromFavourite mr-btm-md">
                <div class="topbar d-flex flex-row">
                    <h4 class="modal-title mr-auto">@lang('front.modal.category.search_fav_car')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="red-head"></div>
                </div>

                <div class="d-flex flex-column mr-y-md">
                    <div class="custom-select">
                        <select name="vehicle">
                            <option value="0">@lang('front.modal.category.select_car_from_fav')</option>
                            @foreach ($vehicles as $vehicle)
                            <option value="{{ $vehicle->id }}">{{ $vehicle->brand }} {{ $vehicle->model }} {{ $vehicle->variant }} {{ $vehicle->year }}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <div class="d-flex flex-row-reverse">
                        <a href="javascript:void(0)" class="btn btn-blue">@lang('front.button.done')</a>
                    </div>
                </div>
            </div>

            <!-- Create New -->
            {{--<div class="fromNew">
                <div class="topbar d-flex flex-row">
                    <h4 class="modal-title">search from new detail</h4>
                    <div class="red-head"></div>
                </div>

                <div class="mr-top-md">
                    <div class="form-group">
                        <label>Car Brand</label>
                        <div class="custom-select">
                            <select name="brand">
                                <option>Select Car Brand</option>
                                @foreach ($all_car_brands as $car_brand)
                                <option value="{{ $car_brand }}">{{ $car_brand }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Car Model</label>
                        <div class="custom-select">
                            <select name="model">
                                <option value="0">Select Car Model</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Car Variant</label>
                        <div class="custom-select">
                            <select name="variant">
                                <option value="0">Select Car Specification</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Car Year</label>
                        <div class="custom-select">
                            <select name="vehicle">
                                <option value="0">Select Car Year</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row-reverse">
                    <a href="javascript:void(0)" class="btn btn-blue">Done</a>
                </div>
            </div>--}}
        </div>
    </div>
</div>

<style>
.selected-tag-box .badge {
    /*margin-top: -35px;
    position: absolute;*/
}

.selected-tag-box .bootstrap-tagsinput {
    border: none;
    box-shadow: none;
}

.selected-tag-box .badge {
    margin-bottom: 5px;
    background-color: #fff;
    color: #495057;
    border: 1px solid #ced4da;
    border-radius: 1rem;
}

.selected-tag-box input {
    width: 80%;
    color: #495057;
    border: 1px solid #ced4da;
    border-radius: 1rem;
    padding: 8px 12px;
    height: 36px;
}

.selected-tag-box input:focus {
    width: 80%;
    color: #495057;
    border: 1px solid #ced4da;
    border-radius: 1rem;
    padding: .5rem, .75rem;
}

.view-more {
    font-size: 0.7rem;
}
</style>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('common/lib/js/tagsinput.js') }}"></script>
<script>
$(document).ready(function() {

    var category = $('input[name="category"]').val();
    var sort = $('select[name="sort"]').val();
    var minPrice = '';
    var maxPrice = '';

    function renderCardView(pro_id, pro_name,pro_price,pro_image,pro_slug, quantity, flash_price, flash_status, discount, remaining_item) {

        if (flash_status == true) {

            return "<div class='card'>" +
                "<a href='/product/" + pro_slug + "'>" +
                "<div class='card-thumb'>" +
                "<span class='discount-rate bg-orange'>" + discount + "</span>" +
                "<img class='card-img-top' src='" + pro_image + "' onerror='this.onerror=null;this.src=`/asset/images/product/default.jpg`;'></div>" +
                "<div class='card-body'>" +
                ((quantity == 0) ? "<span class='sold-out-tag'>@lang('localize.outStock')</span>" : "") +
                "<h6 class='card-title'>" + pro_name + "</h6>" +
                "<div class='d-flex flex-column'>" +
                "<div class='price-layer'>" +
                "<span class='with_discounted'>" +
                "<p class='card-text red'>" + flash_price + "</p>" +
                "<h6 class='cut black'><small><span class='discounted_price'>" + pro_price + "</span></small></h6>" +
                "</span>" +
                "</div>" +
                "</div>" +
                // "<p class='product-left-amount'>" +
                // "<span class='red'>" + remaining_item + "</span> Barang Tersisa!" +
                // "</p>" +
                "</div>" +
                "</a>" +
                "</div>";
        }

        return "<div class='card'>" +
            "<a href='/product/" + pro_slug + "'>" +
            "<div class='card-thumb'>" +
            "<img class='card-img-top' src='" + pro_image + "' onerror='this.onerror=null;this.src=`/asset/images/product/default.jpg`;'></div>" +
            "<div class='card-body'>" +
            ((quantity == 0) ? "<span class='sold-out-tag'>@lang('localize.outStock')</span>" : "") +
            "<h6 class='card-title'>" + pro_name + "</h6>" +
            "<div class='price-layer'>" +
            "<p class='card-text red'>" + pro_price + "</p>" +
            "</div>" +
            "</div>" +
            "</a>" +
            "</div>";
    }

    function loadRecentProducts(sort, min_price, max_price, car_brand, official_brand, keywords, favorite_vehicle, condition, installation, skip, take, type)
    {
        if (skip + take > 100) {
            take = 100 - skip;
        }

        $.get('/load_more_products', {category: category, sort: sort, min_price: min_price, max_price: max_price, car_brand: car_brand, official_brand: official_brand, keywords: keywords, favorite_vehicle: favorite_vehicle, condition: condition, installation: installation, skip: skip, take: take, type: type}, function(){

        }).done(function(products){

            $('#loader').hide();

            if (skip + take < 100) {
                $('#load-more-recent').show();
            }

            var productsArray = JSON.parse(products);

            if (productsArray.length < 12) {
                $('#load-more-recent').hide();
            }

            $.each(JSON.parse(products), function($key, product) {
                var s3_image = "{{ \Storage::url('product/') }}" + product.pro_mr_id + "/" + product.image;
                $('#recent-products').append(renderCardView(product.pro_id,product.pro_title,product.pro_price,s3_image,product.pro_slug, product.quantity, product.flash_price, product.flash_status, product.discount, product.remaining_item));
            });
        });

    }

    var skip = 0;

    function reloadProductView()
    {
        $('#recent-products .card').remove();
        $('#loader').show();
        $('#load-more-recent').hide();

        skip = 0;
    }

    $('#loader').hide();

    var carBrand = [];
    var officialBrand = [];

    $('#load-more-recent').click(function(){

        var sort = $('select[name="sort"]').val();
        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        $('#loader').show();
        $('#load-more-recent').hide();

        skip += 12;

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, skip, 12, null);

    });

    $('#sorting').change(function() {
        var sort = $(this).val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand ,keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    $('input[name="min_price"]').change(function() {
        var sort = $('#sorting').val();

        var minPrice = $(this).val();
        var maxPrice = $('input[name="max_price"]').val();

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    $('input[name="max_price"]').change(function() {
        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $(this).val();

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    $('input[name="car_brand[]"]').change(function() {

        if ($(this).is(':checked')) {
            carBrand.push($(this).val());
        } else {
            carBrand.splice( $.inArray($(this).val(), carBrand), 1);
        }

        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    $('input[name="official_brand[]"]').change(function() {
        if ($(this).is(':checked')) {
            officialBrand.push($(this).val());
        }
        else {
            officialBrand.splice( $.inArray($(this).val(), officialBrand), 1);
        }

        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    var favoriteVehicle = [];

    $('#carModal .fromFavourite a').click(function() {

        var vehicleId = $('#carModal .fromFavourite select[name="vehicle"]').val();
        favoriteVehicle.push(vehicleId);

        var vehicleFullModel = $('#carModal .fromFavourite select[name="vehicle"] :selected').text();

        $('.main-content-container .top-filter-box a').after('<div class="tag"><label class="your-car">'+vehicleFullModel+'</label><button type="button" class="close" data-dismiss="modal">&times;</button></div>')

        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        $('#carModal').modal('hide');

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    $('.main-content-container').on('click', '.top-filter-box .close', function() {

        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        favoriteVehicle = [];

        $(this).parent().remove();

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    var keywords = [];

    $('input[name="keyword"]').change(function() {

        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        keywords = $(this).tagsinput('items');

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    var condition = [];

    $('input[name="condition[]"]').change(function() {
        if ($(this).is(':checked')) {
            condition.push($(this).val());
        }
        else {
            condition.splice( $.inArray($(this).val(), condition), 1);
        }

        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        // keywords = $(this).tagsinput('items');

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    var installation = 0;

    $('input[name="installation"]').change(function() {
        if ($(this).is(':checked')) {
            installation = 1;
        }
        else {
            installation = 0;
        }

        var sort = $('#sorting').val();

        var minPrice = $('input[name="min_price"]').val();
        var maxPrice = $('input[name="max_price"]').val();

        reloadProductView();

        loadRecentProducts(sort, minPrice, maxPrice, carBrand, officialBrand, keywords, favoriteVehicle, condition, installation, 0, 12, null);
    });

    $('a[href="#collapseMoreCarBrands"]').click(function() {
        $('#collapseMoreCarBrands').on('hidden.bs.collapse', function () {
          $('a[href="#collapseMoreCarBrands"]').text('@lang("localize.view_more")');
        });

        $('#collapseMoreCarBrands').on('shown.bs.collapse', function () {
          $('a[href="#collapseMoreCarBrands"]').text('@lang("localize.view_less")');
        });
    });

    $('a[href="#collapseMoreProductBrands"]').click(function() {
        $('#collapseMoreProductBrands').on('hidden.bs.collapse', function () {
          $('a[href="#collapseMoreProductBrands"]').text('@lang("localize.view_more")');
        });

        $('#collapseMoreProductBrands').on('shown.bs.collapse', function () {
          $('a[href="#collapseMoreProductBrands"]').text('@lang("localize.view_less")');
        });
    });

    // Auto generate brands, models, variants, years
    // $('select[name="brand"]').change(function(e) {
    //     $('select[name="model"]').empty();
    //     $('select[name="variant"]').empty();
    //     $('select[name="vehicle"]').empty();

    //     console.log(123123);

    //     $.ajax({
    //         type: 'GET',
    //         url: '/get_model/'+$(this).val(),
    //         success: function(data) {
    //             $('select[name="model"]').append('<option disabled selected>{{trans("localize.select_model")}}</option>');
    //             for (var i = 0; i < data.length; i++) {
    //                 $('select[name="model"]').append('<option>'+data[i]+'</option>');
    //             }
    //         }
    //     })
    // });

    // $('select[name="model"]').change(function(e) {
    //     $('select[name="variant"]').empty();
    //     $('select[name="vehicle"]').empty();

    //     var brand = $('select[name="brand"]').val();

    //     $.ajax({
    //         type: 'GET',
    //         url: '/get_variant/'+ brand +'/'+$(this).val(),
    //         success: function(data) {
    //             $('select[name="variant"]').append('<option disabled selected>{{trans("localize.select_variant")}}</option>');
    //             for (var i = 0; i < data.length; i++) {
    //                 $('select[name="variant"]').append('<option>'+data[i]+'</option>');
    //             }
    //         }
    //     })
    // });

    // $('select[name="variant"]').change(function(e) {
    //     $('select[name="vehicle"]').empty();

    //     var model = $('select[name="model"]').val();

    //     $.ajax({
    //         type: 'GET',
    //         url: '/get_year/'+ model +'/'+$(this).val(),
    //         success: function(data) {
    //             $('select[name="vehicle"]').append('<option disabled selected>{{trans("localize.select_year")}}</option>');
    //             for (var i = 0; i < data.length; i++) {
    //                 $('select[name="vehicle"]').append('<option value="'+data[i].id+'">'+data[i].year+'</option>');
    //             }
    //         }
    //     })
    // });
});
</script>
@endsection