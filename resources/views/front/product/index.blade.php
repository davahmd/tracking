@extends('layouts.front_master')

@section('header')
    @include('layouts.web.header.main')
@endsection

@section('content')
<div id="main" class="category-container">
    <div class="container">
        <div class="banner-container mr-top-md">
            <!-- banner -->
        </div>
        <div class="pager-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Category</a></li>
                <li class="breadcrumb-item active">Automotive</li>
            </ul>
        </div>
        <div class="subcategory-container mr-y-md">
            <div class="topbar d-flex">
                <h4>Sub Categories</h4>
                <div class="red-head"></div>
            </div>
            <div class="card-columns pd-y-sm">
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/1.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Auto Parts &amp; Spares</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/2.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Auto Tools &amp; Equipment</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/3.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Batteries &amp; Accessories</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/4.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Steering Wheel &amp; Accessories</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/5.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Truck Part &amp; Accessories</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/1.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Auto Parts &amp; Spares</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/2.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Auto Tools &amp; Equipment</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/3.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Batteries &amp; Accessories</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/4.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Steering Wheel &amp; Accessories</p>
                    </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)">
                    <img class="card-img-top" src="{{ asset('asset/images/product/5.png') }}" alt="Card image">
                    <div class="card-body text-center">
                        <p class="card-text">Truck Part &amp; Accessories</p>
                    </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="best-product-container mr-y-lg">
            <div class="topbar d-flex">
                <h4>Most Reviewed</h4>
                <div class="red-head"></div>
            </div>
            <div class="item-container">
        
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/18.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/19.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/20.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/21.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/22.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/19.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>
            <div class="more">
                <a href="javascript:void(0)" class="view-more">See More <i class="material-icons">arrow_forward</i></a>
            </div>
        </div>
        <div class="promotion-product-container mr-y-lg">
            <div class="topbar d-flex">
                <h4 class="mr-auto">Most Purchased</h4>
                <div class="red-head"></div>
            </div>
            <div class="item-container">
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/18.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/19.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/20.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/21.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/22.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/19.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>
            <div class="more">
                <a href="javascript:void(0)" class="view-more">See More <i class="material-icons">arrow_forward</i></a>
            </div>

        </div>
        <div class="latest-product-container mr-y-lg">
            <div class="topbar d-flex">
                <h4 class="mr-auto">Most Favourite</h4>
                <div class="red-head"></div>
            </div>
            <div class="item-container">
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/18.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/19.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/20.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/21.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/22.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="product.html">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ asset('asset/images/product/19.png') }}">
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6>
                            <div class="d-flex flex-column">
                                <p class="card-text red">RP 510.000</p>
                                <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>
            <div class="more">
                <a href="javascript:void(0)" class="view-more">See More <i class="material-icons">arrow_forward</i></a>
            </div>
        </div>
    </div>
</div>
@endsection