<!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="country_state_lib_version" content="{{ Cache::get('country_state_lib_version_number') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> {{ config('app.name') }} - @yield('title')</title>

    <link rel="icon" type="image/png" href="{{ asset('asset/images/favicon.png') }}">

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-reboot.css') }}">

    {{-- Fontawesome --}}
    <link rel="stylesheet" href="{{ asset('asset/css/fontawesome/all.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('asset/css/default.css?v2') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/master.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/dropdown_bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/dashboard.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/custom_select.css') }}">

    <link href="{{ asset('common/lib/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/bootstrap-fileinput.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/tagsinput.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/simplegallery.demo1.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/sweetalert2.css') }}" />
    <style type="text/css">
        #attributes .selection-label {
            color: #ccc;
        }

        input[type=number] {
            -moz-appearance:textfield;
        }
    </style>
    </head>

    <body>
        <div id="header" class="bg-zona">
            <div class="container">
                <div class="zona-logo">
                    <a href="javascript:void(0)">
                        <img src="{{ asset('asset/images/logo/logo.png') }}" />
                    </a>
                </div>
                <button class="navbar-toggler">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div id="navb">
                    <div class="menu-title"></div>
                    <ul class="navbar-primary">
                        <li class="dropdown category-expand">
                            <a class="dropdown-toggle" href="javascript:void(0)" >@lang('front.nav.category')</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">@lang('front.nav.official_brand')</a>
                        </li>
                    </ul>
                </div>
                <div class="nav-others">
                    <form class="form-inline">
                        <span class="material-icons close-search">close</span>
                        <input class="form-control" type="text" placeholder="{{ trans('front.nav.search_in_zona') }}">
                        <button type="submit"><img src="{{ asset('asset/images/icon/icon_search.png') }}" /></button>
                    </form>

                    <ul class="navbar-secondary">
                        <li class="search-toggler">
                            <a href="javascript:void(0)">
                                <img src="{{ asset('asset/images/icon/icon_search.png') }}">
                            </a>
                        </li>
                        <li class="dropdown language-dropdown">
                            <a href="javascript:void(0)" data-toggle="dropdown">
                                <img src="{{ asset('asset/images/icon/icon_globe.png') }}">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0)">@lang('common.english')</a>
                                <a class="dropdown-item" href="javascript:void(0)">@lang('common.indonesian')</a>
                            </div>
                        </li>
                        @if(Auth::check())
                        <li>
                            <a href="javascript:void(0)">
                                <img src="{{ asset('asset/images/icon/icon_cart.png') }}">
                            </a>
                        </li>
                        <li id="notifications">
                            <a href="javascript:void(0)">
                                <img src="{{ asset('asset/images/icon/icon_notification.png') }}">
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" id="account-dropdown" data-toggle="dropdown">
                                <img src="{{ asset('asset/images/icon/icon_account.png') }}">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0)">My Account</a>
                                <a class="dropdown-item" href="javascript:void(0)">Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item logout" href="javascript:void(0)">Log Out</a>
                            </div>
                        </li>
                        @endif
                    </ul>

                    @if(!Auth::check())
                    <ul class="navbar-auth">
                        <li>
                            <a class="btn" href="javascript:void(0)">@lang('front.button.login')</a>
                        </li>
                        <li>
                            <a class="btn" href="javascript:void(0)">@lang('front.button.register')</a>
                        </li>
                    </ul>
                    @endif
                </div>
            </div>
        </div>

        <div id="main">
            <div class="container pd-btm-lg">
                <br>
                @include('layouts.partials.status')
                <div class="main-overview-container d-flex mr-y-md">
                    <div class="product-image-container">
                        <div class="thumbnail-gallery">
                            <section id="gallery" class="simplegallery">
                                <div class="content">
                                    <img src="{{ \Storage::url('product/' . $product['details']['pro_mr_id'] . '/' . $product['main_image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';" class="image_1" alt="">
                                    @foreach($product['images'] as $image)
                                        @continue($product['main_image'] == $image['image'])
                                        <img src="{{ \Storage::url('product/' . $product['details']['pro_mr_id'] . '/' . $image['image']) }}" style="display:none" class="image_{{ $loop->iteration }}" alt="">
                                    @endforeach
                                </div>
                                <div class="clear"></div>
                                <div class="thumbnail d-flex justify-content-between">
                                    @foreach($product['images'] as $image)
                                    <div class="thumb">
                                        <a href="javascript:void(0)" rel="{{ $loop->iteration }}">
                                            <img src="{{ \Storage::url('product/' . $product['details']['pro_mr_id'] . '/' . $image['image']) }}" id="thumb_{{ $loop->iteration }}" alt="">
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="product-overview-container bg-white">
                        <div class="d-flex ">
                            <h4 class="product-title">{{ $product['details']->title }}</h4>
                            <div class="ml-auto">
                                <a class="favourite black" href="javascript:void(0)"><i class="far fa-heart"></i></a>
                                <a class="share black" href="javascript:void(0)"><i class="fas fa-share-square"></i></a>
                            </div>
                        </div>
                        <hr>
                        @if ($product['details']->short_desc)
                        <section class="short-desc">
                            <p class="info mr-top-sm">{{ $product['details']->short_desc }}</p>
                        </section>
                        <hr>
                        @endif
                        <form action="/cart/add" method="post" enctype="multipart/form-data" id="detailcart" class="form-vertical">
                            {{ csrf_field() }}
                            @if(count($attribute_listing['parent']) > 0)
                                <div id="attributes">
                                    @foreach($attribute_listing['parent'] as $parent_id => $attribute)
                                        <div class="form-group">
                                            <label class="selection-label">{{ $attribute['name'] }}</label>
                                            <div id="parent_attribute_{{ $parent_id }}">
                                                @if($parent_id == 0)
                                                    @foreach($attribute['items'] as $key => $name)
                                                        <label class="radio radio-inline attribute-radio">
                                                            <input type="radio" name="attribute_selection[{{ $parent_id }}]" id="attribute_{{ $key }}" value="{{ $parent_id }}:{{ $key }}" class="attribute_selection">
                                                            <span class="checkmark">{{ $name }}</span>
                                                        </label>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <input type="hidden" id="selected_attribute" name="selected_attribute" value="{{ json_encode($product['attributes']) }}">
                                <hr />
                            @endif
                            @if(count($store_service_extra)>0)
                            <div id="attributes" class="service-section">
                                <label class="selection-label">Available Service(s)</label>
                                @foreach($store_service_extra as  $service)
                                <label class="form-check">
                                    <input type="checkbox" class="form-check-input" id="{{$service->id}}" name='service_request[]' value='{{$service->id}}'>
                                    <div class="form-check-label" for="{{$service->id}}">{{$service->service_name}} </div>
                                </label>
                                @endforeach
                            </div>
                            @endif
                            <div class="price-layer">
                                <span class="with_discounted" style="display: {{ ($product['pricing']->purchase_price <> $product['pricing']->price)? 'block' : 'none' }};">
                                    <h6 class="cut black">
                                        <small>
                                            <span class="discounted_price">{{ rpFormat($product['pricing']->price) }}</span>
                                        </small>
                                    </h6>
                                    <h5 class="red">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($product['pricing']->purchase_price) }}</span>
                                        </div>
                                    </h5>
                                </span>
                                {{-- <small>Rp 610.000</small> --}}
                                <h5 class="red">
                                    <span class="without_discounted" style="display: {{ ($product['pricing']->purchase_price == $product['pricing']->price)? 'block' : 'none' }};">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($product['pricing']->purchase_price) }}</span>
                                        </div>
                                    </span>
                                </h5>
                            </div>
                            <div class="quantity-setting mr-top-md">
                                <button type="button" class="decrease">-</button>
                                <input id="qty" type="number" name="qty" value="1" min="1" readonly="">
                                <button type="button" class="increase">+</button>
                            </div>
                            <div class="button-layer mr-top-md">
                                <!-- sold out-->
                                <div id="outStock" class="sold-out-tag" style="display: {{ ($product['pricing']->quantity > 0) ? 'none' : '' }}">@lang('localize.outStock')</div>
                                <button id="btn-add-cart" style="display: {{ ($product['pricing']->quantity > 0) ? '' : 'none' }}" type="button" class="btn btn-cart">@lang('front.button.add_cart')</button>
                                {{-- <a href="javascript:void(0)" class="btn btn-chat">Chat</a> --}}
                                {{-- <a href="javascript:void(0)" class="btn btn-nego">Negotiate</a> --}}
                            </div>
                            <input type="hidden" id="max_quantity" value="{{ $product['pricing']->quantity }}">
                            <input type="hidden" name="product_id" value="{{$product['details']->pro_id}}" />
                            <input type="hidden" name="merchant_id" value="{{$product['details']->pro_mr_id}}" />
                            <input type="hidden" name="price_id" id="price_id" value="{{$product['pricing']->id}}" />
                        </form>
                    </div>
                </div>
                <div class="merchant-container product">
                    <div class="merchant-logo-frame">
                        <img class="merchant-image" src="{{ $store->getImageUrl() }}" onerror="if (this.src != 'error.jpg') this.src = '/asset/images/product/default.jpg';" alt="Merchant image"/>
                    </div>
                    <a class="merchant-name pd-x-sm">{{ $store->stor_name }}</a>
                    <a href="javascript:void(0)" class="btn btn-visit ml-auto">@lang('localize.mer_store_information')</a>
                </div>
                <div class="product-detail-container mr-y-lg">
                    <div class="topbar d-flex">
                        <h4 class="mr-auto">@lang('front.section.product_details')</h4>
                        <div class="red-head"></div>
                    </div>
                    <div class="description-container bg-white mr-top-sm pd-lg">
                        <div class="display-part">
                            {!! $product['details']->desc !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="footer" class="bg-dark">
            <div class="container">
                <div class="footer-container pd-y-lg pd-x-sm white">
                    <div class="subscribe-container mr-auto">
                        <h6>@lang('footer.subscribe_via_email')</h6>
                        <p>@lang('footer.subscribe_text')</p>
                        <form class="form-inline">
                            <input class="form-control mr-sm-2" type="text" placeholder="{{ trans('localize.email') }}">
                            <a class="btn btn-red" href="javascript:void(0)">@lang('footer.subscribe')</a>
                        </form>
                    </div>
                    <div class="contact-container">
                        <p class="ph_no">021 5761605</p>
                        <a href="javascript:void(0)"><p class="email">info@zona.id</p></a>
                        <p class="location">Kl Mega Kuningan Lot 5.1 Menara Rajawali Lt 11, Kuningan Timur</p>
                    </div>
                    <div class="sitemap-1">
                        <a href="javascript:void(0)"><p>@lang('footer.contactUs')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.payment')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.shippingDelivery')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.returns')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.faq')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.guide')</p></a>
                    </div>
                    <div class="sitemap-2">
                        <a href="javascript:void(0)"><p>@lang('footer.aboutUs')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.career')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.privacyPolicy')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.terms')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.conference')</p></a>
                    </div>
                </div>
            </div>
            <div class="footer-container-2 bg-red pd-x-sm white">
                <div class="container">
                    <div class="copy-container mr-auto">
                        @lang('footer.copyright')
                    </div>
                    <div class="social-container">
                        <ul>
                            <li>
                                <a href="javascript:void(0)"><i class="fab fa-facebook-square"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fab fa-twitter-square"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fab fa-google-plus-square"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="{{ asset('asset/js/jquery-3.3.1.js') }}"></script>

        <script type="text/javascript" src="{{ asset('asset/js/popper.js') }}"></script>

        <script type="text/javascript" src="{{ asset('asset/js/bootstrap/bootstrap.js') }}"></script>
        <!--script type="text/javascript" src="{{ asset('asset/js/bootstrap/bootstrap.bundle.js') }}"></script-->

        <script type="text/javascript" src="{{ asset('asset/js/fontawesome/all.js') }}"></script>

        <script type="text/javascript" src="{{ asset('asset/js/simplegallery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('asset/js/sweetalert2.js') }}"></script>

        <script src="{{ asset('asset/js/dropdown_bs4.js') }}"></script>
        <script src="{{ asset('asset/js/custom_select.js') }}"></script>
        <script src="{{ asset('asset/js/zona.app.js') }}"></script>

        <script type="text/javascript" src="{{ asset('backend/js/plugins/iCheck/icheck.min.js') }}"></script>

        <!-- <script type="text/javascript" src="{{ asset('common/js/country_state_f2.js') }}"></script> -->
        <script type="text/javascript" src="{{ asset('common/js/common_function.js') }}"></script>

        <script type="text/javascript" src="{{ asset('common/lib/js/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('common/lib/js/bootstrap-fileinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('common/lib/js/sweetalert.min.js') }}"></script>
    </body>
</html>