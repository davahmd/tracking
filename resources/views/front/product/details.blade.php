
@extends('layouts.front_master')

@section('title', $product['details']['pro_title_en'])

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/blueimp/css/blueimp-gallery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('asset/css/simplegallery.demo1.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('asset/css/sweetalert2.css') }}" />

<style type="text/css">
    #attributes .selection-label {
        color: #ccc;
    }

    input[type=number] {
        -moz-appearance:textfield;
    }
</style>
@endsection

@section('content')
@php
$compiledRatings = $product['details']->compiledRatings();
@endphp
<div id="main">
    <div class="container pd-btm-lg">
        <br>
        @include('layouts.partials.status')
        {{-- <div class="pager-bar mr-top-md">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @if($product->categories->count() < 1)
                <li class="breadcrumb-item">
                    <a href="category.html">{{ $product->categories->first()->parent->name_en }}</a>
                </li>
                @endif
                <li class="breadcrumb-item"><a href="category_detail.html">{{ $product->categories->first()->name_en }}</a></li>
                <li class="breadcrumb-item active">{{ $product->pro_title_en }}</li>
            </ul>
        </div> --}}
        <div class="main-overview-container d-flex mr-y-md">
            <div class="product-image-container">
                <div class="thumbnail-gallery">
                    <section id="gallery" class="simplegallery">
                        <div class="content">
                            <img src="{{ \Storage::url('product/' . $product['details']['pro_mr_id'] . '/' . $product['main_image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';" class="image_1" alt="">
                            @foreach($product['images'] as $image)
                                @continue($product['main_image'] == $image['image'])
                                <img src="{{ \Storage::url('product/' . $product['details']['pro_mr_id'] . '/' . $image['image']) }}" style="display:none" class="image_{{ $loop->iteration }}" alt="">
                            @endforeach
                        </div>
                        <div class="clear"></div>
                        <div class="thumbnail">
                            @foreach($product['images'] as $image)
                            <div class="thumb">
                                <a href="javascript:void(0)" rel="{{ $loop->iteration }}">
                                    <img src="{{ \Storage::url('product/' . $product['details']['pro_mr_id'] . '/' . $image['image']) }}" id="thumb_{{ $loop->iteration }}" alt="">
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
            <div class="product-overview-container bg-white">
                @if(request()->routeIs('product-wholesale'))
                <div class="d-flex ">
                    <h4 class="product-title">{{ $product['details']->title }}</h4>
                    <div class="ml-auto">
                        {{--<a class="favourite black" href="account/favourite.html"><i class="far fa-heart"></i></a>--}}
                        {{-- <a class="share black" href="javascript:void(0)"><i class="fas fa-share-square"></i></a> --}}
                    </div>
                </div>
                    @include('front.partial.star.tag', ['rating' => $compiledRatings])
                @elseif (($promo_product != null && $promo_product_info != null) && ($promo_product_info->limit > $promo_product_info->count) && ($promo_product->started_at <= $today_datetime && $promo_product->ended_at >= $today_datetime))
                <div>
                    <h4 class="product-title">{{ $product['details']->title }}</h4>
                    <div class="product-flash-sale-container">
                        <div class="flash-sale-tag">
                            <img src="/asset/images/icon/icon_flash_sale.png" class="icon-flashsale">
                            <h4>Flash Sale</h4>
                        </div>
                        <div class="time">
                            <input type="hidden" id="ended_time" name="ended_time" value="{{ $end }}">
                            Time Remaining : <div class="time" id="countdown_timer"></div>
                        </div>
                    </div>
                    <p>
                        @include('front.partial.star.tag', ['rating' => $compiledRatings])
                    </p>
                </div>
                @else
                <div class="d-flex ">
                    <h4 class="product-title">{{ $product['details']->title }}</h4>
                    <div class="ml-auto">
                        {{--<a class="favourite black" href="account/favourite.html"><i class="far fa-heart"></i></a>--}}
                        {{-- <a class="share black" href="javascript:void(0)"><i class="fas fa-share-square"></i></a> --}}
                        <a href="javascript:void(0)" data-toggle="dropdown">
                            <img src="{{ asset('asset/images/icon/icon_share.png') }}">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}"><i class="fab fa-facebook-f"></i> @lang('localize.fb')</a>
                            <a class="dropdown-item" href="https://twitter.com/intent/tweet?text=({{ $product['details']->title }})&url={{ url()->current() }}"><i class="fab fa-twitter"></i> @lang('localize.twit')</a>
                            <a class="dropdown-item" href="https://wa.me/?text={{ url()->current() }}"><i class="fab fa-whatsapp"></i> @lang('localize.whatsapp')</a>
                        </div>
                    </div>
                </div>
                    @include('front.partial.star.tag', ['rating' => $compiledRatings])
                @endif

                <hr>

                @if ($product['details']->short_desc)
                <section class="short-desc">
                    <p class="info mr-top-sm">{{ $product['details']->short_desc }}</p>
                </section>
                <hr>
                @endif
                <form action="/cart/add" method="post" enctype="multipart/form-data" id="detailcart" class="form-vertical">
                    {{ csrf_field() }}
                    @if(count($attribute_listing['parent']) > 0)
                        <div id="attributes">
                            @foreach($attribute_listing['parent'] as $parent_id => $attribute)
                                <div class="form-group">
                                    <label class="selection-label">{{ $attribute['name'] }}</label>
                                    <div id="parent_attribute_{{ $parent_id }}">
                                        @if($parent_id == 0)
                                            @foreach($attribute['items'] as $key => $name)
                                                <label class="radio radio-inline attribute-radio">
                                                    <input type="radio" name="attribute_selection[{{ $parent_id }}]" id="attribute_{{ $key }}" value="{{ $parent_id }}:{{ $key }}" class="attribute_selection">
                                                    <span class="checkmark" id="attribute-item-{{ $key }}">{{ $name }}</span>
                                                </label>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <input type="hidden" id="selected_attribute" name="selected_attribute" value="{{ json_encode($product['attributes']) }}">
                        <hr />
                    @endif
                    @if(request()->routeIs('product') && count($store_service_extra)>0)
                    <div id="attributes" class="service-section">
                        <label class="selection-label">{{trans('localize.available_service')}}</label>
                        @foreach($store_service_extra as $service)
                        <label class="form-check">
                            <input type="checkbox" class="form-check-input" id="{{$service->id}}" name='service_request[]' value='{{$service->id}}'>
                            <div class="form-check-label" for="{{$service->id}}">{{$service->service_name}} </div>
                        </label>
                        @endforeach
                    </div>
                    @endif
                    {{-- <div class="price-layer">
                        <span class="with_discounted" style="display: {{ ($product['pricing']->purchase_price <> $product['pricing']->price)? 'block' : 'none' }};">
                            <h6 class="cut black">
                                <small>
                                    <span class="discounted_price">{{ rpFormat($product['pricing']->price) }}</span>
                                </small>
                            </h6>
                            <h5 class="red">
                                <div>
                                    <span class="discounted_purchase_price">{{ rpFormat($product['pricing']->purchase_price) }}</span>
                                </div>
                            </h5>
                        </span>
                        <h5 class="red">
                            <span class="without_discounted" style="display: {{ ($product['pricing']->purchase_price == $product['pricing']->price)? 'block' : 'none' }};">
                                <div>
                                    <span class="discounted_purchase_price">{{ rpFormat($product['pricing']->purchase_price) }}</span>
                                </div>
                            </span>
                        </h5>
                    </div> --}}
                    <div class="price-layer">
                        @if(request()->routeIs('product-wholesale'))
                            <span class="with_discounted">
                                <h5 class="red">
                                    <div>
                                        @if(auth()->user()->isRetailerForDistributor($product['details']->merchant))
                                            <span class="discounted_purchase_price">{{ rpFormat($product['pricing']->wholesale_price) }}</span>
                                        @else
                                            <span class="discounted_purchase_price">{{ config('app.currency_code') }} ???</span>
                                        @endif
                                    </div>
                                </h5>
                                <h6 class="cut black">
                                    <small>
                                        <span class="discounted_price">{{ rpFormat($product['pricing']->price) }}</span>
                                    </small>
                                </h6>
                            </span>
                        @elseif (($promo_product != null && $promo_product_info != null) && ($promo_product_info->limit > $promo_product_info->count) && ($promo_product->started_at <= $today_datetime && $promo_product->ended_at >= $today_datetime))
                            <span class="with_discounted">
                                <h5 class="red">
                                    <div>
                                        <span class="discounted_purchase_price">{{ $promo_product->discount_rate ? rpFormat($product['pricing']->price *(1-$promo_product->discount_rate)) : rpFormat($product['pricing']->price - $promo_product->discount_value) }}</span>
                                    </div>
                                </h5>
                                <h6 class="cut black">
                                    <small>
                                        <span class="discounted_price">{{ rpFormat($product['pricing']->price) }}</span>
                                    </small>
                                </h6>
                            </span>
                        @elseif (Auth::check())
                        <h5 class="red">
                            <span class="without_discounted">
                                <div>
                                    <span class="discounted_purchase_price">{{ rpFormat($product['pricing']->price) }}</span>
                                </div>
                            </span>
                        </h5>
                        @else
                        @endif
                    </div>
                    @if(request()->routeIs('product-wholesale'))
                    <p class="product-left-amount">
                        <span class="red">
                            {{ $product['details']->pro_qty }}
                        </span> @lang('localize.item_left')
                    </p>
                    @elseif ($promo_product_info)
                    <p class="product-left-amount">
                        <span class="red">
                            @if ($product['details']->pro_qty > ($promo_product_info->limit - $promo_product_info->count))
                                {{ $promo_product_info->limit - $promo_product_info->count }}
                            @else
                                {{ $product['details']->pro_qty }}
                            @endif
                        </span> @lang('localize.item_left')
                    </p>
                    @endif
                    <div class="quantity-setting mr-top-md">
                        <button type="button" class="decrease">-</button>
                        @if(request()->routeIs('product-wholesale'))
                        <input id="qty" type="number" name="qty" value="{{ $product['pricing']->min_quantity }}" min="{{ $product['pricing']->min_quantity }}" readonly="">
                        @else
                        <input id="qty" type="number" name="qty" value="1" min="1" readonly="">
                        @endif
                        <button type="button" class="increase">+</button>
                    </div>

                    <div class="button-layer mr-top-md">
                        @if(request()->routeIs('product-wholesale'))
                            @if(auth()->user()->isRetailerForDistributor($product['details']->merchant))
                                <div id="outStock" class="sold-out-tag" style="display: {{ ($product['pricing']->quantity > 0) ? 'none' : '' }}">@lang('localize.outStock')</div>
                                <button id="btn-add-cart" style="display: {{ ($product['pricing']->quantity > 0) ? '' : 'none' }}" type="{{ auth()->check()?'submit':'button' }}" class="btn btn-cart">@lang('front.button.add_cart')</button> --}}
                                <button id="btn-nego" type="button" class="btn btn-nego" data-toggle="modal" data-target="#PriceNego" style="{{ ($product['pricing']->negotiable == false)?'display:none':'' }}">@lang('localize.negotiate')</button>
                            @else
                                <div class="alert alert-warning">
                                    Only retailer buy with wholesale price.
                                </div>
                            @endif
                        @else
                        <!-- sold out-->
                        <div id="outStock" class="sold-out-tag" style="display: {{ ($product['pricing']->quantity > 0) ? 'none' : '' }}">@lang('localize.outStock')</div>
                        <button id="btn-add-cart" style="display: {{ ($product['pricing']->quantity > 0) ? '' : 'none' }}" type="{{ auth()->check()?'submit':'button' }}" class="btn btn-cart">@lang('front.button.add_cart')</button>
                            @if($active_nego)
                                <a href="{{ route('nego-detail', $active_nego) }}" id="btn-ge-detail" class="btn btn-nego" style="width: auto;">@lang('localize.view') @lang('merchant.nav.negotiation')</a>
                            @else
                                <button id="btn-nego" type="button" class="btn btn-nego" data-toggle="modal" data-target="#PriceNego" style="{{ ($product['pricing']->negotiable == false)?'display:none':'' }}">@lang('localize.negotiate')</button>
                            @endif
                            {{-- <a href="javascript:void(0)" class="btn btn-chat">Chat</a> --}}
                        {{-- <a href="javascript:void(0)" class="btn btn-nego">Negotiate</a> --}}
                        @endif
                    </div>

                    @if (request()->routeIs('product-wholesale'))
                    <input type="hidden" id="max_quantity" value="{{ $product['details']->pro_qty }}">
                    @elseif (($promo_product != null && $promo_product_info != null) && ($promo_product_info->limit > $promo_product_info->count) && ($promo_product->started_at <= $today_datetime && $promo_product->ended_at >= $today_datetime))
                    <input type="hidden" id="max_quantity" value="{{ ($product['details']->pro_qty > ($promo_product_info->limit - $promo_product_info->count)) ? $promo_product_info->limit - $promo_product_info->count : $product['details']->pro_qty }}">
                    @else
                    <input type="hidden" id="max_quantity" value="{{  $product['pricing']->quantity }}">
                    @endif
                    <input type="hidden" id="min_quantity" value="{{ (request()->routeIs('product-wholesale')) ? $product['pricing']->min_quantity : 1 }}">
                    <input type="hidden" name="product_id" value="{{$product['details']->pro_id}}" />
                    <input type="hidden" name="merchant_id" value="{{$product['details']->pro_mr_id}}" />
                    <input type="hidden" name="price_id" id="price_id" value="{{$product['pricing']->id}}" />
                    @if(request()->routeIs('product-wholesale') && auth()->check() && auth()->user()->isRetailerForDistributor($product['details']->merchant))
                        <input type="hidden" name="wholesale" id="wholesale" value="true">
                    @endif
                </form>
            </div>
        </div>
        <div class="merchant-container product">
            <div class="merchant-logo-frame">
                <img class="merchant-image" src="{{ $store->getImageUrl() }}" onerror="if (this.src != 'error.jpg') this.src = '/asset/images/product/default.jpg';" alt="Merchant image"/>
            </div>
            <a class="merchant-name pd-x-sm">{{ $store->stor_name }}</a>
            <a href="/store/{{ $store->url_slug }}" class="btn btn-visit ml-auto">@lang('localize.mer_store_information')</a>
        </div>
        <div class="product-detail-container mr-y-lg">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('front.section.product_details')</h4>
                <div class="red-head"></div>
            </div>
            <div class="description-container bg-white mr-top-sm pd-lg">
                <div class="display-part">
                    {!! $product['details']->desc !!}
                </div>
            </div>
        </div>

        <div class="section-review">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('localize.review_rating')</h4>
                <div class="red-head"></div>
            </div>
            <div class="review-panel">
                <div class="rating">
                    <div class="current-rate">
                        <h2>{{ $compiledRatings->rate }}/5</h2>
                        @include('front.partial.star.tag', ['rating' => $compiledRatings])
                        <label>{{ $compiledRatings->total }} @lang('localize.rating')</label>
                    </div>
                    <div class="rate-detail">

                        @foreach ($compiledRatings->cumulative as $i => $rating)
                        <div class="list">
                            @include('front.partial.star.tag', ['percentage' => $i * 20])
                            @include('front.partial.star.bar')
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="review">
                    <h4>@lang('localize.review')</h4>

                    @foreach ($ratings as $rating)
                    @include('front.partial.rating')
                    @endforeach

                    <ul class="pagination">
                        {{ $ratings->links() }}
                    </ul>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="login-modal">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content select-car-modal">
            <!-- Create New -->
            <div class="fromNew">
                <div class="logo-container text-center">
                    <a href="{{ route('home') }}"><img src="{{ asset('asset/images/logo/logo.png') }}" /></a>
                </div>
                <hr>
                <div class="topbar d-flex flex-row">
                    <h4 class="modal-title text-center">@lang('localize.login_to') <span style="color: rgb(255,24,24);">@lang('common.mall_name')</span></h4>
                    <div class="red-head"></div>
                </div>

                <form id="form-login" class="form-login" action="{{ route('login') }}" method="POST">
                    {{ csrf_field() }}
                <div class="mr-top-md">
                    <div class="form-group">
                        <label class="control-label">@lang('localize.email')</label>
                        <input class="form-control" name="login" type="email">
                    </div>
                    <div class="form-group">
                        <label class="control-label">@lang('localize.password')</label>
                        <input class="form-control" name="password" type="password">
                    </div>
                    <input type="hidden" name="from_cart" value="true">
                    <input type="hidden" name="path" value="{{ \Request::fullUrl() }}">
                </div>
                <hr>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-red">@lang('front.button.login')</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="PriceNego" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang('localize.price_negotiation')</h4>
            </div>
            <div class="modal-body">
                <form id="form-nego" action="{{ route('new_negotiation') }}" method="POST">
                {{ csrf_field() }}
                <section class="product">
                    <div class="thumb">
                        <img src="{{ \Storage::url('product/' . $product['details']['pro_mr_id'] . '/' . $product['main_image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                    </div>
                    <div class="product-content">
                        <h4>{{ $product['details']->title }}</h4>
                        @if(count($attribute_listing['parent']) > 0)
                            <ul class="attribute" id="nego-attr">
                                @foreach($attribute_listing['parent'] as $parent_id => $attribute)
                                    <li>{{ $attribute['name'] }} : <span id="nego-attr-item"></span></li>
                                @endforeach
                            </ul>
                        @endif
                        {{--<span class="cart-item-label">Installation</span> <small class="no-shipping">* this item will not be shipped</small>--}}
                        {{--<div class="service-section">--}}
                            {{--<span class="service-item">Painting service</span>--}}
                        {{--</div>--}}
                        <div class="price" id="nego-price">
                            @if(request()->routeIs('product-wholesale'))
                                {{ rpFormat($product['pricing']->wholesale_price) }}
                            @else
                                {{ rpFormat($product['pricing']->price) }}
                            @endif
                        </div>
                    </div>
                </section>

                {{--@if ($existing_nego)--}}
                {{--<section class="expiry-date">
                    Expiry Time : <span id="countdown"></span> left
                </section>--}}

                    {{--@if ($existing_nego->merchant_offer_price)--}}
                    {{--<section class="offer">--}}
                        {{--<label>Seller Offer Price</label>--}}
                        {{--<div class="price">RP 44.000</div>--}}
                        {{--<div class="action">--}}
                            {{--<button class="btn">Accept</button>--}}
                        {{--</div>--}}
                    {{--</section>--}}
                    {{--@endif--}}
                {{--@endif--}}

                <section class="form">
                    <div class="form-group">
                        <label>@lang('localize.quantity')</label>
                        <div class="quantity-setting">
                            <button type="button" class="decrease">-</button>
                            <input id="nego-qty" type="number" name="nego_qty" value="1" min="1">
                            <button type="button" class="increase">+</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>@lang('localize.price_offer') @lang('localize.per_quantity')</label>
                        <input class="form-control" name="nego_price">
                        <span class="help-block">{!! trans('localize.negotiation_rule', ['price' => request()->routeIs('product-wholesale') ? $product['pricing']->wholesale_price : $product['pricing']->price]) !!}</span>
                    </div>

                    <div class="action">
                        <button id="btn-submit-nego" type="submit" class="btn btn-red">@lang('localize.submit')</button>
                    </div>

                    <div class="hidden">
                        <input type="hidden" name="nego_product_id" value="{{$product['details']->pro_id}}">
                        <input type="hidden" id="nego-pricing-id" name="nego_pricing_id" value="{{$product['pricing']->id}}">
                        @if(request()->routeIs('product-wholesale'))
                            <input type="hidden" name="wholesale" value="1">
                        @endif
                    </div>

                </section>
                </form>
            </div>
            @if($product['details']->negotiations->isNotEmpty())
                <div class="modal-header border-top">
                    <h4 class="modal-title">Active Negotiations</h4>
                </div>
                <div class="p-3">
                    <table class="table table-borderless">
                        <tr>
                            <th>#ID</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Time Left</th>
                        </tr>
                        @foreach($product['details']->negotiations as $negotiation)
                            @if($negotiation->parent)
                                <tr>
                                    <td>
                                        <a href="{{ route('nego-detail', $negotiation->parent) }}">#{{ $negotiation->parent->id }}</a>
                                    </td>
                                    <td>{{ $negotiation->parent->quantity }}</td>
                                    <td>
                                        @if($negotiation->parent->merchant_offer_price)
                                            {{ rpFormat($negotiation->parent->merchant_offer_price) }}
                                        @else
                                            {{ rpFormat($negotiation->parent->customer_offer_price) }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($negotiation->parent->expired_at)->diffForHumans() }}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td>
                                    <a href="{{ route('nego-detail', $negotiation) }}">#{{ $negotiation->id }}</a>
                                </td>
                                <td>{{ $negotiation->quantity }}</td>
                                <td>
                                    @if($negotiation->merchant_offer_price)
                                        {{ rpFormat($negotiation->merchant_offer_price) }}
                                    @else
                                        {{ rpFormat($negotiation->customer_offer_price) }}
                                    @endif
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($negotiation->expired_at)->diffForHumans() }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @endif
      </div>

    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('asset/js/simplegallery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('asset/js/sweetalert2.js') }}"></script>
<script type="text/javascript" src="{{ asset('asset/js/moment.js') }}"></script>
@if (!empty($success) || session('success'))
    @if(!isset($active_nego)) {
        <script type="text/javascript">
            swal('{{ trans("common.success") }}', '{{ trans("front.cart.success.add") }}', 'success');
        </script>
    @else
        <script type="text/javascript">
            swal('{{ trans("common.success") }}', '{{ session('success') }}', 'success');
        </script>
    @endif
@endif
<script>
    $(document).ready(function(){

        var EndDate = $( "#ended_time" ).val();
        var countDownDate = new Date(EndDate).getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

        // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("countdown_timer").innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";

            // If the count down is over, write some text
            if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown_timer").innerHTML = "EXPIRED";
            }
        }, 1000);

        $('#gallery').simplegallery({
            galltime : 400,
            gallcontent: '.content',
            gallthumbnail: '.thumbnail',
            gallthumb: '.thumb'
        });

        $('.quantity-setting .increase').click(function () {
            $(this).prev().val(+$(this).prev().val() + 1);
        });

        $('.quantity-setting .decrease').click(function () {
            if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
        });

        $('.number').keydown(function (e) {-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

        var parent_listing = {!! json_encode($attribute_listing['parent']) !!};
        var pricing_attribute = {!! $attribute_listing['pricing'] !!};
        var attribute_end = parseInt("{{ count($attribute_listing['parent']) }}")-1;
        var this_attribute_onload = {!! json_encode($product['attributes']) !!};

        $('#nego-attr-item').text($('#attribute-item-' + this_attribute_onload).text());

        if(this_attribute_onload.length > 0) {
            var onload_attributes = new Array;
            var next_parent_id = 1;
            $.each(this_attribute_onload, function( parent, attribute_id ) {
                onload_attributes[parseInt(parent)] = parseInt(attribute_id);

                if(parent == 0 ) {
                    $("#attribute_"+attribute_id).prop('checked', true);
                }

                // $("#attribute_"+next_parent_id).html(""); //reset child options
                // $("#attribute_"+next_parent_id).append("<option value='0'>"+'{{ trans('localize.selectOption') }}'+"</option>");


                var result = get_child_attribute_list(pricing_attribute, onload_attributes);

                if(next_parent_id <= attribute_end) {
                    $.each(result, function(key, result_attribute_id) {
                        var attribute_name = String(parent_listing[next_parent_id]["items"][result_attribute_id]);
                        var val = next_parent_id+":"+result_attribute_id;
                            // $("#attribute_"+next_parent_id).append("<option value='"+val+"'>"+attribute_name+"</option>");
                            $("#parent_attribute_"+next_parent_id).append("<label class='radio radio-inline'><input type='radio' name='attribute_selection["+next_parent_id+"]' id='attribute_"+result_attribute_id+"' value='"+val+"' class='attribute_selection'><span>"+attribute_name+"</span></label>");
                    });
                }
                $("#attribute_"+attribute_id).prop('checked', true);

                next_parent_id++;
            });
        }

        $(document).on('change', 'input.attribute_selection', function() {
            var selected = this.value.split(':');
            var selected_parent = parseInt(selected[0]);
            var selected_attribute = parseInt(selected[1]);
            var selected_attribute_name = $('#attribute-item-' + selected_attribute).text();
            var attributes = new Array;

            $('#nego-attr-item').text(selected_attribute_name);

            if(selected_parent == attribute_end) {
                $(".attribute_selection").each(function(){
                    if ($(this).prop('checked')) {
                        attributes.push(parseInt(this.value.split(':')[1]));
                    }
                });

                get_pricing_by_selected_attribute(attributes);

                return false;
            }

            if(selected_parent > 0)
            {
                var parent = 0;
                while(parent <= selected_parent) {
                    attributes[parent] = parseInt($("input[name='attribute_selection["+parent+"]']:checked").val().split(':')[1]);
                    parent++;
                }
            } else {
                attributes[selected_parent] = selected_attribute;
            }

            var loop = (selected_parent + 1 );
            while ( loop <= attribute_end) {

                $("#parent_attribute_"+loop).html(""); //reset child options
                loop++;
            }

            var childs = get_child_attribute_list(pricing_attribute,attributes);
            var next_parent_id = attributes.length;

            $.each(childs, function(key,attribute_id) {
                var attribute_name = String(parent_listing[next_parent_id]["items"][attribute_id]);
                var value = next_parent_id+":"+attribute_id;
                $("#parent_attribute_"+next_parent_id).append("<label class='radio radio-inline'><input type='radio' name='attribute_selection["+next_parent_id+"]' id='attribute_"+attribute_id+"' value='"+value+"' class='attribute_selection'><span>"+attribute_name+"</span></label>");
            });
        });

        $("#detailcart").on('submit', function(e){
            var valid = true;
            var total_in_cart = {!! json_encode($total_in_cart) !!};
            var total = parseInt(total_in_cart[$('#price_id').val()]);
            if(isNaN(total))
                total = 0;

            // Check all attributes selected
            var attribute_end = parseInt("{{ count($attribute_listing['parent']) }}");
            var attribute_selected = $(":radio[class='attribute_selection']:checked").length;

            if (attribute_selected != attribute_end) {
                swal("@lang('localize.alert')!", "@lang('localize.select_option')", "error");
                valid = false;
            }

            if ($('#qty').val() == '' || ( parseInt($('#qty').val()) + total) > parseInt($('#max_quantity').val())) {
                swal("@lang('localize.alert')!", "@lang('localize.checkout_insufficient')"+$('#max_quantity').val(), "error");
                valid = false;
            }

            if ((parseInt($('#qty').val()) + total) < parseInt($('#min_quantity').val())) {
                swal("@lang('localize.alert')!", "@lang('localize.checkout_moq')"+$('#min_quantity').val(), "error");
                valid = false;
            }

            if (!valid) {
                e.preventDefault();
            } else {
                $('.loading').show();
            }
        });

        function get_child_attribute_list(pricing_attribute, attributes)
        {
            var contains = new Array;
            var max_check = parseInt(attributes.length) - 1;
            var next_parent_id = attributes.length;
            $.each(pricing_attribute, function(pricing_id, items) {
                var i = 0;
                var has = 0;
                while ( i <= max_check) {
                    if(items[i] == attributes[i]) {
                        has++;
                    }
                    i++;
                }

                if(has > max_check) {
                    var stats = $.inArray(pricing_attribute[pricing_id][next_parent_id], contains);
                    if (stats == -1) {
                        contains.push(pricing_attribute[pricing_id][next_parent_id]);
                    }
                }
            });

            return contains;
        }

        //function to populate child select box
        function get_pricing_by_selected_attribute(attributes) {

            $.ajax({
                type: "GET",
                url: '/product_detail/get_attribute_selection/'+"{{$product['details']->pro_id}}",
                data: {attributes: attributes},
                beforeSend : function() {

                },
                success: function (response) {
                    console.log(response);
                    if(response != 'empty') {
                        $(".loading").show();

                        if(response['is_discounted'] == 1) {
                            $('.with_discounted').show();
                            $('.without_discounted').hide();
                            $('.discounted_rate').text(parseFloat(response['discounted_rate']).toFixed(2));
                            $('.discounted_purchase_price').text(response['purchase_price']);
                            $('.discounted_price').text(response['price']);
                            $('#max_quantity').val(response['quantity']);
                        } else {
                            $('.with_discounted').hide();
                            $('.without_discounted').show();
                            $('.discounted_purchase_price').text(response['purchase_price']);
                            $('#max_quantity').val(response['quantity']);
                        }

                        if(response['quantity'] > 0) {
                            $('#btn-add-cart').show();
                            $('#outStock').hide();
                        } else {
                            $('#btn-add-cart').hide();
                            $('#outStock').show();
                        }

                        if (response.negotiation !== null) {
                            $('#btn-add-cart').next().remove();
                            $('')
                        }

                        if(response['negotiable'] == true) {
                            $('#btn-nego').show();
                            $('#nego-price').text(response['purchase_price']);
                            $('#nego-base-price').text(response['purchase_price']);
                            $('#nego-pricing-id').val(response['price_id']);
                        }
                        else {
                            $('#btn-nego').hide();
                        }

                        $('#selected_attribute').val(JSON.stringify(attributes));
                        $('#price_id').val(response['price_id']);
                        $(".loading").hide();
                    } else {
                        swal('Not Available!','Product with this attribute is not available. Please select another options.','warning');
                    }
                }
            });
        }
    });

    $('#btn-submit-nego').click(function () {
        $(this).html("<i class='fa fa-spinner fa-spin'></i> Processing..");
        $(this).prop('disabled', true);
        $('#form-nego').submit();
    });

</script>
@if(!auth()->check())
<script type="text/javascript">
    $('#btn-add-cart').click(function(){
        $('#login-modal').modal();
    });
</script>
@endif
@if($existing_nego)
<script type="text/javascript">

    var expired_at = "{{ $existing_nego->expired_at }}";

    var cd = setInterval(function() {

        var now = moment().utc().format('YYYY-MM-DD HH:mm:ss');

        var difference = new Date(expired_at).getTime() - new Date(now).getTime();
        console.log(difference);
        var days = Math.floor(difference / (1000 * 60 * 60 * 24));
        var hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + (24 * days);
        var minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((difference % (1000 * 60)) / 1000);

        $('#countdown').text(hours + ':' + minutes + ':' + seconds);
    }, 1000);

</script>
@endif
@endsection
