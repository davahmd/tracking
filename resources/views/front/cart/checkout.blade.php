
@extends('layouts.front_master')

@section('title', trans('localize.cart'))

@section('style')
<link rel="stylesheet" href="{{ asset('assets/js/jquery-loading-master/loading.css') }}">
<style>
#custom-overlay {
  background-color: #000;
  opacity: 0.7;
  display:none;
}
.loading-spinner {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(/asset/images/dribbble.gif) 50% 50% no-repeat;
    background-size: 100px;

}
.shipping-radio-btn {
    display : none;
}
</style>
@endsection

@section('content')
    @php
        if (request()->routeIs('wholesale-checkout')) {
            $carts = $wholesaleCarts;
            $total = $wholesaleTotal;
        }
    @endphp
    <div id="main">
        <div class="container pd-btm-lg pd-top-md">
            <div class="main-overview-container mr-y-md">
                @if (!$carts->count())
                <!-- if the cart empty -->
                <div class="empty">
                    <img src="{{ asset('asset/images/icon/icon_cart_empty.png') }}">
                    <h5>@lang('localize.nothing_found_in_cart')</h5>
                    <a href="{{ route('home') }}" class="btn">@lang('localize.conShopping')</a>
                </div>
                @else
                <form action="{{ route('checkout') }}" method="POST" enctype="multipart/form-data" id="checkout_submit" class="checkout-form-container">
                {{ csrf_field() }}
                <div class="cart-item-container">
                    @foreach ($carts->groupBy('product.store.stor_name') as $store_name => $items)
                    <div class="cart-merchant-items mr-btm-md border">
                        <div class="merchant-bar">
                            <a href="javascript:void(0)" class="d-flex align-items-center">
                                <i class="fa fa-store"></i>&ensp;<h6>{{ $store_name }}</h6>
                            </a>

                            @if($items->where('service_ids',null)->count()>0)
                            <select id="select-courier-{{$items[0]->product->pro_sh_id}}" name="shipping_courier[]" class="form-control select-courier" data-id="{{$items[0]->product->pro_sh_id}}">
                                <option value="0">Select Courier</option>
                            </select>
                            @endif
                            <input type="hidden" id="shipping-fee-{{$items[0]->product->pro_sh_id}}" class="store-shipping-fee" value="0">
                            {{-- <p class="estimation-time">Estimate Delivery in 4-7 days</p> --}}
                        </div>
                        @foreach ($items as $key => $cart)
                        <div class="cart-container">
                            <div class="cart-item">

                                <div class="cart-item-image">
                                    <a href="/product/{{ Helper::slug_maker($cart->product->pro_title_en, $cart->product->pro_id) }}">
                                        <img src="{{ $cart->product->mainImageUrl() }}" class="image_1" alt="" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';" />
                                    </a>
                                    @if($cart->nego_id)
                                        <span class="discount-rate bg-orange">Nego</span>
                                    @endif
                                </div>
                                <div class="cart-item-content mr-top-sm">
                                    <h6 class="cart-item-title">
                                        <a href="/product/{{ Helper::slug_maker($cart->product->pro_title_en, $cart->product->pro_id) }}">{{ $cart->product->title }}</a>
                                    </h6>
                                    <p>{{ rpFormat($cart->charge->amount->value) }}</p>
                                    <p class="cut black">
                                        <small><span class="discounted_price">
                                            {{ ($cart->charge->amount->original_price) ? rpFormat($cart->charge->amount->original_price) : '' }}
                                        </span></small>
                                    </p>
                                    @if($cart->service_ids)
                                    <span class="cart-item-label">@lang('localize.installation')</span> <small class="no-shipping">* @lang('localize.this_item_will_not_be_shipped')</small>
                                    <div class="service-section">
                                        @foreach($cart->services as $service)
                                            <span class="service-item">{{$service->service_name}}</span>
                                        @endforeach
                                    </div>
                                    @endif
                                    <p>
                                        @if($cart->pricing->attributes)
                                            <small>
                                                <p><strong>{!!$cart->pricing->attributes_name_view!!}</strong></p>
                                            </small>
                                        @endif
                                    </p>
                                    <p class="cart-item-qty">@lang('localize.quantity'): {{$cart->quantity}}</p>
                                    <p class="cart-item-qty">@lang('localize.weight'): {{$cart->weight_per_qty * $cart->quantity }} gram</p>

                                    <!-- COURIER SELECTION, come with some script at below  -->
                                    <!--select name="shipping" class="form-control select-courier">
                                        <option>Select Courier</option>
                                    </select-->
                                </div>

                                {{-- <div class="cart-item-subtotal mr-top-sm"><h5>{{ $cart->pricing->country->co_cursymbol }} <span id="pricetotal_{{$key}}" class="price_total">{{ number_format( $cart->charge->amount->subtotal, 2) }}<span></h5></div> --}}
                                <div class="cart-item-subtotal mr-top-sm">
                                    <h5><span id="pricetotal_{{$key}}" class="price_total">{{ rpFormat($cart->charge->amount->subtotal) }}<span></h5>
                                </div>
                            </div>
                        </div>

                        @if ($key < (count($items) - 1))
                        @endif

                        @endforeach
                    </div>
                    @endforeach
                </div>

                <div class="side-container">
                    @include('front.common.success')
                    @include('front.common.errors')
                    <div class="delivery-container border">
                        <div class="d-flex align-items-center">
                            <h5 class="product-title mr-auto">@lang('localize.Delivery_Address')</h5>
                            <button type="button" class="btn btn-red btn-sm" data-toggle="modal" data-target="#addressModal">
                            @if(count($shippings) > 0)
                                @lang('localize.change_address')
                            @else
                                @lang('localize.add_new_address')&ensp;<i class="fa fa-plus fa-sm"></i>
                            @endif
                            </button>
                        </div>
                        <hr />
                        <div class="d-flex mr-top-sm shipping-address">
                            <div class="address-info">
                            @foreach($shippings as $shipping)
                                @if($shipping->ship_id == $selectedship_id)
                                    <p class="address-receiver">{{$shipping->ship_name}}</p>
                                    <p class="info"><i class="material-icons md-18" style="font-size: 16px">phone</i><span class="phone-info">{{$shipping->ship_phone}}</span></p>
                                    <p class="info"><i class="material-icons md-18" style="font-size: 16px">place</i><span class="address-location">{{ $shipping->getFullAddress() }}</span></p>
                                @endif
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="order-summary-container border">
                        <h5 class="product-title">@lang('localize.cart_summary')</h5>
                        <hr />
                        <div class="d-flex mr-top-sm">
                            <p class="mr-auto">@lang('localize.subtotal') ({{ count($carts) }} @lang('localize.items'))</p>
                            <p>{{ rpFormat(($total->amount->subtotal == 0) ? 0 : $total->amount->subtotal) }}</span></p>
                            <input type="hidden" id="subtotal" value="{{ ($total->amount->subtotal == 0) ? 0 : ceil($total->amount->subtotal) }}"/>
                        </div>

                        <div class="d-flex">
                            <p class="mr-auto">@lang('localize.shipping_fees')</p>
                            {{-- <p>{{$cart->pricing->country->co_cursymbol}} <span id="shipping_fee">0</span></p> --}}
                            <p>{{ config('app.currency_code') }} <span id="shipping_fee_view">0</span></p>
                            <input type="hidden" id="shipping_fee" value="0"/>
                        </div>

                        {{--<div class="d-flex">
                            <p class="mr-auto">Promo Code</p>
                            <p>0</p>
                        </div>--}}
                        <div class="d-flex mr-top-sm promo-input-container">
                            <input class="promo-input mr-auto" type="text" name="promo_code" placeholder="Enter promo code" />
                            <i id="promo-loader" class='fa fa-spinner fa-pulse' style="display: none;"></i>
                            <button class="btn btn-promo" type="button">Apply</button>
                        </div>
                        <small id="promo-code-response" class="text-danger"></small>
                        <hr />
                        <div class="d-flex mr-top-sm">
                            <p class="mr-auto">@lang('localize.discount')</p>
                            <p id="discount_total">{{ rpFormat(0) }}</p>
                        </div>

                        @if ($total->amount->charges->service)
                            <div class="d-flex">
                                <p class="mr-auto">@lang('localize.gst')</p>
                                <p id="service_charge_fee">{{ rpFormat(($total->amount->charges->service == 0) ? 0 : $total->amount->charges->service) }}</span></p>
                            </div>
                        @endif
                        <input type="hidden" id="service_charge" value="{{ ($total->amount->charges->service == 0) ? 0 : ceil($total->amount->charges->service) }}"/>

                        @if ($total->amount->charges->platform)
                            <div class="d-flex">
                                <p class="mr-auto">@lang('localize.platform_charges')</p>
                                <p id="platform_charge_fee">{{ rpFormat(($total->amount->charges->platform == 0) ? 0 : $total->amount->charges->platform) }}</span></p>
                            </div>
                        @endif
                        <input type="hidden" id="platform_charge" value="{{ ($total->amount->charges->platform == 0) ? 0 : ceil($total->amount->charges->platform) }}"/>

                        <div class="d-flex mr-top-sm">
                            <h5 class="mr-auto">@lang('localize.total')</h5>
                            {{-- <h5 class="red">{{$cart->pricing->country->co_cursymbol}} <span id="total">{{ ($total->amount->subtotal == 0)? 0 : $total->amount->subtotal }}</span></h5> --}}
                            <h5 id="total" class="red font-weight-bold">{{ ($total->amount->total == 0)? 0 : rpFormat($total->amount->total) }}</h5>
                        </div>
                        <div class="button-layer mr-top-md">
                            {{--<input type="checkbox" id="fail" name="fail"> Checkout Fail--}}
                            <input type="hidden" name="ship_id"  id="ship_id" value="{{$selectedship_id}}">
                            @if(request()->routeIs('wholesale-checkout'))
                                <input type="hidden" name="wholesale" value="true">
                            @endif
                            <button class="btn btn-cart" id="btn_submit" name="checkoutform">{{trans('localize.make_payment')}}</button>
                        </div>
                    </div>
                </div>
                @endif
                </form>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="addressModal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content select-address-modal">
                <div class="row">
                    <div class="col-md-12 mr-btm-md">
                        <div class="topbar d-flex flex-row">
                            <h4 class="modal-title mr-auto">@lang('localize.select_delivery_address')</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="red-head"></div>
                        </div>
                    </div>
                    <!-- Select Address -->
                    <div class="change-address col-md-7">
                        <form class="mr-top-sm" action="/cart/checkout" method="post">
                            {{ csrf_field() }}
                            <div class="address-box mr-btm-sm">
                            @php
                                $count=0;
                            @endphp
                            @foreach($shippings as $shipping)
                                @php
                                    $count++;
                                @endphp
                                <a href="javascript:void(0)">
                                    <div data-id="{{$shipping->ship_id}}" class="address-item {{$shipping->ship_id==$selectedship_id?'active':''}}">
                                        <div class="shipping-radio-btn">
                                            <input type="radio" name="shipping-address-radio" value="{{$shipping->ship_id}}" id="choose-address-{{$shipping->ship_id}}">
                                        </div>
                                        <p class="address-label">@lang('localize.quantity') #{{$count}}  {!!$shipping->ship_id==$selectedship_id?'<span class="selected">Selected</span>':''!!}</p>
                                        <div class="address-info">
                                            <p class="address-receiver">{{$shipping->ship_name}}</p>
                                            <p class="info"><i class="material-icons md-18" style="font-size: 16px">phone</i><span class="phone-info">{{$shipping->ship_phone}}</span></p>
                                            <p class="info"><i class="material-icons md-18" style="font-size: 16px">place</i><span class="address-location">{{ $shipping->getFullAddress() }}</span></p>
                                        </div>
                                    </div>
                                </a>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <button class="btn btn-blue" type="submit" name="addressselected">@lang('localize.use_this_address')</button>
                            </div>
                        </form>
                    </div>
                    <!-- Add New Address -->
                    <div class="add-address col-md-5">
                        <div class="topbar">
                            <h4 class="modal-title">@lang('localize.add_new_address')</h4>
                        </div>
                        <form class="mr-top-sm" action="/cart/checkout" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="country" id="country" value="{{ $country->co_id }}" />
                            <div class="form-group">
                                <input type="text" name="ship_name" placeholder="{{trans('localize.name')}}" value="{{ old('ship_name') }}" />
                            </div>
                            <div class="form-group">
                                <select name="areacode" class="form-control js-option-tags">
                                    <option   value="{{$country->phone_country_code}}">+{{$country->phone_country_code}} - {{$country->co_name}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="{{trans('localize.phone')}}" value="{{ old('phone') }}" />
                            </div>
                            <div class="form-group">
                                <input type="text" name="address1" placeholder="{{trans('localize.address1')}}" value="{{ old('address1') }}" />
                            </div>
                            <div class="form-group">
                                <input type="text" name="address2" placeholder="{{trans('localize.address2')}}" value="{{ old('address2') }}" />
                            </div>
                            <div class="form-group">
                                <select id="state" name="state" class="form-control js-option-tags">
                                    <option>{{trans('localize.selectState')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="city" name="city" class="form-control js-option-tags">
                                    <option>{{trans('localize.selectCity')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="subdistrict" name="subdistrict" class="form-control js-option-tags">
                                    <option>{{trans('localize.selectSubdistrict')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" name="zipcode" placeholder="{{trans('localize.zipcode')}}" value="{{ old('zipcode') }}" />
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label" >
                                        <input class="form-check-input" type="checkbox" >
                                        <span>{{trans('localize.save_this_address_as_default')}}</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-blue" type="submit" name="addressform">@lang('localize.add_new_address')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="custom-overlay">
                    <div class="loading-spinner"></div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ asset('assets/js/jquery-loading-master/loading.js') }}"></script>
<script>
    @foreach($courier_options as $option)
        @if($option['options'])
        var data_{{$option['store_id']}} = [
            @foreach($option['options'] as $c)
                {
                    text:'{{$c->code}}',
                    children: [
                    @foreach($c->costs as $co)
                        {
                            id:'{{$co->service}}',
                            text:'{{$co->description}}',
                            html:'<div class="courier-list"><div class="courier-name">{{$co->description}}<label class="time">{{$co->cost[0]->etd}} Days</label></div><div class="shipping-price">{{rpFormat($co->cost[0]->value)}}</div></div>',
                            display:'{{$c->code}} - {{$co->description}} - {{rpFormat($co->cost[0]->value)}}',
                            value: '{{$co->cost[0]->value}}',
                            code: '{{$c->code}}',
                            etd:'{{$co->cost[0]->etd}}',
                            ship_id:'{{$selectedship_id}}'
                        },
                    @endforeach
                    ]
                },
            @endforeach

        ];
        @else
        var data_{{$option['store_id']}} = [{
            id: '0',
            text:'@lang("localize.checkouts.no_courier_available")',
            html:'@lang("localize.checkouts.no_courier_available")'
        }];
        @endif

        $("#select-courier-{{$option['store_id']}}").select2({
            minimumResultsForSearch: -1,
            data: data_{{$option['store_id']}},
            templateResult: courier,
            templateSelection: formatData,
            escapeMarkup: function(m) {
                return m;
            }
        });
    @endforeach

    $(".select-courier").on("select2:select", function (e) {

        var id = $(this).attr('data-id');
        var thisoldshippingfee = parseInt($('#shipping-fee-'+id).val());
        var thisnewshippingfee = parseInt(e.params.data.value);
        var totalshippingfee = parseInt($('#shipping_fee').val());
        totalshippingfee = totalshippingfee-thisoldshippingfee+thisnewshippingfee;
        $('#shipping-fee-'+id).val(thisnewshippingfee);
        $('#shipping_fee_view').html(number_seperator(totalshippingfee));
        $('#shipping_fee').val(totalshippingfee);
        var subtotal =  parseInt($('#subtotal').val());
        var service_charge =  parseInt($('#service_charge').val());
        var platform_charge =  parseInt($('#platform_charge').val());
        $('#total').html("{{ config('app.currency_code') }} " + number_seperator(subtotal+platform_charge+service_charge+totalshippingfee));

        $('#btn_submit').attr('disabled', true);

        $.ajax({
        type:   'post',
                data: { '_token':'{{ csrf_token() }}',
                        'store_id': id,
                        'ship_code':e.params.data.code,
                        'ship_service':e.params.data.id,
                        'ship_note': e.params.data.code + ' - ' + e.params.data.text,
                        'ship_eta':e.params.data.etd,
                        'ship_id':e.params.data.ship_id},
                // url: '{{url('/update_cart_courier')}}',
                url: '/update_cart_courier',
                beforeSend: function()
                {
                    $('body').loading({
                        overlay: $("#custom-overlay")
                    });
                },
                success: function ( ) {
                    $('body').loading('stop');
                    $('#btn_submit').attr('disabled', false);
                    if ($('.promo-input').val() != '') {
                        promoCodeCheck();
                        console.log('changing courier and send promo check');
                    }
                }
            });
     });

    function number_seperator(number) {
        number += '';
        var x = number.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function courier(data) {
        if (!data.id) {
            return data.text;
        }
        return data.html;
    }

    function formatData (data) {
        if (!data.display) {
            return 'Select Courier';
        }
        var $display = $( '<span>'+data.display+'</span>');
        return $display;
        };

</script>

<script src="/backend/js/custom.js"></script>
<script>
$(document).on('click','.address-item', function() {
            var id = $(this).attr('data-id');
            $('.address-item').removeClass('active');
            $('span.selected').remove();
            $(this).addClass('active');
            $("#choose-address-"+id).prop("checked", true);

            //$( ".shipping-address" ).html('');
            $(this).find('.address-label').append('<span class="selected">Selected</span>');
//$(this).find('.address-info').clone().appendTo( ".shipping-address" );
            //$("#ship_id").val(id);
           // $.ajax({
           //             type: 'post',
           //             data: { "_token":"{{ csrf_token() }}", 'name': 'cart_ship_id', 'value':id },
           //           url: '{{url('/set_server_cookie')}}',
           //             success: function ( ) {
            //
            //            }
            //        });


        });

load_state('#state',"{{ $country->co_id }}", "{{ old('state') }}");
load_city('#city',"{{ old('state') }}", "{{ old('city') }}");
load_subdistrict('#subdistrict', "{{ old('city') }}", "{{ old('subdistrict') }}");

$(document).ready(function() {

    $('.btn-promo').click(function() {
        promoCodeCheck();
    });


    // $('.select-courier').on('select2:select', function (e) {
    //     if ($('.promo-input').val() != '') {
    //         promoCodeCheck();
    //         console.log('changing courier and send promo check');
    //     }
    // });

    $('.delete_button').click(function() {
        var url = '/account/profile/delivery/delete/'+ $(this).data('ship-id');
        $('#deleteAddressModal a').attr('href', url);
    });


    load_state('#state',"{{ $country->co_id }}", "{{ old('state') }}");

    $('#state').change(function() {
        var update_input = '#city';
        var state_id = $(this).val();

        load_city(update_input, state_id);
    });

    $('#city').change(function() {
        var update_input = '#subdistrict';
        var city_id = $(this).val();

        load_subdistrict(update_input, city_id);
    });
});

function promoCodeCheck() {

    var promo_code = $('input[name="promo_code"]').val();

    $('#promo-loader').css('display', 'block');

    $.ajax({
        type: 'POST',
        url: '/get_promo_code',
        data: { promo_code: promo_code },
        success: function(data) {
            $('#promo-code-response').empty();
            $('#promo-loader').css('display', 'none');
            if(data.status == 'success') {

                // console.log('price discount: ' + data.result.discounted_product_value);
                // console.log('shipping discount: ' + data.result.discounted_shipping_fee);
                // console.log('platform: ' + data.result.total_platform);
                // console.log('service: ' + data.result.total_service);
                // console.log('total: ' + data.result.total_price);

                $('#promo-code-response').html('{{ trans("localize.promo_code_successfully_applied") }}');
                $('#discount_total').html('- ' + data.result.total_discount);
                $('#platform_charge_fee').html(data.result.total_platform_format);
                $('#platform_charge').val(data.result.total_platform);
                $('#service_charge_fee').html(data.result.total_service_format);
                $('#service_charge').val(data.result.total_service);
                $('#total').html(data.result.total_price);

            } else {
                $('#promo-code-response').html(data.message);
                $('#discount_total').html('{{ rpFormat(0) }}');
                $('#total').html(data.total_price);
            }
        }
    })
}

</script>
@endsection
