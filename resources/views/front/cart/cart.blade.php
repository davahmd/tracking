@extends('layouts.front_master')

@section('title', trans('localize.cart'))

@section('style')
<style type="text/css">
    input[type=number] {
        -moz-appearance:textfield;
    }
</style>
@endsection

@section('content')

    <div id="main">
        <div class="container pd-btm-lg pd-top-md">
            @if(auth()->user() && auth()->user()->merchant)
                <div class="main-overview-container mr-y-md">
                    <nav class="nav nav-pills nav-justified border-bottom">
                        <a class="nav-item nav-link {{ request()->routeIs('cart') ? 'active' : '' }}" href="{{ route('cart') }}">
                            <h5>Cart ({{ $carts->count() }} @lang('localize.items'))</h5>
                        </a>
                        <a class="nav-item nav-link {{ request()->routeIs('wholesale-cart') ? 'active' : '' }}" href="{{ route('wholesale-cart') }}">
                            <h5>Wholesale Cart ({{ $wholesaleCarts->count() }} @lang('localize.items'))</h5>
                        </a>
                    </nav>
                </div>
            @endif

            @php
                if (request()->routeIs('wholesale-cart')) {
                    $carts = $wholesaleCarts;
                    $total = $wholesaleTotal;
                }
            @endphp

            <div class="main-overview-container d-flex mr-y-md">
                @if (!$carts->count())
                <!-- if the cart empty -->
                <div class="empty">
                    <img src="{{ asset('asset/images/icon/icon_cart_empty.png') }}">
                    <h5>@lang('localize.nothing_found_in_cart')</h5>
                    <a href="{{ route('home') }}" class="btn">@lang('localize.conShopping')</a>
                </div>
                @else

                <div class="cart-item-container">
                    @foreach ($carts->groupBy('product.store.stor_name') as $store_name => $items)
                    <div class="cart-merchant-items mr-btm-md border">
                        <div class="merchant-bar">
                            <a href="javascript:void(0)" class="d-flex align-items-center">
                                <i class="fas fa-store"></i>&ensp;<h6>{{ $store_name }}</h6>
                            </a>
                            {{-- <p class="estimation-time">Estimate Delivery in 4-7 days</p> --}}
                        </div>
                        @foreach ($items as $key => $cart)
                        <div class="cart-container">
                            <div class="cart-item">
                                <div class="cart-item-image">
                                    <a href="/product/{{ Helper::slug_maker($cart->product->pro_title_en, $cart->product->pro_id) }}"><img src="{{ $cart->product->mainImageUrl() }}" class="image_1" alt="" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';" /></a>
                                    @if($cart->nego_id)
                                        <span class="discount-rate bg-orange">Nego</span>
                                    @endif
                                </div>
                                <div class="cart-item-content">
                                    <h6 class="cart-item-title">
                                        <a href="/product/{{ Helper::slug_maker($cart->product->pro_title_en, $cart->product->pro_id) }}">{{ $cart->product->title }}</a>
                                    </h6>
                                    <p>{{ rpFormat($cart->charge->amount->value) }}</p>
                                    <p class="cut black">
                                        <small><span class="discounted_price">
                                            {{ ($cart->charge->amount->original_price) ? rpFormat($cart->charge->amount->original_price) : '' }}
                                        </span></small>
                                    </p>
                                    @if($cart->service_ids)
                                    <span class="cart-item-label">@lang('localize.installation')</span> <small class="no-shipping">* @lang('localize.this_item_will_not_be_shipped')</small>
                                    <div class="service-section">
                                        @foreach($cart->services as $service)
                                            <span class="service-item">{{$service->service_name}}</span>
                                        @endforeach
                                    </div>
                                    @endif

                                    <p>
                                        @if($cart->pricing->attributes)
                                            <small>
                                                <p><strong>{!!$cart->pricing->attributes_name_view!!}</strong></p>
                                            </small>
                                        @endif
                                    </p>
                                    <div class="mr-top-sm align-items-center">
                                        @if($cart->nego_id)
                                            @lang('localize.quantity') : {{ $cart->quantity }}
                                        @else
                                            <div class="quantity-setting">
                                                <button class="decrease" data-key="{{ $cart->id }}">-</button>
                                                <input type="number" class="number data-id-{{ $cart->id }}"
                                                       id="qty_{{$cart->id}}" data-key="{{ $cart->id }}"
                                                       data-id="{{ $cart->id }}"
                                                       @if($cart->flashsale->limit)
                                                       data-max="{{ $cart->flashsale->limit - $cart->flashsale->count }}"
                                                       @else
                                                       data-max="{{ $cart->pricing->quantity }}"
                                                       @endif
                                                       @if(request()->routeIs('wholesale-cart'))
                                                       data-price="{{ ceil($cart->pricing->wholesale_price) }}"
                                                       data-min="{{ $cart->pricing->min_quantity }}"
                                                       @else
                                                       data-price="{{ ceil(($cart->discounted) ? $cart->pricing->discounted_price : $cart->pricing->price) }}"
                                                       data-min="1"
                                                       @endif
                                                       value="{{ $cart->quantity }}" data-value="{{ $cart->quantity }}"/>
                                                <button class="increase" data-key="{{ $cart->id }}">+</button>
                                                <input type="hidden" id="quantity_check_{{$cart->id}}" class="quantity_check" orderqty-id="{{$cart->quantity}}" proqty-id="{{$cart->pricing->quantity}}" proname-id="{{$cart->product->pro_title_en}}">
                                            </div>
                                        @endif
                                        <div class="cart-item-delete"><a href="javascript:void(0)" class="delete cart_remove" data-id="{{$cart->id}}"><i class="material-icons">delete</i></a></div>
                                    </div>
                                </div>

                                <div class="cart-item-subtotal mr-top-sm">
                                    <h5>
                                        <span id="pricetotal_{{$cart->id}}">{{ rpFormat($cart->charge->amount->subtotal) }}</span>
                                    </h5>
                                </div>
                                <input type="hidden" id="pricetotal_input_{{$cart->id}}" class="price_total" value="{{ ceil($cart->charge->amount->subtotal) }}"/>
                            </div>
                        </div>
                        @if ($key < (count($items) - 1))
                        @endif
                        @endforeach
                    </div>
                    @endforeach
                </div>

                <div class="side-container">
                    @include('front.common.success')
                    @include('front.common.errors')
                    <div class="order-summary-container border">
                        <h5 class="product-title">@lang('localize.cart_summary')</h5>
                        <hr />
                        <div class="d-flex mr-top-sm">
                            @php
                            $item_count = 0;
                            foreach ($carts as $cart) {
                                $item_count += $cart->quantity;
                            }
                            @endphp
                            <p class="mr-auto">@lang('localize.subtotal') ({{ $item_count }} @lang('localize.items'))</p>
                            <p><span id="subtotal">{{ rpFormat(($total->amount->subtotal == 0)? 0 : $total->amount->subtotal) }}</span></p>
                        </div>

                        {{-- @if ($total->amount->charges->service)
                        <div class="d-flex">
                            <p class="mr-auto">@lang('localize.service_charge')</p>
                            <p>{{$cart->pricing->country->co_cursymbol}} {{ number_format(($total->amount->charges->service == 0)? 0 : $total->amount->charges->service, 2) }}</p>
                        </div>
                        @endif

                        @if ($total->amount->charges->platform)
                        <div class="d-flex">
                            <p class="mr-auto">@lang('localize.platform_charges')</p>
                            <p>{{$cart->pricing->country->co_cursymbol}} {{ number_format(($total->amount->charges->platform == 0)? 0 : $total->amount->charges->platform, 2) }}</p>
                        </div>
                        @endif --}}
                        {{-- <div class="d-flex">
                            <p class="mr-auto">Promo Code</p>
                            <p>0</p>
                        </div> --}}
                        {{-- <div class="d-flex mr-top-sm">
                            <input class="promo-input mr-auto" type="text" placeholder="Enter promo code" />
                            <button class="btn btn-promo" type="submit">Apply</button>
                        </div> --}}
                        <hr />
                        {{-- <div class="d-flex mr-top-sm">
                            <h5 class="mr-auto">@lang('localize.total')</h5>
                            <h5 class="red">{{$cart->pricing->country->co_cursymbol}} <span id="total">{{ number_format(($total->amount->total == 0)? 0 : $total->amount->total, 2) }}</span></h5>
                        </div> --}}
                        <div class="button-layer mr-top-md">
                            @if(request()->routeIs('cart'))
                                <a href="{{ route('checkout') }}" class="btn btn-cart">@lang('localize.carts.proceed_to_checkout')</a>
                            @else
                                <a href="{{ route('wholesale-checkout') }}" class="btn btn-cart">@lang('localize.carts.proceed_to_checkout')</a>
                            @endif
                        </div>
                    </div>
                </div>



                @endif
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.input_checkbox').iCheck('check');
        function number_seperator(number) {
            number += '';
            var x = number.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }

        $(document).ready(function() {
            $('.number').on('change', function(event) {
                var item_key = $(this).attr('data-key');
                var cart_id = $(this).attr('data-id');
                var max_qty = $(this).attr('data-max');
                var min_qty = $(this).attr('data-min');
                var price = parseFloat($(this).attr('data-price'));
                var qty = parseInt($(this).val());
                var qty_before = parseInt($(this).attr('data-value'));
                var subtotal = parseFloat(price * qty);

                if(qty < min_qty) {
                    swal('Minimum quantity limit');
                    event.preventDefault();
                    $(this).val(qty_before);
                    return;
                }

                if(qty > max_qty) {
                    swal('Maximum quantity limit');
                    event.preventDefault();
                    $(this).val(qty_before);
                    return;
                }

                $(this).attr('data-value', qty);
                $.ajax({
                    type: 'post',
                    data: { 'id': cart_id, 'qty': qty },
                    url: '/cart/update',
                    success: function (responseText) {
                        if (responseText) {
                            if (responseText == "success") {
                                $('#qty_'+item_key).val(qty);
                                $('#pricetotal_'+item_key).html(number_seperator(subtotal));
                                $('#subtotal').myfunction();
                                $('#quantity_check_'+item_key).attr({'orderqty-id': qty});
                            }
                        }
                    }
                });
            });

            $.fn.myfunction = function() {
                var subtotal = 0;
                var platform = {{ ($total->amount->charges->platform == 0)? 0 : $total->amount->charges->platform }};
                var service = {{ ($total->amount->charges->service == 0)? 0 : $total->amount->charges->service }};
                var shipping = {{ ($total->amount->charges->shipping == 0)? 0 : $total->amount->charges->shipping }};
                var total = 0.00;
                $('input.price_total').each(function( index ) {
                    var sub = parseFloat($(this).val());
                    console.log(sub);
                    subtotal = (parseFloat(subtotal) + parseFloat(sub));
                    total = (parseFloat(subtotal) + parseFloat(platform) + parseFloat(service) + parseFloat(shipping));
                });
                $('#subtotal').html("{{ config('app.currency_code') }} " + number_seperator(subtotal));
                $('#total').html("{{ config('app.currency_code') }} " + number_seperator(total));
                return this;
            };

            $('.increase').on('click', function(){
                var item_key = $(this).attr('data-key');
                var cart_id = $('.data-id-'+item_key).attr('data-id');
                var max_qty = $('.data-id-'+item_key).attr('data-max');
                var price = parseFloat($('.data-id-'+item_key).attr('data-price'));
                var qty = parseInt($('#qty_'+item_key).val())+1;
                var subtotal = parseFloat(price * qty);

                if(qty <= max_qty) {
                    $('.data-id-'+item_key).attr('data-value', qty);
                    $.ajax({
                        type: 'post',
                        data: { 'id': cart_id, 'qty': qty },
                        url: '/cart/update',
                        success: function (responseText) {
                            if (responseText) {
                                if (responseText == "success") {
                                    $('#qty_'+item_key).val(qty);
                                    $('#pricetotal_'+item_key).html("{{ config('app.currency_code') }} " + number_seperator(subtotal));
                                    $('#pricetotal_input_'+item_key).val(subtotal);
                                    $('#subtotal').myfunction();
                                    $('#quantity_check_'+item_key).attr({'orderqty-id': qty});
                                }
                            }
                        }
                    });
                } else {
                    swal('Maximum quantity limit');
                }
            });

            $('.decrease').on('click', function(){
                var item_key = $(this).attr('data-key');
                var cart_id = $('.data-id-'+item_key).attr('data-id');
                var price = parseFloat($('.data-id-'+item_key).attr('data-price'));
                var qty = parseInt($('#qty_'+item_key).val())-1;
                var subtotal = parseFloat(price * qty);
                var min_qty = $('.data-id-'+item_key).attr('data-min');

                if(qty >= min_qty) {
                    $('.data-id-'+item_key).attr('data-value', qty);
                    $.ajax({
                        type: 'post',
                        data: { 'id': cart_id, 'qty': qty },
                        url: '/cart/update',
                        success: function (responseText) {
                            if (responseText) {
                                if (responseText == "success") {
                                    $('#qty_'+item_key).val(qty);
                                    $('#pricetotal_'+item_key).html("{{ config('app.currency_code') }} " + number_seperator(subtotal) );
                                    $('#pricetotal_input_'+item_key).val(subtotal);
                                    $('#subtotal').myfunction();
                                    $('#quantity_check_'+item_key).attr({'orderqty-id': qty});
                                }
                            }
                        }
                    });
                } else {
                    swal('Minimum quantity limit');
                }
            });

            $('.cart_remove').on('click', function(index) {
                var id = $(this).attr('data-id');
                swal({
                    title: "@lang('localize.remove_item_from_cart')",
                    text: "@lang('localize.sure')",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "@lang('localize.remove')",
                    cancelButtonText: "@lang('localize.cancel')",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                }, function(){
                    $.ajax({
                        type: 'post',
                        data: { 'id': id },
                        url: '/cart/delete',
                        success: function (responseText) {
                            if (responseText) {
                                if (responseText == "success") {
                                    window.location.reload(true);
                                }
                            }
                        }
                    });
                });
            });
        });




    </script>








@endsection