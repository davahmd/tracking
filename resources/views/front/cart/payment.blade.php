@extends('layouts.front_master')

@section('title', trans('localize.cart'))

@section('content')
<div class="container">
    <div class="checkout-status">
        <div class="loader loader--style6" title="5">
            <svg version="1.1" id="Layer_1" x="0px" y="0px" width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                <rect x="0" y="13" width="4" height="5" fill="#F50006">
                <animate attributeName="height" attributeType="XML"
                    values="5;21;5"
                    begin="0s" dur="0.6s" repeatCount="indefinite" />
                <animate attributeName="y" attributeType="XML"
                    values="13; 5; 13"
                    begin="0s" dur="0.6s" repeatCount="indefinite" />
                </rect>
                <rect x="10" y="13" width="4" height="5" fill="#333">
                <animate attributeName="height" attributeType="XML"
                    values="5;21;5"
                    begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                <animate attributeName="y" attributeType="XML"
                    values="13; 5; 13"
                    begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                </rect>
                <rect x="20" y="13" width="4" height="5" fill="#F50006">
                <animate attributeName="height" attributeType="XML"
                    values="5;21;5"
                    begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                <animate attributeName="y" attributeType="XML"
                    values="13; 5; 13"
                    begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                </rect>
            </svg>
        </div>
        <h5>@lang("localize.processing_checkout")</h5>
        <p>@lang("localize.redirect_message")</p>
    </div>
</div>

@endsection

@section('script')
<script src="{{env('MIDTRANS_PAYMENT_URL')}}" data-client-key="{{env('MIDTRANS_CLIENT_KEY')}}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        snap.show();
        // SnapToken acquired from previous step
        snap.pay('{{$token}}', {
          // Optional
          onSuccess: function(result){
            ProcessResult(result);
          },
          // Optional
          onPending: function(result){
            /* You may add your own js here, this is just example  document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);*/
            ProcessResult(result);
          },
          // Optional
          onError: function(result){
            /* You may add your own js here, this is just example  document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);*/
            ProcessResult(result);
            paymentCancelled();
          },
          onClose: function(){
            innertext= '<img src="{{ asset("asset/images/icon/icon_checkout_fail.png") }}">'+
                                      '<h5>@lang("localize.checkout_fail")</h5>'+
                                      '<p>@lang("localize.transDetail_failed")</p>'+
                                      '<a href="/account/general/order/detail/{{$transaction_id}}" class="btn btn-red-outline">Check your order detail</a>';
            $('.checkout-status').html(innertext);

            paymentCancelled();
          }
        });
      });

      function paymentCancelled() {
          $.ajax({
              'type': 'POST',
              'data': {
                  '_token': '{{ csrf_token() }}'
              },
              'url': '{{ route('payment.cancelled', $transaction_id) }}'
          });
      }

      function ProcessResult(result)
      {
        var innertext='';
        if(result.status_code=='200')
        {
          innertext= '<img src="{{ asset("asset/images/icon/icon_checkout_success.png") }}">'+
                        '<h5>@lang("localize.checkout_success")</h5>'+
                        '<p>@lang("localize.thanksDetail")</p>'+
                        '<a href="/account/general/order/detail/{{$transaction_id}}" class="btn btn-red-outline">Check your order status</a>';
        }
        else if(result.status_code=='201')
        {
          innertext= '<img src="{{ asset("asset/images/icon/icon_checkout_success.png") }}">'+
                        '<h5>Pending</h5>'+
                        '<p>Your payment if pending for further action.</p>'+
                        '<a href="/account/general/order/detail/{{$transaction_id}}" class="btn btn-red-outline">Check your order status</a>';
        }
        else
        {
          innertext= '<img src="{{ asset("asset/images/icon/icon_checkout_fail.png") }}">'+
                        '<h5>@lang("localize.checkout_fail")</h5>'+
                        '<p>@lang("localize.transDetail_failed")</p>'+
                        '<a href="/account/general/order/detail/{{$transaction_id}}" class="btn btn-red-outline">Check your order detail</a>';
        }
        $('.checkout-status').html(innertext);

      }

          /* $.ajax({
            type:   'post',
                    data: { '_token':'{{ csrf_token() }}',
                            'result': result},
                    url: '{{url('/cart_payment')}}',
                    success: function ( result) {
                      //document.getElementById('result-json').innerHTML = result->messages;

                      var innertext='';
                      if(result.status=='200')
                      {
                        innertext= '<img src="{{ asset("asset/images/icon/icon_checkout_success.png") }}">'+
                                      '<h5>@lang("localize.checkout_success")</h5>'+
                                      '<p>@lang("localize.thanksDetail")</p>'+
                                      '<a href="/account/general/order/detail/{{$transaction_id}}" class="btn btn-red-outline">Check your order status</a>';
                      }
                      else if(result.status=='201')
                      {
                        innertext= '<img src="{{ asset("asset/images/icon/icon_checkout_success.png") }}">'+
                                      '<h5>Pending</h5>'+
                                      '<p>Your payment if pending for further action.</p>'+
                                      '<a href="/account/general/order/detail/{{$transaction_id}}" class="btn btn-red-outline">Check your order status</a>';
                      }
                      else
                      {
                        innertext= '<img src="{{ asset("asset/images/icon/icon_checkout_fail.png") }}">'+
                                      '<h5>@lang("localize.checkout_fail")</h5>'+
                                      '<p>@lang("localize.transDetail_failed")</p>'+
                                      '<a href="/account/general/order/detail/{{$transaction_id}}" class="btn btn-red-outline">Check your order detail</a>';
                      }
                      $('.checkout-status').html(innertext);

                    }
                }); */

    </script>
@endsection