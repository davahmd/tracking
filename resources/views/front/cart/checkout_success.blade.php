@extends('layouts.front_master')

@section('title', trans('localize.cart'))


@section('content')
<div class="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            <div class="main-content-container full">
                <div class="checkout-success">
                    <img src="{{ asset('asset/images/icon/icon_checkout_success.png') }}">
                    <h4>@lang('localize.checkout_success')</h4>
                    <p>@lang('localize.thanksDetail')</p>
                    <a href="/" class="btn btn-red-outline">@lang('localize.conShopping')</a>
                </div>
                <div class="order-top-layer">
                    <div>
                        <h6>Order ID: {{$transaction_id}}</h6>
                        <p>{{$orders[0]->order_date}}</p>
                    </div>
                    <a href="javascript:void(0)" class="btn btn-cancel">@lang('received')</a>
                </div>
                <div class="mr-top-sm"> 
                    @foreach($orders as $c)
                    @php  
                        $attributes = ($c->order_attributes != null)? json_decode($c->order_attributes) : null; 
                    @endphp
                    <div class="order-item">
                        <div class="top-layer">
                            <p>Package by <a href="javascript:void(0)">{{$c->product->store->stor_name}}</a></p>
                        </div>
                        <div class="product-layer">
                            <a href="javascript:void(0)">
                                <div class="product-image-container">
                                    <img class="product-image" src="{{ \Storage::url('product/' . $c->product->pro_mr_id . '/' . $c->product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                                </div>
                            </a>
                            <div class="product-content cart-item-content">
                                <a href="javascript:void(0)"><h6 class="product-title">{{$c->product->pro_title_en}}</h6></a>
                                @if($c->service_ids)
                                    <span class="cart-item-label">Installation</span> <small class="no-shipping">* this item will not be shipped</small>
                                    <div class="service-section">
                                        @foreach($c->services as $service)
                                            <span class="service-item">{{$service->service_name}}</span> 
                                        @endforeach
                                    </div>
                                    @endif
                                <p class="info">
                                    @if($attributes != null)
                                        @foreach($attributes as $key => $value)
                                        <label><b>{{$key}} : </b> {{$value}}</p></label>
                                        @endforeach
                                    @endif
                                </p>
                                <p class="info">
                                    <label>QTY: <span class="red">{{$c->order_qty}}</span></label>
                                    <label>Total: <span class="red">{{rpFormat($c->total_product_price)}}</span></label>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div class="row mr-top-sm">
                    <div class="col-md-6">
                        <div class="delivery-container">
                            <div class="shipping-address">
                                <h5>Shipping Address</h5>
                                <div class="delivery-item">
                                    <i class="fas fa-user-alt"></i>
                                    <p class="info pd-x-sm">{{$shipping->ship_name}}</p>
                                </div>
                                <div class="delivery-item">
                                    <i class="fas fa-map-marker-alt"></i>
                                    <p class="info pd-x-sm">{{ $shipping->getFullAddress() }}</p>
                                </div>
                                <div class="delivery-item">
                                    <i class="fas fa-phone"></i>
                                    <p class="info pd-x-sm">{{$shipping->ship_phone}}</p>
                                </div>
                            </div>
                             
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="total-container border">
                            <h5>Payment Information</h5>
                            <div class="mr-top-sm">
                                <div class="list">
                                    <p>Merchandise Subtotal ({{$orders->count()}}} items)</p><p>{{rpFormat($parent_order->order_amount)}}</p>
                                </div>
                                <div class="list">
                                    <p>Shipping Fee</p><p>{{rpFormat($parent_order->shipping_amount)}}</p>
                                </div>
                                <div class="list">
                                    <p>Payment Method</p><p>Credit Card - CIMB Niaga</p>
                                </div>
                                <div class="list">
                                    <p>Grand Total</p><p class="red">{{rpFormat($parent_order->charged_amount)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<style>
    h3{
        color:#404040;
        font-family: "Open Sans", Microsoft Yahei, sans-serif;
    }
</style>
@endsection