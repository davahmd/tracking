@extends('layouts.front_master')

@section('title', trans('localize.cart'))


@section('content')

<div class="container">
    <div class="checkout-fail">
        <img src="{{ asset('asset/images/icon/icon_checkout_fail.png') }}">
        <h5>@lang('localize.checkout_fail')</h5>
        <p>@lang('localize.transDetail_failed')</p>
        <a href="/cart/checkout" class="btn btn-red-outline">@lang('localize.return_checkout')</a>
    </div>
</div>

@endsection
