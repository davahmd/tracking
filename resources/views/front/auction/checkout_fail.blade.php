@extends('layouts.master')

@section('content')
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <a class="home" href="javascript:void(0)" title="Return to Home">@lang('localize.home')</a>
                <span class="navigation-pipe">&nbsp;</span>
                <a href="javascript:void(0)">@lang('localize.bid')</a>
                <span class="navigation-pipe">&nbsp;</span>
                <span class="navigation_page">@lang('localize.checkout_result')</span>
            </div>
            <!-- ./breadcrumb -->
        <div class="general2">
            <!-- page heading-->
            <h2 class="page-heading no-line">
                <span class="page-heading-title2">@lang('localize.checkout_result')</span>
            </h2>
            <!-- ../page heading-->
            <div class="page-content page-order">
                <div class="order-fail-content">
                    <div class="box-border">
                        <h3>@lang('localize.transDetail')</h3>
                        <p>@lang('localize.transDetail_failed')</p>
                        <div class="cart_navigation">
                            <a class="prev-btn" href="/auctions/checkout/{{$auc_id}}">@lang('localize.return_checkout')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('script')
@endsection
