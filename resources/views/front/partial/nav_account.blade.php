<div class="sidebar-container">
    <div class="title-box d-flex align-items-center">
        <div class="user-logo-frame d-flex align-items-center">
            @if(auth()->check())
            <img class="user-image" src="{{ ($cus_details->cus_pic != '') ? Storage::url('gallery/customer/'. $cus_details->cus_pic):'/common/images/stock.png' }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
            @endif
        </div>
        <h6 class="user-name">{{ auth()->check()?$cus_details->cus_name:'Guest' }}</h6>
    </div>
    <div class="menu-box">
        <ul class="list-group list-group-flush">
            @if(auth()->check())
            <li class="list-group-item list-general"><a href="javascript:void(0)"><span>@lang('front.nav.account.general_info')</span></a>
                <ul class="list-group list-group-flush sub-list collapse collapse-general {{ request()->is('account/general*')?'show':'' }}">
                    <li class="list-group-item"><a href="{{ route('overview') }}" class="{{ request()->is('account/general/overview')?'active':'' }}"><span>@lang('front.nav.account.general.overview')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('notification') }}" class="{{ request()->is('account/general/notification')?'active':'' }}"><span>@lang('front.nav.account.general.notification')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('chat-list') }}" class="{{ request()->is('account/general/chat*')?'active':'' }}"><span>@lang('front.nav.account.general.chat')</span></a></li>
                    {{-- <li class="list-group-item"><a href="{{ route('favourite') }}" class="{{ request()->is('account/general/favourite*')?'active':'' }}"><span>@lang('front.nav.account.general.favorites')</span></a></li> --}}
                    <li class="list-group-item"><a href="{{ route('automobile') }}" class="{{ request()->is('account/general/automobile*')?'active':'' }}"><span>@lang('front.nav.account.general.automobile')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('order-history') }}" class="{{ request()->routeIs('order-history')?'active':'' }}"><span>@lang('front.nav.account.general.order')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('order-history-wholesale') }}" class="{{ request()->routeIs('order-history-wholesale')?'active':'' }}"><span>@lang('front.nav.account.general.wholesale-order')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('schedule') }}" class="{{ request()->is('account/general/schedule*')?'active':'' }}"><span>@lang('front.nav.account.general.schedule')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('nego-history') }}" class="{{ request()->routeIs('nego-history') ? 'active' : '' }}"><span>@lang('front.nav.account.general.negotiation')</span></a></li>
                </ul>
            </li>
            <li class="list-group-item list-profile"><a href="javascript:void(0)">@lang('localize.myprofile')</a>
                <ul class="list-group list-group-flush sub-list collapse collapse-profile {{ request()->is('account/profile*')?'show':'' }}">
                    <li class="list-group-item"><a href="{{ route('profile') }}" class="{{ request()->is('account/profile/details*')?'active':'' }}"><span>@lang('front.nav.account.profile')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('delivery') }}" class="{{ request()->is('account/profile/delivery')?'active':'' }}"><span>@lang('front.nav.account.delivery')</span></a></li>
                    {{--<li class="list-group-item"><a href="{{ route('payment') }}" class="{{ request()->is('account/profile/payment')?'active':'' }}"><span>Payment Method</span></a></li>--}}
                </ul>
            </li>
            <li class="list-group-item list-setting"><a href="javascript:void(0)">@lang('localize.Settings')</a>
                <ul class="list-group list-group-flush sub-list collapse collapse-setting {{ request()->is('account/setting*')?'show':'' }}">
                    <li class="list-group-item"><a href="{{ route('change-password') }}" class="{{ request()->is('account/setting/password')?'active':'' }}"><span>@lang('front.nav.account.setting.change_pw')</span></a></li>
                    <!-- <li class="list-group-item"><a href="{{ route('notification-setting') }}" class="{{ request()->is('account/setting/notification')?'active':'' }}"><span>Notification Setting</span></a></li>
                    <li class="list-group-item"><a href="{{ route('chat-setting') }}" class="{{ request()->is('account/setting/chat')?'active':'' }}"><span>Chat Setting</span></a></li> -->
                    {{--<li class="list-group-item"><a href="{{ route('language') }}" class="{{ request()->is('account/setting/language')?'active':'' }}"><span>Language</span></a></li>--}}
                </ul>
            </li>
            @endif
            <li class="list-group-item list-support"><a href="javascript:void(0)">@lang('front.nav.account.support')</a>
                <ul class="list-group list-group-flush sub-list collapse collapse-support {{ request()->is('account/support*')?'show':'' }}">
                    <li class="list-group-item"><a href="{{ route('help') }}" class="{{ request()->is('account/support/help')?'active':'' }}"><span>@lang('front.nav.account.help')</span></a></li>
                    <li class="list-group-item"><a href="{{ route('faq') }}" class="{{ request()->is('account/support/faq')?'active':'' }}"><span>@lang('front.nav.account.faq')</span></a></li>
                </ul>
            </li>
            @if(auth()->check())
            <li class="list-group-item logout"><a href="{{ route('logout') }}">@lang('localize.logout')</a></li>
            @endif
        </ul>
    </div>
</div>