<div class="list">
    <div class="username">{{ $rating->customer->cus_name }}</div>
    <div class="date">{{ \Helper::UTCtoTZ($rating->created_at) }}</div>
    <div class="stars">
        <div class="star-foreground" style="width: {{ $rating->percentage }}%"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
        <div class="star-background"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
    </div>

    @if(isset($toggleDisplay))
    <p>
        <div class="switch pull-right">
            <i class="fa fa-eye"></i> @lang('localize.show')
            <div class="onoffswitch">
                <input type="checkbox" {{ $rating->display ? 'checked' : '' }} class="onoffswitch-checkbox" id="toggle-{{ $rating->id }}" onchange="toggleDisplay(this, {{ $rating->id }}, '{{ $userType }}')">
                <label class="onoffswitch-label" for="toggle-{{ $rating->id }}">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>
        </div>
    </p>
    @endif

    <p style="min-height:30px;">
        {{ $rating->review }}
    </p>

    @if (!$rating->images->isEmpty())
    <p>
        @foreach ($rating->images as $image)
        <a href="{{ $image->imageLink }}" data-gallery="">
            <img src="{{ $image->imageLink }}" onerror="this.onerror=null;this.src='/common/images/stock.png';" class="img-responsive img-thumbnail" style="height:50px; width:auto;">
        </a>

        {{-- <img src="{{ $image->imageLink }}" onerror="this.onerror=null;this.src='/common/images/stock.png';" class="img img-thumbnail" style="height:50px; width:auto;"> --}}
        @endforeach
    </p>
    @endif
</div>