<div class="rate-bar">
    <div class="background"></div>
    <div class="foreground" style="width:{{ $rating->percentage }}%;"></div>
</div>
<label>{{ $rating->total }}</label>