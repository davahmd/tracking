<div class="stars">
    <div class="star-foreground" style="width: {{ $percentage or $rating->percentage }}%"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
    <div class="star-background"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
</div>