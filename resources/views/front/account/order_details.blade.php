@extends('layouts.front_master')

@section('title', trans('front.nav.account.general.order'))

@section('content')
<div id="main">
    <div class="container">
        @include('layouts.partials.status')
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            <div class="main-content-container">
                @foreach($order_details as $order_detail => $values)
                <div class="order-top-layer shadow-sm mr-btm-sm">
                    <div>
                    <h6>@lang('localize.order_id'): {{$values[0]['parent_order_id']}}</h6>
                        <p>{{$values[0]['order_date']}}</p>
                    </div>
                    {{-- <a href="javascript:void(0)" class="btn btn-complaint">Make Complaint</a> --}}
                </div>
                <div class="mr-top-sm">
                    @foreach($values as $value)
                    {{-- @if($value->order_status == 3) --}}
                    <div class="order-item">
                        <div style="padding: 10px 15px;">
                            <div class="top-layer">
                            <p>@lang('localize.package_by') <a href="javascript:void(0)">{{$value->name}}</a></p>
                            <p>@lang('localize.tracking_number'): {{$value->order_tracking_no}}</p>
                            </div>
                            <div class="product-layer">
                                <div class="d-flex">
                                    <a href="javascript:void(0)"><img class="product-image" src="../images/product/3.png" /></a>
                                    <div class="product-content">
                                        <a href="javascript:void(0)">
                                            <h6 class="product-title">
                                                {{$value->pro_title_en}}
                                            </h6>
                                        </a>
                                        <p class="info">
                                            @lang('localize.quantity'): <span class="red">{{$value->order_qty}}</span> | @lang('localize.total'): <span class="red">@lang('localize.rp') {{$value->product_price}}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btm-layer">
                            <div class="d-flex align-items-center">
                                <p class="info">@lang('localize.status'):</p>
                                @if($value->order_status == 1 && $value->payer_status == 0)
                                    <span class="progress-status status-red">@lang('localize.awaiting_payment')</span>
                                @elseif($value->order_status == 1 && $value->payer_status == 1)
                                    <span class="progress-status status-black">@lang('localize.awaiting_merchant_confirmation')</span>
                                @elseif($value->order_status == 2)
                                    <span class="progress-status status-blue">@lang('localize.on_packaging')</span>
                                @elseif($value->order_status == 3)
                                    <span class="progress-status status-blue">@lang('localize.on_delivery')</span>
                                @elseif($value->order_status == 4)
                                    <span class="progress-status status-green">@lang('localize.completed')</span>
                                @elseif($value->order_status == 5)
                                    <span class="progress-status status-red">@lang('localize.cancelled')</span>
                                @endif
                                {{-- <p class="info">Estimate Delivered: 7 Jul - 9 Jul</p> --}}
                            </div>
                            {{-- <a class="btn-log" data-toggle="collapse" href="#collapseOne">
                                Log<i class="material-icons">keyboard_arrow_down</i>
                            </a> --}}
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion" style="border-top: 1px solid rgba(0,0,0,0.1);">
                            <div class="card-body">
                                <ul>
                                    <li>21-11-2017 15:39 Item delivered to NICHOLAS YAP</li>
                                    <li>21-11-2017 09:51 Item out for delivery</li>
                                    <li>21-11-2017 05:52 Arrive at delivery facility at</li>
                                    <li>21-11-2017 01:38 Consignment dispatch out from Transit Office</li>
                                    <li>20-11-2017 21:30 Item processed</li>
                                    <li>20-11-2017 17:25 Item dispatched out</li>
                                    <li>20-11-2017 17:23 Item dispatched out</li>
                                    <li>20-11-2017 17:19 Item picked up</li>
                                    <li>19-11-2017 00:54 Order details sent to seller</li>
                                    <li>19-11-2017 00:53 Order created</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    {{-- @endif --}}
                    @endforeach
                </div>
                @endforeach

                <div class="row mr-top-sm">
                    <div class="col-sm-6">
                        <div class="delivery-container">
                            <div class="shipping-address">
                                <h5>@lang('localize.shipping_address')</h5>
                                <div class="delivery-item">
                                    <i class="material-icons">person</i>
                                <p class="info pd-x-sm">{{$shipping_details->ship_name}}</p>
                                </div>
                                <div class="delivery-item">
                                    <i class="material-icons">place</i>
                                <p class="info pd-x-sm">{{$shipping_address}}</p>
                                </div>
                                <div class="delivery-item">
                                    <i class="material-icons">call</i>
                                <p class="info pd-x-sm">{{$shipping_details->ship_phone}}</p>
                                </div>
                            </div>
                            <div class="billing-address">
                                <h5>@lang('localize.billing_address')</h5>
                                <div class="delivery-item">
                                    <i class="material-icons">person</i>
                                    <p class="info pd-x-sm">{{$shipping_details->ship_name}}</p>
                                </div>
                                <div class="delivery-item">
                                    <i class="material-icons">place</i>
                                    <p class="info pd-x-sm">{{$shipping_address}}</p>
                                </div>
                                <div class="delivery-item">
                                    <i class="material-icons">call</i>
                                    <p class="info pd-x-sm">{{$shipping_details->ship_phone}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="total-container border">
                            <h5>@lang('localize.payment_information')</h5>
                            <div class="mr-top-sm">
                                <div class="d-flex justify-content-between">
                                <p>@lang('localize.merchandise_subtotal') ({{$payment_details->total_items}} @lang('localize.items'))</p><p>@lang('localize.rp') {{$payment_details->sub_total}}</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                <p>@lang('localize.shipping_fees')</p><p>@lang('localize.rp') {{$payment_details->shipping_fees}}</p>
                                </div>
                                {{-- <div class="d-flex justify-content-between">
                                    <p>Promo Discount Fee</p><p>-Rp 10.000</p>
                                </div> --}}
                                <div class="d-flex justify-content-between">
                                    @if($payment_method == 1)
                                        <p>@lang('localize.payment_method')</p><p>@lang('localize.vcredit')</p>
                                    @else
                                        <p>@lang('localize.payment_method')</p><p>@lang('localize.cash')</p>
                                    @endif
                                </div>
                                <div class="d-flex justify-content-between">
                                <p>@lang('localize.grand_total')</p><p class="red">@lang('localize.rp') {{number_format(($payment_details->sub_total - $payment_details->shipping_fees), 2)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
<link href="/asset/css/dashboard.css" rel="stylesheet">
<link href="/asset/css/default.css?v2" rel="stylesheet">
<link href="/asset/css/dropdown_bs4.css" rel="stylesheet">
<link href="/asset/css/master.css" rel="stylesheet">
<link href="/asset/css/custom_select.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endsection

@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="/asset/js/dropdown_bs4.js"></script>
<script src="/asset/js/custom_select.j"></script>
<script src="/asset/js/zona.app.js"></script>
@endsection