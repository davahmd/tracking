@extends('layouts.front_master')

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
                <div class="account-container bg-white border pd-md">
                    <div class="account-titlebar d-flex align-items-center">
                        <h5 class="account-title">Chat Setting</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection