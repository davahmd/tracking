@extends('layouts.front_master')

@section('title', trans('front.nav.account.setting.change_pw'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')

            <div class="main-content-container">
                @include('layouts.partials.status')
                <div class="account-container bg-white border pd-md">
                    <div class="account-titlebar d-flex align-items-center">
                        <h5 class="account-title">{{trans('localize.change_password')}}</h5>
                    </div>
                    <form action="{{ url('account/setting/password') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row mr-top-md">
                            <div class="col-md-6">
                                <div class="form-item">
                                    <label>{{trans('localize.old_password')}}</label>
                                    <div class="custom-input">
                                        <input type="password" name="old_password" />
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label>{{trans('localize.new_password')}}</label>
                                    <div class="custom-input">
                                        <input type="password" name="password" />
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label>{{trans('localize.confirm_password')}}</label>
                                    <div class="custom-input">
                                        <input type="password" name="password_confirmation" />
                                    </div>
                                </div>

                                <div class="form-action">
                                    <button type="submit" class="btn btn-red">{{trans('localize.save')}} <i class="fas fa-pencil-alt"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection