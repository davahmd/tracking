@extends('layouts.front_master')

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
                <div class="account-container bg-white border pd-md">
                    <div class="account-titlebar d-flex align-items-center">
                        <h5 class="account-title">Select Language</h5>
                    </div>
                    
                    <form>
                    <div class="mr-top-md">
                        <div class="form-check-inline">
                            <div class="input-group mb-3">
                                <div class="input-group-text">
                                    <label class="form-check-label" for="radio1">
                                        <input type="radio" id="radio1" name="optradio" value="option1" checked> Bahasa Indonesia
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-check-inline">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <label class="form-check-label" for="radio2">
                                        <input type="radio" id="radio2" name="optradio" value="option2"> English (UK)
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-check-inline">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <label class="form-check-label" for="radio3">
                                        <input type="radio" id="radio3" name="optradio" value="option3"> English (US)
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-check-inline">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <label class="form-check-label" for="radio4">
                                        <input type="radio" id="radio4" name="optradio" value="option4"> Espanol
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-check-inline">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <label class="form-check-label" for="radio5">
                                        <input type="radio" id="radio5" name="optradio" value="option5"> Dutch
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-action">
                        <button type="submit" class="btn btn-red">Save&ensp;<span><i class="fa fa-save"></i></span></button>
                    </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection