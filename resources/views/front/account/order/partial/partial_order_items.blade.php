<div id="contents" class="tab-content">
    <div id="{{ $type }}" class="tab-pane active">
        <div class="mr-top-sm">
            @foreach($parent_orders as $parent_order => $values)
                @if($parent_order == $tab_index)
                @foreach($values->sortByDesc('parent_order_id')->groupBy('parent_order_id') as $value => $items)
                <div class="order-item">
                    <div class="top-layer">
                        <p class="order-id">@lang('localize.transaction_id') : {{ $items->first()->transaction_id }}</p>
                        <p>{{ $items[0]['order_date'] }}</p>
                        <p><a class="btn" style="font-size: 11px;" href="{{ route('order-detail', [$items[0]->transaction_id]) }}">@lang('localize.View_Order_Details')</a></p>
                    </div>
                    @foreach($items as $item)
                        <div class="product-layer">
                            <div class="inner">
                                <a class="thumb" target="_blank" href="/product/{{ Helper::slug_maker($item->product->pro_title_en, $item->product->pro_id) }}">
                                    <img class="product-image" src="{{ $item->product->mainImageUrl() }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';" />
                                </a>
                                <div class="product-content">
                                    <a target="_blank" href="/product/{{ Helper::slug_maker($item->product->pro_title_en, $item->product->pro_id) }}">
                                        <h6 class="product-title">{{$item->product->title}}</h6>
                                    </a>

                                    <!-- Attributes -->
                                    @php
                                        $attributes = ($item->order_attributes != null) ? json_decode($item->order_attributes) : null;
                                    @endphp

                                    @if ($attributes != null)
                                        @foreach($attributes as $key2 => $value2)
                                            <p class="info">
                                                {{$key2}} : <span class="red">{{$value2}}</span>
                                            </p>
                                        @endforeach
                                    @endif

                                    <!-- Quantity -->
                                    <p class="info">
                                        @lang('localize.quantity') : <span class="red">{{$item->order_qty}}</span></span>
                                    </p>

                                    <!-- Price -->
                                    <p class="info">
                                        @lang('localize.total') : <span class="red">{{rpFormat($item->order_value)}}</span>
                                    </p>

                                    @if ($item->services->count() > 0)
                                    <div class="service-info">
                                        <label>@lang('localize.service_name') :</label>
                                        <ul>
                                        @foreach ($item->services as $service)
                                            <li>
                                                <a href="{{route('member-schedule-service', [$service->id])}}">
                                                <span class="red">{{$service->service_name_current}}</span>
                                                <span class="btn" >@lang('localize.schedule_service')</span>
                                                </a>
                                            </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="text-right ml-auto">
                                <!-- Order Status -->
                                @if ($item->order_status == 0)
                                    <span class="progress-status status-red">@lang('localize.awaiting_payment')</span>
                                @elseif ($item->order_status == 1)
                                    <span class="progress-status status-black">@lang('localize.awaiting_merchant_confirmation')</span>
                                @elseif ($item->order_status == 2)
                                    <span class="progress-status status-blue">@lang('localize.packaging')</span>
                                @elseif ($item->order_status == 3)
                                    <span class="progress-status status-blue">@lang('localize.on_delivery')</span>
                                @elseif ($item->order_status == 4)
                                    <span class="progress-status status-green">@lang('localize.completed')</span>
                                @elseif ($item->order_status == 5)
                                    <span class="progress-status status-green">@lang('localize.cancelled')</span>
                                @elseif ($item->order_status == 7)
                                    <span class="progress-status status-blue">@lang('localize.installation')</span>
                                @else
                                    <span class="progress-status status-black">Undefined Status</span>
                                @endif
                            </div>
                            @php
                                $dateExps = DB::table('nm_order')
                                ->select(DB::raw('TIMESTAMPDIFF(HOUR, updated_at, now()) as Hour '))
                                ->where('order_status', 4)
                                ->where('order_id', $item->order_id)
                                ->get();
                            @endphp
                            <div class="schedule-btn">
                                @if ($item->order_status == 3)
                                    <a class="btn" href="{{route('order-update-status', [$item->order_id]) }}">@lang('localize.package_received')</a>
                                    <a class="btn" href="{{route('complain::create-complain', [$item->order_id]) }}">@lang('localize.complain.complain')</a>    
                                @elseif ($item->order_status == 4)
                                    @foreach ($dateExps as $dateExp)
                                        @if ($dateExp->Hour < 72)
                                            <a class="btn" href="{{route('complain::create-complain', [$item->order_id]) }}">@lang('localize.complain.complain')</a>    
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                @endforeach
                @endif
            @endforeach
            {{ $parent_orders[$tab_index]->links() }}
        </div>
    </div>
</div>