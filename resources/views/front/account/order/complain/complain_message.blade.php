@extends('layouts.front_master')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/blueimp/css/blueimp-gallery.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/widget/complain.css') }}" />
@endsection

@section('content')
    <div class="main">
        <div class="container">
            <div class="account-dashboard-container d-flex pd-y-md">

                @include('front.partial.nav_account')

                <div class="main-content-container full">

                    @include('layouts.partials.status')

                    @include('shared.complain.complain', [
                        'messages' => $messages,
                        'order' => $order,
                        'send_url' => '/account/complains/' . $order->order_id . '/messages/' . $complain->id,
                        'escalate_url' => '/account/complains/' . $order->order_id . '/escalate/' . $complain->id,
                        'is_admin' => false,
                        'is_merchant' => false,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.web.translation_script');

@section('script')
    <script src="{{ asset('backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('common/js/widget/complain.js') }}"></script>
@endsection