@extends('layouts.front_master')

@section('style')
    <style type="text/css">
        .kv-file-upload, .kv-file-zoom {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="main">
        <div class="container">
            <div class="account-dashboard-container d-flex pd-y-md">

                @include('front.partial.nav_account')

                <div class="main-content-container full">

                    @include('layouts.partials.status')

                    <div class="complain-container bg-white border pd-md">
                        <div class="complain-titlebar d-flex align-items-center">
                            <h5>@lang('localize.complain.form.add_heading')</h5>
                        </div>

                        <form action="{{ route('complain::post-create-complain', [$order->getKey()]) }}" method="POST" enctype="multipart/form-data" id="form-complain">
                            {{ csrf_field() }}

                            <input type="hidden" value="1" name="is_creator" />

                            <div class="row mr-top-md">
                                <div class="col-md-12">
                                    <div class="form-item">
                                        <label>@lang('localize.complain.form.title')</label>
                                        <div class="custom-input">
                                            <?php
                                            $field = 'subject_id';
                                            $name = $field;
                                            $placeholder = trans('localize.complain.form.title_placeholder');
                                            ?>
                                            <select class="form-control {{$name}}" name="{{$name}}">
                                                <option value="">{{$placeholder}}</option>
                                                @foreach ($subjects as $subject)
                                                    <option value="{{ $subject->getKey() }}" {{ (old($name) == $subject->getKey()) ? 'selected' : '' }}>{{ $subject->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!--<div class="form-item">
                                        <label>@lang('localize.complain.form.related_order')</label>
                                        <div class="custom-input">
                                            <?php
                                            /*$field = 'nm_order_id';
                                            $name = $field;
                                            $placeholder = trans('localize.complain.form.related_order_placeholder');*/
                                            ?>
                                            <select class="form-control {{$name}}" name="{{$name}}">
                                                <option value="">{{$placeholder}}</option>
                                                {{--@foreach ($orders as $order)--}}
                                                    {{--<option value="{{ $order->getKey() }}" {{ (old($name) == $order->getKey()) ? 'selected' : '' }}>{{ $order->product->title }}</option>--}}
                                                {{--@endforeach--}}
                                            </select>
                                        </div>
                                    </div>-->

                                    <div class="form-item">
                                        <label>@lang('localize.complain.form.message')</label>
                                        <div class="custom-input">
                                            <?php
                                            $field = 'message';
                                            $name = $field;
                                            $placeholder = 'Enter Message';
                                            ?>
                                            <textarea name="{{$name}}" class="form-control {{$name}}">{{old($name)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-item">
                                        <label>@lang('localize.complain.form.images')</label>
                                        {{--<div class="custom-input">--}}
                                            <input id="file-complain" type="file" accept="image/*" name="attachments[]" multiple>
                                        {{--</div>--}}
                                        {{--<input type="file" accept="image/*" name="attachments[]" multiple id="attachments" data-url="/upload">--}}
                                        {{--</div>--}}
                                    </div>

                                    <div class="form-action">
                                        {{--<button class="btn btn-red" type="button" onclick="submit();">@lang('localize.complain.form.btn_add')</button>--}}
                                        <button class="btn btn-red" type="submit" id="submit-btn-complain">@lang('localize.complain.form.btn_add')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{  asset('common/lib/vendor/bootstrap-file-input/themes/fa/theme.js') }}"></script>
<script>
    $("#file-complain").fileinput({
        theme: "fa",
        // uploadUrl: "/file-upload-batch/2",
        showUpload: false,
        maxCount: 5,
        initialPreviewShowDelete: true,
        initialPreviewShowUpload: false,
        browseOnZoneClick: false,
        showBrowse: true,
        showUploadedThumbs: false,
    });

    // Dropzone.options.myDropzone = {
    //     url: complainForm.attr('action'),
    //     autoProcessQueue: false,
    //     uploadMultiple: true,
    //     parallelUploads: 6,
    //     maxFiles: 6,
    //     acceptedFiles: "image/*",
    //     paramName: "attachments",
    //
    //     init: function () {
    //
    //         var submitButton = document.querySelector("#submit-btn-complain");
    //         var wrapperThis = this;
    //
    //         submitButton.addEventListener("click", function () {
    //             wrapperThis.processQueue();
    //         });
    //
    //         this.on("addedfile", function (file) {
    //
    //             // Create the remove button
    //             var removeButton = Dropzone.createElement("<button class='btn btn-lg dark'>Remove File</button>");
    //
    //             // Listen to the click event
    //             removeButton.addEventListener("click", function (e) {
    //                 // Make sure the button click doesn't submit the form:
    //                 e.preventDefault();
    //                 e.stopPropagation();
    //
    //                 // Remove the file preview.
    //                 wrapperThis.removeFile(file);
    //                 // If you want to the delete the file on the server as well,
    //                 // you can do the AJAX request here.
    //             });
    //
    //             // Add the button to the file preview element.
    //             file.previewElement.appendChild(removeButton);
    //         });
    //
    //         this.on('sendingmultiple', function (data, xhr, formData) {
    //             formData.append("_token", csrfToken);
    //             formData.append("subject_id", $('.subject_id').val());
    //             formData.append("message", $('.message').val());
    //         });
    //
    //         // this.on('success', function(a, b, c) {
    //         //     console.log(a, b, c)
    //         // });
    //
    //         this.on('error', function(a, response, c) {
    //
    //             var alert = $('.alert-danger');
    //             var ul = alert.find('ul');
    //
    //             ul.html('');
    //
    //             if (typeof response !== 'string') {
    //                 for (var elem in response) {
    //                     if (response[elem].length === 1) {
    //                         ul.append('<li>' + response[elem] + '</li>');
    //                     } else {
    //                         for (var i = 0; i < response[elem].length; i++) {
    //                             ul.append('<li>' + response[elem][i] + '</li>');
    //                         }
    //                     }
    //
    //                 }
    //             } else {
    //                 ul.append('<li>' + response + '</li>');
    //             }
    //
    //
    //             alert.css('display', 'block');
    //
    //             this.removeFile(a);
    //         });
    //
    //         this.on('completemultiple', function() {
    //             submitButton.addEventListener("click", function () {
    //                 wrapperThis.processQueue();
    //             });
    //         })
    //     }
    // };
    // $(document).ready(function() {
    //     $('#file').imageuploadify();
    // });
    // function submit()
    // {
    //     $('#form-complain').submit();
    // }


</script>
@endsection