@extends('layouts.front_master')
@section('content')
<div class="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">

            @include('front.partial.nav_account')

            <div class="main-content-container full">

                @include('layouts.partials.status')

                <div class="order-item">
                    @if($type == 'store')
                    <div class="product-layer">
                        <a target="_blank" href="{{ $order->product->store->slugUrl }}">
                            <div class="product-image-container">
                                <img class="product-image" src="{{ $order->product->store->getImageUrl() }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                            </div>
                        </a>
                        <div class="product-content cart-item-content">
                            <small>@lang('localize.package_by')</small><br>
                            <a target="_blank" href="{{ $order->product->store->slugUrl }}"><h6 class="product-title">{{ $order->product->store->stor_name }}</h6></a>
                        </div>
                    </div>
                    @endif

                    @if($type == 'product')
                    <div class="top-layer">
                        <p>@lang('localize.package_by') <a target="_blank" href="{{ $order->product->store->slugUrl }}">{{ $order->product->store->stor_name }}</a></p>
                    </div>
                    <div class="product-layer">
                        <a target="_blank" href="{{ $order->product->slugUrl }}">
                            <div class="product-image-container">
                                <img class="product-image" src="{{ $order->product->mainImageUrl() }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                            </div>
                        </a>
                        <div class="product-content cart-item-content">
                            <a target="_blank" href="{{ $order->product->slugUrl }}"><h6 class="product-title">{{ $order->product->title }}</h6></a>
                            <p class="info"></p>
                            <p class="info">
                                <label>@lang('localize.quantity'): <span class="red">1</span></label>
                                <label>@lang('localize.total'): <span class="red">{{rpFormat($order->total_product_price)}}</span></label>
                            </p>
                        </div>
                    </div>
                    @endif
                </div>

                <form method="POST" action="{{ route('review-create-submit', [$type, $order->order_id]) }}" enctype="multipart/form-data" id="reviewForm">
                    {{ csrf_field() }}

                    <div class="review-form">
                        @if($type == 'product')
                        <div class="title">@lang('localize.REVIEW.product')</div>
                        @endif

                        @if($type == 'store')
                        <div class="title">@lang('localize.REVIEW.store')</div>
                        @endif

                        <div class="rating">
                            <div class="form-group">
                                <label class="control-label">@lang('localize.RATING.your')</label>
                                <div class="rating-radio-btn">
                                    @for ($i = 1; $i <= 5; $i++)
                                    <label>
                                        <input type="radio" name="rating" value="{{ $i }}" {{ old('rating', 5) == $i ? 'checked' : '' }}/>
                                        @for ($b = 1; $b <= $i; $b++)
                                        <span class="icon">★</span>
                                        @endfor
                                        {{-- <label class="rate-number">0.0/5</label> --}}
                                    </label>
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="review">
                            <div class="form-group">
                                <label class="control-label">@lang('localize.REVIEW.your')</label>
                                <textarea name="review" class="form-control" rows="3"></textarea>

                                {{--<input id="file" type="file" accept="image/*" name="images[]" multiple>--}}
                            </div>

                            <div class="action">
                                <button class="btn btn-red" type="button" onclick="submit();">@lang('localize.review')</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
.file-drop-zone-title{
    color: #aaa;
    font-size: 1.6em;
    padding: 30px 10px;
    cursor: default;
}
.btn-cancel {
    color: #ff1818;
    border-color: #ff1818;
    background: #fff;
}
.krajee-default.file-preview-frame .kv-file-content {
    width: auto;
    height: 100px;
}
button.btn.fileinput-upload-button
{
    display: none !important;
}
</style>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('asset/js/imageuploadify.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#file').imageuploadify();
});
function submit()
{
    $('#reviewForm').submit();
}
</script>
@endsection