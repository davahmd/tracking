@extends('layouts.front_master')
@section('content')
<div class="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
        @include('front.partial.nav_account')
            <div class="main-content-container full">
                <div class="order-top-layer">
                    <div>
                        <h6>@lang('localize.transaction_id') : {{$parent_order->transaction_id}}</h6>
                        <p>{{ \Helper::UTCtoTZ($parent_order->items->pluck('order_date')->first()) }}</p>
                    </div>
                    {{-- <a href="javascript:void(0)" class="btn btn-cancel">@lang('received')</a> --}}
                </div>
                <div class="mr-top-sm">
                    @foreach($parent_order->items->groupBy('product.store.stor_id') as $items)
                    @php
                        $c = $items->first();
                        $attributes = ($c->order_attributes != null)? json_decode($c->order_attributes) : null;
                    @endphp

                    <div class="order-item">
                        <div class="top-layer">
                            <p>@lang('localize.package_by') <a target="_blank" href="/store/{{ $c->product->store->url_slug }}">{{$c->product->store->stor_name}}</a></p>
                            @unless($parent_order->wholesale)
                                @if($c->rateEligibility->store)
                                    <a class="text-danger" href="{{ route('review-create', ['store', $c->order_id]) }}"><i class="fa fa-star"></i> @lang('localize.REVIEW.store')</a>
                                @else
                                    @if($c->storeRating)
                                    <span class="card-rating">
                                        @include('front.partial.star.tag', ['rating' => $c->storeRating])
                                    </span>
                                    @endif
                                @endif
                            @endunless
                        </div>

                        @foreach ($items as $c)
                        <div class="product-layer">
                            <a target="_blank" href="/product/{{ Helper::slug_maker($c->product->pro_title_en, $c->product->pro_id) }}">
                                <div class="product-image-container">
                                    <img class="product-image" src="{{ \Storage::disk('s3')->url('product/' . $c->product->pro_mr_id . '/' . $c->product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                                </div>
                            </a>

                            <div class="product-content cart-item-content">
                                <a target="_blank" href="/product/{{ Helper::slug_maker($c->product->pro_title_en, $c->product->pro_id) }}"><h6 class="product-title">{{$c->product->pro_title_en}}</h6></a>
                                @if($c->service_ids)
                                    <span class="cart-item-label">Installation</span> <small class="no-shipping">* this item will not be shipped</small>
                                    <div class="service-section">
                                        @foreach($c->services as $service)
                                            <span class="service-item">{{$service->service_name_current}}</span>
                                        @endforeach
                                    </div>
                                @endif

                                <p class="info">
                                    @if($attributes != null)
                                        @foreach($attributes as $key => $value)
                                        <label><b>{{$key}} : </b> {{$value}}</p></label>
                                        @endforeach
                                    @endif
                                </p>

                                <p class="info">
                                    <label>@lang('localize.status'):
                                        
                                    @if ($c->order_status == 0)
                                        <span class="progress-status status-red">@lang('localize.awaiting_payment')</span>
                                    @elseif ($c->order_status == 1)
                                        <span class="progress-status status-black">@lang('localize.awaiting_merchant_confirmation')</span>
                                    @elseif ($c->order_status == 2)
                                        <span class="progress-status status-blue">@lang('localize.packaging')</span>
                                    @elseif ($c->order_status == 3)
                                        <span class="progress-status status-blue">@lang('localize.on_delivery')</span>
                                    @elseif ($c->order_status == 4)
                                        <span class="progress-status status-green">@lang('localize.completed')</span>
                                    @elseif ($c->order_status == 5)
                                        <span class="progress-status status-green">@lang('localize.cancelled')</span>
                                    @elseif ($c->order_status == 6)
                                        <span class="progress-status status-green">@lang('localize.refunded')</span>
                                    @elseif ($c->order_status == 7)
                                        <span class="progress-status status-blue">@lang('localize.installation')</span>
                                    @else
                                        <span class="progress-status status-black">Undefined Status</span>
                                    @endif
                                </p><br>

                                <p class="info">
                                    <label>@lang('localize.price'): @if($c->discounted_product_value != 0) <span class="cut">{{ rpFormat($c->product_original_price) }}</span> @endif <span class="red">{{ rpFormat($c->product_price) }}</span></label><br>
                                </p>

                                <p class="info">
                                    <label>@lang('localize.quantity'): <span class="red">{{$c->order_qty}}</span></label>
                                    <label>@lang('localize.total'): <span class="red">{{ rpFormat(($parent_order->items->sum('total_product_price') + $parent_order->shipping_amount + $parent_order->service_amount + $parent_order->platform_amount) - $parent_order->discounted_shipping_amount) }}</span></label>
                                </p>

                                <p class="info">
                                    @unless($parent_order->wholesale)
                                        @if($c->rateEligibility->product)
                                        <div class="schedule-btn">
                                            <a class="btn" href="{{ route('review-create', ['product', $c->order_id]) }}">@lang('localize.REVIEW.product')</a>
                                        </div>
                                        @else
                                        @if($c->productRating)
                                        <span class="card-rating">
                                            @include('front.partial.star.tag', ['rating' => $c->productRating])
                                        </span>
                                        @endif
                                        @endif
                                    @endunless
                                </p>
                            </div>
                        </div>
                        @endforeach

                    </div>
                    @endforeach
                </div>

                <div class="row mr-top-sm">
                    <div class="col-md-12">
                        <div class="total-container border">
                            <h5>@lang('localize.payment_information')</h5>
                            <div class="mr-top-sm">
                                <div class="list">
                                    <p>@lang('localize.merchandise_subtotal') ({{$parent_order->items->sum('order_qty')}} items)</p><p>{{rpFormat($parent_order->items->sum('total_product_price'))}}</p>
                                </div>
                                @if ($parent_order->service_amount > 0)
                                <div class="list">
                                    <p>@lang('localize.gst')</p><p>{{rpFormat($parent_order->service_amount)}}</p>
                                </div>
                                @endif

                                @if ($parent_order->platform_amount > 0)
                                <div class="list">
                                    <p>@lang('localize.platform_charges')</p><p>{{rpFormat($parent_order->platform_amount)}}</p>
                                </div>
                                @endif

                                <div class="list">
                                    <p>@lang('localize.shipping_fees')</p><p>{{rpFormat($parent_order->shipping_amount)}}</p>
                                </div>
                                <div class="list">
                                    <p>@lang('localize.discount')</p><p>{{ rpFormat($parent_order->discounted_order_amount + $parent_order->discounted_shipping_amount) }}</p>
                                </div>
                                <div class="list">
                                    <p>@lang('localize.payment_method')</p>
                                    @if ($parent_order->pg_transaction)
                                        <p>{{ $parent_order->pg_transaction->payment_type }} - {{ $parent_order->pg_transaction->bank }}</p>
                                    @endif
                                </div>
                                <div class="list">
                                    <p>@lang('localize.grand_total')</p><p class="red">{{ rpFormat(($parent_order->items->sum('total_product_price') + $parent_order->shipping_amount + $parent_order->service_amount + $parent_order->platform_amount) - $parent_order->discounted_shipping_amount) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <div class="row mr-top-sm">
                    <div class="col-md-12">
                        <div class="total-container border">
                            <h5>@lang('localize.shipping_information')</h5>
                            <div class="mr-top-sm">
                                @if($details)
                                <div class="list"> 
                                    <p>@lang('localize.trackingno')</p><p>{{$details['waybill_number']}}</p>
                                </div>
                                <div class="list">
                                    <p>@lang('localize.shipment_weight')</p><p>{{$details['weight']}} kg</p>
                                </div>
                                <div class="list">
                                    <p>@lang('localize.shipping_name')</p><p>{{$details['shippper_name']}}</p>
                                </div>
                                <div class="list">
                                    <p>@lang('localize.received_name')</p><p><b>{{$details['receiver_name']}}</b></p>
                                </div>
                                <div class="list">
                                    <p>@lang('localize.received_address')</p><p>{{$details['receiver_address1']}} {{$details['receiver_address2']}} {{$details['receiver_address3']}}</p>
                                </div>
                                @else
                                <div>
                                    <div class="title">@lang('localize.emptyStatus')</div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mr-top-sm">
                    <div class="col-md-12">
                        <div class="total-container border">
                            <h5>@lang('localize.shipping_history')</h5>
                            <div class="mr-top-sm">
                                <table class="table-resposive" align="center">
                                    <tbody>
                                        @if($manifest)
                                        @foreach($manifest as $key => $value)
                                        <tr>
                                            <td>
                                                <div class="title">{{$value['manifest_date']}}</div>
                                            </td>
                                            <td rowspan="2"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
                                            <td rowspan="2">
                                                <div class="description">{{$value['manifest_description']}}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="caption">{{$value['manifest_time']}}</div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <div>
                                            <div class="title">@lang('localize.emptyStatus')</div>
                                        </div>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection