@extends('layouts.front_master')

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
                <div class="account-container bg-white border pd-md">
                    <div class="account-titlebar d-flex align-items-center">
                        <h5>Bank Account</h5>
                        <a class="btn btn-red" data-toggle="modal" data-target="#addBankModal">
                            Add New<i class="material-icons">add</i>
                        </a>
                    </div>
                    <div class="detail-box mr-top-sm">
                        <div class="detail-item align-items-center">
                            <div class="detail-item-img"><img src="http://www.companieshistory.com/wp-content/uploads/2014/02/Maybank.png" /></div>
                            <div class="pd-y-sm">
                                <span class="detail-label">Default</span>
                                <p class="detail-bank-name">Abc Bank</p>
                                <p class="detail-bank-no">1235 3331 2223 4551</p>
                                <p class="detail-bank-holder">Tan Ah Meng</p>
                            </div>
                            <div class="ml-auto">
                                <a href="javascript:void(0)" class="option dropdown-toggle" data-toggle="dropdown">Option</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javascript:void(0)">Edit</a>
                                    <a class="dropdown-item" href="javascript:void(0)">Remove Bank</a>
                                </div>
                            </div>
                        </div>
                        <div class="detail-item align-items-center">
                            <div class="detail-item-img"><img src="http://www.companieshistory.com/wp-content/uploads/2014/02/Maybank.png" /></div>
                            <div class="pd-y-sm">
                                <p class="detail-bank-name">MEGA Card</p>
                                <p class="detail-bank-no">1235 3331 2223 4551</p>
                                <p class="detail-bank-holder">Tan Ah Meng</p>
                            </div>
                            <div class="ml-auto">
                                <a href="javascript:void(0)" class="option dropdown-toggle" data-toggle="dropdown">Option</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javascript:void(0)">Set default</a>
                                    <a class="dropdown-item" href="javascript:void(0)">Edit</a>
                                    <a class="dropdown-item" href="javascript:void(0)">Remove Bank</a>
                                </div>
                            </div>
                        </div>
                        <div class="detail-item align-items-center">
                            <div class="detail-item-img"><img src="http://www.companieshistory.com/wp-content/uploads/2014/02/Maybank.png" /></div>
                            <div class="pd-y-sm">
                                <p class="detail-bank-name">Mandiri Platinum</p>
                                <p class="detail-bank-no">1235 3331 2223 4551</p>
                                <p class="detail-bank-holder">Tan Ah Meng</p>
                            </div>
                            <div class="ml-auto">
                                <a href="javascript:void(0)" class="option dropdown-toggle" data-toggle="dropdown">Option</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javascript:void(0)">Set default</a>
                                    <a class="dropdown-item" href="javascript:void(0)">Edit</a>
                                    <a class="dropdown-item" href="javascript:void(0)">Remove Bank</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection