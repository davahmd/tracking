@extends('layouts.front_master')

@section('title', trans('localize.profile'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')

            <div class="main-content-container">
                <div class="bg-white shadow-sm">
                    {{--<div class="banner-container"></div>--}}
                    <div class="topbar d-flex align-items-center pd-sm">
                        <div class="mr-auto d-flex align-items-center">
                            <div class="profile-image">
                                <img src="{{ ($customer->cus_pic != '') ? Storage::url('gallery/customer/'. $customer->cus_pic):'/common/images/stock.png' }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
                            </div>
                            <h4>{{ isset($customer->cus_name)?$customer->cus_name:null }}</h4>
                        </div>
                        <button class="btn btn-blue" data-toggle="modal" data-target="#changeAvatarModal">@lang('localize.change_avatar')</button>
                    </div>
                </div>

                <div class="account-container bg-white border mr-top-sm pd-md">
                    @include('layouts.partials.status')
                    <div class="account-titlebar d-flex align-items-center">
                        <h5>{{trans('localize.myprofile')}}</h5>
                        <a href="{{ route('profile_edit') }}" class="btn btn-red">{{trans('localize.edit')}}&ensp;<span><i class="fa fa-edit"></i></span></a>
                    </div>
                    <div class="row mr-top-md">
                        <div class="col-md-6">
                            <div class="form-item">
                                <label>{{trans('localize.email')}}</label>
                                <p>{{isset($customer->user->email)?$customer->user->email:null}}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.name')}}</label>
                                <p>{!! !empty($customer->cus_name) ? $customer->cus_name : '-' !!}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.address1')}}</label>
                                <p>{!! !empty($customer->cus_address1) ? $customer->cus_address1 : '-' !!}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.address2')}}</label>
                                <p>{!! !empty($customer->cus_address2) ? $customer->cus_address2 : '-' !!}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.license_expired_date')}}</label>
                                <p>{!! !empty($customer->license_expired_date) ? date('d/m/Y', strtotime($customer->license_expired_date)) : '-' !!}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-item">
                                <label>{{trans('localize.country')}}</label>
                                <p>{!! !empty($customer->country->co_name) ? $customer->country->co_name : '-' !!}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.province')}}</label>
                                <p>{!! !empty($customer->state->name) ? $customer->state->name : '-' !!}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.city')}}</label>
                                <p>{!! !empty($customer->city->name) ? $customer->city->name : '-' !!}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.subdistrict')}}</label>
                                <p>{!! !empty($customer->subdistrict->name) ? $customer->subdistrict->name : '-' !!}</p>
                            </div>
                            <div class="form-item">
                                <label>{{trans('localize.zipcode')}}</label>
                                <p>{!! !empty($customer->cus_postalcode) ? $customer->cus_postalcode : '-' !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="changeAvatarModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content add-car-modal">

            <!-- Create New -->
            <div class="fromNew">
                <form method="post" action="/account/profile/avatar" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="topbar d-flex flex-row">
                        <h4 class="modal-title mr-auto">{{trans('localize.change_avatar')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="red-head"></div>
                    </div>
                    <div class="flex-row mr-top-md">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Image')}}</label>
                            <div class="col-lg-10">
                                <div id="upload-image-div">
                                    <input type="file" class="form-control-file" name="image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button type="submit" class="btn btn-blue">{{trans('localize.done')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('input[name="image"]').fileinput();
});
</script>
@endsection