@extends('layouts.front_master')

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')

            <div class="main-content-container">
                <div class="bg-white shadow-sm">
                    {{--<div class="banner-container"></div>--}}
                    <div class="topbar d-flex align-items-center pd-sm">
                        <div class="mr-auto d-flex align-items-center">
                            <div class="profile-image">
                                <img src="{{ Storage::url('gallery/customer/'. $customer->cus_pic) }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
                            </div>
                            <h4>{{$customer->cus_name}}</h4>
                        </div>
                        <button class="btn btn-blue" data-toggle="modal" data-target="#changeAvatarModal">Change Avatar</button>
                    </div>
                </div>

                <div class="account-container bg-white border mr-top-sm pd-md">
                	@include('layouts.partials.status')
                	<form action="/account/profile/details/edit" method="post">
                		{{ csrf_field() }}
	                    <div class="account-titlebar d-flex align-items-center">
	                        <h5>{{trans('localize.myprofile')}}</h5>
	                        <button type="submit" class="btn btn-green">{{trans('localize.save')}}</button>
	                    </div>
	                    <div class="row mr-top-md">
	                        <div class="col-md-6">
	                            <div class="form-item">
	                                <label>{{trans('localize.email')}}</label>
	                                <div class="custom-input">
	                                	<input type="text" name="email" value="{{$customer->user->email}}" placeholder="{{trans('localize.email')}}" disabled>
	                                </div>
	                            </div>
	                            <div class="form-item">
	                                <label>{{trans('localize.name')}}</label>
	                                <div class="custom-input">
	                                	<input type="text" name="name" value="{{$customer->cus_name}}" placeholder="{{trans('localize.name')}}">
	                                </div>
	                            </div>
	                            <div class="form-item">
	                            	<label>{{trans('localize.address1')}}</label>
	                            	<div class="custom-input">
	                            		<input type="text" name="address1" value="{{$customer->cus_address1}}" placeholder="{{trans('localize.address1')}}">
	                            	</div>
	                            </div>
	                            <div class="form-item">
	                            	<label>{{trans('localize.address2')}}</label>
	                            	<div class="custom-input">
	                            		<input type="text" name="address2" value="{{$customer->cus_address2}}" placeholder="{{trans('localize.address2')}}">
	                            	</div>
	                            </div>
	                            <div class="form-item">
	                            	<label>{{trans('localize.license_expired_date')}}</label>
	                            	<div class="input-group date">
	                            		<div class="custom-input">
		                            		<select class="form-control" name="day">
		                            			<option disabled>-- Day --</option>
		                            			@for ($i=1; $i<=31; $i++)
		                            			<option value="{{ $i }}" {{ (old('day', $customer->license_expired_date ? explode('-', $customer->license_expired_date)[2] : '' ) == $i) ? 'selected':'' }}>{{ $i }}</option>
		                            			@endfor
		                            		</select>
		                            	</div>
		                            	<div class="custom-input">
		                            		<select class="form-control" name="month">
		                            			<option disabled>-- Month --</option>
		                            			@for ($i=1; $i<=12; $i++)
		                            			<option value="{{ $i }}" {{ (old('month', $customer->license_expired_date ? explode('-', $customer->license_expired_date)[1] : '' ) == $i) ? 'selected':'' }}>{{ $i }}</option>
		                            			@endfor
		                            		</select>
		                            	</div>
		                            	<div class="custom-input">
		                            		<select class="form-control" name="year">
		                            			<option disabled>-- Year --</option>
		                            			@for ($i=date('Y')+10; $i>=date('Y'); $i--)
		                            			<option value="{{ $i }}" {{ (old('year', $customer->license_expired_date ? explode('-', $customer->license_expired_date)[0] : '') == $i) ? 'selected':'' }}>{{ $i }}</option>
		                            			@endfor
		                            		</select>
		                            	</div>
                                </div>
	                            </div>
	                        </div>
	                        <div class="col-md-6">
	                        	<div class="form-item">
	                            	<label>{{trans('localize.country')}}</label>
	                            	<div class="custom-input">
		                            	<select id="country" name="country" class="form-control js-option-tags">
		                            		<option>{{trans('localize.selectCountry')}}</option>
		                            		@foreach ($countries as $country)
		                            		<option {{ (old('country', $customer->cus_country) == $country->co_id) ? 'selected' : '' }} value="{{$country->co_id}}">{{$country->co_name}}</option>
		                            		@endforeach
		                            	</select>
		                            </div>
	                            </div>
	                            <div class="form-item">
	                            	<label>{{trans('localize.province')}}</label>
	                            	<div class="custom-input">
		                            	<select id="state" name="state" class="form-control js-option-tags">
		                            		<option>{{trans('localize.selectState')}}</option>
		                            	</select>
		                            </div>
	                            </div>
	                        	<div class="form-item">
	                            	<label>{{trans('localize.city')}}</label>
	                            	<div class="custom-input">
		                            	<select id="city" name="city" class="form-control js-option-tags">
		                            		<option>{{trans('localize.selectCity')}}</option>
		                            	</select>
		                            </div>
	                            </div>
	                            <div class="form-item">
	                            	<label>{{trans('localize.subdistrict')}}</label>
	                            	<div class="custom-input">
		                            	<select id="subdistrict" name="subdistrict" class="form-control js-option-tags">
		                            		<option>{{trans('localize.selectSubdistrict')}}</option>
		                            	</select>
		                            </div>
	                            </div>
	                            <div class="form-item">
	                            	<label>{{trans('localize.zipcode')}}</label>
	                            	<div class="custom-input">
	                            		<input type="text" name="zipcode" value="{{$customer->cus_postalcode}}" placeholder="{{trans('localize.zipcode')}}">
	                            	</div>
	                            </div>
	                        </div>
	                    </div>
	                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="changeAvatarModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content add-car-modal">

            <!-- Create New -->
            <div class="fromNew">
                <form method="post" action="/account/profile/avatar" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="topbar d-flex flex-row">
                        <h4 class="modal-title mr-auto">{{trans('localize.change_avatar')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="red-head"></div>
                    </div>
                    <div class="flex-row mr-top-md">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('localize.Image')}}</label>
                            <div class="col-lg-10">
                                <div id="upload-image-div">
                                    <input type="file" class="form-control-file" name="image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button type="submit" class="btn btn-blue">{{trans('localize.done')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script>
load_state('#state',"{{ old('country', $customer->cus_country) }}", "{{ old('state', $customer->cus_state) }}");
load_city('#city',"{{ old('state', $customer->cus_state) }}", "{{ old('city', $customer->cus_city) }}");
load_subdistrict('#subdistrict', "{{ old('city', $customer->cus_city) }}", "{{ old('subdistrict', $customer->cus_subdistrict) }}");

$(document).ready(function() {

  $('input[name="image"]').fileinput();

	// $('#datepicker').datepicker({
 //    format: 'dd/mm/yyyy',
 //    startDate: '+1d'
	// });

	// $('.js-option-tags').select2({
 //    	tags: false
 //    });

    $('#country').change(function() {
        var update_input = '#state';
        var country_id = $(this).val();

        load_state(update_input, country_id);
    });

	$('#state').change(function() {
		var update_input = '#city';
		var state_id = $(this).val();

		load_city(update_input, state_id);
	});

	$('#city').change(function() {
		var update_input = '#subdistrict';
		var city_id = $(this).val();

		load_subdistrict(update_input, city_id);
	});
});
</script>
@endsection