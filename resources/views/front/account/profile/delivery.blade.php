@extends('layouts.front_master')

@section('title', trans('front.nav.account.delivery'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')

            <div class="main-content-container">
                @include('layouts.partials.status')
                <div class="account-container bg-white border pd-md">
                    <div class="account-titlebar d-flex align-items-center">
                        <h5>{{trans('localize.Delivery_Address')}}</h5>
                        <button class="btn btn-red" data-toggle="modal" data-target="#addAddressModal">
                            {{trans('localize.add_new')}}&ensp;<span><i class="fa fa-plus"></i></span>
                        </button>
                    </div>
                    <div class="address-box mr-top-sm">
                        @foreach ($shippings as $shipping)
                        <div class="address-item active">
                            <p class="address-label">{{trans('localize.address')}} #{{$loop->iteration}}
                                @if($shipping->isdefault)<span>{{trans('localize.default')}}</span>@endif
                            </p>
                            <p class="address-receiver">{{$shipping->ship_name}}</p>
                            <p class="address-location info">{{$shipping->ship_address1}} {{$shipping->ship_address2}} {!! !empty($shipping->subdistrict->name) ? $shipping->subdistrict->name : '' !!} {!! !empty($shipping->city->name) ? $shipping->city->name : '' !!} {{$shipping->ship_postalcode}} {!! !empty($shipping->state->name) ? $shipping->state->name : '' !!} {!! !empty($shipping->country->co_name) ? $shipping->country->co_name : '' !!}</p>
                            <br>
                            <div class="text-right">
                                @if($shipping->isdefault != true)
                                <a href="/account/profile/delivery/setdefault/{{$shipping->ship_id}}" >
                                    <button class="btn btn-red">
                                    {{trans('localize.set_as_default')}}
                                    </button>
                                </a>
                                @endif

                                @if($shippings->count() > 1)
                                <button class="delete_button btn btn-red-outline" data-target="#deleteAddressModal" data-toggle="modal" data-ship-id="{{$shipping->ship_id}}">
                                    {{trans('localize.delete')}}
                                </button>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Cover Modal -->
<div class="modal fade" id="addAddressModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content add-address-modal">
            <div class="topbar">
                <h4 class="modal-title">{{trans('localize.add_new_address')}}</h4>
            </div>
            <form class="mr-top-sm" action="/account/profile/delivery" method="post">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="ship_name" placeholder="{{trans('localize.name')}}" value="{{ old('ship_name') }}" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select name="areacode" class="form-control js-option-tags">
                                @foreach ($countries as $country)
                                <option {{ (old('areacode', $customer->areacode) == $country->phone_country_code) ? 'selected' : '' }} value="{{$country->phone_country_code}}">+{{$country->phone_country_code}} - {{$country->co_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text"  class="form-control" name="phone" placeholder="{{trans('localize.phone')}}" value="{{ old('phone') }}" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="address1" placeholder="{{trans('localize.address1')}}" value="{{ old('address1') }}" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="address2" placeholder="{{trans('localize.address2')}}" value="{{ old('address2') }}" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="country" name="country" class="form-control js-option-tags">
                                <option>{{trans('localize.selectCountry')}}</option>
                                @foreach ($countries as $country)
                                <option {{ (old('country') == $country->co_id) ? 'selected' : '' }} value="{{$country->co_id}}">{{$country->co_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="state" name="state" class="form-control js-option-tags">
                                <option>{{trans('localize.selectState')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="city" name="city" class="form-control js-option-tags">
                                <option>{{trans('localize.selectCity')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="subdistrict" name="subdistrict" class="form-control js-option-tags">
                                <option>{{trans('localize.selectSubdistrict')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="zipcode" placeholder="{{trans('localize.zipcode')}}" value="{{ old('zipcode') }}" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                      <label class="form-check-label" for="default">
                        <input class="form-check-input" type="checkbox" name="isdefault" value="1" id="default">
                        <span>{{trans('localize.save_this_address_as_default')}}</span>
                      </label>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-blue" type="submit">{{trans('localize.save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Remove Modal -->
<div class="modal fade" id="deleteAddressModal">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content remove-car-modal">

            <!-- Remove Address -->
            <div class="fromRemove">
                <div class="topbar d-flex justify-content-end">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="text-center">
                    <h5>{{trans('localize.delete_this_address')}}?</h5>
                    <a href="">
                        <button class="btn">{{trans('localize.Yes')}}</button>
                    </a>
                    <button class="btn" data-dismiss="modal">{{trans('localize.No')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>

<script>
load_state('#state',"{{ old('country') }}", "{{ old('state') }}");
load_city('#city',"{{ old('state') }}", "{{ old('city') }}");
load_subdistrict('#subdistrict', "{{ old('city') }}", "{{ old('subdistrict') }}");

$(document).ready(function() {

    $('.delete_button').click(function() {
        var url = '/account/profile/delivery/delete/'+ $(this).data('ship-id');
        $('#deleteAddressModal a').attr('href', url);
    });

    $('#country').change(function() {
        var update_input = '#state';
        var country_id = $(this).val();

        load_state(update_input, country_id);
    });

    $('#state').change(function() {
        var update_input = '#city';
        var state_id = $(this).val();

        load_city(update_input, state_id);
    });

    $('#city').change(function() {
        var update_input = '#subdistrict';
        var city_id = $(this).val();

        load_subdistrict(update_input, city_id);
    });
});
</script>
@endsection