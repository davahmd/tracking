@extends('layouts.front_master')

@section('title', trans('front.nav.account.help'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
                <div class="account-container bg-white border pd-md">
                    <div class="account-titlebar d-flex align-items-center">
                        <h5>Help</h5>
                    </div>
                    <div class="mr-top-md" style="text-align: justify;">
                        <p class="info">Staying calm, composed and maintaining strong self esteem in today's tough environment can be difficult but is 
                            not impossible if you follow a few simple guidelines. Here are 6 tips you can use as a starter guide to self improvement.
                        </p>
                        <p class="info">Everything and everyone else around you can affect your self esteem. Other people can deliberately or inadvertently 
                            damage your self image. Unchecked people and circumstances can ultimately destroy your self esteem and pull you down in ways 
                            you won't even notice. Don't let these influences get the best of you. But what should you avoid?
                        </p>
                        <h6>1: A Negative Work Environment</h6>
                        <p class="info">Beware of a "dog eat dog" environment where everyone else is fighting just to get ahead. This is where non-appreciative 
                            people usually thrive and working extra is expected and not rewarded. In this environment no one will appreciate 
                            your contributions even if you miss lunch, dinner, and stay at work late into the night. Unless you are very fortunate 
                            most of the time you will work too hard with no help from others around you. This typre of atmosphere will ruin your self 
                            esteem. This is not just healthy competition, at its worst it is brutal and very damaging.
                        </p>
                        <h6>2: Other Peoples Behavior</h6>
                        <p class="info">Beware of a "dog eat dog" environment where everyone else is fighting just to get ahead. This is where non-appreciative 
                            people usually thrive and working extra is expected and not rewarded. In this environment no one will appreciate 
                            your contributions even if you miss lunch, dinner, and stay at work late into the night. Unless you are very fortunate 
                            most of the time you will work too hard with no help from others around you. This typre of atmosphere will ruin your self 
                            esteem. This is not just healthy competition, at its worst it is brutal and very damaging.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection