@extends('layouts.front_master')

@section('title', trans('front.nav.account.general.automobile'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')

            <div class="main-content-container">

                @include('layouts.partials.status')

                <div class="topbar d-flex align-items-center mr-btm-sm mr-top-sm">
                    <h4 class="mr-auto">{{trans('localize.my_automobile')}}</h4>
                    <div class="red-head"></div>
                    <button class="btn btn-blue" data-toggle="modal" data-target="#addCarModal">{{trans('localize.add_new')}}</button>
                </div>
                <div class="car-list pd-top-md">
                    @foreach ($vehicles as $vehicle)
                    <div class="car-item d-flex align-items-center">
                        <div class="car-item-image d-flex align-items-center">
                            @if ($vehicle->vehicle_image)
                            <img src="{{ Storage::url('gallery/vehicle/'.$vehicle->vehicle_image) }}">
                            @else
                            <img src="/common/images/stock.png">
                            @endif
                        </div>
                        <div class="car-item-detail">
                            <div class="d-flex align-items-center">
                                <h4 class="car-item-name">{{$vehicle->brand}} {{$vehicle->model}}</h4>
                                <img class="car-item-brand" src="{{ Storage::url('gallery/vehicle/'.strtolower($vehicle->brand).'/'.strtolower($vehicle->brand).'.png') }}">
                            </div>
                            <p class="car-item-spec">{{$vehicle->model}} {{$vehicle->variant}}</p>
                            <p class="car-item-year">{{$vehicle->year}}</p>
                        </div>
                        <div class="car-item-button d-flex flex-column ml-auto">
                            <button class="btn @if($vehicle->pivot->isdefault) btn-default @else btn-setdefault @endif" onclick="{{ 'makeDefault(' . $vehicle->pivot->id . ')' }}">
                                @if($vehicle->pivot->isdefault)
                                {{trans('localize.default')}}
                                @else
                                {{trans('localize.set_as_default')}}
                                @endif
                            </button>
                            <button class="btn btn-outline-secondary edit-car" data-toggle="modal" data-target="#editCarModal" data-id="{{ $vehicle->pivot->id }}" data-brand="{{ $vehicle->brand }}" data-model="{{ $vehicle->model }}" data-variant="{{ $vehicle->variant }}" data-vehicle="{{ $vehicle->id }}" data-name="{{ $vehicle->pivot->name }}" data-insurance="{{ $vehicle->pivot->insurance }}" data-insurance-expired-date="{{ $vehicle->pivot->insurance_expired_date }}" data-road-tax-expired-date="{{ $vehicle->pivot->road_tax_expired_date }}" data-mileage="{{ $vehicle->pivot->mileage }}">{{trans('localize.edit')}}</button>
                            <button class="btn btn-outline-danger delete_button" data-toggle="modal" data-target="#removeCarModal" data-automobile="{{$vehicle->pivot->id}}">{{trans('localize.remove')}}</button>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addCarModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content add-car-modal">

            <!-- Create New -->
            <div class="fromNew">
                <form method="post" action="/account/general/automobile/add">
                    {{ csrf_field() }}
                    <div class="topbar d-flex flex-row">
                        <h4 class="modal-title mr-auto">{{trans('localize.add_new_car_detail')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="row mr-top-md">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('localize.Brand')}}</label>
                                <div>
                                    <select name="brand" class="form-control js-option-tags" style="width: 100%;">
                                        <option>{{trans('localize.select_brand')}}</option>
                                        @foreach ($brands as $brand)
                                        <option>{{$brand}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.Model')}}</label>
                                <div>
                                    <select name="model" class="form-control js-option-tags" style="width: 100%;">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.Variant')}}</label>
                                <div>
                                    <select name="variant" class="form-control js-option-tags" style="width: 100%;">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.Year')}}</label>
                                <div>
                                    <select name="vehicle" class="form-control js-option-tags" style="width: 100%;">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.name')}}</label>
                                <div class="custom-input">
                                    <input type="text" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('localize.car_insurance_purchased')}}</label>
                                <div class="custom-input">
                                    <input type="text" name="insurance">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.car_insurance_expired_date')}}</label>
                                <div class="input-group date datepicker d-flex">
                                    <input type="text" class="form-control" name="insurance_expired_date">
                                    <button type="button" class="btn btn-red"> <i class="far fa-calendar-alt"></i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.road_tax_expired_date')}}</label>
                                <div class="input-group date datepicker d-flex">
                                    <input type="text" class="form-control" name="road_tax_expired_date">
                                    <button type="button" class="btn btn-red"> <i class="far fa-calendar-alt"></i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.mileage')}}</label>
                                <div class="d-flex custom-input">
                                    <input type="number" name="mileage" min="0">
                                    <span>KM</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button type="submit" class="btn btn-blue">{{trans('localize.done')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editCarModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content add-car-modal">

            <!-- Create New -->
            <div class="fromEdit">
                <form method="post" action="/account/general/automobile/edit">
                    {{ csrf_field() }}
                    <input type="hidden" name="id">
                    <div class="topbar d-flex flex-row">
                        <h4 class="modal-title mr-auto">{{trans('localize.edit_car_detail')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="row mr-top-md">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('localize.Brand')}}</label>
                                <div>
                                    <select name="brand" class="form-control js-option-tags" style="width: 100%;">
                                        @foreach ($brands as $brand)
                                        <option>{{$brand}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.Model')}}</label>
                                <div>
                                    <select name="model" class="form-control js-option-tags" style="width: 100%;">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.Variant')}}</label>
                                <div>
                                    <select name="variant" class="form-control js-option-tags" style="width: 100%;">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.Year')}}</label>
                                <div>
                                    <select name="vehicle" class="form-control js-option-tags" style="width: 100%;">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.name')}}</label>
                                <div class="custom-input">
                                    <input type="text" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('localize.car_insurance_purchased')}}</label>
                                <div class="custom-input">
                                    <input type="text" name="insurance">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.car_insurance_expired_date')}}</label>
                                <div class="input-group date datepicker d-flex">
                                    <input type="text" class="form-control" name="insurance_expired_date">
                                    <button type="button" class="btn btn-red"> <i class="far fa-calendar-alt"></i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.road_tax_expired_date')}}</label>
                                <div class="input-group date datepicker d-flex">
                                    <input type="text" class="form-control" name="road_tax_expired_date">
                                    <button type="button" class="btn btn-red"> <i class="far fa-calendar-alt"></i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('localize.mileage')}}</label>
                                <div class="d-flex custom-input">
                                    <input type="number" name="mileage" min="0">
                                    <span>KM</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button type="submit" class="btn btn-blue">{{trans('localize.done')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Remove Modal -->
<div class="modal fade" id="removeCarModal">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content remove-car-modal">

            <!-- Remove Car -->
            <div class="fromRemove">
                <div class="topbar d-flex justify-content-end">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="text-center">
                    <h5>{{trans('localize.remove_this_car')}}?</h5>
                    <a href="">
                        <button class="btn">{{trans('localize.Yes')}}</button>
                    </a>
                    <button class="btn" data-dismiss="modal">{{trans('localize.No')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script>
function makeDefault(vehicle_id)
{
    window.location.replace("/account/general/automobile/setdefault/" + vehicle_id);
}

$(document).ready(function() {

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '+1d'
    });

    $('.js-option-tags').select2({
        tags: false,
    });

    $('#editCarModal').on('hidden.bs.modal', function () {
        $('#editCarModal select[name="model"]').empty()
        $('#editCarModal select[name="variant"]').empty()
        $('#editCarModal select[name="vehicle"]').empty()
    });

    $('.edit-car').click(function() {

        var insuranceExpiredDate = $(this).data('insurance-expired-date');
        var roadTaxExpiredDate = $(this).data('road-tax-expired-date');

        var insuranceExpiredDateArray = insuranceExpiredDate.split('-');
        var roadTaxExpiredDateArray = roadTaxExpiredDate.split('-');

        if (insuranceExpiredDate != '') {
            var insuranceFormattedDate = insuranceExpiredDateArray[2]+'/'+insuranceExpiredDateArray[1]+'/'+insuranceExpiredDateArray[0];
        }

        if (roadTaxExpiredDate != '') {
            var roadTaxFormattedDate = roadTaxExpiredDateArray[2]+'/'+roadTaxExpiredDateArray[1]+'/'+roadTaxExpiredDateArray[0];
        }

        $('#editCarModal input[name="id"]').val($(this).data('id'));

        $('#editCarModal select[name="brand"]').val($(this).data('brand'));
        // $('#editCarModal select[name="model"]').val($(this).data('model'));
        // $('#editCarModal select[name="variant"]').val($(this).data('variant'));
        // $('#editCarModal select[name="vehicle"]').val($(this).data('vehicle'));

        $('#editCarModal input[name="name"]').val($(this).data('name'));
        $('#editCarModal input[name="insurance"]').val($(this).data('insurance'));

        $('#editCarModal input[name="insurance_expired_date"]').val(insuranceFormattedDate);
        $('#editCarModal input[name="road_tax_expired_date"]').val(roadTaxFormattedDate);

        $('#editCarModal input[name="mileage"]').val($(this).data('mileage'));

        $('.js-option-tags').change(function () {
            $('a.select2-choice span').text($(this).val());
        }).trigger('change');

        preload_model($(this).data('brand'), $(this).data('model'));
        preload_variant($(this).data('brand'), $(this).data('model'), $(this).data('variant'));
        preload_year($(this).data('model'), $(this).data('variant'), $(this).data('vehicle'));
    });

    function preload_model(brand, model) {
        $.ajax({
            type: 'GET',
            url: '/get_model/'+ brand,
            success: function(data) {
                $('#editCarModal select[name="model"]').append('<option disabled selected>{{trans("localize.select_model")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    if (data[i] == model) {
                        $('#editCarModal select[name="model"]').append('<option selected>'+data[i]+'</option>');
                    }
                    else {
                        $('#editCarModal select[name="model"]').append('<option>'+data[i]+'</option>');
                    }
                }
            }
        })
    }

    function preload_variant(brand, model, variant) {
        $.ajax({
            type: 'GET',
            url: '/get_variant/'+ brand +'/'+ model,
            success: function(data) {
                $('#editCarModal select[name="variant"]').append('<option disabled selected>{{trans("localize.select_variant")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    if (data[i] == variant) {
                        $('#editCarModal select[name="variant"]').append('<option selected>'+data[i]+'</option>');
                    }
                    else {
                        $('#editCarModal select[name="variant"]').append('<option>'+data[i]+'</option>');
                    }
                }
            }
        })
    }

    function preload_year(model, variant, vehicle) {
        $.ajax({
            type: 'GET',
            url: '/get_year/'+ model +'/'+variant,
            success: function(data) {
                $('#editCarModal select[name="vehicle"]').append('<option disabled selected>{{trans("localize.select_year")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].id == vehicle) {
                        $('#editCarModal select[name="vehicle"]').append('<option value="'+data[i].id+'" selected>'+data[i].year+'</option>');
                    }
                    else {
                        $('#editCarModal select[name="vehicle"]').append('<option value="'+data[i].id+'">'+data[i].year+'</option>');
                    }
                }
            }
        })
    }

    $('.delete_button').click(function() {
        var url = '/account/general/automobile/delete/' + $(this).data('automobile');
        $('#removeCarModal a').attr('href', url);
    });

    $('#addCarModal select[name="brand"]').change(function(e) {
        $('#addCarModal select[name="model"]').empty();
        $('#addCarModal select[name="variant"]').empty();
        $('#addCarModal select[name="vehicle"]').empty();

        $.ajax({
            type: 'GET',
            url: '/get_model/'+$(this).val(),
            success: function(data) {
                $('#addCarModal select[name="model"]').append('<option disabled selected>{{trans("localize.select_model")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#addCarModal select[name="model"]').append('<option>'+data[i]+'</option>');
                }
            }
        })
    });

    $('#addCarModal select[name="model"]').change(function(e) {
        $('#addCarModal select[name="variant"]').empty();
        $('#addCarModal select[name="vehicle"]').empty();

        var brand = $('#addCarModal select[name="brand"]').val();

        $.ajax({
            type: 'GET',
            url: '/get_variant/'+ brand +'/'+$(this).val(),
            success: function(data) {
                $('#addCarModal select[name="variant"]').append('<option disabled selected>{{trans("localize.select_variant")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#addCarModal select[name="variant"]').append('<option>'+data[i]+'</option>');
                }
            }
        })
    });

    $('#addCarModal select[name="variant"]').change(function(e) {
        $('#addCarModal select[name="vehicle"]').empty();

        var model = $('#addCarModal select[name="model"]').val();

        $.ajax({
            type: 'GET',
            url: '/get_year/'+ model +'/'+$(this).val(),
            success: function(data) {
                $('#addCarModal select[name="vehicle"]').append('<option disabled selected>{{trans("localize.select_year")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#addCarModal select[name="vehicle"]').append('<option value="'+data[i].id+'">'+data[i].year+'</option>');
                }
            }
        })
    });

    $('#editCarModal select[name="brand"]').on('select2:select', function(e) {
        $('#editCarModal select[name="model"]').empty();
        $('#editCarModal select[name="variant"]').empty();
        $('#editCarModal select[name="vehicle"]').empty();

        $.ajax({
            type: 'GET',
            url: '/get_model/'+$(this).val(),
            success: function(data) {
                $('#editCarModal select[name="model"]').append('<option disabled selected>{{trans("localize.select_model")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#editCarModal select[name="model"]').append('<option>'+data[i]+'</option>');
                }
            }
        })
    });

    $('#editCarModal select[name="model"]').on('select2:select', function(e) {
        $('#editCarModal select[name="variant"]').empty();
        $('#editCarModal select[name="vehicle"]').empty();

        var brand = $('#editCarModal select[name="brand"]').val();

        $.ajax({
            type: 'GET',
            url: '/get_variant/'+ brand +'/'+$(this).val(),
            success: function(data) {
                $('#editCarModal select[name="variant"]').append('<option disabled selected>{{trans("localize.select_variant")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#editCarModal select[name="variant"]').append('<option>'+data[i]+'</option>');
                }
            }
        })
    });

    $('#editCarModal select[name="variant"]').on('select2:select', function(e) {
        $('#editCarModal select[name="vehicle"]').empty();

        var model = $('#editCarModal select[name="model"]').val();

        $.ajax({
            type: 'GET',
            url: '/get_year/'+ model +'/'+$(this).val(),
            success: function(data) {
                $('#editCarModal select[name="vehicle"]').append('<option disabled selected>{{trans("localize.select_year")}}</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#editCarModal select[name="vehicle"]').append('<option value="'+data[i].id+'">'+data[i].year+'</option>');
                }
            }
        })
    });
});
</script>
@endsection