@extends('layouts.front_master')

@section('title', trans('front.nav.account.general.overview'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
            
                <div class="account-container bg-white border pd-md">
                    <div class="account-titlebar d-flex align-items-center">
                        <h5>@lang('localize.account')</h5>
                    </div>
                    
                    <div class="sub-heading">@lang('localize.chat')</div>
                    <div class="overview-list">
                        <img class="list-image" src="{{ asset('asset/images/icon/icon_account_chat.png') }}">
                        <div class="list-content">@lang('localize.unread_messages', ['count' => $chat_unread])</div>
                        
                        <div class="list-action">
                            <a href="{{ route('chat-list') }}">@lang('localize.view') <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                    
                    <div class="sub-heading">@lang('localize.notification')</div>
                    <div class="overview-list">
                        <img class="list-image" src="{{ asset('asset/images/icon/icon_account_transaction.png') }}" />
                        <div class="list-content">@lang('localize.new_notifications', ['count' => $notification_unread])</div>
                        
                        <div class="list-action">
                            <a href="{{ route('notification') }}">@lang('localize.view') <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                        
                </div>
            
                
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addCarModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content add-car-modal">

            <!-- Create New -->
            <div class="fromNew">
                <div class="topbar d-flex flex-row">
                    <h4 class="modal-title mr-auto">Add new car detail</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="red-head"></div>
                </div>
                <div class="row mr-top-md">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Car Brand</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Brand</option>
                                    <option value="1">Audi</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Model</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Model</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Year</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Specification</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Specification</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Car Insurance Purchased</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Insurance</option>
                                    <option value="1">Allianz</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Insurance Expired Date</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Date</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Road Tax Expired Date</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Date</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Mileage</label>
                            <div class="d-flex custom-input">
                                <input type="text" placeholder="Enter mileage" />
                                <span>KM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row-reverse">
                    <a href="javascript:void(0)" class="btn btn-blue">Done</a>
                </div>
            </div>                                    
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editCarModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content edit-car-modal">

            <!-- Edit Car -->
            <div class="fromEdit">
                <div class="topbar d-flex flex-row">
                    <h4 class="modal-title mr-auto">Edit car detail</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="red-head"></div>
                </div>

                <div class="row mr-top-md">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Car Brand</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Brand</option>
                                    <option value="1">Audi</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Model</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Model</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Year</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Specification</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Specification</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Car Insurance Purchased</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Car Insurance</option>
                                    <option value="1">Allianz</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Car Insurance Expired Date</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Date</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Road Tax Expired Date</label>
                            <div class="custom-select">
                                <select>
                                    <option value="0">Select Date</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Mileage</label>
                            <div class="d-flex custom-input">
                                <input type="text" placeholder="Enter mileage" />
                                <span>KM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row-reverse">
                    <a href="javascript:void(0)" class="btn btn-blue">Done</a>
                </div>
            </div>                                    
        </div>
    </div>
</div>

<!-- Remove Modal -->
<div class="modal fade" id="removeCarModal">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content remove-car-modal">

            <!-- Remove Car -->
            <div class="fromRemove">
                <div class="topbar d-flex justify-content-end">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="text-center">
                    <h5>Remove this car?</h5>
                    <button class="btn">Yes</button>
                    <button class="btn">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection