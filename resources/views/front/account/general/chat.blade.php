@extends('layouts.front_master')

@section('title', trans('localize.chat'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
                <div class="chat-container single">
                    <div class="chat-item bg-white">
                        <div class="chat-item-top pd-sm">
                            <div class="d-flex">
                                <div class="chat-image xs">
                                    <img src="{{ $store_img }}">
                                </div>
                                <div class="chat-content">
                                    <h6 class="chat-title">{{ $store_name }}</h6>
                                    <p class="info">{{ $latest_timestamp }}</p>
                                    <p id="connection" class="text-danger">@lang('localize.connection_issue')</p>
                                </div>
                            </div>
                        </div>
                        <div id="chat-item-body" class="chat-item-body">
                            <ul>
                                @foreach($chats as $chat)
                                    <li class="chat-{{ ($chat->sender_type == 'U') ? 'right' : 'left' }}">
                                        <div class="chat-inner" data-chat="{{ $chat->id }}">
                                            <p>{!! preg_replace('/[\r\n]/', '<br>', $chat->message) !!}</p>
                                            <span class="date">{{ Helper::UTCtoTZ($chat->created_at, 'j/m/Y g:i A') }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="chat-item-bottom pd-sm d-flex">
                            <textarea id="msg" class="flex-grow-1" rows="1" placeholder="@lang('localize.write_message')" maxlength="500"></textarea>
                            <button id="send" type="button" class="btn btn-red"><span id="i_send">@lang('localize.send')<i class="material-icons">send</i></span><i id="i_spinner" class="fa fa-spinner fa-pulse d-none" style="margin: 0 auto"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('asset/js/autosize.min.js') }}"></script>
<script>
$(function() {
    scrollToBottom('chat-item-body');
    $('#msg').focus();
    autosize($('#msg'));

    $('#msg').keydown((e) => {
        if (e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            $('#send').click();
        }
    });

    $('#send').click(() => {
        let msgValue = $('#msg').val().trim();
        if (!msgValue) {
            $('#msg').val('');
            autosize.update($('#msg'));
            return;
        }

        $('#i_send').addClass('d-none');
        $('#i_spinner').removeClass('d-none');

        $.ajax({
            url: '/account/general/chat/' + '{{ $store_id }}' + '/' + '{{ $product_id }}',
            type: 'post',
            dataType: 'json',
            data: {
                msg: msgValue
            }
        })
        .done((data) => {
            if (data.status === true) {
                let message = $('<div/>').text(msgValue).html();
                $('#chat-item-body ul').append(chatModule.buildMessage('right', message));
                $('.info').html(moment().format('D/M/YY h:mm A'))
                scrollToBottom('chat-item-body');
                $('#msg').val('');
                autosize.update($('#msg'));
            }
        })
        .fail((data) => {
            setInterval(function() {
                if ($('#connection').css('visibility') == 'hidden') {
                    $('#connection').css('visibility', 'visible');
                }
                else {
                    $('#connection').css('visibility', 'hidden');
                }
            }, 1000);
        })
        .always((data) => {
            $('#i_spinner').addClass('d-none');
            $('#i_send').removeClass('d-none');
        });
    });

    Echo.join('chat.{{ auth()->id() }}.{{ $store_id }}.{{ $product_id }}')
        .listen('MessageCreated', (e) => {
            $('#chat-item-body ul').append(chatModule.buildMessage(e.chat.sender_type == 'S' ? 'left' : 'right', e.chat.message));
            scrollToBottom('chat-item-body');
        });
});
</script>
@endsection