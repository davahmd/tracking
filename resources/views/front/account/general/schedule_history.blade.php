@extends('layouts.front_master')

@section('title', trans('front.nav.account.general.schedule'))

@section('content')
<div id="main">
    <div class="container">
        @include('layouts.partials.status')
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            <div class="main-content-container">
                <div class="top-box d-flex align-items-center mr-btm-sm">
                    <ul id="tabs_list" class="nav nav-pills">
                        <li class="nav-item">
                        <a class="nav-link {{ $type ? ($type == 'tab-unscheduled') ? 'active' : '' : 'active' }}" id="tab-unscheduled" data-toggle="tab" href="#tab-unscheduled">@lang('localize.unscheduled')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-scheduled') ? 'active' : '' }}" id="tab-scheduled" data-toggle="tab" href="#tab-scheduled">@lang('localize.scheduled')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-confirmed') ? 'active' : '' }}" id="tab-confirmed" data-toggle="tab" href="#tab-confirmed">@lang('localize.confirmed')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-completed') ? 'active' : '' }}" id="tab-completed" data-toggle="tab" href="#tab-completed">@lang('localize.completed')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-cancelled') ? 'active' : '' }}" id="tab-cancelled" data-toggle="tab" href="#tab-cancelled">@lang('localize.cancelled')</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div id="tab-unscheduled" class="tab-pane {{ ($type == 'tab-unscheduled') ? 'active' : '' }}">
                        <div class="mr-top-sm">
                            @foreach($schedules as $schedule => $values)
                                @if($schedule == 'unscheduled')
                                    @foreach($values->sortByDesc('parent_order_id')->groupBy('parent_order_id') as $value => $keys)
                                        <div class="order-item">
                                            <div class="top-layer">
                                                <div>
                                                    <p class="order-id">@lang('localize.transaction_id'): {{$keys->first()->transaction_id}}</p>
                                                    <p>{{$keys[0]['order_date']}}</p>
                                                </div>
                                                {{-- <a class="btn btn-detail" href="{{route('member-schedule-service', [$value])}}">Schedule Service</a> --}}
                                            </div>
                                            @foreach($keys as $key)
                                                <div class="product-layer">
                                                    <div class="d-flex">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <img class="product-image" src="{{ $key->mainImageUrl() }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
                                                        </a>
                                                        <div class="product-content">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <h6 class="product-title">{{$key->pro_title_en}}</h6>
                                                        </a>
                                                            <p class="info">
                                                                Service: <span class="red">{{$key->service_name_current}}</span>
                                                            </p>
                                                            <p class="info">
                                                                @lang('localize.quantity'): <span class="red">{{$key->order_qty}}</span> | @lang('localize.total'): <span class="red">{{ rpFormat($key->product_price) }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <a class="chat-btn" href="{{ route('chat-conversation', $key->pro_id) }}" target="_blank">
                                                        <img src="{{ asset('asset/images/icon/icon_account_chat.png') }}">
                                                    </a>
                                                    @if($key->status == 0)
                                                        <div class="text-right ml-auto">
                                                            <span class="progress-status status-red">@lang('localize.awaiting_your_schedule')</span>
                                                            {{-- <a class="btn btn-detail" href="{{route('member-schedule-service', [$key->id])}}">Schedule Service</a> --}}
                                                        </div>
                                                        <div class="schedule-btn">
                                                            <a class="btn" href="{{route('member-schedule-service', [$key->id])}}">@lang('localize.schedule_service')</a>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            @endforeach
                            {{ $schedules['unscheduled']->links() }}
                        </div>
                    </div>
                    <div id="tab-scheduled" class="tab-pane {{ ($type == 'tab-scheduled') ? 'active' : '' }}">
                        <div class="mr-top-sm">
                            @foreach($schedules as $schedule => $values)
                                @if($schedule == 'scheduled')
                                    @foreach($values->sortByDesc('parent_order_id')->groupBy('parent_order_id') as $value => $keys)
                                        <div class="order-item">
                                            <div class="top-layer">
                                                <div>
                                                    <p class="order-id">@lang('localize.transaction_id'): {{$keys->first()->transaction_id}}</p>
                                                    <p>{{$keys[0]['order_date']}}</p>
                                                </div>
                                                {{-- <a class="btn btn-detail" href="{{route('member-schedule-service', [$value])}}">Reschedule Service</a> --}}
                                            </div>
                                            @foreach($keys as $key)
                                                <div class="product-layer">
                                                    <div class="d-flex">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <img class="product-image" src="{{ $key->mainImageUrl() }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
                                                        </a>
                                                        <div class="product-content">
                                                            <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                                <h6 class="product-title">{{$key->pro_title_en}}</h6>
                                                            </a>
                                                            <p class="info">
                                                                @lang('localize.service'): <span class="red">{{$key->service_name_current}}</span>
                                                            </p>
                                                            <p class="info">
                                                                @lang('localize.quantity'): <span class="red">{{$key->order_qty}}</span> | @lang('localize.total'): <span class="red">{{ rpFormat($key->product_price) }}</span>
                                                            </p>
                                                            <p class="info">
                                                                {{-- @lang('localize.schedule_date'): <span class="red">{{ Helper::UTCtoTZ($key->schedule_datetime,'j/m/Y g:i A') }}</span> --}}
                                                                @lang('localize.schedule_date'): <span class="red">{{ $key->schedule_datetime }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <a class="chat-btn" href="{{ route('chat-conversation', $key->pro_id) }}" target="_blank">
                                                        <img src="{{ asset('asset/images/icon/icon_account_chat.png') }}">
                                                    </a>
                                                    @if($key->status == 1)
                                                        <div class="text-right ml-auto">
                                                            <span class="progress-status status-red">@lang('localize.awaiting_merchant_confirmation')</span>
                                                        </div>
                                                    @elseif($key->status == 2)
                                                        <div class="text-right ml-auto">
                                                            <span class="progress-status status-red">@lang('localize.awaiting_your_confirmation')</span>
                                                            <a class="btn btn-detail" href="{{route('member-schedule-service-update-status', [$key->id, 3])}}">@lang('localize.confirm')</a>
                                                        </div>
                                                    @endif
                                                    <div class="schedule-btn">
                                                        <a class="btn" href="{{route('member-schedule-service', [$key->id])}}">@lang('localize.reschedule_service')</a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            @endforeach
                            {{ $schedules['scheduled']->links() }}
                        </div>
                    </div>
                    <div id="tab-confirmed" class="tab-pane {{ ($type == 'tab-confirmed') ? 'active' : '' }}">
                        <div class="mr-top-sm">
                            @foreach($schedules as $schedule => $values)
                                @if($schedule == 'confirmed')
                                    @foreach($values->sortByDesc('parent_order_id')->groupBy('parent_order_id') as $value => $keys)
                                        <div class="order-item">
                                            <div class="top-layer">
                                                <div>
                                                    <p class="order-id">@lang('localize.transaction_id'): {{$keys->first()->transaction_id}}</p>
                                                    <p>{{$keys[0]['order_date']}}</p>
                                                </div>
                                            </div>
                                            @foreach($keys as $key)
                                                <div class="product-layer">
                                                    <div class="d-flex">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <img class="product-image" src="{{ $key->mainImageUrl() }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
                                                        </a>
                                                        <div class="product-content">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <h6 class="product-title">{{$key->pro_title_en}}</h6>
                                                        </a>
                                                            <p class="info">
                                                                @lang('localize.service'): <span class="red">{{$key->service_name_current}}</span>
                                                            </p>
                                                            <p class="info">
                                                                @lang('localize.quantity'): <span class="red">{{$key->order_qty}}</span> | @lang('localize.total'): <span class="red">{{ rpFormat($key->product_price) }}</span>
                                                            </p>
                                                            <p class="info">
                                                                {{-- @lang('localize.schedule_date'): <span class="red">{{ Helper::UTCtoTZ($key->schedule_datetime,'j/m/Y g:i A') }}</span> --}}
                                                                @lang('localize.schedule_date'): <span class="red">{{ $key->schedule_datetime }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <a class="chat-btn" href="{{ route('chat-conversation', $key->pro_id) }}" target="_blank">
                                                        <img src="{{ asset('asset/images/icon/icon_account_chat.png') }}">
                                                    </a>
                                                    @if($key->status == 3)
                                                        <div class="text-right ml-auto">
                                                            <span class="progress-status status-blue">@lang('localize.ready_to_install')</span>
                                                            {{-- <a class="btn btn-detail" href="{{route('member-schedule-service', [$key->id])}}">Schedule Service</a> --}}
                                                        </div>
                                                        <div class="schedule-btn">
                                                            <a class="btn status" href="{{route('member-schedule-service-update-status', [$key->id, 4])}}">@lang('localize.installation_completed')</a>
                                                            {{--<a class="btn" href="{{route('member-schedule-service', [$key->id])}}">@lang('localize.reschedule_service')</a>--}}
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            @endforeach
                            {{ $schedules['confirmed']->links() }}
                        </div>
                    </div>
                    <div id="tab-completed" class="tab-pane {{ ($type == 'tab-completed') ? 'active' : '' }}">
                        <div class="mr-top-sm">
                            @foreach($schedules as $schedule => $values)
                                @if($schedule == 'completed')
                                    @foreach($values->sortByDesc('parent_order_id')->groupBy('parent_order_id') as $value => $keys)
                                        <div class="order-item">
                                            <div class="top-layer">
                                                <div>
                                                    <p class="order-id">@lang('localize.transaction_id'): {{$keys->first()->transaction_id}}</p>
                                                    <p>{{$keys[0]['order_date']}}</p>
                                                </div>
                                            </div>
                                            @foreach($keys as $key)
                                                <div class="product-layer">
                                                    <div class="d-flex">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <img class="product-image" src="{{ $key->mainImageUrl() }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
                                                        </a>
                                                        <div class="product-content">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <h6 class="product-title">{{$key->pro_title_en}}</h6>
                                                        </a>
                                                            <p class="info">
                                                                @lang('localize.service'): <span class="red">{{$key->service_name_current}}</span>
                                                            </p>
                                                            <p class="info">
                                                                @lang('localize.quantity'): <span class="red">{{$key->order_qty}}</span> | @lang('localize.total'): <span class="red">{{ rpFormat($key->product_price) }}</span>
                                                            </p>
                                                            <p class="info">
                                                                {{-- @lang('localize.schedule_date'): <span class="red">{{ Helper::UTCtoTZ($key->schedule_datetime,'j/m/Y g:i A') }}</span> --}}
                                                                @lang('localize.schedule_date'): <span class="red">{{ $key->schedule_datetime }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <a class="chat-btn" href="{{ route('chat-conversation', $key->pro_id) }}" target="_blank">
                                                        <img src="{{ asset('asset/images/icon/icon_account_chat.png') }}">
                                                    </a>
                                                    @if($key->status == 4)
                                                    <div class="text-right ml-auto">
                                                        <span class="progress-status status-green">@lang('localize.completed')</span>
                                                    </div>
                                                @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            @endforeach
                            {{ $schedules['completed']->links() }}
                        </div>
                    </div>
                    <div id="tab-cancelled" class="tab-pane {{ ($type == 'tab-cancelled') ? 'active' : '' }}">
                        <div class="mr-top-sm">
                            @foreach($schedules as $schedule => $values)
                                @if($schedule == 'cancelled')
                                    @foreach($values->sortByDesc('parent_order_id')->groupBy('parent_order_id') as $value => $keys)
                                        <div class="order-item">
                                            <div class="top-layer">
                                                <div>
                                                    <p class="order-id">@lang('localize.transaction_id'): {{$keys->first()->transaction_id}}</p>
                                                    <p>{{$keys[0]['order_date']}}</p>
                                                </div>
                                                {{-- <a class="btn btn-detail" href="{{route('member-schedule-service', [$value])}}">Schedule Service</a> --}}
                                            </div>
                                            @foreach($keys as $key)
                                                <div class="product-layer">
                                                    <div class="d-flex">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <img class="product-image" src="{{ $key->mainImageUrl() }}" onerror="this.onerror=null;this.src='/common/images/stock.png';"/>
                                                        </a>
                                                        <div class="product-content">
                                                        <a target="_blank" href="/product/{{ Helper::slug_maker($key->pro_title_en, $key->pro_id) }}">
                                                            <h6 class="product-title">{{$key->pro_title_en}}</h6>
                                                        </a>
                                                            <p class="info">
                                                                @lang('localize.service'): <span class="red">{{$key->service_name_current}}</span>
                                                            </p>
                                                            <p class="info">
                                                                @lang('localize.quantity'): <span class="red">{{$key->order_qty}}</span> | @lang('localize.total'): <span class="red">{{ rpFormat($key->product_price) }}</span>
                                                            </p>
                                                            <p class="info">
                                                                {{-- @lang('localize.schedule_date'): <span class="red">{{ Helper::UTCtoTZ($key->schedule_datetime,'j/m/Y g:i A') }}</span> --}}
                                                                @lang('localize.schedule_date'): <span class="red">{{ $key->schedule_datetime }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <a class="chat-btn" href="{{ route('chat-conversation', $key->pro_id) }}" target="_blank">
                                                        <img src="{{ asset('asset/images/icon/icon_account_chat.png') }}">
                                                    </a>
                                                    @if($key->status == -1)
                                                        <div class="text-right ml-auto">
                                                            <span class="progress-status status-green">@lang('localize.cancelled')</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            @endforeach
                            {{ $schedules['cancelled']->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
    <link href="/asset/css/dashboard.css?v4" rel="stylesheet">
    <style>
    .product-layer .chat-btn {
        right: 5px;
        position: absolute;
    }

    .product-layer .chat-btn img {
        height: 30px;
        width: 30px;
    }

    .product-layer .text-right {
        margin-top: 32px;
    }
    </style>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('backend/js/jquery-ui-1.10.4.min.js') }}"></script>
<script>
    $(document).ready(function() {
 
        $("#tab-unscheduled").click(function() {
            window.location.href = '{{url("account/general/schedule/tab-unscheduled")}}';
        });

        $("#tab-scheduled").click(function() {
            window.location.href = '{{url("account/general/schedule/tab-scheduled")}}';
        });

        $("#tab-confirmed").click(function() {
            window.location.href = '{{url("account/general/schedule/tab-confirmed")}}';
        });

        $("#tab-completed").click(function() {
            window.location.href = '{{url("account/general/schedule/tab-completed")}}';
        });

        $("#tab-cancelled").click(function() {
            window.location.href = '{{url("account/general/schedule/tab-cancelled")}}';
        });

    });
</script>
@endsection