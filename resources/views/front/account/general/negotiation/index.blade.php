@extends('layouts.front_master')

@section('title', trans('front.nav.account.general.order'))

@section('content')
    <div id="main">
        <div class="container">
            @include('layouts.partials.status')
            <div class="account-dashboard-container d-flex pd-y-md">
                @include('front.partial.nav_account')
                <div class="main-content-container">
                    <div class="top-box d-flex align-items-center mr-btm-sm">
                        <ul id="tabs_list" class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link {{ ($type == 'all') ? 'active' : '' }}" id="tab-all" href="{{ route('nego-history', ['all']) }}" data-content-id="tab-0">@lang('localize.all')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ ($type == 'active') ? 'active' : '' }}" id="tab-all" href="{{ route('nego-history', ['active']) }}" data-content-id="tab-0">@lang('localize.active')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ ($type == 'completed') ? 'active' : '' }}" id="tab-all" href="{{ route('nego-history', ['completed']) }}" data-content-id="tab-0">@lang('localize.completed')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ ($type == 'declined') ? 'active' : '' }}" id="tab-unpaid" href="{{ route('nego-history', ['declined']) }}" data-content-id="tab-0">@lang('localize.declined')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ ($type == 'expired') ? 'active' : '' }}" id="tab-expired" href="{{ route('nego-history', ['expired']) }}" data-content-id="tab-7">@lang('localize.expired')</a>
                            </li>

                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="tab-pending" class="tab-pane active">
                            <div class="mr-top-sm">
                                @foreach($negotiations as $negotiation)
                                    <div class="order-item">
                                        <div class="top-layer">
                                            <div>
                                                <p>
                                                    {{ \Helper::UTCtoTZ($negotiation->updated_at) }}
                                                    @if(!$negotiation->is_expired && in_array($negotiation->status, [10]))
                                                     | Time Left: <span class="red time-left">{{ $negotiation->expired_at }}</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <div>
                                                @if($negotiation->status == $negotiation::STATUS['ACTIVE'] && $negotiation->merchant_offer_price != null && !$negotiation->is_expired)
                                                    <a class="btn btn-sm btn-detail" href="{{ route('member-nego-accept', $negotiation) }}">Accept</a>
                                                    <a class="btn btn-sm btn-detail" href="{{ route('member-nego-reject', $negotiation) }}">Reject</a>
                                                @endif
                                                <a class="btn btn-sm btn-detail" href="{{ route('nego-detail', $negotiation) }}">Detail</a>
                                            </div>
                                        </div>
                                        <div class="product-layer">
                                            <div class="product-content-box d-flex">
                                                <a href="#"><img class="product-image" src="{{ $negotiation->product->mainImageUrl() }}" /></a>
                                                <div class="product-content">
                                                    <a href="#"><h6 class="product-title">{{ $negotiation->product->title }}</h6></a>
                                                    <p class="info">
                                                        Original Price: <span class="red">{{ rpFormat($negotiation->pricing->price) }}</span>
                                                    </p>
                                                    <p class="info">
                                                        QTY: <span class="red">{{ $negotiation->quantity }}</span> |
                                                        Nego Price: <span class="red">
                                                            @if($negotiation->merchant_offer_price)
                                                                {{ rpFormat($negotiation->merchant_offer_price) }}
                                                            @else
                                                                {{ rpFormat($negotiation->customer_offer_price) }}
                                                            @endif
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="product-status-box text-right ml-auto">
                                                <p>
                                                    @if($negotiation->is_expired)
                                                        <span class="progress-status status-red">{{ $negotiation::statusType()[$negotiation::STATUS['EXPIRED']] }}</span>
                                                    @elseif($negotiation->status == $negotiation::STATUS['ACTIVE'])
                                                        @if($negotiation->merchant_offer_price)
                                                            <span class="progress-status status-red">@lang('localize.awaiting_your_confirmation')</span>
                                                        @else
                                                            <span class="progress-status status-red">@lang('localize.awaiting_merchant_confirmation')</span>
                                                        @endif
                                                    @else
                                                        <span class="progress-status status-red">{{ $negotiation::statusType()[$negotiation->status] }}</span>
                                                    @endif
                                                    <br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            var time_left = [];
            setInterval(function() {
                $('.time-left').each(function (index) {
                    
                    if (typeof time_left[index] == 'undefined') {
                        time_left[index] = $(this).html();
                    }


                    var expired_at = time_left[index];

                    var now = moment().utc().format('YYYY-MM-DD HH:mm:ss');
                    var difference = new Date(expired_at).getTime() - new Date(now).getTime();
                    var days = Math.floor(difference / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + (24 * days);
                    var minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((difference % (1000 * 60)) / 1000);

                    $(this).html(hours + ':' + minutes + ':' + seconds);
                })

            }, 1000);
        });

    </script>
@endsection