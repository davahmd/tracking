@extends('layouts.front_master')

@section('title', trans('front.nav.account.general.order'))

@section('content')
    <div id="main">
        <div class="container">
            @include('layouts.partials.status')
            <div class="account-dashboard-container d-flex pd-y-md">
                @include('front.partial.nav_account')
                <div class="main-content-container">
                    <div class="order-top-layer shadow-sm mr-btm-sm">
                        <div>
                            <h6>Negotiation ID: #{{ $negotiation->id }}</h6>
                            <p>{{ \Helper::UTCtoTZ($negotiation->updated_at) }}</p>
                        </div>
                        <div>
                            @if($negotiation->status == $negotiation::STATUS['ACTIVE'] && $negotiation->merchant_offer_price && !$negotiation->is_expired)
                                <a href="{{ route('member-nego-accept', $negotiation) }}" class="btn btn-cancel">Accept</a>
                                <a href="{{ route('member-nego-reject', $negotiation) }}" class="btn btn-cancel">Reject</a>
                            @endif
                        </div>
                    </div>
                    <div class="mr-top-sm">
                        <div class="order-item">
                            <div style="padding: 10px 15px;">
                                <div class="top-layer">
                                    <div class="d-flex align-items-center">
                                        <p class="info" style="margin-right: 5px;">Status:</p>
                                            @if($negotiation->is_expired)
                                                <span class="progress-status status-red">{{ $negotiation::statusType()[$negotiation::STATUS['EXPIRED']] }}</span>
                                            @elseif($negotiation->status == $negotiation::STATUS['ACTIVE'])
                                                @if($negotiation->merchant_offer_price)
                                                    <span class="progress-status status-red">@lang('localize.awaiting_your_confirmation')</span>
                                                @else
                                                    <span class="progress-status status-red">@lang('localize.awaiting_merchant_confirmation')</span>
                                                @endif
                                            @else
                                                <span class="progress-status status-red">{{ $negotiation::statusType()[$negotiation->status] }}</span>
                                            @endif
                                    </div>
                                    <div class="d-flex align-items-right">
                                        <p class="info" style="margin-right: 5px;">
                                            @if(!$negotiation->is_expired && in_array($negotiation->status, [10]))
                                            Time Left: <span id="countdown" class="progress-status status-red"></span>
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="product-layer">
                                    <div class="d-flex">
                                        <a href="#"><img class="product-image" src="{{ $negotiation->product->mainImageUrl() }}" /></a>
                                        <div class="product-content">
                                            <a href="#">
                                                <h6 class="product-title">
                                                    {{ $negotiation->product->title }}
                                                </h6>
                                            </a>
                                            <p class="info">
                                                QTY: <span class="red">{{ $negotiation->quantity }}</span> |
                                                Total: <span class="red">
                                                    @if($negotiation->merchant_offer_price)
                                                        {{ rpFormat($negotiation->merchant_offer_price) }}
                                                    @else
                                                        {{ rpFormat($negotiation->customer_offer_price) }}
                                                    @endif
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseOne" style="border-top: 1px solid rgba(0,0,0,0.1);">
                                <div class="card-body">
                                    <ul>
                                        @if($negotiation->parent)
                                            <li>{{ \Helper::UTCtoTZ($negotiation->parent->created_at) }} - You negotiate with price {{ rpFormat($negotiation->parent->customer_offer_price) }}</li>
                                            @if($negotiation->parent->merchant_offer_price)
                                                <li>{{ \Helper::UTCtoTZ($negotiation->updated_at) }} - Merchant counter negotiate with price {{ rpFormat($negotiation->parent->merchant_offer_price) }}</li>
                                            @endif
                                        @endif
                                            <li>{{ \Helper::UTCtoTZ($negotiation->created_at) }} - You start negotiate with price {{ rpFormat($negotiation->customer_offer_price) }}</li>
                                            @if($negotiation->merchant_offer_price)
                                                <li>{{ \Helper::UTCtoTZ($negotiation->updated_at) }} - Merchant counter negotiate with price {{ rpFormat($negotiation->merchant_offer_price) }}</li>
                                            @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(!$negotiation->parent && $negotiation->status == $negotiation::STATUS['ACTIVE'] && $negotiation->merchant_offer_price && !$negotiation->final_price)
                        <div class="row mr-top-sm">
                            <div class="col-sm-6">
                                <div class="total-container">
                                    <h5>Counter Offer</h5>
                                    <div class="mr-top-sm">
                                        <form action="{{ route('member-nego-counter', $negotiation) }}" method="POST">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="counter_price">New Price</label>
                                                <input type="number" name="price" class="form-control">
                                            </div>
                                            <button class="btn btn-green">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        var expired_at = "{{ $negotiation->expired_at }}";

        var cd = setInterval(function() {

            var now = moment().utc().format('YYYY-MM-DD HH:mm:ss');

            var difference = new Date(expired_at).getTime() - new Date(now).getTime();
            var days = Math.floor(difference / (1000 * 60 * 60 * 24));
            var hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + (24 * days);
            var minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((difference % (1000 * 60)) / 1000);

            $('#countdown').text(hours + ':' + minutes + ':' + seconds);
        }, 1000);

    </script>
@endsection