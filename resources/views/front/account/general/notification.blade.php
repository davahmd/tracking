@extends('layouts.front_master')

@section('title', trans('localize.notification'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">

                <div class="topbar d-flex align-items-center mr-btm-sm mr-top-sm">
                    <h4 class="mr-auto">{{trans('localize.notification')}}</h4>
                    <div class="red-head"></div>
                </div>

                <div class="top-box d-flex align-items-center mr-btm-sm">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab-general">General</a>
                        </li>
                        {{--<li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-promo">News &amp; Promotion</a>
                        </li>--}}
                    </ul>
                    <form class="form-inline ml-auto" id="filter" action="{{ url( 'account/general/notification') }}" method="GET">
                        <input class="form-control" type="text" value="{{$input['name']}}" placeholder="{{ trans('localize.search') }}" id="name" name="name"/>
                        <a href="javascript:void(0)" id="submit-a"><i class="material-icons black">search</i></a>
                    </form>
                </div>

                <div class="alert alert-warning incoming-notification" style="display: none">
                    Received <b><span id="incoming-notification-counter">0</span></b> new notifications.
                    <a href="{{ request()->url() }}" class="btn btn-sm btn-warning">View</a>
                </div>


                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="tab-general" class="tab-pane active">

                        <div class="mr-top-sm">
                            @foreach($notifications as $notification)
                                @php
                                    eval('$notificationTitle = ' . $notification->data['trans_title'] . ";");
                                    eval('$notificationBody = ' . $notification->data['trans_body'] . ";");
                                @endphp
                                <div class="notification">
                                    <div class="inner">
                                        <a href="javascript:void(0)" class="thumb">
                                            <img class="notification-image" src="{{ $notification->data['image'] }}" onerror="this.onerror=null;this.src='/common/images/stock.png';">
                                        </a>
                                        <div class="notification-content">

                                            <a href="javascript:void(0)"><h6 class="notification-title">{{ $notificationTitle }}</h6></a>
                                            <p class="info">
                                                {!! $notificationBody !!}
                                            </p>
                                        </div>
                                    </div>
                                    @if($notification->data['type'] == 'service')
                                        <a class="btn ml-auto" href="{{ route('schedule') }}">Check Detail</a>
                                    @elseif($notification->data['type'] == 'transaction')
                                        <a class="btn ml-auto" href="{{ route('order-detail', $notification->data['transaction_id']) }}">Check Detail</a>
                                    @elseif($notification->data['type'] == 'price_negotiation')
                                        <a class="btn ml-auto" href="{{ route('nego-detail', $notification->data['negotiation_id']) }}">Check Detail</a>
                                    @endif
                                </div>
                            @endforeach
                            {{--
                            <div class="notification">
                                <div class="d-flex">
                                    <a href="javascript:void(0)"><img class="notification-image" src="{{ asset('asset/images/product/18.png') }}"></a>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)"><h6 class="notification-title">Reminder: Payment Required</h6></a>
                                        <p class="info">
                                            Please pay <b>Rp 38.000</b> for your orders <b>18062612024884J</b> &amp; <b>3</b> more by 28-07-2018 to avoid cancellation.<br />
                                            28-06-2018 08:03
                                        </p>
                                    </div>
                                </div>
                                <a class="btn ml-auto" href="javascript:void(0)">Check Detail</a>
                            </div>
                            --}}
                        </div>

                        <div class="pt-5 text-center">
                            {{ $notifications->links() }}
                        </div>
                    </div>

                    <div id="tab-promo" class="tab-pane">
                        <div class="mr-top-sm">
                            <div class="notification">
                                <div class="d-flex" style="padding-right: 15px;">
                                    <a href="promotion.html"><img class="notification-image-promo" src="../images/product/18.png" onerror="this.onerror=null;this.src='/common/images/stock.png';"/></a>
                                    <div class="notification-content">
                                        <a href="promotion.html"><h6 class="notification-title">Flash Sales June 2018 Special!</h6></a>
                                        <p class="notification-date">19 April 2018 20:22</p>
                                        <p class="notification-text">
                                            In this digital generation where information can be easily obtained within seconds. 
                                            In this digital generation where information can be easily obtained within seconds...
                                        </p>
                                    </div>
                                </div>
                                <a class="btn ml-auto" href="promotion.html">Check Detail</a>
                            </div>
                            <div class="notification">
                                <div class="d-flex" style="padding-right: 15px;">
                                    <a href="promotion.html"><img class="notification-image-promo" src="../images/product/18.png" onerror="this.onerror=null;this.src='/common/images/stock.png';"/></a>
                                    <div class="notification-content">
                                        <a href="promotion.html"><h6 class="notification-title">50% Discount for 1st Purchase</h6></a>
                                        <p class="notification-date">19 April 2018 20:22</p>
                                        <p class="notification-text">
                                            In this digital generation where information can be easily obtained within seconds. 
                                            In this digital generation where information can be easily obtained within seconds...
                                        </p>
                                    </div>
                                </div>
                                <a class="btn ml-auto" href="promotion.html">Check Detail</a>
                            </div>
                            <div class="notification">
                                <div class="d-flex" style="padding-right: 15px;">
                                    <a href="javascript:void(0)"><img class="notification-image-promo" src="../images/product/18.png" onerror="this.onerror=null;this.src='/common/images/stock.png';"/></a>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)"><h6 class="notification-title">Recommend a friend, get 10% Voucher Max Rp 200.000</h6></a>
                                        <p class="notification-date">19 April 2018 20:22</p>
                                        <p class="notification-text">
                                            In this digital generation where information can be easily obtained within seconds. 
                                            In this digital generation where information can be easily obtained within seconds...
                                        </p>
                                    </div>
                                </div>
                                <a class="btn ml-auto" href="javascript:void(0)">Check Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        document.getElementById("submit-a").onclick = function() {
            document.getElementById("filter").submit();
        }
        
        var notificationCounter = 0;

        //RUH 19072019 kill the socket.io
        {{--Echo.private('App.Models.Customer.' + "{{ auth()->id() }}")--}}
            {{--.notification((notification) => {--}}
                {{--console.log(notification);--}}
                {{--notificationCounter = notificationCounter + 1;--}}
                {{--$('#incoming-notification-counter').html(notificationCounter);--}}
                {{--$(".incoming-notification").show();--}}
            {{--});--}}
    </script>
@endsection