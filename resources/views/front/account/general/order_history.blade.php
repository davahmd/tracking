@extends('layouts.front_master')

@section('title', trans('front.nav.account.general.order'))

@section('content')
<div id="main">
    <div class="container">
        @include('layouts.partials.status')
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            <div class="main-content-container">
                <div class="top-box d-flex align-items-center mr-btm-sm">
                    <ul id="tabs_list" class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-all') ? 'active' : '' }}" id="tab-all" data-toggle="tab" href="#tab-all" data-content-id="tab-0">@lang('localize.all')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-unpaid') ? 'active' : '' }}" id="tab-unpaid" data-toggle="tab" href="#tab-unpaid" data-content-id="tab-0">@lang('localize.unpaid')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-installation') ? 'active' : '' }}" id="tab-installation" data-toggle="tab" href="#tab-installation" data-content-id="tab-7">@lang('localize.installation')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $type ? ($type == 'tab-processing') ? 'active' : '' : 'active'}}" id="tab-processing" data-toggle="tab" href="#tab-processing" data-content-id="tab-1">@lang('localize.processing')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-packaging') ? 'active' : '' }}" id="tab-packaging" data-toggle="tab" href="#tab-packaging" data-content-id="tab-2">@lang('localize.packaging')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-shipped') ? 'active' : '' }}" id="tab-shipped" data-toggle="tab" href="#tab-shipped" data-content-id="tab-3">@lang('localize.shipped')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-completed') ? 'active' : '' }}" id="tab-completed" data-toggle="tab" href="#tab-completed" data-content-id="tab-4">@lang('localize.completed')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ ($type == 'tab-cancelled') ? 'active' : '' }}" id="tab-cancelled" data-toggle="tab" href="#tab-cancelled" data-content-id="tab-5">@lang('localize.cancelled')</a>
                        </li>
                    </ul>
                </div>
                <!-- Tab panes -->
                @include('front.account.order.partial.partial_order_items', [
                    'type' => $type,
                    'tab_index' => $index
                ])
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {

        $("#tab-all").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-all") : url("account/general/order/history/tab-all")}}';
        });

        $("#tab-unpaid").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-unpaid") : url("account/general/order/history/tab-unpaid")}}';
        });

        $("#tab-installation").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-installation") : url("account/general/order/history/tab-installation")}}';
        });

        $("#tab-processing").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-processing") : url("account/general/order/history/tab-processing")}}';
        });

        $("#tab-packaging").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-packaging") : url("account/general/order/history/tab-packaging")}}';
        });

        $("#tab-shipped").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-shipped") : url("account/general/order/history/tab-shipped")}}';
        });

        $("#tab-completed").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-completed") : url("account/general/order/history/tab-completed")}}';
        });

        $("#tab-cancelled").click(function() {
            window.location.href = '{{ request()->routeIs('order-history-wholesale') ? url("account/general/order/wholesale/history/tab-cancelled") : url("account/general/order/history/tab-cancelled")}}';
        });
        // $('#tabs_list a').each(function() {
        //     console.log($(this).data('content-id'));
        //     if (!document.getElementById($(this).data('content-id'))) {
        //         var element = document.createElement("div");
        //         element.id = $(this).data('content-id');
        //         element.className = 'tab-pane';
        //         element.appendChild(document.createTextNode('No Record'));
        //         document.getElementById('contents').appendChild(element);
        //     }
        // });

        // $("#tabs_list li").click(function() {
        //     var url = document.location.hash;
        //     // console.log(url);
        //     console.log($('#tabs_list li a.active').text());

        // });
        // $('#tabs_list a').tabs({
        //     select: function(event, ui){
        //         console.log(event);
        //     }
        // });
        // var url = document.location.href+"&success=yes";
        // document.location = url;
    });
</script>
@endsection