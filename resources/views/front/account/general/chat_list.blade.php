@extends('layouts.front_master')

@section('title', trans('localize.chat'))

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
                <div class="topbar d-flex align-items-center mr-btm-sm mr-top-sm">
                    <h4 class="mr-auto">{{trans('localize.chat')}}</h4>
                    <div class="red-head"></div>
                </div>
                <div class="top-box d-flex justify-content-end mr-btm-sm">
                    <!-- <h4 class="mr-auto">@lang('localize.chat')</h4> -->
                    {{-- <form class="form-inline">
                        <input class="form-control" type="text" placeholder="@lang('localize.search')" />
                        <a href="javascript:void(0)"><i class="material-icons black">@lang('localize.search')</i></a>
                    </form> --}}
                </div>
                <div class="chat-container">
                    @foreach ($store_groups as $store_group)
                        @php
                            if (count($store_group) == 0) {
                                continue;
                            }
                            $store_group = $store_group->sortByDesc('id');
                            $store_chat = $store_group->first();
                            $store_unread = $store_group->where('sender_type', 'S')->where('read_at', null)->first() ? true : false;
                        @endphp
                        
                        <div id="chat_store_{{ $store_chat->store_id }}">
                            <div class="chat-item" data-target="#chat_store_prod_{{ $store_chat->store_id }}" aria-expanded="false">
                                <a href="javascript:void(0)">
                                    <div class="d-flex pd-sm">
                                        <div class="chat-image">
                                            <img src="{{ \Storage::url('store/'.$store_chat->store_id.'/'.$store_chat->store->stor_img) }}" onerror="this.src = '{{ asset('asset/images/logo/logo_merchant.png') }}'">
                                        </div>
                                        <div class="chat-content flex-grow-1">
                                            <h6 class="chat-title">{{ $store_chat->store->stor_name ?? 'n/a' }}</h6>
                                            <p class="chat-text">
                                                @php
                                                    $name = $store_chat->sender_type == 'U' ? $store_chat->customer->cus_name : $store_chat->store->stor_name;
                                                    $msg = $store_chat->message ?? '';
                                                    $timestamp = \App\Helpers\DateTimeHelper::systemToOperationDateTime($store_chat->created_at, 'j/m/Y g:h A');
                                                @endphp
                                                {{ $name }}: {!! preg_replace('/[\r\n]/', '<br>', str_limit($msg ?? '', 100, '...')) !!}
                                            </p>
                                            <p class="info">{{ $timestamp }}</p>
                                        </div>
                                    </div>
                                </a>

                                <div class="collapse" id="chat_store_prod_{{ $store_chat->store_id }}">
                                    @php
                                        $store_group = $store_group->groupBy('pro_id')->all();
                                    @endphp

                                    @foreach ($store_group as $product_group)
                                        @php
                                            if (count($product_group) == 0) {
                                                continue;
                                            }
                                            $product_group = $product_group->sortByDesc('id');
                                            $product_chat = $product_group->first();
                                            $product_unread = $product_group->where('sender_type', 'S')->where('read_at', null)->first() ? true : false;
                                        @endphp

                                        <div id="chat_store_{{ $product_chat->store_id }}_cus_{{ $product_chat->cus_id }}_pro_{{ $product_chat->pro_id }}" class="chat-item">
                                            <a class="chat-open" href="{{ route('chat-conversation', ['product_id' => $product_chat->pro_id]) }}" data-store="{{ $product_chat->store_id }}" data-cus="{{ $product_chat->cus_id }}" data-pro="{{ $product_chat->pro_id }}" target="_blank">
                                                <div class="d-flex pd-sm">
                                                    <div class="chat-image sub-image">
                                                        <img src="{{ $product_chat->product->mainImageUrl() }}" onerror="this.src = '{{ asset('asset/images/product/default.jpg') }}'">
                                                    </div>
                                                
                                                    <div class="chat-content flex-grow-1">
                                                        <h6 class="chat-title">
                                                            {{ $product_chat->product->title }}
                                                        </h6>
                                                        <p class="chat-text">
                                                            @php
                                                                $msg = $product_chat->message ?? '';
                                                                $timestamp = \App\Helpers\DateTimeHelper::systemToOperationDateTime($product_chat->created_at, 'j/m/Y g:h A');
                                                            @endphp
                                                            {{ $name }}: {!! preg_replace('/[\r\n]/', '<br>', str_limit($msg ?? '', 100, '...')) !!}
                                                        </p>
                                                        <p class="info">{{ $timestamp }}</p>
                                                    </div>
                                                </div>
                                            </a>
                                            <span class="notice{{ $product_unread ? '' : ' d-none' }}"></span>
                                        </div>
                                    @endforeach
                                </div>
                                <span class="notice{{ $store_unread ? '' : ' d-none' }}"></span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.chat-open').click((e) => {
        chatModule.customerOpenChat(e);
    });

    $('.chat-item').click((e) => {
        let target = $(e.currentTarget).data('target');
        $(target).collapse('toggle');
        e.stopPropagation();
    });
</script>
@endsection


