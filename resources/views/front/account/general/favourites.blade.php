@extends('layouts.front_master')

@section('title', 'Overview')

@section('content')
<div id="main">
    <div class="container">
        <div class="account-dashboard-container d-flex pd-y-md">
            @include('front.partial.nav_account')
            
            <div class="main-content-container">
                <ul class="nav nav-pills mr-btm-sm">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tab-product">Favourite Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-merchant">Followed Merchants</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="tab-product" class="tab-pane active">
                        <!-- <div class="top-filter-box d-flex align-items-center mr-btm-sm">
                            <a href="javascript:void(0)" class="btn add-all"><i class="material-icons">shopping_cart</i> Add all to cart</a>
                            <div class="form-inline ml-auto">
                                <label>Sort by&nbsp;&nbsp;</label>
                                <select class="form-control" id="sorting">
                                    <option>Latest Update</option>
                                    <option>Most Popular</option>
                                    <option>Most Reviews</option>
                                    <option>Top Recommended</option>
                                </select>
                            </div>
                        </div> -->

                        <div class="item-container side-listing">
                                <div class="card">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top" src="{{ asset('asset/images/product/5.png') }}" alt="Card image">
                                    </a>
                                    <div class="card-body">
                                        <span class="discount-rate bg-orange">44% OFF</span>
                                        <a href="javascript:void(0)"><h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6></a>
                                        <div class="d-flex flex-column" style="width: 100%;">
                                            <p class="card-old-price">RP 650.000</p>
                                            <p class="card-text red">RP 510.000</p>
                                            <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                        <a href="javascript:void(0)" class="add-to-cart">Add to Cart&nbsp;<span><i class="fa fa-shopping-cart"></i></span></a>
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top" src="{{ asset('asset/images/product/10.png') }}" alt="Card image">
                                    </a>
                                    <div class="card-body">
                                        <span class="discount-rate bg-orange">44% OFF</span>
                                        <a href="javascript:void(0)"><h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6></a>
                                        <div class="d-flex flex-column" style="width: 100%;">
                                            <p class="card-old-price">RP 650.000</p>
                                            <p class="card-text red">RP 510.000</p>
                                            <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                        <a href="javascript:void(0)" class="add-to-cart">Add to Cart&nbsp;<span><i class="fa fa-shopping-cart"></i></span></a>
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top" src="{{ asset('asset/images/product/20.png') }}" alt="Card image">
                                    </a>
                                    <div class="card-body">
                                        <span class="discount-rate bg-orange">44% OFF</span>
                                        <a href="javascript:void(0)"><h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6></a>
                                        <div class="d-flex flex-column" style="width: 100%;">
                                            <p class="card-old-price">RP 650.000</p>
                                            <p class="card-text red">RP 510.000</p>
                                            <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                        <a href="javascript:void(0)" class="add-to-cart">Add to Cart&nbsp;<span><i class="fa fa-shopping-cart"></i></span></a>
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top" src="{{ asset('asset/images/product/6.png') }}" alt="Card image">
                                    </a>
                                    <div class="card-body">
                                        <span class="discount-rate bg-orange">44% OFF</span>
                                        <a href="javascript:void(0)"><h6 class="card-title">OMP Velocita 350mm Black Leather Steering Wheel</h6></a>
                                        <div class="d-flex flex-column" style="width: 100%;">
                                            <p class="card-old-price">RP 650.000</p>
                                            <p class="card-text red">RP 510.000</p>
                                            <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                        <a href="javascript:void(0)" class="add-to-cart">Add to Cart&nbsp;<span><i class="fa fa-shopping-cart"></i></span></a>
                                    </div>
                                </div>
                                
                        </div>
                    </div>

                    <div id="tab-merchant" class="tab-pane">
                        <div class="mr-top-sm">
                            <div class="item-merchant">
                                <div class="item-merchant-top">
                                    <div class="d-flex align-items-center mr-auto">
                                        <a href="javascript:void(0)">
                                            <div class="profile-image">
                                                <img class="item-merchant-logo" src="../images/brand/discbrake/1.png" />
                                            </div>
                                        </a>
                                        <div>
                                            <a href="javascript:void(0)"><h6 class="item-merchant-title">Brembo Official Store</h6></a>
                                            <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <a class="btn" href="javascript:void(0)">Following</a>
                                </div>
                                <div class="item-merchant-bottom">
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/6.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/6.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/6.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/6.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/6.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-merchant">
                                <div class="item-merchant-top">
                                    <div class="d-flex align-items-center mr-auto">
                                        <a href="javascript:void(0)">
                                            <div class="profile-image">
                                                <img class="item-merchant-logo" src="../images/brand/windowfilm/1.jpg" />
                                            </div>
                                        </a>
                                        <div>
                                            <a href="javascript:void(0)"><h6 class="item-merchant-title">Iceberg Window Films</h6></a>
                                            <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <a class="btn" href="javascript:void(0)">Following</a>
                                </div>
                                <div class="item-merchant-bottom">
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/19.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/20.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <img src="../images/product/19.png" />
                                        <p class="merchant-product-price">RP 47.000</p>
                                        <p class="merchant-product-discount">25% OFF</p>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/20.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/19.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-merchant">
                                <div class="item-merchant-top">
                                    <div class="d-flex align-items-center mr-auto">
                                        <a href="javascript:void(0)">
                                            <div class="profile-image">
                                                <img class="item-merchant-logo" src="../images/brand/tyre/6.png" />
                                            </div>
                                        </a>
                                        <div>
                                            <a href="javascript:void(0)"><h6 class="item-merchant-title">GoodYear Official Store</h6></a>
                                            <span class="card-rating">
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star shine"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <a class="btn" href="javascript:void(0)">Following</a>
                                </div>
                                <div class="item-merchant-bottom">
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/tyre/1.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/tyre/2.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/tyre/3.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/tyre/4.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                    <div class="merchant-product flex-fill">
                                        <a href="javascript:void(0)">
                                            <img src="../images/product/tyre/5.png" />
                                            <p class="merchant-product-price">RP 47.000</p>
                                            <p class="merchant-product-discount">25% OFF</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection