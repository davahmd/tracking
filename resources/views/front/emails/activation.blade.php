@extends('layouts.front_email')

@section('content')
<tr>
    <td bgcolor="#ffffff" align="center" style="padding: 15px;">
        <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
                                @lang('email.member.account_activation.title', ['mall_name' => trans('common.mall_name')])
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                @lang('email.member.account_activation.msg', ['mall_name' => trans('common.mall_name')])
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="padding-top: 25px;" class="padding">
                                            <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                                <tr>
                                                    <center style="margin:5px 0 15px">
                                                        <a href="{{ $activation_link }}" style="background-color:#ff1818; border: 0; border-radius: 8px; padding: 14px 60px; font-weight: bold; color: #FFF; text-decoration: none;">@lang('email.member.account_activation.btn', ['mall_name' => trans('common.mall_name')])</a>
                                                    </center>
                                                    <p><span style="display:inline-block;height:10px;width:10px;background:#ff1818;margin-right:5px;"></span> Zona Team</p>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
@stop
