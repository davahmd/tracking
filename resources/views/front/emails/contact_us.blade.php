@extends('layouts.front_email')
@section('content')
<tr>
    <td width="50"></td>
    <td valign="middle" width="450">
        <p style="font-size:18px; color:#4a4a4a;">@lang('email.member.contact.greeting', ['cus_name' => $details['name']]),</p>
        <div style="font-size:15px;color:#4a4a4a">
            <p>@lang('email.member.contact.msg')</p>

            <center style="background-color:#f8f8f8;padding:20px;">
            <p>@lang('email.member.contact.details')</p>
            <p>
                <strong>@lang('email.member.contact.subject')</strong> : {{$details['subject']}}<br><br>
                <strong>@lang('email.member.contact.content')</strong> : {{$details['content']}}<br>
            </p>
            </center>

            <p><span style="display:inline-block;height:10px;width:10px;background:#ff1818;margin-right:5px;"></span> Zona Team</p>
        </div>
    </td>
    <td width="50"></td>
</tr>
<br>
@endsection
