@extends('layouts.front_email')
@section('content')
<tr>
    <td bgcolor="#ffffff" align="center" style="padding: 15px;">
        <table border="0" cellpadding="0" cellspacing="0" width="1000" class="responsive-table">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <!-- COPY -->
                            <td style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">We have received your order!</td>
                        </tr>
                        <tr>
                            <td align="left" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                Thank you for shopping with @lang('common.mall_name'). You shall receive a status update within 3 working day, if not your order will be canceled and Credit will be refunded.
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding: 20px 0 0 0; font-size: 14px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                <strong> Transaction ID : </strong> {{ $transaction->transaction_id }} <br>
                                <strong> Date : </strong> {{ \Helper::UTCtoTZ($transaction->date, 'd M Y H:i:s A') }} <br>
                                <strong> Ship To : </strong><br>
                                @if($shipping)
                                {!! ucwords(implode('<br>', array_map('trim', array_filter([$shipping->ship_name, $shipping->ship_address1, $shipping->ship_address2, $shipping->ship_city_name])))) !!} <br>
                                {{ ucwords(trim(implode(', ', array_filter([$shipping->ship_postalcode, $shipping->state? $shipping->state->name : null, $shipping->country? $shipping->country->co_name : null])))) }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br>
                                <table style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" width="100%" cellpadding="5">
                                    <tr>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>@lang('common.credit_name')</th>
                                        <th>Total</th>
                                    </tr>
                                    <tr><td colspan="7"><hr/></td></tr>
                                    @foreach ($orders as $order)
                                    <tr>
                                        <td>
                                            {{ $order->product }}
                                            @if($order->options)
                                            <p><br>
                                            @foreach ($order->options as $parent => $child)
                                                <b>{{ $parent }} : </b> {{ $child }} @if(!$loop->last)<br>@endif
                                            @endforeach
                                            </p>
                                            @endif
                                        </td>
                                        <td>{{ $order->quantity }}</td>
                                        <td>{{ number_format($order->credit, 4) }}</td>
                                        <td>{{ number_format($order->total_credit, 4) }}</td>
                                    </tr>
                                    @endforeach
                                    <tr><td colspan="7"><br></td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </td>
</tr>
@stop
