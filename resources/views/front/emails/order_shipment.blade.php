@extends('layouts.front_email')
@section('content')
<tr>
    <td bgcolor="#ffffff" align="center" style="padding: 15px;">
        <table border="0" cellpadding="0" cellspacing="0" width="1000" class="responsive-table">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="font-size: 27px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Your Order is on the way!</td>
                        </tr>
                        <tr>
                            <td align="left" style="padding: 15px 0 5px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                Here is your shipment details :
                            </td>
                        </tr>
                        <tr>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;">
                                <tr>
                                    <th style="width:10%;">DO. Number</th>
                                    <th>:</th>
                                    <td class="text-left">{{ $delivery->do_number() }}</td>
                                </tr>
                                @if(!$delivery->courier_id && !$delivery->tracking_number)
                                <tr>
                                    <th style="width:10%;">Courier</th>
                                    <th>:</th>
                                    <td class="text-left">Self Pickup</td>
                                </tr>
                                <tr>
                                    <th>Appointment Detail</th>
                                    <th>:</th>
                                    <td class="text-left">{{ $delivery->appointment_detail }}</td>
                                </tr>
                                @else
                                <tr style="width:10%;">
                                    <th>Courier</th>
                                    <th>:</th>
                                    <td class="text-left">{{ $delivery->courier? $delivery->courier->name : '' }}</td>
                                </tr>
                                <tr>
                                    <th>Tracking Number</th>
                                    <th>:</th>
                                    <td class="text-left">{{ $delivery->tracking_number }}</td>
                                </tr>
                                <tr>
                                    <th>Tracking URL</th>
                                    <th>:</th>
                                    <td class="text-left">{{ $delivery->courier? $delivery->courier->link : '' }}</td>
                                </tr>
                                @endif
                                @if($shipping)
                                <tr style="width:10%;">
                                    <th>Bill To</th>
                                    <th>:</th>
                                    <td class="text-left">
                                        <b>{{ $shipping->ship_name }}</b><br>
                                        {!! ucwords(implode('<br>', array_map('trim', array_filter([$shipping->ship_address1, $shipping->ship_address2, $shipping->ship_city_name])))) !!} <br>
                                        {{ ucwords(trim(implode(', ', array_filter([$shipping->ship_postalcode, $shipping->state? $shipping->state->name : null, $shipping->country? $shipping->country->co_name : null])))) }}
                                    </td>
                                </tr>
                                @endif
                            </table>
                        </tr>
                        <tr>
                            <td>
                                <br>
                                <table style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" width="100%" cellpadding="5">
                                    <tr>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th nowrap>Price</th>
                                        <th>Total</th>
                                    </tr>
                                    <tr><td colspan="5"><hr/></td></tr>
                                    @foreach ($delivery->items as $order)
                                    <tr>
                                        <td>
                                            {{ $order->product? $order->product->pro_title_en : '' }}
                                            @if($order->options)
                                            <p><br>
                                            @foreach ($order->options as $parent => $child)
                                                <b>{{ $parent }} : </b> {{ $child }} @if(!$loop->last)<br>@endif
                                            @endforeach
                                            </p>
                                            @endif
                                        </td>
                                        <td>{{ $order->order_qty }}</td>
                                        <td>{{ number_format($order->total()['product_price'], 2) }}</td>
                                        <td> {{ number_format($order->total()['price'], 2) }}</td>
                                    </tr>
                                    @endforeach
                                    <tr><td colspan="5"><br></td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
@stop
