@extends('layouts.front_master')

@section('content')
<div id="main" class="index-container"> 
    <div class="page-title">
        <div class="container">
            <h1>About Us</h1>
        </div>
    </div>
    <div class="generic-container">
        <div class="container">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac sollicitudin leo. Aenean in vehicula mi. Ut at est at sapien fringilla euismod a ut justo. Ut non tristique tortor. Nunc condimentum euismod leo facilisis dapibus. Curabitur cursus diam purus, id volutpat enim tempor id. Donec in est nibh. Vestibulum cursus mattis lectus et blandit. Praesent lectus ante, tincidunt sit amet arcu ac, ullamcorper cursus tellus.</p>
            <p>Nullam ultrices lorem nunc, non viverra mi sodales sed. Integer ac elit id massa dapibus eleifend et ultrices sem. Maecenas et pulvinar eros, non blandit ante. Cras imperdiet iaculis diam nec congue. Duis feugiat aliquam nibh in fringilla. Nulla ut lacinia metus. Aliquam vestibulum ligula neque, id faucibus magna eleifend at. Nam nec nisi consectetur, semper nisl nec, condimentum est. Duis eget enim eget nunc tincidunt feugiat. Pellentesque ultricies lectus tellus, quis porttitor elit pulvinar non. Donec dignissim nibh vel vehicula pretium. Proin finibus metus eu auctor fermentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem velit, molestie ut dignissim commodo, maximus et ipsum. Integer tortor lectus, aliquam sed magna sit amet, bibendum pretium nunc. Sed elementum ex sit amet turpis mollis, ut consequat urna congue.</p>
            <p>Proin malesuada felis eros. Nunc faucibus orci ipsum, nec rutrum odio fermentum ac. Vestibulum condimentum cursus odio, vitae fringilla magna tempus nec. Donec et nulla magna. Suspendisse vel lacinia magna. Suspendisse mattis pulvinar ex, id aliquet ipsum pellentesque ut. Suspendisse tempus libero sed neque suscipit, eu mattis ante faucibus. Sed scelerisque interdum magna nec interdum. Nullam interdum nisi sit amet tristique pulvinar.</p>
            <p>Phasellus posuere porta purus, et tempus sem sollicitudin sit amet. Vestibulum ex sem, fringilla eget velit at, tempus laoreet justo. Mauris augue ex, porttitor at egestas vitae, aliquam eget arcu. Duis vestibulum pellentesque mauris nec pellentesque. Aenean in malesuada odio, non semper lacus. Phasellus ac lobortis purus. Sed sodales odio ipsum, eu ullamcorper justo malesuada id. Duis blandit velit aliquam massa malesuada, sed imperdiet orci commodo. Praesent tellus nulla, mollis at felis et, aliquet accumsan ligula. Nunc ac molestie nisi. Fusce pharetra elit ornare, sollicitudin felis euismod, tincidunt diam. Integer feugiat vehicula risus id malesuada. Praesent condimentum magna tortor, sed convallis est convallis ut.</p>
        </div>
    </div>
</div>
@endsection
