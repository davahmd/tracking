@extends('layouts.front_master')
@section('title', $cms->cp_title_en)

@section('content')
<div id="main" class="index-container"> 
    <div class="page-title">
        <div class="container">
            @if (Session::get('lang') == 'en')
                <h1>{{ $cms->cp_title_en }}</h1>
            @else
                <h1>{{ $cms->cp_title_id }}</h1>
            @endif
        </div>
    </div>
    <div class="generic-container">
        <div class="container">
            @if (Session::get('lang') == 'en')
                {!! $cms->cp_description_en !!}    
            @else
                {!! $cms->cp_description_id !!}
            @endif
        </div>
    </div>
</div>
@endsection