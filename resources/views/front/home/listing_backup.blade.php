@extends('layouts.front_master')
@section('header')
    @include('layouts.web.header.main')
@endsection

@section('title', trans('front.section.'.$type.'_products'))

@section('style')
<style type="text/css">
#loader {
    color: red;
}
</style>
@endsection


@section('content')
<div id="main" class="index-container">
    <div class="recommend-products-container">
        <div class="container pd-y-lg">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('front.section.'.$type.'_products')</h4>
                <div class="red-head"></div>
            </div>
            <div id="recent-products" class="item-container">

                @foreach($products as $product)
                <div class="card">
                    <a href="/product/{{ Helper::slug_maker($product->pro_title_en, $product->pro_id) }}">
                        <div class="card-thumb">
                            <img class="card-img-top" src="{{ \Storage::url('product/' . $product->pro_mr_id . '/' . $product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                        </div>
                        <div class="card-body">
                            @if ($product->pro_qty < 1)
                            <span class="sold-out-tag">@lang('localize.outStock')</span>
                            @endif
                            <h6 class="card-title">{{ $product->title }}</h6>
                            <p class="card-text red">{{ rpFormat($product->lowest_price) }}</p>
                            {{-- <span class="card-rating">
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star"></i>
                            </span> --}}
                        </div>
                    </a>
                </div>
                {{-- @break($loop->iteration == 24) --}}
                @endforeach

            </div>

            <br>

            <div id='loader' class='text-center'>
                <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
            </div>

            <div class="btmbar d-flex justify-content-center">
                <a href="javascript:void(0)" id="load-more-recent" class="btn btn-red">Load More</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){

    function renderCardView(id, title, price, image, slug, quantity) {
        return "<div class='card'>" +
            "<a href='/product/" + slug + "'>" +
            "<div class='card-thumb'>" +
            "<img class='card-img-top' src='" + image + "' onerror='this.onerror=null;this.src=`/asset/images/product/default.jpg`;'></div>" +
            "<div class='card-body'>" +
            ((quantity == 0) ? "<span class='sold-out-tag'>@lang('localize.outStock')</span>" : "") +
            "<h6 class='card-title'>" + title + "</h6>" +
            "<p class='card-text red'>" + price + "</p>" +
            "</div>" +
            "</a>" +
            "</div>";
    }

    function loadMoreProducts(skip, take)
    {
        if (skip + take > 100) {
            take = 100 - skip;
        }

        $.get('/get_more_products/{{$type}}', {skip: skip, take: take}, function(){

        }).done(function(products){
            $('#loader').hide();

            if (!jQuery.isEmptyObject(products)) {
                if (skip + take < 100) {
                    $('#load-more-recent').show();
                }

                $.each(products, function($key, product) {
                    var s3_image = "{{ \Storage::url('product/') }}" + product.merchant_id + "/" + product.image;
                    $('#recent-products').append(renderCardView(product.id,product.title, product.price, s3_image, product.slug, product.quantity));
                });
            }
            else {
                $('#load-more-recent').hide();
            }
        });

    }

    $('#loader').hide();

    var skip = 24;

    $('#load-more-recent').click(function(){

        $('#loader').show();
        $('#load-more-recent').hide();

        loadMoreProducts(skip, 24);

        skip += 24;
    });

});
</script>
@endsection