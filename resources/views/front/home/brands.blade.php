@extends('layouts.front_master')
@section('header')
@include('layouts.web.header.main')
@endsection

@section('title', 'Featured Products')

@section('style')
<style type="text/css">
    #loader {
        color: red;
    }
</style>
@endsection


@section('content')
<div id="main" class="index-container" style="height: 600px;">
    <div class="official-merchant-container container pd-y-lg">
        <div class="topbar d-flex">
            <h4>@lang('localize.official_brands')</h4>
            <div class="red-head"></div>
        </div>
        <div class="merchant-container brands">

            @foreach($official_brands as $brand)
            <div class="card">
                <a href="/official-brand/{{ $brand->brand_slug }}">
                    <img class="card-img" src="{{ \Storage::url('officialbrands/' . $brand->brand_Img) }}" onerror="this.onerror=null;this.src='/asset/images/brand/default.jpg';">
                </a>
            </div>
            @break($loop->iteration == 8)
            @endforeach

        </div>

    </div>
</div>
@endsection