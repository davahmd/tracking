@extends('layouts.front_master')
@section('header')
@include('layouts.web.header.main')
@endsection

@section('title', 'Featured Products')

@section('style')
<style type="text/css">
    #loader {
        color: red;
    }
</style>
@endsection


@section('content')
<div id="main" class="index-container">
    <div class="official-merchant-container container pd-y-lg">
        <div class="topbar d-flex">
            <h4>@lang('front.section.featured_stores')</h4>
            <div class="red-head"></div>
        </div>
        <div class="merchant-container stores">

            @foreach($featured_stores as $store)
            <div class="card">
                <a href="/store/{{ $store->url_slug }}">
                    <img class="card-img" src="{{ $store->getImageUrl() }}" onerror="this.onerror=null;this.src='/asset/images/brand/default.jpg';">
                </a>
            </div>
            @break($loop->iteration == 8)
            @endforeach

        </div>
         
    </div>
</div>
@endsection