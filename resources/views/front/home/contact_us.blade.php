@extends('layouts.front_master')

@section('title', trans('localize.contactus'))

@section('content')
<div id="main" class="index-container">
        
    <div class="page-title">
        <div class="container">
            <h1>@lang('localize.contactus')</h1>
        </div>
    </div>

    <div class="generic-container">
        <div class="container">
            
            <div class="contact-detail">
                <div class="list">
                    <img src="{{ asset('asset/images/icon/icon_address.png') }}">
                    <div class="list-content">
                        <label>@lang('localize.address')</label>
                        <p>Kl Mega Kuningan Lot 5.1 Menara Rajawali Lt 11, Kuningan Timur</p>
                    </div>
                </div>
                <div class="list">
                    <img src="{{ asset('asset/images/icon/icon_email.png') }}">
                    <div class="list-content">
                        <label>@lang('localize.email')</label>
                        <p>info@zona.id</p>
                    </div>
                </div>
                <div class="list">
                    <img src="{{ asset('asset/images/icon/icon_phone.png') }}">
                    <div class="list-content">
                        <label>@lang('localize.phone')</label>
                        <p>021 5761605</p>
                    </div>
                </div>
                <div class="list">
                    <img src="{{ asset('asset/images/icon/icon_fax.png') }}">
                    <div class="list-content">
                        <label>@lang('localize.fax')</label>
                        <p>021 5761609</p>
                    </div>
                </div>
            </div>
            
            <div class="contact-form">
                @include('front.common.notifications')
                <form action="/contact" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label">@lang('localize.name')</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">@lang('localize.email')</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
                <div class="form-group">
                        <label class="control-label">@lang('localize.subject')</label>
                        <input type="text" class="form-control" name="subject" maxlength="100" value="{{ old('subject') }}">
                    </div>
                <div class="form-group">
                    <label class="control-label">@lang('localize.message')</label>
                    <textarea class="form-control" rows="10" name="content">{{ old('content') }}</textarea>
                </div>
                
                <div class="action">
                    <button type="submit" class="btn">@lang('localize.send')</button>
                </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
@endsection
