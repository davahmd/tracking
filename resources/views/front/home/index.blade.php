@extends('layouts.front_master')

@section('title', trans('localize.home'))

@section('style')
<style type="text/css">
#loader {
    color: red;
}

.distributor-check:not([href]):not([tabindex]) {
    position: absolute;
    z-index: 1;
    top: 5px;
    right: 10px;
    padding: .25rem .4rem;
    border-radius: 50%;
    color: #ffffff;
    font-size: 0.7em;
}
</style>
@endsection

@section('content')
<div id="main" class="index-container">
    <div class="main-slide">
        <div id="banner-slide" class="banner-container carousel slide" data-ride="carousel">
            <!-- The slideshow -->
            <div class="carousel-inner">
                @foreach($sliders as $slider)
                <div class="carousel-item {{ $loop->first?'active':'' }}">
                    <img src="{{ \Storage::url('banner/' . $slider->bn_img) }}" onerror="this.onerror=null;this.src='/asset/images/banner/default.jpg';">
                </div>
                @endforeach
            </div>
            <div class="banner-promo-container bg-white">
                <div class="container d-flex align-items-center">
                    <div class="bullets mr-auto">
                    <!-- Indicators -->
                    <ul class="carousel-indicators carousel-indicators-bullet">
                        @foreach($sliders as $slider)
                        <li data-target="#banner-slide" data-slide-to="{{ $loop->index }}" class="{{ $loop->first?'active':'' }}"></li>
                        @endforeach
                    </ul>
                    </div>
                    {{-- <a class="view-more" href="javascript:void(0)">@lang('front.nav.see_all_promo')</a> --}}
                </div>
            </div>
        </div>
        <div class="side-banner">
            <a href="javascript:void(0)">
                <div class="list">
                    <img src="{{ asset('/asset/images/side_banner/bca_loan.jpg') }}">
                </div>
            </a>
            <a href="javascript:void(0)">
                <div class="list">
                    <img src="{{ asset('/asset/images/side_banner/insurance_innovators_indo.jpg') }}">
                </div>
            </a>
            <a href="javascript:void(0)">
                <div class="list">
                    <img src="{{ asset('/asset/images/side_banner/auto_paint.jpg') }}">
                </div>
            </a>
        </div>
    </div>
    <div class="featured-product-container container pd-top-lg pd-btm-md">
        <div class="topbar d-flex">
            <h4>@lang('front.section.featured_products')</h4>
            <div class="red-head"></div>
        </div>
        <div class="item-container">

            @foreach($featured_products as $featured_product)
                <div class="card">
                    <a href="/product/{{ Helper::slug_maker($featured_product->pro_title_en, $featured_product->pro_id) }}">
                        <div class="card-thumb">
                            @if (array_key_exists($featured_product->pro_id, $promo_featured_products) && ($promo_item_info[$featured_product->pro_id]->limit > $promo_item_info[$featured_product->pro_id]->count) && ($promo_featured_products[$featured_product->pro_id]->started_at <= $today_datetime && $promo_featured_products[$featured_product->pro_id]->ended_at >= $today_datetime))
                                <span class="discount-rate bg-orange">{{ $promo_featured_products[$featured_product->pro_id]->discount_rate ? 100*($promo_featured_products[$featured_product->pro_id]->discount_rate) . '% OFF' : rpFormat($promo_featured_products[$featured_product->pro_id]->discount_value) . ' OFF' }}</span>
                            @endif
                            <img class="card-img-top" src="{{ \Storage::url('product/' . $featured_product->pro_mr_id . '/' . $featured_product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                        </div>
                        <div class="card-body">
                            @if ($featured_product->pro_qty < 1)
                            <span class="sold-out-tag">@lang('localize.outStock')</span>
                            @endif
                            <h6 class="card-title">{{ $featured_product->title }}</h6>
                            <div class="d-flex flex-column">
                                {{-- <div class="price-layer">
                                    <span class="with_discounted" style="display: {{ ($featured_product->pricing->first()->getPurchasePrice() <> $featured_product->pricing->first()->price)? 'block' : 'none' }};">
                                        <h6 class="cut black">
                                            <small>
                                                <span class="discounted_price">{{ rpFormat($featured_product->pricing->first()->price) }}</span>
                                            </small>
                                        </h6>
                                        <h5 class="red">
                                            <div>
                                                <span class="discounted_purchase_price">{{ rpFormat($featured_product->pricing->first()->getPurchasePrice()) }}</span>
                                            </div>
                                        </h5>
                                    </span>
                                    <h5 class="red">
                                        <span class="without_discounted" style="display: {{ ($featured_product->pricing->first()->getPurchasePrice() == $featured_product->pricing->first()->price)? 'block' : 'none' }};">
                                            <div>
                                                <span class="discounted_purchase_price">{{ rpFormat($featured_product->pricing->first()->getPurchasePrice()) }}</span>
                                            </div>
                                        </span>
                                    </h5>
                                </div> --}}
                                <div class="price-layer">
                                @if (array_key_exists($featured_product->pro_id, $promo_featured_products) && ($promo_item_info[$featured_product->pro_id]->limit > $promo_item_info[$featured_product->pro_id]->count) && ($promo_featured_products[$featured_product->pro_id]->started_at <= $today_datetime && $promo_featured_products[$featured_product->pro_id]->ended_at >= $today_datetime))
                                    <span class="with_discounted">
                                        <h5 class="red">
                                            <div>
                                                <span class="discounted_purchase_price">{{ $promo_featured_products[$featured_product->pro_id]->discount_rate ? rpFormat($featured_product->pricing->first()->price *(1-$promo_featured_products[$featured_product->pro_id]->discount_rate)) : rpFormat($featured_product->pricing->first()->price - $promo_featured_products[$featured_product->pro_id]->discount_value) }}</span>
                                            </div>
                                        </h5>
                                        <h6 class="cut black">
                                            <small>
                                                <span class="discounted_price">{{ rpFormat($featured_product->pricing->first()->price) }}</span>
                                            </small>
                                        </h6>
                                    </span>
                                @else
                                    <h5 class="red">
                                        <span class="without_discounted">
                                            <div>
                                                <span class="discounted_purchase_price">{{ rpFormat($featured_product->pricing->first()->price) }}</span>
                                            </div>
                                        </span>
                                    </h5>
                                @endif
                                </div>
                                {{--<p class="card-text red">{{ rpFormat($featured_product->lowest_price) }}</p>--}}
                                {{-- <span class="card-rating">
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star shine"></i>
                                    <i class="fa fa-star"></i>
                                </span> --}}
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach

        </div>
        <div class="more">
            <a href="/featured-products" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
        </div>
    </div>
    @if ($flash_products)
    <div class="flash-sales-container">
        <div class="container pd-y-lg">
            <div class="topbar">
                <img src="/asset/images/icon/icon_flash_sale.png" class="icon-flashsale">
                <h4>Flash Sale </h4>
                {{-- <h4>Flash Sale <small class="badge">Spesial Mudik 18 Mei 2018</small></h4> --}}
                <!--div class="red-head"></div-->
                {{-- <div class="disp-countdown">
                    <span class="pd-x-sm black"><i class="fas fa-stopwatch"></i></span>
                    <div class="bg-black">
                        <span class="disp-hr">00</span>
                        <span class="disp-gap">:</span>
                        <span class="disp-min">00</span>
                        <span class="disp-gap">:</span>
                        <span class="disp-sec">00</span>
                    </div>
                </div> --}}
            </div>
            <div class="item-container">
                @foreach ($flash_products as $flash_product)
                    @if ($flash_product->pro_qty > 0)
                    <div class="card">
                        <a href="/product/{{ Helper::slug_maker($flash_product->pro_title_en, $flash_product->pro_id) }}">
                            <div class="card-thumb">
                                <span class="discount-rate bg-orange">{{ $flash_promotion_info[$flash_product->pro_id]->discount_rate ? 100*($flash_promotion_info[$flash_product->pro_id]->discount_rate) . '% OFF' : rpFormat($flash_promotion_info[$flash_product->pro_id]->discount_value) . ' OFF' }}</span>
                                <img class="card-img-top" src="{{ \Storage::url('product/' . $flash_product->pro_mr_id . '/' . $flash_product->mainImage['image']) }}" alt="Card image">
                            </div>
                            @if ($flash_product->pricing->first())
                            <div class="card-body">
                                {{-- @if ($flash_product_info[$flash_product->pro_id]->limit <= $flash_product_info[$flash_product->pro_id]->count)
                                <span class="sold-out-tag">@lang('localize.outStock')</span>
                                @endif --}}
                                <h6 class="card-title">{{ $flash_product->title }}</h6>
                                <div class="d-flex flex-column">
                                    <div class="price-layer">
                                        <span class="with_discounted">
                                            <h5 class="red">
                                                <div>
                                                    <span class="discounted_purchase_price">{{ $flash_promotion_info[$flash_product->pro_id]->discount_rate ? rpFormat($flash_product->pricing->first()->price *(1-$flash_promotion_info[$flash_product->pro_id]->discount_rate)) : rpFormat($flash_product->pricing->first()->price - $flash_promotion_info[$flash_product->pro_id]->discount_value) }}</span>
                                                </div>
                                            </h5>
                                            <h6 class="cut black">
                                                <small>
                                                    <span class="discounted_price">{{ rpFormat($flash_product->pricing->first()->price) }}</span>
                                                </small>
                                            </h6>
                                        </span>
                                    </div>
                                </div>
                                <p class="product-left-amount">
                                <span class="red">
                                    @if ($flash_product->pro_qty > ($flash_product_info[$flash_product->pro_id]->limit - $flash_product_info[$flash_product->pro_id]->count))
                                        {{ $flash_product_info[$flash_product->pro_id]->limit - $flash_product_info[$flash_product->pro_id]->count }}
                                    @else
                                        {{ $flash_product->pro_qty }}
                                    @endif
                                </span> Barang Tersisa!
                                </p>
                            </div>
                            @endif
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="more">
                <a href="/flashsale-products" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
    @endif
    <div class="trending-product-container container pd-y-md">
        <div class="topbar d-flex">
            <h4>@lang('front.section.trending_products')</h4>
            <div class="red-head"></div>
        </div>
        <div class="item-container">

            @foreach($trending_products as $trending_product)
            <div class="card">
                <a href="/product/{{ Helper::slug_maker($trending_product->pro_title_en, $trending_product->pro_id) }}">
                    <div class="card-thumb">
                        @if (array_key_exists($trending_product->pro_id, $promo_trending_products) && ($promo_item_info[$trending_product->pro_id]->limit > $promo_item_info[$trending_product->pro_id]->count) && ($promo_trending_products[$trending_product->pro_id]->started_at <= $today_datetime && $promo_trending_products[$trending_product->pro_id]->ended_at >= $today_datetime))
                            <span class="discount-rate bg-orange">{{ $promo_trending_products[$trending_product->pro_id]->discount_rate ? 100*($promo_trending_products[$trending_product->pro_id]->discount_rate) . '% OFF' : rpFormat($promo_trending_products[$trending_product->pro_id]->discount_value) . ' OFF' }}</span>
                        @endif
                        {{-- <span class="discount-rate bg-orange">Negotiable!</span> --}}
                        <img class="card-img-top" src="{{ \Storage::url('product/' . $trending_product->pro_mr_id . '/' . $trending_product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                    </div>
                    <div class="card-body">
                        @if ($trending_product->pro_qty < 1)
                        <span class="sold-out-tag">@lang('localize.outStock')</span>
                        @endif
                        <h6 class="card-title">{{ $trending_product->title }}</h6>
                        <div class="d-flex flex-column">
                            {{-- <div class="price-layer">
                                <span class="with_discounted" style="display: {{ ($trending_product->pricing->first()->getPurchasePrice() <> $trending_product->pricing->first()->price)? 'block' : 'none' }};">
                                    <h6 class="cut black">
                                        <small>
                                            <span class="discounted_price">{{ rpFormat($trending_product->pricing->first()->price) }}</span>
                                        </small>
                                    </h6>
                                    <h5 class="red">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($trending_product->pricing->first()->getPurchasePrice()) }}</span>
                                        </div>
                                    </h5>
                                </span>
                                <h5 class="red">
                                    <span class="without_discounted" style="display: {{ ($trending_product->pricing->first()->getPurchasePrice() == $trending_product->pricing->first()->price)? 'block' : 'none' }};">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($trending_product->pricing->first()->getPurchasePrice()) }}</span>
                                        </div>
                                    </span>
                                </h5>
                            </div> --}}
                            <div class="price-layer">
                            @if (array_key_exists($trending_product->pro_id, $promo_trending_products) && ($promo_item_info[$trending_product->pro_id]->limit > $promo_item_info[$trending_product->pro_id]->count) && ($promo_trending_products[$trending_product->pro_id]->started_at <= $today_datetime && $promo_trending_products[$trending_product->pro_id]->ended_at >= $today_datetime))
                                <span class="with_discounted">
                                    <h5 class="red">
                                        <div>
                                            <span class="discounted_purchase_price">{{ $promo_trending_products[$trending_product->pro_id]->discount_rate ? rpFormat($trending_product->pricing->first()->price *(1-$promo_trending_products[$trending_product->pro_id]->discount_rate)) : rpFormat($trending_product->pricing->first()->price - $promo_trending_products[$trending_product->pro_id]->discount_value) }}</span>
                                        </div>
                                    </h5>
                                    <h6 class="cut black">
                                        <small>
                                            <span class="discounted_price">{{ rpFormat($trending_product->pricing->first()->price) }}</span>
                                        </small>
                                    </h6>
                                </span>
                            @elseif (Auth::check())
                                <h5 class="red">
                                    <span class="without_discounted">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($trending_product->pricing->first()->price) }}</span>
                                        </div>
                                    </span>
                                </h5>
                            @endif
                            </div>
                            {{--<p class="card-text red">{{ rpFormat($trending_product->lowest_price) }}</p>--}}
                            {{-- <span class="card-rating">
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star"></i>
                            </span> --}}
                        </div>
                    </div>
                </a>
            </div>
            @break($loop->iteration == 6)
            @endforeach

        </div>
        <div class="more">
            <a href="/trending-products" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
        </div>
    </div>
    <div class="official-merchant-container container pd-y-lg">
        <div class="topbar d-flex">
            <h4>@lang('front.section.featured_stores')</h4>
            <div class="red-head"></div>
        </div>
        <div class="merchant-container stores">

            @foreach($featured_stores as $store)
            <div class="card">
                <a href="/store/{{ $store->url_slug }}">
                    <img class="card-img" src="{{ $store->getImageUrl() }}" onerror="this.onerror=null;this.src='/asset/images/brand/default.jpg';">
                </a>
            </div>
            @break($loop->iteration == 8)
            @endforeach

        </div>
        <div class="more">
            <a href="/featured-stores" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
        </div>
    </div>

    @if(auth()->user() && auth()->user()->merchant)
    <div class="official-merchant-container container pd-y-lg">
        <div class="topbar d-flex">
            <h4>@lang('front.section.distributors')</h4>
            <div class="red-head"></div>
        </div>
        <div class="merchant-container stores">

            @include('front.merchant.partials.merchant-list', ['merchants' => $distributors])
            {{--@foreach($distributors as $distributor)--}}
                {{--<div class="card">--}}
                    {{--<span class="distributor-check bg-green"><i class="fa fa-check"></i></span>--}}
                    {{--<a href="/merchants/{{ $distributor->mer_id }}">--}}
                        {{--<img class="card-img" src="" onerror="this.onerror=null;this.src='/asset/images/brand/default.jpg';">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--@endforeach--}}

        </div>
        <div class="more">
            <a href="{{ url('/merchants/distributors') }}" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
        </div>
    </div>
    @endif

    <div class="official-merchant-container container pd-y-lg">
        <div class="topbar d-flex">
            <h4>@lang('front.section.official_brands')</h4>
            <div class="red-head"></div>
        </div>
        <div class="merchant-container brands">

            @foreach($official_brands as $brand)
            <div class="card">
                <a href="/official-brand/{{ $brand->brand_slug }}">
                    <img class="card-img" src="{{ $brand->logoPath }}" onerror="this.onerror=null;this.src='/asset/images/brand/default.jpg';">
                </a>
            </div>
            @break($loop->iteration == 8)
            @endforeach

        </div>
        <div class="more">
            <a href="/official-brands" class="view-more">@lang('front.nav.see_more')&ensp;<span><i class="fa fa-arrow-right"></i></span></a>
        </div>
    </div>
    <div class="category-collections-container">
        <div class="container pd-y-lg">
            <div class="topbar d-flex">
                <h4>@lang('front.section.featured_categories')</h4>
                <div class="red-head"></div>
            </div>
            <div class="feature-category-container">
                @foreach($featured_categories as $featured_category)
                    <div class="card cc-bg-1">
                        <a href="/category/{{$featured_category->url_slug}}">
                        <div class="card-thumb">
                        <img class="card-img-top" src="{{ \Storage::url('category/image/' . $featured_category->image) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                        </div>
                        <div class="card-img-overlay">
                            <h6 class="card-title">{{ $featured_category->name_en }}</h6>
                            {{-- <p class="card-text">Reduce Interior Heat &amp; Fading</p> --}}
                        </div>
                        </a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    <div class="recommend-products-container">
        <div class="container pd-y-lg">
            <div class="topbar d-flex">
                <h4 class="mr-auto">@lang('front.section.latest_products')</h4>
                <div class="red-head"></div>
            </div>
            <div id="recent-products" class="item-container">

                @foreach($latest_products as $latest_product)
                <div class="card">
                    <a href="/product/{{ Helper::slug_maker($latest_product->pro_title_en, $latest_product->pro_id) }}">
                        <div class="card-thumb">
                            @if (array_key_exists($latest_product->pro_id, $promo_latest_products) && ($promo_item_info[$latest_product->pro_id]->limit > $promo_item_info[$latest_product->pro_id]->count) && ($promo_latest_products[$latest_product->pro_id]->started_at <= $today_datetime && $promo_latest_products[$latest_product->pro_id]->ended_at >= $today_datetime))
                                <span class="discount-rate bg-orange">{{ $promo_latest_products[$latest_product->pro_id]->discount_rate ? 100*($promo_latest_products[$latest_product->pro_id]->discount_rate) . '% OFF' : rpFormat($promo_latest_products[$latest_product->pro_id]->discount_value) . ' OFF' }}</span>
                            @endif
                            <img class="card-img-top" src="{{ \Storage::url('product/' . $latest_product->pro_mr_id . '/' . $latest_product->mainImage['image']) }}" onerror="this.onerror=null;this.src='/asset/images/product/default.jpg';">
                        </div>
                        <div class="card-body">
                            @if ($latest_product->pro_qty < 1)
                            <span class="sold-out-tag">@lang('localize.outStock')</span>
                            @endif
                            <h6 class="card-title">{{ $latest_product->title }}</h6>
                            {{-- <div class="price-layer">
                                <span class="with_discounted" style="display: {{ ($latest_product->pricing->first()->getPurchasePrice() <> $latest_product->pricing->first()->price)? 'block' : 'none' }};">
                                    <h6 class="cut black">
                                        <small>
                                            <span class="discounted_price">{{ rpFormat($latest_product->pricing->first()->price) }}</span>
                                        </small>
                                    </h6>
                                    <h5 class="red">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($latest_product->pricing->first()->getPurchasePrice()) }}</span>
                                        </div>
                                    </h5>
                                </span>
                                <h5 class="red">
                                    <span class="without_discounted" style="display: {{ ($latest_product->pricing->first()->getPurchasePrice() == $latest_product->pricing->first()->price)? 'block' : 'none' }};">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($latest_product->pricing->first()->getPurchasePrice()) }}</span>
                                        </div>
                                    </span>
                                </h5>
                            </div> --}}
                            <div class="price-layer">
                            @if (array_key_exists($latest_product->pro_id, $promo_latest_products) && ($promo_item_info[$latest_product->pro_id]->limit > $promo_item_info[$latest_product->pro_id]->count) && ($promo_latest_products[$latest_product->pro_id]->started_at <= $today_datetime && $promo_latest_products[$latest_product->pro_id]->ended_at >= $today_datetime))
                                <span class="with_discounted">
                                    <h5 class="red">
                                        <div>
                                            <span class="discounted_purchase_price">{{ $promo_latest_products[$latest_product->pro_id]->discount_rate ? rpFormat($latest_product->pricing->first()->price *(1-$promo_latest_products[$latest_product->pro_id]->discount_rate)) : rpFormat($latest_product->pricing->first()->price - $promo_latest_products[$latest_product->pro_id]->discount_value) }}</span>
                                        </div>
                                    </h5>
                                    <h6 class="cut black">
                                        <small>
                                            <span class="discounted_price">{{ rpFormat($latest_product->pricing->first()->price) }}</span>
                                        </small>
                                    </h6>
                                </span>
                            @elseif (Auth::check())
                                <h5 class="red">
                                    <span class="without_discounted">
                                        <div>
                                            <span class="discounted_purchase_price">{{ rpFormat($latest_product->pricing->first()->price) }}</span>
                                        </div>
                                    </span>
                                </h5>
                            @endif
                            </div>
                            {{--<p class="card-text red">{{ rpFormat($latest_product->lowest_price) }}</p>--}}
                            {{-- <span class="card-rating">
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star shine"></i>
                                <i class="fa fa-star"></i>
                            </span> --}}
                        </div>
                    </a>
                </div>
                @break($loop->iteration == 24)
                @endforeach

            </div>

            <br>

            <div id='loader' class='text-center'>
                <span><i class='fa fa-spinner fa-pulse fa-2x'></i></span>
            </div>

            <div class="btmbar d-flex justify-content-center">
                <a href="javascript:void(0)" id="load-more-recent" class="btn btn-red">@lang('front.button.load_more')</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){

    function renderCardView(id, title, price, image, slug, quantity) {
        return "<div class='card'>" +
            "<a href='/product/" + slug + "'>" +
            "<div class='card-thumb'>" +
            "<img class='card-img-top' src='" + image + "' onerror='this.onerror=null;this.src=`/asset/images/product/default.jpg`;'></div>" +
            "<div class='card-body'>" +
            ((quantity == 0) ? "<span class='sold-out-tag'>@lang('localize.outStock')</span>" : "") +
            "<h6 class='card-title'>" + title + "</h6>" +
            "@if (Auth::check())<p class='card-text red'>" + price + "</p> @endif" +
            "</div>" +
            "</a>" +
            "</div>";
    }

    function loadRecentProducts(skip, take)
    {
        if (skip + take > 100) {
            take = 100 - skip;
        }

        $.get('/get_recent_products', {skip: skip, take: take}, function(){

        }).done(function(products){
            $('#loader').hide();

            if (!jQuery.isEmptyObject(products)) {
                if (skip + take < 100) {
                    $('#load-more-recent').show();
                }

                $.each(products, function($key, product) {
                    var s3_image = "{{ \Storage::url('product/') }}" + product.merchant_id + "/" + product.image;
                    $('#recent-products').append(renderCardView(product.id,product.title, product.price, s3_image, product.slug, product.quantity));
                });
            }
            else {
                $('#load-more-recent').hide();
            }
        });

    }

    $('#loader').hide();

    var skip = 24;

    $('#load-more-recent').click(function(){

        $('#loader').show();
        $('#load-more-recent').hide();

        loadRecentProducts(skip, 24);

        skip += 24;
    });

});
</script>
@endsection
