<!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('asset/images/favicon.png') }}">
    <title> {{ config('app.name') }} - @yield('title')</title>
    
    {{-- Bootstrap --}}
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-reboot.css') }}">

    {{-- Fontawesome --}}
    <link rel="stylesheet" href="{{ asset('asset/css/fontawesome/all.css') }}">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> -->

    <link rel="stylesheet" href="{{ asset('asset/css/admin_login.css') }}">
    @yield('style')
    </head>
    <body>
        @yield('content')
        
        <script src="{{ asset('asset/js/jquery-3.3.1.js') }}"></script>
        <script type="text/javascript" src="{{ asset('asset/js/popper.js') }}"></script>

        <script type="text/javascript" src="{{ asset('asset/js/bootstrap/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('asset/js/bootstrap/bootstrap.bundle.js') }}"></script>

        <script type="text/javascript" src="{{ asset('asset/js/fontawesome/all.js') }}"></script>

        <script src="{{ asset('asset/js/dropdown_bs4.js') }}"></script>
        <script src="{{ asset('asset/js/custom_select.js') }}"></script>
        <script src="{{ asset('asset/js/zona.app.js') }}"></script>
        @yield('scripts')
    </body>
</html>