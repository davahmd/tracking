<!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="country_state_lib_version" content="{{ Cache::get('country_state_lib_version_number') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> {{ config('app.name') }} - @yield('title')</title>

    <link rel="icon" type="image/png" href="{{ asset('asset/images/favicon.png') }}">

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-reboot.css') }}">

    {{-- Fontawesome --}}
    <link rel="stylesheet" href="{{ asset('asset/css/fontawesome/all.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('asset/css/default.css?v2.3') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/master.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/dropdown_bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/dashboard.css?v=1.1') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/custom_select.css') }}">

    <link href="{{ asset('common/lib/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/bootstrap-fileinput.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/tagsinput.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}">

    <style>
        .cart-group {
            width: 250px;
        }
    </style>

    @yield('style')
    <style type="text/css">
        pre.sf-dump {
            overflow: visible;
        }
    </style>

    <style type="text/css">
        .twitter-typeahead .tt-hint {
    display: none;
}


#search {
    background-color: #eee !important;
    border-radius: 18px !important;
    height: 40px;
    width: 250px;
    margin-top: 18px;
    outline: none;
    border: 0;
    padding: 20px;
    margin-right: 15px;
}

.search .suggestion a {
    text-decoration: none;
    color: #333;
}

.twitter-typeahead .tt-menu {
    margin-top: 4px;
    width: 350px;
}

.tt-dataset {
    background-color: #fff;
    border-radius: 15px;
    border: 1px solid #eee;
}

.search .suggestion {
    /*width: 350px;*/
    background-color: transparent;
  /*margin-left: 0;*/
}

.search .suggestion:hover {
    background-color: transparent;
}

.search {
    border-bottom: 1px solid #eee;
}

.search:hover {
    background-color: #eee;
}

.search:last-child {
    border-bottom: none;
}

.search:first-child:hover {
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
}

.search:last-child:hover {
    border-bottom-left-radius: 15px;
    border-bottom-right-radius: 15px;
}

.search .suggestion .image {
    text-align: center;
    padding: 10px 20px;
}

.search .suggestion .info {
  line-height: 1;
  text-align: left;
  padding: 20px;
  font-size: 1rem;
}

.search a {
    width: 100%;
}

.search .suggestion .tagline {
  font-size: 12px;
  font-weight: 200;
}

.search .suggestion .secondary {
  font-weight: 12px;
}

.loading .fa-spinner {
    margin: 20px;
}

.loading {
    margin: 0 auto;
    text-align: center;
}

/* Steps */
.step {
  position: relative;
  min-height: 1em;
  color: gray;
}

.step + .step {
  margin-top: 1.5em
}

.step > div:first-child {
  position: static;
  height: 0;
}

.step > div:not(:first-child) {
  margin-left: 1.5em;
  padding-left: 1em;
}

.step.step-active {
  color: #ff1818;
}

.step.step-active .circle {
  background-color: #ff1818;
}

/* Circle */
.circle {
  background: gray;
  position: relative;
  width: 1.5em;
  height: 1.5em;
  line-height: 1.5em;
  border-radius: 100%;
  color: #fff;
  text-align: center;
  box-shadow: 0 0 0 3px #fff;
}

/* Vertical Line */
.circle:after {
  content: ' ';
  position: absolute;
  display: block;
  top: 1px;
  right: 50%;
  bottom: 1px;
  left: 50%;
  height: 100%;
  width: 1px;
  transform: scale(1, 2);
  transform-origin: 50% -100%;
  background-color: rgba(0, 0, 0, 0.25);
  z-index: -1;
}

.step:last-child .circle:after {
  display: none
}

/* Stepper Titles */
.title {
  line-height: 1.5em;
  font-weight: bold;
}

.caption {
  font-size: 0.8em;
}

.description {
  
}
    </style>

    </head>
    <body>
    <div class="wrapper">
        <div id="header" class="bg-zona">
            <div class="container">
                <div class="zona-logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('asset/images/logo/logo.png') }}" />
                    </a>
                </div>
                <button class="navbar-toggler">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div id="navb">
                    <div class="menu-title"></div>
                    <ul class="navbar-primary">
                        <li class="dropdown category-expand">
                            <a class="dropdown-toggle" href="javascript:void(0)" >@lang('front.nav.category')</a>

                            <!-- wide category dropdown menu -->
                            <div class="dropdown-menu">
                                <div class="menu-back-toggler">back</div>
                                <div class="container">

                                    <div class="wide-menu">

                                        <div class="row">
                                        @foreach($categories as $key => $category)
                                        <div class="box1 col-md-2">
                                            <a href="/category/{{$category->url_slug}}"><h6>{{ $category->name }}</h6></a>
                                            <ul class="nav flex-column">
                                                @foreach($category->children as $subcat)
                                                <li>
                                                    <a href="/category/{{$subcat->url_slug}}">{{ $subcat->name }}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </li>
                        {{-- <li>
                            <a href="javascript:void(0)">@lang('front.nav.service')</a>
                        </li> --}}
                        <li>
                            <a href="{{ route('official-brands') }}">@lang('front.nav.official_brand')</a>
                        </li>
                        <li>
                            <a href="{{ route('news_promotions') }}">@lang('front.nav.news_promotions')</a>
                        </li>
                    </ul>
                </div>
                <div class="nav-others">
                    <form class="form-inline" method="post" action="/search">
                        {{ csrf_field() }}
                        <span class="material-icons close-search">close</span>
                        <input class="form-control" type="text" name="search" placeholder="{{ trans('front.nav.search_in_zona') }}" value="{{ !empty($search) ? $search : '' }}" autocomplete="off">
                        <button type="submit"><img src="{{ asset('asset/images/icon/icon_search.png') }}" /></button>
                    </form>


                    <ul class="navbar-secondary">
                        <li class="search-toggler">
                            <a href="javascript:void(0)">
                                <img src="{{ asset('asset/images/icon/icon_search.png') }}">
                            </a>
                        </li>
                        <li class="dropdown language-dropdown">
                            <a href="javascript:void(0)" data-toggle="dropdown">
                                <img src="{{ asset('asset/images/icon/icon_globe.png') }}">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="/home/setlocale?lang=en">@lang('common.english')</a>
                                <a class="dropdown-item" href="/home/setlocale?lang=ind">@lang('common.indonesia')</a>
                            </div>
                        </li>
                        @if(Auth::check())
                            @if(auth()->user()->merchant)

                                <li class="dropdown">
                                    <a href="javascript:void(0)" id="account-dropdown" data-toggle="dropdown">
                                        <img src="{{ asset('asset/images/icon/icon_cart.png') }}">
                                        @if ($carts->count() || $wholesaleCarts->count())
                                        <span class="notice"></span>
                                        @endif
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right cart-group">
                                        <a class="dropdown-item" href="{{ route('cart') }}">@lang('localize.cart') ({{ $carts->count() }} @lang('localize.items'))</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('wholesale-cart') }}">Wholesale Cart ({{ $wholesaleCarts->count() }} @lang('localize.items'))</a>
                                    </div>
                                </li>
                            @else
                                <li>
                                    <a href="{{ route('cart') }}">
                                        <img src="{{ asset('asset/images/icon/icon_cart.png') }}">
                                        @if ($carts->count() || $wholesaleCarts->count())
                                            <span class="notice"></span>
                                        @endif
                                    </a>
                                </li>
                            @endif
                            <li id="notifications">
                                <a href="{{ route('notification') }}">
                                    <img src="{{ asset('asset/images/icon/icon_notification.png') }}">
                                    @if(auth()->user()->unreadNotifications()->count() > 0)
                                    <span class="notice"></span>
                                    @endif
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" id="account-dropdown" data-toggle="dropdown">
                                    <img src="{{ asset('asset/images/icon/icon_account.png') }}">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">{{ isset($customer->cus_name)?$customer->cus_name:null }}</a>
                                    <a class="dropdown-item" href="{{ route('overview') }}"> @lang('localize.myaccount')</a>
                                    @if(auth()->user()->merchant)
                                    <a class="dropdown-item" href="{{ url('merchant') }}">@lang('localize.mymerchant')</a>
                                    @elseif(auth()->user()->storeUser)
                                    <a class="dropdown-item" href="{{ url('store') }}">@lang('localize.mystore')</a>
                                    @endif
                                    {{-- <a class="dropdown-item" href="javascript:void(0)">@lang('localize.Settings')</a> --}}
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item logout" href="{{ route('logout') }}">@lang('localize.logout')</a>
                                </div>
                            </li>
                        @endif
                    </ul>

                    @if(!Auth::check())
                    <ul class="navbar-auth">
                        <li>
                            <a class="btn" href="{{ route('login') }}">@lang('front.button.login')</a>
                        </li>
                        <li>
                            <a class="btn" href="{{ route('register') }}">@lang('front.button.register')</a>
                        </li>
                    </ul>
                    @endif
                </div>
            </div>
        </div>

        @yield('content')
    </div>

        <div id="footer" class="bg-dark">
            <div class="container">
                <div class="footer-container pd-y-lg pd-x-sm white">
                    <div class="subscribe-container mr-auto">
                        {{--  <h6>@lang('footer.subscribe_via_email')</h6>
                        <p>@lang('footer.subscribe_text')</p>
                        <form class="form-inline">
                            <input class="form-control mr-sm-2" type="text" placeholder="{{ trans('localize.email') }}">
                            <a class="btn btn-red" href="javascript:void(0)">@lang('footer.subscribe')</a>
                        </form>  --}}
                    </div>
                    {{-- <div class="contact-container">
                        <p class="ph_no">021 5761605</p>
                        <a href="javascript:void(0)"><p class="email">info@zona.id</p></a>
                        <p class="location">Kl Mega Kuningan Lot 5.1 Menara Rajawali Lt 11, Kuningan Timur</p>
                    </div>
                    <div class="sitemap-1">
                        <a href="{{ route('contact-us') }}"><p>@lang('footer.contactUs')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.payment')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.shippingDelivery')</p></a>
                        <a href="javascript:void(0)"><p>@lang('footer.returns')</p></a>
                        <a href="{{ route('faq') }}"><p>@lang('footer.faq')</p></a>
                        <a href="{{ route('help') }}"><p>@lang('footer.guide')</p></a>
                    </div> --}}
                    <div class="sitemap-2">
                        @foreach ($footer_cms as $cms)
                            <a href="/info/{{ $cms->cp_url }}">
                                @if (Session::get('lang') == 'en')
                                <p>{{ $cms->cp_title_en }}</p>    
                                @else
                                <p>{{ $cms->cp_title_id }}</p>
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="footer-container-2 bg-red pd-x-sm white">
                <div class="container">
                    <div class="copy-container mr-auto">
                        @lang('footer.copyright')
                    </div>
                    <div class="social-container">
                        <ul>
                            <li>
                                <a href="javascript:void(0)"><i class="fab fa-facebook-square"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fab fa-twitter-square"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fab fa-google-plus-square"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div id="blueimp-gallery" class="blueimp-gallery" style="display:none;">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">x</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>

        <script type="text/javascript" src="{{ asset('asset/js/jquery-3.3.1.js') }}"></script>

        <script type="text/javascript" src="{{ asset('asset/js/popper.js') }}"></script>

        <script type="text/javascript" src="{{ asset('asset/js/bootstrap/bootstrap.js') }}"></script>
        {{-- <script type="text/javascript" src="{{ asset('asset/js/bootstrap/bootstrap.bundle.js') }}"></script> --}}

        <script type="text/javascript" src="{{ asset('asset/js/fontawesome/all.js') }}"></script>

        <script src="{{ asset('asset/js/dropdown_bs4.js') }}"></script>
        <script src="{{ asset('asset/js/custom_select.js') }}"></script>
        <script src="{{ asset('asset/js/zona.app.js') }}"></script>

        <script type="text/javascript" src="{{ asset('backend/js/plugins/iCheck/icheck.min.js') }}"></script>

        {{-- <script type="text/javascript" src="{{ asset('common/js/country_state_f2.js') }}"></script> --}}
        <script type="text/javascript" src="{{ asset('common/js/common_function.js') }}"></script>

        <script type="text/javascript" src="{{ asset('common/lib/js/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('common/lib/js/bootstrap-fileinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('common/lib/js/sweetalert.min.js') }}"></script>
        <script src="//momentjs.com/downloads/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
        <script type="text/javascript" src="{{ asset('js/echo.js') . '?v2' }}"></script>
        <script type="text/javascript" src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('common/lib/js/typeahead.bundle.min.js') }}"></script>

        <script>
            $(document).ready(function() {
              var users = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                  url: '/search?q=%QUERY',
                  wildcard: '%QUERY'
                }

              });

              users.initialize();

              $('input[name="search"]').typeahead({
                hint: true,
                highlight: true,
                minLength: 3
              }, {
                name: 'users',
                displayKey: 'username',
                source: users.ttAdapter(),
                templates: {
                  pending: [
                    '<div class="loading"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></div>'
                  ],
                  empty: [
                    '<div class="search"><div class="suggestion"><div class="info">no results found</div></div></div>'
                  ].join('\n'),
                  suggestion: function(data) {
                    console.log(data);
                    // best bet thru changing json field and combined 2 tables tgt
                    var secondary = '';
                    var image = '';
                    var address = '';
                    var name = '';
                    var display = '';
                    var tagline = '';
                    var hyphenName = '';

                    String.prototype.trimToLength = function(m) {
                      return (this.length > m) 
                        ? jQuery.trim(this).substring(0, m).split(" ").slice(0, -1).join(" ") + "..."
                        : this;
                    };
                    
                    name = data.name;
                    url = data.url;
                    image = data.image;

                    return '<div class="search">' +
                              '<div class="suggestion">' +
                                '<a class="row" href="' + url + '">' +
                                  '<div class="col-sm-2 image">' + 
                                    '<img height="40" width="40" src="' + image + '" onerror="this.onerror=null;this.src=`/asset/images/product/default.jpg`;">' +
                                  '</div>' +
                                  '<div class="col-sm-10 info">' + 
                                    name +
                                  '</div>' +
                                '</a>' +
                              '</div>' +
                            '</div>'
                  }
                }
              });
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //RUH 19072019 kill the socket.io
            {{--Echo.private('customer.{{ auth()->user() ? auth()->user()->customer->cus_id : null }}')--}}
                {{--.notification((notification) => {--}}
                    {{--if (notification === undefined || notification.length == 0) return;--}}

                    {{--if ($("#notifications").find('span').length == 0) {--}}
                        {{--$("#notifications a").append('<span class="notice"></span>');--}}
                    {{--}--}}

                    {{--@if (request()->route() && request()->route()->named('chat-list'))--}}
                        {{--chatModule.updateChatList(notification);--}}
                    {{--@endif--}}
                {{--});--}}
        </script>

        @yield('script')
    </body>
</html>