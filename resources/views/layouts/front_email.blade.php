<!DOCTYPE html>
<html>
	<head></head>
	<body>
	
	<div style="background-color: #f8f8f8; padding: 50px 0; width: 100%;">

		<table align="center" border="0" cellpadding="0" cellspacing="0" width="550" style="font-family:Arial,sans-serif;background:#fff;border-top:2px solid #ff1818;">
			<tbody>
				
				<tr>
					<td align="center" colspan="3" style="border-bottom: 1px solid #e6e6e6;">
						<img src="{{ url('/asset/images/logo/logo_email.png') }}">
					</td>
				</tr>
				
				<tr height="40">
					<td colspan="3"></td>
				</tr>

                @yield('content')

                <tr height="40">
					<td colspan="3"></td>
				</tr>
			</tbody>
		</table>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="550" style="font-family:Arial,sans-serif;font-size:12px;color:#aaa;margin-top:10px;">		
				<tr>
					<td colspan="3">
						<p>This e-mail is confidential. It may also be legally privileged. If you are not the addressee you may not copy, forward, disclose or use any part of it. If you have received this message in error, please delete it and all copies from your system and notify the sender immediately by return e-mail.</p>
					</td>
				</tr>
			</tbody>
		</table>
		
	</div>
	
	</body>
</html>