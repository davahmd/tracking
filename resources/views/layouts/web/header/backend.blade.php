<header class="MainHeader">
	<div class="logo">
		<a href="/">
			<img src="{{ asset('common/images/logo.png') }}">
		</a>
	</div>

	<div class="MemberNav MemberRightNav">
		<ul>
			<li>
				<a href="javascript:void(0)" data-toggle="modal" data-target="#language">
					<img src="{{ asset('assets/images/icon/icon_language.png') }}">
                    <span>@lang('localize.lang')</span>
				</a>
			</li>
		</ul>
	</div>
</header>