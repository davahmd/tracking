
@if($listings instanceof \Illuminate\Pagination\LengthAwarePaginator)
@php
$total = $listings->Total();
$first = $listings->firstItem();
$last = $listings->lastItem();
$max = $listings->lastPage();
$links = $listings->appends(Request::except('page'))->links();
@endphp

@if($total)
<tfoot>
    <tr>
        <td colspan="100">
            <div class=" col-xs-2">
                <span class="pagination">
                    @lang('localize.pagination.text', ['first' => $first, 'last' => $last, 'total' => $total])
                </span>
            </div>

            @if($last > 1)
            <div class="col-xs-8 text-center">
                {{ $links }}
            </div>
            <div class="col-xs-2 text-right pagination">
                <div class="input-group">
                <input class="form-control" type='number' id="pageno" size="3" min="1" max="{{ $max }}" placeholder="@lang('localize.Go_To_Page')" value="{{ Request::get('page') }}">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" onclick="gotopage('page')">@lang('localize.Go') !</button>
                    </span>
                </div>
            </div>
            @endif

        </td>
    </tr>
</tfoot>
@else
@if(!isset($noDataDisplay))
<tr>
    <td colspan="100" class="text-center">@lang('localize.nodata')</td>
</tr>
@endif
@endif
@endif