<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="kM02mnFDSZS1w3uqZ15PoRFkIhvb8uH6khL7HFrY">

    <title>{{ config('app.name') }}</title>

    <link rel="shortcut icon" href="{{ asset('common/images/favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/front.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/loading.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" media="all">

    @yield('styles')

</head>

<body>
    <div class="PageContainer">

        @yield('header')

        <div class="MainContent">

            @yield('content')

        </div>
    </div>
</body>
</html>
