<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="country_state_lib_version" content="{{ Cache::get('country_state_lib_version_number') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> {{ config('app.name') }} - @yield('title')</title>

    <link rel="icon" type="image/png" href="{{ asset('asset/images/favicon.png') }}">

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/bootstrap/bootstrap-reboot.css') }}">

    {{-- Fontawesome --}}
    <link rel="stylesheet" href="{{ asset('asset/css/fontawesome/all.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('asset/css/default.css?v=1.10') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/master.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/dropdown_bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/dashboard.css?v=1.03') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/custom_select.css') }}">

    <link href="{{ asset('common/lib/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/bootstrap-fileinput.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('common/lib/css/tagsinput.css') }}" rel="stylesheet" type="text/css" />

    @yield('style')
</head>
<body>
    <div class="wrapper">
        @yield('content')
    </div>

    <script type="text/javascript" src="{{ asset('asset/js/jquery-3.3.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('asset/js/popper.js') }}"></script>
    <script type="text/javascript" src="{{ asset('asset/js/bootstrap/bootstrap.js') }}"></script>

    <script type="text/javascript" src="{{ asset('asset/js/fontawesome/all.js') }}"></script>

    <script src="{{ asset('asset/js/dropdown_bs4.js') }}"></script>
    <script src="{{ asset('asset/js/custom_select.js') }}"></script>
    <script src="{{ asset('asset/js/zona.app.js') }}"></script>

    <script type="text/javascript" src="{{ asset('backend/js/plugins/iCheck/icheck.min.js') }}"></script>

    {{-- <script type="text/javascript" src="{{ asset('common/js/country_state_f2.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('common/js/common_function.js') }}"></script>

    <script type="text/javascript" src="{{ asset('common/lib/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('common/lib/js/bootstrap-fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('common/lib/js/sweetalert.min.js') }}"></script>
    <script src="//momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    <script type="text/javascript" src="{{ asset('js/echo.js') . '?v2' }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    @yield('script')
</body>
</html>