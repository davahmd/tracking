﻿<?php

return [

    'greeting' => 'Halo!',
    'regards' => 'Salam',
    'subcopy' => 'Jika Anda mengalami kesulitan mengklik tombol: actionText, salin dan tempel URL di bawah ini ke browser web Anda: [:actionUrl](:actionUrl)',

    'member' => [

        'reset_password' => [
            'title' => 'Setel Ulang Kata Sandi',
            'msg' => 'Anda menerima email ini karena kami menerima permintaan pengaturan ulang kata sandi untuk akun Anda.',
            'msg2' => 'Jika Anda tidak meminta pengaturan ulang kata sandi, tidak ada tindakan lebih lanjut yang diperlukan.',
            'btn' => 'Setel Ulang Kata Sandi',
        ],

        'account_activation' => [
            'title' => 'Verifikasi email',
            'msg' => 'Silakan klik tombol di bawah untuk memverifikasi email Anda agar dapat menerima email dari :mall_name di masa mendatang.',
            'msg2' => 'Akun ini terdaftar oleh administrator Kami, silakan ubah kata sandi Anda untuk alasan keamanan.',
            'btn' => 'Klik Di Sini untuk Memverifikasi',
        ],

        'auction_won' => 'Anda Telah Memenangkan :Lelang mall_name!',

        'forgot_password' => [
            'greeting' => 'Yang terhormat :cus_name',
            'msg' => '<p>kami menerima permintaan untuk mengatur ulang kata sandi yang terkait dengan alamat email ini. Jika Anda mengajukan permintaan ini, harap ikuti instruksi di bawah ini.</p>
            <p>Jika Anda tidak meminta pengaturan ulang kata sandi, Anda dapat dengan aman mengabaikan email ini. Kami meyakinkan Anda bahwa akun pelanggan Anda aman</p>',
            'btn_reset' => 'Klik di sini untuk mengatur ulang',
        ],

        'contact' => [
            'greeting' => 'Yang terhormat :cus_name',
            'msg' => 'Kami telah menerima pertanyaan Anda, kami akan segera menjawab pertanyaan Anda!',
            'details' => 'Rincian Pertanyaan',
            'subject' => 'Subyek',
            'content' => 'Pesan',
        ],
    ],

    'merchant' => [
        'account_activation' => [
            'title' => 'Satu langkah lagi',
            'msg' => 'Terima kasih telah mendaftar dengan akun :mall_name. Sebelum Anda mulai merasakan platform belanja kami, silakan klik tombol di bawah ini untuk mengaktifkan akun :mall_name Anda.',
            'msg2' => 'Akun ini terdaftar oleh administrator kami, silakan ubah kata sandi Anda untuk alasan keamanan.',
            'btn' => 'Klik Di Sini untuk Mengaktifkan',
        ],
    ],

    'admin' => [
        'welcome' => 'Selamat datang di Portal Admin :mall_name',

        'reset_password' => ':mall_name Setel Ulang Kata Sandi',
        'reset_secure_code' => ':mall_name Setel Ulang Kode Aman Pembayaran',

        'reset_password_merchant' => ':mall_name Setel Ulang Kata Sandi Merchant',
    ],

];