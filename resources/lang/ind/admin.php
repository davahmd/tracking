<?php

return [
    'login' => [
        'title' => 'Login Admin',
    ],

    'welcome' => 'Selamat datang di ZONA Admin',

    'nav' => [
        'dashboard' => 'Dasbor',
        'transactions' => 'Transaksi',
        'products' => 'Produk',
        'services' => 'Jadwal Layanan',
        'customers' => 'Pelanggan',
        'merchants' => 'Merchant',
        'stores' => 'Toko',
        'news' => 'Berita',
        'settings' => 'Pengaturan',
        'admins' => 'Administrator',
        'promotions' => 'Promosi',
        'calendar' => 'Kalender',
        'complains' => 'pengaduan',
        'negotiation' => 'Negosiasi',
    ],
];