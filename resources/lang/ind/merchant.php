<?php

return [
    'login' => [
        'title' => 'Login Merchant',
    ],

    'welcome' => 'Selamat datang di Merchant ZONA',

    'nav' => [
        'settings' => 'Pengaturan',
        'dashboard' => 'Dasbor',
        'transactions' => 'Transaksi',
        'products' => 'Produk',
        'promotions' => 'Promosi',
        'services' => 'Jadwal Layanan',
        'stores' => 'Toko',
        'calendar' => 'Kalender',
        'chat' => 'Obrolan',
        'complaint' => 'Keluhan',
        'negotiation' => 'Negosiasi',
    ],
];