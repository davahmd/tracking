<?php

return [
    // validation
    'username' => 'username',
    'email' => 'email',
    'password' => 'password',
    'merchant_id' => 'id_merchant',
    'member_id' => 'id_anggota',
    'inv_no' => 'no_inv',
    'order_id' => 'id_pesanan',
    'currency_rate' => 'nilai_tukar_mata_uang',
    'amount' => 'jumlah',
    'vcoin' => 'vcoin',
    'member_service_charge_rate' => 'tingkat_biaya_layanan_anggota',
    'member_service_charge_vcoin' => 'vcoin_biaya_layanan_anggota',
    'total_vcoin' => 'total_vcoin',
    'remark' => 'remark',
    'security_code' => 'kode_keamanan',

    // in code
    'systemError' => 'kesalahan sistem',
    'login' => 'login',
    'merchant' => 'merchant',
    'member' => 'anggota',
    'currency' => 'mata uang',
    'payment' => 'pembayaran',
    'primaryAcc' => 'akun merchant utama',

    'success' => ' sukses',
    'fail' => ' gagal',
    'notFound' => ' tidak ditemukan',

    'billNo' => 'tagihan # ',
    'hasAlreadyBeen' => ' sudah ',
    'hasNotYetBeen' => ' belum ',
    'paid' => 'dibayar',
    'cancelled' => 'dibatalkan',
    'claimed' => 'diklaim',
    'createOrder' => 'buat pesanan',

    'merchantId' => 'id merchant ',
    'memberId' => 'id member ',

    'doesNotBelongToThis' => ' bukan milik ini ',
    'orderCancelledByMerchant' => 'pesanan dibatalkan oleh merchant',
    'orderCancelledByMember' => ' pesanan dibatalkan oleh anggota',
    'cannotCancelAsPaymentMade' => ' tidak dapat dibatalkan karena pembayaran dilakukan',
    'hasBeenClaimedSuccessfully' => ' telah berhasil diklaim',

    'wrongCode' => 'kode keamanan pembayaran salah',
    'insufficientFund' => 'Kredit anggota tidak mencukupi\'s :jenis_dompet dompet',

    'failRetrieve' => 'Gagal mengambil data.',
    'successRetrieve' => 'Berhasil mengambil data.',
    'longitude' => 'longitude',
    'latitude' => 'latitude',
    'page' => 'halaman',
    'size' => 'ukuran',
    'search' => 'pencarian',
    'store_id' => 'ID Toko ',
    'customer_id' => 'ID Pelanggan',
    'review' => 'Ulasan',
    'addReview' => 'Tambahkan ulasan',
    'rating' => 'Peringkat',
    'addRating' => 'Tambahkan peringkat',
    'country_id' => 'ID Negara',
    'featured' => 'Diutamakan',
    'newest' => 'Terbaru',
    'found' => ' ditemukan',
    'notActive' => ' tidak aktif',
    'online' => ' tipe online',
    'notOffline' => ' tipe tidak Offline',

    'store' => 'toko',
    'storeId' => 'id toko ',
    'notAcceptPayment' => ' tidak dapat menerima pembayaran',

    'phonehash' => 'hash telepon',
    'hasNotBeen' => ' belum pernah ',

    'exceedLimit' => ' batas transaksi bulanan terlampaui',
    'invoice' => 'nomor faktur',

    'merchant_not_found' => 'Merchant tidak ditemukan',
    'validation_failed' => 'Validasi gagal',
    'coupon' => 'Kupon',
    'coupon_code' => 'Kode Kupon',
    'redemption' => 'Penebusan',
    'redemption_date' => 'Tanggal Penebusan',
    'redeemed' => 'Ditebus',
    'open' => 'Buka',
    'normal_purchase' => 'Pembelian Normal',
    'coupon_already_redeemed' => 'Kode kupon sudah ditebus/dibatalkan',
    'success_redeemed' => 'Berhasil ditebus :tipe',

    'cartId' => 'id keranjang',
    'deleted' => ' telah berhasil dihapus dari keranjang',
    'quantity' => 'kuantitas',
    'updated' => ' telah berhasil diperbarui',
    'failUpdate' => ' gagal memperbarui.',
    'failDelete' => ' gagal dihapus dari keranjang.',
    'productId' => 'id produk',
    'pricingId' => 'id harga',
    'addCart' => 'Masukkan ke keranjang',
    'memberName' => 'nama anggota',
    'memberAddress' => 'alamat anggota',
    'memberAvatar' => 'avatar anggota',
    'secureCode' => 'Kode keamanan',
    'secureCodeOld' => 'kode keamanan lama',
    'generated' => ' dihasilkan & kirim ke email Anda.',
    'failGenerate' => ' gagal menghasilkan. Silakan coba lagi.',
    'emptied' => 'Keranjang Anda berhasil dikosongkan',
    'failEmpty' => 'gagal menghapus semua item dari keranjang Anda.',
    'cartEmpty' => 'Tidak ada Barang yang ditemukan di keranjang Anda.',

    'invalid_verification_key' => 'Kunci Verifikasi Tidak Valid',
    'status_not_open' => ':Status tipe ini tidak terbuka',
    'invalid_order_status' => 'Status pesanan tidak valid',
    'date_expired' => ':tipe sudah kedaluwarsa',
    'event' => 'Peristiwa',
    'event_name' => 'Name peristiwa',
    'sort' => 'Menyortir',
    'status' => 'Status',
    'customer_name' => 'Nama pelanggan',
    'ticket_number' => 'Nomor tiket',
    'id' => 'Id',
    'cart_limit_quantity' => 'Jumlah item keranjang telah mencapai batasnya',
    'country_code' => 'Kode negara',
    'product' => [
        'type' => 'Tipe produk',
    ],
    'NotFound' => ':tipe tidak ditemukan!',
    'successUpdate' => ':Tipe telah berhasil diperbarui',
    'successCreate' => ':Tipe telah berhasil dibuat',
    'successDelete' => ':Tipe telah berhasil dihapus',
];