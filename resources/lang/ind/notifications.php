<?php

return [
    'merchant' => [
        'order' => [
            'cancelled' => [
                'title' => 'Pesanan Dibatalkan',
                'body' => 'Pesan barang dari transaksi #:transaction_id telah dibatalkan.'
            ],
            'completed' => [
                'title' => 'Pesanan Selesai',
                'body' => 'Pesan barang dari transaksi #:transaction_id telah selesai.'
            ],
            'placed' => [
                'title' => 'Pesanan Ditempatkan',
                'body' => 'Pesan barang dari transaksi #:transaction_id telah ditempatkan.'
            ],
        ],
        'service' => [
            'cancelled' => [
                'title' => 'Layanan Dibatalkan',
                'body' => 'Layanan #:service_id telah dibatalkan.'
            ],
            'rescheduled' => [
                'title' => 'Layanan Dijadwalkan Ulang',
                'body' => 'Layanan #:service_id telah dijadwal ulang.'
            ],
            'schedule-received' => [
                'title' => 'Layanan Diterima',
                'body' => 'Layanan #:service_id sudah diterima.'
            ],
            'schedule-reminder' => [
                'title' => 'Pengingat: Diperlukan Jadwal Layanan',
                'body' => 'Anda memiliki :total layanan tidak terjadwal.'
            ],
            'confirmed' => [
                'title' => 'Layanan Dikonfirmasi',
                'body' => 'Layanan #:service_id sudah diterima.'
            ],
        ],
        'price-negotiation' => [
            'received' => [
                'title' => 'Negosiasi Baru',
                'body' => 'Anda menerima negosiasi harga baru.'
            ],
            'counter-offer' => [
                'title' => 'Penawaran Konter Diterima',
                'body' => 'Anda menerima tawaran konter negosiasi baru.'
            ],
            'accepted' => [
                'title' => 'Negosiasi Diterima',
                'body' => 'Negosiasi Anda telah diterima.'
            ],
            'rejected' => [
                'title' => 'Negosiasi Ditolak',
                'body' => 'Negosiasi Anda telah ditolak.'
            ]
        ]
    ],

    'customer' => [
        'order' => [
            'cancelled' => [
                'title' => 'Pesanan Dibatalkan',
                'body' => 'Barang pesanan Anda dari transaksi #:transaction_id telah dibatalkan.'
            ],
            'completed' => [
                'title' => 'Pesanan Selesai',
                'body' => 'Barang pesanan Anda dari transaksi #:transaction_id telah selesai.',
                'review' => [
                    'line_1' => 'Terima kasih telah berbelanja bersama kami. Kami berharap Anda dapat memberikan ulasan kepada penjual tentang produk yang baru dibeli.',
                    'line_2' => 'Terima kasih atas dukungan Anda dan berharap dapat melayani Anda lain kali!',
                ],
            ],
            'placed' => [
                'title' => 'Pesanan Ditempatkan',
                'body' => 'Barang pesanan Anda dari transaksi #:transaction_id telah ditempatkan.'
            ],
            'shipped' => [
                'title' => 'Pesanan dikirimkan',
                'body' => 'Barang pesanan Anda dari transaksi #:transaction_id telah dikirimkan.'
            ],
            'received' => [
                'title' => 'Pesanan Diterima',
                'body' => 'Barang pesanan Anda dari transaksi #:transaction_id sudah diterima.'
            ]
        ],
        'service' => [
            'cancelled' => [
                'title' => 'Layanan Dibatalkan',
                'body' => 'Permintaan Anda untuk layanan #:service_id telah dibatalkan.'
            ],
            'rescheduled' => [
                'title' => 'Layanan Dijadwalkan Ulang',
                'body' => 'Permintaan Anda untuk layanan #:service_id telah dijadwal ulang.'
            ],
            'schedule-received' => [
                'title' => 'Layanan Diterima',
                'body' => 'Permintaan Anda untuk layanan #:service_id sudah diterima.'
            ],
            'schedule-reminder' => [
                'title' => 'Pengingat: Diperlukan Jadwal Layanan',
                'body' => 'Anda memiliki :total layanan tidak terjadwal.'
            ],
            'confirmed' => [
                'title' => 'Layanan Dikonfirmasi',
                'body' => 'Layanan Anda #:service_id telah dikonfirmasi.'
            ],
        ],
        'price-negotiation' => [
            'received' => [
                'title' => 'Negosiasi Terkirim',
                'body' => 'Negosiasi harga Anda telah dikirim.'
            ],
            'counter-offer' => [
                'title' => 'Negosiasi Terkirim - Penawaran Konter',
                'body' => 'Negosiasi penawaran balik Anda telah dikirim.'
            ],
            'accepted' => [
                'title' => 'Negosiasi Diterima',
                'body' => 'Negosiasi Anda telah diterima.'
            ],
            'rejected' => [
                'title' => 'Negosiasi Ditolak',
                'body' => 'Negosiasi Anda telah ditolak.'
            ],
            'expired' => [
                'title' => 'Negosiasi Berakhir',
                'body' => 'Negosiasi Anda telah kedaluwarsa.'
            ],
            'failed' => [
                'title' => 'Negosiasi Gagal',
                'body' => 'Negosiasi Anda gagal.'
            ],
        ]
    ],

];