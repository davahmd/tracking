<?php

return [

    'mr' => 'Mr.',
    'ms' => 'Ms.',
    'mrs' => 'Mrs.',
    'dr' => 'Dr.',
    'dato_sri' => 'Dato Sri',
    'datuk' => 'Datuk',
    'datin' => 'Datin',
    'professor' => 'Professor',

    'employee' => 'Karyawan',
    'supervisor' => 'Supervisor',
    'executive' => 'Executive',
    'manager' => 'Manager',
    'self_employed' => 'Wiraswasta',
    'director' => 'Direktur',
    'student' => 'Pelajar',

    'spm' => 'SPM',
    'diploma' => 'Diploma',
    'bachelor_degreee' => 'Bachelor Degree',
    'undergraduate' => 'Undergraduate',
    'master_degree' => 'Master Degree',
    'professional_degree' => 'Professional Degree',
    'doctorate' => 'Doctorate',

    'malay' => 'Malaysia',
    'chinese' => 'Chinese',
    'indian' => 'India',

    'christianity' => 'Kristen',
    'islam' => 'Islam',
    'hinduism' => 'Hindu',
    'buddhism' => 'Buddha',
    'atheism' => 'Ateisme',
    'confucianism' => 'Konfusianisme',
    'taoism' => 'Taoisme',

    'single' => 'Single',
    'married' => 'Menikah',
    'divorced' => 'Bercerai',
    'widowed' => 'Janda',

    'income_below' => 'Dibawah :amount',
    'income_between' => ':from ke :to',
    'income_above' => 'Diatas :from',
    'income_none' => 'Tidak ada',

    'none' => 'Tidak ada',
    'others' => 'Lainnya',
];