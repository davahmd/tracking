<?php

return [
	'holiday' => 'Libur',
	'holiday-list' => 'Daftar Libur',
	'offday-list' => 'Daftar Hari Tidak Aktif',
	'add-holiday' => 'Menambahkan Libur',
	'add-offday' => 'Menambahkan Off Day',
	'holidays' => [
		'labor-day' => 'Hari Buruh',
	], 
	'success' => [
		'holiday-added' => ':hari diaktifkan: mulai dengan sukses.',
		'holiday-deleted' => 'Libur dihapus.',
		'offday-added' => ':hari diaktifkan: mulai dengan sukses.',
		'offday-removed' => 'Offday dihapus.',
		'whitelist-added' => 'Daftar putih ditambahkan pada: tanggal',
		'whitelist-removed' => 'Daftar putih dihapus.',
	],
	'error' => [
		'overlap' => 'Liburan tidak dapat tumpang tindih dengan liburan lainnya.',
		'pending_scheduled_service' => 'Ada layanan terjadwal tertunda pada: tanggal. Harap batalkan semua layanan yang dijadwalkan sebelum menetapkan hari libur/libur pada hari ini.',
	],
	'warning' => [
		'delete-holiday' => 'Apakah Anda yakin akan menghapus liburan ini?',
		'whitelist-holiday' => 'Anda yakin bekerja di liburan ini?',
		'whitelisted-day' => 'Hari ini masuk daftar putih.',
		'remove-whitelisted-day' => 'Apakah Anda ingin menghapus hari daftar putih?',
	],
];