<?php

return [

    'language' => 'Inggris',

    /* Nav */
    'nav' => [
        'homepage' => 'Beranda',
        'category' => 'Kategori',
        'service' => 'Layanan',
        'language' => 'Bahasa',
        'official_brand' => 'merek resmi',
        'news_promotions' => 'Berita & Promosi',
        'see_all_promo' => 'Lihat Semua Promosi',
        'see_more' => 'Lihat Lainnya',

        'search_in_zona' => 'Cari di Zona',

        /* Account Nav */
        'account' => [
            'general_info' => 'informasi umum',
            'support' => 'dukungan',
            'general' => [
                'overview' => 'Peninjauan',
                'notification' => 'Notifikasi',
                'chat' => 'Obrolan',
                'favorites' => 'Favorit & Mengikuti',
                'automobile' => 'Mobil Saya',
                'order' => 'Pesanan Saya',
                'wholesale-order' => 'Pesanan Grosir Saya',
                'schedule' => 'Jadwal Saya',
                'negotiation' => 'Negosiasi Saya'
            ],
            'profile' => 'Profil',
            'delivery' => 'Alamat pengiriman',
            'setting' => [
                'change_pw' => 'Ganti kata sandi',
            ],
            'help' => 'Bantuan',
            'faq' => 'FAQ',
        ],

        'cat' => [
            'filter' => 'Filter',
            'tag' => 'Tag yang Dipilih',
            'product_brands' => 'Merek Produk',
            'car_brands' => 'Merek Mobil',
            'price_range' => 'Kisaran Harga',
            'sort_by' => 'Urut berdasarkan',
        ],

        'sort' => [
            'latest_update' => 'Update Terbaru',
            'most_popular' => 'Paling Populer',
            'price_low_hi' => 'Harga rendah ke tinggi',
            'price_hi_low' => 'Harga tinggi ke rendah',
        ],

        'mer_profile' => 'Profil\'s Merchant',
        'store_profile' => 'Profil\'s Toko',
        'store_products' => 'Produk\'s Toko',

    ],

    'button' => [
        'login' => 'login',
        'register' => 'Daftar',
        'load_more' => 'Muat lebih',
        'search_by_car' => 'Cari berdasarkan mobil Anda',
        'done' => 'Selesai',
        'add_cart' => 'Masukkan ke Keranjang',
        'visit_store' => 'Kunjungi Toko',
        'share' => 'Bagikan',
    ],

    'section' => [
        'trending_products' => 'Produk yang Sedang Populer',
        'featured_products' => 'Produk Unggulan',
        'search_products' => 'Produk yang dicari',
        'featured_stores' => 'Toko Unggulan',
        'distributors' => 'Distributor',
        'official_brands' => 'Merek Resmi',
        'featured_categories' => 'Kategori Unggulan',
        'latest_products' => 'Produk Terbaru',
        'sub_categories' => 'Sub Kategori',
        'most_reviewed' => 'Paling Banyak Ditinjau',
        'most_purchased' => 'Paling Banyak Dibeli',
        'most_favourited' => 'Paling disukai',
        'product_details' => 'Rincian Produk',
        'most_popular' => 'Paling Populer',
        'mer_rate' => 'Peringkat untuk :mer_name',
        'store_rate' => 'Peringkat untuk :stor_name',
        'reviews' => 'Ulasan',
        'most_popular_products' => 'Paling Populer',
        'flashsale_products' => 'Produk Flash Sale'
    ],

    'modal' => [
        'category' => [
            'search_fav_car' => 'Cari dari <b>Mobil Saya</b> list',
            'select_car_from_fav' => 'Silakan pilih mobil',
        ],
    ],

    'merchant' => [
        'product' => 'Produk\'s Merchant',
        'address' => 'Alamat\'s Merchant',
    ],

    'store' => [
        'product' => 'Produk\'s Toko',
        'address' => 'Alamat\'s Toko',
    ],

    'auth' => [
        'login' => 'Login',
        'login_to' => 'Masuk ke',
        'register' => 'Daftar',
        'fullname' => 'Nama lengkap',
        'password' => 'Kata sandi',
        'password_confirmation' => 'konfirmasi kata sandi',
        'hint' => [
            'password_length' => 'Kata sandi harus mengandung setidaknya 8 karakter.',
            'password_security' => 'Kata sandi membutuhkan setidaknya 1 karakter kapital dan 1 karakter numerik.',
            'go_to_login' => 'Sudah memiliki akun?',
            'go_to_register' => 'Tidak memiliki akun?',
            'login_here' => 'Login disini',
            'register_here' => 'Sign up disini',
        ],
    ],

    'cart' => [
        'success' => [
            'add' => 'Produk berhasil ditambahkan ke Keranjang.',
        ],
        'error' => [
            'add' => 'Gagal menambahkan produk ke Keranjang.',
        ],
    ],

    'contact' => [
        'success' => 'Pertanyaan Anda telah terkirim, kami akan segera menjawab pertanyaan Anda.',
    ],

    'order' => [
        'success' => [
            'schedule' => '[:schedule] dijadwalkan.',
            'schedule_order_completed' => 'Pesanan Selesai.',
        ],
        'error' => [
            'shipment' => 'Penerimaan Kiriman Gagal. Silakan coba lagi.',
            'schedule' => 'Jadwal gagal',
            'double_confirm_service_completed' => "Harap verifikasi status layanan terjadwal jika selesai.",
            'store_offday' => 'Toko tutup :tanggal jatuh tempo :hari',
        ],
        'warning' => [
            'schedule_order_cancel' =>'[:schedule] dibatalkan dan item pesanan ini telah dibatalkan juga.',
            'schedule_cancel' => '[:schedule] dibatalkan.',
            'schedule_datetime_cannot_be_same' => 'slot waktu ini telah tercatat, silakan memilih slot waktu yang lain.',
        ],
    ],
];
