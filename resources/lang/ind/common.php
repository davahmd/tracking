<?php

return [

    /**
     * lang for Datatables plugin.
     *
     * will passing url for json file
     */
    'lang-datatables' => '/backend/js/plugins/datatables/lang/en.json',

    'mall_name' => 'Zona',
    'credit_name' => 'Kredit',

    // Language
    'english' => 'Inggris',
    'indonesian' => 'Indonesian',
    'indonesia' => 'Indonesia',

    'keyword' => 'Kata kunci',
    'min' => 'Minimum',
    'max' => 'Maksimum',

    'pro_sold' => 'Produk Terjual',
    'ratings' => 'Peringkat',
    'reviews' => 'Ulasan',
    'rate_and_rev' => 'Peringkat & Ulasan',
    'favourite' => 'Favorit',

    'operation_hour' => 'Jam Operasi',
    'since' => 'Sejak',
    'delivery_from' => 'Pengiriman dari',
    'statistic' => 'Statistik',

    'success' => 'Sukses',
    'failed' => 'Gagal',
    'error' => 'Kesalahan',
];