<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ' :attribute harus diterima.',
    'active_url'           => ' :attribute bukan URL yang valid.',
    'after'                => ' :attribute harus menjadi tanggal sesudahnya :date.',
    'alpha'                => ' :attribute hanya boleh berisi huruf.',
    'alpha_dash'           => ' :attribute hanya berisi huruf, angka, dan garis putus-putus.',
    'alpha_num'            => ' :attribute hanya boleh berisi huruf dan angka.',
    'array'                => ' :attribute harus berupa array.',
    'before'               => ' :attribute harus tanggal sebelum :date.',
    'between'              => [
        'numeric' => ' :attribute harus diantara :min dan :max.',
        'file'    => ' :attribute harus diantara :min dan :max kilobyte.',
        'string'  => ' :attribute harus diantara :min dan :max karakter.',
        'array'   => ' :attribute harus diantara :min dan :max item.',
    ],
    'boolean'              => ' :attribute kolom harus benar atau salah.',
    'confirmed'            => ' :attribute konfirmasi tidak cocok.',
    'date'                 => ' :attribute bukan tanggal yang valid.',
    'date_format'          => ' :attribute tidak cocok dengan format :format.',
    'different'            => ' :attribute dan :other harus berbeda.',
    'digits'               => ' :attribute harus :digits digit.',
    'digits_between'       => ' :attribute harus diantara :min dan :max digit.',
    'distinct'             => ' :attribute kolom memiliki nilai duplikat.',
    'email'                => ' :attribute Harus alamat email yang valid.',
    'exists'               => 'Pilihan :attribute tidak valid.',
    'filled'               => ' :attribute kolom wajib diisi.',
    'image'                => ' :attribute harus berupa gambar.',
    'in'                   => 'Tak dapat dikirim ke yang dipilih :attribute .',
    'in_array'             => ' :attribute kolom tidak ada di :other.',
    'integer'              => ' :attribute harus harus integer.',
    'ip'                   => ' :attribute harus alamat IP yang valid.',
    'json'                 => ' :attribute harus berupa string JSON yang valid.',
    'max'                  => [
        'numeric' => ' :attribute tidak lebih besar dari :max.',
        'file'    => ' :attribute tidak lebih besar dari :max kilobytes.',
        'string'  => ' :attribute tidak lebih besar dari :max characters.',
        'array'   => ' :attribute tidak lebih besar dari :max items.',
    ],
    'mimes'                => ' :attribute harus berupa file tipe: :values.',
    'min'                  => [
        'numeric' => ' :attribute paling tidak harus :min.',
        'file'    => ' :attribute harus minimal :min kilobyte.',
        'string'  => ' :attribute harus minimal :min karakter.',
        'array'   => ' :attribute harus minimal :min item.',
    ],
    'not_in'               => 'Tidak dapat dikirim ke yang dipilih :attribute .',
    'numeric'              => ' :attribute harus berupa angka.',
    'present'              => ' :attribute kolom harus ada.',
    'regex'                => ' :attribute format tidak valid.',
    'required'             => ' :attribute kolom wajib diisi.',
    'required_if'          => ' :attribute kolom wajib diisi saat :other adalah :value.',
    'required_unless'      => ' :attribute kolom wajib diisi kecuali :other ada di :values.',
    'required_with'        => ' :attribute kolom wajib diisi saat :values ada',
    'required_with_all'    => ' :attribute kolom wajib diisi saat :values ada.',
    'required_without'     => ' :attribute kolom wajib diisi saat :values tidak ada.',
    'required_without_all' => ' :attribute kolom wajib diisi saat tidak ada :values ada.',
    'same'                 => ' :attribute dan :other harus cocok.',
    'size'                 => [
        'numeric' => ' :attribute harus :size.',
        'file'    => ' :attribute harus :size kilobyte.',
        'string'  => ' :attribute harus :size karakter.',
        'array'   => ' :attribute harus berisi :size item.',
    ],
    'string'               => ' :attribute harus berupa string.',
    'timezone'             => ' :attribute harus zona valid.',
    'unique'               => ' :attribute telah digunakan.',
    'url'                  => ' :attribute format tidak valid.',
    'valid_hash'           => ' :attribute yang dimasukkan tidak cocok',
    'password'             => ':attribute harus mengandung setidaknya 1 huruf besar, 1 huruf kecil dan 1 angka',
    'username'             => ':attribute hanya boleh berisi huruf dan angka',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'pesan khusus',
        ],
        'username' => [
            'regex' => 'Nama pengguna harus alfanumerik',
        ],
        'securecode' => [
            'digits' => 'Kode aman pembayaran harus 6 digit',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'payment_secure_code' => 'Kode Pembayaran Aman',
        'securecode' => 'Kode aman',
        'oldcode' => 'Kode Aman Lama',
        'username' => 'Nama pengguna',
    ],
    'captcha' => 'Kode captcha yang dimasukkan tidak valid. Silakan coba lagi',
];
