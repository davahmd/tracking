<?php

return [
	'holiday' => '假期',
	'holiday-list' => '假期列表',
	'add-holiday' => '增添假期',
	
	'holidays' => [
		'labor-day' => '劳动节',
	], 

	'success' => [
		'holiday-added' => '成功在:start设立:day',
	],
];