<?php

return [
    'login' => [
        'title' => 'Admin Login',
    ],

    'welcome' => 'Welcome to ZONA Admin',

    'nav' => [
        'dashboard' => 'Dashboard',
        'transactions' => 'Transactions',
        'products' => 'Products',
        'services' => 'Service Schedules',
        'customers' => 'Customers',
        'merchants' => 'Merchants',
        'stores' => 'Stores',
        'news' => 'News',
        'settings' => 'Settings',
        'admins' => 'Administrators',
        'promotions' => 'Promotions',
        'calendar' => 'Calendar',
        'complains' => 'Complaint',
        'negotiation' => 'Negotiation',
    ],
];