<?php

return [
    'merchant' => [
        'order' => [
            'cancelled' => [
                'title' => 'Order Cancelled',
                'body' => 'Order items from transaction #:transaction_id has been cancelled.'
            ],
            'completed' => [
                'title' => 'Order Completed',
                'body' => 'Order items from transaction #:transaction_id has been completed.'
            ],
            'placed' => [
                'title' => 'Order Placed',
                'body' => 'Order items from transaction #:transaction_id has been placed.'
            ],
        ],
        'service' => [
            'cancelled' => [
                'title' => 'Service Cancelled',
                'body' => 'Service #:service_id has been cancelled.'
            ],
            'rescheduled' => [
                'title' => 'Service Rescheduled',
                'body' => 'Service #:service_id has been rescheduled.'
            ],
            'schedule-received' => [
                'title' => 'Service Received',
                'body' => 'Service #:service_id has been received.'
            ],
            'schedule-reminder' => [
                'title' => 'Reminder: Service Schedule Required',
                'body' => 'You have :total unschedule service.'
            ],
            'confirmed' => [
                'title' => 'Service Confirmed',
                'body' => 'Service #:service_id has been received.'
            ],
        ],
        'price-negotiation' => [
            'received' => [
                'title' => 'New Negotiation',
                'body' => 'You received a new price negotiation.'
            ],
            'counter-offer' => [
                'title' => 'Counter Offer Received',
                'body' => 'You received a new negotiation\'s counter offer.'
            ],
            'accepted' => [
                'title' => 'Negotiation Accepted',
                'body' => 'Your negotiation has been accepted.'
            ],
            'rejected' => [
                'title' => 'Negotiation Rejected',
                'body' => 'Your negotiation has been rejected.'
            ]
        ]
    ],

    'customer' => [
        'order' => [
            'cancelled' => [
                'title' => 'Order Cancelled',
                'body' => 'Your order items from transaction #:transaction_id has been cancelled.'
            ],
            'completed' => [
                'title' => 'Order Completed',
                'body' => 'Your order items from transaction #:transaction_id has been completed.',
                'review' => [
                    'line_1' => 'Thank you for shopping with us. We wish you could leave a review to seller on the recent product purchased.',
                    'line_2' => 'Thanks for your support and looking forward to serve you next time!',
                ],
            ],
            'placed' => [
                'title' => 'Order Placed',
                'body' => 'Your order items from transaction #:transaction_id has been placed.'
            ],
            'shipped' => [
                'title' => 'Order Shipped',
                'body' => 'Your order items from transaction #:transaction_id has been shipped.'
            ],
            'received' => [
                'title' => 'Order Received',
                'body' => 'Your order items from transaction #:transaction_id has been received.'
            ]
        ],
        'service' => [
            'cancelled' => [
                'title' => 'Service Cancelled',
                'body' => 'Your request for service #:service_id has been cancelled.'
            ],
            'rescheduled' => [
                'title' => 'Service Rescheduled',
                'body' => 'Your request for service #:service_id has been rescheduled.'
            ],
            'schedule-received' => [
                'title' => 'Service Received',
                'body' => 'Your request for service #:service_id has been received.'
            ],
            'schedule-reminder' => [
                'title' => 'Reminder: Service Schedule Required',
                'body' => 'You have :total unschedule service.'
            ],
            'confirmed' => [
                'title' => 'Service Confirmed',
                'body' => 'Your service #:service_id has been confirmed.'
            ],
        ],
        'price-negotiation' => [
            'received' => [
                'title' => 'Negotiation Sent',
                'body' => 'Your price negotiation has been sent.'
            ],
            'counter-offer' => [
                'title' => 'Counter Offer Received',
                'body' => 'You received a new negotiation\'s counter offer.'
            ],
            'accepted' => [
                'title' => 'Negotiation Accepted',
                'body' => 'Your negotiation has been accepted.'
            ],
            'rejected' => [
                'title' => 'Negotiation Rejected',
                'body' => 'Your negotiation has been rejected.'
            ],
            'expired' => [
                'title' => 'Negotiation Expired',
                'body' => 'Your negotiation has been expired.'
            ],
            'failed' => [
                'title' => 'Negotiation Failed',
                'body' => 'Your negotiation has been failed.'
            ],
        ]
    ],

];