<?php

return [
    'login' => [
        'title' => 'Merchant Login',
    ],

    'welcome' => 'Welcome to ZONA Merchant',

    'nav' => [
        'settings' => 'Settings',
        'dashboard' => 'Dashboard',
        'transactions' => 'Transactions',
        'products' => 'Products',
        'promotions' => 'Promotions',
        'services' => 'Service Schedules',
        'stores' => 'Stores',
        'calendar' => 'Calendar',
        'chat' => 'Chat',
        'complaint' => 'Complaint',
        'negotiation' => 'Negotiations',
    ],
];