<?php

return [

    /**
     * lang for Datatables plugin.
     *
     * will passing url for json file
     */
    'lang-datatables' => '/backend/js/plugins/datatables/lang/en.json',

    'mall_name' => 'Zona',
    'credit_name' => 'Credit',

    // Language
    'english' => 'English',
    'indonesian' => 'Indonesian',
    'indonesia' => 'Indonesia',

    'keyword' => 'Keyword',
    'min' => 'Minimum',
    'max' => 'Maximum',

    'pro_sold' => 'Product Sold',
    'ratings' => 'Ratings',
    'reviews' => 'Reviews',
    'rate_and_rev' => 'Ratings & Reviews',
    'favourite' => 'Favourite',

    'operation_hour' => 'Operation Hour',
    'since' => 'Since',
    'delivery_from' => 'Delivery From',
    'statistic' => 'Statistic',

    'success' => 'Success',
    'failed' => 'Failed',
    'error' => 'Error',
];