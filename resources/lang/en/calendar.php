<?php

return [
	'holiday' => 'Holiday',
	'holiday-list' => 'Holiday List',
	'offday-list' => 'Offday List',
	'add-holiday' => 'Add Holiday',
	'add-offday' => 'Add Offday',
	'holidays' => [
		'labor-day' => 'Labor Day',
	], 
	'success' => [
		'holiday-added' => ':day is set on :start successfully.',
		'holiday-deleted' => 'Holiday is removed.',
		'offday-added' => ':day is set on :start successfully.',
		'offday-removed' => 'Offday is removed.',
		'whitelist-added' => 'Whitelist is added on :date',
		'whitelist-removed' => 'Whitelist is removed.',
	],
	'error' => [
		'overlap' => 'Holiday cannot be overlapped with other holiday.',
		'pending_scheduled_service' => 'There are pending scheduled service(s) at :date. Please cancel out all scheduled service before to set holiday/offday on this day.',
	],
	'warning' => [
		'delete-holiday' => 'Are you sure to remove this holiday?',
		'whitelist-holiday' => 'Are you sure to work on this holiday?',
		'whitelisted-day' => 'This day is whitelisted.',
		'remove-whitelisted-day' => 'Do you want to remove the whitelisted day?',
	],
];