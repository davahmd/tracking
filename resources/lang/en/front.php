<?php

return [

    'language' => 'English',

    /* Nav */
    'nav' => [
        'homepage' => 'Home',
        'category' => 'Category',
        'service' => 'Service',
        'language' => 'Language',
        'official_brand' => 'official brand',
        'news_promotions' => 'News & Promotions',
        'see_all_promo' => 'See All Promotions',
        'see_more' => 'See More',

        'search_in_zona' => 'Search in Zona',

        /* Account Nav */
        'account' => [
            'general_info' => 'general information',
            'support' => 'support',
            'general' => [
                'overview' => 'Overview',
                'notification' => 'Notification',
                'chat' => 'Chat',
                'favorites' => 'Favorites & Follows',
                'automobile' => 'My Automobile',
                'order' => 'My Orders',
                'wholesale-order' => 'My Wholesale Orders',
                'schedule' => 'My Schedules',
                'negotiation' => 'My Negotiations'
            ],
            'profile' => 'Profile',
            'delivery' => 'Delivery Address',
            'setting' => [
                'change_pw' => 'Change Password',
            ],
            'help' => 'Help',
            'faq' => 'FAQ',
        ],

        'cat' => [
            'filter' => 'Filter',
            'tag' => 'Selected Tag',
            'product_brands' => 'Product Brands',
            'car_brands' => 'Car Brands',
            'price_range' => 'Price Range',
            'sort_by' => 'Sort by',
        ],

        'sort' => [
            'latest_update' => 'Latest Update',
            'most_popular' => 'Most Popular',
            'price_low_hi' => 'Price low to high',
            'price_hi_low' => 'Price high to low',
        ],

        'mer_profile' => 'Merchant\'s Profile',
        'store_profile' => 'Stores\'s Profile',
        'store_products' => 'Stores\'s Products',

    ],

    'button' => [
        'login' => 'login',
        'register' => 'register',
        'load_more' => 'Load More',
        'search_by_car' => 'Search by your automobile',
        'done' => 'Done',
        'add_cart' => 'Add To Cart',
        'visit_store' => 'Visit Store',
        'share' => 'Share'
    ],

    'section' => [
        'trending_products' => 'Trending Products',
        'featured_products' => 'Featured Products',
        'search_products' => 'Searched Products',
        'featured_stores' => 'Featured Stores',
        'distributors' => 'Distributors',
        'official_brands' => 'Official Brands',
        'featured_categories' => 'Featured Categories',
        'latest_products' => 'Latest Products',
        'sub_categories' => 'Sub Categories',
        'most_reviewed' => 'Most Reviewed',
        'most_purchased' => 'Most Purchased',
        'most_favourited' => 'Most Favourited',
        'product_details' => 'Product Details',
        'most_popular' => 'Most Popular',
        'mer_rate' => 'Ratings for :mer_name',
        'store_rate' => 'Ratings for :stor_name',
        'reviews' => 'Ulasan',
        'most_popular_products' => 'Most Popular',
        'flashsale_products' => 'Flash Sale Products'
    ],

    'modal' => [
        'category' => [
            'search_fav_car' => 'Search from <b>My Automobile</b> list',
            'select_car_from_fav' => 'Please select car',
        ],
    ],

    'merchant' => [
        'product' => 'Merchant\'s products',
        'address' => 'Merchant\'s address',
    ],

    'store' => [
        'product' => 'Store\'s products',
        'address' => 'Store\'s address',
    ],

    'auth' => [
        'login' => 'Login',
        'login_to' => 'Login To',
        'register' => 'Register',
        'fullname' => 'Full name',
        'password' => 'Password',
        'password_confirmation' => 'Password confirmation',
        'hint' => [
            'password_length' => 'Password must contains at least 8 characters.',
            'password_security' => 'Password requires at least 1 capital character and 1 numeric character.',
            'go_to_login' => 'Already have an account?',
            'go_to_register' => 'Do not have an account?',
            'login_here' => 'Login here',
            'register_here' => 'Sign up here',
        ],
    ],

    'cart' => [
        'success' => [
            'add' => 'Product is added to cart successfully.',
        ],
        'error' => [
            'add' => 'Failed to add the product to cart.',
        ],
    ],

    'contact' => [
        'success' => 'Your inquiries has been send, we will respond to your inquiries immediately.',
    ],

    'order' => [
        'success' => [
            'schedule' => '[:schedule] is scheduled.',
            'schedule_order_completed' => 'Order is completed.',
        ],
        'error' => [
            'shipment' => 'Shipment Acceptant Failed. Please try again.',
            'schedule' => 'Schedule failed',
            'double_confirm_service_completed' => "Please verify the scheduled service's status if completed.",
            'store_offday' => 'The store is closed on :date due to :day',
        ],
        'warning' => [
            'schedule_order_cancel' =>'[:schedule] is cancelled and this order item has been cancelled too.',
            'schedule_cancel' => '[:schedule] is cancelled.',
            'schedule_datetime_cannot_be_same' => 'This timeslot is same with the record, please pick another timeslot.',
        ],
    ],
];
